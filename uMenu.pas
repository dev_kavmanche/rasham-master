unit uMenu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,Menus,
  ExtCtrls, StdCtrls, HMemo, HChkBox, ComCtrls, HRichEdt, Buttons, HListBox,
  HLabel;

type
  TfmMenu = class(TForm)
    pnOne: TPanel;
    Panel1: TPanel;
    b1: TImage;
    b2: TImage;
    b4: TImage;
    b5: TImage;
    b6: TImage;
    b7: TImage;
    b8: TImage;
    b9: TImage;
    pnList: TPanel;
    lbList: THebListBox;
    bt03: TButton;
    bt04: TButton;
    pn1: TPanel;
    n1: THebLabel;
    b3: TImage;
    procedure b1Click(Sender: TObject);
    procedure lbListDblClick(Sender: TObject);
    procedure bt03Click(Sender: TObject);
    procedure bt04Click(Sender: TObject);
    procedure lbListKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure FindItemByCaption(CurNode : tMenuItem; inp : string; var FoundItem : tMenuItem);
    procedure DisplayItems(CurNode : tMenuItem);
  end;

var
  fmMenu: TfmMenu;

implementation

uses Main, Util, utils2;

{$R *.DFM}

procedure TfmMenu.FindItemByCaption(CurNode: TMenuItem; inp: string; var FoundItem: TMenuItem);
var
  i: integer;
begin
  if CurNode.Caption <> inp then
    for i := 0 to CurNode.Count-1 do
      FindItemByCaption(CurNode.Items[i], inp, FoundItem)
  else
    FoundItem := CurNode;
end;

procedure TfmMenu.DisplayItems(CurNode : tMenuItem);
var
  i: integer;
begin
  lbList.Items.Clear;
  if bt04.Caption = '����� ���' then
    bt03.Caption := '����� ����'
  else
    bt03.Caption := '���� ����';

  if CurNode = fmMain.HebMainMenu1.Items then
  begin
    n1.Caption := '����� ����';
    bt04.Caption := '����� ���';
  end
  else
  begin
    n1.Caption := CurNode.Caption;
    bt04.Caption := '���� ������';
  end;

  for i := 0 to CurNode.Count-1 do
    if (CurNode.Items[i].Caption <> '-') and (CurNode.Items[i].Visible) and (CurNode.Items[i].Tag <> 99) then
      lbList.Items.Add(CurNode.Items[i].Caption);
  if pnList.Visible then
    lbList.ItemIndex:= 0;
end;

procedure TfmMenu.b1Click(Sender: TObject);
begin
  if pnList.Visible then
    bt03Click(Self);
  b1.Cursor := crDefault;
  b2.Cursor := crDefault;
  b3.Cursor := crDefault;
  b4.Cursor := crDefault;
  b5.Cursor := crDefault;
  b6.Cursor := crDefault;
  b7.Cursor := crDefault;
  b8.Cursor := crDefault;
  b9.Cursor := crDefault;
  with fmMain do
  begin
    if Sender = b1 then
      DisplayItems(N6);
    if Sender = b2 then
      DisplayItems(N5);
    if Sender = b3 then
      DisplayItems(N1);
    if Sender = b4 then
      DisplayItems(N25);
    if Sender = b5 then
      DisplayItems(N11);
    if Sender = b6 then
      DisplayItems(N20);
    if Sender = b7 then
      DisplayItems(miTakanon);
    if Sender = b8 then
      DisplayItems(N28);
    if Sender = b9 then
      DisplayItems(N14);
  end;
  pnList.Visible := True;
  lbList.SetFocus;
  lbList.ItemIndex := 0;
end;

procedure TfmMenu.lbListDblClick(Sender: TObject);
var
  CurrentItem: TMenuItem;
begin
  FindItemByCaption(fmMain.HebMainMenu1.Items, lbList.Items.Strings[lbList.ItemIndex], CurrentItem);
  if n1.Caption = '����� ����' then
    DisplayItems(CurrentItem)
  else
  begin
    if CurrentItem = fmMain.N14 then  // Same Caption
      CurrentItem:= fmMain.N15;
    if CurrentItem = fmMain.N5 then
      CurrentItem:= fmMain.N8;
    CurrentItem.Click;
  end;
end;

procedure TfmMenu.bt03Click(Sender: TObject);
begin
  If (Sender = bt03) and (bt03.Caption <> '���� ����') then
    DisplayItems(fmMain.HebMainMenu1.Items)
  else
  begin
    pnList.Visible := False;
    b1.Cursor := crHandPoint;
    b2.Cursor := crHandPoint;
    b3.Cursor := crHandPoint;
    b4.Cursor := crHandPoint;
    b5.Cursor := crHandPoint;
    b6.Cursor := crHandPoint;
    b7.Cursor := crHandPoint;
    b8.Cursor := crHandPoint;
    b9.Cursor := crHandPoint;
    bt04.Caption:= '���� ������';
  end;
end;

procedure TfmMenu.bt04Click(Sender: TObject);
begin
  lbListDblClick(lbList);
end;

procedure TfmMenu.lbListKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    lbListDblClick(lbList);
  if Key = VK_ESCAPE then
    bt03Click(bt03);
end;

procedure TfmMenu.FormShow(Sender: TObject);
begin
   WriteLog('fmMenu Create', GetLogFileName);//'C:\RashLog.txt');
  //==>tsahi 24.7.05 : centering the screen because it is shaped diferently
  fmMenu.top := (fmMain.Height - fmMenu.Height) div 2 -90;
  fmMenu.left := (fmMain.Width  - fmMenu.Width) div 2 +10;
  //<==
end;

end.
