{
MODIFICATION HISTORY
DATE         BY    TASK      VERSION     DESCRIPTION
25/12/2019   sts   19396     1.7.7       code refactoring
}

unit BlankUndo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HGrids, SdGrid, HRadio, HGroupBx, HRadGrup, DataObjects, Db,
  DBTables;

type
  TfmUndoBlank = class(TfmBlankTemplate)
    Grid: TSdGrid;
    Panel29: TPanel;
    Panel21: TPanel;
    Panel23: TPanel;
    rgOptions: THebRadioGroup;
    btDelete: TButton;
    taTemp: TTable;
    procedure rgOptionsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure GridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
    procedure Panel23Click(Sender: TObject);
    procedure Panel21Click(Sender: TObject);
    procedure Panel29Click(Sender: TObject);
  private
    { Private declarations }
    Action: TFileAction;
  protected
    { Protected declarations }
    function HistoryEnum(FileItem: TFileItem): Boolean;
    procedure EvDataLoaded; override;
    procedure EvPrintData; override;
    procedure DirectorsUndo (History: THistory; FileAction: TFileAction);
//    procedure ManagersUndo(CompanyNum: integer; MZeut, MName, MAddress, MPhone: string);
  public
    { Public declarations }
  end;

var
  fmUndoBlank: TfmUndoBlank;

implementation

uses Main, Util, cSTD, dialog, PrintUndo, DataM, MultipleDirectorsChoice ;

{$R *.DFM}

function TfmUndoBlank.HistoryEnum(FileItem: TFileItem): Boolean;
begin
  if Action in [faAll,FileItem.FileInfo.Action] then
    with Grid.Rows[Grid.RowCount-1] do
    begin
      Objects[0] := Pointer(FileItem.FileInfo.RecNum);
      Strings[0] := DateToLongStr(FileItem.FileInfo.DecisionDate);
      Strings[1] := GetFileActionName(FileItem.FileInfo.Action);
      Strings[2] := FileItem.FileInfo.FileName;
      Grid.RowCount := Grid.RowCount + 1;
    end;
  Result := True;
end;

procedure TfmUndoBlank.EvDataLoaded;
const
  //FileIndex: Array [0..13] of TFileAction = (faNumber, faIncrease, faDecrease, faHakzaa, faHaavara, faHamara, faDirector, faAddress, faDoh, faStockHolderInfo, faDirectorInfo, faCompanyName, faZipCode, faAll);
  FileIndex: array [0..16] of TFileAction = (faNumber, faIncrease, faDecrease, faHakzaa, faHaavara,           faDirector, faAddress, faDoh, faStockHolderInfo, faDirectorInfo, faCompanyName, faHamara, faManager, faSignName, faZipCode, faTakanon, faAll);
var
  History: THistory;
begin
  Action := FileIndex[rgOptions.ItemIndex];

  Grid.RowCount:= 1;
  Grid.ResetLine(0);
  History := THistory.Create;
  try
    History.EnumBlanks(Company.RecNum, HistoryEnum);
  finally
    History.Free;
  end;
  if Grid.RowCount > 1 then
    Grid.RowCount := Grid.RowCount-1;
  Grid.Repaint;
  btDelete.Enabled := False;
  //Panel23Click(panel23);
  // Moshe 14/11/2018 - First sort upon record number
  // so items order is upon their creation.
  SortGrid(Grid,'0,OBJECT,DOWN');
  inherited;
end;

procedure TfmUndoBlank.rgOptionsClick(Sender: TObject);
begin
  inherited;
  EvDataLoaded;
end;

procedure TfmUndoBlank.FormCreate(Sender: TObject);
begin
  ShowStyle := afActive;
  FileAction := faNone;
  ReadOnly := True;
  inherited;
end;

procedure TfmUndoBlank.btDeleteClick(Sender: TObject);

  function GetFileItem: TFileItem;
  var
    FilesList: TFilesList;
    i: Integer;
  begin
    Result := nil;
    FilesList := TFilesList.Create(Company.RecNum, StrToFileAction(Grid.Cells[1,Grid.Row]), False);
    try
      for i := 0 To FilesList.Count-1 do
        if FilesList.FileInfo[i].FileName = Grid.Cells[2, Grid.Row] then
          Result := FilesList.Items[i];
    finally
      FilesList.Free;
    end;
  end;

var
  FileItem: TFileItem;
  History: THistory;
begin
  inherited;
  History := THistory.Create;
  try
    if (Grid.Row < 0) or (Grid.Cells[0,Grid.Row] = '') then
    begin
      fmDialog.ActiveControl:= fmDialog.Button3;
      fmDialog.Caption:= '��� ����';
      fmDialog.memo.lines.Clear;
      fmDialog.memo.lines.add('��� ��');
      fmDialog.memo.lines.add('�� ���� ������ �����');
      fmDialog.Button1.Visible:= false;
      fmDialog.Button2.Visible:= False;
      fmDialog.Button3.Caption:= '�����';
      fmDialog.ShowModal;
      fmDialog.Button3.Caption:= '�����';
      fmDialog.Button1.Visible:= True;
      fmDialog.Button2.Visible:= True;
      fmDialog.Caption:= '����';
    end
    else
    begin
      FileItem := GetFileItem;
      if (not History.IsActionLastEx(Company.RecNum, FileItem)) then
      begin
        Application.MessageBox(
          PChar('��� ��!' + #10#13 +
            '����� ������ ������ ������ ��. �� ���� ��' + #10#13 +
            '������� ������� ���� ����� ������ �������.' + #10#13#10#13 +
            '������ ������� ����� ���� ���:' + #10#13#10#13 +
            DateToLongStr(FileItem.FileInfo.DecisionDate) + '   ' +
            GetFileActionName(FileItem.FileInfo.Action) + '   ' + FileItem.FileInfo.FileName),
          '��� ����',
          MB_OK + MB_ICONASTERISK + MB_RTLREADING + MB_RIGHT);
      end
      else
      begin
        fmDialog.memo.Lines.Clear;
        fmDialog.memo.Lines.Add('');
        fmDialog.memo.Lines.Add('��� ���� �� ������ ?');
        fmDialog.Button3.Visible := False;
        fmDialog.ShowModal;
        fmDialog.Button3.Visible := True;
        if (fmDialog.DialogResult = drYES) then
        begin
          if (StrToFileAction(Grid.Cells[1, Grid.Row]) in [faStockHolderInfo, faDirectorInfo, faManager, faSignName]) then
            DirectorsUndo(History, StrToFileAction(Grid.Cells[1, Grid.Row]))
          else if (StrToFileAction(Grid.Cells[1, Grid.Row]) = faZipCode) then
            History.UndoFile(FileItem)
          else
            History.UndoLastBlank(Company.RecNum, StrToFileAction(Grid.Cells[1, Grid.Row]));
          EvDataLoaded;
        end;
      end;
      FileItem.Free;
    end;
  finally
    History.Free;
  end;
end; // TfmUndoBlank.btDeleteClick

procedure TfmUndoBlank.GridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
begin
  inherited;
  btDelete.Enabled:= RemoveSpaces(Grid.Cells[0,Row], rmBoth)<>'';
end;

procedure TfmUndoBlank.Panel23Click(Sender: TObject);
begin
  inherited;
  if (Grid.RowCount = 1) or (RemoveSpaces(Grid.Cells[0,0], rmBoth) = '') then
    Exit;
  SortGrid(Grid,'0,DATE,DOWN;0,OBJECT,DOWN')
end;

procedure TfmUndoBlank.Panel21Click(Sender: TObject);
begin
  inherited;
  if (Grid.RowCount = 1) or (RemoveSpaces(Grid.Cells[0,0], rmBoth) = '') then
    Exit;
  SortGrid(Grid,'1,STRING,UP;0,OBJECT,DOWN')
end;

procedure TfmUndoBlank.Panel29Click(Sender: TObject);
begin
  inherited;
  if (Grid.RowCount = 1) or (RemoveSpaces(Grid.Cells[0,0], rmBoth) = '') then
    Exit;
  SortGrid(Grid,'2,STRING,UP;0,OBJECT,DOWN')
end;

procedure TfmUndoBlank.EvPrintData;
var
  I,J: Integer;
begin
  with taTemp.FieldDefs do
  begin
    Clear;
    Add('Date', ftString, 10, False);
    Add('Action', ftString, 20, False);
    Add('Name', ftString, 80, False);
  end;
  taTemp.CreateTable;
  taTemp.Open;
  For I:= 0 To Grid.RowCount-1 do
  begin
    taTemp.Append;
    For J:= 0 To Grid.ColCount-1 do
      taTemp.Fields[J].AsString:= MakeHebStr(Grid.Cells[J,I], False);
    taTemp.Post;
  end;
  taTemp.Close;
  Application.CreateForm(TfmQRUndo, fmQRUndo);
  try
    fmQRUndo.PrintExecute(Company);
  finally
    fmQRUndo.Free;
  end;
end;

//============== Tsahi: undo multiple Directors
procedure TfmUndoBlank.DirectorsUndo(History: THistory; FileAction: TFileAction);

  function ShowMultiDialog(Zeut: string; OriginalFileAction: TFileAction): Integer;
  var
    ResInt, ResCode: integer;
    Person, OtherPerson: TPersonType;
    sMessage, sMessage2: string;
  begin
    //==>Tsahi: 20.6.05 : changed so as to ask two questions of user (multiple+stockholder) whenever appropriate
    ResInt := 0; //save none
    ResCode := 0;

    if (FileAction = faDirectorInfo) then
    begin
      Person := diDirector;
      OtherPerson := diStockHolder;
    end
    else
    begin
      Person := diStockHolder;
      OtherPerson := diDirector;
    end;

    Application.CreateForm(TfmMultipleDirectorsChoice, fmMultipleDirectorsChoice);
    fmMultipleDirectorsChoice.ShowFull(Zeut ,Grid.Cells[2,Grid.Row],Company.RecNum, Person, self.DecisionDate);

    if (OriginalFileAction = faManager) and (fmMultipleDirectorsChoice.ResultList.Count = 0) then
    begin
      History.UndoLastBlank(Company.RecNum, faManager);
      Result:=0;
      Exit;
    end;

    if ((OriginalFileAction = FileAction)) then //not Nose-Misra or Mankal
    begin
      ResInt := 4; // 4 =change person
      if (fmMultipleDirectorsChoice.ResultList.PersonCount(OtherPerson) > 0) Then
      begin // person is both director and stockholder
        sMessage := '����� �� ���� ����� ������� ����� ��� �����.';
        sMessage2 := '��� ���� �� ������ ������?';
        ResCode := ResCode + 1; //1 = change other person
      end else
    end
    else
    begin
      sMessage := '����� �� ���� �';
      sMessage2 := '��� ���� �� ������ �� ���?';
      if (fmMultipleDirectorsChoice.ResultList.PersonCount(OtherPerson) > 0) Then
      begin
        sMessage := sMessage + '���� ' + PersonTostr(OtherPerson);
        ResCode := 1; //1 = change other person
      end;

      if (fmMultipleDirectorsChoice.ResultList.PersonCount(Person) > 0) Then
      begin
        if ResCode = 1 then
          sMessage := sMessage+' �';
        sMessage := sMessage + '���� ' + PersonTostr(Person);
        ResCode := ResCode + 4; // 4 =change person
      end;
    end;

    if ResCode > 0 then
    begin
      fmDialog.memo.Clear;
      fmDialog.memo.Lines.Add('');
      fmDialog.memo.Lines.Add(sMessage);
      fmDialog.memo.Lines.Add(sMessage2);
      fmDialog.Button3.Visible:= False;
      fmDialog.ShowModal;
      fmDialog.Button3.Visible:= True;
      if fmDialog.DialogResult = drYes then
        ResInt:=ResInt+ResCode;
    end;
    if ((fmMultipleDirectorsChoice.ResultList.PersonCount(diManager) > 0) or
        (fmMultipleDirectorsChoice.ResultList.PersonCount(diSignName) > 0)) Then
    begin
      sMessage:='';
      ResCode:=0;
      fmDialog.memo.Clear;
      fmDialog.memo.Lines.Add('');
      if (OriginalFileAction = faSignName) then
      begin
        ResInt:=ResInt+8;
        sMessage:=sMessage+'���� ���� �� ��� �� ���"� �����.';
        ResCode:=ResCode+2; //2 = change manager
      end
      else if (OriginalFileAction = faManager) then
      begin
        ResInt:=ResInt+2;
        sMessage:=sMessage+'���"� �� ��� �� ���� ���� �����.';
        ResCode:=ResCode+8; //8 = change SignName
      end
      else
      begin
        sMessage:=sMessage+'��� �� ��� �� ���"� �� ���� ���� �����.';
        ResCode:=ResCode+2+8; //8 = change SignName, 2 = change manager
      end;

      if Not(((OriginalFileAction = faSignName) and
              (fmMultipleDirectorsChoice.ResultList.PersonCount(diManager)= 0)) or
             ((OriginalFileAction = faManager) and
              (fmMultipleDirectorsChoice.ResultList.PersonCount(diSignName)=0))) then
      begin
        fmDialog.memo.Lines.Add(sMessage);
        fmDialog.memo.Lines.Add('��� ���� �� ������ �����?');
        fmDialog.Button3.Visible:= False;
        fmDialog.ShowModal;
        fmDialog.Button3.Visible:= True;
        if fmDialog.DialogResult = drYes then
          ResInt:=ResInt+ResCode; //2 change manager+signname
      end;
    end;

    Result := ResInt;
  end;

var
  sZeut, sTableName, sTableName2: string;
  ObjectType, ObjectType2: TObjectClass;
  i: integer;
  OriginalFileAction, FileAction2: TFileAction;
  TempUR,UserResponse: integer;
begin
  OriginalFileAction := FileAction;

  if  (FileAction = faStockHolderInfo) or (FileAction = faDirectorInfo)then
  begin
    if  (FileAction = faStockHolderInfo) then
    begin
      sTableName := Data.taStockHolders.TableName;
      sTableName2 := Data.taDirectors.TableName;
      FileAction2 := faDirectorInfo;
      ObjectType := ocStockHolder;
      ObjectType2:= ocDirector;
    end
    else if (FileAction = faDirectorInfo) then
    begin
      sTableName  := Data.taDirectors.TableName;
      sTableName2 := Data.taStockHolders.TableName;
      FileAction2 := faStockHolderInfo;
      ObjectType  := ocDirector;
      ObjectType2 := ocStockHolder;
    end;

    //get Zeut
    data.qGeneric.SQL.Text := 'SELECT d.Zeut, d.Name, d.RecNum'
        + ' FROM ' + sTableName + ' d, ' + data.taForms.TableName +' fo'
        + ' WHERE d.RecNum = fo.ObjectNum AND fo.FileNum = ' + IntToStr(Integer( Grid.Rows[Grid.Row].Objects[0]))
              + ' AND fo.ObjectType = ''' + ObjToChar(ObjectType) + '''';
    data.qGeneric.open;
    data.qGeneric.First;
    sZeut := data.qGeneric.FieldByName('Zeut').AsString;

    if sZeut = '' then
    begin
      data.qGeneric.SQL.Text := 'SELECT d.Zeut, d.Name, d.RecNum'
         + ' FROM ' + sTableName2 + ' d, ' + data.taForms.TableName + ' fo'
         + ' WHERE d.RecNum = fo.ObjectNum AND fo.FileNum = ' + IntToStr(Integer(Grid.Rows[Grid.Row].Objects[0]))
               + ' AND fo.ObjectType = ''' + ObjToChar(ObjectType2) + '''';
      data.qGeneric.open;
      data.qGeneric.Last;
      sZeut := data.qGeneric.FieldByName('Zeut').AsString;
    end;
  end
  else if (FileAction = faManager) then
  begin
    FileAction:=faDirectorInfo;
    FileAction2:= faStockHolderInfo;
    with data.qGeneric do
    begin
      SQL.Clear;
      SQL.Add('SELECT d.Zeut ');
      SQL.Add('FROM '+ data.taManager.TableName +' d, '+data.taForms.TableName +' fo');
      SQL.Add('WHERE d.RecNum = fo.NewNum AND fo.FileNum = '+ IntToStr(Integer( Grid.Rows[Grid.Row].Objects[0])));
      SQL.Add(' AND fo.ObjectType = '''+ObjToChar(ocCompany)+'''');
      //ExecSQL;
      open;
      Last;
      sZeut := FieldByName('Zeut').AsString;
    end;
  end
  else if (FileAction = faSignName) then
  begin
    FileAction:=faDirectorInfo;
    FileAction2:= faStockHolderInfo;
    with data.qGeneric do
    begin
      SQL.Clear;
      SQL.Add('SELECT d.SigZeut ');
      SQL.Add('FROM '+ data.taSigName.TableName +' d, '+data.taForms.TableName +' fo');
      SQL.Add('WHERE d.RecNum = fo.NewNum AND fo.FileNum = '+ IntToStr(Integer( Grid.Rows[Grid.Row].Objects[0])));
      SQL.Add(' AND fo.ObjectType = '''+ObjToChar(ocCompany)+'''');
      //ExecSQL;
      open;
      Last;
      sZeut:=FieldByName('SigZeut').AsString;
    end;
  end;

  UserResponse := ShowMultiDialog(sZeut, OriginalFileAction);

  Screen.Cursor := crHourGlass;
  try
    For i := 0 to (fmMultipleDirectorsChoice.ResultList.Count - 1) do
    begin
      TempUR :=UserResponse;

      if TempUR >= 8 then //save SignName
      begin
        if PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.SignNameNum > 0 then
          History.UndoLastBlank(PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.CompanyNum, faSignName);
        TempUR := TempUR - 8;
      end;
      if TempUR >= 4 then //save person
      begin
        if ((FileAction = faDirectorInfo) and
            (PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.DirectorNum>0)) or
           ((FileAction = faStockHolderInfo) and
            (PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.StockHolderNum >0)) then
          History.UndoLastBlank(PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.CompanyNum, FileAction);
        TempUR:=TempUR-4;
      end;
      if TempUR >= 2 then //manager
      begin
        if PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.ManagerNum >0 then
          History.UndoLastBlank(PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.CompanyNum, faManager);
        TempUR := TempUR-2;
      end;
      if (TempUR = 1) then //other person
        if ((FileAction2 = faDirectorInfo) and
            (PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.DirectorNum>0)) or
           ((FileAction2 = faStockHolderInfo) and
            (PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.StockHolderNum >0)) then
          History.UndoLastBlank(PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.CompanyNum, FileAction2);
    end;

    FileAction := FileAction2;
    sTableName := sTableName2;
  finally
    Screen.Cursor := crDefault;
  end;

  if   fmMultipleDirectorsChoice <> nil then
  begin
    fmMultipleDirectorsChoice.Free;
    fmMultipleDirectorsChoice:=nil;
  end;
end;
//================
end.

