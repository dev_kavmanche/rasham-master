{
HISTORY:
DATE        VERSION   BY   TASK    DESCRIPTION
17/12/2019  1.7.7     VG   19075   code refactoring, fixed DirectoryExists
                                   added procedures splitPlainText, splitEx
24/12/2019  1.7.7     VG   19364   added format routines
}
unit utils2;


interface

//VG19075
uses
  Windows, Classes, Forms, Qrctrls, DBTables, Controls, Db, memdset, Graphics;

type
  TCharSet = set of Char;

const
  EMPTY_DATE='__/__/____';
  EMPTY_PHONE='___-_______';
  EMPTY_MAIL='__________@__________.__';
  EMPTY_MONEY='_.__';

function IsEmptyNumber(const str:string):boolean;
function DoDeleteFile(const filename:string;var error:string):boolean;
function ExtractFileNameFromError(const str:string):string;
function IsLCKError(const error:string):boolean;
function lastPosStr(const sub,str:string):integer;
function GetWinDir: string;
function MakeFullPath(const dir,filename:string):string;
function GetDriveLetters:string;
procedure DeletePdxLockFile(const StartDir,WinDir:string);
function GetDefaultPenWidth(Form:TForm):integer;
procedure PrepareQRShape(Form:TForm;Width:integer);
procedure SetChecked2(lbl: TQRLabel; v: boolean);
procedure split(const s:string;delimiter:char;var sl:TStringList);
//VG19075
procedure splitPlainText(const cTxtToSplit: String;
                           var aLstSplited: TStringList);
//VG19075
procedure splitEx(const cStrToSplit: String;
                        aDelimiters: TCharSet;
                    var aLstSplited: TStringList);
//VG19364
function FormatDoubleFromQRDBText(qrControl: TQRDBText; nDecimals: Integer): String;
function FormatDoubleForQRControl(dValue: Double; nDecimals: Integer): String;
function FormatStr2StrAsDouble(cValue: String; nDecimals: Integer): String;
function DefineAppropriateFontSize(cText: String; qrControl: TQRDBText): Integer;
procedure DefineAppropriateFontSize4QRDBText(aQRDBControl: TQRDBText;
                                            nOrigFontSize: Integer);

function GetCommaTextCount(const s:string):integer;
function GetCommaText(const s:string;index:integer):string;
function FormatAddress(const street,number,city,index,country:string):string;
function FormatInt(const s:string):string;
procedure SetCaption(Parent: TForm; const LabelName, Text: string);
function Str2Float(const s:string):double;
function Str2Int(const s:string):integer;
function GetTableName4SQL(Table:TTable):string;
function ExtractPureFileName(const filename:string):string;
function RichTextToPlainText(const str:string):string;
function lastpos(c:char;const str:string):integer;
function HebNum(n:integer):string;
function FormatString4SQL(const s:string):string;
procedure split3(const source:string;delimiter:char;var s1,s2,s3:string);
function Float2Str(v:Extended;points:integer):string;
function CanFocusOnControl(ctl:TWinControl):boolean;
function GetDisabledParentName(ctl:TWinControl):string;
function FirstDigit(s:string):integer;
function CountDigits(s:string):integer;
function MyWinExec(filename:string):boolean;
function GetLogFileName:string;
function MyDate2Str(dt:TDateTime):string;
function PadWithSpaces(s:string;l:integer):string;
procedure SetChecked(lbl:TQRLabel;IsChecked:boolean;spaces:integer);
function MyStr2Float(num:double;mandatoryPlacesAfterPoint:integer):string;
function GetAgorot(num:double):integer;
function MySubStr(s:string;l:integer):string;
procedure AppendStr2(dataset:TDataSet;str1,str2:string);
procedure AppendStr(dataset:TDataSet;str:string);
function Pad(c:char;len:integer):string;
function EmptyPlace(len:integer):string;
function Space(len:integer):string;
procedure StartTable(ds:TMemDataSet);
function GetBooleanValue(tbl:TDataSet;const FieldName:string):boolean;
function SetBooleanValue(tbl:TDataSet;const FieldName:string;AValue:boolean):boolean;
function GetTitleFontStyle(var value:string):TFontStyles;
function DirectoryExists(const dir:string):boolean;

implementation

//VG19075
uses
  SysUtils, Dialogs, ShellAPI;

function IsEmptyNumber(const str:string):boolean;
var
  str1:string;
  l,i:integer;
begin
  str1:=trim(str);
  l:=length(str1);
  Result:=False;
  for i:=1 to l do
    if (str1[i]<>'0')and(str1[i]<>' ')then
      exit;
  Result:=True;
end;

function DoDeleteFile(const filename:string;var error:string):boolean;
begin
  error:='';
  Result:=True;
  try
    DeleteFile(filename);
  except on E:Exception do
    begin
      Error:=E.Message;
      Result:=False;
    end;
  end;
end;

function ExtractFileNameFromError(const str:string):string;
var
  p,l:integer;
  s:string;
  found:boolean;
begin
  Result:='';
  p:=lastPosStr('File:',str);
  if p<=0 then
    exit;
  s:=Copy(str,p+5,length(str));
  found:=False;
  repeat
    l:=length(s);
    if l>0 then
    begin
      if (s[l]<=#32)or (s[l] in ['.',';',':','?','!']) then
      begin
        found:=True;
        s:=Copy(s,1,l-1);
      end;
    end;
  until (not found)and(l>0);
  Result:=s;
end;

function IsLCKError(const error:string):boolean;
begin
  Result:=(pos('DIRECTORY IS CONTROLLED BY OTHER .NET FILE',uppercase(error))>0);
end;

function lastPosStr(const sub,str:string):integer;
var
  lSub,lStr,i:integer;
begin
  Result:=0;
  lSub:=length(sub);
  lStr:=length(str);
  if lStr<lSub then
    exit;
  for i:=1 to lStr-lSub+1 do
  begin
    if sub=Copy(str,i,lSub)then
    begin
      Result:=i;
      exit;
    end;
  end;
end;

function GetWinDir: string;
var
  dir: array [0..MAX_PATH] of Char;
begin
  GetWindowsDirectory(dir, MAX_PATH);
  Result := StrPas(dir);
end;


function GetDriveLetters:string;
var
  DriveBitmask: set of 0..25;
  Drive: Integer;
  res:string;
begin
  Integer(DriveBitmask) := GetLogicalDrives();
  res:='';
  for Drive := 0 to 25 do
    if Drive in DriveBitmask then
      res:=res+Chr(Drive+Ord('A'));
  Result:=res;
end;

function MakeFullPath(const dir,filename:string):string;
var
  res:string;
  l,p:integer;
begin
  res:=dir;
  l:=length(dir);
  if l>0 then
    if res[l]<>'\' then
      res:=res+'\';
  p:=lastpos('\',filename);
  if p>0 then
    Result:=res+Copy(filename,p+1,length(filename))
  else
    Result:=res+filename;
end;

procedure DeletePdxLockFile(const StartDir,WinDir:string);
var
  error:integer;
  sr:TSearchRec;
  filename:string;
begin
  if (WinDir='')or(StartDir=WinDir)then
    exit;
  if length(StartDir)+12>MAX_PATH then
    exit;
  filename:=MakeFullPath(StartDir,'PDOXUSRS.LCK');
  if FileExists(filename) then
  begin
    try
      DeleteFile(filename);
    except on E:Exception do
      MessageDlg('Could not delete '+filename+#10+#13+E.Message,mtError,[mbOk],0);
    end;
  end;
  error:=FindFirst(MakeFullPath(StartDir,'*.*'),faDirectory,sr);
  while error=0 do
  begin
    if (sr.Name<>'.')and(sr.Name<>'..')and((faDirectory and sr.Attr)>0)then
      DeletePdxLockFile(MakeFullPath(StartDir,sr.Name),WinDir);
    error:=FindNext(sr);
  end;
end;

function GetDefaultPenWidth(Form:TForm):integer;
var
  i:integer;
begin
  Result:=0;
  if Form=nil then
    exit;
  for i:=0 to Form.ComponentCount-1 do
  begin
    if Form.Components[i] is TQRShape then
    begin
      Result:=(Form.Components[i] as TQRShape).Pen.Width;
      exit;
    end;
  end;
end;

procedure PrepareQRShape(Form:TForm;Width:integer);
var
  i:integer;
begin
  if Form<>nil then
    for i:=0 to Form.Componentcount-1 do
      if Form.Components[i] is TQRShape then
        (Form.Components[i] as TQRShape).Pen.Width:=Width;
end;

procedure SetChecked2(lbl: TQRLabel; v: boolean);
begin
  if (lbl = nil) then
    Exit;
  if (v) then
    lbl.Caption := 'x'
  else
    lbl.Caption := '';
end;

procedure split(const s:string;delimiter:char;var sl:TStringList);
var
  i,l:integer;
  part:string;
begin
  sl.Clear;
  l:=length(s);
  part:='';
  for i:=1 to l do
  begin
    if s[i]=delimiter then
    begin
      sl.Add(trim(part));
      part:='';
    end
    else
      part:=part+s[i];
  end;
  if l>0 then
    if s[l]<>delimiter then
      sl.Add(trim(part));
end;

function GetCommaTextCount(const s:string):integer;
var
  sl:TStringList;
begin
  sl:=TStringList.Create;
  split(s,',',sl);
  Result:=sl.Count;
  sl.Free;
end;

function GetCommaText(const s:string;index:integer):string;
var
  sl:TStringList;
  res:string;
  l,l1,start:integer;
begin
  sl:=TStringList.Create;
  split(s,',',sl);
  if sl.Count<=index then
    Result:=''
  else
  begin
    res:=sl.Strings[index];
    l:=length(res);
    l1:=l;
    start:=1;
    if l>0 then
    begin
      if res[1]='"' then
      begin
        inc(start);
        dec(l1);
      end;
      if (res[l]='"') and (l>1)then
        dec(l1);
    end;
    if l1<l then
      res:=Copy(res,start,l1);
    Result:=res;
  end;
  sl.Free;
end;

function FormatAddress(const street,number,city,index,country:string):string;
var
  res,s:string;
begin
  res:=trim(street);
  s:=FormatInt(number);
  if s<>'' then
  begin
    if res<>'' then res:=res+' ';
    res:=res+s;
  end;
  s:=trim(city);
  if s<>'' then
  begin
    if res<>'' then
      res:=res+' ';
    res:=res+s;
  end;
  s:=FormatInt(index);
  if s<>'' then
  begin
    if res<>'' then
      res:=res+' ';
    res:=res+s;
  end;
  s:=trim(country);
  if s<>'' then
  begin
    if res<>'' then
      res:=res+' ';
    res:=res+s;
  end;
  Result:=res;
end;

function FormatInt(const s:string):string;
var
  i,l:integer;
  s1:string;
begin
  s1:=trim(s);
  Result:=s1;
  l:=length(s1);
  for i:=1 to l do
    if s1[i]<>'0' then
      exit;
  Result:='';
end;

procedure SetCaption(Parent: TForm; const LabelName, Text: string);
var
  cmp: TComponent;
begin
  if (Parent = nil) then
    Exit;
  cmp := Parent.FindComponent(LabelName);
  if (cmp = nil) then
    Exit;
  if (cmp is TQRLabel) then
    (cmp as tQRLabel).Caption := Text;
end;

function Str2Float(const s: string): double;
var
  i,l,start:integer;
  IsNegative,AfterPoint:boolean;
  part1,part2,digit,frac:integer;
  res:double;
begin
  Result := 0.0;
  if (s <> '') then
    Result := StrToFloat(s);
  Exit;
  Result:=0.0;
  l:=length(s);
  if l<=0 then
    exit;
  IsNegative:=(s[1]='-');
  if IsNegative then
    start:=2
  else
    start:=1;
  part1:=0;
  part2:=0;
  AfterPoint:=False;
  frac:=1;
  for i:=start to l do
  begin
    case s[i] of
      '.':AfterPoint:=True;
      '0'..'9':begin
                 digit:=ord(s[i])-ord('0');
                 if AfterPoint then
                   part2:=part2*10+digit
                 else
                   part1:=part1*10+digit;
                 if Afterpoint then
                   frac:=frac*10;
               end;
    end;
  end;
  res:=part1+part2/frac;
  if IsNegative then
    res:=-1*res;
  result:=res;
end;

function Str2Int(const s:string):integer;
var
  i,l,start,res:integer;
  IsNegative:boolean;
begin
  Result:=0;
  l:=length(s);
  if l<=0 then
    exit;
  IsNegative:=(s[1]='-');
  if IsNegative then
    start:=2
  else
    start:=1;
  res:=0;
  for i:=start to l do
    if (s[i]>='0')and(s[i]<='9')then
      res:=res*10+ord(s[i])-ord('0');
  if IsNegative then
    res:=-1*res;
  Result:=res;
end;

function IsAllNumeric(const s:string):boolean;
var
  i,l:integer;
begin
  Result:=False;
  l:=length(s);
  if l<=0 then
    exit;
  for i:=1 to l do
    if (s[i]<'0')or(s[i]>'9') then
      exit;
  Result:=True;
end;

function GetMonthNum(const mname:string):integer;
const
  enames:array[1..12] of string=('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC');
  hnames:array[1..12] of string=('���','���','���','���','���','���','���','���','���','���','���','���');
var
  mname1:string;
  i,n,l:integer;
begin
  mname1:=uppercase(trim(mname));
  for i:=1 to 12 do
  begin
    if Copy(mname1,1,length(enames[i]))=enames[i] then
    begin
      Result:=i;
      exit;
    end;
    if Copy(mname1,1,length(hnames[i]))=hnames[i] then
    begin
      Result:=i;
      exit;
    end;
  end;
  n:=0;
  l:=length(mname1);
  for i:=1 to l do
    if (mname1[i]>='0')and(mname1[i]<='9')then
       n:=10*n+ord(mname1[i])-ord('0');
  Result:=n;
end;

function GetTableName4SQL(Table:TTable):string;
var
  p:integer;
begin
  Result:='';
  if Table=nil then
    exit;
  p:=pos('.',Table.TableName);
  if p>0 then
    Result:=Copy(Table.TableName,1,p-1)
  else
    Result:=Table.TableName;
end;

function ExtractPureFileName(const filename:string):string;
var
  pStart,pEnd,i,l:integer;
begin
  l:=length(filename);
  pStart:=0;
  pEnd:=0;
  for i:=l downto 1 do
  begin
    if (filename[i]='.')and(pEnd=0)then
      pEnd:=i;
    if (filename[i]='\')and(pStart=0)then
      pStart:=i;
  end;
  if pEnd=0 then
    pEnd:=l+1;
  Result:=Copy(filename,pStart+1,pEnd-pStart-1);
end;

function RichTextToPlainText(const str:string):string;
var
  res:string;
  i,l:integer;
  bSlash,bBrace,bFirstLetter,bContinue:boolean;
  ch:char;
begin
  bSlash:=False;
  bBrace:=False;
  bFirstLetter:=False;
  l:=length(str);
  res:='';
  for i:=1 to l do
  begin
    bContinue:=False;
    ch:=str[i];
    case ch of
      '\':begin
            bSlash:=True;
            bContinue:=True;
          end;
      ' ':begin
            bSlash:= False;
            if not bFirstLetter then
              bContinue:=True;
          end;
      '{':begin
            bBrace:=True;
            bSlash:=False;
            bContinue:=True;
          end;
      '}':begin
            bSlash:=False;
            bBrace:=False;
            bContinue:=True;
          end;
    end;//case
    if (not bContinue)and (not bSlash)and (not bBrace) then
    begin
      if not bFirstLetter then
        bFirstLetter:=True;
      res:=res+ch;
    end;
  end;
  Result:=res;
end;

function lastpos(c:char;const str:string):integer;
var
  i,l:integer;
begin
  Result:=0;
  l:=length(str);
  for i:=l downto 1 do
  begin
    if str[i]=c then
    begin
      Result:=i;
      exit;
    end;
  end;
end;

function HebNum(n:integer):string;
  function Num2Letter(n:integer):char;
  begin
    case n of
      1..10:Result:=chr(ord('�')+n-1);
      20,30:Result:=chr(ord('�')+n div 10-1);
      40:Result:='�';
      50:Result:='�';
      60,70:Result:=chr(ord('�')+n div 10-6);
      80:Result:='�';
      90,100:Result:=chr(ord('�')+n div 10-9);
      200,300,400:Result:=chr(ord('�')+n div 100-1);
      else Result:=#0;
    end;
  end;

begin
  if n<=0 then
    Result:=''
  else if n<10 then
    Result:=Num2Letter(n)
  else if (n=15) or (n=16) then
    Result:=Num2Letter(9)+Num2Letter(n-9)
  else if n<100 then
    Result:=Num2Letter((n div 10)*10)+HebNum(n mod 10)
  else if n<1000 then
  begin
    if n mod 100=0 then
    begin
      if n<=400 then
        Result:=Num2Letter(n)
      else
        Result:=Num2Letter(400)+HebNum(n-400);
    end
    else
      Result:=HebNum((n div 100)*100)+HebNum(n mod 100);
  end
  else if n<1000000 then
    Result:=HebNum(n div 1000)+#39+' ����� '+HebNum(n mod 1000)
  else
    Result:='';
end;

function FormatString4SQL(const s:string):string;
var
  res:string;
  i,l:integer;
begin
  res:='';
  res:=res+#39;
  l:=length(s);
  for i:=1 to l do
  begin
    if s[i]=#39 then
      res:=res+'\';
    res:=res+s[i];
  end;
  res:=res+#39;
  Result:=res;
end;

procedure split3(const source:string;delimiter:char;var s1,s2,s3:string);
var
  p1,p2,i,l:integer;
begin
  s1:='';
  s2:='';
  s3:='';
  p1:=0;
  p2:=0;
  l:=length(source);
  for i:=1 to l do
  begin
    if source[i]=delimiter then
    begin
      if p1=0 then
        p1:=i
      else
        p2:=i;
    end
    else
    begin
      if p1=0 then
        s1:=s1+source[i]
      else if p2=0 then
        s2:=s2+source[i]
      else
        s3:=s3+source[i];
    end
  end;
end;

function Float2Str(v:Extended;points:integer):string;
var
  IntPart,FloatPart:string;
  f10:Word;
  i:integer;
  fPart:Extended;
begin
  IntPart:=IntToStr(trunc(v));
  if (v=trunc(v))and(points<=0)then
  begin
    Result:=IntPart;
    exit;
  end;
  if points>0 then
  begin
    f10:=1;
    for i:=1 to points do
      f10:=f10*10;
    FloatPart:=IntToStr(round(f10*(v-trunc(v))));
  end
  else
  begin
    fPart:=v-trunc(v);
    i:=1;
    while (fPart<>trunc(fPart))and(i<10)do
    begin
      fPart:=fPart*10;
      inc(i);
    end;
    FloatPart:=IntToStr(round(fPart));
  end;
  Result:=IntPart+'.'+FloatPart;
end;

function CanFocusOnControl(ctl:TWinControl):boolean;
begin
  if ctl=nil then
    Result:=True
  else if (not ctl.Visible)or(not ctl.Enabled) then
    Result:=False
  else
    Result:=CanFocusOnControl(ctl.Parent);
end;

function GetDisabledParentName(ctl:TWinControl):string;
begin
  if ctl=nil then
    Result:='no disabled/invisible control'
  else if not ctl.Visible then
    Result:=ctl.Name+' unvisible'
  else if not ctl.Enabled then
    Result:=ctl.Name+' disabled'
  else
    Result:=GetDisabledParentName(ctl.Parent);
end;

function FirstDigit(s:string):integer;
var
  i,l:integer;
begin
  l:=length(s);
  for i:=1 to l do
  begin
    if (s[i]>='0')and(s[i]<='9') then
    begin
      Result:=ord(s[i])-ord('0');
      exit;
    end;
  end;
  Result:=-1;
end;

function CountDigits(s:string):integer;
var
  i,l,res:integer;
begin
  l:=length(s);
  res:=0;
  for i:=1 to l do
    if (s[i]>='0')and(s[i]<='9') then
      inc(res);
  Result:=res;
end;

function MyWinExec(filename:string):boolean;
begin
  Result:=True;
  if not FileExists(filename)then
  begin
    Result:=False;
    exit;
  end;
  try
    ShellExecute(Application.Handle,'open',PChar(filename), '','',SW_SHOWNORMAL);
  except
    result:=False;
  end;
end;

function GetLogFileName:string;
var
  s:string;
  drive:char;
  dir:string;
begin
  s:=Application.ExeName;
  if length(s)>0 then
    drive:=s[1]
  else
    drive:='C';
  dir:=drive+':\';
  if not DirectoryExists(dir) then
    dir:=ExtractFilePath(s);
  if length(dir)>0 then
  begin
    if dir[length(dir)]<>'\' then
      dir:=dir+'\';
  end
  else
    dir:=dir+'\';
  Result:=dir+'RashLog.txt';
end;

function MyDate2Str(dt:TDateTime):string;
var
  d,m,y:Word;
  res:string;
begin
  DecodeDate(dt,y,m,d);
  if y<50 then
    inc(y,2000)
  else if y<100 then
    inc(y,1900)
  else if y<1000 then
    inc(y,1000)
  else if y>9999 then
    y:=y mod 10000;
  res:='';
  if d<10 then
    res:=res+'0';
  res:=res+IntToStr(d)+'/';
  if m<10 then
    res:=res+'0';
  res:=res+IntToStr(m)+'/'+IntToStr(y);
  Result:=res;
end;

function PadWithSpaces(s:string;l:integer):string;
var
  res:string;
  l1,i:integer;
begin
  res:=trim(s);
  l1:=length(res);
  for i:=l1+1 to l do
    res:=' '+res;
  Result:=res;
end;

procedure SetChecked(lbl:TQRLabel;IsChecked:boolean;spaces:integer);
var
  strPad:string;
begin
  if spaces>0 then
    strPad:=space(spaces)
  else
    strPad:='';

  if IsChecked then
    lbl.Caption:=strPad+'X'+strPad
  else
    lbl.Caption:=strPad+' '+strPad;
end;

function MyStr2Float(num:double;mandatoryPlacesAfterPoint:integer):string;
var
  s1,s2:string;
  intPart:integer;
  floatPart:double;
  floatPartAsInt,l,i:integer;
begin
  intPart:=trunc(num);
  floatPart:=num-intPart;
  s1:=IntToStr(intPart);
  if (floatpart=0)and(mandatoryPlacesAfterPoint<=0)then
  begin
    Result:=s1;
    exit;
  end;
  if mandatoryPlacesAfterPoint>0 then
    for i:=1 to mandatoryPlacesAfterPoint do
      floatPart:=FloatPart*10
  else
    while floatPart<>trunc(floatPart) do
       floatPart:=FloatPart*10;
  floatPartAsInt:=round(floatPart);
  s2:=IntToStr(floatPartAsInt);
  l:=length(s2);
  for i:=l+1 to mandatoryPlacesAfterPoint do
    s2:='0'+s2;
  result:=s1+'.'+s2;
end;

function GetAgorot(num: Double): integer;
begin
  Result := Round(100 *(num - Trunc(num)));
end;

function MySubStr(s:string;l:integer):string;
var
  i:integer;
  res:string;
begin
  if length(s)<=l then
  begin
    Result:=s;
    exit;
  end;
  res:='';
  for i:=1 to l do
    res:=res+s[i];
  Result:=res;
end;

procedure AppendStr2(dataset:TDataSet;str1,str2:string);
begin
  dataset.Append;
  dataset.FieldByName('Str').AsString:=str1;
  dataset.FieldByName('Str2').AsString:=str2;
  dataset.Post;
end;

procedure AppendStr(dataset:TDataSet;str:string);
begin
  dataset.Append;
  dataset.FieldByName('Str').AsString:=str;
  dataset.Post;
end;

function Pad(c: char; len: integer): string;
var
  i: integer;
  res: string;
begin
  res := '';
  for i:= 1 to len do
    res := res + c;
  Result := res;
end;

function EmptyPlace(len: integer): string;
begin
  Result := pad('_', len);
end;

function Space(len: integer): string;
begin
  Result := pad(' ', len);
end;

procedure StartTable(ds:TMemDataSet);
begin
  ds.Close;
  ds.EmptyTable;
  ds.Open;
end;

function GetBooleanValue(tbl:TDataSet;const FieldName:string):boolean;
var
  ok:boolean;
begin
  Result:=False;
  if tbl.FindField(FieldName)=nil then
    exit;
  ok:=true;
  try
    Result:=tbl.FieldByName(FieldName).AsBoolean;
  except
    ok:=False;
  end;
  if ok then
    exit;
  try
    Result:=(tbl.FieldByName(FieldName).AsInteger<>0);
  except
    Result:=False;
  end;
end;

function SetBooleanValue(tbl:TDataSet;const FieldName:string;AValue:boolean):boolean;
var
  ok:boolean;
  v:integer;
begin
  Result:=True;
  if tbl.FindField(FieldName)=nil then
  begin
    Result:=False;
    exit;
  end;
  ok:=True;
  try
    tbl.FieldByName(FieldName).AsBoolean:=AValue;
  except
    ok:=false;
  end;
  if ok then
    exit;
  if AValue then
    v:=1
  else
    v:=0;
  try
    tbl.FieldByName(FieldName).AsInteger:=v;
  except
    Result:=false;
  end;
end;

function GetTitleFontStyle(var value:string):TFontStyles;
var
  p:integer;
begin
  p:=pos('@',value);
  if p>0 then
  begin
    Value:=Copy(Value,1,p-1)+Copy(Value,p+1,length(value));
    Result:=[fsBold];
  end
  else
    Result:=[];
end;

//VG19075
function DirectoryExists(const dir:string):boolean;
var
  sr:TSearchRec;
begin
  try
    Result := (FindFirst(dir, faDirectory, sr) = 0);
  finally
    FindClose(sr);
  end;
end;

//VG19075
procedure splitPlainText(const cTxtToSplit: String;
                           var aLstSplited: TStringList);
begin
  splitEx(cTxtToSplit, [#10,#13], aLstSplited);
end;

//VG19075
procedure splitEx(const cStrToSplit: String;
                        aDelimiters: TCharSet;
                    var aLstSplited: TStringList);
var
  i, nLen : Integer;
  cBuffStr: String;
begin
  aLstSplited.Clear;
  nLen := Length(cStrToSplit);
  cBuffStr := '';

  for i := 1 to nLen do
  begin
    if (cStrToSplit[i] in aDelimiters) then
    begin
      cBuffStr := Trim(cBuffStr);

      if (Length(cBuffStr) > 0) then
        aLstSplited.Add(cBuffStr);

      cBuffStr := '';
    end
    else
      cBuffStr := cBuffStr + cStrToSplit[i];
  end;

  cBuffStr := Trim(cBuffStr);
  if (Length(cStrToSplit) > 0) then
    aLstSplited.Add(cBuffStr);

end;

//VG19364
//Note: Resulted string contains thousand's separators
function FormatDoubleFromQRDBText(qrControl: TQRDBText; nDecimals: Integer): String;
var
  dResult: Double;
  aFld   : TField;
begin
  aFld := qrControl.DataSet.FieldByName(qrControl.DataField);
  dResult := aFld.AsFloat;
  Result := FormatDoubleForQRControl(dResult, nDecimals);
end;

function FormatStr2StrAsDouble(cValue: String; nDecimals: Integer): String;
var
  cTmpValue: String;
  dValue   : Double;
begin
  cTmpValue := Trim(cValue);
  if (Length(cTmpValue) = 0) then
    Result := cTmpValue
  else
  begin
    try
      dValue := StrToFloat(cTmpValue);
      Result := FormatDoubleForQRControl(dValue, nDecimals);
    except
    on e : Exception do
      Result := cTmpValue;
    end;
  end;
end;

function FormatDoubleForQRControl(dValue: Double; nDecimals: Integer): String;
begin
//"1,234,567,890,123.45"
  Result := FloatToStrF(dValue, ffNumber, 15, nDecimals);
end;

function DefineAppropriateFontSize(cText: String; qrControl: TQRDBText): Integer;
var
  nPixels: Integer;
  aFont  : TFont;
begin
  aFont := qrControl.Font;

  repeat
    nPixels := qrControl.ParentReport.TextWidth(aFont, cText);
    if (nPixels >= qrControl.Width) then
      aFont.Size := aFont.Size - 1;
  until (qrControl.Width > nPixels);

  Result := aFont.Size;
end;

procedure DefineAppropriateFontSize4QRDBText(aQRDBControl: TQRDBText;
                                            nOrigFontSize: Integer);
var
  aFont  : TFont;
  cTmpStr: String;
  nSize  : Integer;
begin
  aFont := aQRDBControl.Font;
  if (aFont.Size < nOrigFontSize) then
    aFont.Size := nOrigFontSize;

  cTmpStr := FormatDoubleFromQRDBText(aQRDBControl, 0);

  nSize := DefineAppropriateFontSize(cTmpStr, aQRDBControl);
  if (aFont.Size <> nSize) then
    aFont.Size := nSize;
end;

end.


