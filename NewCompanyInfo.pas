unit NewCompanyInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CompanyInfoTemplate, HebForm, ExtCtrls, Mask, HGrids, SdGrid, StdCtrls,
  HComboBx, HEdit, ComCtrls, Buttons, HLabel;

type
  TfmNewCompanyInfo = class(TfmCompanyInfoTemplate)
  private
    { Private declarations }
  protected
    function EvLoadData: Boolean; override;
  public
    { Public declarations }
  end;

var
  fmNewCompanyInfo: TfmNewCompanyInfo;

implementation

{$R *.DFM}

uses DataObjects, DraftCompany;

function TfmNewCompanyInfo.EvLoadData: Boolean;
var
  FileItem: TFileItem;
begin
  Result:= False;
  FileItem:= fmDraftCompany.Execute(Self);
  If FileItem=Nil Then
    Exit;
  Company.Free;
  Company := TCompany.LoadFromFile(nil, FileItem);
  EvFillData(Company);
  Result:= True;

  FileName:= FileItem.FileInfo.FileName;
  SetFileIsDraft(True, FileItem.FileInfo.CompanyNum);
  FileItem.Free;
end;

end.
