{
History:
DATE        VERSION   BY   TASK      DESCRIPTION
26/12/2019  1.7.7     VG   19364     added procedure Fill3Purposes; code refactoring
21/12/2020  1.8.4     sts  22057     fixed bug with float rounded to integer; refactoriing;
21/02/2021  1.8.5     sts  22432     fixed bug with share value
21/06/2021  1.8.6     sts  23127     changes in report
}
unit PrintRishum;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Qrctrls, QuickRpt, ExtCtrls, DataObjects, StdCtrls;

type
  TfmQRRishum = class(TForm)
    qrRishum1: TQuickRep;
    QRBand2: TQRBand;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    taHon: TTable;
    qrRishum3: TQuickRep;
    QRBand4: TQRBand;
    QRCompositeReport1: TQRCompositeReport;
    QRLabel61: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRImage1: TQRImage;
    QRLabel5: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    edtCompanyName1: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    edtCompanyName2: TQRLabel;
    QRLabel104: TQRLabel;
    edtCompanyName3: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel20: TQRLabel;
    QRShape1: TQRShape;
    QRLabel21: TQRLabel;
    QRShape2: TQRShape;
    QRShape4: TQRShape;
    lblID: TQRLabel;
    QRLabel22: TQRLabel;
    edtLastName: TQRLabel;
    QRLabel24: TQRLabel;
    edtFirstName: TQRLabel;
    edtID: TQRLabel;
    QRShape8: TQRShape;
    QRLabel26: TQRLabel;
    edtMagishPhone: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRShape7: TQRShape;
    edtTaagidNo: TQRLabel;
    QRLabel32: TQRLabel;
    edtTaagidName: TQRLabel;
    QRLabel34: TQRLabel;
    edtMagishCountry: TQRLabel;
    QRShape9: TQRShape;
    QRLabel36: TQRLabel;
    edtMagishCity: TQRLabel;
    QRShape3: TQRShape;
    QRLabel38: TQRLabel;
    edtMagishStreet: TQRLabel;
    QRShape10: TQRShape;
    edtMagishNumber: TQRLabel;
    QRShape11: TQRShape;
    QRLabel41: TQRLabel;
    edtMagishApt: TQRLabel;
    QRLabel42: TQRLabel;
    QRShape12: TQRShape;
    edtMagishIndex: TQRLabel;
    QRShape13: TQRShape;
    QRLabel46: TQRLabel;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRLabel7: TQRLabel;
    l13: TQRLabel;
    l14: TQRLabel;
    edtEzel: TQRLabel;
    edtPOB: TQRLabel;
    l14a: TQRLabel;
    QRLabel8: TQRLabel;
    l12: TQRLabel;
    QRShape16: TQRShape;
    QRLabel9: TQRLabel;
    QRShape17: TQRShape;
    QRLabel93: TQRLabel;
    QRShape18: TQRShape;
    QRLabel12: TQRLabel;
    edtAddressApt: TQRLabel;
    QRShape19: TQRShape;
    QRLabel48: TQRLabel;
    QRShape20: TQRShape;
    QRLabel49: TQRLabel;
    QRShape21: TQRShape;
    QRLabel50: TQRLabel;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    edtCompanyPhone: TQRLabel;
    QRShape24: TQRShape;
    edtCompanyEMail: TQRLabel;
    QRShape25: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape26: TQRShape;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape27: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel118: TQRLabel;
    qrRishum2: TQuickRep;
    QRBand6: TQRBand;
    QRBand8: TQRBand;
    QRShape28: TQRShape;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRBand11: TQRBand;
    QRImage8: TQRImage;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel165: TQRLabel;
    edtSharesCount: TQRLabel;
    Label6: TQRLabel;
    Label7: TQRLabel;
    Label8: TQRLabel;
    edtGrandTotalAmount: TQRLabel;
    Label9: TQRLabel;
    Label10: TQRLabel;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRBand12: TQRBand;
    QRShape41: TQRShape;
    QRLabel176: TQRLabel;
    QRShape42: TQRShape;
    QRLabel177: TQRLabel;
    QRLabel178: TQRLabel;
    QRLabel179: TQRLabel;
    QRShape43: TQRShape;
    QRLabel180: TQRLabel;
    QRLabel181: TQRLabel;
    QRShape44: TQRShape;
    QRLabel182: TQRLabel;
    QRShape45: TQRShape;
    QRLabel183: TQRLabel;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    QRLabel196: TQRLabel;
    QRShape70: TQRShape;
    QRShape71: TQRShape;
    QRLabel197: TQRLabel;
    QRShape72: TQRShape;
    QRShape73: TQRShape;
    QRSubDetail1: TQRSubDetail;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    QRShape50: TQRShape;
    QRShape51: TQRShape;
    QRShape52: TQRShape;
    QRShape53: TQRShape;
    QRShape54: TQRShape;
    QRShape55: TQRShape;
    QRShape56: TQRShape;
    QRShape74: TQRShape;
    QRShape75: TQRShape;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRBand13: TQRBand;
    QRLabel166: TQRLabel;
    QRLabel167: TQRLabel;
    QRLabel186: TQRLabel;
    QRLabel187: TQRLabel;
    edtAllocTotal: TQRLabel;
    QRLabel188: TQRLabel;
    QRLabel189: TQRLabel;
    QRShape57: TQRShape;
    QRLabel190: TQRLabel;
    QRLabel191: TQRLabel;
    QRShape58: TQRShape;
    QRLabel192: TQRLabel;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabel195: TQRLabel;
    QRLabel204: TQRLabel;
    QRLabel205: TQRLabel;
    QRLabel206: TQRLabel;
    QRLabel207: TQRLabel;
    QRLabel208: TQRLabel;
    QRLabel209: TQRLabel;
    QRLabel210: TQRLabel;
    QRShape59: TQRShape;
    QRShape60: TQRShape;
    QRShape61: TQRShape;
    QRShape62: TQRShape;
    QRShape63: TQRShape;
    QRShape64: TQRShape;
    QRShape65: TQRShape;
    QRShape66: TQRShape;
    edtAllocLastName0: TQRLabel;
    edtAllocFirstName0: TQRLabel;
    edtAllocID0: TQRLabel;
    edtAllocState0: TQRLabel;
    edtAllocCity0: TQRLabel;
    edtAllocStreet0: TQRLabel;
    edtAllocNo0: TQRLabel;
    edtAllocZip0: TQRLabel;
    edtAllocLastName1: TQRLabel;
    edtAllocLastName2: TQRLabel;
    edtAllocLastName3: TQRLabel;
    edtAllocFirstName1: TQRLabel;
    edtAllocFirstName2: TQRLabel;
    edtAllocFirstName3: TQRLabel;
    edtAllocID1: TQRLabel;
    edtAllocID2: TQRLabel;
    edtAllocID3: TQRLabel;
    edtAllocState1: TQRLabel;
    edtAllocState2: TQRLabel;
    edtAllocState3: TQRLabel;
    edtAllocCity1: TQRLabel;
    edtAllocCity2: TQRLabel;
    edtAllocCity3: TQRLabel;
    edtAllocStreet1: TQRLabel;
    edtAllocStreet2: TQRLabel;
    edtAllocStreet3: TQRLabel;
    edtAllocNo1: TQRLabel;
    edtAllocNo2: TQRLabel;
    edtAllocNo3: TQRLabel;
    edtAllocZip1: TQRLabel;
    edtAllocZip2: TQRLabel;
    edtAllocZip3: TQRLabel;
    QRShape67: TQRShape;
    QRShape68: TQRShape;
    QRShape69: TQRShape;
    QRShape76: TQRShape;
    QRLabel211: TQRLabel;
    QRLabel212: TQRLabel;
    QRLabel213: TQRLabel;
    QRShape77: TQRShape;
    QRShape78: TQRShape;
    QRShape79: TQRShape;
    QRShape80: TQRShape;
    QRShape81: TQRShape;
    QRShape82: TQRShape;
    QRShape83: TQRShape;
    QRShape84: TQRShape;
    QRLabel214: TQRLabel;
    QRShape87: TQRShape;
    QRShape88: TQRShape;
    QRShape89: TQRShape;
    QRShape90: TQRShape;
    QRShape91: TQRShape;
    edtAllocType0: TQRLabel;
    edtAllocType1: TQRLabel;
    edtAllocType2: TQRLabel;
    edtAllocType3: TQRLabel;
    edtAllocValueSh0: TQRLabel;
    edtAllocValueSh1: TQRLabel;
    edtAllocValueSh2: TQRLabel;
    edtAllocValueSh3: TQRLabel;
    edtAllocValueAg0: TQRLabel;
    edtAllocValueAg1: TQRLabel;
    edtAllocValueAg2: TQRLabel;
    edtAllocValueAg3: TQRLabel;
    QRShape85: TQRShape;
    QRLabel215: TQRLabel;
    QRLabel216: TQRLabel;
    QRLabel217: TQRLabel;
    QRShape86: TQRShape;
    QRShape92: TQRShape;
    QRShape93: TQRShape;
    QRShape94: TQRShape;
    QRShape95: TQRShape;
    QRShape96: TQRShape;
    QRShape97: TQRShape;
    edtAllocSumSh0: TQRLabel;
    edtAllocSumSh1: TQRLabel;
    edtAllocSumSh2: TQRLabel;
    edtAllocSumSh3: TQRLabel;
    edtAllocSumAg0: TQRLabel;
    edtAllocSumAg1: TQRLabel;
    edtAllocSumAg2: TQRLabel;
    edtAllocSumAg3: TQRLabel;
    QRLabel219: TQRLabel;
    QRShape98: TQRShape;
    QRLabel220: TQRLabel;
    QRShape99: TQRShape;
    QRShape100: TQRShape;
    QRShape101: TQRShape;
    QRShape102: TQRShape;
    QRShape103: TQRShape;
    edtAllocCount0: TQRLabel;
    edtAllocCount1: TQRLabel;
    edtAllocCount2: TQRLabel;
    edtAllocCount3: TQRLabel;
    QRLabel225: TQRLabel;
    lblAllocAppendixOld: TQRLabel;
    QRBand14: TQRBand;
    QRShape31: TQRShape;
    QRLabel222: TQRLabel;
    QRLabel223: TQRLabel;
    QRLabel224: TQRLabel;
    qrRishum3a: TQuickRep;
    QRBand15: TQRBand;
    QRImage10: TQRImage;
    QRLabel227: TQRLabel;
    QRLabel228: TQRLabel;
    QRLabel229: TQRLabel;
    QRLabel230: TQRLabel;
    QRLabel233: TQRLabel;
    QRLabel234: TQRLabel;
    QRLabel235: TQRLabel;
    QRLabel236: TQRLabel;
    edtAppendixTotal: TQRLabel;
    QRLabel238: TQRLabel;
    QRLabel239: TQRLabel;
    taRishumHolders: TTable;
    taRishumShares: TTable;
    QRBand16: TQRBand;
    QRBand17: TQRBand;
    QRShape32: TQRShape;
    QRLabel237: TQRLabel;
    QRLabel240: TQRLabel;
    QRLabel241: TQRLabel;
    QRShape33: TQRShape;
    QRLabel242: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel245: TQRLabel;
    QRLabel246: TQRLabel;
    QRLabel247: TQRLabel;
    QRLabel248: TQRLabel;
    QRLabel249: TQRLabel;
    QRLabel250: TQRLabel;
    QRLabel251: TQRLabel;
    QRLabel252: TQRLabel;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRShape39: TQRShape;
    QRShape104: TQRShape;
    QRShape105: TQRShape;
    QRShape106: TQRShape;
    QRShape107: TQRShape;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRSubDetail2: TQRSubDetail;
    QRShape108: TQRShape;
    QRLabel253: TQRLabel;
    QRLabel254: TQRLabel;
    QRLabel255: TQRLabel;
    QRShape109: TQRShape;
    QRShape110: TQRShape;
    QRShape111: TQRShape;
    QRShape112: TQRShape;
    QRShape113: TQRShape;
    QRLabel256: TQRLabel;
    QRShape114: TQRShape;
    QRShape115: TQRShape;
    QRShape116: TQRShape;
    QRLabel257: TQRLabel;
    QRLabel258: TQRLabel;
    QRLabel259: TQRLabel;
    QRShape117: TQRShape;
    QRShape118: TQRShape;
    QRShape119: TQRShape;
    QRShape120: TQRShape;
    QRLabel260: TQRLabel;
    QRShape121: TQRShape;
    QRLabel261: TQRLabel;
    QRShape122: TQRShape;
    QRShape123: TQRShape;
    QRLabel262: TQRLabel;
    QRShape126: TQRShape;
    QRShape127: TQRShape;
    QRShape128: TQRShape;
    QRShape129: TQRShape;
    QRShape130: TQRShape;
    QRShape131: TQRShape;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRBand18: TQRBand;
    QRShape124: TQRShape;
    QRLabel263: TQRLabel;
    QRLabel264: TQRLabel;
    QRLabel265: TQRLabel;
    qrRishum4: TQuickRep;
    QRBand19: TQRBand;
    QRLabel267: TQRLabel;
    QRLabel268: TQRLabel;
    QRLabel269: TQRLabel;
    QRLabel270: TQRLabel;
    QRLabel271: TQRLabel;
    QRLabel273: TQRLabel;
    QRLabel275: TQRLabel;
    QRLabel276: TQRLabel;
    edtOtherInfo: TQRLabel;
    QRLabel278: TQRLabel;
    QRLabel279: TQRLabel;
    QRLabel280: TQRLabel;
    QRLabel281: TQRLabel;
    QRLabel284: TQRLabel;
    QRLabel285: TQRLabel;
    QRLabel286: TQRLabel;
    edtLegalSections: TQRLabel;
    edtSignatorySections: TQRLabel;
    QRLabel288: TQRLabel;
    QRLabel289: TQRLabel;
    edtCEOName: TQRLabel;
    QRShape125: TQRShape;
    QRLabel290: TQRLabel;
    edtCEOID: TQRLabel;
    edtCEOAddress: TQRLabel;
    QRShape132: TQRShape;
    QRShape133: TQRShape;
    QRLabel291: TQRLabel;
    QRLabel292: TQRLabel;
    edtCEOMail: TQRLabel;
    QRShape134: TQRShape;
    QRLabel293: TQRLabel;
    edtCEOPhone: TQRLabel;
    QRShape135: TQRShape;
    QRLabel294: TQRLabel;
    QRLabel295: TQRLabel;
    edtOfficerName: TQRLabel;
    QRShape136: TQRShape;
    QRLabel296: TQRLabel;
    edtOfficerID: TQRLabel;
    QRShape137: TQRShape;
    QRLabel297: TQRLabel;
    edtOfficerPosition: TQRLabel;
    QRShape138: TQRShape;
    QRLabel298: TQRLabel;
    QRLabel299: TQRLabel;
    QRLabel301: TQRLabel;
    QRLabel303: TQRLabel;
    QRLabel304: TQRLabel;
    QRLabel305: TQRLabel;
    QRLabel306: TQRLabel;
    QRLabel308: TQRLabel;
    QRLabel318: TQRLabel;
    QRLabel319: TQRLabel;
    QRShape151: TQRShape;
    QRLabel350: TQRLabel;
    QRLabel351: TQRLabel;
    QRImage12: TQRImage;
    qrRishum5: TQuickRep;
    QRBand10: TQRBand;
    QRLabel322: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel324: TQRLabel;
    QRLabel62: TQRLabel;
    edtStatement2: TQRLabel;
    QRShape153: TQRShape;
    QRLabel64: TQRLabel;
    edtStatementLastName2: TQRLabel;
    QRShape154: TQRShape;
    QRLabel65: TQRLabel;
    edtStatementFirstName2: TQRLabel;
    QRShape155: TQRShape;
    QRLabel66: TQRLabel;
    edtStatementZehut2: TQRLabel;
    QRShape156: TQRShape;
    QRLabel67: TQRLabel;
    QRShape157: TQRShape;
    QRLabel68: TQRLabel;
    edtStatementDate2: TQRLabel;
    QRShape158: TQRShape;
    QRLabel69: TQRLabel;
    edtStatement3: TQRLabel;
    QRShape159: TQRShape;
    QRLabel70: TQRLabel;
    edtStatementLastName3: TQRLabel;
    QRShape160: TQRShape;
    QRLabel71: TQRLabel;
    edtStatementFirstName3: TQRLabel;
    QRShape161: TQRShape;
    QRLabel73: TQRLabel;
    QRShape162: TQRShape;
    edtStatementZehut3: TQRLabel;
    QRLabel75: TQRLabel;
    QRShape163: TQRShape;
    QRLabel77: TQRLabel;
    edtStatementDate3: TQRLabel;
    QRShape164: TQRShape;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRShape165: TQRShape;
    QRLabel85: TQRLabel;
    l21: TQRLabel;
    l20: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    l28: TQRLabel;
    QRLabel90: TQRLabel;
    edtMagishZehut: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel110: TQRLabel;
    edtMagishDate: TQRLabel;
    QRLabel111: TQRLabel;
    QRShape166: TQRShape;
    QRImage6: TQRImage;
    QRShape168: TQRShape;
    QRBand9: TQRBand;
    QRShape152: TQRShape;
    QRLabel72: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel76: TQRLabel;
    QRBand20: TQRBand;
    QRShape167: TQRShape;
    QRLabel89: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRBand5: TQRBand;
    QRLabel45: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel55: TQRLabel;
    QRImage3: TQRImage;
    QRLabel56: TQRLabel;
    QRLabel156: TQRLabel;
    QRLabel157: TQRLabel;
    QRLabel158: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel143: TQRLabel;
    QRShape37: TQRShape;
    edt175_1_List: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    edt175_2_List: TQRLabel;
    QRShape38: TQRShape;
    QRLabel124: TQRLabel;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    QRLabel161: TQRLabel;
    QRImage5: TQRImage;
    QRImage9: TQRImage;
    QRImage11: TQRImage;
    QRImage2: TQRImage;
    QRImage7: TQRImage;
    QRImage4: TQRImage;
    QRLabel201: TQRLabel;
    QRLabel202: TQRLabel;
    qrRishum6: TQuickRep;
    QRBand21: TQRBand;
    QRLabel175: TQRLabel;
    QRLabel203: TQRLabel;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel413: TQRLabel;
    QRLabel414: TQRLabel;
    edtAddress: TQRLabel;
    QRLabel416: TQRLabel;
    edtMail: TQRLabel;
    QRLabel418: TQRLabel;
    edtMobile: TQRLabel;
    QRLabel420: TQRLabel;
    QRLabel421: TQRLabel;
    QRLabel422: TQRLabel;
    QRLabel423: TQRLabel;
    QRLabel424: TQRLabel;
    QRLabel425: TQRLabel;
    QRLabel426: TQRLabel;
    QRLabel427: TQRLabel;
    edtConfirmedCopies: TQRLabel;
    QRLabel429: TQRLabel;
    QRLabel430: TQRLabel;
    edtMoreCopies: TQRLabel;
    QRLabel432: TQRLabel;
    QRLabel433: TQRLabel;
    QRImage13: TQRImage;
    QRShape186: TQRShape;
    QRShape187: TQRShape;
    QRImage16: TQRImage;
    QRBand22: TQRBand;
    QRShape188: TQRShape;
    QRLabel442: TQRLabel;
    QRLabel443: TQRLabel;
    QRLabel444: TQRLabel;
    edtResp_1: TQRLabel;
    edtResp_2: TQRLabel;
    QRShape169: TQRShape;
    QRShape170: TQRShape;
    QRShape171: TQRShape;
    QRShape172: TQRShape;
    QRShape173: TQRShape;
    edtX1: TQRLabel;
    QRShape174: TQRShape;
    edtX2: TQRLabel;
    QRShape175: TQRShape;
    edtX3: TQRLabel;
    QRShape176: TQRShape;
    edtX4: TQRLabel;
    edtX175_1a: TQRLabel;
    edtX175_1b: TQRLabel;
    edtX175_2a: TQRLabel;
    edtX175_2b: TQRLabel;
    QRShape183: TQRShape;
    edtXNoParValue: TQRLabel;
    QRShape184: TQRShape;
    edtXGrandTotal: TQRLabel;
    QRShape185: TQRShape;
    QRShape189: TQRShape;
    edtXAllocNoPar: TQRLabel;
    QRShape190: TQRShape;
    edtXAllocTotal: TQRLabel;
    QRShape191: TQRShape;
    edtXAppendixNoPar: TQRLabel;
    QRShape192: TQRShape;
    edtXAppendixTotal: TQRLabel;
    edtXNoLegal: TQRLabel;
    edtXLegal: TQRLabel;
    edtXSignatory: TQRLabel;
    QRShape193: TQRShape;
    edtXConfirmedCopies: TQRLabel;
    QRShape194: TQRShape;
    edtXMoreCopies: TQRLabel;
    QRShape195: TQRShape;
    QRShape196: TQRShape;
    QRShape197: TQRShape;
    edtLine22a: TQRLabel;
    edtLine22b: TQRLabel;
    edtLine22c: TQRLabel;
    QRShape198: TQRShape;
    QRShape199: TQRShape;
    QRShape200: TQRShape;
    edtLine23a: TQRLabel;
    edtLine23b: TQRLabel;
    edtLine23c: TQRLabel;
    edtLine24a: TQRLabel;
    edtLine24b: TQRLabel;
    edtLine24c: TQRLabel;
    QRShape201: TQRShape;
    QRShape202: TQRShape;
    QRShape203: TQRShape;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRShape204: TQRShape;
    QRShape205: TQRShape;
    QRShape206: TQRShape;
    QRShape207: TQRShape;
    QRShape208: TQRShape;
    QRLabel44: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel243: TQRLabel;
    QRLabel266: TQRLabel;
    QRLabel277: TQRLabel;
    QRLabel226: TQRLabel;
    QRLabel287: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel302: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel320: TQRLabel;
    QRLabel328: TQRLabel;
    QRShape209: TQRShape;
    QRShape210: TQRShape;
    QRLabel330: TQRLabel;
    QRLabel332: TQRLabel;
    edtStatement0: TQRLabel;
    QRShape139: TQRShape;
    QRLabel321: TQRLabel;
    edtStatementLastName0: TQRLabel;
    QRShape140: TQRShape;
    QRLabel323: TQRLabel;
    edtStatementFirstName0: TQRLabel;
    QRShape141: TQRShape;
    QRLabel325: TQRLabel;
    edtStatementZehut0: TQRLabel;
    QRShape142: TQRShape;
    QRLabel327: TQRLabel;
    QRShape143: TQRShape;
    QRLabel329: TQRLabel;
    edtStatementDate0: TQRLabel;
    QRShape144: TQRShape;
    QRLabel331: TQRLabel;
    edtStatement1: TQRLabel;
    QRShape145: TQRShape;
    QRLabel333: TQRLabel;
    edtStatementLastName1: TQRLabel;
    QRShape146: TQRShape;
    QRLabel335: TQRLabel;
    edtStatementFirstName1: TQRLabel;
    QRShape147: TQRShape;
    QRLabel337: TQRLabel;
    QRShape148: TQRShape;
    edtStatementZehut1: TQRLabel;
    QRLabel339: TQRLabel;
    QRShape149: TQRShape;
    QRLabel341: TQRLabel;
    edtStatementDate1: TQRLabel;
    QRShape150: TQRShape;
    QRLabel343: TQRLabel;
    QRLabel345: TQRLabel;
    QRLabel346: TQRLabel;
    QRLabel347: TQRLabel;
    QRLabel348: TQRLabel;
    QRLabel353: TQRLabel;
    QRLabel354: TQRLabel;
    QRLabel355: TQRLabel;
    QRLabel356: TQRLabel;
    QRLabel357: TQRLabel;
    QRLabel358: TQRLabel;
    QRLabel359: TQRLabel;
    QRLabel360: TQRLabel;
    QRLabel389: TQRLabel;
    QRLabel387: TQRLabel;
    QRLabel390: TQRLabel;
    QRLabel388: TQRLabel;
    QRLabel385: TQRLabel;
    QRLabel383: TQRLabel;
    QRLabel386: TQRLabel;
    QRLabel384: TQRLabel;
    QRLabel221: TQRLabel;
    QRLabel218: TQRLabel;
    Label3: TLabel;
    QRLabel173: TQRLabel;
    QRLabel174: TQRLabel;
    Label1: TQRLabel;
    Label2: TQRLabel;
    Label4: TQRLabel;
    QRLabel200: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel272: TQRLabel;
    QRLabel274: TQRLabel;
    lblAllocAppendix: TQRLabel;
    QRShape211: TQRShape;
    QRLabel163: TQRLabel;
    QRShape40: TQRShape;
    QRLabel168: TQRLabel;
    QRLabel170: TQRLabel;
    edt175_3_List: TQRLabel;
    edtX175_3a: TQRLabel;
    edtX175_3b: TQRLabel;
    QRShape212: TQRShape;
    QRLabel115: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel144: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel169: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel199: TQRLabel;
    edtAllocCountAg0: TQRLabel;
    edtAllocCountAg1: TQRLabel;
    edtAllocCountAg2: TQRLabel;
    edtAllocCountAg3: TQRLabel;
    QRShape177: TQRShape;
    QRShape178: TQRShape;
    QRLabel282: TQRLabel;
    QRLabel283: TQRLabel;
    QRShape179: TQRShape;
    QRShape180: TQRShape;
    QRShape181: TQRShape;
    QRShape182: TQRShape;
    QRShape213: TQRShape;
    QRShape214: TQRShape;
    QRShape215: TQRShape;
    QRShape216: TQRShape;
    QRLabel300: TQRLabel;
    QRLabel307: TQRLabel;
    QRLabel309: TQRLabel;
    QRLabel310: TQRLabel;
    QRLabel311: TQRLabel;
    QRLabel312: TQRLabel;
    QRLabel313: TQRLabel;
    QRLabel314: TQRLabel;
    QRLabel315: TQRLabel;
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure taRishumHoldersAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FCompany: TCompany;
    FDecisionDate: TDateTime;
    FTooManyShares: boolean;

    procedure PrintAllocShares(Company: TCompany);
    function TooManyShares(Company: TCompany): boolean;
    procedure CreateRishumTables;
    procedure PrintAppendixShares(Company: TCompany);
    procedure PrintAllocSum(Sum: double);
    procedure CleanCaptions;
    procedure StatementHolders(Company: TCompany);
    //VG19364
    procedure Fill3Purposes(cTxt: String; lblPurpose1, lblPurpose2, lblPurpose3: TQRLabel);
  public
    { Public declarations }
    procedure EmptyData;
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintEmptyProc(DoPrint: Boolean);
    procedure PrintEmpty(Filename: string);
    procedure PrintExecute(Company: TCompany; DecisionDate: TDateTime);
  end;

var
  fmQRRishum: TfmQRRishum;

implementation

{$R *.DFM}

uses
  Util, PrintFirstDirector, PrinterSetup, PrintAddress,
  PrintFirstStockHolder, PrintTakanon, prTakanon, utils2;

const MAX_LINE_LEN=120;

procedure TfmQRRishum.PrintEmptyProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(qrRishum1);
  fmPrinterSetup.FormatQR(qrRishum2);
  fmPrinterSetup.FormatQR(qrRishum3);
  fmPrinterSetup.FormatQR(qrRishum4);
  fmPrinterSetup.FormatQR(qrRishum5);
  fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
  if DoPrint then
    QRCompositeReport1.print
  else
  begin
    // Moshe 23/01/2018
    qrRishum1.Preview;
    Application.ProcessMessages;

    qrRishum2.Preview;
    Application.ProcessMessages;

    qrRishum3.Preview;
    Application.ProcessMessages;

    qrRishum4.Preview;
    Application.ProcessMessages;

    qrRishum5.Preview;
    Application.ProcessMessages;
  end;
end; // TfmQRRishum.PrintEmptyProc

procedure TfmQRRishum.PrintEmpty(Filename: string);
begin
  EmptyData;
  fmPrinterSetup.Execute(FileName, PrintEmptyProc);
end; // TfmQRRishum.PrintEmpty

procedure TfmQRRishum.EmptyData;
var
  I: Integer;
begin
  taHon.FieldDefs.Clear;
  taHon.FieldDefs.Add('Name',         ftString, 40, False);
  taHon.FieldDefs.Add('Value',        ftString, 20, False);
  taHon.FieldDefs.Add('Rashum',       ftString, 20, False);
  taHon.FieldDefs.Add('Mokza',        ftString, 20, False);
  taHon.FieldDefs.Add('TotalValue',   ftString, 20, False);
  taHon.FieldDefs.Add('ValueSh',      ftString, 20, False);
  taHon.FieldDefs.Add('ValueAg',      ftString, 20, False);
  taHon.FieldDefs.Add('TotalValueSh', ftString, 20, False);
  taHon.FieldDefs.Add('TotalValueAg', ftString, 20, False);
  taHon.CreateTable;
  taHon.Open;
  for I := 1 to 4 do
  begin
    taHon.Append;
    taHon.FieldByName('Name').AsString := '';
    taHon.FieldByName('Value').AsString := '';
    taHon.FieldByName('Rashum').AsString := '';
    taHon.FieldByName('Mokza').AsString := '';
    taHon.FieldByName('TotalValue').AsString := '';
    taHon.FieldByName('ValueSh').AsString := '';
    taHon.FieldByName('ValueAg').AsString := '';
    taHon.FieldByName('TotalValueSh').AsString := '';
    taHon.FieldByName('TotalValueAg').AsString := '';
    taHon.Post;
  end;

  for I := 0 To Self.ComponentCount - 1 Do
  begin
    if ((Components[I] is TQRLabel) and (UpperCase(Copy(Components[I].Name, 0,  3)) = 'EDT')) then
      (Components[i] as TQRLabel).Caption := '';
  end;
end; // TfmQRRishum.EmptyData

//VG19364
procedure TfmQRRishum.Fill3Purposes(cTxt: String;
                             lblPurpose1: TQRLabel;
                             lblPurpose2: TQRLabel;
                             lblPurpose3: TQRLabel);
var
  lstSplited: TStringList;
  cBuffStr  : String;
  arrPurposes: array[0..2] of TQRLabel;
  i   : Integer;
  nLen: Integer;
begin
  arrPurposes[0] := lblPurpose1;
  arrPurposes[1] := lblPurpose2;
  arrPurposes[2] := lblPurpose3;
  for i := 0 to 2 do
    arrPurposes[i].Caption := '';
  lstSplited := TStringList.Create;
  try
    cBuffStr := Trim(cTxt);
    if (Length(cBuffStr) > 0) and (cBuffStr <> DefaultTakanon2) then
    begin
      splitPlainText(cBuffStr, lstSplited);
      nLen := lstSplited.Count-1;
      for i := 0 to nLen do
        arrPurposes[i].Caption := lstSplited[i];
    end;
  finally
    lstSplited.Free;
  end;
end;

procedure TfmQRRishum.PrintExecute(Company: TCompany; DecisionDate: TDateTime);
var
  FirstName, LastName: string;
  i: Integer;
  TotalRashum: double;
  TotalValue: double;
  TotalHon: double;
  TotalHonString: string;
  VS, VSh, VAg: string;
  str:string;
begin
  FCompany := Company;
  FDecisionDate := DecisionDate;
  if (Company = nil) then
  begin
    PrintEmpty('');
    Exit;
  end;

  edtCompanyName1.Caption := Company.Name;
  edtCompanyName2.Caption := Company.CompanyInfo.CompanyName2;
  edtCompanyName3.Caption := Company.CompanyInfo.CompanyName3;
  If Company.Magish.Taagid then
  begin
    edtFirstName.Caption := '';
    edtLastName.Caption := '';
    edtID.Caption := '';
    edtTaagidName.Caption := Company.Magish.Name;
    edtTaagidNo.Caption := Company.Magish.Zeut;
  end
  else
  begin
    SplitName(Company.Magish.Name, FirstName, LastName);
    edtFirstName.Caption := FirstName;
    edtLastName.Caption := LastName;
    edtID.Caption := Company.Magish.Zeut;
    edtTaagidName.Caption := '';
    edtTaagidNo.Caption := '';
  end;
  edtMagishCountry.Caption    := Company.Magish.Country;
  edtMagishCity.Caption       := Company.Magish.City;
  edtMagishStreet.Caption     := Company.Magish.Street;
  edtMagishNumber.Caption     := Company.Magish.Number;
  edtMagishApt.Caption        := '';
  edtMagishIndex.Caption      := Company.Magish.Index;
  edtMagishPhone.Caption      := Company.Magish.Phone;
  l12.Caption                 := Company.Address.City;
  l13.Caption                 := Company.Address.Street;
  l14a.Caption                := Company.Address.Number;
  l14.Caption                 := Company.Address.Index;
  edtEzel.Caption             := Company.Address.Ezel;
  edtPOB.Caption              := Company.Address.POB;
  edtAddressApt.Caption       := '';
  edtCompanyEMail.Caption     := Company.CompanyInfo.Mail;
  edtCompanyPhone.Caption     := Company.Address.Phone;

  TotalRashum := 0;
  TotalHon := 0;
  taHon.FieldDefs.Clear;
  taHon.FieldDefs.Add('Name',         ftString, 40, False);
  taHon.FieldDefs.Add('Value',        ftString, 20, False);
  taHon.FieldDefs.Add('Rashum',       ftString, 20, False);
  taHon.FieldDefs.Add('Mokza',        ftString, 20, False);
  taHon.FieldDefs.Add('TotalValue',   ftString, 20, False);
  taHon.FieldDefs.Add('ValueSh',      ftString, 20, False);
  taHon.FieldDefs.Add('ValueAg',      ftString, 20, False);
  taHon.FieldDefs.Add('TotalValueSh', ftString, 20, False);
  taHon.FieldDefs.Add('TotalValueAg', ftString, 20, False);
  taHon.CreateTable;
  taHon.Open;
  Company.HonList.Filter := afActive;
  For i := 0 to Company.HonList.Count - 1 do
  begin
    With Company.HonList.Items[i] as THonItem do
    begin
      TotalRashum := TotalRashum + Rashum;
      TotalValue := Value * Rashum;
      TotalHon := TotalHon + TotalValue;
      taHon.Append;
      taHon.FieldByName('Name').AsString := Name;
      if ((Value - Round(Value)) > 0) then
      begin
        taHon.FieldByName('Value').AsString := FloatToStrF(Value, ffNumber, 15, 4);
      end
      else
      begin
         taHon.FieldByName('Value').AsString := FloatToStrF(Value, ffNumber, 15, 2);
      end;
      VS := FloatToStrF(Value, ffNumber, 15, 2);
      SplitBySep(VS, '.', VSh, VAg);
      taHon.FieldByName('ValueSh').AsString := VSh;
      taHon.FieldByName('ValueAg').AsString := VAg;
      taHon.FieldByName('Rashum').AsString := FloatToStrF(Rashum, ffNumber, 15, 2);  //sts22057
      taHon.FieldByName('Mokza').AsString := FloatToStrF(Mokza, ffNumber, 15,0);
      if ((TotalValue - Round(TotalValue) ) > 0) then
      begin
        taHon.FieldByName('TotalValue').AsString := FloatToStrF(TotalValue, ffNumber, 15,2);
      end
      else
      begin
         taHon.FieldByName('TotalValue').AsString := FloatToStrF(TotalValue, ffNumber, 15, 2);
      end;
      VS := FloatToStrF(TotalValue, ffNumber, 15, 2);
      SplitBySep(VS, '.', VSh, VAg);
      taHon.FieldByName('TotalValueSh').AsString := VSh;
      taHon.FieldByName('TotalValueAg').AsString := iif(VAg = '', '00', VAg); //sts22057
      taHon.Post;
    end;
  end;
  edtSharesCount.Caption := FloatToStrF(TotalRashum, ffNumber, 17, 2);  //sts22057
  edtOtherInfo.Caption := Company.CompanyInfo.OtherInfo;
  SetChecked2(edtResp_1,(Company.CompanyInfo.Option3 = '1'));
  SetChecked2(edtResp_2,(Company.CompanyInfo.Option3 = '3'));
  if Company.CompanyInfo.Option3 = '3' then
    edtOtherInfo.Caption := Company.CompanyInfo.OtherInfo
  else
    edtOtherInfo.Caption := '';

  l20.Caption := Company.CompanyInfo.LawName;
  l21.Caption := Company.Magish.Name;
  edtMagishZehut.Caption := Company.Magish.Zeut;
  edtMagishDate.Caption := DateToStr(Now);
  if (Company.CompanyInfo.LawLicence <> '0') then
    l28.Caption:= Company.CompanyInfo.LawLicence;
  SetChecked2(edtX1,((Company.CompanyInfo.Option1 and 1) > 0));
  SetChecked2(edtX2,((Company.CompanyInfo.Option1 and 2) > 0));
  If ((Company.CompanyInfo.Option1 and 2) > 0) then
    str:=trim(RichTextToPlainText(Company.CompanyInfo.Takanon2_2))
  else
    str:='';
  //VG19364
  Fill3Purposes(str, edtLine22a, edtLine22b, edtLine22c);
  SetChecked2(edtX3,((Company.CompanyInfo.Option1 and 4) > 0));
  If ((Company.CompanyInfo.Option1 and 4) > 0) then
    str:=trim(RichTextToPlainText(Company.CompanyInfo.Takanon2_3))
  else
    str:='';
  //VG19364
  Fill3Purposes(str, edtLine23a, edtLine23b, edtLine23c);
  SetChecked2(edtX4,((Company.CompanyInfo.Option1 and 8)>0));
  If ((Company.CompanyInfo.Option1 and 8) > 0) then
    str:=trim(RichTextToPlainText(Company.CompanyInfo.Takanon2_4))
  else
    str:='';
  //VG19364
  Fill3Purposes(str, edtLine24a, edtLine24b, edtLine24c);
  // Moshe 16/11/2016
  // 175 Chapter
  SetChecked2(edtX175_1a,(Company.CompanyInfo.SharesRestrictIndex=0));
  SetChecked2(edtX175_1b,(Company.CompanyInfo.SharesRestrictIndex=1));
  if Company.CompanyInfo.SharesRestrictIndex=0 then
    edt175_1_List.Caption := Company.CompanyInfo.SharesRestrictChapters
  else
    edt175_1_List.Caption := '';

  SetChecked2(edtX175_2a,(Company.CompanyInfo.DontOfferIndex=0));
  SetChecked2(edtX175_2b,(Company.CompanyInfo.DontOfferIndex=1));
  if Company.CompanyInfo.DontOfferIndex = 0 then
    edt175_2_List.Caption := Company.CompanyInfo.DontOfferChapters
  else
    edt175_2_List.Caption := '';

  SetChecked2(edtX175_3a,(Company.CompanyInfo.ShareHoldersNoIndex = 0));
  SetChecked2(edtX175_3b,(Company.CompanyInfo.ShareHoldersNoIndex = 1));
  if Company.CompanyInfo.ShareHoldersNoIndex = 0 then
    edt175_3_List.Caption := Company.CompanyInfo.ShareHoldersNoChapters
  else
    edt175_3_List.Caption := '';

  SetChecked2(edtXNoParValue, (TotalHon <= 0));
  SetChecked2(edtXGrandTotal, (TotalHon > 0));
  if (TotalHon = 0) then
    edtGrandTotalAmount.Caption := ''
  else
  begin
    TotalHonString := FloatToStrF(TotalHon, ffNumber, 15, 2);  //sts22057
    edtGrandTotalAmount.Caption := TotalHonString;
  end;

  CleanCaptions;
  PrintAllocShares(Company);
  SetChecked2(edtXNoLegal,(Company.CompanyInfo.LimitedSignatoryIndex = 0));
  SetChecked2(edtXLegal,(Company.CompanyInfo.LimitedSignatoryIndex = 1));
  SetChecked2(edtXSignatory,(Company.CompanyInfo.LimitedSignatoryIndex = 2));
  if Company.CompanyInfo.LimitedSignatoryIndex = 1 then
     edtLegalSections.Caption := Company.CompanyInfo.LegalSectionsChapters
  else
     edtLegalSections.Caption:='';
  if Company.CompanyInfo.LimitedSignatoryIndex = 2 then
    edtSignatorySections.Caption := Company.CompanyInfo.SignatorySectionsChapters
  else
    edtSignatorySections.Caption := '';

  // CEO
  if (Company.Managers.Count > 0) then
  begin
    edtCEOName.Caption := Company.Managers.Items[0].Name;
    edtCEOID.Caption := Company.Managers.Items[0].Zeut;
    edtCEOAddress.Caption := TManager(Company.Managers.Items[0]).Address;
    edtCEOMail.Caption := TManager(Company.Managers.Items[0]).Mail;
    edtCEOPhone.Caption := TManager(Company.Managers.Items[0]).Phone;
  end
  else
  begin
    edtCEOName.Caption := '';
    edtCEOID.Caption := '';
    edtCEOAddress.Caption := '';
    edtCEOMail.Caption := '';
    edtCEOPhone.Caption := '';
  end;

  // Authorized Officer
  edtOfficerName.Caption := Company.CompanyInfo.SigName;
  edtOfficerID.Caption := Company.CompanyInfo.SigZeut;
  edtOfficerPosition.Caption := Company.CompanyInfo.SigTafkid;
  StatementHolders(Company);
  edtAddress.Caption := Trim(Company.Address.Street) + ' ' + Trim(Company.Address.Number) + ' ' +
                        Trim(Company.Address.City)   + ' ' + Trim(Company.Address.Index);
  edtMail.Caption := Company.CompanyInfo.Mail;
  edtMobile.Caption := Company.Address.Phone;

  edtConfirmedCopies.Caption := '';
  edtMoreCopies.Caption := '';
  SetChecked2(edtXConfirmedCopies,(Company.CompanyInfo.ConfirmedCopies <> '0'));
  SetChecked2(edtXMoreCopies,(Company.CompanyInfo.MoreCopies <> '0'));
  if (Company.CompanyInfo.ConfirmedCopies <> '0') then
    edtConfirmedCopies.Caption := Company.CompanyInfo.ConfirmedCopies;
  if (Company.CompanyInfo.MoreCopies <> '0') then
    edtMoreCopies.Caption := Company.CompanyInfo.MoreCopies;
end; // TfmQRRishum.PrintExecute

//-------------------------------------------
// Moshe 20/11/2016
//-------------------------------------------
procedure TfmQRRishum.PrintAllocShares(Company: TCompany);
var
  i, j, l: integer;
  V, V1, V2: string;
  T, T1, T2: string;
  cnt, cntFrac, cntDec: string;
  total, sum: double;
begin
  sum := 0;
  l := 0;

  FTooManyShares := TooManyShares(Company);
  if (FTooManyShares) then
  begin
    lblAllocAppendix.Enabled := True;
    PrintAppendixShares(Company);
    Exit;
  end;

  // Loop on stock holders
  for i := 0 to Company.StockHolders.Count-1 do
  begin
    // Stock holder details
    // Moshe 22/05/2019
    if (TStockHolder(Company.StockHolders.Items[i]).Taagid) then
    begin
      V1 := '';
      V2 := TStockHolder(Company.StockHolders.Items[i]).Name;

      case i of
        0:
        begin
          edtAllocLastName0.Left := edtAllocFirstName0.Left;
          edtAllocLastName0.Width := edtAllocLastName0.Width + edtAllocFirstName0.Width + 10;
          edtStatement0.Left := edtStatementFirstName0.Left;
          edtStatement0.Width := edtStatement0.Width * 3 + 40;
        end;
        1:
        begin
          edtAllocLastName1.Left := edtAllocFirstName1.Left;
          edtAllocLastName1.Width := edtAllocLastName1.Width + edtAllocFirstName1.Width + 10;
          edtStatement1.Left := edtStatementFirstName1.Left;
          edtStatement1.Width := edtStatement1.Width * 3 + 40;
        end;
        2:
        begin
          edtAllocLastName2.Left := edtAllocFirstName2.Left;
          edtAllocLastName2.Width := edtAllocLastName2.Width + edtAllocFirstName2.Width + 10;
          edtStatement2.Left := edtStatementFirstName2.Left;
          edtStatement2.Width := edtStatement2.Width * 3 + 40;
        end;
        3:
        begin
          edtAllocLastName3.Left := edtAllocFirstName3.Left;
          edtAllocLastName3.Width := edtAllocLastName3.Width + edtAllocFirstName3.Width + 10;
          edtStatement3.Left := edtStatementFirstName3.Left;
          edtStatement3.Width := edtStatement3.Width * 3 + 40;
        end;
      end;
    end
    else
      SplitName(TStockHolder(Company.StockHolders.Items[i]).Name, V1, V2);

    case i of
      0:
      begin
        edtStatement0.Caption := TStockHolder(Company.StockHolders.Items[i]).Name;
        edtAllocLastName0.Caption := V2;
        edtAllocFirstName0.Caption := V1;
        edtAllocID0.Caption := TStockHolder(Company.StockHolders.Items[i]).Zeut;
        edtAllocState0.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Country;
        edtAllocCity0.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.City;
        edtAllocStreet0.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Street;
        edtAllocNo0.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Number;
        edtAllocZip0.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Index;
      end;

      1:
      begin
        edtStatement1.Caption := TStockHolder(Company.StockHolders.Items[i]).Name;
        edtAllocLastName1.Caption := V2;
        edtAllocFirstName1.Caption := V1;
        edtAllocID1.Caption := TStockHolder(Company.StockHolders.Items[i]).Zeut;
        edtAllocState1.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Country;
        edtAllocCity1.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.City;
        edtAllocStreet1.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Street;
        edtAllocNo1.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Number;
        edtAllocZip1.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Index;
      end;

      2:
      begin
        edtStatement2.Caption := TStockHolder(Company.StockHolders.Items[i]).Name;
        edtAllocLastName2.Caption := V2;
        edtAllocFirstName2.Caption := V1;
        edtAllocID2.Caption := TStockHolder(Company.StockHolders.Items[i]).Zeut;
        edtAllocState2.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Country;
        edtAllocCity2.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.City;
        edtAllocStreet2.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Street;
        edtAllocNo2.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Number;
        edtAllocZip2.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Index;
      end;

      3:
      begin
        edtStatement3.Caption := TStockHolder(Company.StockHolders.Items[i]).Name;
        edtAllocLastName3.Caption := V2;
        edtAllocFirstName3.Caption := V1;
        edtAllocID3.Caption := TStockHolder(Company.StockHolders.Items[i]).Zeut;
        edtAllocState3.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Country;
        edtAllocCity3.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.City;
        edtAllocStreet3.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Street;
        edtAllocNo3.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Number;
        edtAllocZip3.Caption := TStockHolder(Company.StockHolders.Items[i]).Address.Index;
      end;
    end;

    // Loop on stocks
    for j := 0 to TStockHolder(Company.StockHolders.Items[i]).Stocks.Count - 1 do
    begin
      total := TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count
               * TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Value;
      Sum := Sum + Total;

      // Print stocks
      if (l < 4) then
      begin
        V := FloatToStrF(TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Value, ffNumber, 15, 2);
        SplitBySep(V, '.', V1, V2);
        V2 := iif(V2 = '', '00', V2);
        T := FloatToStrF(Total, ffNumber, 15, 2);
        SplitBySep(T, '.', T1, T2);
        T2 := iif(T2 = '', '00', T2);
        cnt := FloatToStrF(TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count, ffNumber, 15, 2); //sts22057
        SplitBySep(cnt, '.', cntDec, cntFrac);
        cntFrac := iif(cntFrac = '', '00', cntFrac);
        case l of
          0:
          begin
            edtAllocType0.Caption := TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Name;
            edtAllocValueSh0.Caption := V1;
            edtAllocValueAg0.Caption := V2;
            edtAllocCount0.Caption := cntDec;     //sts22057
            edtAllocCountAg0.Caption := cntFrac;
            edtAllocSumSh0.Caption := V1;         //sts22432
            edtAllocSumAg0.Caption := V2;         //sts22432
          end;

          1:
          begin
            edtAllocType1.Caption := TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Name;
            edtAllocValueSh1.Caption := V1;
            edtAllocValueAg1.Caption := V2;
            edtAllocCount1.Caption := cntDec;     //sts22057
            edtAllocCountAg1.Caption := cntFrac;
            edtAllocSumSh1.Caption := V1;         //sts22432
            edtAllocSumAg1.Caption := V2;         //sts22432
          end;

          2:
          begin
            edtAllocType2.Caption := TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Name;
            edtAllocValueSh2.Caption := V1;
            edtAllocValueAg2.Caption := V2;
            edtAllocCount2.Caption := cntDec;     //sts22057
            edtAllocCountAg2.Caption := cntFrac;
            edtAllocSumSh2.Caption := V1;
            edtAllocSumAg2.Caption := V2;         //sts22432
          end;                                    //sts22432

          3:
          begin
            edtAllocType3.Caption := TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Name;
            edtAllocValueSh3.Caption := V1;
            edtAllocValueAg3.Caption := V2;
            edtAllocCount3.Caption := cntDec;     //sts22057
            edtAllocCountAg3.Caption := cntFrac;
            edtAllocSumSh3.Caption := V1;         //sts22432
            edtAllocSumAg3.Caption := V2;         //sts22432
          end;
        end; // case
        Inc(l);
      end;
    end;
  end; // for i
  PrintAllocSum(Sum);
end; // TfmQRRishum.PrintAllocShares

function TfmQRRishum.TooManyShares(Company: TCompany): boolean;
var
  i, total: integer;
begin
  Result := True;
  if (Company.StockHolders.Count > 4) then
    Exit;
  total := 0;

  // Loop on stock holders
  for i := 0 to Company.StockHolders.Count - 1 do
  begin
    total := total + TStockHolder(Company.StockHolders.Items[i]).Stocks.Count;
  end;
  Result := (total > 4);
end;

procedure TfmQRRishum.PrintAppendixShares(Company: TCompany);
var
  i, j: integer;
  V, V1, V2: string;
  T, T1, T2: string;
  lTotal, lSum: double;
begin
  lSum := 0;
  CreateRishumTables;

  // Loop on stock holders
  for i := 0 to Company.StockHolders.Count-1 do
  begin
    // Stock holder details
    // Moshe 28/05/2019
    if (TStockHolder(Company.StockHolders.Items[i]).Taagid) then
    begin
      V1 := TStockHolder(Company.StockHolders.Items[i]).Name;
      V2 := '';
    end
    else
      SplitName(TStockHolder(Company.StockHolders.Items[i]).Name, V1, V2);

    taRishumHolders.Append;
    taRishumHolders.FieldByName('Zehut').AsString     := TStockHolder(Company.StockHolders.Items[i]).Zeut;
    taRishumHolders.FieldByName('Name').AsString      := TStockHolder(Company.StockHolders.Items[i]).Name;
    taRishumHolders.FieldByName('LastName').AsString  := V2;
    taRishumHolders.FieldByName('FirstName').AsString := V1;
    taRishumHolders.FieldByName('Country').AsString   := TStockHolder(Company.StockHolders.Items[i]).Address.Country;
    taRishumHolders.FieldByName('City').AsString      := TStockHolder(Company.StockHolders.Items[i]).Address.City;
    taRishumHolders.FieldByName('Street').AsString    := TStockHolder(Company.StockHolders.Items[i]).Address.Street;
    taRishumHolders.FieldByName('Number').AsString    := TStockHolder(Company.StockHolders.Items[i]).Address.Number;
    taRishumHolders.FieldByName('Index').AsString     := TStockHolder(Company.StockHolders.Items[i]).Address.Index;
    taRishumHolders.Post;

    // Loop on stocks
    for j := 0 to TStockHolder(Company.StockHolders.Items[i]).Stocks.Count-1 do
    begin
      lTotal := TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count *
                TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Value;
      lSum := lSum + lTotal;
      V := FloatToStrF(TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Value, ffNumber, 15, 2);
      SplitBySep(V, '.', V1, V2);
      T := FloatToStrF(lTotal, ffNumber, 15, 2);
      SplitBySep(T, '.', T1, T2);

      taRishumShares.Append;
      taRishumShares.FieldByName('Zehut').AsString    := TStockHolder(Company.StockHolders.Items[i]).Zeut;
      taRishumShares.FieldByName('Name').AsString     := TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Name;
      taRishumShares.FieldByName('Value').AsString    := V;
      taRishumShares.FieldByName('ValueSh').AsString  := V1;
      taRishumShares.FieldByName('ValueAg').AsString  := V2;
      taRishumShares.FieldByName('Count').AsString    := FloatToStrF(TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count, ffNumber, 15, 0);
      taRishumShares.FieldByName('SumSh').AsString    := T1;
      taRishumShares.FieldByName('SumAg').AsString    := T2;
      taRishumShares.Post;
    end; // for j
  end; // for i
  taRishumHolders.First;
  taRishumShares.First;
  PrintAllocSum(lSum);
end; // TfmQRRishum.PrintAppendixShares

procedure TfmQRRishum.PrintAllocSum(Sum: double);
begin
  SetChecked2(edtXAllocNoPar,(Sum<=0));
  SetChecked2(edtXAppendixNoPar,(Sum<=0));
  SetChecked2(edtXAllocTotal,(Sum>0));
  SetChecked2(edtXAppendixTotal,(Sum>0));
  if (Sum = 0) then
  begin
    edtAllocTotal.Caption := '';
    edtAppendixTotal.Caption := '';
  end
  else
  begin
    edtAllocTotal.Caption := FloatToStrF(Sum, ffNumber, 15,4);
    edtAppendixTotal.Caption := edtAllocTotal.Caption;
  end;
end;

procedure TfmQRRishum.StatementHolders(Company: TCompany);
var
  i: integer;
  V1, V2: string;
begin
  // Loop on stock holders
  for i := 0 to Company.StockHolders.Count-1 do
  begin
    // Stock holder details

    // Moshe 22/05/2019
    if (TStockHolder(Company.StockHolders.Items[i]).Taagid) then
    begin
      V1 := '';
      V2 := '';
    end
    else
      SplitName(TStockHolder(Company.StockHolders.Items[i]).Name, V1, V2);

    case i of
      0:
      begin
        edtStatementLastName0.Caption := V2;
        edtStatementFirstName0.Caption := V1;
        edtStatementZehut0.Caption := TStockHolder(Company.StockHolders.Items[i]).Zeut;
        edtStatementDate0.Caption := DateToStr(Date);
      end;

      1:
      begin
        edtStatementLastName1.Caption := V2;
        edtStatementFirstName1.Caption := V1;
        edtStatementZehut1.Caption := TStockHolder(Company.StockHolders.Items[i]).Zeut;
        edtStatementDate1.Caption := DateToStr(Date);
      end;

      2:
      begin
        edtStatementLastName2.Caption := V2;
        edtStatementFirstName2.Caption := V1;
        edtStatementZehut2.Caption := TStockHolder(Company.StockHolders.Items[i]).Zeut;
        edtStatementDate2.Caption := DateToStr(Date);
      end;

      3:
      begin
        edtStatementLastName3.Caption := V2;
        edtStatementFirstName3.Caption := V1;
        edtStatementZehut3.Caption := TStockHolder(Company.StockHolders.Items[i]).Zeut;
        edtStatementDate3.Caption := DateToStr(Date);
      end;
    end;
  end;
end; // TfmQRRishum.StatementHolders

procedure TfmQRRishum.PrintProc(DoPrint: Boolean);
var
  i: Integer;
begin
  if (FCompany = nil) then
  begin
    PrintEmptyProc(DoPrint);
    Exit;
  end;
  fmPrinterSetup.FormatQR(qrRishum1);
  fmPrinterSetup.FormatQR(qrRishum2);
  fmPrinterSetup.FormatQR(qrRishum3);
  fmPrinterSetup.FormatQR(qrRishum3a);
  fmPrinterSetup.FormatQR(qrRishum4);
  fmPrinterSetup.FormatQR(qrRishum5);

  If DoPrint then
  begin
    qrRishum1.Print;
    qrRishum2.Print;
    qrRishum3.Print;
    if (FTooManyShares) then
      qrRishum3a.Print;
    qrRishum4.Print;
    qrRishum5.Print;
  end
  else
  begin
    qrRishum1.Preview;
    Application.ProcessMessages;

    qrRishum2.Preview;
    Application.ProcessMessages;

    qrRishum3.Preview;
    Application.ProcessMessages;

    if (FTooManyShares) then
    begin
      qrRishum3a.Preview;
      Application.ProcessMessages;
    end;

    qrRishum4.Preview;
    Application.ProcessMessages;

    qrRishum5.Preview;
    Application.ProcessMessages;
  end;

  for i := 0 to FCompany.Directors.Count-1 do
  begin
    Application.CreateForm(TfmQRFirstDirector, fmQRFirstDirector);
    fmQRFirstDirector.PrintExecute(FCompany, FCompany.Directors.Items[i] as TDirector, FDecisionDate, DoPrint);
    fmQRFirstDirector.Free;
  end;

  Application.CreateForm(TfmQRFirstStockHolder, fmQRFirstStockHolder);
  for i := 0 to FCompany.StockHolders.Count-1 do
    fmQRFirstStockHolder.PrintExecute(FCompany, FCompany.StockHolders.Items[i] as TStockHolder, FDecisionDate, DoPrint);
  fmQRFirstStockHolder.Free;

  Application.CreateForm(TfrmQrTakanon, frmQrTakanon);
  frmQRTakanon.InnerPrintProc(FCompany, DoPrint);
  frmQrTakanon.Free;
end; // TfmQRRishum.PrintProc

procedure TfmQRRishum.QRCompositeReport1AddReports(Sender: TObject);
begin
  QRCompositeReport1.Reports.Add(qrRishum1);
  QRCompositeReport1.Reports.Add(qrRishum2);
  QRCompositeReport1.Reports.Add(qrRishum3);

  if (FTooManyShares) then
    QRCompositeReport1.Reports.Add(qrRishum3a);

  QRCompositeReport1.Reports.Add(qrRishum4);
  QRCompositeReport1.Reports.Add(qrRishum5);
end;

procedure TfmQRRishum.CreateRishumTables;
begin
  try
    taRishumHolders.FieldDefs.Clear;
    taRishumHolders.FieldDefs.Add('Zehut',    ftString, 10, False);
    taRishumHolders.FieldDefs.Add('Name',     ftString, 80, False);
    taRishumHolders.FieldDefs.Add('LastName', ftString, 40, False);
    taRishumHolders.FieldDefs.Add('FirstName',ftString, 40, False);
    taRishumHolders.FieldDefs.Add('Country',  ftString, 20, False);
    taRishumHolders.FieldDefs.Add('City',     ftString, 30, False);
    taRishumHolders.FieldDefs.Add('Street',   ftString, 40, False);
    taRishumHolders.FieldDefs.Add('Number',   ftString,  6, False);
    taRishumHolders.FieldDefs.Add('Index',    ftString,  7, False);
    taRishumHolders.CreateTable;
    taRishumHolders.Open;

    taRishumShares.FieldDefs.Clear;
    taRishumShares.FieldDefs.Add('Zehut',    ftString, 10, False);
    taRishumShares.FieldDefs.Add('Name',     ftString, 30, False);
    taRishumShares.FieldDefs.Add('Value',    ftString, 20, False);
    taRishumShares.FieldDefs.Add('ValueSh',  ftString, 10, False);
    taRishumShares.FieldDefs.Add('ValueAg',  ftString,  2, False);
    taRishumShares.FieldDefs.Add('Count',    ftString, 10, False);
    taRishumShares.FieldDefs.Add('SumSh',    ftString, 10, False);
    taRishumShares.FieldDefs.Add('SumAg',    ftString,  2, False);
    taRishumShares.IndexDefs.Add('', 'Zehut;Name;', [ixPrimary]);
    taRishumShares.CreateTable;
    taRishumShares.Open;
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
end;

procedure TfmQRRishum.CleanCaptions;
var
  i: integer;
begin
  for i := 0 to ComponentCount-1 do
  begin
    if ((Components[i] is TQRLabel) and
      ((UpperCase(Copy(Components[i].Name, 0,  8)) = 'EDTALLOC') or
       (UpperCase(Copy(Components[i].Name, 0, 12)) = 'EDTSTATEMENT'))
      ) then
      TQRLabel(Components[i]).Caption := '';
  end;
end;

procedure TfmQRRishum.taRishumHoldersAfterScroll(DataSet: TDataSet);
begin
  taRishumShares.Filtered := False;
  taRishumShares.Filter := 'Zehut = ''' + taRishumHolders.FieldByName('Zehut').AsString + '''';
  taRishumShares.Filtered := True;
end;

end.

