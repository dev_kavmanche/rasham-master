unit dialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, HMemo, ExtCtrls, HebForm, HLabel, HEdit;

type
  tDialogResult = (drYES, drNO, drCANCEL);
  TfmDialog = class(TForm)
    Notebook: TNotebook;
    Panel1: TPanel;
    Image1: TImage;
    Bevel1: TBevel;
    memo: THebMemo;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    HebForm1: THebForm;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  DialogResult : TDialogResult;

  end;

var
  fmDialog: TfmDialog;

implementation

uses cSTD;
{$R *.DFM}

procedure TfmDialog.Button1Click(Sender: TObject);
begin
  DialogResult := drYES;
  Close;
end;

procedure TfmDialog.Button2Click(Sender: TObject);
begin
  DialogResult := drNO;
  Close;
end;

procedure TfmDialog.FormShow(Sender: TObject);
begin
  If Button1.Visible Then
    Button1.SetFocus
  Else
    If Button3.Visible Then
      Button3.SetFocus;
end;

procedure TfmDialog.Button3Click(Sender: TObject);
begin
  DialogResult:= drCancel;
  Close;
end;

procedure TfmDialog.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
    Button3Click(Sender);
end;

end.
