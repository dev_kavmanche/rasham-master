unit Doh;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HEdit, HRadio, HChkBox, HScrlBox, HListBox, HGrids, SdGrid, DataObjects,
  HComboBx,dbtables, BDE;

type
  TfmDoh = class(TfmBlankTemplate)
    HebScrollBox1: THebScrollBox;
    Shape9: TShape;
    HebLabel4: THebLabel;
    Shape10: TShape;
    bx3: THebCheckBox;
    Panel2: TPanel;
    Shape8: TShape;
    HebLabel5: THebLabel;
    Shape6: TShape;
    Shape7: TShape;
    Panel3: TPanel;
    Shape11: TShape;
    HebLabel6: THebLabel;
    Shape12: TShape;
    Shape13: TShape;
    HebLabel7: THebLabel;
    Shape14: TShape;
    bx4: THebRadioButton;
    bx5: THebRadioButton;
    bx6: THebRadioButton;
    Panel4: TPanel;
    Shape15: TShape;
    HebLabel8: THebLabel;
    Shape16: TShape;
    HebLabel10: THebLabel;
    HebLabel11: THebLabel;
    Shape17: TShape;
    HebLabel12: THebLabel;
    Shape18: TShape;
    HebLabel27: THebLabel;
    bx7: THebRadioButton;
    e15: THebEdit;
    e16: THebEdit;
    bx8: THebRadioButton;
    e17: THebEdit;
    bx9: THebRadioButton;
    pnDohList: TPanel;
    Shape5: TShape;
    HebLabel3: THebLabel;
    lbAnnualReports: THebListBox;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Shape88: TShape;
    Bevel3: TBevel;
    Shape89: TShape;
    HebLabel45: THebLabel;
    Shape87: TShape;
    Shape86: TShape;
    HebLabel44: THebLabel;
    Shape84: TShape;
    HebLabel42: THebLabel;
    Shape85: TShape;
    Shape19: TShape;
    HebLabel9: THebLabel;
    Shape20: TShape;
    HebLabel13: THebLabel;
    Shape21: TShape;
    Shape22: TShape;
    HebLabel14: THebLabel;
    Shape23: TShape;
    Shape24: TShape;
    HebLabel15: THebLabel;
    Shape25: TShape;
    HebLabel16: THebLabel;
    Shape27: TShape;
    Shape28: TShape;
    HebLabel17: THebLabel;
    e23: THebEdit;
    e22: TEdit;
    e21: THebEdit;
    Panel23: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel29: TPanel;
    Grid2: TSdGrid;
    gb51: TPanel;
    gb52: TPanel;
    e26: THebEdit;
    e25: TEdit;
    e24: THebEdit;
    Shape35: TShape;
    bx2: THebCheckBox;
    bx1: THebCheckBox;
    btSearch: TButton;
    Shape40: TShape;
    gbSearch: TPanel;
    HebScrollBox2: THebScrollBox;
    Shape38: TShape;
    Bevel2: TBevel;
    Shape29: TShape;
    Shape34: TShape;
    Shape32: TShape;
    HebLabel22: THebLabel;
    HebLabel18: THebLabel;
    HebLabel20: THebLabel;
    HebLabel21: THebLabel;
    Shape30: TShape;
    HebLabel23: THebLabel;
    HebLabel24: THebLabel;
    Shape31: TShape;
    Shape33: TShape;
    HebLabel25: THebLabel;
    Shape39: TShape;
    HebLabel26: THebLabel;
    Bevel4: TBevel;
    bx31: THebRadioButton;
    bx32: THebRadioButton;
    e31: TMaskEdit;
    pnExec: TPanel;
    e32: THebComboBox;
    Shape41: TShape;
    Shape42: TShape;
    HebLabel28: THebLabel;
    e33: THebComboBox;
    HebLabel29: THebLabel;
    HebLabel30: THebLabel;
    HebLabel31: THebLabel;
    HebLabel34: THebLabel;
    HebLabel35: THebLabel;
    HebLabel36: THebLabel;
    Shape45: TShape;
    HebLabel37: THebLabel;
    Shape46: TShape;
    Shape47: TShape;
    HebLabel38: THebLabel;
    Shape48: TShape;
    HebLabel39: THebLabel;
    Shape43: TShape;
    HebLabel40: THebLabel;
    HebLabel41: THebLabel;
    edLawyer: THebEdit;
    edNameLawyer: THebEdit;
    edIDNumLawyer: THebEdit;
    edLicenseLawyer: THebEdit;
    edLawyerID: THebEdit;
    edLawyerAddress: THebEdit;
    edLAwyer2: THebEdit;
    Shape51: TShape;
    Shape53: TShape;
    Shape44: TShape;
    Shape49: TShape;
    Shape50: TShape;
    Shape52: TShape;
    Shape54: TShape;
    Shape55: TShape;
    HebLabel43: THebLabel;
    HebLabel46: THebLabel;
    HebLabel48: THebLabel;
    HebLabel49: THebLabel;
    HebLabel50: THebLabel;
    Shape56: TShape;
    Shape57: TShape;
    HebLabel51: THebLabel;
    HebLabel52: THebLabel;
    Shape58: TShape;
    Shape59: TShape;
    Shape60: TShape;
    Shape62: TShape;
    Shape63: TShape;
    Shape67: TShape;
    HebLabel53: THebLabel;
    HebLabel54: THebLabel;
    edAccountant: THebEdit;
    edNameAccountant: THebEdit;
    edIDNumAccountant: THebEdit;
    edLicenseAccountant: THebEdit;
    edAccountantID: THebEdit;
    edAccountant2: THebEdit;
    edAccountantAddress: THebEdit;
    Shape68: TShape;
    HebLabel55: THebLabel;
    Shape69: TShape;
    edMail: TEdit;
    Shape70: TShape;
    HebLabel56: THebLabel;
    cmbMemaleRole: THebComboBox;
    procedure bx1Click(Sender: TObject);
    procedure e13Exit(Sender: TObject);
    procedure e13Enter(Sender: TObject);
    procedure pnExecClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gb51Click(Sender: TObject);
    procedure gb52Click(Sender: TObject);
    procedure btSearchClick(Sender: TObject);
    procedure gbSearchClick(Sender: TObject);
    procedure CriticalEditChange(Sender: TObject);
  private
    { Private declarations }
    LawyerRestruct : boolean;
    AccountantRestruct : boolean;
    procedure DeleteEnum(ObjectNum, NewNum: Integer);
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    function LoadLastDohDate: TDateTime;
    procedure RestructDohTables;
  protected
    procedure EvDeleteEmptyRows; override;
    procedure EvPrintData; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvDataLoaded; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    procedure EvTimerOver; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    function EvLoadData: Boolean; override;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;
var
  fmDoh: TfmDoh;

implementation

uses Util, utils2, cSTD, DataM, LoadDialog, dialog2, SaveDialog, DohFull, PrintDoh,
  PeopleSearch, Main,
  HDialogs, PrintAnnr, PrinterSetup, PrintAnnualReport;

{$R *.DFM}

const
  C_REPORT_YEAR = '�� ����� �� ��� ������ ������ �������.';

procedure TfmDoh.LoadEnum(ObjectNum, NewNum: Integer);
var
  TmpStrs: TStringList;
begin
  with DATA.taDoh Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= NewNum;
      If Not GotoKey Then
        Raise Exception.Create('Record not found at TfmDoh.LoadEnum');
      e15.Text:= FieldByName('TEXT4A').AsString;
      e16.Text:= FieldByName('TEXT4B').AsString;
      e17.Text:= FieldByName('TEXT4C').AsString;
      e21.Text:= FieldByName('E21').AsString;
      e22.Text:= FieldByName('E22').AsString;
      e23.Text:= FieldByName('E23').AsString;
      e24.Text:= FieldByName('E24').AsString;
      e25.Text:= FieldByName('E25').AsString;
      e26.Text:= FieldByName('E26').AsString;
      TmpStrs:= TStringList.Create;
      TmpStrs.CommaText:= FieldByName('GRID2COL0').AsString;
      Grid2.RowCount:= TmpStrs.Count;
      Grid2.Cols[0].CommaText:= TmpStrs.CommaText;
      TmpStrs.Free;
      Grid2.Cols[1].CommaText:= FieldByName('GRID2COL1').AsString;
      Grid2.Cols[2].CommaText:= FieldByName('GRID2COL2').AsString;
      Grid2.Cols[3].CommaText:= FieldByName('GRID2COL3').AsString;
      Grid2.Repaint;
      e31.Text:= FieldByName('TEXT31').AsString;
      e32.Text:= FieldByName('E32').AsString;
      e33.Text:= FieldByName('E33').AsString;
      cmbMemaleRole.Text:=FieldByName('MemaleRole').AsString;

      //==>Tsahi 14.6.06: Added Lawyer
      edLawyer.Text := FieldByName('LawyerName').AsString;
      edLawyer2.Text := edLawyer.Text;
      edLawyerID.Text := FieldByName('LawyerID').AsString;
      edLawyerAddress.Text := FieldByName('LawyerAddress').AsString;
      edLicenseLawyer.Text := FieldByName('LawyerLicense').AsString;

      edNameLawyer.Text := e32.Text;
      edIDNumLawyer.Text := e33.Text;
      //<==
      //==>Tsahi 11.9.08: Added Accountant
      edAccountant.Text := FieldByName('AccountantName').AsString;
      edAccountant2.Text := edAccountant.Text;
      edAccountantID.Text := FieldByName('AccountantID').AsString;
      edAccountantAddress.Text := FieldByName('AccountantAddress').AsString;
      edLicenseAccountant.Text := FieldByName('AccountantLicense').AsString;

      edNameAccountant.Text := e32.Text;
      edIDNumAccountant.Text := e33.Text;
      //<==
      // Moshe 28/12/2015
    //  edReportYear.Text := FieldByName('ReportYear').AsString;
      edMail.Text := FieldByName('EMail').AsString;

      bx1.Checked:= (FieldByName('GROUP1').AsString = '0') Or (FieldByName('GROUP1').AsString = '3');
      bx2.Checked:= (FieldByName('GROUP1').AsString = '1') Or (FieldByName('GROUP1').AsString = '3');

      bx3.Checked:= FieldByName('GROUP2').AsString = '1';
      If FieldByName('GROUP3').AsString = '0' Then
         bx4.Checked:= True
      Else
        If FieldByName('GROUP3').AsString = '1' Then
          bx5.Checked:= True
        Else
          bx6.Checked:= True;
      If FieldByName('GROUP4').AsString = '0' Then
        bx7.Checked:= True
      Else
        If FieldByName('GROUP4').AsString = '1' Then
          bx8.Checked:= True
        Else
          bx9.Checked:= True;
      If FieldByName('GROUP31').AsString = '0' Then
        bx31.Checked := True
      Else
        bx32.Checked := True;
    end;
end;

function TfmDoh.EvLoadData: Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
begin
  Result := False;
  RestructDohTables;

  FileItem := fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faDoh);
  if (FileItem = nil) then
    Exit;
  DecisionDate := FileItem.FileInfo.DecisionDate;
  NotifyDate := FileItem.FileInfo.NotifyDate;
  FileItem.EnumChanges(ocCompany, Company.RecNum, acDelete, LoadEnum);

  // 770
  // Moshe 16/01/2019 -  this is a patch
  Application.ProcessMessages;
  ForceDecisionDate(FileItem.FileInfo.DecisionDate);

  FileItem.Free;
  FilesList.Free;
  Result := True;
end;

procedure TfmDoh.DeleteEnum(ObjectNum, NewNum: Integer);
begin
  With Data.taDoh Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= NewNum;
      If Not GotoKey Then
        Raise Exception.Create('Record not found at TfmDoh.DeleteEnum');
      Delete;
    end;
end;

procedure TfmDoh.DeleteFile(FileItem: TFileItem);
begin
  If Not FileItem.FileInfo.Draft Then
    Raise Exception.Create('This file is not Draft at TfmDoh.DeleteFile');
  If (FileItem.FileInfo.Action <> faDoh) Then
    raise Exception.Create('Invalid file type at TfmDoh.DeleteFile');
  FileItem.EnumChanges(ocCompany, Company.RecNum, acDelete, DeleteEnum);
  FileItem.DeleteFile;
end;

function TfmDoh.EvSaveData(Draft: Boolean): Boolean;

  function CollectData: Integer;
  begin
    With DATA.taDoh Do
      begin
        Append;
        FieldByName('E21').AsString:= E21.Text;
        FieldByName('E22').AsString:= E22.Text;
        FieldByName('E23').AsString:= E23.Text;
        FieldByName('E24').AsString:= E24.Text;
        FieldByName('E25').AsString:= E25.Text;
        FieldByName('E26').AsString:= E26.Text;
        FieldByName('TEXT31').AsString:= E31.Text;
        FieldByName('E32').AsString:= E32.Text;
        FieldByName('E33').AsString:= E33.Text;
        FieldByName('MemaleRole').AsString:=cmbMemaleRole.Text;
        FieldByName('TEXT4A').AsString:= e15.Text;
        FieldByName('TEXT4B').AsString:= e16.Text;
        FieldByName('TEXT4C').AsString:= e17.Text;
        FieldByName('GRID2COL0').AsString:= Grid2.Cols[0].CommaText;
        FieldByName('GRID2COL1').AsString:= Grid2.Cols[1].CommaText;
        FieldByName('GRID2COL2').AsString:= Grid2.Cols[2].CommaText;
        FieldByName('GRID2COL3').AsString:= Grid2.Cols[3].CommaText;
        //==>Tsahi 14.6.06: Added Lawyer
        FieldByName('LawyerName').AsString := edLawyer.Text;
        FieldByName('LawyerID').AsString := edLawyerID.Text;
        FieldByName('LawyerAddress').AsString := edLawyerAddress.Text;
        FieldByName('LawyerLicense').AsString := edLicenseLawyer.Text;
        //<==
        //==>Tsahi 11.9.08: Added Accountant
        FieldByName('AccountantName').AsString := edAccountant.Text;
        FieldByName('AccountantID').AsString := edAccountantID.Text;
        FieldByName('AccountantAddress').AsString := edAccountantAddress.Text;
        FieldByName('AccountantLicense').AsString := edLicenseAccountant.Text;
        //<==

        // Moshe 30/12/2015
       // FieldByName('ReportYear').AsInteger := StrToInt(edReportYear.Text);
        FieldByName('EMail').AsString := edMail.Text;

        If bx1.Checked Then
          If bx2.Checked Then
            FieldByName('GROUP1').AsString:= '3'
          Else
            FieldByName('GROUP1').AsString:= '0'
        Else
          If bx2.Checked then
            FieldByName('GROUP1').AsString:= '1'
          Else
            FieldByName('GROUP1').AsString:= '4';

        If bx3.Checked Then
          FieldByName('GROUP2').AsString:= '1'
        Else
          FieldByName('GROUP2').AsString:= '0';
        If bx4.Checked Then
          FieldByName('GROUP3').AsString:= '0'
        Else
          If bx5.Checked Then
            FieldByName('GROUP3').AsString:= '1'
          Else
            FieldByName('GROUP3').AsString:= '2';
        If bx7.Checked Then
          FieldByName('GROUP4').AsString:= '0'
        Else
          If bx8.Checked Then
            FieldByName('GROUP4').AsString:= '1'
          Else
            FieldByName('GROUP4').AsString:= '2';
        If bx31.Checked Then
          FieldByName('GROUP31').AsString:= '0'
        Else
          FieldByName('GROUP31').AsString:= '1';
        Post;
        fmDialog2.Show;
        Result:= FieldByName('RecNum').AsInteger;
      end;
  end;

var
  FileItem: TFileItem;
  FileInfo: TFileInfo;
  FilesList: TFilesList;
begin
  Result := False;
  if CheckErrors(False,True,Draft) then Exit;

  try
    RestructDohTables;
    FilesList := TFilesList.Create(Company.RecNum, faDoh, Draft);
    FileInfo := TFileInfo.Create;
    FileInfo.Action := faDoh;
    FileInfo.Draft := Draft;
    FileInfo.DecisionDate := DecisionDate;
    FileInfo.NotifyDate := NotifyDate;
    FileInfo.CompanyNum := Company.RecNum;
    FileItem := fmSaveDialog.Execute(Self, FilesList, FileInfo);
    if FileItem <> nil then
    begin
      try
        FileName := FileItem.FileInfo.FileName;
        FileItem.SaveChange(ocCompany, Company.RecNum, acDelete, CollectData);
        FileItem.Free;
        fmDialog2.Show;
        Result := True;
      except on E: Exception do
        WriteChangeLog(#9#9#9 + 'Exception Message: ' + E.Message);
      end;
    end;
  finally
    FileInfo.Free;
    FilesList.Free;
  end;
end; // TfmDoh.EvSaveData

procedure TfmDoh.EvTimerOver;
begin
  Inherited;
  e15.Color := clWindow;
  e16.Color := clWindow;
end;

procedure TfmDoh.EvDataLoaded;
var
  FilesList: TFilesList;
  i: Integer;
begin
  inherited;
  Loading := True;
  Company.Directors.Filter := afActive;
  cmbMemaleRole.ItemIndex := 0;
  e32.Items.Clear;
  e33.Items.Clear;
  for i := 0 to Company.Directors.Count-1 do
  begin
    e32.Items.Add(Company.Directors.Items[i].Name);
    e33.Items.Add(Company.Directors.Items[i].Zeut);
  end;
  Grid2.RowCount:= 1;
  Grid2.ResetLine(0);
  Company.Managers.Filter:= afActive;
  for i := 0 To Company.Managers.Count-1 do
  With Company.Managers.Items[i] as TManager Do
    begin
      Grid2.Cells[0,Grid2.RowCount-1] := Name;
      Grid2.Cells[1,Grid2.RowCount-1] := Zeut;
      Grid2.Cells[2,Grid2.RowCount-1] := Address;
      Grid2.Cells[3,Grid2.RowCount-1] := Phone;
      Grid2.RowCount := Grid2.RowCount+1;
    end;
  if Grid2.RowCount > 1 then
    Grid2.RowCount := Grid2.RowCount-1;
  Grid2.Repaint;

  if not DoNotClear then
  begin
    e31.Text := DateToLongStr(Now);
    lbAnnualReports.Clear;
    FilesList := TFilesList.Create(Company.RecNum, faDoh, False);
    try
      for i := 0 to FilesList.Count - 1 do
        lbAnnualReports.Items.Add(DateToLongStr(FilesList.FileInfo[i].DecisionDate));
    finally
      FilesList.Free;
    end;
    bx1.Checked := True;
    bx2.Checked := False;
    bx3.Checked := True;
    bx4.Checked := True;
    bx5.Checked := False;
    bx6.Checked := False;
    bx7.Checked := True;
    bx8.Checked := False;
    bx9.Checked := False;
    bx31.Checked := True;
    bx32.Checked := False;
    e15.Text := fmMain.UserOptions.RoeHeshbon;
    e16.Text := fmMain.UserOptions.RoeAddress;

{      try
      AssignFile(SourceFile, Data.taName.Database.Directory+'Account.dat');
      Reset(SourceFile);
      try
        Readln(SourceFile, S);
        e15.Text:= S;
        Readln(SourceFile, S);
        e16.Text:= S;
      finally
        CloseFile(SourceFile);
      end;
    except
      e15.Text:='';
      e16.Text:='';
    end;}

    e21.Text :='';
    e22.Text :='';
    e23.Text :='';
    e32.ItemIndex := 0;
    e33.ItemIndex := 0;
    e24.Text := Company.CompanyInfo.SigName;
    e25.Text := Company.CompanyInfo.SigZeut;
    e26.Text := Company.CompanyInfo.SigTafkid;

    //==> 13/6/06 Tsahi: Added LAwyer details
    edLawyer.Text   := Company.CompanyInfo.LawName;
    edLawyer2.Text  := edLawyer.Text;
    edLawyerID.Text := Company.CompanyInfo.LawZeut;
    edLawyerAddress.Text := Company.CompanyInfo.LawAddress;
    edLicenseLawyer.Text := Company.CompanyInfo.LawLicence;

    edNameLawyer.Text := e32.Text;
    edIDNumLawyer.Text := e33.Text;

    //==> 11/9/08 Tsahi: Added Accountant details
    edAccountant.Text   := fmMain.UserOptions.RoeHeshbon;
    edAccountant2.Text  := edAccountant.Text;
    edAccountantID.Text := fmMain.UserOptions.RoeIdNum;
    edAccountantAddress.Text := fmMain.UserOptions.RoeAddress;
    edLicenseAccountant.Text := fmMain.UserOptions.RoeLicense;

    edNameAccountant.Text := e32.Text;
    edIDNumAccountant.Text := e33.Text;
  end;
  Loading := False;
end; // TfmDoh.EvDataLoaded

procedure TfmDoh.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
  CreatedDate: TDateTime;
begin
  case Page of
  0: begin
      CreatedDate := Company.CreatedDate;
      if ((CreatedDate > 0) and (CreatedDate > StrToDate(e13.Text))) then
      begin
        ErrorAt := e13;
        raise Exception.Create('����� ����� ����� - ���� ����� �����.');
      end;
      if bx7.Checked then
      begin
        CheckEdit(Silent, e15);
        CheckEdit(Silent, e16);
      end
      else if bx8.Checked then
      begin
        CheckEdit(Silent, e17);
      end;
     end;
  1: CheckGrid(Page, Silent, Extended, Grid2);
  end;
end;

procedure TfmDoh.bx1Click(Sender: TObject);
begin
  inherited;
  If Not Loading Then
    DataChanged := True;
end;

procedure TfmDoh.EvFocusNext(Sender: TObject);
begin
  If Sender = e14 then e13.SetFocus; // Changed Order
  if Sender = e13 then
    case PageIndex Of
      0: bx1.SetFocus;
      1: e21.SetFocus;
      2: e31.SetFocus;
    end;
  If Sender = e15 Then e16.SetFocus;
  If Sender = e17 Then pnB1Click(pnB2);
  if Sender = e21 then e22.SetFocus;
  if Sender = e22 then e23.SetFocus;
  if Sender = e23 then e24.SetFocus;
  if Sender = e24 then e25.SetFocus;
  if Sender = e25 then e26.SetFocus;
  if Sender = e26 then Grid2.SetFocus;
  if Sender = e31 then e32.SetFocus;
  if Sender = e32 then e33.SetFocus;
  if Sender = e33 then cmbMemaleRole.SetFocus;

  if (Sender = bx1) or (Sender = bx2) Then bx3.SetFocus;
  if Sender = bx3 then bx4.SetFocus;
  if (Sender is THebRadioButton) Then
    With (Sender as THebRadioButton) Do
      case Tag Of
        2: bx7.SetFocus;
        3: pnB1Click(pnB2);
        4: e31.SetFocus;
      end;
end;

procedure TfmDoh.e13Exit(Sender: TObject);
begin
  inherited;
  pnDohList.Visible := False;
end;

procedure TfmDoh.e13Enter(Sender: TObject);
begin
  inherited;
  pnDohList.Visible := True;
end;

function TfmDoh.LoadLastDohDate: TDateTime;
var
  i: Integer;
begin
  Result := EncodeDate(1900, 01, 01);
  for i := 0 to lbAnnualReports.Items.Count - 1 do
  begin
    if (Result < StrToDate(lbAnnualReports.Items[i])) and
       (StrToDate(lbAnnualReports.Items[i]) < DecisionDate) then
      Result := StrToDate(lbAnnualReports.Items[i]);
  end;
end;

procedure TfmDoh.pnExecClick(Sender: TObject);
var
  TempCompany: TCompany;
begin
  inherited;
  if CheckErrors(False, False, False) Then
    Exit;
  Application.CreateForm(TfmDohFull, fmDohFull);
  TempCompany:= TCompany.Create(Company.RecNum, DecisionDate+1, False);
  WindowState:= wsMinimized;
  fmDohFull.ShowDoh(TempCompany, LoadLastDohDate);
  TempCompany.Free;
end;

procedure TfmDoh.FormCreate(Sender: TObject);
begin
  FileAction:= faDoh;
  ShowStyle:= afActive;
  inherited;
end;

procedure TfmDoh.EvPrintData;
var
  A: TAnnualReportData;
  lCompany: TCompany;
begin
  if CheckErrors(False, True, False) then
    Exit;

  A := TAnnualReportData.Create;

  A.DirectorsChanged := bx1.Checked;
  A.DirectorsNotChanged := bx2.Checked;
  A.MusmakhName := e24.Text;
  A.MusmakhZeut := e25.Text;
  A.MusmakhRole := e26.Text;
  A.ReportsApproved := bx3.Checked;
  A.ReportsDisplayed := bx4.Checked;
  A.DoesNotHaveAnnualAssembly := bx5.Checked;
  A.DoesNotHaveToMakeReports := bx6.Checked;
  A.ThereIsAnAccountant := bx7.Checked;
  A.AccountantNameIs := bx7.Checked;
  A.NoAccountant := bx8.Checked;
  A.NonActive := bx9.Checked;
  A.AccountantName := e15.Text;
  A.AccountantAddress := e16.Text;
  A.DaysWithoutAccountant := Str2Int(e17.Text);
  A.FManagerName := Grid2.Cols[0].CommaText;
  A.FManagerID := Grid2.Cols[1].CommaText;
  A.FManagerAddress := Grid2.Cols[2].CommaText;
  A.FManagerPhone := Grid2.Cols[3].CommaText;
  A.MemuneName := e21.Text;
  A.MemuneID := e22.Text;
  A.MemuneRole := e23.Text;
  A.ReportBrought := bx31.Checked;
  A.ReportSent := bx32.Checked;
  A.ReportSentAt := StrToDate(e31.Text);
  A.MatzhirName := e32.Text;
  A.MatzhirID := e33.Text;
  A.MatzhirRole := cmbMemaleRole.Text;
  A.AdvName := edLawyer.Text;
  A.AdvSignName := edNameLawyer.Text;
  A.AdvSignID := edIDNumLawyer.Text;
  A.AdvMeametName := edLAwyer2.Text;
  A.AdvMeametAddress := edLawyerAddress.Text;
  A.AdvMeametID := edLawyerID.Text;
  A.AdvMeametLicence := edLicenseLawyer.Text;
  A.AccName := edAccountant.Text;
  A.AccSignName := edNameAccountant.Text;
  A.AccSignID := edIDNumAccountant.Text;
  A.AccMeametName := edAccountant2.Text;
  A.AccMeametAddress := edAccountantAddress.Text;
  A.AccMeametID := edAccountantID.Text;
  A.AccMeametLicence := edLicenseAccountant.Text;
  A.NotifyDate := NotifyDate;
  A.DecisionDate := DecisionDate;
  A.LastDoh := LoadLastDohDate;

  lCompany := TCompany.Create(Company.RecNum, DecisionDate, False);
  Application.CreateForm(TQRAnnualReport, QRAnnualReport);
  try
    QRAnnualReport.PrintExecute('��"� ����', lCompany, A);
  finally
    QRAnnualReport.Free;
    A.Free;
    lCompany.Free;
  end;
end; // TfmDoh.EvPrintData

procedure TfmDoh.FormShow(Sender: TObject);
begin
  inherited;
  e14.SetFocus;
end;

procedure TfmDoh.gb51Click(Sender: TObject);
begin
  inherited;
  Grid2.LineDelete;
  DataChanged := True;
end;

procedure TfmDoh.gb52Click(Sender: TObject);
begin
  inherited;
  Grid2.LineInsert;
  DataChanged := True;
end;

procedure TfmDoh.EvDeleteEmptyRows;
var
  I: Integer;
begin
  With Grid2 do
    For I:= RowCount-1 downto 0 do
      If (RemoveSpaces(Cells[0,I],rmBoth)='') and CheckNumberField(Cells[1,I]) and (RemoveSpaces(Cells[2,I],rmBoth)='') then
        LineDeleteByIndex(I);      
end;

procedure TfmDoh.btSearchClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= TCompany.Create(Company.RecNum, Company.ShowingDate, False);
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
    begin
      e21.SetFocus;
      Exit;
    end;

  Loading:= True;
  If FResult is TStockHolder Then
    With FResult as TStockHolder do
      begin
        e21.Text:= Name;
        e22.Text:= Zeut;
      end;

  if FResult is TDirector Then
    With FResult as TDirector do
      begin
        e21.Text:= Name;
        e22.Text:= Zeut;
      end;
  TempCompany.Free;
  Loading:= False;
  DataChanged := True;
  e21.SetFocus;
end;

procedure TfmDoh.gbSearchClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= TCompany.Create(Company.RecNum, Company.ShowingDate, False);
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
    begin
      Grid2.SetFocus;
      Exit;
    end;

  Loading:= True;
  If FResult is TStockHolder Then
    With FResult as TStockHolder do
      begin
        If Grid2.Cells[0,Grid2.RowCount-1] <> '' Then
          Grid2.RowCount:= Grid2.RowCount+1;
        Grid2.Cells[0,Grid2.RowCount-1]:= Name;
        Grid2.Cells[1,Grid2.RowCount-1]:= Zeut;
        Grid2.Cells[2,Grid2.RowCount-1]:= Address.Street+' '+Address.Number+' '+ Address.City+' '+Address.Index+' '+Address.Country;
        Grid2.Cells[3,Grid2.RowCount-1]:= '';
      end;

  If FResult is TDirector Then
    With FResult as TDirector do
      begin
        If Grid2.Cells[0,Grid2.RowCount-1] <> '' Then
          Grid2.RowCount:= Grid2.RowCount+1;
        Grid2.Cells[0,Grid2.RowCount-1]:= Name;
        Grid2.Cells[1,Grid2.RowCount-1]:= Zeut;
        Grid2.Cells[2,Grid2.RowCount-1]:= Address.Street+' '+Address.Number+' '+ Address.City+' '+Address.Index+' '+Address.Country;
        Grid2.Cells[3,Grid2.RowCount-1]:= Address.Phone;
      end;

  TempCompany.Free;
  Loading:= False;
  DataChanged := True;
  Grid2.SetFocus;
end;

//==============================================================================
procedure TfmDoh.CriticalEditChange(Sender: TObject);
begin
  inherited;
  if (Sender = e32) And (e33.ItemIndex<>-1) Then
    e33.ItemIndex:= e32.ItemIndex;
  if (Sender = e33) And (e32.ItemIndex<>-1) Then
    e32.ItemIndex:= e33.ItemIndex;
  edNameLawyer.Text := e32.Text;
  edIDNumLawyer.Text := e33.Text;
  edNameAccountant.Text  := e32.Text;
  edIDNumAccountant.Text := e33.Text;
end;

//==============================================================================
procedure TfmDoh.RestructDohTables;
var
  NewField : ChangeRec;
  ATable   : TTable;
begin
  Screen.Cursor :=crHourglass;
//  ATable := TTable.Create( Self );
  ATable :=Data.taDoh;
  LawyerRestruct := false;
  AccountantRestruct := false;

  try
    ATable.Close;
{    ATable.DatabaseName := 'Rasham';
    ATable.TableName := Data.taDoh.TableName;}

    BackupTable(ATable.TableName);

    ATable.FieldDefs.Update;
    ATable.Exclusive:= True;
    ATable.Open;

    if ATable.FieldDefs.IndexOf( 'LawyerName' ) = -1 then
    begin
        NewField.szName:='LawyerName';
        NewField.iLength:=40;
        NewField.iType:= fldPDXCHAR;
        NewField.iSubType:= 0;
        NewField.iPrecision:= 0;
        AddField(ATable, NewField);
        LawyerRestruct := True;
    end;

    if ATable.FieldDefs.IndexOf( 'LawyerAddress' ) = -1 then
    begin
        NewField.szName:='LawyerAddress';
        NewField.iLength:=100;
        NewField.iType:= fldPDXCHAR;
        NewField.iSubType:= 0;
        NewField.iPrecision:= 0;
        AddField(ATable, NewField);
    end;

    if ATable.FieldDefs.IndexOf( 'LawyerID' ) = -1 then
    begin
        NewField.szName:='LawyerID';
        NewField.iLength:=10;
        NewField.iType:= fldPDXCHAR;
        NewField.iSubType:= 0;
        NewField.iPrecision:= 0;
        AddField(ATable, NewField);
    end;

    if ATable.FieldDefs.IndexOf( 'LawyerLicense' ) = -1 then
    begin
        NewField.szName:='LawyerLicense';
        NewField.iLength:=20;
        NewField.iType:= fldPDXCHAR;
        NewField.iSubType:= 0;
        NewField.iPrecision:= 0;
        AddField(ATable, NewField);
    end;

    if LawyerRestruct then
    begin
      ATable.First;
      While not ATable.EOF do
      begin
        ATable.Edit;
        ATable.FieldByName('LawyerName').AsString := Company.CompanyInfo.LawName;
        ATable.FieldByName('LawyerAddress').AsString := Company.CompanyInfo.LawAddress;
        ATable.FieldByName('LawyerID').AsString := Company.CompanyInfo.LawZeut ;
        ATable.FieldByName('LawyerLicense').AsString := Company.CompanyInfo.LawLicence;
        ATable.Next;
      end;
    end;

    //==> Tsahi 11.9.08: Added Accountant fields
    if ATable.FieldDefs.IndexOf( 'AccountantName' ) = -1 then
    begin
        NewField.szName:='AccountantName';
        NewField.iLength:=40;
        NewField.iType:= fldPDXCHAR;
        NewField.iSubType:= 0;
        NewField.iPrecision:= 0;
        AddField(ATable, NewField);
        AccountantRestruct := True;
    end;

    if ATable.FieldDefs.IndexOf( 'AccountantAddress' ) = -1 then
    begin
        NewField.szName:='AccountantAddress';
        NewField.iLength:=100;
        NewField.iType:= fldPDXCHAR;
        NewField.iSubType:= 0;
        NewField.iPrecision:= 0;
        AddField(ATable, NewField);
    end;

    if ATable.FieldDefs.IndexOf( 'AccountantID' ) = -1 then
    begin
        NewField.szName:='AccountantID';
        NewField.iLength:=10;
        NewField.iType:= fldPDXCHAR;
        NewField.iSubType:= 0;
        NewField.iPrecision:= 0;
        AddField(ATable, NewField);
    end;

    if ATable.FieldDefs.IndexOf( 'AccountantLicense' ) = -1 then
    begin
        NewField.szName:='AccountantLicense';
        NewField.iLength:=20;
        NewField.iType:= fldPDXCHAR;
        NewField.iSubType:= 0;
        NewField.iPrecision:= 0;
        AddField(ATable, NewField);
    end;

    if AccountantRestruct then
    begin
      ATable.First;
      While not ATable.EOF do
      begin
        ATable.Edit;
        ATable.FieldByName('AccountantName').AsString := Company.CompanyInfo.LawName;
        ATable.FieldByName('AccountantAddress').AsString := Company.CompanyInfo.LawAddress;
        ATable.FieldByName('AccountantID').AsString := Company.CompanyInfo.LawZeut ;
        ATable.FieldByName('AccountantLicense').AsString := Company.CompanyInfo.LawLicence;
        ATable.Next;
      end;
    end;
    //<== Tsahi 11.9.08

    // ++ Moshe 04/01/2016
    if (ATable.FieldDefs.IndexOf( 'ReportYear' ) = -1) then
    begin
        NewField.szName := 'ReportYear';
        NewField.iLength := 0;
        NewField.iType := fldPDXLONG;
        NewField.iSubType := 0;
        NewField.iPrecision := 0;
        AddField(ATable, NewField);
    end;

    if ATable.FieldDefs.IndexOf( 'EMail' ) = -1 then
    begin
        NewField.szName := 'Email';
        NewField.iLength := 100;
        NewField.iType := fldPDXCHAR;
        NewField.iSubType := 0;
        NewField.iPrecision := 0;
        AddField(ATable, NewField);
    end;
    // --

  finally
    ATable.Close;
    ATable.Exclusive:= False;
    ATable.Open;
    Screen.Cursor :=crDefault;
  end;
end; // TfmDoh.RestructDohTables

//==============================================================================
// Moshe Levy 27/12/2015
{function TfmDoh.CheckReportYear: boolean;
var
  lYear: string;
  nYear: integer;
begin
  Result := False;
  try
    lYear := edReportYear.Text;
    if (Length(lYear) <> 4) then Exit;
    nYear := 0;
    try
      nYear := StrToInt(lYear);
    except end;
    if ((nYear < 2000) or (nYear > 2999)) then Exit;
    Result := True;
  finally
    if (not Result) then begin
      HebMessageDlg(C_REPORT_YEAR, mtError, [mbOk], 0);
      PageControl.ActivePage := TabSheet3;
      HebScrollBox2.ScrollInView(edReportYear);
      edReportYear.SetFocus;
    end;
  end;
end;}

//==============================================================================
//==============================================================================
//==============================================================================
end.

