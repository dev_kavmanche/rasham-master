unit PrintDirectors;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, DataObjects;

type
  TfmQRDirectors = class(TForm)
    qrDirectors1Old: TQuickRep;
    QRBand1: TQRBand;
    QRShape5: TQRShape;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    l1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel23: TQRLabel;
    QRShape25: TQRShape;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRShape31: TQRShape;
    l3: TQRLabel;
    l12: TQRLabel;
    l21: TQRLabel;
    QRShape6: TQRShape;
    QRLabel9: TQRLabel;
    QRShape7: TQRShape;
    l2: TQRLabel;
    QRShape45: TQRShape;
    QRBand2: TQRBand;
    QRLabel8: TQRLabel;
    QRShape2: TQRShape;
    l4: TQRLabel;
    l13: TQRLabel;
    l22: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    l5: TQRLabel;
    l14: TQRLabel;
    l23: TQRLabel;
    QRShape11: TQRShape;
    QRLabel18: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape10: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    l6: TQRLabel;
    l15: TQRLabel;
    l24: TQRLabel;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRLabel33: TQRLabel;
    QRShape17: TQRShape;
    QRLabel27: TQRLabel;
    l7: TQRLabel;
    l16: TQRLabel;
    l25: TQRLabel;
    QRShape18: TQRShape;
    QRLabel34: TQRLabel;
    l8: TQRLabel;
    l17: TQRLabel;
    l26: TQRLabel;
    QRShape19: TQRShape;
    QRLabel41: TQRLabel;
    l9: TQRLabel;
    l18: TQRLabel;
    l27: TQRLabel;
    l10: TQRLabel;
    l19: TQRLabel;
    l28: TQRLabel;
    QRLabel49: TQRLabel;
    l31: TQRLabel;
    l30: TQRLabel;
    l32: TQRLabel;
    l33: TQRLabel;
    l34: TQRLabel;
    l35: TQRLabel;
    l36: TQRLabel;
    l37: TQRLabel;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape26: TQRShape;
    l40: TQRLabel;
    l39: TQRLabel;
    l41: TQRLabel;
    l42: TQRLabel;
    l43: TQRLabel;
    l44: TQRLabel;
    l45: TQRLabel;
    l46: TQRLabel;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    l49: TQRLabel;
    l48: TQRLabel;
    l50: TQRLabel;
    l51: TQRLabel;
    l52: TQRLabel;
    l53: TQRLabel;
    l54: TQRLabel;
    l55: TQRLabel;
    QRShape30: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    l58: TQRLabel;
    l57: TQRLabel;
    l59: TQRLabel;
    l60: TQRLabel;
    l61: TQRLabel;
    l62: TQRLabel;
    l63: TQRLabel;
    l64: TQRLabel;
    QRLabel93: TQRLabel;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    l11: TQRLabel;
    l20: TQRLabel;
    l29: TQRLabel;
    QRShape40: TQRShape;
    l38: TQRLabel;
    QRShape41: TQRShape;
    QRShape42: TQRShape;
    l47: TQRLabel;
    QRShape43: TQRShape;
    l56: TQRLabel;
    QRShape44: TQRShape;
    l65: TQRLabel;
    QRLabel97: TQRLabel;
    QRShape63: TQRShape;
    QRLabel103: TQRLabel;
    QRLabel113: TQRLabel;
    QRShape64: TQRShape;
    QRLabel32: TQRLabel;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    l66: TQRLabel;
    l68: TQRLabel;
    l70: TQRLabel;
    QRShape50: TQRShape;
    QRShape51: TQRShape;
    l67: TQRLabel;
    l69: TQRLabel;
    l71: TQRLabel;
    QRLabel57: TQRLabel;
    l73: TQRLabel;
    l72: TQRLabel;
    QRShape52: TQRShape;
    QRShape53: TQRShape;
    l75: TQRLabel;
    l74: TQRLabel;
    QRShape54: TQRShape;
    l77: TQRLabel;
    l76: TQRLabel;
    QRShape55: TQRShape;
    l79: TQRLabel;
    l78: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel108: TQRLabel;
    QRShape56: TQRShape;
    QRShape57: TQRShape;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRShape58: TQRShape;
    QRShape59: TQRShape;
    QRShape60: TQRShape;
    QRShape61: TQRShape;
    l80: TQRLabel;
    l88: TQRLabel;
    l96: TQRLabel;
    QRShape62: TQRShape;
    QRLabel116: TQRLabel;
    QRShape65: TQRShape;
    l81: TQRLabel;
    l89: TQRLabel;
    l97: TQRLabel;
    QRLabel120: TQRLabel;
    QRShape66: TQRShape;
    QRShape67: TQRShape;
    QRShape68: TQRShape;
    QRShape69: TQRShape;
    l82: TQRLabel;
    l90: TQRLabel;
    l98: TQRLabel;
    QRShape70: TQRShape;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel132: TQRLabel;
    QRShape71: TQRShape;
    QRShape72: TQRShape;
    QRShape73: TQRShape;
    QRShape74: TQRShape;
    QRShape75: TQRShape;
    QRShape76: TQRShape;
    QRLabel136: TQRLabel;
    QRShape77: TQRShape;
    QRLabel137: TQRLabel;
    l84: TQRLabel;
    l92: TQRLabel;
    l100: TQRLabel;
    QRShape78: TQRShape;
    QRLabel141: TQRLabel;
    l85: TQRLabel;
    l93: TQRLabel;
    l101: TQRLabel;
    QRShape79: TQRShape;
    QRLabel145: TQRLabel;
    l86: TQRLabel;
    l94: TQRLabel;
    l102: TQRLabel;
    l87: TQRLabel;
    l95: TQRLabel;
    l103: TQRLabel;
    QRLabel152: TQRLabel;
    l105: TQRLabel;
    l104: TQRLabel;
    l106: TQRLabel;
    l108: TQRLabel;
    l109: TQRLabel;
    l110: TQRLabel;
    l111: TQRLabel;
    QRShape80: TQRShape;
    QRShape81: TQRShape;
    QRShape82: TQRShape;
    QRShape83: TQRShape;
    QRShape84: TQRShape;
    QRShape85: TQRShape;
    l113: TQRLabel;
    l112: TQRLabel;
    l114: TQRLabel;
    l116: TQRLabel;
    l117: TQRLabel;
    l118: TQRLabel;
    l119: TQRLabel;
    QRShape87: TQRShape;
    QRShape88: TQRShape;
    l121: TQRLabel;
    l120: TQRLabel;
    l122: TQRLabel;
    l124: TQRLabel;
    l125: TQRLabel;
    l126: TQRLabel;
    l127: TQRLabel;
    QRShape89: TQRShape;
    QRShape90: TQRShape;
    QRShape91: TQRShape;
    l129: TQRLabel;
    l128: TQRLabel;
    l130: TQRLabel;
    l132: TQRLabel;
    l133: TQRLabel;
    l134: TQRLabel;
    l135: TQRLabel;
    QRShape86: TQRShape;
    l83: TQRLabel;
    l91: TQRLabel;
    l99: TQRLabel;
    l107: TQRLabel;
    l115: TQRLabel;
    l123: TQRLabel;
    l131: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel131: TQRLabel;
    qrDirectors2Old: TQuickRep;
    QRBand3: TQRBand;
    QRShape92: TQRShape;
    QRShape93: TQRShape;
    QRShape94: TQRShape;
    QRShape95: TQRShape;
    QRShape96: TQRShape;
    QRShape97: TQRShape;
    QRShape98: TQRShape;
    QRShape99: TQRShape;
    QRShape100: TQRShape;
    QRShape101: TQRShape;
    QRLabel301: TQRLabel;
    QRShape164: TQRShape;
    QRLabel303: TQRLabel;
    QRShape165: TQRShape;
    QRShape166: TQRShape;
    QRShape167: TQRShape;
    l138: TQRLabel;
    l142: TQRLabel;
    l146: TQRLabel;
    QRShape168: TQRShape;
    QRLabel307: TQRLabel;
    QRLabel308: TQRLabel;
    QRLabel309: TQRLabel;
    l148: TQRLabel;
    l150: TQRLabel;
    QRShape177: TQRShape;
    QRShape178: TQRShape;
    l152: TQRLabel;
    l154: TQRLabel;
    QRShape179: TQRShape;
    l156: TQRLabel;
    l158: TQRLabel;
    QRShape181: TQRShape;
    l160: TQRLabel;
    l162: TQRLabel;
    l144: TQRLabel;
    l140: TQRLabel;
    l136: TQRLabel;
    l137: TQRLabel;
    l141: TQRLabel;
    l145: TQRLabel;
    l149: TQRLabel;
    l153: TQRLabel;
    l157: TQRLabel;
    l161: TQRLabel;
    QRBand4: TQRBand;
    QRLabel129: TQRLabel;
    QRShape102: TQRShape;
    QRShape103: TQRShape;
    QRShape104: TQRShape;
    QRShape105: TQRShape;
    l139: TQRLabel;
    l143: TQRLabel;
    l147: TQRLabel;
    QRShape106: TQRShape;
    l151: TQRLabel;
    QRShape107: TQRShape;
    QRShape108: TQRShape;
    l155: TQRLabel;
    QRShape109: TQRShape;
    l159: TQRLabel;
    QRShape110: TQRShape;
    l163: TQRLabel;
    QRLabel193: TQRLabel;
    QRShape111: TQRShape;
    QRLabel194: TQRLabel;
    QRLabel195: TQRLabel;
    QRShape112: TQRShape;
    QRLabel196: TQRLabel;
    QRLabel202: TQRLabel;
    QRLabel203: TQRLabel;
    QRLabel204: TQRLabel;
    QRLabel205: TQRLabel;
    QRLabel206: TQRLabel;
    QRLabel207: TQRLabel;
    ls1: TQRLabel;
    QRLabel209: TQRLabel;
    ls2: TQRLabel;
    c: TQRLabel;
    QRLabel211: TQRLabel;
    QRLabel212: TQRLabel;
    QRLabel213: TQRLabel;
    l164: TQRLabel;
    QRLabel214: TQRLabel;
    l165: TQRLabel;
    QRLabel215: TQRLabel;
    QRLabel216: TQRLabel;
    l166: TQRLabel;
    QRLabel217: TQRLabel;
    QRLabel218: TQRLabel;
    l167: TQRLabel;
    QRLabel423: TQRLabel;
    QRLabel219: TQRLabel;
    QRLabel192: TQRLabel;
    QRLabel197: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel199: TQRLabel;
    QRLabel200: TQRLabel;
    QRLabel201: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel233: TQRLabel;
    lLawyer: TQRLabel;
    lFullName: TQRLabel;
    QRLabel236: TQRLabel;
    lIDNum: TQRLabel;
    QRLabel238: TQRLabel;
    QRLabel239: TQRLabel;
    QRLabel240: TQRLabel;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    lLawyer2: TQRLabel;
    QRLabel248: TQRLabel;
    lLawyerLicense: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel246: TQRLabel;
    lLawyerAddress: TQRLabel;
    lLawyerID: TQRLabel;
    QRLabel85: TQRLabel;
    QRShape114: TQRShape;
    lPhone: TQRLabel;
    QRLabel83: TQRLabel;
    QRShape116: TQRShape;
    lAddress2: TQRLabel;
    lAddress1: TQRLabel;
    QRShape113: TQRShape;
    QRShape115: TQRShape;
    qrDirectors1: TQuickRep;
    QRBand5: TQRBand;
    QRImage13: TQRImage;
    QRImage8: TQRImage;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRBand6: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRShape140: TQRShape;
    QRLabel144: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape117: TQRShape;
    QRShape118: TQRShape;
    QRLabel25: TQRLabel;
    edtCompanyName: TQRLabel;
    QRShape119: TQRShape;
    QRLabel28: TQRLabel;
    QRShape120: TQRShape;
    edtCompanyZeut: TQRLabel;
    QRShape121: TQRShape;
    edtCompanyPhone: TQRLabel;
    QRShape122: TQRShape;
    edtCompanyCity: TQRLabel;
    edtCompanyStreet: TQRLabel;
    QRShape123: TQRShape;
    QRShape124: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel26: TQRLabel;
    QRShape125: TQRShape;
    QRShape126: TQRShape;
    QRShape127: TQRShape;
    QRShape128: TQRShape;
    QRShape129: TQRShape;
    QRLabel29: TQRLabel;
    QRShape130: TQRShape;
    QRShape131: TQRShape;
    QRShape132: TQRShape;
    QRLabel31: TQRLabel;
    QRShape133: TQRShape;
    QRShape134: TQRShape;
    QRShape135: TQRShape;
    QRShape136: TQRShape;
    QRLabel35: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRShape137: TQRShape;
    QRShape138: TQRShape;
    QRShape139: TQRShape;
    QRShape141: TQRShape;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel50: TQRLabel;
    QRShape143: TQRShape;
    QRShape144: TQRShape;
    QRShape145: TQRShape;
    QRShape146: TQRShape;
    QRShape147: TQRShape;
    QRShape148: TQRShape;
    QRShape149: TQRShape;
    QRShape150: TQRShape;
    QRShape151: TQRShape;
    QRShape152: TQRShape;
    QRShape153: TQRShape;
    QRShape154: TQRShape;
    QRShape155: TQRShape;
    QRShape156: TQRShape;
    QRShape157: TQRShape;
    edtDirZip1: TQRLabel;
    edtDirNumber1: TQRLabel;
    edtDirStreet1: TQRLabel;
    edtDirStreet2: TQRLabel;
    edtDirNumber2: TQRLabel;
    edtDirZip2: TQRLabel;
    edtDirZip3: TQRLabel;
    edtDirNumber3: TQRLabel;
    edtDirStreet3: TQRLabel;
    edtDirNumber4: TQRLabel;
    edtDirZip4: TQRLabel;
    edtDirStreet4: TQRLabel;
    edtDirStreet5: TQRLabel;
    edtDirNumber5: TQRLabel;
    edtDirZip5: TQRLabel;
    edtDirZip6: TQRLabel;
    edtDirNumber6: TQRLabel;
    edtDirStreet6: TQRLabel;
    edtDirStreet7: TQRLabel;
    edtDirNumber7: TQRLabel;
    edtDirZip7: TQRLabel;
    edtDirCity7: TQRLabel;
    edtDirCity6: TQRLabel;
    edtDirCity5: TQRLabel;
    edtDirCity4: TQRLabel;
    edtDirCity3: TQRLabel;
    edtDirCity2: TQRLabel;
    edtDirCity1: TQRLabel;
    edtDirZeut7: TQRLabel;
    edtDirZeut6: TQRLabel;
    edtDirZeut5: TQRLabel;
    edtDirZeut4: TQRLabel;
    edtDirZeut3: TQRLabel;
    edtDirZeut2: TQRLabel;
    edtDirZeut1: TQRLabel;
    edtDirTaagid1: TQRLabel;
    edtDirTaagid2: TQRLabel;
    edtDirTaagid3: TQRLabel;
    edtDirTaagid4: TQRLabel;
    edtDirTaagid5: TQRLabel;
    edtDirTaagid6: TQRLabel;
    edtDirTaagid7: TQRLabel;
    edtDirFirst7: TQRLabel;
    edtDirFirst6: TQRLabel;
    edtDirFirst5: TQRLabel;
    edtDirFirst4: TQRLabel;
    edtDirFirst3: TQRLabel;
    edtDirFirst2: TQRLabel;
    edtDirFirst1: TQRLabel;
    QRShape158: TQRShape;
    QRShape159: TQRShape;
    QRShape160: TQRShape;
    QRShape161: TQRShape;
    edtDirDate1: TQRLabel;
    edtDirDate2: TQRLabel;
    edtDirDate3: TQRLabel;
    QRShape162: TQRShape;
    edtDirDate4: TQRLabel;
    QRShape163: TQRShape;
    QRShape169: TQRShape;
    edtDirDate5: TQRLabel;
    QRShape170: TQRShape;
    edtDirDate6: TQRLabel;
    QRShape171: TQRShape;
    edtDirDate7: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel147: TQRLabel;
    QRMemo1: TQRMemo;
    QRLabel30: TQRLabel;
    QRLabel148: TQRLabel;
    edtDirLast1: TQRLabel;
    edtDirLast2: TQRLabel;
    edtDirLast3: TQRLabel;
    edtDirLast4: TQRLabel;
    edtDirLast5: TQRLabel;
    edtDirLast6: TQRLabel;
    QRShape174: TQRShape;
    edtDirLast7: TQRLabel;
    QRShape142: TQRShape;
    QRLabel44: TQRLabel;
    QRShape172: TQRShape;
    QRShape173: TQRShape;
    QRShape175: TQRShape;
    QRShape176: TQRShape;
    edtDirDay1: TQRLabel;
    edtDirDay2: TQRLabel;
    QRLabel54: TQRLabel;
    QRShape180: TQRShape;
    QRShape182: TQRShape;
    QRShape183: TQRShape;
    QRShape184: TQRShape;
    edtDirDay7: TQRLabel;
    QRShape185: TQRShape;
    edtDirDay4: TQRLabel;
    edtDirDay3: TQRLabel;
    edtDirName4: TQRLabel;
    edtDirName5: TQRLabel;
    edtDirName6: TQRLabel;
    edtDirName7: TQRLabel;
    edtDirName2: TQRLabel;
    edtDirName3: TQRLabel;
    edtDirName1: TQRLabel;
    edtDirDay5: TQRLabel;
    edtDirDay6: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRShape186: TQRShape;
    QRShape187: TQRShape;
    QRLabel70: TQRLabel;
    QRShape188: TQRShape;
    edtDirDate8: TQRLabel;
    edtDirZip8: TQRLabel;
    edtDirNumber8: TQRLabel;
    edtDirStreet8: TQRLabel;
    edtDirCity8: TQRLabel;
    QRShape189: TQRShape;
    edtDirZeut8: TQRLabel;
    edtDirTaagid8: TQRLabel;
    edtDirFirst8: TQRLabel;
    edtDirLast8: TQRLabel;
    QRShape190: TQRShape;
    QRShape191: TQRShape;
    QRShape192: TQRShape;
    edtDirDay8: TQRLabel;
    edtDirName8: TQRLabel;
    edtDirMonth1: TQRLabel;
    edtDirMonth2: TQRLabel;
    edtDirMonth3: TQRLabel;
    edtDirMonth4: TQRLabel;
    edtDirMonth5: TQRLabel;
    edtDirMonth6: TQRLabel;
    edtDirMonth7: TQRLabel;
    edtDirMonth8: TQRLabel;
    edtDirYear1: TQRLabel;
    edtDirYear2: TQRLabel;
    edtDirYear3: TQRLabel;
    edtDirYear4: TQRLabel;
    edtDirYear5: TQRLabel;
    edtDirYear6: TQRLabel;
    edtDirYear7: TQRLabel;
    edtDirYear8: TQRLabel;
    qrDirectors2: TQuickRep;
    QRBand7: TQRBand;
    QRImage1: TQRImage;
    QRImage2: TQRImage;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRShape193: TQRShape;
    QRShape195: TQRShape;
    QRShape196: TQRShape;
    QRShape197: TQRShape;
    QRShape198: TQRShape;
    QRShape199: TQRShape;
    QRShape200: TQRShape;
    QRShape201: TQRShape;
    QRShape202: TQRShape;
    QRShape203: TQRShape;
    QRShape204: TQRShape;
    QRShape205: TQRShape;
    QRLabel58: TQRLabel;
    QRShape206: TQRShape;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRShape207: TQRShape;
    QRShape208: TQRShape;
    QRShape209: TQRShape;
    QRShape210: TQRShape;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRShape211: TQRShape;
    QRShape212: TQRShape;
    QRShape213: TQRShape;
    QRShape215: TQRShape;
    QRShape216: TQRShape;
    QRShape217: TQRShape;
    QRLabel66: TQRLabel;
    edtTaagidStreet1: TQRLabel;
    edtTaagidStreet2: TQRLabel;
    QRShape218: TQRShape;
    QRLabel72: TQRLabel;
    edtTaagidNumber1: TQRLabel;
    edtTaagidNumber2: TQRLabel;
    edtTaagidZip1: TQRLabel;
    edtTaagidZip2: TQRLabel;
    QRLabel79: TQRLabel;
    edtTaagidName4: TQRLabel;
    edtTaagidNumber4: TQRLabel;
    QRShape219: TQRShape;
    QRShape220: TQRShape;
    edtTaagidName5: TQRLabel;
    QRShape221: TQRShape;
    edtTaagidName6: TQRLabel;
    QRShape222: TQRShape;
    QRShape223: TQRShape;
    edtTaagidName7: TQRLabel;
    edtTaagidNumber7: TQRLabel;
    edtTaagidZip7: TQRLabel;
    QRLabel89: TQRLabel;
    edtTaagidCity1: TQRLabel;
    edtTaagidCity2: TQRLabel;
    edtTaagidCity3: TQRLabel;
    edtTaagidCity4: TQRLabel;
    edtTaagidCity7: TQRLabel;
    edtTaagidStreet3: TQRLabel;
    edtTaagidStreet4: TQRLabel;
    edtTaagidStreet7: TQRLabel;
    QRShape224: TQRShape;
    edtTaagidCity5: TQRLabel;
    edtTaagidCity6: TQRLabel;
    edtTaagidStreet5: TQRLabel;
    edtTaagidStreet6: TQRLabel;
    edtTaagidNumber6: TQRLabel;
    edtTaagidNumber5: TQRLabel;
    edtTaagidZip5: TQRLabel;
    edtTaagidZip6: TQRLabel;
    edtTaagidZip3: TQRLabel;
    edtTaagidZip4: TQRLabel;
    edtTaagidNumber3: TQRLabel;
    edtTaagidName3: TQRLabel;
    edtTaagidName2: TQRLabel;
    edtTaagidName1: TQRLabel;
    edtTaagidFirst1: TQRLabel;
    edtTaagidFirst2: TQRLabel;
    edtTaagidFirst3: TQRLabel;
    edtTaagidFirst4: TQRLabel;
    edtTaagidFirst5: TQRLabel;
    edtTaagidFirst6: TQRLabel;
    edtTaagidFirst7: TQRLabel;
    QRLabel156: TQRLabel;
    QRLabel157: TQRLabel;
    QRLabel158: TQRLabel;
    QRLabel159: TQRLabel;
    edtTaagidZeut1: TQRLabel;
    edtTaagidZeut2: TQRLabel;
    edtTaagidZeut3: TQRLabel;
    edtTaagidZeut4: TQRLabel;
    edtTaagidZeut5: TQRLabel;
    edtTaagidZeut6: TQRLabel;
    edtTaagidZeut7: TQRLabel;
    QRShape214: TQRShape;
    edtTaagidZip8: TQRLabel;
    edtTaagidNumber8: TQRLabel;
    edtTaagidStreet8: TQRLabel;
    edtTaagidCity8: TQRLabel;
    edtTaagidZeut8: TQRLabel;
    QRShape225: TQRShape;
    edtTaagidFirst8: TQRLabel;
    edtTaagidLast8: TQRLabel;
    edtTaagidLast7: TQRLabel;
    edtTaagidLast6: TQRLabel;
    edtTaagidLast5: TQRLabel;
    edtTaagidLast4: TQRLabel;
    edtTaagidLast3: TQRLabel;
    edtTaagidLast2: TQRLabel;
    edtTaagidLast1: TQRLabel;
    QRShape226: TQRShape;
    edtTaagidName8: TQRLabel;
    QRShape227: TQRShape;
    QRLabel174: TQRLabel;
    QRLabel175: TQRLabel;
    QRShape228: TQRShape;
    QRShape229: TQRShape;
    QRShape230: TQRShape;
    QRShape231: TQRShape;
    QRShape232: TQRShape;
    QRShape233: TQRShape;
    QRShape234: TQRShape;
    QRShape235: TQRShape;
    QRShape236: TQRShape;
    QRShape237: TQRShape;
    QRShape238: TQRShape;
    QRShape239: TQRShape;
    QRLabel65: TQRLabel;
    QRShape240: TQRShape;
    QRLabel67: TQRLabel;
    QRShape241: TQRShape;
    QRShape242: TQRShape;
    QRShape243: TQRShape;
    edtNoneZeut1: TQRLabel;
    edtNoneZeut2: TQRLabel;
    edtNoneZeut3: TQRLabel;
    QRShape244: TQRShape;
    QRLabel75: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel80: TQRLabel;
    edtNoneFirst4: TQRLabel;
    edtNoneZeut4: TQRLabel;
    QRShape245: TQRShape;
    QRShape246: TQRShape;
    edtNoneFirst5: TQRLabel;
    edtNoneZeut5: TQRLabel;
    QRShape247: TQRShape;
    edtNoneFirst6: TQRLabel;
    edtNoneZeut6: TQRLabel;
    QRShape248: TQRShape;
    edtNoneFirst7: TQRLabel;
    edtNoneZeut7: TQRLabel;
    edtNoneFirst3: TQRLabel;
    edtNoneFirst2: TQRLabel;
    edtNoneFirst1: TQRLabel;
    edtNoneName1: TQRLabel;
    edtNoneName2: TQRLabel;
    edtNoneName3: TQRLabel;
    edtNoneName4: TQRLabel;
    edtNoneName5: TQRLabel;
    edtNoneName6: TQRLabel;
    edtNoneName7: TQRLabel;
    QRLabel105: TQRLabel;
    QRShape249: TQRShape;
    QRShape250: TQRShape;
    QRShape251: TQRShape;
    QRShape252: TQRShape;
    QRShape253: TQRShape;
    QRShape254: TQRShape;
    QRShape255: TQRShape;
    QRShape256: TQRShape;
    QRLabel106: TQRLabel;
    QRShape257: TQRShape;
    QRLabel107: TQRLabel;
    QRLabel112: TQRLabel;
    QRShape258: TQRShape;
    edtNoneDay2: TQRLabel;
    edtNoneDay1: TQRLabel;
    edtNoneDay3: TQRLabel;
    edtNoneDay4: TQRLabel;
    edtNoneDay5: TQRLabel;
    edtNoneDay6: TQRLabel;
    edtNoneDay7: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel128: TQRLabel;
    edtNoneDay8: TQRLabel;
    QRShape259: TQRShape;
    edtNoneMonth1: TQRLabel;
    edtNoneMonth2: TQRLabel;
    edtNoneMonth3: TQRLabel;
    edtNoneMonth4: TQRLabel;
    edtNoneMonth5: TQRLabel;
    edtNoneMonth6: TQRLabel;
    edtNoneMonth7: TQRLabel;
    edtNoneMonth8: TQRLabel;
    edtNoneYear1: TQRLabel;
    edtNoneYear2: TQRLabel;
    edtNoneYear3: TQRLabel;
    edtNoneYear4: TQRLabel;
    edtNoneYear5: TQRLabel;
    edtNoneYear6: TQRLabel;
    edtNoneYear7: TQRLabel;
    edtNoneYear8: TQRLabel;
    edtNoneZeut8: TQRLabel;
    QRShape260: TQRShape;
    edtNoneName8: TQRLabel;
    QRShape261: TQRShape;
    edtNoneFirst8: TQRLabel;
    QRLabel170: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRShape262: TQRShape;
    edtNoneLast8: TQRLabel;
    edtNoneLast7: TQRLabel;
    edtNoneLast6: TQRLabel;
    edtNoneLast5: TQRLabel;
    edtNoneLast4: TQRLabel;
    edtNoneLast3: TQRLabel;
    edtNoneLast2: TQRLabel;
    edtNoneLast1: TQRLabel;
    QRShape263: TQRShape;
    QRLabel71: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel84: TQRLabel;
    QRShape264: TQRShape;
    QRLabel86: TQRLabel;
    QRBand8: TQRBand;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel90: TQRLabel;
    QRShape265: TQRShape;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    qrDirectors3: TQuickRep;
    QRBand9: TQRBand;
    QRBand10: TQRBand;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRShape266: TQRShape;
    QRLabel98: TQRLabel;
    QRImage3: TQRImage;
    QRImage4: TQRImage;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    edtSigName: TQRLabel;
    QRLabel149: TQRLabel;
    edtSigZeut: TQRLabel;
    QRLabel151: TQRLabel;
    edtSigTafkid: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel160: TQRLabel;
    edtSigNotifyDate: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel163: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    edtSigLawyer1: TQRLabel;
    edtSigFullName: TQRLabel;
    QRLabel169: TQRLabel;
    edtSigID: TQRLabel;
    QRLabel176: TQRLabel;
    QRLabel177: TQRLabel;
    QRLabel178: TQRLabel;
    QRLabel179: TQRLabel;
    QRLabel180: TQRLabel;
    edtSigLawyer2: TQRLabel;
    QRLabel182: TQRLabel;
    edtSigLawyerLicense: TQRLabel;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    edtSigLawyerAddress: TQRLabel;
    edtSigLawyerID: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel188: TQRLabel;
    edtApplyed: TQRLabel;
    edtNotApplyed: TQRLabel;
    QRLabel99: TQRLabel;
  private
    { Private declarations }
    FApplyed: Boolean;
    FTaagidOnly,
    FNonActive,
    FActiveDirectors: TDirectors;
    FCompany: TCompany;
    FNotifyDate: TDateTime;
    FDoPrint: Boolean;
    LastActiveDirector,
    LastTaagid,
    LastNonActive: Integer;
    procedure PrintNextPage1;
    procedure PrintNextPage2;
    procedure PrintNextPage3;
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintEmptyProc(DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintEmpty(const FileName: String);
    procedure PrintExecute(const FileName: String; Company: TCompany; TaagidOnly, ActiveDirectors, NonActive: TDirectors; Applyed: Boolean; NotifyDate: TDateTime);
  end;

var
  fmQRDirectors: TfmQRDirectors;

implementation

{$R *.DFM}

uses Util, PrinterSetup, Main, Utils2;

procedure TfmQRDirectors.PrintEmptyProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(qrDirectors1);
  fmPrinterSetup.FormatQR(qrDirectors2);
  fmPrinterSetup.FormatQR(qrDirectors3);
  if DoPrint then
  begin
    qrDirectors1.Print;
    qrDirectors2.Print;
    qrDirectors3.Print;
  end
  else
  begin
    qrDirectors1.Preview;
    Application.ProcessMessages;
    qrDirectors2.Preview;
    Application.ProcessMessages;
    qrDirectors3.Preview;
    Application.ProcessMessages;
  end;
end;

procedure TfmQRDirectors.PrintEmpty(const FileName: String);
begin
  fmPrinterSetup.Execute(FileName,PrintEmptyProc);
end;

procedure TfmQRDirectors.PrintProc(DoPrint: Boolean);
begin
  FDoPrint := DoPrint;

  // Moshe 16/06/2019
  LastActiveDirector := -1;
  LastTaagid := -1;
  PrintNextPage1;

  LastNonActive := 0;
  PrintNextPage2;
  PrintNextPage3;
end;

procedure TfmQRDirectors.PrintExecute(const FileName: String; Company: TCompany; TaagidOnly, ActiveDirectors, NonActive: TDirectors; Applyed: Boolean; NotifyDate: TDateTime);
begin
  FApplyed:= Applyed;
  FCompany:= Company;
  FActiveDirectors:= ActiveDirectors;
  FTaagidOnly:= TaagidOnly;
  FNonActive:= NonActive;
  FNotifyDate:= NotifyDate;
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQRDirectors.PrintNextPage1;
var
  i: integer;
  s: string;
  dir: TDirector;
  FirstName, LastName: string;
  lYear, lMonth, lDay : Word;
begin
  if (LastActiveDirector >= FActiveDirectors.Count) and (LastTaagid >= FTaagidOnly.Count) then
    Exit;

  // Moshe 16/06/2019 -- Print page at least once
  LastActiveDirector := 0;
  LastTaagid := 0;

  edtCompanyName.Caption    := FCompany.Name;
  edtCompanyZeut.Caption    := FCompany.Zeut;
  edtCompanyStreet.Caption  := FCompany.Address.Street + ' ' + FCompany.Address.Number;
  edtCompanyCity.Caption    := FCompany.Address.City + ' ' + FCompany.Address.Index;
  edtCompanyPhone.Caption   := FCompany.Address.Phone;

  // Clear Director controls
  for i := 0 to Self.ControlCount-1 do
  begin
    if ((Self.Controls[i].ClassName = 'TQRLabel') and
        (UpperCase(Copy(Self.Controls[i].Name, 1, 6)) = 'EDTDIR')) then
    TQRLabel(Self.Controls[i]).Caption := '';
  end;

  // Fill active directors
  for i := 1 to 8 do
  begin
    if (LastActiveDirector >= FActiveDirectors.Count) then
      Break;

    dir := FActiveDirectors.Items[LastActiveDirector] as TDirector;
    s := IntToStr(i);
    DecodeDate(dir.Date2, lYear, lMonth, lDay);

    if (dir.Taagid = nil) then
    begin
      SplitName(dir.Name, FirstName, LastName);
      SetCaption(Self, 'edtDirLast' + s, LastName);
      SetCaption(Self, 'edtDirFirst' + s, FirstName);
    end
    else
    begin
      SetCaption(Self, 'edtDirTaagid' + s, dir.Taagid.Name);
    end;

    SetCaption(Self, 'edtDirName' + s, dir.Name);
    SetCaption(Self, 'edtDirZeut' + s, dir.Zeut);
    SetCaption(Self, 'edtDirCity' + s, dir.Address.City);
    SetCaption(Self, 'edtDirStreet' + s, dir.Address.Street);
    SetCaption(Self, 'edtDirNumber' + s, dir.Address.Number);
    SetCaption(Self, 'edtDirZip' + s, dir.Address.Index);
    SetCaption(Self, 'edtDirDate' + s, DateToLongStr(Dir.Date1));
    SetCaption(Self, 'edtDirYear' + s, IntToStr(lYear));
    SetCaption(Self, 'edtDirMonth' + s, IntToStr(lMonth));
    SetCaption(Self, 'edtDirDay' + s, IntToStr(lDay));

    Inc(LastActiveDirector);
  end;
  Inc(LastActiveDirector);

  // Clear Taagid controls
  for i := 0 to Self.ControlCount-1 do
  begin
    if ((Self.Controls[i].ClassName = 'TQRLabel') and
        (UpperCase(Copy(Self.Controls[i].Name, 1, 9)) = 'EDTTAAGID')) then
    TQRLabel(Self.Controls[i]).Caption := '';
  end;

  // Fill taagid info
  for i := 1 to 8 do
  begin
    if (LastTaagid >= FTaagidOnly.Count) then
      Break;

    dir := FTaagidOnly.Items[LastTaagid] as TDirector;
    s := IntToStr(i);
    SplitName(dir.Taagid.Name, FirstName, LastName);

    SetCaption(Self, 'edtTaagidName' + s,   dir.Name);
    SetCaption(Self, 'edtTaagidLast' + s,   LastName);
    SetCaption(Self, 'edtTaagidFirst' + s,  FirstName);
    SetCaption(Self, 'edtTaagidZeut' + s,   dir.Taagid.Zeut);
    SetCaption(Self, 'edtTaagidCity' + s,   dir.Taagid.City);
    SetCaption(Self, 'edtTaagidStreet' + s, dir.Taagid.Street);
    SetCaption(Self, 'edtTaagidNumber' + s, dir.Taagid.Number);
    SetCaption(Self, 'edtTaagidZip' +s,     dir.Taagid.Index);

    Inc(LastTaagid);
  end;
  Inc(LastTaagid);

  fmPrinterSetup.FormatQR(qrDirectors1);
  If FDoPrint then
    qrDirectors1.Print
  else
  begin
    qrDirectors1.Preview;
    Application.ProcessMessages;
  end;
  PrintNextPage1;
end; // TfmQRDirectors.PrintNextPage1

procedure TfmQRDirectors.PrintNextPage2;
var
  i: integer;
  s: string;
  dir: TDirector;
  FirstName, LastName: string;
  lYear, lMonth, lDay : Word;
begin
  if (LastNonActive >= FNonActive.Count) then
    Exit;
    
  // Clear none active director controls
  for i := 0 to Self.ControlCount-1 do
  begin
    if ((Self.Controls[i].ClassName = 'TQRLabel') and
        (UpperCase(Copy(Self.Controls[i].Name, 1, 7)) = 'EDTNONE')) then
    TQRLabel(Self.Controls[i]).Caption := '';
  end;

  // Fill none active directors
  for i := 1 to 8 do
  begin
    if (LastNonActive >= FNonActive.Count) then
      Break;

    dir := FNonActive.Items[LastNonActive] as TDirector;
    s := IntToStr(i);
    DecodeDate(dir.LastChanged, lYear, lMonth, lDay);
    SplitName(dir.Name, FirstName, LastName);

    if (dir.Taagid <> nil) then
      SetCaption(Self, 'edtNoneName' + s, dir.Taagid.Name);
    SetCaption(Self, 'edtNoneLast' + s, LastName);
    SetCaption(Self, 'edtNoneFirst' + s, FirstName);
    SetCaption(Self, 'edtNoneZeut' + s, dir.Zeut);
    SetCaption(Self, 'edtNoneYear' + s, IntToStr(lYear));
    SetCaption(Self, 'edtNoneMonth' + s, IntToStr(lMonth));
    SetCaption(Self, 'edtNoneDay' + s, IntToStr(lDay));

    Inc(LastNonActive);
  end;
  Inc(LastNonActive);

  If FApplyed then
    ls2.Font.Style:= [fsStrikeOut]
  Else
    ls1.Font.Style:= [fsStrikeOut];

  l164.Caption:= FCompany.CompanyInfo.SigName;
  l165.Caption:= FCompany.CompanyInfo.SigZeut;
  l166.Caption:= FCompany.CompanyInfo.SigTafkid;
  l167.Caption:= DateToLongStr(FNotifyDate);

  //==>14.6.05 Tsahi : LAwyer details
  lLawyer.Caption:= FCompany.CompanyInfo.LawName ;
  lLawyer2.Caption:= lLawyer.Caption;
  lFullName.Caption:= l164.Caption ;
  lIDNum.Caption:= l165.Caption;
  lLawyerAddress.Caption:= FCompany.CompanyInfo.LawAddress;
  lLawyerID.Caption:= FCompany.CompanyInfo.LawZeut;
  lLawyerLicense.Caption:= FCompany.CompanyInfo.LawLicence;

  if lLawyerID.Caption='0' then
    lLawyerID.Caption:='';
  if lIDNum.Caption='0' then
    lIDNum.Caption:='';
  if lLawyerLicense.Caption='0' then
    lLawyerLicense.Caption:='';
  //<==

  fmPrinterSetup.FormatQR(qrDirectors2);
  If FDoPrint then
    qrDirectors2.Print
  else
  begin
    qrDirectors2.Preview;
    Application.ProcessMessages;
  end;
  PrintNextPage2;
end; // TfmQRDirectors.PrintNextPage2

procedure TfmQRDirectors.PrintNextPage3;
begin
  if (FApplyed) then
    edtApplyed.Caption := 'X'
  else
    edtNotApplyed.Caption := 'X';

  edtSigName.Caption            := FCompany.CompanyInfo.SigName;
  edtSigZeut.Caption            := FCompany.CompanyInfo.SigZeut;
  edtSigTafkid.Caption          := FCompany.CompanyInfo.SigTafkid;
  edtSigNotifyDate.Caption      := DateToLongStr(FNotifyDate);
  edtSigLawyer1.Caption         := FCompany.CompanyInfo.LawName;
  edtSigLawyer2.Caption         := FCompany.CompanyInfo.LawName;
  edtSigFullName.Caption        := FCompany.CompanyInfo.SigName;
  edtSigID.Caption              := FCompany.CompanyInfo.SigZeut;
  edtSigLawyerAddress.Caption   := FCompany.CompanyInfo.LawAddress;
  edtSigLawyerID.Caption        := FCompany.CompanyInfo.LawZeut;
  edtSigLawyerLicense.Caption   := FCompany.CompanyInfo.LawLicence;

  if (edtSigID.Caption = '0') then edtSigID.Caption := '';
  if (edtSigLawyerID.Caption = '0') then edtSigLawyerID.Caption := '';
  if (edtSigLawyerLicense.Caption = '0') then edtSigLawyerLicense.Caption := '';

  fmPrinterSetup.FormatQR(qrDirectors3);
  if (FDoPrint) then
    qrDirectors3.Print
  else
    qrDirectors3.Preview;
  Application.ProcessMessages;

end; // TfmQRDirectors.PrintNextPage3

end.

