unit PrintHaavara;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, DataObjects;

type
  TfmQRHaavara = class(TForm)
    qrHaavara1: TQuickRep;
    QRBand1: TQRBand;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel8: TQRLabel;
    l1: TQRLabel;
    l2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape8: TQRShape;
    QRLabel15: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel30: TQRLabel;
    l17: TQRLabel;
    QRLabel32: TQRLabel;
    l18: TQRLabel;
    QRLabel33: TQRLabel;
    l19: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel62: TQRLabel;
    l20: TQRLabel;
    l21: TQRLabel;
    QRLabel65: TQRLabel;
    l12: TQRLabel;
    QRBand2: TQRBand;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel25: TQRLabel;
    l3: TQRLabel;
    QRLabel22: TQRLabel;
    edtSetFirst: TQRLabel;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRLabel27: TQRLabel;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRLabel28: TQRLabel;
    QRShape23: TQRShape;
    QRLabel29: TQRLabel;
    QRLabel40: TQRLabel;
    l5: TQRLabel;
    QRLabel42: TQRLabel;
    l6: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRShape29: TQRShape;
    QRShape31: TQRShape;
    QRLabel46: TQRLabel;
    l8: TQRLabel;
    QRLabel48: TQRLabel;
    l9: TQRLabel;
    QRShape35: TQRShape;
    QRLabel50: TQRLabel;
    QRShape36: TQRShape;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    l10: TQRLabel;
    QRShape37: TQRShape;
    QRShape38: TQRShape;
    QRLabel54: TQRLabel;
    edtGetFirst: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    l13: TQRLabel;
    l14: TQRLabel;
    l15: TQRLabel;
    l16: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape39: TQRShape;
    l6a: TQRLabel;
    QRShape41: TQRShape;
    l6b: TQRLabel;
    l8a: TQRLabel;
    l8b: TQRLabel;
    l9b: TQRLabel;
    l9a: TQRLabel;
    qrHaavara2: TQuickRep;
    QRBand11: TQRBand;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel233: TQRLabel;
    lLawyer: TQRLabel;
    lFullName: TQRLabel;
    QRLabel236: TQRLabel;
    lIDNum: TQRLabel;
    QRLabel238: TQRLabel;
    QRLabel239: TQRLabel;
    QRLabel240: TQRLabel;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    lLawyer2: TQRLabel;
    QRLabel248: TQRLabel;
    lLawyerLicense: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel246: TQRLabel;
    lLawyerAddress: TQRLabel;
    lLawyerID: TQRLabel;
    QRLabel253: TQRLabel;
    QRLabel254: TQRLabel;
    QRLabel255: TQRLabel;
    DetailBand1: TQRBand;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRLabel47: TQRLabel;
    QRShape52: TQRShape;
    QRShape57: TQRShape;
    QRLabel53: TQRLabel;
    lPhone: TQRLabel;
    lAddress1: TQRLabel;
    QRImage1: TQRImage;
    QRImage2: TQRImage;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel49: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape9: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    edtSetLast: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRShape18: TQRShape;
    QRShape24: TQRShape;
    edtStkAg1: TQRLabel;
    edtStkAg2: TQRLabel;
    edtStkAg3: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    edtGetLast: TQRLabel;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape7: TQRShape;
    QRBand4: TQRBand;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRShape140: TQRShape;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRImage3: TQRImage;
    QRImage4: TQRImage;
    QRLabel9: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel39: TQRLabel;
    QRBand3: TQRBand;
    QRLabel31: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    QRShape30: TQRShape;
    QRLabel38: TQRLabel;
    QRLabel41: TQRLabel;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
  private
    { Private declarations }
    FDoPrint: Boolean;
    LastStock: Integer;
    FStocks: TStocks;
    FFirst, FSecond: TStockHolder;
    FCompanyInfo: TCompanyInfo;
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintEmptyProc(DoPrint: Boolean);
    procedure PrintNextPage1;
    procedure PrintNextPage2;    
  public
    { Public declarations }
    procedure PrintEmpty(const FileName: String);
    procedure PrintExecute(const FileName: String; Company: TCompany; FirstStockHolder, SecondStockHolder: TStockHolder; Stocks: TStocks; DecisionDate, NotifyDate: TDateTime);
  end;

var
  fmQRHaavara: TfmQRHaavara;

implementation

uses Util, Printers, PrinterSetup, PrintShtar;

{$R *.DFM}

procedure TfmQRHaavara.PrintEmptyProc(DoPrint: Boolean);
begin
  //==>14.6.06 Tsahi: Changed to reflact added Lawyer details
  fmPrinterSetup.FormatQR(qrHaavara1);
  fmPrinterSetup.FormatQR(qrHaavara2);
  If DoPrint then
  begin
    qrHaavara1.Print;
    qrHaavara2.Print;
  end Else
  begin
    qrHaavara1.Preview;
    qrHaavara2.Preview;
    Application.ProcessMessages;
  end;
  //<==
end;

procedure TfmQRHaavara.PrintEmpty(const FileName: String);
begin
  fmPrinterSetup.Execute(FileName, PrintEmptyProc);
end;

procedure TfmQRHaavara.PrintNextPage1;
var
  stk: TStock;
  VS, VSh, VAg: string;
  FirstName, LastName: string;
begin
  if (LastStock >= FStocks.Count) then
    Exit;

  // Fill Stock List
  try
    // line #1
    stk := FStocks.Items[LastStock] as TStock;

    l6.Caption := '';
    l8.Caption := '';
    l9.Caption := '';

    VS := FloatToStrF(stk.Value, ffNumber, 15, 4);
    SplitBySep(VS, '.', VSh, VAg);

    l6.Caption := stk.Name;
    l8.Caption := VSh;
    l9.Caption := FloatToStrF(stk.Count, ffNumber, 15, 2);
    edtStkAg1.Caption := VAg;
    Inc(LastStock);
  except
  end;

  // line #2
  if (LastStock < FStocks.Count) then
  try
    stk := FStocks.Items[LastStock] as TStock;

    l6a.Caption := '';
    l8a.Caption := '';
    l9a.Caption := '';

    VS := FloatToStrF(stk.Value, ffNumber, 15, 4);
    SplitBySep(VS, '.', VSh, VAg);

    l6a.Caption := stk.Name;
    l8a.Caption := VSh;
    l9a.Caption := FloatToStrF(stk.Count, ffNumber, 15, 2);
    edtStkAg2.Caption := VAg;

    Inc(LastStock);
  except
  end;

  // line #3
  if (LastStock < FStocks.Count) then
  try
    stk := FStocks.Items[LastStock] as TStock;

    l6b.Caption := '';
    l8b.Caption := '';
    l9b.Caption := '';

    VS := FloatToStrF(stk.Value, ffNumber, 15, 4);
    SplitBySep(VS, '.', VSh, VAg);

    l6b.Caption := stk.Name;
    l8b.Caption := VSh;
    l9b.Caption := FloatToStrF(stk.Count, ffNumber, 15, 2);
    edtStkAg2.Caption := VAg;

    Inc(LastStock);
  except
  end;

  // Split only person
  if (FFirst.Taagid) then
  begin
    edtSetFirst.Caption := '';
    edtSetLast.Caption := FFirst.Name;
  end
  else
  begin
    SplitName(FFirst.Name, FirstName, LastName);
    edtSetFirst.Caption := FirstName;
    edtSetLast.Caption := LastName;
  end;
  l3.Caption := FFirst.Zeut;

  // Split only person
  if (FSecond.Taagid) then
  begin
    edtGetFirst.Caption := '';
    edtGetLast.Caption := FSecond.Name;
  end
  else
  begin
    SplitName(FSecond.Name, FirstName, LastName);
    edtGetFirst.Caption := FirstName;
    edtGetLast.Caption := LastName;
  end;

  l10.Caption := FSecond.Zeut;
  l12.Caption := FSecond.Address.Country;
  l13.Caption := FSecond.Address.City;
  l14.Caption := FSecond.Address.Street;
  l15.Caption := FSecond.Address.Number;
  l16.Caption := FSecond.Address.Index;

  //PrintNextPage1;
end; // TfmQRHaavara.PrintNextPage1

procedure TfmQRHaavara.PrintNextPage2;
begin
  //==>14.6.05 Tsahi
  lLawyer.Caption:= FCompanyInfo.LawName;
  lLawyer2.Caption:= lLawyer.Caption;
  lFullName.Caption:= FCompanyInfo.SigName;
  lIDNum.Caption:= FCompanyInfo.SigZeut ;
  lLawyerAddress.Caption:= FCompanyInfo.LawAddress ;
  lLawyerID.Caption:= FCompanyInfo.LawZeut ;
  lLawyerLicense.Caption:= FCompanyInfo.LawLicence ;

  if lLawyerID.Caption='0' then
    lLawyerID.Caption:='';
  if lIDNum.Caption='0' then
    lIDNum.Caption:='';
  if lLawyerLicense.Caption='0' then
    lLawyerLicense.Caption:='';
  //<==
  //PrintNextPage2;
end;


procedure TfmQRHaavara.PrintExecute(const FileName: String; Company: TCompany; FirstStockHolder, SecondStockHolder: TStockHolder; Stocks: TStocks; DecisionDate, NotifyDate: TDateTime);
begin
  l1.Caption := Company.Name;
  l2.Caption := Company.Zeut;
  //==>Tsahi 18.6.06: added address and phone details
  lAddress1.Caption := Company.Address.Street + ' ' + Company.Address.Number + ' ' +
                       Company.Address.City + ' ' + Company.Address.Index;
  lPhone.Caption := Company.Address.Phone;
  //<==
  l5.Caption  := DateToLongStr(DecisionDate);
  l17.Caption := Company.CompanyInfo.SigName;
  l18.Caption := Company.CompanyInfo.SigZeut;
  l19.Caption := Company.CompanyInfo.SigTafkid;
  l20.Caption := DateToLongStr(NotifyDate);
  FStocks     := Stocks;
  FFirst      := FirstStockHolder;
  FSecond     := SecondStockHolder;
  FCompanyInfo := Company.CompanyInfo;
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQRHaavara.PrintProc(DoPrint: Boolean);
begin
  FDoPrint := DoPrint;
  LastStock := 0;
  PrintNextPage1;
  PrintNextPage2;

  PrintEmptyProc(DoPrint);

  Application.CreateForm(TfmQRShtar, fmQRShtar);
  fmQRShtar.PrintExecute(l1.Caption, l2.Caption, FFirst, FSecond, FStocks, DoPrint);
end;

end.
