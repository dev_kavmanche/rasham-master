unit unMerge;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, HLabel,
  Mask, ToolEdit, HGrids, SdGrid, HListBox, unMergeData, FileCtrl, dbTables,
  DataM, Compress, DataObjects;

type
  TfmMergeModule = class(TfmTemplate)
    edDestFileName: TFilenameEdit;
    HebLabel1: THebLabel;
    sdCompanies: TSdGrid;
    HebLabel3: THebLabel;
    btAddToMain: TButton;
    comp: TCompress;
    sdMergeCompanies: TSdGrid;
    HebLabel2: THebLabel;
    btAddToMerge: TButton;
    btOpen: TButton;
    Progress: TProgressBar;
    procedure sdCompaniesEnter(Sender: TObject);
    procedure sdMergeCompaniesEnter(Sender: TObject);
    procedure btOpenClick(Sender: TObject);
    procedure btAddToMergeClick(Sender: TObject);
    procedure btAddToMainClick(Sender: TObject);
    procedure edDestFileNameChange(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    TempDirectory: String;
    ShowProgress : boolean;
    ProgressCounter : integer;
    DBHasChanged  : boolean;
    procedure FillGrid(Grid: TSdGrid; Source: TData);
    procedure DeleteFiles(const FileName: String);
    procedure LoadMergeModule(const FileName: String);
    procedure NewMergeModule(const FileName: String);
    procedure SaveMergeModule(const FileName: String);
    procedure BackupBeforeImport;
  protected
    procedure EvFillDefaultData; override;
  public
    { Public declarations }
  end;

var
  fmMergeModule: TfmMergeModule;

implementation

uses Util, Backup, dialog2,FixProblems, utils2;

{$R *.DFM}

procedure TfmMergeModule.DeleteFiles(const FileName: String);
var
  SearchRec: TSearchRec;
  Error, I: Integer;
  FilesToDelete: TStringList;
begin
  FilesToDelete:= TStringList.Create;
  Error:= FindFirst(FileName, faAnyFile, SearchRec);
  While Error = 0 do
    begin
      If (SearchRec.Attr and faDirectory) <> faDirectory then
        FilesToDelete.Add(ExtractFilePath(FileName)+SearchRec.Name);
      Error:= FindNext(SearchRec);
    end;
  FindClose(SearchRec);
  For I:= 0 To FilesToDelete.Count-1 do
    DeleteFile(FilesToDelete[I]);
  FilesToDelete.Free;
end;

procedure TfmMergeModule.EvFillDefaultData;
begin
  TempDirectory:= ExtractFilePath(ParamStr(0))+'TmpDir\';
  sdCompanies.Cells[0,0]:= '�� �����';
  sdCompanies.Cells[1,0]:= '���� �����';

  sdMergeCompanies.Cells[0,0]:= sdCompanies.Cells[0,0];
  sdMergeCompanies.Cells[1,0]:= sdCompanies.Cells[1,0];

  FillGrid(sdCompanies, Data);
end;

procedure TfmMergeModule.NewMergeModule(const FileName: String);
var
  I: Integer;
begin
  DeleteFile(FileName);

  If Assigned(MergeData) Then
    MergeData.TablesActivate(False)
  else
    Application.CreateForm(TMergeData, MergeData);

  If DirectoryExists(TempDirectory) then
    DeleteFiles(TempDirectory+'*.*')
  Else
    ForceDirectories(TempDirectory);

  For I:= 0 To MergeData.ComponentCount-1 do
    If MergeData.Components[I] is TTable then
      with MergeData.Components[I] as TTable do
        begin
          Close;
          DatabaseName:= Data.taName.DatabaseName;
          IndexDefs.Update;
          FieldDefs.Update;
          DatabaseName:= TempDirectory;
          CreateTable;
          Open;
        end;
end;

procedure TfmMergeModule.SaveMergeModule(const FileName: String);
var
  Files: TStringList;

  procedure AddTable(Table: TTable);
  var
    Error     : Integer;
    SearchRec : TsearchRec;
  begin

    Error:=FindFirst(Table.Database.Directory+Copy(Table.TableName,1,Length(Table.TableName) - Length(ExtractFileExt(Table.TableName)))+'.*',faAnyFile,SearchRec);
    While Error=0 do
     begin
       Files.Add(Table.Database.Directory+SearchRec.Name);
       Error:=FindNext(SearchRec);
     end;
  end;

  procedure AddFiles;
  var
    I: Integer;
  begin
    For I:= 0 To MergeData.ComponentCount-1 do
      If MergeData.Components[I] is TTable then
        AddTable(MergeData.Components[I] as TTable);
  end;

begin
  Files:= TStringList.Create;
  try
    AddFiles;
    comp.TargetPath:= MergeData.taName.Database.Directory;
    MergeData.TablesActivate(False);
    comp.CompressFiles(FileName,Files,coLZH5);
    MergeData.TablesActivate(True);
  finally
    Files.Free;
  end;
end;

procedure TfmMergeModule.LoadMergeModule(const FileName: String);
var
  Files: TStringList;
  I: Integer;
begin
  If DirectoryExists(TempDirectory) then
  begin
    try
      MergeData.TablesActivate(False);
    except end;
    DeleteFiles(TempDirectory+'*.*')
  end
  Else
    ForceDirectories(TempDirectory);

  If Not Assigned(MergeData) Then
    Application.CreateForm(TMergeData, MergeData);

  Files:= TStringList.Create;
  try
    comp.TargetPath:= TempDirectory;
    comp.ScanCompressedFile(FileName,Files);
    comp.ExpandFiles(FileName, Files);
    For I:= 0 to MergeData.ComponentCount-1 do
      If MergeData.Components[I] is TTable Then
        With MergeData.Components[I] as TTable do
          begin
            DatabaseName:= TempDirectory;
            Open;
          end;
  finally
    comp.FreeFileList(Files);
    Files.Free;
  end;
end;

procedure TfmMergeModule.sdCompaniesEnter(Sender: TObject);
begin
  inherited;
  btAddToMerge.Enabled:= sdCompanies.Cells[0,1] <> '';
  btAddToMain.Enabled:= False;
end;

procedure TfmMergeModule.sdMergeCompaniesEnter(Sender: TObject);
begin
  inherited;
  btAddToMerge.Enabled:= False;
  btAddToMain.Enabled:= sdMergeCompanies.Cells[0,1] <> '';
end;

procedure TfmMergeModule.btOpenClick(Sender: TObject);
begin
  inherited;
  if FileExists(edDestFileName.FileName) then
    LoadMergeModule(edDestFileName.FileName)
  else
    NewMergeModule(edDestFileName.FileName);

  BackupBeforeImport;
  FillGrid(sdMergeCompanies, MergeData);
  sdCompanies.Enabled:= True;
  sdMergeCompanies.Enabled:= True;

end;

procedure TfmMergeModule.FillGrid(Grid: TSdGrid; Source: TData);
var
  OldEnabled: Boolean;
begin
  OldEnabled:= Grid.Enabled;
  Grid.Enabled:= True;
  Grid.RowCount:= 2;
  Grid.Rows[1].Clear;
  Source.taName.First;
  While not Source.taName.EOF do
    begin
      If Source.taName.FieldByName('ResInt1').AsInteger <> 1 Then
        begin
          Grid.Cells[0,Grid.RowCount-1]:= Source.taName.FieldByName('Name').AsString;
          Grid.Cells[1,Grid.RowCount-1]:= Source.taName.FieldByName('CompNum').AsString;
          If Grid.Cells[1, Grid.RowCount-1] = '-1' then
            Grid.Cells[1, Grid.RowCount-1]:= '����';
          Grid.Cells[2, Grid.RowCount-1]:= Source.taName.FieldByName('RecNum').AsString;
          Grid.RowCount:= Grid.RowCount + 1;
        end;

      Source.taName.Next;
    end;
  If Grid.RowCount > 2 Then
    Grid.RowCount:= Grid.RowCount -1;
  //SortGrid(Grid, 0, 1, stString, stNumber);
  SortGrid(Grid,'0,STRING,UP;1,NUMBER,UP');
  Grid.Repaint;
  Grid.Enabled:= OldEnabled;
end;

procedure TfmMergeModule.btAddToMergeClick(Sender: TObject);
begin
  inherited;
  Data.TablesReset;
  MergeData.TablesReset;
  If sdMergeCompanies.Cols[0].IndexOf(sdCompanies.Cells[0, sdCompanies.Row]) <> -1 Then
    raise Exception.Create('��� �� ����� ��� �����');
  try
    CopyCompany(Data, MergeData, StrToInt(sdCompanies.Cells[2, sdCompanies.Row]),  sdCompanies.Cells[0, sdCompanies.Row]);
    SaveMergeModule(edDestFileName.FileName);
  finally
    FillGrid(sdMergeCompanies, MergeData);
    Data.TablesReset;
    MergeData.TablesReset;
  end;
end;

procedure TfmMergeModule.btAddToMainClick(Sender: TObject);
begin
  inherited;
  Data.TablesReset;
  MergeData.TablesReset;
  if sdCompanies.Cols[0].IndexOf(sdMergeCompanies.Cells[0, sdMergeCompanies.Row]) <> -1 then
    raise Exception.Create('��� �� ����� ��� �����');
  try
    DBHasChanged :=CopyCompany(MergeData, Data, StrToInt(sdMergeCompanies.Cells[2, sdMergeCompanies.Row]), sdCompanies.Cells[0, sdCompanies.Row]);
  finally
    FillGrid(sdCompanies, Data);
    Data.TablesReset;
    MergeData.TablesReset;
  end;
end;

procedure TfmMergeModule.edDestFileNameChange(Sender: TObject);
begin
  inherited;
  btAddToMain.Enabled:= False;
  btAddToMerge.Enabled:= False;
  sdCompanies.Enabled:= False;
  sdMergeCompanies.RowCount:=2;
  sdMergeCompanies.Rows[1].Clear;
  sdMergeCompanies.Repaint;
  sdMergeCompanies.Enabled:= False;
end;

procedure TfmMergeModule.BackupBeforeImport;
var
  DateTimeStr: string;
  FileName: string;
  FilePAth: string;
  i :integer;
  LogFileName:string;
begin
  LogFileName:=GetLogFileName;
  try
    WriteLog('Quiet BackUp Before Import', LogFileName);//'C:\RashLog.txt');
    FilePath := ExtractFilePath(ParamStr(0))+'BackUp\';
    for i := 0 to 99 do
    begin
      DateTimeToString(DateTimeStr,'ddmmyy',Now);
      FileName := DateTimeStr;
      if i<10 then
        FileName := FileName+'0';
      FileName := FileName+IntToStr(i)+'.rbk';

    if not FileExists( FilePath+FileName) then
        break;
    end;
    Application.CreateForm(TfmBackUp, fmBackUp);
    fmBackup.SendToBack;
    fmDialog2.Panel1.Caption:= '...'+'�� ����';
    fmDialog2.Show;
    Application.ProcessMessages;
    fmBackUp.BackUpExecute(FilePath+FileName);
    fmBackUp.Close;
    fmDialog2.Close;
    WriteLog('Quiet BackUp Done', LogFileName);//'C:\RashLog.txt');
  except
    WriteLog('Quiet BackUp Failed', LogFileName);//'C:\RashLog.txt');
  end;
end;
procedure TfmMergeModule.FormPaint(Sender: TObject);
begin
  inherited;
  if ShowProgress then
  begin
    if ProgressCounter =50 then
    begin
      ProgressCounter := 0;
      if Progress.Position <= 95 then
        Progress.Position := Progress.Position+1;
    end else
      inc(ProgressCounter);
  end;
end;

procedure TfmMergeModule.FormCreate(Sender: TObject);
begin
  inherited;
  ShowProgress := False;
  ProgressCounter := 0;
  DBHasChanged := false;
end;

procedure TfmMergeModule.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if DBHasChanged then
  begin
    try
      ShowProgress := True;
      FixDB(False,Self );   //Tsahi 11/10/05 : Fix Director saving problems if that DB is from the period before the fix
      Progress.Position := 100;
      ShowProgress := False;
    finally
       MergeData.TablesReset;
    end;
  end;
end;

end.
