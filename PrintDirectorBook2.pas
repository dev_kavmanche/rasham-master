unit PrintDirectorBook2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Qrctrls, QuickRpt, ExtCtrls, DataObjects;

type
  TfmQRDirBook2 = class(TForm)
    qrDirectors: TQuickRep;
    QRBand3: TQRBand;
    QRLabel9: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    l1: TQRLabel;
    l2: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    l3: TQRLabel;
    l4: TQRLabel;
    QRLabel8: TQRLabel;
    QRBand1: TQRBand;
    QRDBText9: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    taDirectors: TTable;
    QRLabel15: TQRLabel;
    QRDBText10: TQRDBText;
    procedure QRDBText9Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    FCompany: TCompany;
    FNotifyDate, FDecisionDate: TDateTime;
    procedure PrintProc(DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintExecute(const FileName: String; Company: TCompany; NotifyDate, DecisionDate: TDateTime);
  end;

var
  fmQRDirBook2: TfmQRDirBook2;

implementation

{$R *.DFM}

uses Util, PrinterSetup;

procedure TfmQRDirBook2.PrintExecute(const FileName: String; Company: TCompany; NotifyDate, DecisionDate: TDateTime);
begin
  FCompany:= Company;
  FNotifyDate:= NotifyDate;
  FDecisionDate:= DecisionDate;
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQRDirBook2.PrintProc(DoPrint: Boolean);
begin
  l1.Caption:= FCompany.Name;
  l2.Caption:= FCompany.Zeut;
  l3.Caption:= DateToLongStr(FNotifyDate);
  l4.Caption:= DateToLongStr(FDecisionDate);
  taDirectors.Active:= True;
  fmPrinterSetup.FormatQR(qrDirectors);
  If DoPrint then
    qrDirectors.Print
  Else
    begin
      qrDirectors.Preview;
      Application.ProcessMessages;
    end;
end;

procedure TfmQRDirBook2.QRDBText9Print(sender: TObject; var Value: String);
begin
  if Trim(Value)='0' then
    Value:= '';
end;

end.
