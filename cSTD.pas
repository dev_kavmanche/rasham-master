unit cStd;
// ******************************************************************** //
// Standard rutines library                                             //
// ------------------------                                             //
// By Cerberus Software 1999 (c)                                        //
// ******************************************************************** //
interface
uses SysUtils, Grids;

type
tAppPath = (apRoot,apData);
tRemMode = (rmLeft,rmRight,rmBoth);

function DefaultPath(Mode : tAppPath) : string;
function AddComas(inp : string; Freq : word) : string;
function RemoveComas(inp : string) : string;
function FormatStr(inp : string; fLength : word) : string;
function DateToLongStr(Date : tDateTime) : string;
function RemoveSpaces(inp : string; RemMode : tRemMode) : string;
function GridEmpty(grid : tStringGrid) : boolean;

implementation
//-----------------------------------------------------------------------
function DefaultPath(Mode : tAppPath) : string;
begin
 if Mode = apRoot then DefaultPath := ExtractFilePath(ParamStr(0)) else
 if Mode = apData then DefaultPath := ExtractFilePath(ParamStr(0))+'data\';
end;
//-----------------------------------------------------------------------
function AddComas(inp : string; Freq : word) : string;
var
i,counter,sp : integer;
tmpS         : string;
begin
 tmpS := ''; Counter := -1;

 if Pos('.',Inp)<>0 then begin
                    sp := Pos('.',Inp)-1;
                    for i := Length(inp) downto sp do
                             Insert(inp[i],tmpS,1);
                    Counter := 0;
                    end else sp := Length(inp);

 for i:=sp downto 1 do
  begin
    inc(counter);
    if Counter=Freq then
       begin
        Insert(',',tmpS,1);
        Counter:=0;
       end;
    Insert(inp[i],tmpS,1);
  end;

 AddComas := tmpS;
end;
//-----------------------------------------------------------------------
function RemoveComas(inp : string) : string;
begin
 While Pos(',',inp)<>0 do Delete(inp,Pos(',',inp),1);
 RemoveComas := inp;
end;
//-----------------------------------------------------------------------
function FormatStr(inp : string; fLength : word) : string;
var
i,ol : integer;
begin
  ol := fLength-Length(inp)-1;
  for i:=0 to ol do inp:='0'+inp;
  FormatStr := inp;
end;
//-----------------------------------------------------------------------
function DateToLongStr(Date : tDateTime) : string;
var
Year,Month,Day : word;
begin
 DecodeDate(Date,Year,Month,Day);
 DateToLongStr :=  FormatStr(IntToStr(Day),2)+'/'
                  +FormatStr(IntToStr(Month),2)+'/'
                  +IntToStr(Year);
end;
//-----------------------------------------------------------------------
function RemoveSpaces(inp : string; RemMode : tRemMode) : string;
begin
  case RemMode of
    rmLeft: Result:= TrimLeft(Inp);
    rmRight: Result:= TrimRight(Inp);
    rmBoth: Result:= Trim(Inp);
  end;

{try
 if (RemMode = rmLeft) or (RemMode = rmBoth) then
             While Inp[1]=' ' do Delete(Inp,1,1);

 if (RemMode = rmRight) or (RemMode = rmBoth) then
             While Inp[Length(inp)]=' ' do Delete(Inp,Length(inp),1);

RemoveSpaces := inp;
except
RemoveSpaces := '';
end;}
end;
//-----------------------------------------------------------------------
function GridEmpty(grid : tStringGrid) : boolean;
var
Empty : boolean;
i     : integer;
begin
Empty := true;
for i:=0 to grid.ColCount-2 do
    if RemoveSpaces(grid.Cells[i,0],rmBoth)<>'' then Empty := false;
GridEmpty := Empty;
end;
//-----------------------------------------------------------------------
end.
