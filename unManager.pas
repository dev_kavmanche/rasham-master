unit unManager;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HGrids, SdGrid, cSTD, DataObjects;

type
  TfmManager = class(TfmBlankTemplate)
    Panel22: TPanel;
    Panel29: TPanel;
    Panel21: TPanel;
    Panel23: TPanel;
    grid5: TSdGrid;
    Bevel4: TBevel;
    gbSearch: TPanel;
    gb51: TPanel;
    gb52: TPanel;
    procedure gbSearchClick(Sender: TObject);
    procedure gb51Click(Sender: TObject);
    procedure gb52Click(Sender: TObject);
    procedure grid5KeyPress(Sender: TObject; var Key: Char);
    procedure grid5SelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure grid5SetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
    procedure grid5Exit(Sender: TObject);
  private
    { Private declarations }
    ManagerID: string;
    CurrManager: integer;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvDataLoaded; override;
    procedure EvDeleteEmptyRows; override;
    procedure EvFillDefaultData; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    function EvLoadData: Boolean; override;
    function EvCollectData(var NonDraft: TManagers): TManagers;
    procedure CheckManagerID;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmManager: TfmManager;

implementation

uses PeopleSearch, Util, dialog2, SaveDialog, LoadDialog, DataM;

{$R *.DFM}

procedure TfmManager.DeleteFile(FileItem: TFileItem);
var
  Managers: TManagers;
begin
  If Not FileItem.FileInfo.Draft Then
    Raise Exception.Create('This file is not Draft at TfmManager.DeleteFile');
  If (FileItem.FileInfo.Action <> FileAction) Then
    raise Exception.Create('Invalid file type at TfmManager.DeleteFile');
  Managers:= TManagers.LoadFromFile(Company, FileItem);
  Managers.FileDelete(FileItem);
  Managers.Free;
  FileItem.DeleteFile;
end;

function TfmManager.EvLoadData: Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  Managers: TManagers;
  I: Integer;
begin
  Result:= False;
  FileItem:= fmLoadDialog.Execute(Self, FilesList, Company.RecNum, FileAction);
  If FileItem = Nil Then
    Exit;
  FileName:= FileItem.FileInfo.FileName;
  DecisionDate:= (FileItem.FileInfo.DecisionDate);
  NotifyDate:= (FileItem.FileInfo.NotifyDate);
  Managers:= TManagers.LoadFromFile(Company, FileItem);
  If Managers.Count>0 then
    begin
      Grid5.RowCount:= Managers.Count;
      for I:= 0 to Managers.Count-1 do
        begin
          Grid5.Cells[0,I]:= Managers.Items[I].Name;
          Grid5.Cells[1,I]:= Managers.Items[I].Zeut;
          Grid5.Cells[2,I]:= (Managers.Items[I] as TManager).Address;
          Grid5.Cells[3,I]:= (Managers.Items[I] as TManager).Phone;
        end;
    end
  else
    begin
      Grid5.RowCount:= 1;
      Grid5.ResetLine(0);
    end;
  Result:= True;
  Grid5.Repaint;
  FileItem.Free;
  FilesList.Free;
end;

function TfmManager.EvCollectData(var NonDraft: TManagers): TManagers;

  function AddNext(const Name, Zeut, Address, Phone, Mail: String): TManager;
  var
    TempManager: TManager;
  begin
    Result := TManager.CreateNew(DecisionDate, Name, Zeut, Address, Phone, Mail);
    TempManager := Company.Managers.FindByZeut(Zeut) as TManager;
    if TempManager<> nil then
      if (TempManager.Name = Result.Name) and
         (TempManager.Address = Result.Address) and
         (TempManager.Phone = Result.Phone) then
        Exit
      else
        begin
          TempManager.DeActivate(DecisionDate);
          NonDraft.Add(TempManager);
          NonDraft.Add(Result);
        end
    else
      NonDraft.Add(Result);
  end;

var
  TempManager: TManager;
  I: Integer;
begin
  Company.Managers.Filter:= afActive;
  Result:= TManagers.CreateNew;
  NonDraft:= Tmanagers.CreateNew;
  For I:= 0 to Grid5.RowCount-1 do
    if not RowEmpty(Grid5.Rows[I]) then   //added by Tsahi 09/11/05 to prevent empty rows from being saved...
      Result.Add(AddNext(Grid5.Cells[0, I], Grid5.Cells[1, I], Grid5.Cells[2,I], Grid5.Cells[3,I], ''));

  for I:= Company.Managers.Count-1 downto 0 do
    begin
      TempManager:= Result.FindByZeut(Company.Managers.Items[I].Zeut) as TManager;
      if TempManager=nil then
        begin
          TempManager:= Company.Managers.Items[I] as TManager;
          TempManager.DeActivate(DecisionDate);
          NonDraft.Add(TempManager);
        end;
    end;
end;

function TfmManager.EvSaveData(Draft: Boolean): Boolean;
var
  FileItem: TFileItem;
  FileInfo: TFileInfo;
  FilesList: TFilesList;
  DraftManagers, NonDraft: TManagers;
begin
  Result:= False;
  try
    DraftManagers:= EvCollectData(NonDraft);
    FilesList:= TFilesList.Create(Company.RecNum, FileAction, Draft);
    FileInfo:= TFileInfo.Create;
    FileInfo.Action:= FileAction;
    FileInfo.Draft:= Draft;
    FileInfo.DecisionDate:= DecisionDate;
    FileInfo.NotifyDate:= NotifyDate;
    FileInfo.CompanyNum:= Company.RecNum;
    FileItem:= fmSaveDialog.Execute(Self, FilesList, FileInfo);
    If FileItem<>nil Then
      begin
        if not Draft then
          NonDraft.ExSaveData(FileItem, Company);//SaveData(Company, FileItem.FileInfo.RecNum);//
        FileItem.FileInfo.Draft:= True;
        DraftManagers.ExSaveData(FileItem, Company);

        FileName:= FileItem.FileInfo.FileName;
        FileItem.Free;
        fmDialog2.Show;
        Result:= True;
      end
    Else
      FileInfo.Free;
    DraftManagers.Free;
    NonDraft.Deconstruct;
    FilesList.Free;
  finally
  end;
end;

procedure TfmManager.gbSearchClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  TempCompany:= TCompany.Create(Company.RecNum, Company.ShowingDate, False);
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
    begin
      Grid5.SetFocus;
      TempCompany.Free;
      Exit;
    end;

  Loading:= True;
  If FResult is TStockHolder Then
    With FResult as TStockHolder do
      begin
        If Grid5.Cells[0,Grid5.RowCount-1] <> '' Then
          Grid5.RowCount:= Grid5.RowCount+1;
        Grid5.Cells[0,Grid5.RowCount-1]:= Name;
        Grid5.Cells[1,Grid5.RowCount-1]:= Zeut;
        Grid5.Cells[2,Grid5.RowCount-1]:= Address.Street+' '+Address.Number+' '+ Address.City+' '+Address.Index+' '+Address.Country;
        Grid5.Cells[3,Grid5.RowCount-1]:= '';
      end;

  If FResult is TDirector Then
    With FResult as TDirector do
      begin
        If Grid5.Cells[0,Grid5.RowCount-1] <> '' Then
          Grid5.RowCount:= Grid5.RowCount+1;
        Grid5.Cells[0,Grid5.RowCount-1]:= Name;
        Grid5.Cells[1,Grid5.RowCount-1]:= Zeut;
        Grid5.Cells[2,Grid5.RowCount-1]:= Address.Street+' '+Address.Number+' '+ Address.City+' '+Address.Index+' '+Address.Country;
        Grid5.Cells[3,Grid5.RowCount-1]:= Address.Phone;
      end;

  TempCompany.Free;
  Loading:= False;
  DataChanged:= True;
  Grid5.SetFocus;
end;


procedure TfmManager.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
begin
  Inherited EvCheckPage(Page, Silent, Extended, Draft);
  CheckGrid(Page, Silent, Extended, Grid5);
end;

procedure TfmManager.gb51Click(Sender: TObject);
begin
  inherited;
  Grid5.LineDelete;
  DataChanged:= True;
end;

procedure TfmManager.gb52Click(Sender: TObject);
begin
  inherited;
  Grid5.LineInsert;
  DataChanged:= True;
end;

procedure TfmManager.grid5KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  If Key > #32 Then
    Case Grid5.Col of
      0: If Length(Grid5.Cells[0, Grid5.Row]) > 79 Then
           Key:= #0;
      1: If (*Length(Grid5.Cells[1, Grid5.Row]) > 8) Or*) (Not (Key in ['0'..'9'])) Then
           Key:= #0;
      2: If Length(Grid5.Cells[2, Grid5.Row]) > 59 Then
           Key:= #0;
      3: If Length(Grid5.Cells[3, Grid5.Row]) > 14 Then
           Key:= #0;
    end;
end;

procedure TfmManager.grid5SelectCell(Sender: TObject; Col, Row: Integer;
  var CanSelect: Boolean);
begin
  inherited;

  if Sender=Grid5 then
    begin
      CheckManagerID;
      if Grid5.PCol = 1 then
        Grid5.Cells[Grid5.Pcol,Grid5.Prow] := Trim(Grid5.Cells[Grid5.Pcol,Grid5.Prow]);
       // Grid5.Cells[Grid5.Pcol,Grid5.Prow] := FormatNumber(Grid5.Cells[Grid5.Pcol,Grid5.Prow],9,0,True);
    end;
end;

procedure TfmManager.EvFocusNext(Sender: TObject);
begin
  inherited;
  If Sender = e14 Then
    grid5.SetFocus;
end;

procedure TfmManager.EvDataLoaded;
var
  I: Integer;
begin
  inherited;
  if DoNotClear Then
    Exit;
    
  Loading:= True;
  with Company do
    try
      grid5.RowCount:= 1;
      Grid5.Rows[0].Clear;
      Managers.Filter:= afActive;
      For I:= 0 To Managers.Count-1 do
        With Managers.Items[I] As TManager Do
          begin
            grid5.Cells[0,Grid5.RowCount-1] := Name;
            grid5.Cells[1,Grid5.RowCount-1] := Zeut;
            grid5.Cells[2,Grid5.RowCount-1] := Address;
            grid5.Cells[3,Grid5.RowCount-1] := Phone;
            grid5.RowCount:= grid5.RowCount+1;
          end;
        If Grid5.RowCount>1 Then
          Grid5.RowCount:= Grid5.RowCount-1;
        grid5.Repaint;
    finally
      Loading:= False;
    end;
end;

procedure TfmManager.EvDeleteEmptyRows;
var
  I: Integer;
begin
  With Grid5 Do
    For I:= RowCount-1 DownTo 0 Do
      If (RemoveSpaces(Cells[0,I], rmBoth) = '') And CheckNumberField(Cells[1,I]) And (RemoveSpaces(Cells[2,I], rmBoth) = '') And
         (RemoveSpaces(Cells[3,I], rmBoth) = '') Then
        LineDeleteByIndex(I);
end;

procedure TfmManager.EvFillDefaultData;
begin
  inherited;
  grid5.ResetLine(0);
end;

procedure TfmManager.FormCreate(Sender: TObject);
begin
  FileAction:= faManager;
  ShowStyle:= afActive;
  ManagerID:='';
  CurrManager:=0;
  inherited;

end;
//================================
procedure TfmManager.grid5SetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: String);
begin
  inherited;
  if (Acol = 1 ) then
  begin
    ManagerID:=Value;
    CurrManager:=ARow;
  end;
end;
//================================// Tsahi 10/11/05 fixes problem with inappropriate message when entering manager id
procedure TfmManager.CheckManagerID;
begin
  if (StrToIntDef(ManagerID,1) = 0) then
  begin
    Application.MessageBox(PChar('���� ���� ���� ���� ����� 0.'+#10#13+'�� �����.           '),
                           PChar('����'),MB_OK);
    Grid5.cells[1,CurrManager] := '';
  end;
  ManagerID:='';
  CurrManager:=0;
end;

procedure TfmManager.grid5Exit(Sender: TObject);
begin
  inherited;
  CheckManagerID;
end;

end.
