unit PrintSignName;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls,DataObjects;

type
  TSignature=record
    Name:string;
    Zeut:string;
    Tafkid:string;
  end;
  TfmQrSignName = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRImage5: TQRImage;
    QRLabel51: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRImage6: TQRImage;
    QRLabel1: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel30: TQRLabel;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRLabel37: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRBand8: TQRBand;
    QRShape43: TQRShape;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QuickRep2: TQuickRep;
    QRBand2: TQRBand;
    QRImage1: TQRImage;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel52: TQRLabel;
    QRImage2: TQRImage;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRShape48: TQRShape;
    QRCompositeReport1: TQRCompositeReport;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRShape53: TQRShape;
    QRShape54: TQRShape;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRShape55: TQRShape;
    QRLabel76: TQRLabel;
    QRShape56: TQRShape;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRShape57: TQRShape;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRShape66: TQRShape;
    QRLabel92: TQRLabel;
    QRBand3: TQRBand;
    QRShape67: TQRShape;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    edtCompanyName: TQRLabel;
    edtCompanyNum1: TQRLabel;
    edtCompanyNum3: TQRLabel;
    edtCompanyNum2: TQRLabel;
    edtCompanyNum4: TQRLabel;
    edtCompanyNum5: TQRLabel;
    edtCompanyNum6: TQRLabel;
    edtCompanyNum7: TQRLabel;
    edtCompanyNum8: TQRLabel;
    edtCompanyNum9: TQRLabel;
    edtSigName: TQRLabel;
    edtSigID9: TQRLabel;
    edtSigID8: TQRLabel;
    edtSigID7: TQRLabel;
    edtSigID6: TQRLabel;
    edtSigID5: TQRLabel;
    edtSigID4: TQRLabel;
    edtSigID3: TQRLabel;
    edtSigID2: TQRLabel;
    edtSigID1: TQRLabel;
    edtPrevSigName: TQRLabel;
    edtPrevSigID1: TQRLabel;
    edtMemaleName: TQRLabel;
    edtMemaleID: TQRLabel;
    edtDate: TQRLabel;
    edtLawName: TQRLabel;
    edtMemaleRole: TQRLabel;
    edtSigName2: TQRLabel;
    edtSigID: TQRLabel;
    edtLawName2: TQRLabel;
    edtLawID: TQRLabel;
    edtLawAddress: TQRLabel;
    edtLawLicence: TQRLabel;
    QRLabel100: TQRLabel;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    edtPrevSigID2: TQRLabel;
    QRShape36: TQRShape;
    QRLabel103: TQRLabel;
    QRShape37: TQRShape;
    edtPrevSigID3: TQRLabel;
    QRShape38: TQRShape;
    edtPrevSigID4: TQRLabel;
    QRShape39: TQRShape;
    edtPrevSigID5: TQRLabel;
    QRShape40: TQRShape;
    edtPrevSigID6: TQRLabel;
    QRShape41: TQRShape;
    edtPrevSigID7: TQRLabel;
    QRShape42: TQRShape;
    edtPrevSgID8: TQRLabel;
    QRShape68: TQRShape;
    edtPrevSigID9: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel173: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel101: TQRLabel;
    QRShape52: TQRShape;
    QRLabel73: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRShape19: TQRShape;
    QRShape27: TQRShape;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRShape28: TQRShape;
    QRLabel110: TQRLabel;
    QRShape49: TQRShape;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRShape50: TQRShape;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel122: TQRLabel;
    QRShape51: TQRShape;
    QRLabel123: TQRLabel;
    procedure QRCompositeReport1AddReports(Sender: TObject);
  private
    { Private declarations }
    procedure PrintProc(DoPrint: Boolean);
    procedure LoadData(ACompany:TCompany;ASignature:TSignature);
  public
    { Public declarations }
    procedure PrintEmpty(const FileName: String);
    procedure PrintExecute(const FileName: string; ACompany: TCompany;ASignature:TSignature);
  end;

var
  fmQrSignName: TfmQrSignName;

implementation

uses PrinterSetup;

{$R *.DFM}
procedure TfmQrSignName.LoadData(ACompany:TCompany;ASignature:TSignature);
var
  i:integer;
  s:string;
begin
  if ACompany=Nil then
    begin
      for i:=0 to ComponentCount-1 do
        if (Components[i] is TQRLabel)and(uppercase(Copy(Components[i].Name,1,3))='EDT') then
            (Components[i] as TQRLabel).Caption:='';
      exit;
    end;
  edtCompanyName.Caption:=ACompany.Name;
  edtSigName.Caption:=ASignature.Name;
  s:=trim(ACompany.Zeut);
  while length(s)<9 do
    s:='0'+s;
  for i:=1 to 9 do
    (FindComponent('edtCompanyNum'+IntToStr(i)) as TQRLabel).Caption:=s[10-i];
  s:=trim(ASignature.Zeut);
  while length(s)<9 do
    s:='0'+s;
  for i:=1 to 9 do
    (FindComponent('edtSigID'+IntToStr(i)) as TQRLabel).Caption:=s[10-i];
  edtPrevSigName.Caption:=ACompany.CompanyInfo.SigName;
  s:=trim(ACompany.CompanyInfo.SigZeut);
  while length(s)<9 do
    s:='0'+s;
  for i:=1 to 9 do
    (FindComponent('edtPrevSigID'+IntToStr(i)) as TQRLabel).Caption:=s[10-i];

  edtMemaleName.Caption   := ACompany.CompanyInfo.SigName;
  edtMemaleID.Caption     := ACompany.CompanyInfo.SigZeut;
  edtMemaleRole.Caption   := ACompany.CompanyInfo.SigTafkid;
  edtDate.Caption         := DateToStr(Now);
  edtLawName.Caption      := ACompany.CompanyInfo.LawName;
  edtSigName2.Caption     := ACompany.CompanyInfo.SigName;
  edtSigID.Caption        := ACompany.CompanyInfo.SigZeut;
  edtLawName2.Caption     := ACompany.CompanyInfo.LawName;
  edtLawID.Caption        := ACompany.CompanyInfo.LawZeut;
  edtLawAddress.Caption   := ACompany.CompanyInfo.LawAddress;
  edtLawLicence.Caption   := ACompany.CompanyInfo.LawZeut;
end; // TfmQrSignName.LoadData

procedure TfmQrSignName.PrintEmpty(const FileName: String);
var sig:TSignature;
begin
  LoadData(nil,sig);
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQrSignName.PrintExecute(const FileName: string; ACompany: TCompany;ASignature:TSignature);
begin
   LoadData(ACompany, ASignature);
   fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQrSignName.PrintProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(QuickRep1);
  fmPrinterSetup.FormatQR(QuickRep2);
  fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
  if (DoPrint) then
    QRCompositeReport1.Print
  else
  begin
    QuickRep1.Preview;
    Application.ProcessMessages;

    QuickRep2.Preview;
  end;
end;

procedure TfmQrSignName.QRCompositeReport1AddReports(Sender: TObject);
var i:integer;
begin
  for i:=1 to 2 do
    QRCompositeReport1.Reports.Add(FindComponent('QuickRep'+IntToStr(i)) as TQuickRep);
end;

end.
