unit urRshmOneHinstCheckAndLog;

interface

uses
  windows
  ,Db
  ,DBTables
  ,Bde
  ;

type

  TSICheckAndLog = class
    private
      fKeyNo: integer;
      fKeyIp: string;
      fKeyCNm: string;
      fKeyUnm: string;
      fAppMutex: THandle;
      fDb: TDatabase;
      fLogTbl: TTable;
      function GetLogTbl(const aTblName: string): TTable;

    protected
      constructor Create;
      procedure Activate;
      procedure FreeAppMutex;
      procedure WriteLogOff;
      procedure WriteLogOn;
      function GetAppMutex: THandle;

    public
      class function Instance: TSICheckAndLog;
      destructor Destroy;                           override;
      function LogTo(const aDBPath: string): TSICheckAndLog;
      function IsUnique: boolean;
  end;

implementation

{
  if not TSICheckAndLog.Instance.isUnique(dbPath) then
    exit;
}
uses
 SysUtils
 ,dialogs
 ,SyncObjs
 ,utils2
 ,uAppSysUtils
 ;

const
  C_APP_MUTEX = 'hashavim_{B5F3AEDA-4104-40CC-9596-8D591AAE2EAC}';
  C_APP_LOG_TBL = 'RashamLogonInfo.db';
var
  uInst: TSICheckAndLog;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
class function TSICheckAndLog.Instance: TSICheckAndLog;
var
  cs: TCriticalSection;
begin
  cs := TCriticalSection.Create;
  try
    if uInst = nil then
    begin
      cs.Enter;
      if uInst = nil then
        try
          uInst := TSICheckAndLog.Create;
        finally
          cs.Leave;
        end;
    end;
    result := uInst;
  finally
    cs.free;
  end;
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
constructor TSICheckAndLog.Create;
begin
  fAppMutex := 0;
  fKeyNo := -1;
  fDb := nil;
  fLogTbl := nil;
  fKeyIp := '';
  fKeyCNm := '';
  fKeyUnm := '';
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
destructor TSICheckAndLog.Destroy;
begin
  FreeAppMutex;
  if assigned(fLogTbl) then
    try
      fLogTbl.Active := true;
      WriteLogOff;
    finally
      fLogTbl.close;
      fLogTbl.free;
    end;
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
function TSICheckAndLog.IsUnique: boolean;
begin
  result := fAppMutex <> 0;
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
function TSICheckAndLog.LogTo(const aDBPath: string): TSICheckAndLog;
var
 valStr: string;
begin
  result := self;
  valStr := aDBPath;
  If valStr[Length(valStr)] = '\' then
    Delete(valStr, Length(valStr), 1);
  if DirectoryExists(valStr) then
  begin
    if fLogTbl <> nil then
      fLogTbl.Free;
    fLogTbl := GetLogTbl(valStr + '\' + C_APP_LOG_TBL);
    WriteLogOn;
  end;
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
function TSICheckAndLog.GetAppMutex: THandle;
begin
  result := CreateMutex (nil, True, C_APP_MUTEX);
  if (result = 0) or (GetLastError = ERROR_ALREADY_EXISTS) then
    result := 0;
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
procedure TSICheckAndLog.FreeAppMutex;
begin
  if fAppMutex <> 0 then
    CloseHandle(fAppMutex);
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
procedure TSICheckAndLog.Activate;
begin
  fAppMutex := GetAppMutex;
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
function TSICheckAndLog.GetLogTbl(const aTblName: string): TTable;
begin
  result := TTable.Create(nil);
  result.TableType := ttParadox;
  result.tablename := aTblName;
  if not FileExists(aTblName) then
    try
      result.FieldDefs.Clear;
      result.FieldDefs.Add('KeyNo',    ftAutoInc,   0, True);
      result.FieldDefs.Add('UserName', ftString,  128, false);
      result.FieldDefs.Add('CompName', ftString,  128, false);
      result.FieldDefs.Add('CompIp',   ftString,   20, false);
      result.FieldDefs.Add('LoginDt',  ftDateTime,  0, false);
      result.FieldDefs.Add('logoutDt', ftDateTime,  0, false);
      result.IndexDefs.Clear;
      result.IndexDefs.Add('keyNoIdx','keyNo', [ixPrimary, ixUnique]);
      result.CreateTable;
      result.Open;
      result.close;
    except
      on e: exception do
        Showmessage(e.message);
    end;
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
procedure TSICheckAndLog.WriteLogOff;
begin
  if Assigned(fLogTbl) then
  begin
    fLogTbl.Open;
    try
      try
        fLogTbl.FindKey([fKeyNo]);
        fLogTbl.Edit;
        fLogTbl.FieldByName('LogoutDt').AsDateTime := Now;
        fLogTbl.Post;
      finally
        fLogTbl.Close;
      end;
    except
      on e: exception do
        Showmessage(e.message);
    end;
  end;
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
procedure TSICheckAndLog.WriteLogOn;
begin
  if Assigned(fLogTbl) then
  begin
    fLogTbl.Open;
    try
      try
        fLogTbl.Insert;
        fLogTbl.FieldByName('LoginDt').AsDateTime := Now;
        fLogTbl.FieldByName('UserName').AsString := TAppSysUtils.GetTheUserDomain + '\' +TAppSysUtils.GetTheUserName;
        fLogTbl.FieldByName('CompName').AsString := TAppSysUtils.GetTheComputerName;
        fLogTbl.FieldByName('CompIp').AsString := TAppSysUtils.GetIPAddress;
        fLogTbl.Post;
        fLogTbl.Last;
        fKeyNo := fLogTbl.FieldByName('KeyNo').AsInteger;
      finally
        fLogTbl.Close;
      end;
    except
      on e: exception do
        Showmessage(e.message);
    end;
  end;
end;


initialization
  uInst := nil;
  TSICheckAndLog.Instance.Activate;

finalization
  if assigned(uInst) then
    uInst.free;
end.
