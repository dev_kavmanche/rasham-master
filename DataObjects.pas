{***************************************************************
Unit Name: DataObjects
Purpose  :  base Data manipulation classes
Author   :  N/A

HISTORY
DATE        VERSION   BY   TASK      DESCRIPTION
26/08/2019  1.7.7     sts            code refactoring
02/07/2020  1.8.3     sts  20808     code refactoring(remove unnesesary with ... do)
23/12/2020  1.8.4     sts  22057     refactoring
21/04/2021  1.8.4     sts  22596     fixed bug with exption when option3 is emptystr
13/12/2021  1.9.0     sts  24327     fixed bug with wron using function argument 
}


unit DataObjects;

interface

uses DataM, Windows, Messages, SysUtils, Classes, DBTables,forms,Dialogs,db, utils2;


Type
  TActivityFilter = (afAll, afActive, afNonActive);
              //  0           1                2              3              4                 5           6           7          8            9            :                  ;               <              =          >
  TAction      = (acAddress,  acTaagidAddress, acNewDirector, acNewManager,  acNewStockHolder, acNewStock, acSignName, acDelete,  acNewHon,    acNewMagish, acNewMuhaz,        acRecNum,       acNewName,     acNewZeut, acSubmissionData);
  TObjectClass = (ocCompany,  ocHon,           ocDirector,    ocStockHolder, ocStock,          ocMuhaz,    ocManager,  ocTaagid);
  TFileAction  = (faRishum,   faNumber,        faIncrease,    faDecrease,    faHakzaa,         faHaavara,  faDirector, faAddress, faAddingNew, faDoh,       faStockHolderInfo, faDirectorInfo, faCompanyName, faHamara,  faManager,  faSignName, faEngName, faZipCode, faTakanon, faAll, faNone);
  TPersonType  = (diDirector, diStockHolder,   diBoth,        diManager,     diSignName );

  TEnumChangesProc = function (Line: Integer; ChangeDate: TDateTime; Param: Pointer): Boolean of object;
  TFileEnumProc = procedure (ObjectNum, NewNum: Integer) of Object;

  TMultipleSelectRec = record
    CompanyName: string;
    CompanyNum: integer;
    DirectorNum: integer;
    StockHolderNum: integer;
    ManagerNum: integer;
    SignNameNum : integer;
    PersonType: TPersonType;
  end;

  PMultipleSelectRec = ^TMultipleSelectRec;


const
  MultiActions = [faStockHolderInfo, faDirectorInfo, faCompanyName, faDirector, faAddress, faIncrease, faDecrease, faHaavara, faHakzaa, faDoh, faManager, faHamara, faZipCode];

  DefaultTakanon2 = '{\rtf1\fbidis\ansi\deff0{\fonttbl{\f0\fnil\fcharset177 David;}}' +
                    '\viewkind4\uc1\pard\rtlpar\qr\lang1037\f0\rtlch\fs26' +
                    '\par }';
  DefaultTakanon4 = '{\rtf1\fbidis\ansi\deff0{\fonttbl{\f0\fnil\fcharset177 David;}}' +
                    '\viewkind4\uc1\pard\rtlpar\qr\lang1037\f0\rtlch\fs26' +
                    '\par }';
  DefaultTakanon5 = '{\rtf1\fbidis\ansi\deff0{\fonttbl{\f0\fnil\fcharset177 David;}}\viewkind4\uc1\pard\rtlpar\qr\lang1037\f0\rtlch\fs24 \ul\b\''e4\''f2\''e1\''f8\''fa \''ee\''f0\''e9\''e5\''fa, \''e4\''f6\''f2\''fa \''ee\''f0\''e9\''e5\''fa \''e5\''e0' +
                    '\''e2\''e9\''f8\''fa \''e7\''e5\''e1 \''ec\''f6\''e9\''e1\''e5\''f8, \''ee\''f1\''f4\''f8 \''e1\''f2\''ec\''e9 \''e4\''ee\''f0\''e9\''e5\''fa\ulnone\b0 \par\par \''e0.\tab\''eb\''ec \''e4\''f2\''e1\''f8\''fa \''ee\''f0\''e9\''e5\''fa \''e8\''f2' +
                    '\''e5\''f0\''e4 \''e0\''e9\''f9\''e5\''f8 \''e4\''e3\''e9\''f8\''f7\''e8\''e5\''f8\''e9\''ed.\par \''e1.\tab\''e0\''f1\''e5\''f8 \''ec\''e7\''e1\''f8\''e4 \''ec\''e4\''f6\''e9\''f2 \''ee\''f0\''e9\''e5\''fa \''e5/\''e0\''e5 \''e0\''e2' +
                    '\''f8\''e5\''fa \''e7\''e5\''e1 \''ec\''f6\''e9\''e1\''e5\''f8.\par \''e2.\tab\''ee\''f1\''f4\''f8 \''e1\''f2\''ec\''e9 \''e4\''ee\''f0\''e9\''e5\''fa \''ec\''e0 \''e9\''f2\''ec\''e4 \''f2\''ec \''e7\''ee\''e9\''f9\''e9\''ed, \''ee' +
                    '\''ec\''e1\''e3 \''f2\''e5\''e1\''e3\''e9 \''e4\''e7\''e1\''f8\''e4 \''e0\''e5 \''ee\''e9 \''f9\''e4\''e9\''e5 \''f2\''e5\''e1\''e3\''e9\''e4 \''e5\''e1\''e4\''e9\''e5\''fa\''ed \''f2\''e5\''e1\''e3\''e9\''e4 \par \tab\''e5\''e0\''f3 ' +
                    '\''ec\''e0\''e7\''f8 \''f9\''e4\''e5\''f4\''f1\''f7\''e4 \''f2\''e1\''e5\''e3\''fa\''ed \''e4\''ed \''ee\''e5\''f1\''e9\''f4\''e9\''ed \''ec\''e4\''e9\''e5\''fa \''e1\''f2\''ec\''e9 \''ee\''f0\''e9\''e5\''fa \''e1\''e7\''e1\''f8\''e4.\par\par }';
  DefaultTakanon7 = '{\rtf1\fbidis\ansi\ansicpg1255\deff0{\fonttbl{\f0\fnil\fcharset177 David;}}' +
                    '\viewkind4\uc1\pard\rtlpar\qr\lang1037\ul\b\f0\rtlch\fs26\''e4\''e5\''f8\''e0\''e5\''fa \''f0\''e5\''f1\''f4\''e5\''fa\b0'+
                    '\par \par \ulnone     ____________________________________________________________________' +
                    '\par\par     ____________________________________________________________________\par\par }';

type
  // Forward Diclaration
  //....................
  TDirectors = Class;
  TCompany = Class;
  TChanges = Class;
  TStockHolder = Class;
  TStockHolders = Class;
  THonList = Class;
  THonItem = Class;
  TManager = Class;
  TManagers = Class;
  TStocks = Class;
  TStock = Class;
  TCompanyInfo = Class;
  TDirector = Class;
  TFilesList = Class;
  TFileItem = Class;
  TMuhazItem = Class;
  TMuhazList = Class;
  TFileInfo = Class;
  //v 9.0 : Feb 2011: submission data
  TSubmissionData = class;
  TEnumFileItemProc = function (ObjectType: TObjectClass; Action: TAction; ObjectNum, NewNum: Integer; FileItem: TFileItem): Boolean of Object;

  // Base types
  //++++++++++++++++++

  // Address
  //........
  TBasicAddress = class
  private
    FRecNum : Integer;
    FStreet : String;
    FNumber : String;
    FIndex  : String;
    FCity   : String;
    FCountry: String;
  public
    constructor CreateNew(Const Street, Number, Index, City, Country: String);

    property RecNum : Integer read FRecNum;
    property Street : String read FStreet;
    property Number : String read FNumber;
    property Index  : String read FIndex write FIndex;
    property City   : String read FCity;
    property Country: String read FCountry;
  end;

  TStockHolderAddress = class(TBasicAddress)
  private
    FFileItem: TFileItem;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    procedure DeleteEnum(ObjectNum, NewNum: Integer);
  public
    class procedure Undo(RecNum: Integer);
    procedure ExSaveData(FileItem: TFileItem; Parent: TStockHolder; ChangeDate: TDateTime);
    procedure SaveData(Parent: TStockHolder; ChangeDate: TDateTime; Draft: Boolean; FileNum: Integer);
    procedure FileDelete(Parent: TStockHolder; FileItem: TFileItem);
    constructor Create(RecNum: Integer);
    constructor LoadFromFile(ParentNum: Integer; FileItem: TFileItem);
  end;

  TCompanyAddress = class(TBasicAddress)
  private
    FPOB    : String;
    FEzel   : String;
    FPhone  : String;
    FFax    : String;
    FFileItem: TFileItem;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    procedure DeleteEnum(ObjectNum, NewNum: Integer);
  public
    constructor CreateNew(Const Street, Number, Index, City, Country, POB, Ezel, Phone, Fax: String);
    constructor LoadFromFile(ParentNum: Integer; FileItem: TFileItem);

    procedure FileDelete(Parent: TCompany; FileItem: TFileItem);
    procedure ExSaveData(FileItem: TFileItem; Parent: TCompany; ChangeDate: TDateTime);
    procedure SaveData(Parent: TCompany; ChangeDate: TDateTime; Draft: Boolean; FileNum: Integer); // Use it only when created with CreateNew constructor

    property POB    : String read FPOB;
    property Ezel   : String read FEzel;
    property Phone  : String read FPhone;
    property Fax    : String read FFax;
  end;

  TTaagid = class(TBasicAddress)
  private
    FName   : String;
    FZeut   : String;
    FFileItem: TFileItem;

    procedure DeleteEnum(ObjectNum, NewNum: Integer);
    procedure LoadEnum(ObjectNum, NewNum: Integer);
  public
    class procedure Undo(RecNum: Integer);

    constructor LoadFromFile(ParentNum: Integer; FileItem: TFileItem);
    constructor Create(RecNum: Integer);
    constructor CreateNew(const Street, Number, Index, City, Country, Name, Zeut: String);

    procedure FileDelete(Parent: TDirector; FileItem: TFileItem);
    procedure ExSaveData(FileItem: TFileItem; Parent: TDirector; ChangeDate: TDateTime);
    procedure SaveData(Parent: TDirector; ChangeDate: TDateTime; Draft: Boolean; FileNum: Integer); // Use it only when created with CreateNew constructor

    property Name: String read FName write FName;
    property Zeut: String read FZeut write FZeut;
  end;

  TMagish = Class(TBasicAddress)
  private
    FName: String;
    FZeut: String;
    FPhone: String;
    FTaagid: Boolean;
    FRecNum: Integer;
    FMail: String;
    FFax: String;
  public
    class procedure Undo(RecNum: integer);

    constructor LoadFromFile(ParentNum: Integer; FileItem: TFileItem);
    constructor CreateNew(Const Name, Zeut, Street, Number, Index, City, Country, Phone, Mail, Fax: String; Taagid: Boolean);
    constructor Create(Parent: TCompany);

    procedure AppendRec;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    procedure FileDelete(FileItem:TFileItem);
    procedure SaveData(Parent: TCompany; FileItem: TFileItem);
    procedure SaveData2(Parent:TCompany; FileNum:integer; WithChanges,Draft:boolean);
    procedure SaveFixed(Parent: TCompany; FileNum: Integer);
    procedure ExSaveData(FileItem:TFileItem;Parent:TCompany);
    function UpdateData(RecNum: integer): boolean;

    property RecNum: Integer read FRecNum;
    property Phone: String read FPhone;
    property Taagid: Boolean read FTaagid;
    property Name: String read FName;
    property Zeut: String read FZeut;
    property Mail: string read FMail;
    property Fax: string read FFax;
  end;

  TDirectorAddress = class(TBasicAddress)
  private
    FPhone  : String;
    FFileItem: TFileItem;

    procedure DeleteEnum(ObjectNum, NewNum: Integer);
    procedure LoadEnum(ObjectNum, NewNum: Integer);
  public
    class procedure Undo(RecNum: Integer);
    constructor Create(RecNum: Integer);
    constructor CreateNew(Const Street, Number, Index, City, Country, Phone: String);
    constructor LoadFromFile(ParentNum: Integer; FileItem: TFileItem);
    procedure FileDelete(Parent: TDirector; FileItem: TFileItem);
    procedure ExSaveData(FileItem: TFileItem; Parent: TDirector; ChangeDate: TDateTime);
    procedure SaveData(Parent: TDirector; ChangeDate: TDateTime; Draft: Boolean; FileNum: Integer); // Use it only when created with CreateNew constructor
    property Phone: String read FPhone;
  end;

  // Main Classes
  //+++++++++++++

  // TBasicClass - Parent class for all objects
  //..........................................
  TBasicClass = Class
  private
    FParent: TCompany;
    FLastChanged: TDateTime;
  public
    constructor CreateNew(LastChanged: TDateTime);
    constructor Create(Parent: TCompany);

    property Parent: TCompany read FParent;
    property LastChanged: TDateTime read FLastChanged write FLastChanged;
  end;

  TChanges = Class(TBasicClass)
  private
    Table: TTable;
    class function GetParent(ObjectType: TObjectClass; Action: TAction; RecNum: Integer): Integer;
    function Enumeration(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
  public
    constructor Create(Parent: TCompany);
    destructor Destroy; override;
    procedure FileDelete(ObjectType: TObjectClass; Number: Integer; Action: TAction; RecNum: Integer);
    function GetLastChange(ObjectType: TObjectClass; Number: Integer; Action: TAction): Integer;
    procedure EnumFileItem(FileItem: TFileItem; EnumProc: TEnumFileItemProc);
    procedure EnumChanges(ObjectType: TObjectClass; Number: Integer; Action: TAction; EnumProc: TEnumChangesProc; EnumParam: Pointer);
    procedure SaveChanges(ObjectType: TObjectClass; Number: Integer; Action: TAction; NewLine: Integer; ChangeDate: TDateTime; FileNum: Integer);
    procedure ChangeRegistrationDate(ObjectType: TObjectClass; Number: Integer; Action: TAction; NewLine: Integer; Date: TDateTime);
    function GetStockInfo(OriginalStocks: TStocks; FileItem: TFileItem; Stock: TStock): TStock; // For BookStock
    class procedure UndoFile(FileItem: TFileItem);
    function GetLatestChangeFileNumber(ObjectType: TObjectClass; Number: Integer; Action: TAction): Integer;
  end;

  TCompanyInfo = Class(TBasicClass)
  private
    FRecNum       : Integer;
    FSigName      : String;
    FSigZeut      : String;
    FSigTafkid    : String;
    FLawName      : String;
    FLawAddress   : String;
    FLawZeut      : String;
    FLawLicence   : String;
    FOption1      : Byte;
    FOption3      : Char;
    FOtherInfo    : String;
    FFileItem     : TFileItem;
    FRegistrationDate: TDateTime;
    FTakanon2,
    FTakanon2_1,
    FTakanon2_2,
    FTakanon2_3,
    FTakanon2_4,
    FTakanon4,
    FTakanon5,
    FTakanon7     : String;
    FEnglishName  : String;
    FCompanyName2 : String;
    FCompanyName3 : String;
    FMail         : String;
    FSharesRestrictIndex: integer;
    FDontOfferIndex: integer;
    FShareHoldersNoIndex: integer;
    FSharesRestrictChapters: String;
    FDontOfferChapters: String;
    FShareHoldersNoChapters: String;
    FLimitedSignatoryIndex: integer;
    FLegalSectionsChapters: String;
    FSignatorySectionsChapters: String;
    FConfirmedCopies: String;
    FMoreCopies: String;
    FRegulationItemsIndex: integer;

    procedure DeleteEnum(ObjectNum, NewNum: Integer);
    procedure WriteRegistrationDate(Value: TDateTime);
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    procedure SetTakanon2(Value: String);
    procedure SetTakanon2_1(Value: String);
    procedure SetTakanon4(Value: String);
    procedure SetTakanon5(Value: String);
    procedure SetTakanon7(Value: String);
  public
    class procedure UndoSignName(RecNum: integer);

    constructor LoadFromFile(Parent: TCompany; FileItem: TFileItem);
    constructor CreateNew(LastChanged: TDateTime);
    constructor Create(RecNum: Integer; Parent: TCompany);

    procedure FileDelete(FileItem: TFileItem);
    procedure ExSaveData(FileItem: TFileItem; Parent: TCompany);
    procedure SetRegistrationDate(Value: TDateTime; Changes: TChanges);
    procedure SaveData(aParent: TCompany; aDraft: Boolean; aFileNum: Integer);
    procedure SetEnglishName(EngName: String; Parent: TCompany; DecisionDate: TDateTime);
    function UpdateData(Parent: TCompany; RecNum: integer): boolean;

    property SigName                  : String    read FSigName                   write FSigName;
    property RecNum                   : Integer   read FRecNum                    write FRecNum;
    property SigZeut                  : String    read FSigZeut                   write FSigZeut;
    property SigTafkid                : String    read FSigTafkid                 write FSigTafkid;
    property LawName                  : String    read FLawName                   write FLawName;
    property LawAddress               : String    read FLawAddress                write FLawAddress;
    property LawZeut                  : String    read FLawZeut                   write FLawZeut;
    property LawLicence               : String    read FLawLicence                write FLawLicence;
    property Option1                  : Byte      read FOption1                   write FOption1;
    property Option3                  : Char      read FOption3                   write FOption3;
    property OtherInfo                : String    read FOtherInfo                 write FOtherInfo;
    property Takanon2                 : String    read FTakanon2                  write SetTakanon2;
    property Takanon2_1               : String    read FTakanon2_1                write SetTakanon2_1;
    property Takanon2_2               : String    read FTakanon2_2                write FTakanon2_2;
    property Takanon2_3               : String    read FTakanon2_3                write FTakanon2_3;
    property Takanon2_4               : String    read FTakanon2_4                write FTakanon2_4;
    property Takanon4                 : String    read FTakanon4                  write SetTakanon4;
    property Takanon5                 : String    read FTakanon5                  write SetTakanon5;
    property Takanon7                 : String    read FTakanon7                  write SetTakanon7;
    property EnglishName              : String    read FEnglishName               write FEnglishName;
    property RegistrationDate         : TDateTime read FRegistrationDate          write FRegistrationDate;
    property CompanyName2             : String    read FCompanyName2              write FCompanyName2;
    property CompanyName3             : String    read FCompanyName3              write FCompanyName3;
    property Mail                     : String    read FMail                      write FMail;
    property SharesRestrictIndex      : integer   read FSharesRestrictIndex       write FSharesRestrictIndex;
    property DontOfferIndex           : integer   read FDontOfferIndex            write FDontOfferIndex;
    property ShareHoldersNoIndex      : integer   read FShareHoldersNoIndex       write FShareHoldersNoIndex;
    property SharesRestrictChapters   : String    read FSharesRestrictChapters    write FSharesRestrictChapters;
    property DontOfferChapters        : String    read FDontOfferChapters         write FDontOfferChapters;
    property ShareHoldersNoChapters   : String    read FShareHoldersNoChapters    write FShareHoldersNoChapters;
    property LimitedSignatoryIndex    : integer   read FLimitedSignatoryIndex     write FLimitedSignatoryIndex;
    property LegalSectionsChapters    : String    read FLegalSectionsChapters     write FLegalSectionsChapters;
    property SignatorySectionsChapters: String    read FSignatorySectionsChapters write FSignatorySectionsChapters;
    property ConfirmedCopies          : String    read FConfirmedCopies           write FConfirmedCopies;
    property MoreCopies               : String    read FMoreCopies                write FMoreCopies;
    property RegulationItemsIndex     : integer   read FRegulationItemsIndex      write FRegulationItemsIndex;
  end;

  TListedClass = Class(TBasicClass)
  private
    FZeut: String;
    FName: String;
    FRecNum: Integer;
    FActive: Boolean;
  protected
    function GetZeutProc(Line: Integer; ChangeDate: TDateTime; Param: Pointer): Boolean;
    function GetNameProc(Line: Integer; ChangeDate: TDateTime; Param: Pointer): Boolean;
    procedure ChangeSigName(CompanyNum: integer; NewZeut, NewName: string; FileInfo: TFileInfo);
  public
    constructor Create(Parent: TCompany; LastChanged: TDateTime);
    constructor CreateNew(LastChanged: TDateTime; Const Name, Zeut: String);

    procedure FileDelete(FileItem: TFileItem); virtual; abstract;
    procedure ExSaveData(FileItem: TFileItem; Parent: TBasicClass); virtual; abstract;
    procedure DeActivate(ChangeDate: TDateTime);
    procedure SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer); virtual; abstract; // Use it only when created with CreateNew constructor
    procedure SetRegistrationDate(Value: TDateTime; Changes: TChanges); virtual; abstract;

    property Name: String read FName;
    property Zeut: String read FZeut;
    property RecNum: Integer read FRecNum;
    property Active: Boolean read FActive write FActive;
  end;

  TStock = class(TListedClass)
  private
    FValue: Double;
    FCount: Double;
    FPaid : Double;
    FParent: TStockHolder;
  public
    class procedure Undo(RecNum: Integer);

    constructor Create(RecNum: Integer; Parent: TStockHolder; LastChanged: TDateTime);
    constructor CreateNew(LastChanged: TDateTime; Const Name: String; Value, Count, Paid: Double; Active: Boolean);
    constructor LoadFromFile(Parent: TStockHolder; RecNum: Integer);

    procedure FileDelete(FileItem: TFileItem); override;
    procedure ExSaveData(FileItem: TFileItem; Parent: TBasicClass); override;
    procedure SetRegistrationDate(Value: TDateTime; Changes: TChanges); override;
    procedure SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer); override;

    property Parent: TStockHolder read FParent;
    property Value : Double read FValue;
    property Count : Double read FCount;
    property Paid  : Double read FPaid;
  end;

  TMuhazItem = Class(TListedClass)
  private
    FValue: Double;
    FShtarValue: Double;
  public
    class procedure Undo(RecNum: Integer);

    constructor Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);
    constructor CreateNew(LastChanged: TDateTime; Const Name, Zeut: String; Value, ShtarValue: Double; Active: Boolean);
    constructor LoadFromFile(Parent: TCompany; RecNum: Integer);

    procedure FileDelete(FileItem: TFileItem); override;
    procedure ExSaveData(FileItem: TFileItem; Parent: TBasicClass); override;
    procedure SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer); override;
    procedure SetRegistrationDate(Value: TDateTime; Changes: TChanges); override;
    property Value: Double read FValue;
    property ShtarValue: Double read FShtarValue;
  end;

  THonItem = class(TListedClass)
  private
    FValue:     Double;
    FRashum:    Double;
    FMokza:     Double;
    FCash:      Double;
    FNonCash:   Double;
    FPart:      Double;
    FPartValue: Double;
  public
    class procedure Undo(RecNum: Integer);

    constructor LoadFromFile(Parent: TCompany; RecNum: Integer);
    constructor CreateNew(LastChanged: TDateTime; Const Name: String; Value, Rashum, Mokza, Cash, NonCash, Part, PartValue: Double; Active: Boolean);
    constructor Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);

    procedure FileDelete(FileItem: TFileItem); override;
    procedure SetRegistrationDate(Value: TDateTime; Changes: TChanges); override;
    procedure ExSaveData(FileItem: TFileItem; Parent: TBasicClass); override;
    procedure SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer); override;

    property Value:     Double read FValue;
    property Rashum:    Double read FRashum;
    property Mokza:     Double read FMokza;
    property Cash:      Double read FCash;
    property NonCash:   Double read FNonCash;
    property Part:      Double read FPart;
    property PartValue: Double read FPartValue;
  end;

  TManager = Class(TListedClass)
  private
    FPhone: String;
    FAddress: String;
    FMail: String;
  public
    class procedure Undo(RecNum: Integer);
    class procedure UndoManagerChange(RecNum: Integer);

    constructor LoadFromFile(Parent: TCompany; RecNum: Integer);
    constructor Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);
    constructor CreateNew(LastChanged: TDateTime; Const Name, Zeut, Address, Phone, Mail: String);

    procedure FileDelete(FileItem: TFileItem); override;
    procedure SetRegistrationDate(Value: TDateTime; Changes: TChanges); override;
    procedure ExSaveData(FileItem: TFileItem; Parent: TBasicClass); override;
    procedure SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer); override;

    property Phone:   String read FPhone;
    property Address: String read FAddress  write FAddress;
    property Mail:    String read FMail     write FMail;
    property Name:    String read FName     write FName;
    property Zeut:    String read FZeut     write FZeut;
  end;

  TDirector = Class(TListedClass)
  private
    FDate1: TDateTime;
    FDate2: TDateTime;
    FTaagid: TTaagid;
    FAddress: TDirectorAddress;
  public
    class procedure Undo(RecNum: Integer);

    constructor Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);
    constructor CreateNew(LastChanged: TDateTime; Const Name, Zeut: String; Date1, Date2: TDateTime; Address: TDirectorAddress; Taagid: TTaagid; Active: Boolean);
    constructor LoadFromFile(FileItem: TFileItem; Parent: TCompany; RecNum: Integer);
    destructor Destroy; override;

    procedure FileDelete(FileItem: TFileItem); override;
    procedure SetRegistrationDate(Value: TDateTime; Changes: TChanges); override;
    procedure ExSaveData(FileItem: TFileItem; Parent: TBasicClass); override;
    procedure SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer); override;
    procedure SaveDataToAll(SelectedDirectors: TMultipleSelectRec; NewAddress: TDirectorAddress;
              NewName, NewZeut: string; ChangeName, ChangeZeut, LastChange, FirstObject: boolean; FileInfo: TFileInfo);
    procedure SaveSpecifiedData(DirRecNum, FileNum, CompanyNum: integer;  FileInfo: TFileInfo;
              NewAddress: TDirectorAddress; NewName, NewZeut: string; ChangeName, ChangeZeut: boolean;
              ObjectType: TObjectClass; var NewFileInfo: TFileInfo);

    property Taagid: TTaagid read FTaagid;
    property Address: TDirectorAddress read FAddress;
    property Date1: TDateTime read FDate1;
    property Date2: TDateTime read FDate2;
  protected
     function ChangeManager(CompanyNum: integer; NewZeut,NewName: string; NewAddress: TDirectorAddress; FileInfo, NewFileInfo: TFileInfo): integer;
  end;

  TStockHolder = Class(TListedClass)
  private
    FAddress: TStockHolderAddress;
    FStocks: TStocks;
    FTaagid: Boolean;
  public
    class procedure Undo(RecNum: Integer);

    constructor Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);
    constructor CreateNew(LastChanged: TDateTime; Const Name, Zeut: String; Taagid: Boolean;
                Address: TStockHolderAddress; Stocks: TStocks; Active: Boolean);
    constructor LoadFromFile(FileItem: TFileItem; Parent: TCompany; RecNum: Integer);
    destructor Destroy; override;

    procedure FileDelete(FileItem: TFileItem); override;
    procedure ExSaveData(FileItem: TFileItem; Parent: TBasicClass); override;
    procedure SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer); override;
    procedure SaveDataToAll(SelectedStockHolders: TMultipleSelectRec; NewAddress: TStockHolderAddress;
              NewName, NewZeut: string; ChangeName, ChangeZeut, LastChange, FirstObject: boolean; FileInfo: TFileInfo);
    procedure SaveSpecifiedData(Phone: string; DirRecNum, FileNum, CompanyNum: integer;
              FileInfo: TFileInfo; NewAddress: TStockHolderAddress ;  NewName, NewZeut: string;
              ChangeName,ChangeZeut: boolean; ObjectType:TObjectClass; var NewFileInfo:TFileInfo);
    function ChangeManager(CompanyNum: integer; NewZeut,NewName: string; NewAddress: TStockHolderAddress; FileInfo, NewFileInfo: TFileInfo): integer;
    procedure SetRegistrationDate(Value: TDateTime; Changes: TChanges); override;

    property Stocks: TStocks read FStocks;
    property Address: TStockHolderAddress read FAddress;
    property Taagid: Boolean read FTaagid;
  end;

  TCompany = Class(TListedClass)
  private
    FDisabled: Boolean;
    FAddress: TCompanyAddress;
    FDirectors: TDirectors;
    FStockHolders: TStockHolders;
    FHonList: THonList;
    FMuhazList: TMuhazList;
    FManagers: TManagers;
    FCompanyInfo: TCompanyInfo;
    FMagish: TMagish;
    FSubmissionData: TSubmissionData;  //v 9.0 : Feb 2011: submission data
    TempName: String;
    FFastLoad: Boolean;  // See TCompanies -> SearchLoad

    function GetName: String;
    procedure GetRecNumProc(ObjectNum, NewNum: Integer);
    procedure SetDisabled(Value: Boolean);
    procedure GetDirectorsLatestZips(zipFilesList: TFilesList; showingDate: TDateTime);
    procedure GetStockHoldersLatestZips(zipFilesList: TFilesList; showingDate: TDateTime);
    procedure GetManagersLatestZips(zipFilesList: TFilesList; showingDate: TDateTime);
  public
    constructor Create(RecNum: Integer; ShowingDate: TDateTime; FastLoad: Boolean);
    constructor CreateNew(ShowingDate: TDateTime; Const Name, Zeut: String; Address: TCompanyAddress;
       Directors: TDirectors; StockHolders: TStockHolders; HonList: THonList; MuhazList: TMuhazList;
       Managers: TManagers; CompanyInfo: TCompanyInfo ;SubmissionData:TSubmissionData; Magish:TMagish);
    constructor LoadFromFile(Parent: TCompany; FileItem: TFileItem);
    constructor LoadFromFileTakanonChange(FileItem: TFileItem; CompanyRecNum: integer);
    destructor Destroy; override;

    function NameEnum(FileItem: TFileItem): Boolean;
    procedure FileDelete(FileItem: TFileItem); override;
    procedure ExSaveData(FileItem: TFileItem; Parent: TBasicClass); override;
    procedure SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer); override;
    procedure SetNumber(const Number, AName: String);
    procedure SetRegistrationDate(Value: TDateTime; Changes: TChanges); override;
    function GetCreatedDate: TDateTime;

    property Name read GetName;
    property Address: TCompanyAddress read FAddress;
    property Directors: TDirectors read FDirectors;
    property StockHolders: TStockHolders read FStockHolders;
    property HonList: THonList read FHonList;
    property MuhazList: TMuhazList read FMuhazList;
    property Managers: TManagers read FManagers;
    property CompanyInfo: TCompanyInfo read FCompanyInfo;
    property ShowingDate: TDateTime read FLastChanged write FLastChanged;
    property Magish: TMagish read FMagish write FMagish;
    property SubmissionData: TSubmissionData read FSubmissionData write FSubmissionData;
    property Disabled: Boolean read FDisabled write SetDisabled;
    property FastLoad: Boolean read FFastLoad;
    property CreatedDate: TDateTime read GetCreatedDate;
  end;

  TListObject = Class(TBasicClass)
  private
    FFileItem: TFileItem;
    FFilter: TActivityFilter;
    FFullList: TList;
    FActiveList: TList;
    FNonActiveList: TList;
    FCurrentList: TList;
    function GetCount: Integer;
    procedure LoadData; virtual; abstract;
    procedure FillData; virtual; abstract;
    procedure SetFilter(Value: TActivityFilter);
    function GetData(Index: Integer): TListedClass;
  public
    constructor CreateNew;
    constructor Create(Parent: TCompany);
    constructor LoadFromFile(Parent: TCompany; FileItem: TFileItem);
    destructor Destroy; override;
    destructor Deconstruct;

    procedure FileDelete(FileItem: TFileItem);
    procedure ExSaveData(FileItem: TFileItem; Parent: TBasicClass); virtual;
    procedure SaveData(Parent: TBasicClass; FileNum: Integer); // TBasicClass - Because it could be TCompany or TStockHolder
    procedure SetRegistrationDate(Date: TDateTime; Changes: TChanges);
    procedure Add(NewChild: TListedClass); // Adds to ActiveList ans FullList
    function FindByZeut(const Zeut: String): TListedClass;
    function FindByName(const Name: String): TListedClass;
    function FindByRecNum(const recNum: integer): TListedClass;

    property Count: Integer read GetCount;
    property Items[Index: Integer]: TListedClass read GetData;
    property Filter: TActivityFilter read FFilter write SetFilter;
  end;

  TDirectors = Class(TListObject)
  private
    procedure LoadData; override;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    procedure FillData; override;
    function EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
  end;

  THonList = Class(TListObject)
  private
    procedure LoadData; override;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    procedure FillData; override;
    function EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
  public
    function FindHonItem(const Name: String; Value: Double): THonItem;
  end;

  TMuhazList = Class(TListObject)
  private
    procedure LoadData; override;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    procedure FillData; override;
    function EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
  public
    function CountMuhaz(const Name: String; Value: Double): Double;
  end;

  TManagers = Class(TListObject)
  private
    procedure FillData; override;
    procedure LoadData; override;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    function EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
  end;

  TCompanies = Class(TListObject)
  private
    FShowingDate: TDateTime;
    FSearchLoad: Boolean;  // Load only StockHolders and Directors for fmPeopleSearch only
    FShowDisabled: Boolean;
    function GetData(Index: Integer): TCompany;
    function GetName(Index: Integer): String;
    function GetNumber(Index: Integer): String;
    function GetRecNum(Index: Integer): Integer;
    procedure FillData; override;
  public
    constructor Create(ShowingDate: TDateTime; ShowDisabled: Boolean; SearchLoad: Boolean);
    destructor Destroy; override;
    property ShowDisabled: Boolean read FShowDisabled;
    property Items[Index: Integer]: TCompany read GetData;
    property Names[Index: Integer]: String read GetName;
    property Numbers[Index: Integer]: String read GetNumber;
    property RecNums[Index: Integer]: Integer read GetRecNum;
    property SearchLoad: Boolean read FSearchLoad;
  end;

  TStockHolders = Class(TListObject)
  private
    procedure FillData; override;
    procedure LoadData; override;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    function EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
  end;

  TStocks = Class(TListObject)
  private
    FParent: TStockHolder;
    procedure LoadData; override;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    function EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
  public
    constructor LoadFromFile(Parent: TStockHolder; FileItem: TFileItem);
    constructor Create(Parent: TStockHolder);

    property Parent: TStockHolder read FParent;
    procedure FillData; override;
    function FindStock(const Name: String; Value: Double): TStock;
  end;

  TFileInfo = Class
  public
    Action       : TFileAction;
    Draft        : Boolean;
    FileName     : String;
    //--> Change Company Name functionality improvement == Added on 06/11/2001 by Elijah Shlieserman
    //PreviousName : String;
    //<--
    DecisionDate : TDateTime;
    RecNum       : Integer;
    notifyDate   : TDateTime;
    CompanyNum   : Integer;
    OldStyle     : Boolean;
    //--> additional fields added to allow Company Name style changes for other fields (namely Zip Code). == Tsahi: 20/06/2013
    AdditionalData: string; //  links to ResStr1 field in Files table
    AdditionalObjectClass: TObjectClass; ////  links to ResStr2 field in Files table
    AdditionalObjectNum : Integer;
    // Moshe 16/12/2018
    Takanon: Boolean;
    procedure Assign(Source: TFileInfo);
  end;

  TFileItem = Class
  private
    FFileInfo: TFileInfo;
    FParent: TFilesList;
  public
    constructor Create(Parent: TFilesList; FileInfo: TFileInfo);
    destructor Destroy; override;
    procedure DeleteFile;
    procedure SetDraft;
    procedure DeleteRecord(ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction; NewNum: Integer);
    function EnumChanges(ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction; EnumProc: TFileEnumProc): Integer;
    procedure SaveChange(ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction; NewNum: Integer);
    function SaveNameChange(const NewName: String; Action: TAction; ObjectType: TObjectClass): Integer;
    function LoadNameChange(var NewName: String): Boolean;
    function GetLastChange(FileNum: integer; ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction): Integer;
    function GetDataRecNum(CompanyNum: integer; FileAction: TFileAction; ObjectType: TObjectClass; DataAction: TAction): integer;
    // Moshe 16/12/2018
    function EnumTakanonChanges(ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction; EnumProc: TFileEnumProc): Integer;
    property FileInfo: TFileInfo read FFileInfo;
  end;

  TFilesList = Class
  private
    FCompany: Integer;
    FDraft: Boolean;
    FAction: TFileAction;
    FData: TList;
    function GetCount: Integer;
    function GetData(Index: Integer): TFileItem;
    function GetFileInfo(Index: Integer): TFileInfo;
  public
    constructor Create(Company: Integer; FileAction: TFileAction; Draft: Boolean);
    destructor Destroy; override;
    procedure Add(FileInfo: TFileInfo);
    property Count: Integer read GetCount;
    property FileInfo[Index: Integer]: TFileInfo read GetFileInfo;
    property Items[Index: Integer]: TFileItem read GetData;
    function FileExists(const FileName: String; var Index: Integer): Boolean;
    function GetLatestZip( ObjectType: TObjectClass; objectNum: integer; ShowingDate, currentActionDate: TDateTime; defaultValue: string): string;
    class function GetLatestActionDate(company: TCompany; fileAction: TFileAction; Draft: boolean;
                                       objectClass: TObjectClass; objectNum: integer; action:TAction): TDateTime;
  end;

  TMultiDirList = class (TList) //Tsahi: multidirectors list
    Public
      Constructor CreateAndFill(FZeut, FFileName: string; PChangeDate: TDateTime);
      function  FindRecord(CompanyNum : integer): integer;
      function  PersonCount(PersonType: TPersonType): integer;
      procedure FreeList;
    private
      Zeut: String;
      FileName: string;
      ChangeDate: TDateTime;
      procedure LoadByZeut (PersonType: TPersonType);
      procedure LoadByNewZeut ( PersonType: TPersonType);
      procedure FilterByFileName;
      function  AddNewRecord: integer;
      procedure DeleteRecord (Index: integer);
      procedure  SearchForNewZeut( PersonType: TPersonType);
  end;

  TBlankEnumProc = function (FileItem: TFileItem): Boolean of Object;

  THistory = Class
  private
    function CompareActions(Action1, Action2: TFileAction): Boolean;
    function CompareDates(FileInfo: TFileInfo; Date: TDateTime; Action: TFileAction): Boolean;
  public
    function EnumBlanks(CompanyNum: Integer; EnumProc: TBlankEnumProc): Integer;
    procedure UndoLastBlank(CompanyNum: Integer; FileType: TFileAction);
    procedure UndoFile(FileItem: TFileItem);
    function IsActionLast(CompanyNum: Integer; Action: TFileAction; Date: TDateTime): Boolean;
    function IsActionLastEx(CompanyNum: Integer; var FileItem: TFileItem): Boolean;
    function IsZipActionLast(CompanyNum: Integer; AdditionalObjectClass:TObjectClass; AdditionalObjectNum: integer; Date: TDateTime): Boolean;
  end;

  //v 9.0 : Feb 2011: submission data
  TSubmissionData = class
  private
    FRecNum: integer; //record number (for ease of debugging)
    FDate: TDateTime;
    FRepresentativeName: string;
    FRepresentativeRole: string;
    FRepresentativePhone: string;
    FRepresentativeMail: string;
    FAltHebrewName1: string;
    FAltEnglishName1: string;
    FAltHebrewName2: string;
    FAltEnglishName2: string;
    FAltHebrewName3: string;
    FAltEnglishName3: string;
    FGetCertificate: boolean;
    FEMail: string;
    FFax: string;
    FGetCopies: boolean;
    FTakanonCopies: integer;
    FCertificateCopies: integer;
    FPayment: double;
    isLoadedFromDb : boolean;
    FEzel  : string;
    FDocsByMail : boolean;

    procedure SaveExisting (Parent: TCompany);
    procedure SaveNew (Parent: TCompany);
    function FilterCompanyRecord(CompanyNumber: integer) : boolean;
    procedure FillData(RecNum: integer;
                               Date: TDateTime;
                               RepresentativeName: string;
                               RepresentativeRole: string;
                               RepresentativePhone: string;
                               RepresentativeMail: string;
                               AltHebrewName1: string;
                               AltEnglishName1: string;
                               AltHebrewName2: string;
                               AltEnglishName2: string;
                               AltHebrewName3: string;
                               AltEnglishName3: string;
                               GetCertificate: boolean;
                               EMail: string;
                               Fax: string;
                               GetCopies: boolean;
                               TakanonCopies: integer;
                               CertificateCopies: integer;
                               Payment: double;
                               Ezel:string;
                               DocsByMail:boolean);
    function GetUnregisteredCompanyNum(Company: TCompany): integer;
    procedure SaveFieldsToTable(companyNum : integer);
  public
    property RecNum: integer read                  FRecNum;
    //    property CompanyNumber: integer read           GetCompanyNumber;
    property Date: TDateTime read                  FDate;
    property RepresentativeName: string read       FRepresentativeName;
    property RepresentativeRole: string read       FRepresentativeRole;
    property RepresentativePhone: string read      FRepresentativePhone;
    property RepresentativeMail: string read       FRepresentativeMail;
    property AltHebrewName1: string read           FAltHebrewName1;
    property AltEnglishName1: string read          FAltEnglishName1;
    property AltHebrewName2: string read           FAltHebrewName2;
    property AltEnglishName2: string read          FAltEnglishName2;
    property AltHebrewName3: string read           FAltHebrewName3;
    property AltEnglishName3: string read          FAltEnglishName3;
    property GetCertificate: boolean read          FGetCertificate;
    property EMail: string read                    FEMail;
    property Fax: string read                      FFax;
    property GetCopies: boolean read               FGetCopies;
    property TakanonCopies: integer read           FTakanonCopies;
    property CertificateCopies: integer read       FCertificateCopies;
    property Payment: double read                  FPayment;
    property Ezel:string read                      fEzel;
    property DocsByMail:boolean read               fDocsByMail;

    constructor Create(Company: TCompany);
    constructor LoadFromFile(Parent: TCompany; FileItem: TFileItem);

    procedure LoadData(Parent: TCompany);
    procedure SaveData(Parent: TCompany);
    procedure ExSaveData(FileItem:TfileItem;Parent:TCompany);
    procedure LoadEnum(ObjectNum,NewNum:integer);

    procedure  ExFillData(
                        Date: TDateTime;
                        RepresentativeName: string;
                        RepresentativeRole: string;
                        RepresentativePhone: string;
                        RepresentativeMail:string;
                        AltHebrewName1: string;
                        AltEnglishName1: string;
                        AltHebrewName2: string;
                        AltEnglishName2: string;
                        AltHebrewName3: string;
                        AltEnglishName3: string;
                        GetCertificate: boolean;
                        EMail: string;
                        Fax: string;
                        GetCopies: boolean;
                        TakanonCopies: integer;
                        CertificateCopies: integer;
                        Payment: double;
                        Ezel:string;
                        DocsByMail:boolean);
   constructor CreateNew(Date: TDateTime;
                        RepresentativeName: string;
                        RepresentativeRole: string;
                        RepresentativePhone: string;
                        RepresentativeMail:string;
                        AltHebrewName1: string;
                        AltEnglishName1: string;
                        AltHebrewName2: string;
                        AltEnglishName2: string;
                        AltHebrewName3: string;
                        AltEnglishName3: string;
                        GetCertificate: boolean;
                        EMail: string;
                        Fax: string;
                        GetCopies: boolean;
                        TakanonCopies: integer;
                        CertificateCopies: integer;
                        Payment: double;
                        Ezel:string;
                        DocsByMail:boolean);

  end;

  function GetFileActionName(FileAction: TFileAction): String;
  function StrToFileAction(const Value: String): TFileAction;
  //procedure ChangesCopy(Source, Dest: TData; ObjectClass: TObjectClass; ObjectNum, NewObjectNum: Integer);
  function CopyCompany(Source, Dest: TData; RecNum: Integer; const CompanyName: String): boolean;
  { Added on 07/03/2005 by ES - new log --> }
  function ObjectToName( ObjVal : TObjectClass ) : String;
  function ActionToName( ActVal : TAction ) : String;
  function ActionToChar(Action: TAction): Char;
  function ObjToChar(Obj: TObjectClass): Char;
  function FileActionToChar(Action: TFileAction): Char;
  function CharToFileAction(C: Char): TFileAction;
  function PersonTostr(Pers: TPersonType):string;
  function CharToObj(C: Char): TObjectClass;
  function CharToAction(C: Char): TAction;
  function GetTableInstance (Table: TTable): TTable;
  { <-- }

implementation

uses
  Util, Main;

type
  PCompaniesRecord = ^TCompaniesRecord;
  TCompaniesRecord = record
    RecNum: Integer;
    Name: String;
    Number: String;
  end;

var
  TempDate: TDateTime;

// Utils
//----------------------------------------------
function StrToFileAction(const Value: String): TFileAction;
var
  i: Integer;
begin
  Result := faAll;
  for i := 0 to Ord(faAll) - 1 do
    if Value = GetFileActionName(TFileAction(i)) then
    begin
      Result:= TFileAction(i);
      Exit;
    end;
end;

function GetFileActionName(FileAction: TFileAction): String;
begin
  case FileAction of
    faNumber:          Result := '����� ����';
    faIncrease:        Result := '����� ���';
    faDecrease:        Result := '����� ���';
    faHakzaa:          Result := '����� �����';
    faHaavara:         Result := '����� �����';
    faDirector:        Result := '����������';
    faAddress:         Result := '��� ����';
    faDoh:             Result := '��"� ����';
    faStockHolderInfo: Result := '���� ��� �����';
    faDirectorInfo:    Result := '���� �������';
    faCompanyName:     Result := '�� �����';
    faHamara:          Result := '���� �����';
    faManager:         Result := '������';
    faSignName:        Result := '����� ����';
    faEngName:         Result := '�� ���� �������';
    faZipCode:         Result := '�����';
    faTakanon:         Result := '����� �����';
  else
    Result := '';
  end;
end;

function ObjToChar(Obj: TObjectClass): Char;
begin
  Result:= Char(Byte(Obj)+ Byte('0'));
end;

function CharToObj(C: Char): TObjectClass;
begin
  Result:= TObjectClass(Byte(C) - Byte('0'));
end;

function ActionToChar(Action: TAction): Char;
begin
  Result:= Char(Byte(Action)+ Byte('0'));
end;

function CharToAction(C: Char): TAction;
begin
  Result:= TAction(Byte(C)-Byte('0'));
end;

function FileActionToChar(Action: TFileAction): Char;
begin
  Result:= Char(Byte(Action)+ Byte('0'));
end;

function CharToFileAction(C: Char): TFileAction;
begin
  Result:= TFileAction(Byte(C)-Byte('0'));
end;

// TBasicAddress
//----------------------------------------------

constructor TBasicAddress.CreateNew(Const Street, Number, Index, City, Country: String);
begin
  Inherited Create;
  FStreet  := Street;
  FNumber  := Number;
  FIndex   := Index;
  FCity    := City;
  FCountry := Country;
end;

// TCompanyAddress
//----------------------------------------------
constructor TCompanyAddress.LoadFromFile(ParentNum: Integer; FileItem: TFileItem);
begin
  if (FileItem.EnumChanges(ocCompany, ParentNum, acAddress, LoadEnum) <= 0) then
//  if (FileItem.EnumChanges(ocCompany, ParentNum, acNewMagish, LoadEnum) <= 0) then //sts23822
    raise Exception.Create('Record count Error at TCompanyAddress.LoadFromFile');
end;

procedure TCompanyAddress.LoadEnum(ObjectNum, NewNum: Integer);
begin
  Data.taAddress.EditKey;
//  Data.taAddress.FieldByName('RecNum').AsInteger := ObjectNum; //sts23822
  Data.taAddress.FieldByName('RecNum').AsInteger := NewNum;
  If not Data.taAddress.GotoKey Then
    raise Exception.Create('Record not found at TCompanyAddress.LoadEnum');

  FStreet := Data.taAddress.FieldByName('Street').AsString;
  FNumber := Data.taAddress.FieldByName('Number').AsString;
  FIndex  := Data.taAddress.FieldByName('Index').AsString;
  FCity   := Data.taAddress.FieldByName('City').AsString;
  FCountry:= Data.taAddress.FieldByName('Country').AsString;
  FPOB    := Data.taAddress.FieldByName('POB').AsString;
  FEzel   := Data.taAddress.FieldByName('Ezel').AsString;
  FPhone  := Data.taAddress.FieldByName('Phone').AsString;
  FFax    := Data.taAddress.FieldByName('Fax').AsString;
  //FRecNum := ObjectNum; //sts23822
  FRecNum := NewNum;
end;

constructor TCompanyAddress.CreateNew(Const Street, Number, Index, City, Country, POB, Ezel, Phone, Fax: String);
begin
  Inherited CreateNew(Street, Number, Index, City, Country);
  FPOB     := POB;
  FEzel    := Ezel;
  FPhone   := Phone;
  FFax     := Fax;
end;

procedure TCompanyAddress.ExSaveData(FileItem: TFileItem; Parent: TCompany; ChangeDate: TDateTime);
begin
  SaveData(Parent, ChangeDate, FileItem.FileInfo.Draft, FileItem.FileInfo.RecNum);
  FileItem.SaveChange(ocCompany, Parent.RecNum, acAddress, RecNum);
end;

procedure TCompanyAddress.DeleteEnum(ObjectNum, NewNum: Integer);
begin
  if Data.DeleteByRecNum(Data.taAddress,NewNum)then
    FFileItem.DeleteRecord(ocCompany, ObjectNum, acAddress, NewNum)
  else
    Exception.Create('SQL error at TCompanyAddress.DeleteEnum');
end;

procedure TCompanyAddress.FileDelete(Parent: TCompany; FileItem: TFileItem);
var
  Changes: TChanges;
begin
  FFileItem:= FileItem;
  If (FileItem <> nil) and FileItem.FFileInfo.Draft then
  begin
    FileItem.EnumChanges(ocCompany, Parent.RecNum, acAddress, DeleteEnum);
  end
  Else
  begin
    Changes:= TChanges.Create(Parent);
    try
      With DATA.taAddress Do
      begin
        EditKey;
        FRecNum := Changes.GetLastChange(ocCompany, Parent.RecNum, acAddress);
        FieldByName('RecNum').AsInteger := RecNum;
        Changes.FileDelete(ocCompany, Parent.RecNum, acAddress, RecNum);
        If FileItem <> nil then
          FileItem.DeleteRecord(ocCompany, Parent.RecNum, acAddress, RecNum);
      end;
    finally
      Changes.Free;
    end;
  end;
end;

procedure TCompanyAddress.SaveData(Parent: TCompany; ChangeDate: TDateTime; Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving company address (' + Parent.Name + ')');
  { <-- }
  With DATA.taAddress Do
  begin
    Append;
    FieldByName('Street').AsString   := Street;
    FieldByName('Number').AsString   := Number;
    FieldByName('Index').AsString    := Index;
    FieldByName('City').AsString     := City;
    FieldByName('Country').AsString  := Country;
    FieldByName('POB').AsString      := POB;
    FieldByName('Ezel').AsString     := Ezel;
    FieldByName('Phone').AsString    := Phone;
    FieldByName('Fax').AsString      := Fax;
    Post;
    FRecNum:= FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr( FRecNum ));
    { <-- }
    If not Draft Then
    begin
      Changes:= TChanges.Create(Parent);
      try
        Changes.SaveChanges(ocCompany, Parent.RecNum, acAddress, RecNum, ChangeDate, FileNum);
      finally
        Changes.Free;
      end;
    end;
  end;
end;

// TBasicClass
//----------------------------------------------
constructor TBasicClass.CreateNew(LastChanged: TDateTime);
begin
  inherited Create;
  FParent:= Nil;
  FLastChanged:= LastChanged;
end;

constructor TBasicClass.Create(Parent: TCompany);
begin
  inherited Create;
  FParent:= Parent;
end;

// TChanges
//----------------------------------------------
class function TChanges.GetParent(ObjectType: TObjectClass; Action: TAction; RecNum: Integer): Integer;
var
  Table: TTable;
begin
  Table:= TTable.Create(Data);
  Table.DatabaseName:= Data.taChanges.DatabaseName;
  Table.TableName:= Data.taChanges.TableName;
  Table.IndexName:= Data.taChanges.IndexName;
  Table.Open;
  try
    Table.Filter:= '([ObjectType] = ''' + ObjToChar(ObjectType) + ''') AND ([Action] = '''
                   + ActionToChar(Action) + ''') AND ([NewNum] = ''' + IntToStr(RecNum) + ''')';
    Table.Filtered := True;
    Result := Table.FieldByName('ObjectNum').AsInteger;
  finally
    Table.Free;
  end;
end;

//----------------------------------
class procedure TChanges.UndoFile(FileItem: TFileItem);
var
  Table: TTable;
  FileNum: String;
  ManagerNum: integer;
begin
  Table := TTable.Create(Data);
  Table.DatabaseName:= Data.taChanges.DatabaseName;
  Table.TableName:= Data.taChanges.TableName;
  Table.IndexName:= Data.taChanges.IndexName;
  Table.Open;
  if (FileItem.FileInfo.OldStyle) then
    FileNum := '-1'
  else
    FileNum := IntToStr(FileItem.FileInfo.RecNum);

  try
    if (FileItem.FileInfo.Action in [faHakzaa, faDecrease, faIncrease]) then
    begin
      Table.Filter := '([ObjectType] = ''' + ObjToChar(ocCompany)+''') AND ' +
        '([ObjectNum] = ''' + IntToStr(FileItem.FileInfo.CompanyNum) + ''') AND ' +
        '([Action] = ''' + ActionToChar(acNewHon) + ''') AND ' +
        '([Date] = ''' + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''') AND ' +
        '([ResInt1] = ''' + FileNum + ''')';
      Table.Filtered := True;
      while (not Table.EOF) do
      begin
        THonItem.Undo(Table.FieldByName('NewNum').AsInteger);
        Table.Delete;
      end;

      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocHon)+''') AND ([Action] = '''+ActionToChar(acDelete)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while (not Table.EOF) do
      begin
        if (GetParent(ocCompany, acNewHon, Table.FieldByName('NewNum').AsInteger) = FileItem.FileInfo.CompanyNum) then
          Table.Delete
        else
          Table.Next;
      end;
    end;

    if (FileItem.FileInfo.Action = faDirector) then
    begin
      Table.Filter := '([ObjectType] = ''' + ObjToChar(ocCompany) + ''') AND ' +
        '([ObjectNum] = ''' + IntToStr(FileItem.FileInfo.CompanyNum) + ''') AND ' +
        '([Action] = ''' + ActionToChar(acNewDirector) + ''') AND ' +
        '([Date] = ''' + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''') AND ' +
        '([ResInt1] = ''' + FileNum + ''')';
      Table.Filtered := True;
      while (not Table.EOF) do
      begin
        TDirector.Undo(Table.FieldByName('NewNum').AsInteger);
        Table.Delete;
      end;

      Table.Filter := '([ObjectType] = ''' + ObjToChar(ocDirector) + ''') AND ' +
        '([Action] = ''' + ActionToChar(acDelete) + ''') AND ' +
        '([ResInt1] = ''' + FileNum + ''')';
      Table.Filtered := True;
      while (not Table.EOF) do
      begin
        if GetParent(ocCompany, acNewDirector, Table.FieldByName('NewNum').AsInteger) = FileItem.FileInfo.CompanyNum then
          Table.Delete
        else
          Table.Next;
      end;
    end;

    if (FileItem.FileInfo.Action = faManager) then
    begin
      Table.Filter := '([ObjectType] = ''' + ObjToChar(ocCompany) + ''') AND ' +
        '([ObjectNum] = ''' + IntToStr(FileItem.FileInfo.CompanyNum) + ''') AND ' +
        '([Action] = ''' + ActionToChar(acNewManager) + ''') AND ' +
        '([Date] = ''' + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''') AND ' +
        '([ResInt1] = ''' + FileNum + ''')';
      Table.Filtered := True;
      Table.First;
      while (not Table.EOF) do
      begin
        ManagerNum := Table.FieldByName('NewNum').AsInteger;
        TManager.Undo(ManagerNum);
        Table.Delete;
        FileItem.DeleteRecord(ocCompany,FileItem.FileInfo.CompanyNum,acNewManager,ManagerNum);
      end;
      // 27.06.05 Tsahi Undo Manager change in multiple change
      // 09/11/05 Tsahi Changed to undo delted managers as well...
      Table.Filter := '([ObjectType] = ''' + ObjToChar(ocManager) + ''') AND ' +
        '(([Action] = ''' + ActionToChar(acAddress) + ''') OR ' +
          '([Action] = ''' + ActionToChar(acDelete) + ''')) AND ' +
        '([ResInt1] = ''' + FileNum + ''')';
      Table.Filtered := True;
      Table.First;
      while (not Table.EOF) do
      begin
        TManager.UndoManagerChange(Table.FieldByName('ObjectNum').AsInteger);
        Table.Delete;
      end;

      Table.Filter:= '([ObjectType] = ''' + ObjToChar(ocManager) + ''') AND ' +
        '([Action] = ''' + ActionToChar(acDelete) + ''') AND ' +
        '([ResInt1] = ''' + FileNum + ''')';
      Table.Filtered:= True;
      while not Table.EOF do
      begin
        if GetParent(ocCompany, acNewManager, Table.FieldByName('NewNum').AsInteger) = FileItem.FileInfo.CompanyNum Then
          Table.Delete
        else
          Table.Next;
      end;
    end;

    // 28.06.05 Tsahi Undo SignName change in multiple change
    if (FileItem.FileInfo.Action = faSignName) or (FileItem.FileInfo.Action = faEngName) then
    begin
      Table.Filter:= '([ObjectType] = ''' + ObjToChar(ocCompany) + ''') AND ' +
                     '([Action] = ''' + ActionToChar(acSignName) + ''') AND ' +
                     '([Date] = ''' + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''') AND ' +
                     '([ResInt1] = ''' + FileNum + ''')';
      Table.Filtered:= True;
      while (not Table.EOF) do
      begin
        TCompanyInfo.UndoSignName(Table.FieldByName('NewNum').AsInteger);
        Table.Delete;
      end;
    end;

    if FileItem.FileInfo.Action = faHamara then
    begin
      // Delete new Muhaz
      Table.Filter:= '([ObjectType] = ''' + ObjToChar(ocCompany) + ''') And ([ObjectNum] = ''' +
                     IntToStr(FileItem.FileInfo.CompanyNum) + ''') AND ([Action] = ''' + ActionToChar(acNewMuhaz) + ''') AND ([Date] = ''' +
                     DateToLongStr(FileItem.FileInfo.DecisionDate) + ''') And ([ResInt1] = ''' + FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
      begin
        TMuhazItem.Undo(Table.FieldByName('NewNum').AsInteger);
        Table.Delete;
      end;

      // Restore deleted Muhaz Items
      Table.Filter:= '([ObjectType] = ''' + ObjToChar(ocMuhaz) + ''') AND ([Action] = ''' + ActionToChar(acDelete) +
                     ''') AND ([Date] = ''' + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''') And ([ResInt1] = ''' +
                     FileNum + ''')';
      Table.Filtered := True;
      while (not Table.EOF) do
      begin
        if GetParent(ocCompany, acNewMuhaz, Table.FieldByName('NewNum').AsInteger) = FileItem.FileInfo.CompanyNum Then
          Table.Delete
        else
          Table.Next;
      end;

      // Delete new Hon Items
      Table.Filter := '([ObjectType] = ''' + ObjToChar(ocCompany) + ''') And ([ObjectNum] = ''' +
                      IntToStr(FileItem.FileInfo.CompanyNum) + ''') AND ([Action] = ''' + ActionToChar(acNewHon) +
                      ''') AND ([Date] = ''' + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''') And ([ResInt1] = ''' +
                      FileNum + ''')';
      Table.Filtered := True;
      while not Table.EOF do
      begin
        THonItem.Undo(Table.FieldByName('NewNum').AsInteger);
        Table.Delete;
      end;

      // Restore Deleted Hon Items
      Table.Filter:= '([ObjectType] = ''' + ObjToChar(ocHon) + ''') AND ([Action] = ''' + ActionToChar(acDelete) +
                     ''') AND ([Date] = ''' + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''') And ([ResInt1] = ''' +
                     FileNum + ''')';
      Table.Filtered:= True;
      while (not Table.EOF) do
      begin
        if GetParent(ocCompany, acNewHon, Table.FieldByName('NewNum').AsInteger) = FileItem.FileInfo.CompanyNum Then
          Table.Delete
        else
          Table.Next;
      end;

      // Delete Stocks Changes
      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocStockHolder)+''') AND ([Action] = '''+ActionToChar(acNewStock)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
      begin
        if GetParent(ocCompany, acNewStockHolder, Table.FieldByName('ObjectNum').AsInteger)= FileItem.FileInfo.CompanyNum then
        begin
          TStock.Undo(Table.FieldByName('NewNum').AsInteger);
          Table.Delete;
        end
        else
          Table.Next;
      end;

      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocStock)+''') AND ([Action] = '''+ActionToChar(acDelete)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
        If GetParent(ocCompany, acNewStockHolder, GetParent(ocStockHolder, acNewStock, Table.FieldByName('NewNum').AsInteger))= FileItem.FileInfo.CompanyNum then
          begin
            Table.Delete;
          end
        Else
          Table.Next;

      // Delete StockHolders Changes
      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocCompany)+''') And ([ObjectNum] = '''+IntToStr(FileItem.FileInfo.CompanyNum)+''') AND ([Action] = '''+ActionToChar(acNewStockHolder)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
      begin
        TStockHolder.Undo(Table.FieldByName('NewNum').AsInteger);
        Table.Delete;
      end;

      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocStockHolder)+''') AND ([Action] = '''+ActionToChar(acDelete)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
        If GetParent(ocCompany, acNewStockHolder, Table.FieldByName('NewNum').AsInteger) = FileItem.FileInfo.CompanyNum Then
          Table.Delete
        Else
          Table.Next;
    end;

    if FileItem.FileInfo.Action in [faHakzaa, faHaavara] then
    begin
      // Delete Stocks Changes
      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocStockHolder)+''') AND ([Action] = '''+ActionToChar(acNewStock)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
        If GetParent(ocCompany, acNewStockHolder, Table.FieldByName('ObjectNum').AsInteger)= FileItem.FileInfo.CompanyNum then
          begin
            TStock.Undo(Table.FieldByName('NewNum').AsInteger);
            Table.Delete;
          end
        Else
          Table.Next;

      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocStock)+''') AND ([Action] = '''+ActionToChar(acDelete)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
        If GetParent(ocCompany, acNewStockHolder, GetParent(ocStockHolder, acNewStock, Table.FieldByName('NewNum').AsInteger))= FileItem.FileInfo.CompanyNum then
          begin
            Table.Delete;
          end
        Else
          Table.Next;

      // Delete StockHolders Changes
      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocCompany)+''') And ([ObjectNum] = '''+IntToStr(FileItem.FileInfo.CompanyNum)+''') AND ([Action] = '''+ActionToChar(acNewStockHolder)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
        begin
          TStockHolder.Undo(Table.FieldByName('NewNum').AsInteger);
          Table.Delete;
        end;

      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocStockHolder)+''') AND ([Action] = '''+ActionToChar(acDelete)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
        If GetParent(ocCompany, acNewStockHolder, Table.FieldByName('NewNum').AsInteger) = FileItem.FileInfo.CompanyNum Then
          Table.Delete
        Else
          Table.Next;
    end;

    if FileItem.FileInfo.Action = faAddress then
    begin
     Table.Filter:= '([ObjectType] = '''+ObjToChar(ocCompany)+''') And ([ObjectNum] = '''+IntToStr(FileItem.FileInfo.CompanyNum)+''') AND ([Action] = '''+ActionToChar(acAddress)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
     Table.Filtered:= True;
      while not Table.EOF do
        Table.Delete;
    end;

    If FileItem.FileInfo.Action = faStockHolderInfo then
      begin
        Table.Filter:= '([ObjectType] = '''+ObjToChar(ocStockHolder)+''') AND ([Action] = '''+ActionToChar(acNewName)+''') AND ([DATE] ='''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') and ([ResInt1] = '''+FileNum+''')';
        Table.Filtered:= True;
        while not Table.EOF do
          if GetParent(ocCompany, acNewStockHolder, Table.FieldByName('ObjectNum').AsInteger)= FileItem.FileInfo.CompanyNum Then
            Table.Delete
          Else
            Table.Next;

        Table.Filter:= '([ObjectType] = '''+ObjToChar(ocStockHolder)+''') AND ([Action] = '''+ActionToChar(acNewZeut)+''') AND ([DATE] ='''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') and ([ResInt1] = '''+FileNum+''')';
        Table.Filtered:= True;
        while not Table.EOF do
          if GetParent(ocCompany, acNewStockHolder, Table.FieldByName('ObjectNum').AsInteger)= FileItem.FileInfo.CompanyNum Then
            Table.Delete
          Else
            Table.Next;

        Table.Filter:= '([ObjectType] = '''+ObjToChar(ocStockHolder)+''') AND ([Action] = '''+ActionToChar(acAddress)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
        Table.Filtered:= True;
        while not Table.EOF do
          If GetParent(ocCompany, acNewStockHolder, Table.FieldByName('ObjectNum').AsInteger)= FileItem.FileInfo.CompanyNum then
            begin
              TStockHolderAddress.Undo(Table.FieldByName('NewNum').AsInteger);
              Table.Delete;
            end
          Else
            Table.Next;
      end;

    if (FileItem.FileInfo.Action = faDirectorInfo) then
    begin
      Table.Filter := '([ObjectType] = '''+ObjToChar(ocDirector)+''') AND ([Action] = '''+ActionToChar(acNewName)+''') AND ([DATE] ='''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') and ([ResInt1] = '''+FileNum+''')';
      Table.Filtered := True;
      while (not Table.EOF) do
        if GetParent(ocCompany, acNewDirector, Table.FieldByName('ObjectNum').AsInteger)= FileItem.FileInfo.CompanyNum Then
          Table.Delete
        Else
          Table.Next;

      Table.Filter:= '([ObjectType] = '''+ObjToChar(ocDirector)+''') AND ([Action] = '''+ActionToChar(acNewZeut)+''') AND ([DATE] ='''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') and ([ResInt1] = '''+FileNum+''')';
      Table.Filtered:= True;
      while not Table.EOF do
        if GetParent(ocCompany, acNewDirector, Table.FieldByName('ObjectNum').AsInteger)= FileItem.FileInfo.CompanyNum Then
          Table.Delete
        Else
          Table.Next;

      Table.Filter := '([ObjectType] = '''+ObjToChar(ocDirector)+''') AND ([Action] = '''+ActionToChar(acAddress)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered := True;
      while (not Table.EOF) do
      begin
        if GetParent(ocCompany, acNewDirector, Table.FieldByName('ObjectNum').AsInteger)= FileItem.FileInfo.CompanyNum then
        begin
          TDirectorAddress.Undo(Table.FieldByName('NewNum').AsInteger);
          Table.Delete;
        end
        else
          Table.Next;
      end;

      Table.Filter := '([ObjectType] = '''+ObjToChar(ocDirector)+''') AND ([Action] = '''+ActionToChar(acTaagidAddress)+''') AND ([Date] = '''+ DateToLongStr(FileItem.FileInfo.DecisionDate)+''') And ([ResInt1] = '''+FileNum+''')';
      Table.Filtered := True;
      while (not Table.EOF) do
      begin
        if GetParent(ocCompany, acNewDirector, Table.FieldByName('ObjectNum').AsInteger)= FileItem.FileInfo.CompanyNum then
        begin
          TTaagid.Undo(Table.FieldByName('NewNum').AsInteger);
          Table.Delete;
        end
        else
          Table.Next;
      end;
    end;

    // Moshe 07/10/2018
    if (FileItem.FileInfo.Action = faTakanon) then
    begin
      Table.Filter := '([ResInt1] = ''' + FileNum + ''')';
      Table.Filtered := True;
      while (not Table.EOF) do
      begin
        if (TAction(Table.FieldByName('Action').AsInteger) = acNewHon) then
        begin
          THonItem.Undo(Table.FieldByName('NewNum').AsInteger);
        end;
        if (TAction(Table.FieldByName('Action').AsInteger) = acNewMagish) then
        begin
          TMagish.Undo(Table.FieldByName('NewNum').AsInteger);
        end;
        if (TAction(Table.FieldByName('Action').AsInteger) = acNewStock) then
        begin
         TStock.Undo(Table.FieldByName('NewNum').AsInteger);
        end;
        Table.Delete;
      end;
    end;
  finally
    Table.Free;
  end;
  FileItem.SetDraft;
  FileItem.Free;
end; // TChanges.UndoFile

constructor TChanges.Create(Parent: TCompany);
begin
  inherited Create(Parent);
  Table:= TTable.Create(Data);
  Table.DatabaseName:= Data.taChanges.DatabaseName;
  Table.TableName:= Data.taChanges.TableName;
  Table.IndexName:= Data.taChanges.IndexName;
  Table.Open;
end;

destructor TChanges.Destroy;
begin
  Table.Free;
  inherited;
end;

function TChanges.Enumeration(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
type
  PIntger= ^Integer;
begin
  PInteger(Param)^:= Line;
  Result:= False;
  TempDate:= Date;
end;

function TChanges.GetLastChange(ObjectType: TObjectClass; Number: Integer; Action: TAction): Integer;
begin
  Result:= -1;
  EnumChanges(ObjectType, Number, Action, Enumeration, @Result);
end;

function TChanges.GetLatestChangeFileNumber(ObjectType: TObjectClass; Number: Integer; Action: TAction): Integer;
var
   latestDate :TDateTime;
begin
  Result:= -1;
  latestDate := EncodeDate(1900,01,01);

  If Table = nil Then
  begin
    Table := TTable.Create(Data);
    Table.DatabaseName:= Data.taChanges.DatabaseName;
    Table.TableName:= Data.taChanges.TableName;
    Table.IndexName:= Data.taChanges.IndexName;
  end;

  Table.Filtered:= False;
  Table.CancelRange;

  Table.SetRangeStart;
  Table.FieldByName('ObjectType').AsString:= ObjToChar(ObjectType);
  Table.FieldByName('ObjectNum').AsInteger:= Number;
  Table.FieldByName('Action').AsString:= ActionToChar(Action);

  Table.SetRangeEnd;
  Table.FieldByName('ObjectType').AsString:= ObjToChar(ObjectType);
  Table.FieldByName('ObjectNum').AsInteger:= Number;
  Table.FieldByName('Action').AsString:= ActionToChar(Action);
  If Parent <> nil Then
    Table.FieldByName('Date').AsDateTime:= Parent.ShowingDate
  else
    Table.FieldByName('Date').AsDateTime:= EncodeDate(3000, 01, 01);

  Table.ApplyRange;
  Table.Last;
  while (not Table.BOF) do
  begin
    if (latestDate < Table.FieldByName('Date').AsDateTime) then
    begin
      latestDate := Table.FieldByName('Date').AsDateTime;
      Result := Table.FieldByName('Resint1').AsInteger;
    end;
    Table.Prior;
  end;
  Table.CancelRange;
end;

procedure TChanges.FileDelete(ObjectType: TObjectClass; Number: Integer; Action: TAction; RecNum: Integer);
begin
  If Table= nil Then
    Table:= DATA.taChanges;
  With Table Do
  begin
    Filter:= '([ObjectType] = ''' + ObjToChar(ObjectType) + ''') AND ([ObjectNum] = '''
             + IntToStr(Number) + ''') AND ([Action] = ''' + ActionToChar(Action)
             + ''') AND ([NewNum] = ''' + IntToStr(RecNum) + ''')';
    Filtered:= True;
    Last;
    If not BOF Then
      Delete
  end;
end;

procedure TChanges.ChangeRegistrationDate(ObjectType: TObjectClass; Number: Integer; Action: TAction; NewLine: Integer; Date: TDateTime);
begin
  If Table = nil Then
    Table := DATA.taChanges;
  with Table do
  begin
    Filter := '([ObjectType] = ''' + ObjToChar(ObjectType) + ''') and ([ObjectNum] = '''
              + IntToStr(Number) + ''') and ([Action] = ''' + ActionToChar(Action) + ''')';
    If NewLine <> -1 Then
      Filter := Filter + ' and ([NewNum] = ''' + IntToStr(NewLine) + ''')';
    Filtered := True;
    while not EOF do
    begin
      Edit;
      FieldByName('Date').AsDateTime:= Date;
      Post;
      Next;
    end;
    Filtered:= False;
  end;
end;

procedure TChanges.EnumFileItem(FileItem: TFileItem; EnumProc: TEnumFileItemProc);
begin
  If Table = nil Then
  begin
    Table := TTable.Create(Data);
    Table.DatabaseName := Data.taChanges.DatabaseName;
    Table.TableName := Data.taChanges.TableName;
    Table.IndexName := Data.taChanges.IndexName;
  end;

  With Table Do
  begin
    Filtered := False;
    CancelRange;
    Filter := '(Date = ''' + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''') and (ResInt1 = '
              + IntToStr(FileItem.FileInfo.RecNum) + ')';
    Filtered := True;
    if RecordCount = 0 then
      Filter := '(Date = ''' + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''')';
    First;
    while (not EOF) do
    begin
      EnumProc(CharToObj(FieldByName('ObjectType').AsString[1]), CharToAction(FieldByName('Action').AsString[1]), FieldByName('ObjectNum').AsInteger, FieldByName('NewNum').AsInteger, FileItem);
      Next;
    end;
    Filtered:= False;
    CancelRange;
  end;
end;

procedure TChanges.EnumChanges(ObjectType: TObjectClass; Number: Integer; Action: TAction; EnumProc: TEnumChangesProc; EnumParam: Pointer);
var
  TableCreated: boolean;
  ok: boolean;
begin
  TableCreated := False;
  If Table = nil Then
  begin
    TableCreated := True;
    Table := TTable.Create(Data);
    Table.DatabaseName := Data.taChanges.DatabaseName;
    Table.TableName := Data.taChanges.TableName;
    Table.IndexName := Data.taChanges.IndexName;
  end;

  Table.Filtered := False;
  Table.CancelRange;
  Table.SetRangeStart;
  Table.FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
  Table.FieldByName('ObjectNum').AsInteger := Number;
  Table.FieldByName('Action').AsString := ActionToChar(Action);
  Table.FieldByName('Date').AsDateTime := EncodeDate(1899,12,31);
  Table.SetRangeEnd;
  Table.FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
  Table.FieldByName('ObjectNum').AsInteger := Number;
  Table.FieldByName('Action').AsString := ActionToChar(Action);
  if Parent <> nil then
    Table.FieldByName('Date').AsDateTime := Parent.ShowingDate
  else
    Table.FieldByName('Date').AsDateTime := EncodeDate(3000, 01, 01);

  Table.ApplyRange;
  Table.Last;
  ok := True;
  while (not Table.Bof) and (ok) do
  begin
   ok := EnumProc(Table.FieldByName('NewNum').AsInteger, Table.FieldByName('Date').AsDateTime, EnumParam);
   if ok then
     Table.Prior;
  end;
  Table.CancelRange;
  if TableCreated then
    Table.Free;
end; // TChanges.EnumChanges

function TChanges.GetStockInfo(OriginalStocks: TStocks; FileItem: TFileItem; Stock: TStock): TStock;
   function CheckStock(NewNum: Integer; var Res: TStock): Boolean;
   var
     I: Integer;
   begin
     Result := False;
     For I := 0 to OriginalStocks.Count - 1 do
       If (OriginalStocks.Items[I].RecNum = NewNum)
          and (Stock.Value = (OriginalStocks.Items[I] as TStock).Value)
          and (Stock.Name = OriginalStocks.Items[I].Name) Then
       begin
         Res := OriginalStocks.Items[I] as TStock;
         Result := True;
         Break;
       end;
   end;

begin
  Result := Nil;
  If Table = nil Then
    Table := DATA.taChanges;
  OriginalStocks.Filter := afAll;
  Table.Filtered := False;
  try
    Table.Filter := '([ObjectType] = ''' + ObjToChar(ocStockHolder) + ''') and ([Action] ='''
                    + ActionToChar(acNewStock) + ''') and ([Date] = '''
                    + DateToLongStr(FileItem.FileInfo.DecisionDate) + ''')';
    If not FileItem.FileInfo.OldStyle Then
      Table.Filter := Table.Filter + ' and ([ResInt1] = ''' + IntToStr(FileItem.FileInfo.RecNum) + ''')';
    Table.Filtered := True;
    while not (Table.EOF or CheckStock(Table.FieldByName('NewNum').AsInteger, Result))  do
      Table.Next;
  finally
    Table.Filtered := False;
  end;
end;

procedure TChanges.SaveChanges(ObjectType: TObjectClass; Number: Integer; Action: TAction; NewLine: Integer; ChangeDate: TDateTime; FileNum: Integer);
begin
  { Added on 07/03/2005 by ES - new log --> }
  {changed on 07/10/2009 by tsahi: more fields}
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Save changes: objType: ' + ObjectToName( ObjectType) +
      ', RecNum: ' + IntToStr( Number ) +
      ', Action: ' + ActionToName( Action ) +
      ', ChangeDate: ' + DateToStr( ChangeDate ) +
      ', NewNum: ' + IntToStr( NewLine ) +
      ', FileNum:' + IntToStr( FileNum ));

  with DATA.taChanges do
  begin
    Append;
    FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
    FieldByName('ObjectNum').AsInteger := Number;
    FieldByName('Action').AsString := ActionToChar(Action);
    FieldByName('Date').AsDateTime := ChangeDate;
    FieldByName('NewNum').AsInteger := NewLine;
    FieldByName('ResInt1').AsInteger := FileNum;
    Post;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog(#9#9#9#9'RecNum after save: ' + FieldByName('RecNum').AsString);
  end;
end;

// TListedClass
//----------------------------------------------
function TListedClass.GetNameProc(Line: Integer; ChangeDate: TDateTime; Param: Pointer): Boolean;
var
  Table: TTable;
begin
  Result := True;
  if ChangeDate <= Parent.ShowingDate Then
  begin
    Table := TTable.Create(Data);
    try
      Table.DatabaseName:= Data.taForms.DatabaseName;
      Table.TableName := Data.taForms.TableName;
      Table.IndexName := '';
      Table.Open;
      Table.SetKey;
      Table.FieldByName('RecNum').AsInteger:= Line;
      If not Table.GotoKey Then
        raise Exception.Create('Record not found at TListedClass.GetNameProc #1');
      FName := Table.FieldByName('ResStr1').AsString + Table.FieldByName('ResStr2').AsString;
      Result := False;
    finally
      Table.Free;
    end;
  end;
end;

//----------------------------------------------
function TListedClass.GetZeutProc(Line: Integer; ChangeDate: TDateTime; Param: Pointer): Boolean;
var
  Table: TTable;
begin
  Result:= True;
  if ChangeDate <= Parent.ShowingDate Then
  begin
    Table:= TTable.Create(Data);
    try
      Table.DatabaseName:= Data.taForms.DatabaseName;
      Table.TableName:= Data.taForms.TableName;
      Table.IndexName:= '';
      Table.Open;
      Table.SetKey;
      Table.FieldByName('RecNum').AsInteger:= Line;
      If not Table.GotoKey Then
        raise Exception.Create('Record not found at TListedClass.GetZeutProc #1');
      FZeut := Table.FieldByName('ResStr1').AsString ;
      Result:= False;
    finally
      Table.Free;
    end;
  end;
end;

//--------------------------------------------------------------
procedure TListedClass.ChangeSigName(CompanyNum: integer; NewZeut, NewName: string; FileInfo: TFileInfo);
var
  FileNum, NewRec, CurrRec: integer;
  CompanyInfo: TCompanyInfo;
  Company: TCompany;
  FilesList:TFilesList;
begin
  FilesList := TFilesList.Create(CompanyNum,faSignName,False);
  FileInfo.CompanyNum :=CompanyNum;
  FileInfo.Action := faSignName;
  FilesList.Add(FileInfo);
  FileNum:= FileInfo.RecNum;
  With Data.taChanges do
  begin
    Filter := '([ObjectNum] = ' + IntToStr(CompanyNum) + ' ) AND ([ObjectType] = ''' + ObjToChar(ocCompany) +
             ''') AND ([Action]= '''+ActionToChar(acSignName)+''')';
    Filtered := True;
    if RecordCount > 0 then
    begin
      Last;
      CurrRec := FieldByName('NewNum').AsInteger;
    end
    else
      exit;
  end;

  Data.taSigName.EditKey;
  Data.taSigName.FieldByName('RecNum').AsInteger := CurrRec;
  if not Data.taSigName.GotoKey then
    raise Exception.Create('Record not found at TListedClass.ChangeSigName')
  else if Data.taSigName.FieldByName('SigZeut').AsString = Zeut then
  begin
    Company := TCompany.Create(CompanyNum, Now, False);
    CompanyInfo := TCompanyInfo.CreateNew(Now);
    CompanyInfo.Option1                   := Company.CompanyInfo.Option1;
    CompanyInfo.Option3                   := Company.CompanyInfo.Option3;
    CompanyInfo.OtherInfo                 := Company.CompanyInfo.OtherInfo;
    CompanyInfo.SigName                   := NewName;
    CompanyInfo.SigZeut                   := NewZeut;
    CompanyInfo.SigTafkid                 := Company.CompanyInfo.SigTafkid;
    CompanyInfo.LawName                   := Company.CompanyInfo.LawName;
    CompanyInfo.LawAddress                := Company.CompanyInfo.LawAddress;
    CompanyInfo.LawZeut                   := Company.CompanyInfo.LawZeut;
    CompanyInfo.LawLicence                := Company.CompanyInfo.LawLicence;
    CompanyInfo.Takanon2_1                := Company.CompanyInfo.Takanon2_1;
    CompanyInfo.Takanon2_2                := Company.CompanyInfo.Takanon2_2;
    CompanyInfo.Takanon2_3                := Company.CompanyInfo.Takanon2_3;
    CompanyInfo.Takanon2_4                := Company.CompanyInfo.Takanon2_4;
    CompanyInfo.Takanon4                  := Company.CompanyInfo.Takanon4;
    CompanyInfo.Takanon5                  := Company.CompanyInfo.Takanon5;
    CompanyInfo.Takanon7                  := Company.CompanyInfo.Takanon7;
    CompanyInfo.EnglishName               := Company.CompanyInfo.EnglishName;
    CompanyInfo.RegistrationDate          := Company.CompanyInfo.RegistrationDate;
    CompanyInfo.CompanyName2              := Company.CompanyInfo.CompanyName2;
    CompanyInfo.CompanyName3              := Company.CompanyInfo.CompanyName3;
    CompanyInfo.Mail                      := Company.CompanyInfo.Mail;
    CompanyInfo.SharesRestrictIndex       := Company.CompanyInfo.SharesRestrictIndex;
    CompanyInfo.DontOfferIndex            := Company.CompanyInfo.DontOfferIndex;
    CompanyInfo.ShareHoldersNoIndex       := Company.CompanyInfo.ShareHoldersNoIndex;
    CompanyInfo.SharesRestrictChapters    := Company.CompanyInfo.SharesRestrictChapters;
    CompanyInfo.DontOfferChapters         := Company.CompanyInfo.DontOfferChapters;
    CompanyInfo.SharesRestrictChapters    := Company.CompanyInfo.ShareHoldersNoChapters;
    CompanyInfo.LimitedSignatoryIndex     := Company.CompanyInfo.LimitedSignatoryIndex;
    CompanyInfo.LegalSectionsChapters     := Company.CompanyInfo.LegalSectionsChapters;
    CompanyInfo.SignatorySectionsChapters := Company.CompanyInfo.SignatorySectionsChapters;
    CompanyInfo.ConfirmedCopies           := Company.CompanyInfo.ConfirmedCopies;
    CompanyInfo.MoreCopies                := Company.CompanyInfo.MoreCopies;
    CompanyInfo.RegulationItemsIndex      := Company.CompanyInfo.RegulationItemsIndex;
    CompanyInfo.SaveData(Company, False, FileNum);
    NewRec := CompanyInfo.RecNum;
    CompanyInfo.Free;
    FilesList.Items[FilesList.Count - 1].SaveChange(ocCompany,CurrRec , acSignName, NewRec);
    Company.Free;
    Data.taSigName.Filtered := False;
  end;
end;

constructor TListedClass.Create(Parent: TCompany; LastChanged: TDateTime);
begin
  Inherited Create(Parent);
  FLastChanged:= LastChanged;
end;

procedure TListedClass.DeActivate(ChangeDate: TDateTime);
begin
  FActive:= False;
  FLastChanged:= ChangeDate;
end;

constructor TListedClass.CreateNew(LastChanged: TDateTime; Const Name, Zeut: String);
begin
  inherited CreateNew(LastChanged);
  FZeut:= Zeut;
  FName:= Name;
end;

// TTaagid

class procedure TTaagid.Undo(RecNum: Integer);
begin
  If RecNum <> -1 Then
    With Data.taTaagid do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger := RecNum;
      If GoToKey Then
        Delete;
    end;
end;

procedure TTaagid.DeleteEnum(ObjectNum, NewNum: Integer);
begin
  With DATA.taTaagid do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= NewNum;
    If GotoKey Then
      Delete;
  end;
  FFileItem.DeleteRecord(ocDirector, ObjectNum, acTaagidAddress, NewNum);
end;

procedure TTaagid.FileDelete(Parent: TDirector; FileItem: TFileItem);
var
  Changes: TChanges;
  RecNum: Integer;
begin
  FFileItem:= FileItem;
  If (FileItem<>nil) and (FileItem.FFileInfo.Draft) then
  begin
    If FileItem.EnumChanges(ocDirector, Parent.RecNum, acTaagidAddress, DeleteEnum)<>1 Then
      raise Exception.Create('Record count error at TTaagid.FileDelete');
  end
  Else
  begin
    Changes:= TChanges.Create(Parent.Parent);
    try
      With DATA.taTaagid Do
      begin
        EditKey;
        RecNum:= Changes.GetLastChange(ocDirector, Parent.RecNum, acTaagidAddress);
        FieldByName('RecNum').AsInteger:= RecNum;
        If {not }GotoKey then
         // raise Exception.Create('Record not found at TTaagid.FileDelete');
          Delete;
        Changes.FileDelete(ocDirector, Parent.RecNum, acTaagidAddress, RecNum);
        If FileItem<>nil then
          FFileItem.DeleteRecord(ocDirector, Parent.RecNum, acTaagidAddress, RecNum);
      end;
    finally
      Changes.Free;
    end;
  end;
end;

procedure TTaagid.ExSaveData(FileItem: TFileItem; Parent: TDirector; ChangeDate: TDateTime);
begin
  SaveData(Parent, ChangeDate, FileItem.FileInfo.Draft, FileItem.FileInfo.RecNum);
  FileItem.SaveChange(ocDirector, Parent.RecNum, acTaagidAddress, RecNum);
end;

procedure TTaagid.SaveData(Parent: TDirector; ChangeDate: TDateTime; Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9 + '--------------------------');
  WriteChangeLog(#9#9#9 + 'Saving taagid data (' + Self.Name + ', ' + Zeut + ')');
  { <-- }
  With DATA.taTaagid Do
  begin
    Append;
    FieldByName('Name').AsString := Self.Name;
    FieldByName('Zeut').AsString := Zeut;
    FieldByName('Street').AsString := Street;
    FieldByName('Number').AsString := Number;
    FieldByName('Index').AsString := Index;
    FieldByName('City').AsString := City;
    FieldByName('Country').AsString := Country;
    Post;
    FRecNum:= FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog( #9#9#9#9 + 'RecNum after save: ' + IntToStr(FRecNum));
    { <-- }
    If not Draft Then
    begin
      Changes:= TChanges.Create(Parent.Parent);
      try
        Changes.SaveChanges(ocDirector, Parent.RecNum, acTaagidAddress, RecNum, ChangeDate, FileNum);
      finally
        Changes.Free;
      end;
    end;
  end;
end;

constructor TTaagid.CreateNew(Const Street, Number, Index, City, Country, Name, Zeut: String);
begin
  Inherited CreateNew(Street, Number, Index, City, Country);
  FName   := Name;
  FZeut   := Zeut;
end;

constructor TTaagid.LoadFromFile(ParentNum: Integer; FileItem: TFileItem);
begin
  If FileItem.EnumChanges(ocDirector, ParentNum, acTaagidAddress, LoadEnum) > 0 then
    exit;
  FName      := '';
  FZeut      := '';
  FStreet    := '';
  FNumber    := '';
  FIndex     := '';
  FCity      := '';
  FCountry   := '';
end;

procedure TTaagid.LoadEnum(ObjectNum, NewNum: Integer);
begin
  Create(NewNum);
end;

constructor TTaagid.Create(RecNum: Integer);
begin
  inherited Create;
  With DATA.taTaagid Do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If not GotoKey Then
      raise Exception.Create('Record not found at TTaagid.Create');
    With Self Do
    begin
      FName      := FieldByName('Name').AsString;
      FZeut      := FieldByName('Zeut').AsString;
      FStreet    := FieldByName('Street').AsString;
      FNumber    := FieldByName('Number').AsString;
      FIndex     := FieldByName('Index').AsString;
      FCity      := FieldByName('City').AsString;
      FCountry   := FieldByName('Country').AsString;
    end;
  end;
end;

// TStockHolderAddress
//----------------------------------------------
class procedure TStockHolderAddress.Undo(RecNum: Integer);
begin
  With Data.taStockAddress do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If GoToKey Then
      Delete;
  end;
end;

procedure TStockHolderAddress.ExSaveData(FileItem: TFileItem; Parent: TStockHolder; ChangeDate: TDateTime);
begin
  SaveData(Parent, ChangeDate, FileItem.FileInfo.Draft, FileItem.FileInfo.RecNum);
  FileItem.SaveChange(ocStockHolder, Parent.RecNum, acAddress, RecNum);
end;

constructor TStockHolderAddress.LoadFromFile(ParentNum: Integer; FileItem: TFileItem);
begin
  FileItem.EnumChanges(ocStockHolder, ParentNum, acAddress, LoadEnum);
end;

procedure TStockHolderAddress.LoadEnum(ObjectNum, NewNum: Integer);
begin
  Create(NewNum);
end;

constructor TStockHolderAddress.Create(RecNum: Integer);
begin
  inherited Create;
  with DATA.taStockAddress do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger := RecNum;
    if not GotoKey then
      raise Exception.Create('Record not found at TStockHolderAddress.Create');
    with Self do
    begin
      FRecNum  := RecNum;
      FStreet  := FieldByName('Street').AsString;
      FNumber  := FieldByName('Number').AsString;
      FIndex   := FieldByName('Index').AsString;
      FCity    := FieldByName('City').AsString;
      FCountry := FieldByName('Country').AsString;
    end;
  end;
end;

procedure TStockHolderAddress.FileDelete(Parent: TStockHolder; FileItem: TFileItem);
var
  Changes: TChanges;
  RecNum: Integer;
begin
  FFileItem:= FileItem;
  If (FileItem<>nil) and (FileItem.FFileInfo.Draft) then
    FileItem.EnumChanges(ocStockHolder, Parent.RecNum, acAddress, DeleteEnum)
  Else
  begin
    Changes := TChanges.Create(Parent.Parent);
    try
      With DATA.taStockAddress Do
      begin
        EditKey;
        RecNum := Changes.GetLastChange(ocStockHolder, Parent.RecNum, acAddress);
        FieldByName('RecNum').AsInteger:= RecNum;
        If GotoKey then
        begin
          Delete;
          Changes.FileDelete(ocStockHolder, Parent.RecNum, acAddress, RecNum);
          If FileItem <> nil then
            FFileItem.DeleteRecord(ocStockHolder, Parent.RecNum, acAddress, RecNum);
        end;
      end;
    finally
      Changes.Free;
    end;
  end;
end;

procedure TStockHolderAddress.DeleteEnum(ObjectNum, NewNum: Integer);
begin
  With DATA.taStockAddress do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= NewNum;
    If GotoKey Then
      Delete;
  end;
  FFileItem.DeleteRecord(ocStockHolder, ObjectNum, acAddress, NewNum);
end;

procedure TStockHolderAddress.SaveData(Parent: TStockHolder; ChangeDate: TDateTime; Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog( #9#9#9 + '--------------------------');
  WriteChangeLog( #9#9#9 + 'Saving stock holder address (' + Parent.Name + ')');
  { <-- }
  With DATA.taStockAddress Do
  begin
    Append;
    FieldByName('Street').AsString := Street;
    FieldByName('Number').AsString := Number;
    FieldByName('Index').AsString := Index;
    FieldByName('City').AsString := City;
    FieldByName('Country').AsString := Country;
    Post;
    FRecNum := FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog( #9#9#9#9 + 'RecNum after save: ' + IntToStr( FRecNum ));
    { <-- }
    If not Draft Then
    begin
      Changes:= TChanges.Create(Parent.Parent);
      try
        Changes.SaveChanges(ocStockHolder, Parent.RecNum, acAddress, RecNum, ChangeDate, FileNum);
      finally
        Changes.Free;
      end;
    end;
  end;
end;

// TDirectorAddress
//----------------------------------------------
class procedure TDirectorAddress.Undo(RecNum: Integer);
begin
  Data.taDirAddress.EditKey;
  Data.taDirAddress.FieldByName('RecNum').AsInteger:= RecNum;
  If Data.taDirAddress.GoToKey Then
    Data.taDirAddress.Delete;
end;

procedure TDirectorAddress.DeleteEnum(ObjectNum, NewNum: Integer);
begin
  With DATA.taDirAddress do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= NewNum;
    If GotoKey Then
      Delete;
  end;
  FFileItem.DeleteRecord(ocDirector, ObjectNum, acAddress, NewNum);
end;

procedure TDirectorAddress.FileDelete(Parent: TDirector; FileItem: TFileItem);
var
  Changes: TChanges;
begin
  FFileItem:= FileItem;
  If (FileItem<>nil) and (FileItem.FFileInfo.Draft) then
  begin
    FileItem.EnumChanges(ocDirector, Parent.RecNum, acAddress, DeleteEnum);
  end
  Else
  begin
    Changes:= TChanges.Create(Parent.Parent);
    try
      With DATA.taDirAddress Do
      begin
        EditKey;
        FRecNum:= Changes.GetLastChange(ocDirector, Parent.RecNum, acAddress);
        FieldByName('RecNum').AsInteger:= RecNum;
        If GotoKey then
          Delete;
        Changes.FileDelete(ocDirector, Parent.RecNum, acAddress, RecNum);
        If FileItem<>nil then
          FFileItem.DeleteRecord(ocDirector, Parent.RecNum, acAddress, RecNum);
      end;
    finally
      Changes.Free;
    end;
  end;
end;

procedure TDirectorAddress.ExSaveData(FileItem: TFileItem; Parent: TDirector; ChangeDate: TDateTime);
begin
  SaveData(Parent, ChangeDate, FileItem.FileInfo.Draft, FileItem.FileInfo.RecNum);
  FileItem.SaveChange(ocDirector, Parent.RecNum, acAddress, RecNum);
end;

procedure TDirectorAddress.SaveData(Parent: TDirector; ChangeDate: TDateTime; Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog( #9#9#9 + '--------------------------');
  WriteChangeLog( #9#9#9 + 'Saving director address (' + Parent.Name + ')');
  { <-- }
  With DATA.taDirAddress Do
  begin
    Append;
    FieldByName('Phone').AsString := Phone;
    FieldByName('Street').AsString := Street;
    FieldByName('Number').AsString := Number;
    FieldByName('Index').AsString := Index;
    FieldByName('City').AsString := City;
    FieldByName('Country').AsString := Country;
    Post;
    FRecNum := FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog( #9#9#9#9 + 'RecNum after save: ' + IntToStr( FRecNum ));
    { <-- }
    If not Draft Then
    begin
      Changes:= TChanges.Create(Parent.Parent);
      try
        Changes.SaveChanges(ocDirector, Parent.RecNum, acAddress, RecNum, ChangeDate, FileNum);
      finally
        Changes.Free;
      end;
    end;
  end;
end;

//======
constructor TDirectorAddress.CreateNew(Const Street, Number, Index, City, Country, Phone: String);
begin
  Inherited CreateNew(Street, Number, Index, City, Country);
  FPhone:= Phone;
end;

constructor TDirectorAddress.LoadFromFile(ParentNum: Integer; FileItem: TFileItem);
begin
  FileItem.EnumChanges(ocDirector, ParentNum, acAddress, LoadEnum);
end;

procedure TDirectorAddress.LoadEnum(ObjectNum, NewNum: Integer);
begin
  Create(NewNum);
end;

constructor TDirectorAddress.Create(RecNum: Integer);
begin
  inherited Create;
  DATA.taDirAddress.EditKey;
  DATA.taDirAddress.FieldByName('RecNum').AsInteger := RecNum;
  If not DATA.taDirAddress.GotoKey Then
    raise Exception.Createfmt('Record %d not found TDirectorAddress.Create', [recNum])
  else
  begin
    Self.FPhone   := DATA.taDirAddress.FieldByName('Phone').AsString;
    Self.FStreet  := DATA.taDirAddress.FieldByName('Street').AsString;
    Self.FNumber  := DATA.taDirAddress.FieldByName('Number').AsString;
    Self.FIndex   := DATA.taDirAddress.FieldByName('Index').AsString;
    Self.FCity    := DATA.taDirAddress.FieldByName('City').AsString;
    Self.FCountry := DATA.taDirAddress.FieldByName('Country').AsString;
  end;
end;

// TListObject
//----------------------------------------------
procedure TListObject.SetRegistrationDate(Date: TDateTime; Changes: TChanges);
var
  I: Integer;
begin
  Filter := afAll;
  For I := 0 To Count - 1 do
    Items[I].SetRegistrationDate(Date, Changes);
end;

procedure TListObject.Add(NewChild: TListedClass);
begin
  FFullList.Add(NewChild);
  If NewChild.Active Then
    FActiveList.Add(NewChild)
  Else
    FNonActiveList.Add(NewChild);
end;

function TListObject.GetCount: Integer;
begin
  Result:= FCurrentList.Count;
end;

function TListObject.GetData(Index: Integer): TListedClass;
begin
  Result:= TListedClass(FCurrentList.Items[Index]);
end;

procedure TListObject.SetFilter(Value: TActivityFilter);
begin
  FFilter:= Value;
  Case Value of
    afAll : FCurrentList:= FFullList;
    afActive : FCurrentList:= FActiveList;
    afNonActive : FCurrentList:= FNonActiveList;
  end;
end;

constructor TListObject.LoadFromFile(Parent: TCompany; FileItem: TFileItem);
begin
  Inherited Create(Parent);
  FActiveList:= TList.Create;
  FNonActiveList:= TList.Create;
  FFullList:= TList.Create;
  SetFilter(afAll);
  FFileItem:= FileItem;
  LoadData;
end;

constructor TListObject.Create(Parent: TCompany);
begin
  Inherited Create(Parent);
  FActiveList:= TList.Create;
  FNonActiveList:= TList.Create;
  FFullList:= TList.Create;
  SetFilter(afAll);
  FillData;
end;

constructor TListObject.CreateNew;
begin
  Inherited CreateNew(TempDate);
  FActiveList:= TList.Create;
  FNonActiveList:= TList.Create;
  FFullList:= TList.Create;
  SetFilter(afActive);
end;

procedure TListObject.FileDelete(FileItem: TFileItem);
var
  I: Integer;
begin
  For I:= 0 To Count-1 Do
    Items[I].FileDelete(FileItem);
end;

procedure TListObject.SaveData(Parent: TBasicClass; FileNum: Integer);
var
  I: Integer;
begin
  Filter:= afAll;
  For I:= 0 To Count-1 Do
    Items[I].SaveData(Parent, True, False, FileNum);
end;

procedure TListObject.ExSaveData(FileItem: TFileItem; Parent: TBasicClass);
var
  I: Integer;
begin
  Filter := afAll;
  For I := 0 To Count - 1 Do
    Items[I].ExSaveData(FileItem, Parent);
end;

destructor TListObject.Deconstruct;
begin
  FFullList.Free;
  FActiveList.Free;
  FNonActiveList.Free;
  Inherited Destroy;
end;

destructor TListObject.Destroy;

  procedure FreeObjects(List: TList);
  var
    I: Integer;
  begin
    For I := List.Count - 1 DownTo 0 Do
      TListedClass(List.Items[I]).Free;
    List.Free;
  end;

begin
  FreeObjects(FFullList);
  FActiveList.Free;
  FNonActiveList.Free;
  Inherited;
end;

function TListObject.FindByZeut(const Zeut: String): TListedClass;
var
  I: Integer;
begin
  Result:= Nil;
  For I:= 0 To Count -1 Do
    If Items[I].Zeut = Zeut Then
    begin
      Result:= Items[I];
      Exit;
    end;
end;

function TListObject.FindByName(const Name: String): TListedClass;
var
  I: Integer;
begin
  Result:= Nil;
  For I:= 0 To Count -1 Do
    If Items[I].Name = Name Then
    begin
      Result:= Items[I];
      Exit;
    end;
end;

function TListObject.FindByRecNum(const recNum: integer): TListedClass;
var
  I: Integer;
begin
  Result:= Nil;
  For I:= 0 To Count -1 Do
    If Items[I].RecNum = recNum Then
    begin
      Result:= Items[I];
      Exit;
    end;
end;

//======
//======
// TStocks
//----------------------------------------------
constructor TStocks.LoadFromFile(Parent: TStockHolder; FileItem: TFileItem);
begin
  FParent:= Parent;
  inherited LoadFromFile(Parent.Parent, FileItem);
end;

procedure TStocks.LoadEnum(ObjectNum, NewNum: Integer);
var
  Temp: TStock;
begin
  Temp:= TStock.LoadFromFile(Parent, NewNum);
  FFullList.Add(Temp);
  FActiveList.Add(Temp);
end;

procedure TStocks.LoadData;
begin
  FFileItem.EnumChanges(ocStockHolder, Parent.RecNum, acNewStock, LoadEnum);
end;

constructor TStocks.Create(Parent: TStockHolder);
begin
  FParent:= Parent;
  inherited Create(Parent.Parent);
end;

function TStocks.EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
var
  I: Integer;
  Found: Boolean;
begin
  Found:= False;
  For I:= 0 To Count-1 Do
    If Items[I].RecNum = Line Then
    begin
      Found:= True;
      Break;
    end;
  If not Found Then
    TList(Param).Add(TStock.Create(Line, Parent, Date));
  Result:= True;
end;

procedure TStocks.FillData;
var
  I: Integer;
  Changes: TChanges;
begin
  Changes := TChanges.Create(Parent.Parent);
  try
    Changes.EnumChanges(ocStockHolder, Parent.RecNum, acNewStock, EnumAll, FFullList);
    for I := 0 to Count - 1 do
      If TStock(FFullList.Items[I]).Active Then
        FActiveList.Add(FFullList.Items[I])
      Else
        FNonActiveList.Add(FFullList.Items[I]);
  finally
    Changes.Free;
  end;
end;

function TStocks.FindStock(const Name: String; Value: Double): TStock;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
  begin
    if (TStock(Items[I]).Name = Name) and (TStock(Items[I]).Value = Value) then
    begin
      Result := (Items[I] as TStock);
      Exit;
    end;
  end;
end;

// TManagers
//----------------------------------------------
function TManagers.EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
var
  I: Integer;
  Found: Boolean;
begin
  Found := False;
  For I := 0 To Count - 1 Do
    If (Items[I].RecNum = Line) Then
    begin
      Found:= True;
      Break;
    end;
  If not Found Then
    TList(Param).Add(TManager.Create(Line, Parent, Date));
  Result:= True;
end;

procedure TManagers.FillData;
var
  I: Integer;
  Changes: TChanges;
begin
  Changes:= TChanges.Create(Parent);
  try
    Changes.EnumChanges(ocCompany, Parent.RecNum, acNewManager, EnumAll, FFullList);
    For I:= 0 To Count-1 Do
      If TManager(FFullList.Items[I]).Active Then
        FActiveList.Add(FFullList.Items[I])
      Else
        FNonActiveList.Add(FFullList.Items[I]);
  finally
    Changes.Free;
  end;
end;

procedure TManagers.LoadEnum(ObjectNum, NewNum: Integer);
var
  Temp: TManager;
begin
  Temp:= TManager.LoadFromFile(Parent, NewNum);
  FFullList.Add(Temp);
  FActiveList.Add(Temp);
end;

procedure TManagers.LoadData;
begin
  FFileItem.EnumChanges(ocCompany, Parent.RecNum, acNewManager, LoadEnum);
end;

// THonList
function THonList.EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
var
  i: Integer;
  Found: Boolean;
begin
  Found := False;
  for i := 0 to Count-1 do
  begin
    if (Items[i].RecNum = Line) then
    begin
      Found:= True;
      Break;
    end;
  end;
  if (not Found) then
    TList(Param).Add(THonItem.Create(Line, Parent, Date));
  Result := True;
end;

procedure THonList.FillData;
var
  i: Integer;
  Changes: TChanges;
begin
  Changes := TChanges.Create(Parent);
  try
    Changes.EnumChanges(ocCompany, Parent.RecNum, acNewHon, EnumAll, FFullList);
    for i := 0 to Count-1 do
    begin
      if THonItem(FFullList.Items[i]).Active then
        FActiveList.Add(FFullList.Items[i])
      else
        FNonActiveList.Add(FFullList.Items[i]);
    end;
  finally
    Changes.Free;
  end;
end;

procedure THonList.LoadEnum(ObjectNum, NewNum: Integer);
var
  TempHon: THonItem;
begin
  TempHon:= THonItem.LoadFromFile(Parent, NewNum);
  FFullList.Add(TempHon);
  if TempHon.Active then
    FActiveList.Add(TempHon)
  else
    FNonActiveList.Add(TempHon);
end;

procedure THonList.LoadData;
begin
  FFileItem.EnumChanges(ocCompany, Parent.RecNum, acNewHon, LoadEnum);
end;

function THonList.FindHonItem(const Name: String; Value: Double): THonItem;
var
  I: Integer;
begin
  Result := nil;
  for I:= 0 to Count -1 do
  begin
    if (THonItem(Items[I]).Name = Name) and (THonItem(Items[I]).Value = Value) then
    begin
      Result:= (Items[I] as THonItem);
      Exit;
    end;
  end;
end;

// TMuhazList
//----------------------------------------------
function TMuhazList.EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
var
  I: Integer;
  Found: Boolean;
begin
  Found:= False;
  For I:= 0 To Count-1 Do
    If Items[I].RecNum = Line Then
    begin
      Found:= True;
      Break;
    end;
  If not Found Then
    TList(Param).Add(TMuhazItem.Create(Line, Parent, Date));
  Result:= True;
end;

procedure TMuhazList.FillData;
var
  I: Integer;
  Changes: TChanges;
begin
  Changes := TChanges.Create(Parent);
  try
    Changes.EnumChanges(ocCompany, Parent.RecNum, acNewMuhaz, EnumAll, FFullList);
    For I := 0 To Count-1 Do
      If TMuhazItem(FFullList.Items[I]).Active Then
        FActiveList.Add(FFullList.Items[I])
      Else
        FNonActiveList.Add(FFullList.Items[I]);
  finally
    Changes.Free;
  end;
end;

procedure TMuhazList.LoadEnum(ObjectNum, NewNum: Integer);
var
  TempMuhaz: TMuhazItem;
begin
  TempMuhaz:= TMuhazItem.LoadFromFile(Parent, NewNum);
  FFullList.Add(TempMuhaz);
  FActiveList.Add(TempMuhaz);
end;

procedure TMuhazList.LoadData;
begin
  FFileItem.EnumChanges(ocCompany, Parent.RecNum, acNewMuhaz, LoadEnum);
end;

function TMuhazList.CountMuhaz(Const Name: String; Value: Double): Double;
var
  I: Integer;
begin
  Result:= 0;
  For I:= 0 to Count-1 do
    If ((Items[I] as TMuhazItem).Name = Name) And ((Items[I] as TMuhazItem).Value = Value) Then
      Result:= Result + (Items[I] as TMuhazItem).ShtarValue;
end;

// TDirectors
//----------------------------------------------

function TDirectors.EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
var
  i: Integer;
  Found: Boolean;
begin
  Found := False;
  for i := 0 to Count-1 do
  begin
    if Items[i].RecNum = Line then
    begin
      Found := True;
      Break;
    end;
  end;
  if not Found then
    TList(Param).Add(TDirector.Create(Line, Parent, Date));
  Result:= True;
end;

procedure TDirectors.FillData;
var
  I: Integer;
  Changes: TChanges;
begin
  Changes:= TChanges.Create(Parent);
  try
    Changes.EnumChanges(ocCompany, Parent.RecNum, acNewDirector, EnumAll, FFullList);
    For I := 0 To Count - 1 Do
      If TDirector(FFullList.Items[I]).Active Then
        FActiveList.Add(FFullList.Items[I])
      Else
        FNonActiveList.Add(FFullList.Items[I]);
  finally
    Changes.Free;
  end;
end;

procedure TDirectors.LoadEnum(ObjectNum, NewNum: Integer);
var
  TempDirector: TDirector;
begin
  TempDirector:= TDirector.LoadFromFile(FFileItem, Parent, NewNum);
  FFullList.Add(TempDirector);
  FActiveList.Add(TempDirector);
end;

procedure TDirectors.LoadData;
begin
  FFileItem.EnumChanges(ocCompany, Parent.RecNum, acNewDirector, LoadEnum);
end;

// TCompanies
//----------------------------------------------
function TCompanies.GetData(Index: Integer): TCompany;
begin
  Result:= TCompany.Create(PCompaniesRecord(FCurrentList.Items[Index]).RecNum, FShowingDate, SearchLoad);
end;

function TCompanies.GetRecNum(Index: Integer): Integer;
begin
  Result:= PCompaniesRecord(FCurrentList.Items[Index]).RecNum;
end;

function TCompanies.GetName(Index: Integer): String;
begin
  Result:= PCompaniesRecord(FCurrentList.Items[Index]).Name;
end;

function TCompanies.GetNumber(Index: Integer): String;
begin
  Result:= PCompaniesRecord(FCurrentList.Items[Index]).Number;
end;

constructor TCompanies.Create(ShowingDate: TDateTime; ShowDisabled: Boolean; SearchLoad: Boolean);
begin
  FSearchLoad:= SearchLoad;
  FShowDisabled:= ShowDisabled;
  Inherited Create(nil);
  FShowingDate:= ShowingDate;
end;

destructor TCompanies.Destroy;
  procedure DestroyList(List :TList);
  var
    I: Integer;
  begin
    For I:= 0 To List.Count-1 Do
      Dispose(PCompaniesRecord(List.Items[I]));
    List.Free;
  end;

begin
  DestroyList(FFullList);
  FActiveList.Free;
  FNonActiveList.Free;
end;

procedure TCompanies.FillData;
  function EnumAll(Line: Integer; Const Name, Number: String): Boolean;
  var
    I: Integer;
    Found: Boolean;
    PNewRec: PCompaniesRecord;
  begin
    Found := False;
    For I := 0 To Count - 1 Do
      If PCompaniesRecord(FFullList.Items[I]).RecNum = Line Then
      begin
        Found := True;
        Break;
      end;
    If not Found Then
    begin
      PNewRec := New(PCompaniesRecord);
      PNewRec.RecNum := Line;
      PNewRec.Name := Name;
      PNewRec.Number := Number;
      FFullList.Add(PNewRec);
      If (Number <> '����') and (Number <> '-1') Then
        FActiveList.Add(PNewRec)
      Else
        FNonActiveList.Add(PNewRec);
    end;
    Result:= True;
  end;

begin
  With DATA.taName Do
  begin
    If ShowDisabled Then
      Filter:= '([ResInt1] = ''1'')'
    Else
      Filter:= '([ResInt1] <> ''1'')';

    Filtered:= True;
    Last;
    while not BOF Do
    begin
      EnumAll(FieldByName('RecNum').AsInteger, FieldByName('Name').AsString, FieldByName('CompNum').AsString);
      Prior;
    end;
  end;
end;

// TStockHolders
//----------------------------------------------
function TStockHolders.EnumAll(Line: Integer; Date: TDateTime; Param: Pointer): Boolean;
var
  i: Integer;
  Found: Boolean;
begin
  Found := False;
  for i := 0 to Count-1 do
  begin
    if Items[i].RecNum = Line then
    begin
      Found := True;
      Break;
    end;
  end;
  if not Found then
    TList(Param).Add(TStockHolder.Create(Line, Parent, Date));
  Result := True;
end;

procedure TStockHolders.FillData;
var
  I: Integer;
  Changes: TChanges;
begin
  Changes := TChanges.Create(Parent);
  try
    Changes.EnumChanges(ocCompany, Parent.RecNum, acNewStockHolder, EnumAll, FFullList);
    For I := 0 To Count - 1 Do
      If TStockHolder(FFullList.Items[I]).Active Then
        FActiveList.Add(FFullList.Items[I])
      Else
        FNonActiveList.Add(FFullList.Items[I]);
  finally
    Changes.Free;
  end;
end;

procedure TStockHolders.LoadEnum(ObjectNum, NewNum: Integer);
var
  Temp: TStockHolder;
begin
  Temp:= TStockHolder.LoadFromFile(FFileItem, Parent, NewNum);
  FFullList.Add(Temp);
  FActiveList.Add(Temp);
end;

procedure TStockHolders.LoadData;
begin
  FFileItem.EnumChanges(ocCompany, Parent.RecNum, acNewStockHolder, LoadEnum);
end;

// TStock
//----------------------------------------------
procedure TStock.SetRegistrationDate(Value: TDateTime; Changes: TChanges);
begin
  Changes.ChangeRegistrationDate(ocStockHolder, Parent.RecNum, acNewStock, RecNum, Value);
end;

class procedure TStock.Undo(RecNum: Integer);
begin
  With Data.taStockNum do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger := RecNum;
    If GoToKey Then
      Delete;
  end;
end;

procedure TStock.FileDelete(FileItem: TFileItem);
var
  Changes: TChanges;
begin
  If (FileItem=nil) Or (not FileItem.FFileInfo.Draft) then
    Changes:= TChanges.Create(Parent.Parent)
  Else
    Changes:= Nil;
  try
    With DATA.taStockNum Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= RecNum;
      If GotoKey then
        Delete;
      If Assigned(Changes) then
        Changes.FileDelete(ocStockHolder, Parent.RecNum, acNewStock, RecNum);
      If FileItem<>nil then
        FileItem.DeleteRecord(ocStockHolder, Parent.RecNum, acNewStock, RecNum);
    end;
  finally
    if Changes<>nil then
      Changes.Free;
  end;
end;

procedure TStock.ExSaveData(FileItem: TFileItem; Parent: TBasicClass);
begin
  SaveData(Parent, False, FileItem.FFileInfo.Draft, FileItem.FileInfo.RecNum);
  If Active Then
    FileItem.SaveChange(ocStockHolder, TStockHolder(Parent).RecNum, acNewStock, RecNum)
  Else
    FileItem.SaveChange(ocStock, RecNum, acDelete, RecNum);
end;

procedure TStock.SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving stock data (' + Self.Name + ')');
  { <-- }
  If not Active Then
  begin
    Changes:= TChanges.Create((Parent as TStockHolder).Parent);
    try
      Changes.SaveChanges(ocStock, RecNum, acDelete, RecNum, LastChanged, FileNum);
    finally
      Changes.Free;
    end;
    Exit;
  end;

  With DATA.taStockNum Do
  begin
    Append;
    FieldByName('Name').AsString := Self.Name;
    FieldByName('Value').AsFloat := Value;
    FieldByName('Paid').AsFloat  := Paid;
    FieldByName('Count').AsFloat := Count;
    Post;
    FRecNum := FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog( #9#9#9#9 + 'RecNum after save: ' + IntToStr( FRecNum ));
    { <-- }
    If not Draft Then
    begin
      Changes:= TChanges.Create((Parent as TStockHolder).Parent);
      try
        Changes.SaveChanges(ocStockHolder, (Parent as TStockHolder).RecNum, acNewStock, RecNum, LastChanged, FileNum);
      finally
        Changes.Free;
      end;
    end;
  end;
end;

constructor TStock.CreateNew(LastChanged: TDateTime; Const Name: String; Value, Count, Paid: Double; Active: Boolean);
begin
  Inherited CreateNew(LastChanged, Name, '');
  FValue := Value;
  FCount := Count;
  FPaid  := Paid;
  FActive:= Active;
end;

constructor TStock.LoadFromFile(Parent: TStockHolder; RecNum: Integer);
begin
  FParent:= Parent;
  FRecNum:= RecNum;
  With DATA.taStockNum Do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If not GotoKey Then
      raise Exception.Create('Record not found at TStock.LoadFromFile');
    FName:= FieldByName('Name').AsString;
    FValue:= FieldByName('Value').AsFloat;
    FPaid:= FieldByName('Paid').AsFloat;
    FCount:= FieldByName('Count').AsFloat;
    FActive:= True;
  end;
end;

constructor TStock.Create(RecNum: Integer; Parent: TStockHolder; LastChanged: TDateTime);
var
  Changes: TChanges;
begin
  Inherited Create(Parent.Parent, LastChanged);
  FParent := Parent;
  Changes := TChanges.Create(Parent.Parent);
  try
    With DATA.taStockNum Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger := RecNum;
      If not GotoKey Then
        raise Exception.Create('Record not found at TStock.Create');
      FName   := FieldByName('Name').AsString;
      FValue  := FieldByName('Value').AsFloat;
      FPaid   := FieldByName('Paid').AsFloat;
      FCount  := FieldByName('Count').AsFloat;
      FActive := (Changes.GetLastChange(ocStock, RecNum, acDelete) = -1);
      If not FActive Then
        FLastChanged := TempDate;
    end;
  finally
    Changes.Free;
  end;
  FRecNum:= RecNum;
end;

// TStockHolder
//----------------------------------------------
constructor TStockHolder.CreateNew(LastChanged: TDateTime; Const Name, Zeut: String; Taagid: Boolean; Address: TStockHolderAddress; Stocks: TStocks; Active: Boolean);
begin
  Inherited CreateNew(LastChanged, Name, Zeut);
  FAddress:= Address;
  FStocks:= Stocks;
  FTaagid:= Taagid;
  FActive:= Active;
end;

procedure TStockHolder.SetRegistrationDate(Value: TDateTime; Changes: TChanges);
begin
  Stocks.SetRegistrationDate(Value, Changes);
  Changes.ChangeRegistrationDate(ocStockHolder, RecNum, acAddress, -1, Value);
  Changes.ChangeRegistrationDate(ocCompany, Parent.RecNum, acNewStockHolder, RecNum, Value);
end;

destructor TStockHolder.Destroy;
begin
  Address.Free;
  Stocks.Free;
end;

class procedure TStockHolder.Undo(RecNum: Integer);
var
  Changes: TChanges;
begin
  Changes:= TChanges.Create(nil);
  try
    TStockHolderAddress.Undo(Changes.GetLastChange(ocStockHolder, RecNum, acAddress));
  finally
    Changes.Free;
  end;

  With Data.taStockHolders do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If GoToKey Then
      Delete;
  end;
end;

procedure TStockHolder.FileDelete(FileItem: TFileItem);
var
  Changes: TChanges;
begin
  Stocks.FileDelete(FileItem);
  Address.FileDelete(Self, FileItem);
  If (FileItem=nil) Or (not FileItem.FFileInfo.Draft) then
    Changes:= TChanges.Create(Parent)
  Else
    Changes:= Nil;
  try
    With DATA.taStockHolders Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= RecNum;
      If GotoKey then
        Delete;
      If Assigned(Changes) then
        Changes.FileDelete(ocCompany, Parent.RecNum, acNewStockHolder, RecNum);
      If FileItem<>nil then
        FileItem.DeleteRecord(ocCompany, Parent.RecNum, acNewStockHolder, RecNum);
    end;
  finally
    if Changes<>nil then
      Changes.Free;
  end;
end;

procedure TStockHolder.ExSaveData(FileItem: TFileItem; Parent: TBasicClass);
begin
  SaveData(Parent, False, FileItem.FileInfo.Draft, FileItem.FileInfo.RecNum);
  If Active Then
  begin
    FileItem.SaveChange(ocCompany, (Parent as TCompany).RecNum, acNewStockHolder, RecNum);
    Stocks.ExSaveData(FileItem, Self);
    Address.ExSaveData(FileItem ,Self, LastChanged);
  end
  Else
    FileItem.SaveChange(ocStockHolder, RecNum, acDelete, RecNum);
end;

procedure TStockHolder.SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
  TempRec: string;
  NewZeut: string;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving stock holder data (' + Self.Name + ')');
  { <-- }
  If not Active Then
  begin
    Changes:= TChanges.Create((Parent as TCompany).Parent);
    try
      Changes.SaveChanges(ocStockHolder, RecNum, acDelete, RecNum, LastChanged, FileNum);
    finally
      Changes.Free;
    end;
    Exit;
  end;

  NewZeut := '';
  with DATA.taStockHolders do
  begin
    CancelRange;
    Filter:= '(Zeut = '''+Zeut+''')' ;
    Filtered:= True;
    if RecordCount = 0 then
    begin
      Data.taForms.Filtered := False;
      Data.taForms.CancelRange;
      Data.taForms.Filter:= '(ResStr1 = '''+Zeut+''') and (Action = '''+ActionToChar(acNewZeut)+ ''')' ;
      Data.taForms.Filtered:= True;
      if Data.taForms.RecordCount<>0 then
      begin
         Data.taForms.Last;
         while (NewZeut = '') and (not Data.taForms.BOF) do
         begin
           TempRec := Data.taForms.FieldByName('RecNum').AsString;
           Data.taChanges.Filtered := False;
           Data.taChanges.CancelRange;
           Data.taChanges.Filter := '(NewNum = '+ TempRec+' ) and (Action = '''+ActionToChar(acNewZeut)+ ''')' ;
           Data.taChanges.Filtered := True;
           TempRec :=Data.taChanges.FieldByName('ObjectNum').AsString;
           if ( TempRec <> '') then
           begin
             Data.taChanges.Filtered :=False;
             DATA.taStockHolders.Filtered :=False;
             DATA.taStockHolders.Filter :='(RecNum = '+TempRec +')';
             DATA.taStockHolders.Filtered :=True;
             TempRec :=Data.taStockHolders.FieldByName('Zeut').AsString;
             DATA.taStockHolders.Filtered :=False;
             NewZeut:=TempRec;
           end;
            Data.taForms.Prior;
         end;
      end;
      Data.taForms.Filtered:= False;
    end;
    Filtered:= True;
    Append;
    FieldByName('Name').AsString:= Self.Name;
    FieldByName('Zeut').AsString:= Zeut;
    If Taagid then
      FieldByName('Taagid').AsString:= '1'
    Else
      FieldByName('Taagid').AsString:= '0';
    if NewZeut<>'' then
      FieldByName('ResStr1').AsString:=NewZeut;
    Post;

    FRecNum:= FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr( FRecNum ));
    { <-- }
    If not Draft Then
    begin
      Changes:= TChanges.Create(Parent as TCompany);
      try
        Changes.SaveChanges(ocCompany, (Parent as TCompany).RecNum, acNewStockHolder, RecNum, LastChanged, FileNum);
      finally
        Changes.Free;
      end;
    end;
    If SaveChild Then
    begin
      Address.SaveData(Self, LastChanged, Draft, FileNum);
      Stocks.SaveData(Self, FileNum);
    end;
    Filtered:= False;
  end;
end;

constructor TStockHolder.LoadFromFile(FileItem: TFileItem; Parent: TCompany; RecNum: Integer);
begin
  inherited Create(Parent, EncodeDate(1999, 01, 01));
  FRecNum:= RecNum;
  with Data.taStockHolders do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger := RecNum;
    if not GotoKey then
      raise Exception.Create('Record not found at TStockHolder.LoadFromFile');
    FZeut   := FieldByName('Zeut').AsString;
    FName   := FieldByName('Name').AsString;
    FTaagid := FieldByName('Taagid').AsString = '1';
  end;
  FStocks := TStocks.LoadFromFile(Self, FileItem);
  FAddress := TStockHolderAddress.LoadFromFile(RecNum, FileItem);
  FStocks.Filter := afActive;
  FActive := True;
end;

constructor TStockHolder.Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);
var
  Changes: TChanges;
  TempLineNumber: Integer;
begin
  inherited Create(Parent, LastChanged);
  FRecNum := RecNum;
  if not Parent.FastLoad then
    FStocks := TStocks.Create(Self);

  Data.taStockHolders.EditKey;
  Data.taStockHolders.FieldByName('RecNum').AsInteger:= RecNum;
  if not Data.taStockHolders.GotoKey then
    raise Exception.Create('Record not found at TStockHolder.Create #1');
  FZeut   := Data.taStockHolders.FieldByName('Zeut').AsString;
  FName   := Data.taStockHolders.FieldByName('Name').AsString;
  FTaagid := Data.taStockHolders.FieldByName('Taagid').AsString = '1';

  Changes := TChanges.Create(Parent);
  try
    Changes.EnumChanges(ocStockHolder, RecNum, acNewName, GetNameProc, nil);
    Changes.EnumChanges(ocStockHolder, RecNum, acNewZeut, GetZeutProc, nil);

    TempLineNumber := Changes.GetLastChange(ocStockHolder, RecNum, acAddress);
    if TempLineNumber = -1 then
      raise Exception.Create('Record not found at TStockHolder.Create #2');
    FAddress := TStockHolderAddress.Create(TempLineNumber);
    if Parent.FastLoad then
      FActive := True
    else
    begin
      FStocks.Filter := afActive;
      FActive := FStocks.Count > 0;
    end;
  finally
    Changes.Free;
  end;
end;

procedure TStockHolder.SaveDataToAll(SelectedStockHolders: TMultipleSelectRec;
           NewAddress: TStockHolderAddress; NewName, NewZeut: string; ChangeName,
           ChangeZeut, LastChange, FirstObject: boolean; FileInfo: TFileInfo);
var
  NewFileInfo : TFileInfo;
  CurrObj     : TObjectClass;
  CompanyNum  : integer;
  RecNum      : integer;
  DirPhone    : string;
begin
  CompanyNum := SelectedStockHolders.CompanyNum;

  if (SelectedStockHolders.PersonType = diStockHolder)  or (SelectedStockHolders.PersonType = diBoth) then
  begin
    CurrObj := ocStockHolder;
    RecNum  := SelectedStockHolders.StockHolderNum;

    with Data.taChanges do
    begin
      Filter := '([ObjectNum] = ' + intToStr(RecNum) + ') AND ([ObjectType] = '+ ObjToChar(CurrObj) + ')';
      Filtered := True;
      SaveSpecifiedData('', RecNum, FieldByName('ResInt1').AsInteger, CompanyNum, FileInfo,
                        NewAddress, NewName, NewZeut, ChangeName, ChangeZeut, CurrObj, NewFileInfo);
    end;
  end;

  if ((SelectedStockHolders.PersonType = diDirector) or (SelectedStockHolders.PersonType = diBoth)) and
     (SelectedStockHolders.DirectorNum <> 0) then
  begin
    CurrObj := ocDirector;
    RecNum  := SelectedStockHolders.DirectorNum;
    with Data.taChanges do
    begin
      Filter := '([ObjectNum] = ' + intToStr(RecNum) + ') AND ([ObjectType] = ' + ObjToChar(CurrObj) + ')';
      Filtered := True;
      Last;
      Data.taDirAddress.Filter := '[RecNum] = ' + FieldByName('NewNum').AsString;
      Data.taDirAddress.Filtered := True;
      Data.taDirAddress.First;
      DirPhone := Data.taDirAddress.FieldByName('Phone').AsString;
      SaveSpecifiedData(DirPhone, RecNum, FieldByName('ResInt1').AsInteger, CompanyNum, FileInfo,
                        NewAddress, NewName, NewZeut, ChangeName, ChangeZeut, CurrObj, NewFileInfo);
    end;
  end;

  //==>  30.6.05 Tsahi
  if (SelectedStockHolders.ManagerNum <> 0) then
    ChangeManager(CompanyNum ,NewZeut, NewName, NewAddress, FileInfo, NewFileInfo);
  if (SelectedStockHolders.SignNameNum <> 0) then
    ChangeSigName(CompanyNum, NewZeut, NewName, FileInfo);

  Data.taChanges.Filtered := False;

  if LastChange and ChangeZeut then
    FZeut := NewZeut;
end; // procedure TStockHolder.SaveDataToAll

//========
procedure TStockHolder.SaveSpecifiedData(Phone: string; DirRecNum, FileNum, CompanyNum: integer;  FileInfo: TFileInfo;
          NewAddress: TStockHolderAddress; NewName, NewZeut: string; ChangeName, ChangeZeut: boolean;
          ObjectType: TObjectClass; var NewFileInfo: TFileInfo);
var
  Changes: TChanges;
  Company: TCompany;
  S1,S2: string;
  AdTable: TTable;
  FormRecNum, NewDirAd: integer;
  FilesList: TFilesList;
  FileInfoAction: TFileAction;
  NewFileNum: integer;
  Director: TDirector;
begin
  Data.taChanges.Filtered := False;
  NewFileInfo := TFileInfo.Create;
  NewFileInfo.Assign(FileInfo);
  AdTable := Data.taDirAddress;
  FileInfoAction := faDirectorInfo;
  if ObjectType = ocDirector then
  begin
    AdTable := DATA.taDirAddress;
    FileInfoAction := faDirectorInfo;
  end else if ObjectType = ocStockHolder then
  begin
    AdTable := DATA.taStockAddress;
    FileInfoAction := faStockHolderInfo;
  end;
  AdTable.Filtered := False;

  if (ObjectType = ocStockHolder) and (DirRecNum = RecNum) then
    Exit; //save for last
  if CompanyNum = -1 then
    Exit;

  if FileNum <> -1 then
  begin
    try
    Data.qGeneric2.SQL.Text :=
      'SELECT ResInt1 ' +
      'FROM ' + Data.taChanges.TableName + ' ' +
      'WHERE ObjectNum = ' + IntTostr(DirRecNum) + ' AND ObjectType = ''' + ObjToChar(ObjectType) + '''';
    Data.qGeneric2.Open;
    while (not Data.qGeneric2.EOF) do
    begin
      if (Data.qGeneric2.FieldByName('ResInt1').AsInteger = FileNum) then
        Break;
      Data.qGeneric2.Next;
    end;
    if (Data.qGeneric2.EOF) then
      Exit;
    finally
      Data.qGeneric2.Close;
    end;
  end;

  Company := TCompany.Create(CompanyNum, Now, False);
  FilesList := TFilesList.Create(CompanyNum, FileInfoAction, False);
  NewFileInfo.CompanyNum := CompanyNum;
  NewFileInfo.Action := FileInfoAction;
  FilesList.Add(NewFileInfo);
  NewFileNum := NewFileInfo.RecNum;

  //DirAddreess
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving director address (' + Parent.Name + ')');

  With AdTable Do
  begin
    Append;
    if ObjectType = ocDirector then
      FieldByName('Phone').AsString := Phone;
    FieldByName('Street').AsString := NewAddress.Street;
    FieldByName('Number').AsString := NewAddress.Number;
    FieldByName('Index').AsString := NewAddress.Index;
    FieldByName('City').AsString := NewAddress.City;
    FieldByName('Country').AsString := NewAddress.Country;
    Post;
    NewDirAd := FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr( FRecNum ));
    { <-- }

    Changes := TChanges.Create(Company);
    try
      Changes.SaveChanges(ObjectType, DirRecNum, acAddress, NewDirAd, NewFileInfo.DecisionDate, NewFileNum);
    finally
      Changes.Free;
    end;
  end;

  // ChangeName
  if ChangeName then
  begin
    WriteChangeLog(#9#9#9#9'ChangeName in Forms: FileNum: ' + IntToStr(NewFileNum) +
      ', ObjectType: ' + ObjectToName(ObjectType) + ', ObjectNum: -1, Action: ' + ActionToName(acNewName) +
      ', NewNum: -1, ResStr1 + ResStr2: ' + NewName);
    with Data.taForms do
    begin
      Append;
      FieldByName('FileNum').AsInteger := NewFileNum;
      FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
      FieldByName('ObjectNum').AsInteger := -1;
      FieldByName('Action').AsString := ActionToChar(acNewName);
      FieldByName('NewNum').AsInteger := -1;
      S1 := Copy(NewName, 1, 20);
      S2 := Copy(NewName, 21, 80);
      FieldByName('ResStr1').AsString := S1;
      FieldByName('ResStr2').AsString := S2;
      Post;
      FormRecNum := FieldByName('RecNum').AsInteger;
      WriteChangeLog(#9#9#9#9'Saved: RecNum: '+ IntToStr(FormRecNum));

      if (Trim(NewName) <> '') and (NewName <> Self.Name) then
      begin
        Changes := TChanges.Create(Company);
        try
          Changes.SaveChanges(ObjectType, DirRecNum, acNewName, FormRecNum, NewFileInfo.DecisionDate, NewFileNum);
        finally
          Changes.Free;
        end;
      end;
    end;
  end;

  // ChangeZeut
  if ChangeZeut then
  begin
    with Data.taForms do
    begin
      WriteChangeLog(#9#9#9#9'ChangeZeut in Forms: FileNum: ' + IntToStr(NewFileNum) +
        ', ObjectType: ' + ObjectToName(ObjectType) + ', ObjectNum: -1, Action: ' + ActionToName(acNewZeut) +
        ', NewNum :-1, ResStr1: ' + NewZeut);
      Append;
      FieldByName('FileNum').AsInteger := NewFileNum;
      FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
      FieldByName('ObjectNum').AsInteger := -1;
      FieldByName('Action').AsString := ActionToChar(acNewZeut);
      FieldByName('NewNum').AsInteger := -1;
      S1 := Copy(NewZeut, 1, 20);
      FieldByName('ResStr1').AsString := S1;
      Post;
      FormRecNum := FieldByName('RecNum').AsInteger;
      WriteChangeLog(#9#9#9#9'Saved: RecNum: ' + IntToStr(FormRecNum));

      if (Trim(NewZeut) <> '') and (NewZeut <> Zeut) then
      begin
        Changes := TChanges.Create(Company);
        try
          Changes.SaveChanges(ObjectType, DirRecNum, acNewZeut, FormRecNum, NewFileInfo.DecisionDate, NewFileNum);
        finally
          Changes.Free;
        end;
      end;
    end;
  end;

  // Moshe 26/11/2018 - Save Taagid data
  if (ObjectType = ocDirector) then
  begin
    Director := Company.Directors.FindByZeut(Zeut) as TDirector;
    if (Assigned(Director.Taagid)) then
    begin
      if (Director.Taagid.Zeut = Zeut) then
      begin
        if (ChangeName) then
          Director.Taagid.Name := NewName;
        if (ChangeZeut) then
          Director.Taagid.Zeut := NewZeut;
        Director.Taagid.SaveData(Director, Now, False, NewFileNum);
      end;
    end;
  end;

  FilesList.Items[FilesList.Count - 1].SaveChange(ObjectType, DirRecNum, acAddress, NewDirAd);
  FilesList.Free;
  Company.Free;
end; // TStockHolder.SaveSpecifiedData

//------------------Added by Tsahi 27.08.06 ------------------------------------
function TStockHolder.ChangeManager(CompanyNum: integer; NewZeut,NewName: string; NewAddress: TStockHolderAddress; FileInfo, NewFileInfo: TFileInfo): integer;
var
  TempInfo: TFileInfo;
  FilesList: TFilesList;
  DraftManagers, NonDraft: TManagers;
  Company: Tcompany;
  FileNum: integer;
  //==========================================================
  function EvCollectData(var NonDraft: TManagers): TManagers;
    //==========================================================
    function AddNext(const Name, Zeut, Address, Phone, Mail: String): TManager;
    var
      TempManager: TManager;
    begin
      Result := TManager.CreateNew(FileInfo.DecisionDate, Name, Zeut, Address, Phone, Mail);
      TempManager := Company.Managers.FindByZeut(Zeut) as TManager;
      if TempManager<> nil then
        if (TempManager.Name = Result.Name) and
           (TempManager.Address = Result.Address) and
           (TempManager.Phone = Result.Phone) then
          Exit
        else
        begin
          TempManager.DeActivate(FileInfo.DecisionDate);
          NonDraft.Add(TempManager);
          NonDraft.Add(Result);
        end
      else
        NonDraft.Add(Result);
    end;
    //==========================================================
  var
    TempManager: TManager;
    I: Integer;
    PhoneNum, Mail: string;
  begin
    PhoneNum := '';
    Mail := '';
    Result := TManagers.CreateNew;
    NonDraft := Tmanagers.CreateNew;
    For I := 0 to Company.Managers.Count-1 do
      if TManager(Company.Managers.Items[i]).Zeut <> Zeut then
        Result.Add(AddNext(TManager(Company.Managers.Items[i]).Name,
                         TManager(Company.Managers.Items[i]).Zeut,
                         TManager(Company.Managers.Items[i]).Address,
                         TManager(Company.Managers.Items[i]).Phone,
                         TManager(Company.Managers.Items[i]).Mail))
      else if TManager(Company.Managers.Items[i]).Active then begin
        PhoneNum := TManager(Company.Managers.Items[i]).Phone;
        Mail := TManager(Company.Managers.Items[i]).Mail;
      end;

    Result.Add(AddNext(
                NewName,
                NewZeut,
                NewAddress.Street+' '+NewAddress.Number+' '+NewAddress.City+' '
                +NewAddress.Index+', '+NewAddress.Country,
                PhoneNum,
                Mail));

    Company.Managers.Filter := afActive;
    for I := Company.Managers.Count-1 downto 0 do
      begin
        TempManager := Result.FindByZeut(Company.Managers.Items[I].Zeut) as TManager;
        if TempManager=nil then
          begin
            TempManager:= Company.Managers.Items[I] as TManager;
            TempManager.DeActivate(FileInfo.DecisionDate);
            NonDraft.Add(TempManager);
          end;
      end;
  end;
  //==========================================================

begin
  DraftManagers := nil;
  Company:=TCompany.Create(CompanyNum,Now,False);
  try
    DraftManagers:= EvCollectData(NonDraft);
    FilesList:= TFilesList.Create(Company.RecNum, faManager, False);
    TempInfo:= TFileInfo.Create;
    TempInfo.Action:= faManager;
    TempInfo.Draft:= False;
    TempInfo.DecisionDate:= FileInfo.DecisionDate;
    TempInfo.notifyDate:= FileInfo.notifyDate;
    TempInfo.CompanyNum:= Company.RecNum;
    TempInfo.FileName := FileInfo.FileName;
    FilesList.Add(TempInfo);

    If FilesList.Items[FilesList.Count-1]<>nil Then
    begin
      NonDraft.ExSaveData(FilesList.Items[FilesList.Count-1], Company);//SaveData(Company, FileItem.FileInfo.RecNum);//
      TempInfo.Draft :=True;
      DraftManagers.ExSaveData(FilesList.Items[FilesList.Count-1], Company);
    end;

    NonDraft.Deconstruct;
    FileNum := TempInfo.RecNum;
    FilesList.Free;
  finally
    if (Assigned(DraftManagers)) then
      DraftManagers.Deconstruct;
    Company.Free;
  end;
  Result := FileNum;
end;

//================
// TMuhazItem
// --------------------------------------------
procedure TMuhazItem.SetRegistrationDate(Value: TDateTime; Changes: TChanges);
begin
  Changes.ChangeRegistrationDate(ocCompany, Parent.RecNum, acNewMuhaz, RecNum, Value);
end;

constructor TMuhazItem.CreateNew(LastChanged: TDateTime; Const Name, Zeut: String; Value, ShtarValue: Double; Active: Boolean);
begin
  Inherited CreateNew(LastChanged, Name, Zeut);
  FValue:= Value;
  FShtarValue:= ShtarValue;
  FActive:= Active;
end;

procedure TMuhazItem.FileDelete(FileItem: TFileItem);
var
  Changes: TChanges;
begin
  If (FileItem=nil) Or (not FileItem.FFileInfo.Draft) then
    Changes:= TChanges.Create(Parent)
  Else
    Changes:= Nil;
  try
    With DATA.taMuhaz Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= RecNum;
      If GotoKey then
        Delete;
      If Assigned(Changes) then
        Changes.FileDelete(ocCompany, Parent.RecNum, acNewMuhaz, RecNum);
      If FileItem<>nil then
        FileItem.DeleteRecord(ocCompany, Parent.RecNum, acNewMuhaz, RecNum);
    end;
  finally
    if Changes<> nil then
      Changes.Free;
  end;
end;

procedure TMuhazItem.ExSaveData(FileItem: TFileItem; Parent: TBasicClass);
begin
  SaveData(Parent, False, FileItem.FileInfo.Draft, FileItem.FileInfo.RecNum);
  If Active Then
    FileItem.SaveChange(ocCompany, (Parent as TCompany).RecNum, acNewMuhaz, RecNum)
  Else
    FileItem.SaveChange(ocMuhaz, RecNum, acDelete, RecNum);
end;

class procedure TMuhazItem.Undo(RecNum: Integer);
begin
  With Data.taMuhaz do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If GoToKey Then
    Delete;
  end;
end;

procedure TMuhazItem.SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving muhaz item data (' + Self.Name + ')');
  { <-- }
  If not Active Then
  begin
    Changes:= TChanges.Create((Parent as TCompany).Parent);
    try
      Changes.SaveChanges(ocMuhaz, RecNum, acDelete, RecNum, LastChanged, FileNum);
    finally
      Changes.Free;
    end;
    Exit;
  end;

  With DATA.taMuhaz Do
  begin
    Append;
    FieldByName('Name').AsString     := Self.Name;
    FieldByName('Value').AsFloat     := Value;
    FieldByName('ShtarNum').AsString := Zeut;
    FieldByName('ShtarValue').AsFloat:= ShtarValue;
    Post;
    FRecNum := FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog( #9#9#9#9'RecNum after save: ' + IntToStr(FRecNum));
    { <-- }
    If not Draft Then
    begin
      Changes:= TChanges.Create(Parent as TCompany);
      try
        Changes.SaveChanges(ocCompany, (Parent as TCompany).RecNum, acNewMuhaz, RecNum, FLastChanged, FileNum);
      finally
        Changes.Free;
      end;
    end;
  end;
end;

constructor TMuhazItem.LoadFromFile(Parent: TCompany; RecNum: Integer);
begin
  Create(RecNum, Parent, EncodeDate(1999,01,01));
end;

constructor TMuhazItem.Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);
var
  Changes: TChanges;
begin
  Inherited Create(Parent, LastChanged);
  Changes:= TChanges.Create(Parent);
  try
    With DATA.taMuhaz Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger := RecNum;
      If not GotoKey then
        raise Exception.Create('Record not found at TMuhazItem.Create');
      FName      := FieldByName('Name').AsString;
      FValue     := FieldByName('Value').AsFloat;
      FZeut      := FieldByName('ShtarNum').AsString;
      FShtarValue:= FieldByName('ShtarValue').AsFloat;
      FActive:= Changes.GetLastChange(ocMuhaz, RecNum, acDelete) = -1;
      FRecNum:= RecNum;
    end;
  finally
    Changes.Free;
  end;
end;

// THonItem
//----------------------------------------------
procedure THonItem.SetRegistrationDate(Value: TDateTime; Changes: TChanges);
begin
  Changes.ChangeRegistrationDate(ocCompany, Parent.RecNum, acNewHon, RecNum, Value);
end;

class procedure THonItem.Undo(RecNum: Integer);
begin
  With Data.taHon do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If GoToKey Then
      Delete;
  end;
end;

constructor THonItem.CreateNew(LastChanged: TDateTime; Const Name: String; Value, Rashum, Mokza, Cash, NonCash, Part, PartValue: Double; Active: Boolean);
begin
  Inherited CreateNew(LastChanged, Name, '');
  FActive    := Active;
  FValue     := Value;
  FRashum    := Rashum;
  FMokza     := Mokza;
  FCash      := Cash;
  FNonCash   := NonCash;
  FPart      := Part;
  FPartValue := PartValue;
end;

procedure THonItem.FileDelete(FileItem: TFileItem);
var
  Changes: TChanges;
begin
  If (FileItem=nil) Or (not FileItem.FFileInfo.Draft) then
    Changes:= TChanges.Create(Parent)
  Else
    Changes:= Nil;
  try
    With DATA.taHon Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= RecNum;
      If GotoKey then
        Delete;
      If Assigned(Changes) then
        Changes.FileDelete(ocCompany, Parent.RecNum, acNewHon, RecNum);
      If FileItem<>nil then
        FileItem.DeleteRecord(ocCompany, Parent.RecNum, acNewHon, RecNum);
    end;
  finally
    if Changes <>nil then
      Changes.Free;
  end;
end;

procedure THonItem.ExSaveData(FileItem: TFileItem; Parent: TBasicClass);
begin
  SaveData(Parent, False, FileItem.FileInfo.Draft, FileItem.FileInfo.RecNum);
  if (Active) then
    FileItem.SaveChange(ocCompany, (Parent as TCompany).RecNum, acNewHon, RecNum)
  else
    FileItem.SaveChange(ocHon, RecNum, acDelete, RecNum);
end;

procedure THonItem.SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving hon item data (' + Self.Name + ')');

  if not Draft then // "and (not draft) added for fmHamara saving procedure
  begin
    Changes:= TChanges.Create((Parent as TCompany).Parent);
    try
      if not Active then
        Changes.SaveChanges(ocHon, RecNum, acDelete, RecNum, LastChanged, FileNum);
    finally
      Changes.Free;
    end;
    if not Active then
      Exit;
  end;
  with DATA.taHon do
  begin
    Append;
    FieldByName('Name').AsString     := Self.Name;
    FieldByName('Value').AsFloat     := Value;
    FieldByName('Rashum').AsFloat    := Rashum;
    FieldByName('Mokza').AsFloat     := Mokza;
    FieldByName('Cash').AsFloat      := Cash;
    FieldByName('NonCash').AsFloat   := NonCash;
    FieldByName('Part').AsFloat      := Part;
    FieldByName('PartValue').AsFloat := PartValue;
    Post;
    FRecNum := FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr( FRecNum ));
    if not Draft then
    begin
      Changes := TChanges.Create(Parent as TCompany);
      try
        Changes.SaveChanges(ocCompany, (Parent as TCompany).RecNum, acNewHon, RecNum, FLastChanged, FileNum);
      finally
        Changes.Free;
      end;
    end;
  end;
end; // THonItem.SaveData

constructor THonItem.LoadFromFile(Parent: TCompany; RecNum: Integer);
begin
  Create(RecNum, Parent, EncodeDate(1999, 01, 01));
end;

constructor THonItem.Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);
var
  Changes: TChanges;
begin
  Inherited Create(Parent, LastChanged);
  Changes := TChanges.Create(Parent);
  try
    With DATA.taHon Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= RecNum;
      If not GotoKey then
        raise Exception.Create('Record not found at THonItem.Create');
      FName      := FieldByName('Name').AsString;
      FValue     := FieldByName('Value').AsFloat;
      FRashum    := FieldByName('Rashum').AsFloat;
      FMokza     := FieldByName('Mokza').AsFloat;
      FCash      := FieldByName('Cash').AsFloat;
      FNonCash   := FieldByName('NonCash').AsFloat;
      FPart      := FieldByName('Part').AsFloat;
      FPartValue := FieldByName('PartValue').AsFloat;
      FActive:= (Changes.GetLastChange(ocHon, RecNum, acDelete) = -1);
      FRecNum:= RecNum;
    end;
  finally
    Changes.Free;
  end;
end;

// TCompanyInfo
//----------------------------------------------
constructor TCompanyInfo.CreateNew(LastChanged: TDateTime);
begin
  inherited CreateNew(LastChanged);

  FOption1                    := 0;
  FOption3                    := #0;
  FOtherInfo                  := '';
  FSigName                    := '';
  FSigZeut                    := '';
  FSigTafkid                  := '';
  FLawName                    := '';
  FLawAddress                 := '';
  FLawZeut                    := '';
  FLawLicence                 := '';
  FTakanon2                   := DefaultTakanon2;
  FTakanon2_1                 := DefaultTakanon2;
  FTakanon2_2                 := '';
  FTakanon2_3                 := '';
  FTakanon2_4                 := '';
  FTakanon4                   := DefaultTakanon4;
  FTakanon5                   := DefaultTakanon5;
  FTakanon7                   := DefaultTakanon7;
  FEnglishName                := '';
  FRegistrationDate           := 0;

  // Moshe 31/10/2016 Task 12538
  FCompanyName2               := '';
  FCompanyName3               := '';
  FMail                       := '';
  FSharesRestrictIndex        := -1;
  FDontOfferIndex             := -1;
  FShareHoldersNoIndex        := -1;
  FSharesRestrictChapters     := '';
  FDontOfferChapters          := '';
  FShareHoldersNoChapters     := '';
  FLimitedSignatoryIndex      := -1;
  FLegalSectionsChapters      := '';
  FSignatorySectionsChapters  := '';
  FConfirmedCopies            := '';
  FMoreCopies                 := '';
  FRegulationItemsIndex       := -1;
end; // TCompanyInfo.CreateNew

procedure TCompanyInfo.DeleteEnum(ObjectNum, NewNum: Integer);
begin
  With DATA.taSigName do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= NewNum;
    If GotoKey Then
      Delete;
  end;
  FFileItem.DeleteRecord(ocCompany, ObjectNum, acSignName, NewNum);
end;

procedure TCompanyInfo.FileDelete(FileItem: TFileItem);
var
  Changes: TChanges;
begin
  FFileItem := FileItem;
  If (FileItem<>nil) and (FileItem.FFileInfo.Draft) then
  begin
    FileItem.EnumChanges(ocCompany, Parent.RecNum, acSignName, DeleteEnum);
  end
  else
  begin
    Changes := TChanges.Create(Parent);
    try
      With DATA.taSigName Do
      begin
        EditKey;
        FRecNum := Changes.GetLastChange(ocCompany, Parent.RecNum, acSignName);
        FieldByName('RecNum').AsInteger:= RecNum;
        If GotoKey then
          Delete;
        Changes.FileDelete(ocCompany, Parent.RecNum, acSignName, RecNum);
        If FileItem<>nil then
          FFileItem.DeleteRecord(ocCompany, Parent.RecNum, acSignName, RecNum);
      end;
    finally
      Changes.Free;
    end;
  end;
end;

procedure TCompanyInfo.ExSaveData(FileItem: TFileItem; Parent: TCompany);
begin
  SaveData(Parent, FileItem.FFileInfo.Draft, FileItem.FFileInfo.RecNum);
  FileItem.SaveChange(ocCompany, Parent.RecNum, acSignName, RecNum);
end;

procedure TCompanyInfo.SetRegistrationDate(Value: TDateTime; Changes: TChanges);
begin
  WriteRegistrationDate(Value);
  Changes.ChangeRegistrationDate(ocCompany, Parent.RecNum, acSignName, RecNum, Value);
end;

procedure TCompanyInfo.SetEnglishName( EngName: string; Parent: TCompany; DecisionDate: TDateTime);
var
  FileItem : TFileItem;
  FilesList: TFilesList;
  FileInfo: TFileInfo;
begin
  Self.FEnglishName := EngName;
  Self.FLastChanged := DecisionDate;
  FilesList := TFilesList.Create(Parent.RecNum, faEngName, False);
  FileInfo := TFileInfo.Create;
  FileInfo.Action:= faEngName;
  FileInfo.Draft := False;
  FileInfo.FileName := EngName;
  FileInfo.DecisionDate := DecisionDate;
  FileInfo.notifyDate := Now;
  FileInfo.CompanyNum := Parent.RecNum;
  FilesList.Add(FileInfo);
  FileItem := TFileItem.Create(FilesList, FileInfo);
  try
    Self.ExSaveData(FileItem,Parent);
  finally
    FilesList.Free;
    FileItem.Free;
  end;
end;

procedure TCompanyInfo.WriteRegistrationDate(Value: TDateTime);
begin
  FRegistrationDate := Value;
  with Data.taSigName do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger := RecNum;
    if not GotoKey then
      raise Exception.Create('Record not found at TCompanyInfo.WriteRegistrationDate');
    Edit;
    FieldByName('ResInt1').AsInteger := Trunc(Value);
    Post;
  end;
end;

procedure TCompanyInfo.SaveData(aParent: TCompany; aDraft: Boolean; aFileNum: Integer);
var
  Changes: TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving company info data (' + aParent.Name + ')');  //sts24327
  { <-- }
  with DATA.taSigName do
  begin
    Append;
    FieldByName('SigName').AsString                   := FSigName;
    FieldByName('SigZeut').AsString                   := FSigZeut;
    FieldByName('SigTafkid').AsString                 := FSigTafkid;
    FieldByName('LawName').AsString                   := FLawName;
    FieldByName('LawAddress').AsString                := FLawAddress;
    FieldByName('LawZeut').AsString                   := FLawZeut;
    FieldByName('LawLicence').AsString                := FLawLicence;
    if FieldByName('Option1').DataSize <> 1 then
      raise Exception.Create('Field size error at TCompanyInfo.SaveData');
    FieldByName('Option1').SetData(@FOption1);
    FieldByName('Option3').AsString                   := FOption3;
    FieldByName('OtherInfo').AsString                 := FOtherInfo;
    FieldByName('ResInt1').AsInteger                  := Trunc(FRegistrationDate);
    FieldByName('Takanon2_1').AsString                := Takanon2_1;
    FieldByName('Takanon2_2').AsString                := Takanon2_2;
    FieldByName('Takanon2_3').AsString                := Takanon2_3;
    FieldByName('Takanon2_4').AsString                := Takanon2_4;
    FieldByName('Takanon4').AsString                  := Takanon4;
    FieldByName('Takanon5').AsString                  := Takanon5;
    FieldByName('Takanon7').AsString                  := Takanon7;
    FieldByName('EngName').AsString                   := EnglishName;
    FieldByName('CompanyName2').AsString              := CompanyName2;
    FieldByName('CompanyName3').AsString              := CompanyName3;
    FieldByName('Mail').AsString                      := Mail;
    FieldByName('SharesRestrictIndex').AsInteger      := SharesRestrictIndex;
    FieldByName('DontOfferIndex').AsInteger           := DontOfferIndex;
    FieldByName('ShareHoldersNoIndex').AsInteger      := ShareHoldersNoIndex;
    FieldByName('SharesRestrictChapters').AsString    := SharesRestrictChapters;
    FieldByName('DontOfferChapters').AsString         := DontOfferChapters;
    FieldByName('ShareHoldersNoChapters').AsString    := ShareHoldersNoChapters;
    FieldByName('LimitedSignatoryIndex').AsInteger    := LimitedSignatoryIndex;
    FieldByName('LegalSectionsChapters').AsString     := LegalSectionsChapters;
    FieldByName('SignatorySectionsChapters').AsString := SignatorySectionsChapters;
    FieldByName('ConfirmedCopies').AsString           := ConfirmedCopies;
    FieldByName('MoreCopies').AsString                := MoreCopies;
    FieldByName('RegulationItemsIndex').AsInteger     := RegulationItemsIndex;
    Post;
    FRecNum := FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr(FRecNum));
  end;

  // Moshe 29/11/2018 - always save to Forms & Changes
  { -- 770 --
  Data.taForms.Append;
  Data.taForms.FieldByName('FileNum').AsInteger     := FileNum;
  Data.taForms.FieldByName('ObjectType').AsString   := ObjToChar(ocCompany);
  Data.taForms.FieldByName('ObjectNum').AsInteger   := Parent.RecNum;
  Data.taForms.FieldByName('Action').AsString       := ActionToChar(acSignName);
  Data.taForms.FieldByName('NewNum').AsInteger      := FRecNum;
  Data.taForms.Post;
  -- 770 -- }

  Changes := TChanges.Create(aParent);
  try
    Changes.SaveChanges(ocCompany, aParent.RecNum, acSignName, RecNum, FLastChanged, aFileNum); //sts24327
  finally
    Changes.Free;
  end;
end; // TCompanyInfo.SaveData

function TCompanyInfo.UpdateData(Parent: TCompany; RecNum: integer): boolean;
begin
  Result  := False;
  if (RecNum < 1) then
    Exit;
  if (DATA.taSigName.FieldByName('Option1').DataSize <> 1) then
    Exit;

  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Update company info data (' + Parent.Name + ')');

  try
  with DATA.taSigName do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger  := RecNum;
    if (not GotoKey) then
      Exit;
    Edit;
    FieldByName('SigName').AsString                  := FSigName;
    FieldByName('SigZeut').AsString                  := FSigZeut;
    FieldByName('SigTafkid').AsString                := FSigTafkid;
    FieldByName('LawName').AsString                  := FLawName;
    FieldByName('LawAddress').AsString               := FLawAddress;
    FieldByName('LawZeut').AsString                  := FLawZeut;
    FieldByName('LawLicence').AsString               := FLawLicence;
    FieldByName('Option1').SetData(@FOption1);
    FieldByName('Option3').AsString                    := FOption3;
    FieldByName('OtherInfo').AsString                  := FOtherInfo;
    FieldByName('ResInt1').AsInteger                   := Trunc(FRegistrationDate);
    FieldByName('Takanon2_1').AsString                 := Takanon2_1;
    FieldByName('Takanon2_2').AsString                 := Takanon2_2;
    FieldByName('Takanon2_3').AsString                 := Takanon2_3;
    FieldByName('Takanon2_4').AsString                 := Takanon2_4;
    FieldByName('Takanon4').AsString                   := Takanon4;
    FieldByName('Takanon5').AsString                   := Takanon5;
    FieldByName('Takanon7').AsString                   := Takanon7;
    FieldByName('EngName').AsString                    := EnglishName;
    FieldByName('CompanyName2').AsString               := CompanyName2;
    FieldByName('CompanyName3').AsString               := CompanyName3;
    FieldByName('Mail').AsString                       := Mail;
    FieldByName('SharesRestrictIndex').AsInteger       := SharesRestrictIndex;
    FieldByName('DontOfferIndex').AsInteger            := DontOfferIndex;
    FieldByName('ShareHoldersNoIndex').AsInteger       := ShareHoldersNoIndex;
    FieldByName('SharesRestrictChapters').AsString     := SharesRestrictChapters;
    FieldByName('DontOfferChapters').AsString          := DontOfferChapters;
    FieldByName('ShareHoldersNoChapters').AsString     := ShareHoldersNoChapters;
    FieldByName('LimitedSignatoryIndex').AsInteger     := LimitedSignatoryIndex;
    FieldByName('LegalSectionsChapters').AsString      := LegalSectionsChapters;
    FieldByName('SignatorySectionsChapters').AsString  := SignatorySectionsChapters;
    FieldByName('ConfirmedCopies').AsString            := ConfirmedCopies;
    FieldByName('MoreCopies').AsString                 := MoreCopies;
    FieldByName('RegulationItemsIndex').AsInteger      := RegulationItemsIndex;
    Post;
    FRecNum  := RecNum;
    Result  := True;
  end;
  except
  end;
end; // TCompanyInfo.UpdateData

constructor TCompanyInfo.LoadFromFile(Parent: TCompany; FileItem: TFileItem);
begin
  FParent:= Parent;
  If FileItem.EnumChanges(ocCompany, Parent.RecNum, acSignName, LoadEnum) < 1 Then
  //If FileItem.EnumChanges(ocCompany, fileItem.FParent.FCompany, acNewMagish, LoadEnum) < 1 Then //sts23822
    CreateNew(Now);
  FParent:= Parent;
end;

procedure TCompanyInfo.LoadEnum(ObjectNum, NewNum: Integer);
begin
  Create(NewNum, Parent);
end;

constructor TCompanyInfo.Create(RecNum: Integer; Parent: TCompany);
begin
  inherited Create(Parent);

  Data.taSigName.EditKey;
  Data.taSigName.FieldByName('RecNum').AsInteger := RecNum;
  if (not Data.taSigName.GotoKey) then
    raise Exception.Create('Record not found at TCompanyInfo.Create');
  FSigName                := Data.taSigName.FieldByName('SigName').AsString;
  FSigZeut                := Data.taSigName.FieldByName('SigZeut').AsString;
  FSigTafkid              := Data.taSigName.FieldByName('SigTafkid').AsString;
  FLawName                := Data.taSigName.FieldByName('LawName').AsString;
  FLawAddress             := Data.taSigName.FieldByName('LawAddress').AsString;
  FLawZeut                := Data.taSigName.FieldByName('LawZeut').AsString;
  FLawLicence             := Data.taSigName.FieldByName('LawLicence').AsString;
  if Data.taSigName.FieldByName('Option1').DataSize <> 1 then
    raise Exception.Create('Field size error at TCompanyInfo.Create');
  Data.taSigName.FieldByName('Option1').GetData(@FOption1);
  if Data.taSigName.FieldByName('Option3').AsString = '' then                             //sts22596
    raise Exception.Create('Field Option3 cant be empty string at TCompanyInfo.Create');
  FOption3                  := Data.taSigName.FieldByName('Option3').AsString[1];
  FOtherInfo                := Data.taSigName.FieldByName('OtherInfo').AsString;
  FTakanon2                 := Data.taSigName.FieldByName('Takanon2').AsString;
  if (FTakanon2 = '') then
    FTakanon2               := DefaultTakanon2;
  FTakanon2_1               := Data.taSigName.FieldByName('Takanon2_1').AsString;
  if (FTakanon2_1 = '') then
    FTakanon2_1             := DefaultTakanon2;
  FTakanon2_2               := Data.taSigName.FieldByName('Takanon2_2').AsString;
  FTakanon2_3               := Data.taSigName.FieldByName('Takanon2_3').AsString;
  FTakanon2_4               := Data.taSigName.FieldByName('Takanon2_4').AsString;
  FTakanon4                 := Data.taSigName.FieldByName('Takanon4').AsString;
  if FTakanon4 = '' then
    FTakanon4               := DefaultTakanon4;
  FTakanon5                 := Data.taSigName.FieldByName('Takanon5').AsString;
  if FTakanon5 = '' then
    FTakanon5               := DefaultTakanon5;
  FTakanon7                 := Data.taSigName.FieldByName('Takanon7').AsString;
  if FTakanon7 = '' then
    FTakanon7               := DefaultTakanon7;
  FEnglishName              := Data.taSigName.FieldByName('EngName').AsString;
  FRegistrationDate         := Data.taSigName.FieldByName('ResInt1').AsInteger;
  FCompanyName2             := Data.taSigName.FieldByName('CompanyName2').AsString;
  FCompanyName3             := Data.taSigName.FieldByName('CompanyName3').AsString;
  FMail                     := Data.taSigName.FieldByName('Mail').AsString;
  FSharesRestrictIndex      := Data.taSigName.FieldByName('SharesRestrictIndex').AsInteger;
  FDontOfferIndex           := Data.taSigName.FieldByName('DontOfferIndex').AsInteger;
  FShareHoldersNoIndex      := Data.taSigName.FieldByName('ShareHoldersNoIndex').AsInteger;
  FSharesRestrictChapters   := Data.taSigName.FieldByName('SharesRestrictChapters').AsString;
  FDontOfferChapters        := Data.taSigName.FieldByName('DontOfferChapters').AsString;
  FShareHoldersNoChapters   := Data.taSigName.FieldByName('ShareHoldersNoChapters').AsString;
  FLimitedSignatoryIndex    := Data.taSigName.FieldByName('LimitedSignatoryIndex').AsInteger;
  FLegalSectionsChapters    := Data.taSigName.FieldByName('LegalSectionsChapters').AsString;
  FSignatorySectionsChapters:= Data.taSigName.FieldByName('SignatorySectionsChapters').AsString;
  FConfirmedCopies          := Data.taSigName.FieldByName('ConfirmedCopies').AsString;
  FMoreCopies               := Data.taSigName.FieldByName('MoreCopies').AsString;
  FRegulationItemsIndex     := Data.taSigName.FieldByName('RegulationItemsIndex').AsInteger;

  FRecNum:= RecNum;
  if FRegistrationDate = 0 then          // For old version
    FRegistrationDate := EncodeDate(1900, 01, 01);
end;

procedure TCompanyInfo.SetTakanon2(Value: String);
begin
  FTakanon2 := Value;
  if (FTakanon2 = '') then
    FTakanon2 := DefaultTakanon2;
end;

procedure TCompanyInfo.SetTakanon2_1(Value: String);
begin
  FTakanon2_1 := Value;
  if (FTakanon2_1 = '') then
    FTakanon2_1 := DefaultTakanon2;
end;

procedure TCompanyInfo.SetTakanon4(Value: String);
begin
  FTakanon4 := Value;
  if (FTakanon4 = '') then
    FTakanon4 := DefaultTakanon4;
end;

procedure TCompanyInfo.SetTakanon5(Value: String);
begin
  FTakanon5 := Value;
  if (FTakanon5 = '') then
    FTakanon5 := DefaultTakanon5;
end;

procedure TCompanyInfo.SetTakanon7(Value: String);
begin
  FTakanon7 := Value;
  if (FTakanon7 = '') then
    FTakanon7 := DefaultTakanon7;
end;

class procedure TCompanyInfo.UndoSignName(RecNum: integer);
begin
  with Data.taSigName do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    if GoToKey then
      Delete;
  end;
end;

// TManager
//----------------------------------------------
procedure TManager.SetRegistrationDate(Value: TDateTime; Changes: TChanges);
begin
  Changes.ChangeRegistrationDate(ocCompany, Parent.RecNum, acNewManager, RecNum, Value);
end;

class procedure TManager.Undo(RecNum: Integer);
begin
  With Data.taManager do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger := RecNum;
    If GoToKey Then
      Delete;
  end;
end;

//============Added by Tsahi 27.06.05
class procedure TManager.UndoManagerChange(RecNum: Integer);
begin
  With Data.taManager do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If not GoToKey Then
      raise Exception.Create('Record not found at TManager.UndoManagerChange');
    Edit;
    FieldByName('ResInt1').asInteger:=0;
    Post;
  end;
end;

//============
constructor TManager.CreateNew(LastChanged: TDateTime; Const Name, Zeut, Address, Phone, Mail: String);
begin
  Inherited CreateNew(LastChanged, Name, Zeut);
  FAddress := Address;
  FPhone := Phone;
  FActive := True;
  FMail := Mail;
end;

procedure TManager.FileDelete(FileItem: TFileItem);
var
  Changes: TChanges;
begin
  If (FileItem=nil) Or (not FileItem.FFileInfo.Draft) then
    Changes:= TChanges.Create(Parent)
  Else
    Changes:= Nil;
  try
    With DATA.taManager Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= RecNum;
      If GotoKey then
        Delete;
      If Assigned(Changes) then
        Changes.FileDelete(ocCompany, Parent.RecNum, acNewManager, RecNum);
      If FileItem<>nil then
        FileItem.DeleteRecord(ocCompany, Parent.RecNum, acNewManager, RecNum);
    end;
  finally
    if Changes<>nil then
      Changes.Free;
  end;
end;

procedure TManager.ExSaveData(FileItem: TFileItem; Parent: TBasicClass);
begin
  SaveData(Parent, False, FileItem.FFileInfo.Draft, FileItem.FFileInfo.RecNum);
  If Active Then
    FileItem.SaveChange(ocCompany, (Parent as TCompany).RecNum, acNewManager, RecNum)
  Else
    FileItem.SaveChange(ocManager, -1, acDelete, RecNum);
end;

procedure TManager.SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving manager data (' + Self.Name + ')');
  { <-- }
  If (not Active) And (not Draft) Then
    begin
      //==> Tsahi 30.10.05: ResInt1 should be -1 when manager is deleted (otherwise the Rasham sill loads it)
      With DATA.taManager Do
      begin
        SetRange([RecNum],[RecNum]);
        First;
        Edit;
        FieldByName('ResInt1').AsInteger := -1;
        CancelRange;
      end;
      //<== }
      Changes:= TChanges.Create((Parent as TCompany).Parent);
      try
        Changes.SaveChanges(ocManager, RecNum, acDelete, RecNum, LastChanged, FileNum);
      finally
        Changes.Free;
      end;
      Exit;
    end;

  With DATA.taManager Do
  begin
    Append;
    FieldByName('Name').AsString    := Self.Name;
    FieldByName('Zeut').AsString    := Zeut;
    FieldByName('Address').AsString := Address;
    FieldByName('Phone').AsString   := Phone;
    FieldByName('Mail').AsString    := Mail;
    Post;
    FRecNum:= FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog( #9#9#9#9 + 'RecNum after save: ' + IntToStr( FRecNum ));
    { <-- }
    If not Draft Then
    begin
      Changes:= TChanges.Create(Parent as TCompany);
      try
        Changes.SaveChanges(ocCompany, (Parent as TCompany).RecNum, acNewManager, RecNum, {Trunc(Now){}FLastChanged, FileNum);
      finally
        Changes.Free;
      end;
    end;
  end;
end;

constructor TManager.LoadFromFile(Parent: TCompany; RecNum: Integer);
begin
  Create(RecNum, Parent, EncodeDate(1999,01,01));
end;

constructor TManager.Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);
var
  Changes: TChanges;
begin
  Inherited Create(Parent, LastChanged);
  Changes := TChanges.Create(Parent);
  try
    With DATA.taManager Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= RecNum;
      If not GotoKey then
        raise Exception.Create('Record not found at TManager.Create');
      FName    := FieldByName('Name').AsString;
      FZeut    := FieldByName('Zeut').AsString;
      FAddress := FieldByName('Address').AsString;
      FPhone   := FieldByName('Phone').AsString;
      FMail    := FieldByName('Mail').AsString;
      FActive  := Changes.GetLastChange(ocManager, RecNum, acDelete) = -1;
      If not Active Then
        FLastChanged:= TempDate;
      FRecNum  := RecNum;
    end;
  finally
    Changes.Free;
  end;
end;

// TDirector
//----------------------------------------------
procedure TDirector.SetRegistrationDate(Value: TDateTime; Changes: TChanges);
begin
  Changes.ChangeRegistrationDate(ocDirector, RecNum, acAddress, -1, Value);
  if Taagid <> nil then
    Changes.ChangeRegistrationDate(ocDirector, RecNum, acTaagidAddress, -1, Value);
  Changes.ChangeRegistrationDate(ocCompany, Parent.RecNum, acNewDirector, RecNum, Value);
end;

class procedure TDirector.Undo(RecNum: Integer);
var
  Changes: TChanges;
begin
  Changes:= TChanges.Create(Nil);
  try
    TDirectorAddress.Undo(Changes.GetLastChange(ocDirector, RecNum, acAddress));
    TTaagid.Undo(Changes.GetLastChange(ocDirector,RecNum, acTaagidAddress));
  finally
    Changes.Free;
  end;
  With Data.taDirectors do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If GoToKey Then
      Delete;
  end;
end;

constructor TDirector.CreateNew(LastChanged: TDateTime; Const Name, Zeut: String; Date1, Date2: TDateTime; Address: TDirectorAddress; Taagid: TTaagid; Active: Boolean);
begin
  Inherited CreateNew(LastChanged, Name, Zeut);
  FDate1:= Date1;
  FDate2:= Date2;
  FTaagid:= Taagid;
  FAddress:= Address;
  FActive:= Active;
end;

destructor TDirector.Destroy;
begin
  If Assigned(Taagid) Then
    Taagid.Free;
  If Assigned(Address) Then
    Address.Free;
  Inherited;
end;

procedure TDirector.FileDelete(FileItem: TFileItem);
var
  Changes: TChanges;
begin
  If Taagid <> Nil Then
    Taagid.FileDelete(Self, FileItem);
  Address.FileDelete(Self, FileItem);
  If (FileItem=nil) Or (not FileItem.FFileInfo.Draft) then
    Changes:= TChanges.Create(Parent)
  Else
    Changes:= Nil;
  try
    With DATA.taDirectors Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= RecNum;
      If GotoKey then
        Delete;
      If Assigned(Changes) then
        Changes.FileDelete(ocCompany, Parent.RecNum, acNewDirector, RecNum);
      If FileItem<>nil then
        FileItem.DeleteRecord(ocCompany, Parent.RecNum, acNewDirector, RecNum);
    end;
  finally
    if Changes<>nil then
      Changes.Free;
  end;
end;

procedure TDirector.ExSaveData(FileItem: TFileItem; Parent: TBasicClass);
begin
  SaveData(Parent, False, FileItem.FFileInfo.Draft, FileItem.FFileInfo.RecNum);
  If Active Then
  begin
    FileItem.SaveChange(ocCompany, (Parent as TCompany).RecNum, acNewDirector, RecNum);
    If Assigned(Taagid) Then
      Taagid.ExSaveData(FileItem, Self, LastChanged);
    Address.ExSaveData(FileItem ,Self, LastChanged);
  end
  Else
    FileItem.SaveChange(ocDirector, -1, acDelete, RecNum);
end;

procedure TDirector.SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer);
var
  Changes: TChanges;
  TempRec: string;
  NewZeut: string;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving direcor data (' + Self.Name + ', ' + Zeut + ')');
  { <-- }
  If (not Active) And (not Draft) Then
  begin
    Changes:= TChanges.Create((Parent as TCompany).Parent);
    try
      Changes.SaveChanges(ocDirector, RecNum, acDelete, RecNum, LastChanged, FileNum);
    finally
      Changes.Free;
    end;
    Exit;
  end;

  NewZeut := '';

  With DATA.taDirectors Do
  begin
    CancelRange;
    Filtered:= False;
    Filter:= '(Zeut = '''+Zeut+''')' ;
    Filtered:= True;
    if RecordCount=0 then
    begin
      Data.taForms.Filtered:=False;
      Data.taForms.CancelRange;
      Data.taForms.Filter:= '(ResStr1 = '''+Zeut+''') and (Action = '''+ActionToChar(acNewZeut)+ ''')' ;
      Data.taForms.Filtered:= True;
      if RecordCount<>0 then
      begin
         Data.taForms.Last;
         TempRec:= Data.taForms.FieldByName('RecNum').AsString;
         Data.taChanges.Filtered := False;
         Data.taChanges.CancelRange;
         Data.taChanges.Filter := '(NewNum = '+ TempRec+' ) and (Action = '''+ActionToChar(acNewZeut)+ ''')' ;
         Data.taChanges.Filtered := True;
         TempRec :=Data.taChanges.FieldByName('ObjectNum').AsString;
         Data.taChanges.Filtered := False;
         DATA.taDirectors.Filtered := False;
         DATA.taDirectors.Filter := '(RecNum = ' + TempRec +')';
         DATA.taDirectors.Filtered := True;
         TempRec :=Data.taDirectors.FieldByName('Zeut').AsString;
         DATA.taDirectors.Filtered := False;
         NewZeut:=TempRec;
      end;
      Data.taForms.Filtered:= False;
    end;
    Filtered:= True;

    Append;
    FieldByName('Name').AsString:= Self.Name;
    FieldByName('Zeut').AsString:= Zeut;
    FieldByName('Date1').AsDateTime:= Date1;
    FieldByName('Date2').AsDateTime:= Date2;
    If Assigned(Taagid) Then
      FieldByName('Taagid').AsString:= '1'
    Else
      FieldByName('Taagid').AsString:= '0';
    if NewZeut<>'' then
      FieldByName('ResStr1').AsString:=NewZeut;
    Post;
    FRecNum:= FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr(FRecNum));
    { <-- }

    If not Draft Then
    begin
      Changes:= TChanges.Create(Parent as TCompany);
      try
        Changes.SaveChanges(ocCompany, (Parent as TCompany).RecNum, acNewDirector, RecNum, FLastChanged, FileNum);
      finally
        Changes.Free;
        Filtered:= False;
      end;
    end;
    If SaveChild Then
    begin
      If Assigned(Taagid) Then
        Taagid.SaveData(Self, LastChanged, Draft, FileNum);
      Address.SaveData(Self, LastChanged, Draft, FileNum);
    end;
    Filtered:= False;
  end;
end;

//-------------------------------------------------------------
procedure TDirector.SaveDataToAll(SelectedDirectors: TMultipleSelectRec;
          NewAddress: TDirectorAddress; NewName, NewZeut: string;
          ChangeName, ChangeZeut,LastChange, FirstObject: boolean; FileInfo: TFileInfo);
var
  NewFileInfo : TFileInfo;
  CurrObj     : TObjectClass;
  CompanyNum  : integer;
  RecNum      : integer;
begin
  CompanyNum := SelectedDirectors.CompanyNum;

  if (SelectedDirectors.PersonType = diDirector)  or (SelectedDirectors.PersonType = diBoth) then
  begin
    CurrObj := ocDirector;
    RecNum  := SelectedDirectors.DirectorNum;
    with Data.taChanges do
    begin
      Filter := '([ObjectNum] = ' + intToStr(RecNum) + ') AND ([ObjectType] = ' + ObjToChar(CurrObj) + ')';
      Filtered := True;
      SaveSpecifiedData(RecNum,FieldByName('ResInt1').AsInteger,CompanyNum, FileInfo,
                        NewAddress, NewName, NewZeut, ChangeName, ChangeZeut, CurrObj ,NewFileInfo);
    end;
  end;

  if (SelectedDirectors.PersonType = diStockHolder)  or (SelectedDirectors.PersonType = diBoth) and (SelectedDirectors.StockHolderNum >0) then
  begin
    CurrObj := ocStockHolder;
    RecNum  := SelectedDirectors.StockHolderNum;
    With Data.taChanges do
    begin
      Filter := '([ObjectNum] = ' + intToStr(RecNum) + ') AND ([ObjectType] = ' + ObjToChar(CurrObj) + ')';
      Filtered := True;
      Last;
      SaveSpecifiedData(RecNum,FieldByName('ResInt1').AsInteger,CompanyNum, FileInfo,
                        NewAddress, NewName, NewZeut, ChangeName, ChangeZeut, CurrObj ,NewFileInfo);
    end;
  end;

  //==>  28.6.05 Tsahi
  if (SelectedDirectors.ManagerNum <> 0) then
    ChangeManager(CompanyNum ,NewZeut, NewName, NewAddress,FileInfo, NewFileInfo);
  if (SelectedDirectors.SignNameNum <> 0) then
    ChangeSigName(CompanyNum, NewZeut, NewName, FileInfo);

  Data.taChanges.Filtered := False;

  if LastChange and ChangeZeut then
    FZeut := NewZeut;
end; // TDirector.SaveDataToAll

procedure TDirector.SaveSpecifiedData(DirRecNum, FileNum, CompanyNum: integer;  FileInfo: TFileInfo; NewAddress: TDirectorAddress;
          NewName, NewZeut: string; ChangeName, ChangeZeut: boolean; ObjectType:TObjectClass; var NewFileInfo:TFileInfo);
var
  Changes: TChanges;
  Company: TCompany;
  S1, S2: string;
  AdTable: TTable;
  FormRecNum, NewDirAd: integer;
  FilesList: TFilesList;
  FileInfoAction: TFileAction;
  NewFileNum: integer;
  Director: TDirector;
begin
  Data.taChanges.Filtered := False;
  NewFileInfo := TFileInfo.Create;
  NewFileInfo.Assign(FileInfo);
  FileInfoAction := faNone;
  AdTable := Data.taDirAddress;
  if ObjectType = ocDirector then
  begin
    AdTable := Data.taDirAddress;
    DATA.taDirAddress.Filtered := False;
    FileInfoAction := faDirectorInfo;
  end
  else if ObjectType = ocStockHolder then
  begin
    AdTable := Data.taStockAddress;
    Data.taStockHolders.Filtered := False;
    FileInfoAction := faStockHolderInfo;
  end;

 if (ObjectType = ocDirector) and (DirRecNum = RecNum) then
   Exit;
 if CompanyNum = -1 then
   Exit;

  if FileNum <> -1 then
  begin
    try
    Data.qGeneric2.SQL.Text :=
      'SELECT ResInt1 ' +
      'FROM ' + Data.taChanges.TableName + ' ' +
      'WHERE ObjectNum = ' + IntTostr(DirRecNum) + ' AND ObjectType = ''' + ObjToChar(ObjectType) + '''';
    Data.qGeneric2.Open;
    while (not Data.qGeneric2.EOF) do
    begin
      if (Data.qGeneric2.FieldByName('ResInt1').AsInteger = FileNum) then
        Break;
      Data.qGeneric2.Next;
    end;
    if (Data.qGeneric2.EOF) then
      Exit;
    finally
      Data.qGeneric2.Close;
    end;
  end;

  Company := TCompany.Create(CompanyNum, Now, False);
  FilesList := TFilesList.Create(CompanyNum, FileInfoAction, False);
  NewFileInfo.CompanyNum := CompanyNum;
  NewFileInfo.Action := FileInfoAction;
  FilesList.Add(NewFileInfo);
  NewFileNum := NewFileInfo.RecNum;

  // DirAddreess
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving director address (' + Parent.Name + ')');

  With AdTable Do
  begin
    Append;
    if ObjectType = ocDirector then
      FieldByName('Phone').AsString := NewAddress.Phone;
    FieldByName('Street').AsString := NewAddress.Street;
    FieldByName('Number').AsString := NewAddress.Number;
    FieldByName('Index').AsString := NewAddress.Index;
    FieldByName('City').AsString := NewAddress.City;
    FieldByName('Country').AsString := NewAddress.Country;
    Post;
    NewDirAd := FieldByName('RecNum').AsInteger;
    { Added on 07/03/2005 by ES - new log --> }
    WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr(FRecNum));

    Changes := TChanges.Create(Company);
    try
      Changes.SaveChanges(ObjectType, DirRecNum, acAddress, NewDirAd, NewFileInfo.DecisionDate, NewFileNum);
    finally
      Changes.Free;
    end;
  end;

  // ChangeName
  if ChangeName then
  begin
    with Data.taForms do
    begin
      WriteChangeLog(#9#9#9#9'ChangeName in Forms: FileNum: ' + IntToStr(NewFileNum) +
        ', ObjectType: ' + ObjectToName(ObjectType) + ', ObjectNum: -1, Action: ' + ActionToName(acNewName) +
        ', NewNum: -1, ResStr1 + ResStr2: ' + NewName);
      Append;
      FieldByName('FileNum').AsInteger := FileNum;
      FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
      FieldByName('ObjectNum').AsInteger := -1;
      FieldByName('Action').AsString := ActionToChar(acNewName);
      FieldByName('NewNum').AsInteger := -1;
      S1 := Copy(NewName, 1, 20);
      S2 := Copy(NewName, 21, 80);
      FieldByName('ResStr1').AsString := S1;
      FieldByName('ResStr2').AsString := S2;
      Post;
      FormRecNum := FieldByName('RecNum').AsInteger;
      WriteChangeLog(#9#9#9#9'Saved: RecNum: ' + IntToStr(FormRecNum));

      if (Trim(NewName)<> '') and (NewName <> Self.Name) then
      begin
        Changes := TChanges.Create(Company);
        try
          Changes.SaveChanges(ObjectType, DirRecNum, acNewName, FormRecNum, NewFileInfo.DecisionDate, NewFileNum);
        finally
          Changes.Free;
        end;
      end;
    end;
  end;

  // ChangeZeut
  if ChangeZeut then
  begin
    with Data.taForms Do
    begin
      WriteChangeLog(#9#9#9#9'ChangeZeut in Forms: FileNum: ' + IntToStr(NewFileNum) +
        ', ObjectType: ' + ObjectToName(ObjectType) + ', ObjectNum: -1, Action: ' + ActionToName(acNewZeut) +
        ', NewNum: -1, ResStr1: ' + NewZeut);
      Append;
      FieldByName('FileNum').AsInteger := NewFileNum;
      FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
      FieldByName('ObjectNum').AsInteger := -1;
      FieldByName('Action').AsString := ActionToChar(acNewZeut);
      FieldByName('NewNum').AsInteger := -1;
      S1 := Copy(NewZeut, 1, 20);
      FieldByName('ResStr1').AsString := S1;
      Post;
      FormRecNum:= FieldByName('RecNum').AsInteger;
      WriteChangeLog(#9#9#9#9'Saved: RecNum: ' + IntToStr(FormRecNum));

      if (Trim(NewZeut) <> '') and (NewZeut <> Zeut) then
      begin
        Changes := TChanges.Create(Company);
        try
          Changes.SaveChanges(ObjectType, DirRecNum, acNewZeut, FormRecNum, NewFileInfo.DecisionDate, NewFileNum);
        finally
          Changes.Free;
        end;
      end;
    end;
  end;

  // Moshe 26/11/2018 - Save Taagid data
  if (ObjectType = ocDirector) then
  begin
    Director := Company.Directors.FindByZeut(Zeut) as TDirector;
    if (Assigned(Director.Taagid)) then
    begin
      if (Director.Taagid.Zeut = Zeut) then
      begin
        if (ChangeName) then
          Director.Taagid.Name := NewName;
        if (ChangeZeut) then
          Director.Taagid.Zeut := NewZeut;
        Director.Taagid.SaveData(Director, Now, False, NewFileNum);
      end;
    end;
  end;
  FilesList.Items[FilesList.Count-1].SaveChange(ObjectType,DirRecNum,acAddress, NewDirAd);
  FilesList.Free;
  Company.Free;
end; // TDirector.SaveSpecifiedData

constructor TDirector.LoadFromFile(FileItem: TFileItem; Parent: TCompany; RecNum: Integer);
begin
  Inherited Create(Parent, EncodeDate(1999, 01, 01));
  With DATA.taDirectors Do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If not GotoKey Then
      raise Exception.Create('Record not found at TDirector.LoadFromFile');
    FZeut       := FieldByName('Zeut').AsString;
    FName       := FieldByName('Name').AsString;
    FDate1      := FieldByName('Date1').AsDateTime;
    FDate2      := FieldByName('Date2').AsDateTime;
    If FieldByName('Taagid').AsString = '0' Then
      FTaagid:= Nil
    Else
    begin
      FTaagid:= TTaagid.LoadFromFile(RecNum, FileItem);
    end;
    FAddress:= TDirectorAddress.LoadFromFile(RecNum, FileItem);
  end;
  FActive:= True;
  FRecNum:= RecNum;
end;

constructor TDirector.Create(RecNum: Integer; Parent: TCompany; LastChanged: TDateTime);
var
  Changes: TChanges;
  LineNumber: Integer;
begin
  Inherited Create(Parent, LastChanged);
  Changes := TChanges.Create(Parent);
  try
    DATA.taDirectors.EditKey;
    DATA.taDirectors.FieldByName('RecNum').AsInteger:= RecNum;
    If not DATA.taDirectors.GotoKey Then
      raise Exception.Create('Record not found at TDirector.Create #1');

    FZeut  := DATA.taDirectors.FieldByName('Zeut').AsString;
    FName  := DATA.taDirectors.FieldByName('Name').AsString;
    FDate1 := DATA.taDirectors.FieldByName('Date1').AsDateTime;
    FDate2 := DATA.taDirectors.FieldByName('Date2').AsDateTime;
    if DATA.taDirectors.FieldByName('Taagid').AsString = '0' then
      FTaagid := nil
    else
    begin
      LineNumber := Changes.GetLastChange(ocDirector, RecNum, acTaagidAddress);
      if LineNumber = -1 then
        raise Exception.Create('Record not found at TDirector.Create #2');
      FTaagid := TTaagid.Create(LineNumber);
    end;

    LineNumber := Changes.GetLastChange(ocDirector, RecNum, acAddress);
    if LineNumber = -1 then
      raise Exception.Create('Record not found at TDirector.Create #3');
    FAddress:= TDirectorAddress.Create(LineNumber);

    FActive:= Changes.GetLastChange(ocDirector, RecNum, acDelete) = -1;
    if not Active then
      FLastChanged := TempDate;
    Changes.EnumChanges(ocDirector, RecNum, acNewName, GetNameProc, nil);
    Changes.EnumChanges(ocDirector, RecNum, acNewZeut, GetZeutProc, nil);
  finally
    Changes.Free;
  end;
  FRecNum := RecNum;
end; // TDirector.Create

//------------------Added by Tsahi 24.08.06 ------------------------------------
function TDirector.ChangeManager(CompanyNum: integer; NewZeut,NewName: string; NewAddress: TDirectorAddress;  FileInfo, NewFileInfo: TFileInfo): integer;
var
  TempInfo: TFileInfo;
  FilesList: TFilesList;
  DraftManagers, NonDraft: TManagers;
  Company: Tcompany;
  FileNum: integer;

  function EvCollectData(var NonDraft: TManagers): TManagers;

    function AddNext(const Name, Zeut, Address, Phone, Mail: String): TManager;
    var
      TempManager: TManager;
    begin
      Result := TManager.CreateNew(FileInfo.DecisionDate, Name, Zeut, Address, Phone, Mail);
      TempManager:= Company.Managers.FindByZeut(Zeut) as TManager;
      if TempManager<> nil then
        if (TempManager.Name = Result.Name) and (TempManager.Address = Result.Address)
            and (TempManager.Phone = Result.Phone) then
          Exit
        else
        begin
          TempManager.DeActivate(FileInfo.DecisionDate);
          NonDraft.Add(TempManager);
          NonDraft.Add(Result);
        end
      else
        NonDraft.Add(Result);
    end;

  var
    TempManager: TManager;
    I: Integer;
  begin
    Result:= TManagers.CreateNew;
    NonDraft:= Tmanagers.CreateNew;
    For I:= 0 to Company.Managers.Count-1 do
      if TManager(Company.Managers.Items[i]).Zeut<> Zeut then
        Result.Add(AddNext(TManager(Company.Managers.Items[i]).Name,
                         TManager(Company.Managers.Items[i]).Zeut,
                         TManager(Company.Managers.Items[i]).Address,
                         TManager(Company.Managers.Items[i]).Phone,
                         TManager(Company.Managers.Items[i]).Mail ));

    Result.Add(AddNext(
                NewName, NewZeut,
                NewAddress.Street+' '+NewAddress.Number+' '+NewAddress.City+' '
                 +NewAddress.Index+', '+NewAddress.Country,
                NewAddress.Phone,
                ''));

    Company.Managers.Filter:= afActive;
    for I:= Company.Managers.Count-1 downto 0 do
    begin
      TempManager:= Result.FindByZeut(Company.Managers.Items[I].Zeut) as TManager;
      if TempManager=nil then
      begin
        TempManager:= Company.Managers.Items[I] as TManager;
        TempManager.DeActivate(FileInfo.DecisionDate);
        NonDraft.Add(TempManager);
      end;
    end;
  end;

begin
  DraftManagers := nil;
  Company := TCompany.Create(CompanyNum,Now,False);
  try
    DraftManagers := EvCollectData(NonDraft);
    FilesList := TFilesList.Create(Company.RecNum, faManager, False);
    TempInfo := TFileInfo.Create;
    TempInfo.Action:= faManager;
    TempInfo.Draft:= False;
    TempInfo.DecisionDate:= FileInfo.DecisionDate;
    TempInfo.notifyDate:= FileInfo.notifyDate;
    TempInfo.CompanyNum:= Company.RecNum;
    TempInfo.FileName := FileInfo.FileName;
    FilesList.Add(TempInfo);

    If FilesList.Items[FilesList.Count - 1] <> nil Then
    begin
      NonDraft.ExSaveData(FilesList.Items[FilesList.Count - 1], Company);
      TempInfo.Draft :=True;
      DraftManagers.ExSaveData(FilesList.Items[FilesList.Count - 1], Company);
    end;

    NonDraft.Deconstruct;
    FileNum := TempInfo.RecNum;
    FilesList.Free;
  finally
    if (Assigned(DraftManagers)) then
      DraftManagers.Deconstruct;
    Company.Free;
  end;
  Result := FileNum;
end;

{//------------------Added by Tsahi 19.06.05 ------------------------------------
function TDirector.ChangeManager(CompanyNum: integer; NewZeut,NewName: string; NewAddress: TDirectorAddress;  FileInfo, NewFileInfo: TFileInfo): integer;
var
  OldRecNum, NewRecNum, ItemCount, i: integer;
  Company: Tcompany;
  Changes: TChanges;
  FileNum: integer;
  FilesList:TFilesList;
  FileItem :TFileItem;
begin
  Company:=TCompany.Create(CompanyNum,Now,False);
  ItemCount:= Company.Managers.GetCount;
  FilesList:=TFilesList.Create(CompanyNum,faManager,False);
  FileInfo.CompanyNum :=CompanyNum;
  FileInfo.Action := faManager;
  FilesList.Add(FileInfo);
  FileNum:= FileInfo.RecNum;

  For i:=0 to ItemCount-1 do
  begin
    if Company.Managers.Items[i].Zeut = Zeut then
    begin
      With Data.taManager do
      begin
        Editkey;
        FieldByName('RecNum').AsInteger:= Company.Managers.Items[i].RecNum;
        If not GotoKey Then
          raise Exception.Create('Record not found at TDirector.ChangeManager');
        Edit;
        FieldByName('ResInt1').AsInteger :=-1;    //signifies a non active entry
        Post;
      end;

      OldRecNum := Company.Managers.Items[i].RecNum;
      TManager(Company.Managers.Items[i]).FAddress:=NewAddress.Street+' '+NewAddress.Number+' '+NewAddress.City+' '+NewAddress.Index+', '+NewAddress.Country;
      TManager(Company.Managers.Items[i]).FZeut :=NewZeut;
      TManager(Company.Managers.Items[i]).FName :=NewName;
      TManager(Company.Managers.Items[i]).FPhone :=NewAddress.Phone;
      TManager(Company.Managers.Items[i]).SaveData(Company,False,False,FileNum);
      NewRecNum:=TManager(Company.Managers.Items[i]).RecNum;

      Changes:= TChanges.Create(Company);
      try
        Changes.SaveChanges(ocManager, OldRecNum, acAddress, NewRecNum, NewFileInfo.DecisionDate{StrToDate(FormatDateTime('DD/MM/YYYY',now))}{, FileNum);
      finally
        Changes.Free;
      end;

      FilesList.Items[FilesList.Count-1].SaveChange(ocManager,OldRecNum , acAddress, NewRecNum);
      break;
    end;
  end;
  Company.Free;
  result:=FileNum;
end;}

//------------------
// TCompany
//----------------------------------------------
procedure TCompany.SetRegistrationDate(Value: TDateTime; Changes: TChanges);
begin
  Changes:= TChanges.Create(Self);
  try
    Changes.ChangeRegistrationDate(ocCompany, RecNum, acAddress, -1, Value);
    MuhazList.SetRegistrationDate(Value, Changes);
    Managers.SetRegistrationDate(Value, Changes);
    CompanyInfo.SetRegistrationDate(Value, Changes);
    HonList.SetRegistrationDate(Value, Changes);
    StockHolders.SetRegistrationDate(Value, Changes);
    Directors.SetRegistrationDate(Value, Changes);
    Changes.ChangeRegistrationDate(ocCompany, RecNum, acNewMagish, -1, Value);
  finally
    Changes.Free;
  end;
end;

constructor TCompany.CreateNew(ShowingDate: TDateTime;
                               Const Name, Zeut: String;
                               Address: TCompanyAddress;
                               Directors: TDirectors;
                               StockHolders: TStockHolders;
                               HonList: THonList;
                               MuhazList: TMuhazList;
                               Managers: TManagers;
                               CompanyInfo: TCompanyInfo;
                               SubmissionData: TSubmissionData;
                               Magish: TMagish);
begin
  Inherited CreateNew(ShowingDate, Name, Zeut);
  FAddress := Address;
  FDirectors := Directors;
  FStockHolders := StockHolders;
  FMuhazList := MuhazList;
  FHonList := HonList;
  FManagers := Managers;
  FCompanyInfo := CompanyInfo;
  FSubmissionData :=SubmissionData;
  FMagish := Magish;
  FDisabled := False;
end;

function TCompany.NameEnum(FileItem: TFileItem): Boolean;
begin
  Result:= True;
  If (FileItem.FileInfo.Action = faCompanyName) And
     (FileItem.FileInfo.DecisionDate>ShowingDate) then
  begin
    TempName := FileItem.FileInfo.FileName;
    Result := False;
  end;
end;

function TCompany.GetName: String;
var
  History: THistory;
begin
  History:= THistory.Create;
  try
    TempName := FName;
    History.EnumBlanks(RecNum, NameEnum);
    Result := TempName;
  finally
    History.Free;
  end;
end;

destructor TCompany.Destroy;
begin
  if Assigned(Address) then Address.Free;
  if Assigned(Directors) then Directors.Free;
  if Assigned(StockHolders)then StockHolders.Free;
  if Assigned(HonList)then HonList.Free;
  if Assigned(MuhazList)then MuhazList.Free;
  if Assigned(Managers)then Managers.Free;
  if Assigned(CompanyInfo)then CompanyInfo.Free;
  if Assigned(Magish)then Magish.Free;
  if Assigned(SubmissionData)then SubmissionData.Free;
end;

procedure TCompany.GetRecNumProc(ObjectNum, NewNum: Integer);
begin
  FRecNum:= NewNum;
end;

constructor TCompany.LoadFromFileTakanonChange(FileItem:TFileItem;CompanyRecNum:integer);
begin
  Inherited CreateNew(FileItem.FFileInfo.DecisionDate, FileItem.FFileInfo.FileName,
                      IntToStr(FileItem.FFileInfo.CompanyNum));
  FRecNum         := CompanyRecNum;
  FAddress        := TCompanyAddress.LoadFromFile(RecNum, FileItem);
  FDirectors      := TDirectors.LoadFromFile(Self, FileItem);
  FStockHolders   := TStockHolders.LoadFromFile(Self, FileItem);
  FHonList        := THonList.LoadFromFile(Self, FileItem);
  FMuhazList      := TMuhazList.LoadFromFile(Self, FileItem);
  FManagers       := TManagers.LoadFromFile(Self, FileItem);
  FSubmissionData := TSubmissionData.LoadFromFile(Self, FileItem);
  FCompanyInfo    := TCompanyInfo.LoadFromFile(Self, FileItem);
  FMagish         := TMagish.LoadFromFile(RecNum, FileItem);
  FDisabled       := False;
end;

constructor TCompany.LoadFromFile(Parent: TCompany; FileItem: TFileItem);
var
  CompanyName, CompanyNum: string;
begin
  if (Parent = nil) then
  begin
    CompanyName := FileItem.FFileInfo.FileName;
    CompanyNum  := IntToStr(FileItem.FFileInfo.CompanyNum);
  end
  else
  begin
    CompanyName := Parent.Name;
    CompanyNum  := Parent.Zeut;
  end;

  Inherited CreateNew(FileItem.FFileInfo.DecisionDate, CompanyName, CompanyNum);
  if FileItem.FFileInfo.Draft then
    FRecNum := FileItem.FFileInfo.RecNum
  else
  begin
    if FileItem.EnumChanges(ocCompany, -1, acRecNum, GetRecNumProc) < 1 then
      raise Exception.Create('Record count error at TCompany.LoadFromFile');
  end;
  //FAddress        := TCompanyAddress.LoadFromFile(FileItem.fparent.fcompany, FileItem);    //sts23822
  FAddress        := TCompanyAddress.LoadFromFile(RecNum, FileItem);
  FDirectors      := TDirectors.LoadFromFile(Self, FileItem);
  FStockHolders   := TStockHolders.LoadFromFile(Self, FileItem);
  FHonList        := THonList.LoadFromFile(Self, FileItem);
  FMuhazList      := TMuhazList.LoadFromFile(Self, FileItem);
  FManagers       := TManagers.LoadFromFile(Self, FileItem);
  FSubmissionData := TSubmissionData.LoadFromFile(Self,FileItem);
  FCompanyInfo    := TCompanyInfo.LoadFromFile(Self, FileItem);
  //FMagish         := TMagish.LoadFromFile(FileItem.fparent.fcompany, FileItem);  //sts23822
  FMagish         := TMagish.LoadFromFile(RecNum, FileItem);
  FDisabled       := False;
end;

procedure TCompany.FileDelete(FileItem: TFileItem);
begin
  Address.FileDelete(Self, FileItem);
  Directors.FileDelete(FileItem);
  StockHolders.FileDelete(FileItem);
  HonList.FileDelete(FileItem);
  MuhazList.FileDelete(FileItem);
  Managers.FileDelete(FileItem);
  CompanyInfo.FileDelete(FileItem);
  If Assigned(Magish) Then
    Magish.FileDelete(FileItem);
  If (FileItem=nil) Or (not FileItem.FFileInfo.Draft) Then
    With DATA.taName Do
    begin
      EditKey;
      FieldByName('RecNum').AsInteger:= RecNum;
      If GotoKey then
        Delete;
    end;
  If FileItem<> Nil Then
    FileItem.DeleteFile;
end;

procedure TCompany.ExSaveData(FileItem: TFileItem; Parent: TBasicClass);
begin
  try
    if not FileItem.FFileInfo.Draft then
    begin
      SaveData(Parent, False, FileItem.FFileInfo.Draft, FileItem.FileInfo.RecNum);
      FileItem.SaveChange(ocCompany, RecNum, acRecNum, RecNum);
    end
    else
      FRecNum := FileItem.FFileInfo.RecNum;

    Address.ExSaveData(FileItem, Self, LastChanged);
    Directors.ExSaveData(FileItem, Self);
    StockHolders.ExSaveData(FileItem, Self);
    HonList.ExSaveData(FileItem, Self);
    MuhazList.ExSaveData(FileItem, Self);
    Managers.ExSaveData(FileItem, Self);
    CompanyInfo.ExSaveData(FileItem, Self);
    SubmissionData.ExSaveData(FileItem, Self);
    if Assigned(Magish) then
      Magish.ExSaveData(FileItem, Self);
  except
    on E: Exception do
      WriteChangeLog(#9#9#9'TCompany.ExSaveData: Exception Message: ' + E.Message);
  end;
end;

procedure TCompany.SaveData(Parent: TBasicClass; SaveChild, Draft: Boolean; FileNum: Integer);
begin
  try
    { Added on 06/03/2005 by ES --> }
    WriteChangeLog(#9#9'--------------------------');
    WriteChangeLog(#9#9'Saving company (' + Self.Name + ', ' + Zeut + ')');
    { <-- }
    with DATA.taName do
    begin
       Append;
       FieldByName('Name').AsString:= Self.Name;
       FieldByName('CompNum').AsString:= Zeut;
       Post;
       FRecNum := FieldByName('RecNum').AsInteger;
       { Added on 07/03/2005 by ES - new log --> }
       WriteChangeLog(#9#9#9#9'taName: RecNum after save: ' + IntToStr( FRecNum ));
       { <-- }
    end;
    if SaveChild then
    begin
      WriteChangeLog(#9#9#9#9'Saving Child...');
      Address.SaveData(Self, LastChanged, Draft, FileNum);
      Directors.SaveData(Self, FileNum);
      StockHolders.SaveData(Self, FileNum);
      HonList.SaveData(Self, FileNum);
      MuhazList.SaveData(Self, FileNum);
      Managers.SaveData(Self, FileNum);
      CompanyInfo.SaveData(Self, Draft, FileNum);
      SubmissionData.SaveData(Self);
      if Assigned(Magish) then
        Magish.SaveData2(Self, FileNum, True, Draft);
      WriteChangeLog(#9#9#9#9'Child Saved');
    end;
    except
      on E: Exception do
      begin
        WriteChangeLog(#9#9#9'TCompany.SaveData: Exception Message: ' + E.Message);
      end;
  end;
end; // TCompany.SaveData

procedure TCompany.SetNumber(Const Number, AName: String);
begin
  FZeut:= Number;
  FName:= AName;
  with DATA.taName Do
  begin
    Filtered:= False;
    EditKey;
    FieldByName('RecNum').AsInteger:= RecNum;
    If not GotoKey Then
      raise Exception.Create('Record not found at TCompany.SetNumber');
    Edit;
    FieldByName('CompNum').AsString:= Number;
    FieldByName('Name').AsString:= AName;
    Post;
  end;
end;

constructor TCompany.Create(RecNum: Integer; ShowingDate: TDateTime; FastLoad: Boolean);
var
  Changes: TChanges;
  CompInfoRecNum: integer;
  Table : TTable;
  zipFilesList: TFilesList;
  currentActionDate: TDateTime;
begin
  zipFilesList := TFilesList.Create(RecNum, faZipCode, False);
  inherited Create(Self, ShowingDate);
  FFastLoad := FastLoad;
  FRecNum := RecNum;
  With DATA.taName Do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger := RecNum;
    If not GotoKey Then
      raise Exception.Create('Record not found at TCompany.Create #1');
    FName := FieldByName('Name').AsString;
    FZeut := FieldByName('CompNum').AsString;
    try
      FDisabled := FieldByName('ResInt1').AsInteger = 1;
    except
      FDisabled := False;
    end;
  end;
  if not FastLoad Then
  begin
    Changes := TChanges.Create(Self);
    Table := Data.taAddress;
    Table.Open;

    try
      Table.EditKey;
      Table.FieldByName('RecNum').AsInteger := Changes.GetLastChange(ocCompany, RecNum, acAddress);
      if not Table.GotoKey then
        raise Exception.Create('Record not found at TCompany.Create #2');
      currentActionDate :=  TFilesList.GetLatestActionDate(Self, faAddress, False, ocCompany, RecNum, acAddress);
      FAddress := TCompanyAddress.Create;
      FAddress.FStreet  := Table.FieldByName('Street').AsString;
      FAddress.FNumber  := Table.FieldByName('Number').AsString;
      FAddress.FIndex   := zipFilesList.GetLatestZip(ocCompany, RecNum, ShowingDate, currentActionDate, Table.FieldByName('Index').AsString);
      //FAddress.FIndex := Table.FieldByName('Index').AsString;
      FAddress.FCity    := Table.FieldByName('City').AsString;
      FAddress.FCountry := Table.FieldByName('Country').AsString;
      FAddress.FPOB     := Table.FieldByName('POB').AsString;
      FAddress.FEzel    := Table.FieldByName('Ezel').AsString;
      FAddress.FPhone   := Table.FieldByName('Phone').AsString;
      FAddress.FFax     := Table.FieldByName('Fax').AsString;
      FAddress.FRecNum  := Table.FieldByName('RecNum').AsInteger;
      CompInfoRecNum := Changes.GetLastChange(ocCompany, RecNum, acSignName);
      if CompInfoRecNum <> -1 then
        FCompanyInfo := TCompanyInfo.Create(CompInfoRecNum, Self);
    finally
      Changes.Free;
    end;
  end;

  FDirectors := TDirectors.Create(Self);
  GetDirectorsLatestZips(zipFilesList, ShowingDate);

  FStockHolders := TStockHolders.Create(Self);
  GetStockHoldersLatestZips(zipFilesList, ShowingDate);

  if not FastLoad then
  begin
    FHonList := THonList.Create(Self);
    FMuhazList:= TMuhazList.Create(Self);
    FManagers := TManagers.Create(Self);
    GetManagersLatestZips(zipFilesList,ShowingDate);
  end;
  FSubmissionData := TSubmissionData.Create(Self);
  Magish := TMagish.Create(Self);
end; // TCompany.Create

procedure TCompany.GetDirectorsLatestZips(zipFilesList: TFilesList; showingDate: TDateTime);
var
  director: TDirector;
  i :integer;
  currentActionDate: TDateTime;
begin
  for i := 0 to FDirectors.Count - 1 do
  begin
    director :=  FDirectors.Items[i] as TDirector;
    currentActionDate :=  TFilesList.GetLatestActionDate(Self, faDirectorInfo, False, ocDirector, director.RecNum,acAddress);
    director.Address.Index := zipFilesList.GetLatestZip(ocDirector, director.RecNum, showingDate,currentActionDate, director.Address.Index);
    if director.Taagid <> nil then
    begin
       director.Taagid.Index := zipFilesList.GetLatestZip(ocTaagid, director.RecNum, showingDate, currentActionDate, director.Taagid.Index);
    end;
  end;
end;

procedure TCompany.GetStockHoldersLatestZips(zipFilesList: TFilesList; showingDate: TDateTime);
var
  stockHolder: TStockHolder;
  i :integer;
  currentActionDate: TDateTime;
begin
  for i := 0 to FStockHolders.Count - 1 do
  begin
    stockHolder :=  FStockHolders.Items[i] as TStockHolder;
    currentActionDate :=  TFilesList.GetLatestActionDate(Self, faStockHolderInfo, False, ocStockHolder, stockHolder.RecNum, acAddress);
    stockHolder.Address.Index := zipFilesList.GetLatestZip(ocStockHolder, stockHolder.RecNum, showingDate, currentActionDate, stockHolder.Address.Index);
  end;
end;

procedure TCompany.GetManagersLatestZips(zipFilesList: TFilesList; showingDate: TDateTime);
var
  manager: TManager;
  i :integer;
  currentActionDate: TDateTime;
begin
  for i := 0 to FManagers.Count - 1 do
  begin
    manager :=  FManagers.Items[i] as TManager;
    currentActionDate :=  TFilesList.GetLatestActionDate(Self,faManager , False, ocManager, manager.RecNum, acAddress);
    manager.Address := zipFilesList.GetLatestZip(ocManager, manager.RecNum, showingDate, currentActionDate, manager.Address);
  end;
end;

procedure TCompany.SetDisabled(Value: Boolean);
var
  v: integer;
begin
  if Value then
    v := 1
  else
    v := 0;
  With Data.taName do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger := RecNum;
    If not GotoKey Then
      raise Exception.Create('Record not found at TCompany.SetDisabled');
    Edit;
    FieldByName('ResInt1').AsInteger := v;
    Post;
    FDisabled:= Value;
  end;
end;

function TCompany.GetCreatedDate: TDateTime;
var
  Table: TTable;
  lDate: TDateTime;
begin
  lDate := EncodeDate(2999, 12, 31);
  Result := lDate;
  try
    Table := TTable.Create(Data);
    try
      Table.DatabaseName:= Data.taChanges.DatabaseName;
      Table.TableName:= Data.taChanges.TableName;
      Table.IndexName:= Data.taChanges.IndexName;
      Table.Open;
      Table.Filter := '(ObjectType = 0) AND (ObjectNum = ' + IntToStr(Self.RecNum) + ')';
      Table.Filtered := True;
      while (not Table.EOF) do
      begin
        lDate := Table.FieldByName('Date').AsDateTime;
        if (Result > lDate) then
          Result := lDate;
        Table.Next;
      end;
      Table.Close;
    finally
      Table.Free;
    end;
  except
    on E: Exception do
      WriteChangeLog(#9#9#9'TCompany.GetCreatedDate: Exception Message: ' + E.Message);
  end;
end;

// TMagish
//--------------------------------------------
function TMagish.UpdateData(RecNum: integer): boolean;
begin
  // Moshe 19/12/16 Yud Tet Kistev Taf Shin Ain Zain
  Result := False;
  if (RecNum < 1) then
    Exit;
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'UpdateData(' + FName + ', ' + Zeut + ')');
  DATA.taMagish.EditKey;
  DATA.taMagish.FieldByName('RecNum').AsInteger  := RecNum;
  if (not DATA.taMagish.GotoKey) then
    Exit;
  DATA.taMagish.Edit;
  DATA.taMagish.FieldByName('Name').AsString     := FName;
  if FTaagid then
    DATA.taMagish.FieldByName('Taagid').AsString := '1'
  else
    DATA.taMagish.FieldByName('Taagid').AsString := '0';
  DATA.taMagish.FieldByName('Zeut').AsString     := FZeut;
  DATA.taMagish.FieldByName('Phone').AsString    := FPhone;
  DATA.taMagish.FieldByName('Street').AsString   := FStreet;
  DATA.taMagish.FieldByName('Number').AsString   := FNumber;
  DATA.taMagish.FieldByName('Index').AsString    := FIndex;
  DATA.taMagish.FieldByName('City').AsString     := FCity;
  DATA.taMagish.FieldByName('Country').AsString  := FCountry;
  DATA.taMagish.FieldByName('Mail').AsString     := FMail;
  DATA.taMagish.FieldByName('Fax').AsString      := FFax;
  DATA.taMagish.Post;
  FRecNum := RecNum;
  Result := True;
end; // TMagish.UpdateData

procedure TMagish.AppendRec;
begin
  Data.taMagish.Append;
  Data.taMagish.FieldByName('Name').AsString     := FName;
  if (FTaagid) then
    Data.taMagish.FieldByName('Taagid').AsString := '1'
  else
    Data.taMagish.FieldByName('Taagid').AsString := '0';
  Data.taMagish.FieldByName('Zeut').AsString     := FZeut;
  Data.taMagish.FieldByName('Phone').AsString    := FPhone;
  Data.taMagish.FieldByName('Street').AsString   := FStreet;
  Data.taMagish.FieldByName('Number').AsString   := FNumber;
  Data.taMagish.FieldByName('Index').AsString    := FIndex;
  Data.taMagish.FieldByName('City').AsString     := FCity;
  Data.taMagish.FieldByName('Country').AsString  := FCountry;
  Data.taMagish.FieldByName('Mail').AsString     := FMail;
  Data.taMagish.FieldByName('Fax').AsString      := FFax;
  Data.taMagish.Post;
  FRecNum := Data.taMagish.FieldByName('RecNum').AsInteger;
end;

procedure TMagish.SaveFixed(Parent: TCompany; FileNum: Integer);
var
  Changes: TChanges;
begin
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'TMagish.SaveFixed(' + FName + ', ' + Zeut + ')');
  AppendRec;
  WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr( FRecNum ));
  Changes := TChanges.Create(Parent);
  Changes.SaveChanges(ocCompany, Parent.RecNum, acNewMagish, FRecNum, Date, FileNum);
  Changes.Free;
end;

constructor TMagish.LoadFromFile(ParentNum: Integer; FileItem: TFileItem);
var
  Changes: TChanges;
begin
  if FileItem.FileInfo.Draft then
    FRecNum := FileItem.EnumChanges(ocCompany, ParentNum, acNewMagish, LoadEnum) // load Magish with callback function LoadEnum
  else
  begin
    Changes := TChanges.Create(nil);
    try
      FRecNum := Changes.GetLastChange(ocCompany, ParentNum, acNewMagish);
    finally
      Changes.Free;
    end;
    LoadEnum(ParentNum, FRecNum);
  end;
end;

procedure TMagish.LoadEnum(ObjectNum, NewNum: Integer);
begin
  FRecNum := NewNum;
  With DATA.taMagish Do
  begin
    EditKey;
    FieldByName('RecNum').AsInteger := RecNum;
    If not GotoKey Then
    begin
      FName   := '';
      FTaagid := False;
      FZeut   := '';
      FPhone  := '';
      FMail   := '';
      FFax    := '';
      FStreet := '';
      FNumber := '';
      FIndex  := '';
      FCity   := '';
      FCountry:= '';
      exit;
    end;
    With Self Do
    begin
      FName     := FieldByName('Name').AsString;
      FTaagid   := (FieldByName('Taagid').AsString = '1');
      FZeut     := FieldByName('Zeut').AsString;
      FPhone    := FieldByName('Phone').AsString;
      FMail     := FieldByName('Mail').AsString;
      FFax      := FieldByName('Fax').AsString;
      FStreet   := FieldByName('Street').AsString;
      FNumber   := FieldByName('Number').AsString;
      FIndex    := FieldByName('Index').AsString;
      FCity     := FieldByName('City').AsString;
      FCountry  := FieldByName('Country').AsString;
    end;
  end;
end;

constructor TMagish.Create(Parent: TCompany);
var
  Changes: TChanges;
begin
  Changes:= TChanges.Create(Parent);
  try
    FRecNum:= Changes.GetLastChange(ocCompany, Parent.RecNum, acNewMagish);
  finally
    Changes.Free;
  end;
  LoadEnum(Parent.RecNum, FRecNum);
end;

constructor TMagish.CreateNew(Const Name, Zeut, Street, Number, Index, City, Country, Phone,Mail,Fax: String; Taagid: Boolean);
begin
  Inherited CreateNew(Street, Number, Index, City, Country);
  FName:= Name;
  FZeut:= Zeut;
  FPhone:= Phone;
  FMail := Mail;
  FFax  := Fax;
  FTaagid:= Taagid;
end;

procedure TMagish.ExSaveData(FileItem: TFileItem; Parent: TCompany);
begin
  SaveData(Parent, FileItem);
end;

procedure TMagish.SaveData(Parent: TCompany; FileItem: TFileItem);
var
  Changes:TChanges;
begin
  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9' TMagish.SaveData(' + FName + ', ' + Zeut + ')');
  AppendRec;

  { Added on 07/03/2005 by ES - new log --> }
  WriteChangeLog(#9#9#9#9'TMagish.RecNum after save: ' + IntToStr(FRecNum));
  if not Assigned(FileItem) then
    exit;

  if FileItem.FileInfo.Draft then
    FileItem.SaveChange(ocCompany, Parent.RecNum, acNewMagish, FRecNum)
  else
  begin
    Changes := TChanges.Create(nil);
    try
      Changes.SaveChanges(ocCompany, Parent.RecNum, acNewMagish, FRecNum, Date, FileItem.FileInfo.RecNum);
    finally
      Changes.Free;
    end;
  end;
end;

procedure TMagish.SaveData2(Parent: TCompany; FileNum: integer; WithChanges, Draft: boolean);
var
  Changes: TChanges;
  Table: TTable;
begin
  SaveData(Parent, nil);
  if not WithChanges then exit;
  if Draft then
  begin
    Table:=TTable.Create(nil);
    Table.DatabaseName := Data.taForms.DatabaseName;
    Table.TableName := Data.taForms.TableName;
    Table.AfterOpen := Data.taForms.AfterEdit;
    Table.AfterInsert := Data.taForms.AfterInsert;
    Table.Open;
    Table.Append;
    Table.FieldByName('FileNum').AsInteger := FileNum;
    Table.FieldByName('ObjectType').AsString := ObjToChar(ocCompany);
    Table.FieldByName('ObjectNum').AsInteger := Parent.RecNum;
    Table.FieldByName('Action').AsString := ActionToChar(acNewMagish);
    Table.FieldByName('NewNum').AsInteger := FRecNum;
    Table.Post;
    Table.Close;
    Table.Free;
  end
  else
  begin
    Changes := TChanges.Create(Parent);
    try
      Changes.SaveChanges(ocCompany, Parent.RecNum, acNewMagish, FRecNum, Date, FileNum);
    finally
      Changes.Free;
    end;
  end;
end;

procedure TMagish.FileDelete(FileItem:TFileItem);
begin
  if not Data.DeleteByRecNum(Data.taMagish, FRecNum) then
  begin
    raise Exception.Create('SQL error at at TMagish.FileDelete #1');
    Exit;
  end;
  if Assigned(FileItem) then FileItem.DeleteFile;
end;

class procedure TMagish.Undo(RecNum: integer);
begin
  DATA.taMagish.EditKey;
  DATA.taMagish.FieldByName('RecNum').AsInteger := RecNum;
  if (DATA.taMagish.GoToKey) then
    DATA.taMagish.Delete;
end;

// TFileInfo
//---------------------------------------------
procedure TFileInfo.Assign(Source: TFileInfo);
begin
  Action:= Source.Action;
  Draft:= Source.Draft;
  FileName:= Source.FileName;
  //--> Change Company Name functionality improvement == Added on 06/11/2001 by Elijah Shlieserman
  //PreviousName := Source.PreviousName;
  //<--
  DecisionDate:= Source.DecisionDate;
  RecNum:= Source.RecNum;
  notifyDate := Source.notifyDate;
  CompanyNum := Source.CompanyNum;
  OldStyle:= Source.OldStyle;

  //--> additional fields to allow Company Name style changes for other fields (namely Zip Code). == Tsahi: 07/07/2013
  AdditionalData          := Source.AdditionalData;
  AdditionalObjectClass   := Source.AdditionalObjectClass;
  AdditionalObjectNum     := Source.AdditionalObjectNum;
  // Moshe 16/12/2018
  Takanon                 := Source.Takanon;
end;

// TFileItem
//---------------------------------------------

constructor TFileItem.Create(Parent: TFilesList; FileInfo: TFileInfo);
begin
  FParent := Parent;
  FFileInfo := TFileInfo.Create;
  FFileInfo.Assign(FileInfo);
end;

destructor TFileItem.Destroy;
begin
  FFileInfo.Free;
end;

function TFileItem.EnumChanges(ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction;
                               EnumProc: TFileEnumProc): Integer;
var
  Table: TTable;
begin
  Result := 0;
  Table := TTable.Create(Data);
  try
    Table.DatabaseName := Data.taForms.DatabaseName;
    Table.TableName := Data.taForms.TableName;
    Table.IndexName := Data.taForms.IndexName;
    Table.Open;
    begin
      Table.Filter := '([FileNum] = ' + IntToStr(FFileInfo.RecNum) + ') AND ' +
                      '([ObjectType] = ''' + ObjToChar(ObjectType) + ''') AND ' +
                      '([ACTION] = ''' + ActionToChar(Action) + ''')';
      if (ObjectNum <> -1) then
        Table.Filter := Table.Filter + ' AND ([ObjectNum] = ''' + IntToStr(ObjectNum) + ''')';
      Table.Filtered := True;
      Table.First;
      while (not Table.EOF) do
      begin
        EnumProc(Table.FieldByName('ObjectNum').AsInteger, Table.FieldByName('NewNum').AsInteger);
        Result := Result + 1;
        Table.Next;
      end;
      Table.Filtered := False;
    end;
  finally
    Table.Free;
  end;
end;

// Moshe 16/12/2018
function TFileItem.EnumTakanonChanges(ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction;
  EnumProc: TFileEnumProc): Integer;
var
  Table: TTable;
  strFilter: string;
begin
  Result := 0;
  Table := TTable.Create(Data);
  try
    Table.DatabaseName := Data.taForms.DatabaseName;
    Table.TableName := Data.taForms.TableName;
    Table.IndexName := Data.taForms.IndexName;
    Table.Open;
    with Table do
    begin
      strFilter := '(FileNum=' + IntToStr(FFileInfo.RecNum) + ') AND ' +
                   '(ObjectType=''' + ObjToChar(ObjectType)+ ''') AND ' +
                   '(Action= ''' + ActionToChar(Action) + ''') AND ' +
                   '(ResStr1=''Takanon'')';
      if (ObjectNum <> -1) then
        strFilter := strFilter + ' AND (ObjectNum=''' + IntToStr(ObjectNum) + ''')';
      Filter := strFilter;
      Filtered := True;
      First;
      while not Table.EOF do
      begin
        EnumProc(FieldByName('ObjectNum').AsInteger, FieldByName('NewNum').AsInteger);
        Result := Result + 1;
        Next;
      end;
      Filtered := False;
    end;
  finally
    Table.Free;
  end;
end;

function TFileItem.GetLastChange(FileNum: integer; ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction): Integer;
var
  Q: TQuery;
  N: integer;
  W: string;
begin
  Result := -1;
  W := '';
  if (FileNum > 0) then
    W  := 'AND (FileNum=' + IntToStr(FileNum) + ') ';

  Q := TQuery.Create(nil);
  Q.DatabaseName := Data.taForms.DatabaseName;
  Q.SQL.Text := ' SELECT RecNum, NewNum ' +
                ' FROM Forms ' +
                ' WHERE (ObjectType=''' + ObjToChar(ObjectType) + ''') AND ' +
                      ' (ObjectNum=' + IntToStr(ObjectNum) + ') AND ' +
                      ' (Action=''' + ActionToChar(Action) + ''') ';
  if (W <> '') then
    Q.SQL.Text := Q.SQL.Text + W;
  Q.SQL.Text := Q.SQL.Text + ' ORDER BY RecNum DESC';
  Q.Open;
  while (not Q.EOF) do
  begin
    N := Q.FieldByName('NewNum').AsInteger;
    if (Result < N) then
      Result  := N;
    Q.Next;
  end;
  Q.Free;
end;

function TFileItem.GetDataRecNum(CompanyNum: integer; FileAction: TFileAction; ObjectType: TObjectClass; DataAction: TAction): integer;
var
  Q: TQuery;
  N: integer;
begin
  Result := -1;
  Q := TQuery.Create(nil);
  Q.DatabaseName := Data.taForms.DatabaseName;
  Q.SQL.Text := ' SELECT O.RecNum, O.NewNum ' +
                ' FROM Files I, Forms O ' +
                ' WHERE (I.Company=' + IntToStr(CompanyNum) + ') AND ' +
                      ' (I.Action=''' + FileActionToChar(FileAction) + ''') AND ' +
                      ' (I.RecNum=O.FileNum) AND ' +
                      ' (O.ObjectType=''' + ObjToChar(ObjectType) + ''') AND ' +
                      ' (O.Action=''' + ActionToChar(DataAction) + ''') ' +
                      ' ORDER BY O.RecNum DESC';
  Q.Open;
  while (not Q.EOF) do
  begin
    N := Q.FieldByName('NewNum').AsInteger;
    if (Result < N) then
      Result := N;
    Q.Next;
  end;
  Q.Free;
end;

procedure TFileItem.DeleteFile;
begin
  WriteChangeLog(#9#9#9#9'DeleteFile: FileNum: ' + IntToStr(FFileInfo.RecNum));
  if Data.ExecSQL(' DELETE FROM ' + GetTableName4SQL(Data.taForms) +
                  ' WHERE FileNum=' + IntToStr(FFileInfo.RecNum),Data.taForms)then
     WriteChangeLog(#9#9#9#9'All file reference in Forms deleted');
  Data.ExecSQL(' DELETE FROM ' + GetTableName4SQL(Data.taFiles) +
               ' WHERE RecNum=' + IntToStr(FFileInfo.RecNum),Data.taFiles);
end;

procedure TFileItem.DeleteRecord(ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction; NewNum: Integer);
var
  Table: TTable;
begin
  Table:= TTable.Create(Data);
  try
    Table.DatabaseName:= Data.taForms.DatabaseName;
    Table.TableName:= Data.taForms.TableName;
    Table.IndexName:= Data.taForms.IndexName;
    Table.Open;
    With Table Do
    begin
      WriteChangeLog(#9#9#9#9'DeleteRecord: ObjectType: ' + ObjectToName(ObjectType) +
                     ',ObjectNum:' + IntToStr(ObjectNum) + ', Action:' + ActionToName(Action) + ', NewNum:' + IntToStr(NewNum));
      Filter:= '([FileNum] = ''' + IntToStr(FFileInfo.RecNum) + ''') AND ([ObjectType]=''' + ObjToChar(ObjectType)
               + ''') AND ([ObjectNum] = ''' + IntToStr(ObjectNum) + ''') AND ([ACTION] = ''' + ActionToChar(Action)
               + ''') AND ([NewNum] = ''' + IntToStr(NewNum) + ''')';
      Filtered := True;
      If RecordCount > 0 Then
      begin
        Delete;
        WriteChangeLog(#9#9#9#9'Record Deleted');
      end;
    end;
  finally
    Table.Free;
  end;
end;

procedure TFileItem.SetDraft;
var
  TempStr: String;
  FileNum: Integer;
begin
  With Data.taFiles Do
  begin
    FileNum := 0;
    Filter := '([RecName] = ''' + FilterStr(FFileInfo.FileName) + ''') And ([Draft] = ''1'')';
    Filtered := True;

    while not EOF do
    begin
      Inc(FileNum);
      Filter := '([RecName] = ''' + FilterStr(FFileInfo.FileName+IntToStr(FileNum)) + ''') And ([Draft] = ''1'')';
      Filtered := True;
    end;

    Filtered := False;
    TempStr := IndexName;
    IndexName := '';
    EditKey;
    FieldByName('RecNum').AsInteger := FFileInfo.RecNum;
    If not GotoKey Then
      raise Exception.Create('Record not found at FileItem.SetDraft');
    Edit;
    FieldByName('Draft').AsString := '1';
    If FileNum <> 0 Then
      FieldByName('RecName').AsString := FFileInfo.FileName+IntToStr(FileNum);
    Post;
    IndexName := TempStr;
  end;
end;

function TFileItem.LoadNameChange(var NewName: String): Boolean;
var
  Table: TTable;
begin
  Result:= False;
  Table:= TTable.Create(Data);
  Table.DatabaseName:= Data.taForms.DatabaseName;
  Table.TableName:= Data.taForms.TableName;
  Table.IndexName:= Data.taForms.IndexName;
  Table.Open;
  Table.Filter:= '([FileNum] = ''' + IntToStr(FFileInfo.RecNum) + ''') AND ([ObjectType] = ''' +
                 ObjToChar(ocDirector) + ''') AND ([Action] = ''' + ActionToChar(acNewName) + ''')';
  Table.Filtered:= True;
  while not Table.EOF do
  begin
    NewName:= Table.FieldByName('ResStr1').AsString + Table.FieldByName('ResStr2').AsString;
    Result:= True;
    Table.Next;
  end;
  Table.Close;
  Table.Free;
end;

function TFileItem.SaveNameChange(const NewName: String; Action: TAction; ObjectType: TObjectClass): Integer;
var
  Table: TTable;
  S1, S2: String;
begin
  Table:= TTable.Create(Data);
  Table.DatabaseName := Data.taForms.DatabaseName;
  Table.TableName := Data.taForms.TableName;
  Table.IndexName := Data.taForms.IndexName;
  Table.AfterOpen := Data.taForms.AfterOpen;
  Table.AfterInsert := Data.taForms.AfterInsert;
  Table.AfterEdit := Data.taForms.AfterEdit;
  Table.Open;
  With Table Do
  begin
    WriteChangeLog(#9#9#9#9'SaveNameChange in Forms: FileNum:' + IntToStr( FFileInfo.RecNum )+
                   ', ObjectType:' + ObjectToName(ObjectType) +
                   ', ObjectNum:-1, Action:' + ActionToName(acNewName) + ', NewNum:-1, ResStr1+ResStr2:' + NewName);
    Append;
    FieldByName('FileNum').AsInteger := FFileInfo.RecNum;
    FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
    FieldByName('ObjectNum').AsInteger := -1;
    FieldByName('Action').AsString := ActionToChar(Action);
    FieldByName('NewNum').AsInteger := -1;
    S1:= Copy(NewName, 1, 20);
    S2:= Copy(NewName, 21, 80);
    FieldByName('ResStr1').AsString := S1;
    FieldByName('ResStr2').AsString := S2;
    Post;
    Result:= FieldByName('RecNum').AsInteger;
    WriteChangeLog(#9#9#9#9'Saved: RecNum: ' + IntToStr(Result));
  end;
  Table.Free;
end;

procedure TFileItem.SaveChange(ObjectType: TObjectClass; ObjectNum: Integer; Action: TAction; NewNum: Integer);
var
  Table: TTable;
begin
  Table := TTable.Create(Data);
  try
    Table.DatabaseName := Data.taForms.DatabaseName;
    Table.TableName    := Data.taForms.TableName;
    Table.IndexName    := Data.taForms.IndexName;
    Table.AfterOpen    := Data.taForms.AfterOpen;
    Table.AfterInsert  := Data.taForms.AfterInsert;
    Table.AfterEdit    := Data.taForms.AfterEdit;
    Table.Open;
    WriteChangeLog(#9#9#9#9'SaveChange in Forms: FileNum: ' + IntToStr(FFileInfo.RecNum) +
                   ', ObjectType: ' + ObjectToName(ObjectType) +
                   ', ObjectNum: ' + IntToStr(ObjectNum) +
                   ', Action: ' + ActionToName(Action) +
                   ', NewNum: ' + IntToStr(NewNum));
    Table.Append;
    Table.FieldByName('FileNum').AsInteger := FFileInfo.RecNum;
    Table.FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
    Table.FieldByName('ObjectNum').AsInteger := ObjectNum;
    Table.FieldByName('Action').AsString := ActionToChar(Action);
    Table.FieldByName('NewNum').AsInteger := NewNum;
    if (FFileInfo.Takanon) then
      Table.FieldByName('ResStr1').AsString := 'Takanon';
    Table.Post;
    WriteChangeLog(#9#9#9#9'Saved: RecNum: ' + Table.FieldByName('RecNum').AsString);
  finally
    Table.Free;
  end;
end;

// TFilesList
//-----------------------------------------------
function TFilesList.FileExists(const FileName: String; var Index: Integer): Boolean;
var
  I: Integer;
begin
  Result := False;
  For I := 0 To Count - 1 Do
    If FileName = FileInfo[I].FileName Then
    begin
      Result:= True;
      Index:= I;
    end;
end;

function TFilesList.GetCount: Integer;
begin
  Result:= FData.Count;
end;

function TFilesList.GetFileInfo(Index: Integer): TFileInfo;
begin
  Result:= TFileInfo(FData.Items[Index]);
end;

function TFilesList.GetData(Index: Integer): TFileItem;
begin
  Result := TFileItem.Create(Self, TFileInfo(FData.Items[Index]));
end;

constructor TFilesList.Create(Company: Integer; FileAction: TFileAction; Draft: Boolean);
var
  FileInfo: TFileInfo;
  Table: TTable;
  Ch: Char;
  additionalObjectClass: string;
begin
  FCompany := Company;
  FDraft := Draft;
  FAction := FileAction;
  FData := TList.Create;
  Table := TTable.Create(Data);
  try
    Table.DatabaseName := Data.taFiles.DatabaseName;
    Table.TableName := Data.taFiles.TableName;
    Table.IndexName := Data.taFiles.IndexName;
    Table.Open;
    Table.Filtered := False;
    Table.Filter := '([Draft] = ''' + IntToStr(Ord(Draft)) + ''')';

    If Company <> -1 Then
      Table.Filter := Table.Filter + ' AND ([Company] = ''' + IntToStr(Company) + ''')';

    If FileAction <> faAll Then
      Table.Filter:= Table.Filter + ' AND ([ACTION] = ''' + FileActionToChar(FileAction) + ''')';

    Table.Filtered := True;
    Table.First;
    while not Table.EOF Do
    begin
      FileInfo := TFileInfo.Create;
      FileInfo.Draft := Draft;
      Ch := Table.FieldByName('Action').AsString[1];
      Byte(FileInfo.Action) := Byte(Ch) - Byte(Char('0'));
      FileInfo.CompanyNum   := Table.FieldByName('Company').AsInteger;
      FileInfo.RecNum       := Table.FieldByName('RecNum').AsInteger;
      FileInfo.FileName     := Table.FieldByName('RecName').AsString;
      //--> Change Company Name functionality improvement == Added on 06/11/2001 by Elijah Shlieserman
      //FileInfo.PreviousName := FieldByName( 'ResStr1' ).AsString;
      //<--
      FileInfo.DecisionDate := Table.FieldByName('DecisionDate').AsDateTime;
      FileInfo.notifyDate   := Table.FieldByName('notifyDate').AsDateTime;
      FileInfo.OldStyle     := Table.FieldByName('ResInt1').AsString = '';
      FileInfo.AdditionalObjectNum := Table.FieldByName('ResInt2').AsInteger;
      FileInfo.AdditionalData      := Table.FieldByName('ResStr1').AsString;
      additionalObjectClass        := Table.FieldByName('ResStr2').AsString;
      if (additionalObjectClass <> '') then
      begin
         FileInfo.AdditionalObjectClass := CharToObj(additionalObjectClass[1]);
      end;
      FData.Add(FileInfo);
      Table.Next;
    end;
    Table.Filtered := False;
  finally
    Table.Free;
  end;
end;

destructor TFilesList.Destroy;
var
  i: integer;
begin
  for i := 0 To FData.Count-1 do
  begin
    try
      if (Assigned(TFileInfo(FData.Items[i]))) then
        TFileInfo(FData.Items[i]).Free;
    except
    end;
  end;
  try
    if (Assigned(FData)) then
      FData.Free;
  except
  end;
end;

procedure TFilesList.Add(FileInfo: TFileInfo);
begin
  With Data.taFiles Do
  begin
    Append;
    FieldByName('RecName').AsString:= FileInfo.FileName;
    FieldByName('DecisionDate').AsDateTime:= FileInfo.DecisionDate;
    FieldByName('notifyDate').AsDateTime:= FileInfo.notifyDate;
    FieldByName('Draft').AsString:= IntToStr(Ord(FileInfo.Draft));
    FieldByName('Action').AsString:= FileActionToChar(FileInfo.Action);
    FieldByName('Company').AsInteger:= FileInfo.CompanyNum;
    FieldByName('ResInt1').AsInteger:= 1;
    //--> Change Company Name functionality improvement == Added on 06/11/2001 by Elijah Shlieserman
    //FieldByName( 'ResStr1' ).AsString := FileInfo.PreviousName;
    //<--

    FieldByName('ResInt2').AsInteger:= FileInfo.AdditionalObjectNum;
    FieldByName('ResStr1').AsString:= FileInfo.AdditionalData;
    FieldByName('ResStr2').AsString:= ObjToChar(FileInfo.AdditionalObjectClass);

    Post;
    FileInfo.RecNum:= FieldByName('RecNum').AsInteger;
  end;
  FData.Add(FileInfo);
end;

function TFilesList.GetLatestZip( ObjectType: TObjectClass; objectNum: integer; ShowingDate, currentActionDate: TDateTime; defaultValue: string): string;
var
  latestDate: TDateTime;
  i: integer;
  fileInfo: TFileInfo;
begin
  Result:= defaultValue;
  latestDate := EncodeDate(1900, 01, 01);

  for i := 0 to Self.Count - 1 do
  begin
    fileInfo :=  Self.GetFileInfo(i);
    if (fileInfo.Action = faZipCode) and
       (fileInfo.AdditionalObjectClass = ObjectType) and
       (fileInfo.AdditionalObjectNum = objectNum ) and
       (fileInfo.DecisionDate <= showingDate) and
       (fileInfo.DecisionDate >= latestDate) and
       (fileInfo.DecisionDate >= currentActionDate)
       then
    begin
      Result:= fileInfo.AdditionalData;
      latestDate := fileInfo.DecisionDate;
    end;
  end;
end;

class function TFilesList.GetLatestActionDate(company: TCompany; fileAction: TFileAction; Draft: boolean;objectClass:TObjectClass; objectNum: integer; action:TAction): TDateTime;
var
   filesList: TFilesList;
   i, latestFileNum: integer;
   changes: TChanges;
begin
  Result := 0;
  filesList := nil;
  changes := nil;
  try
   filesList := TFilesList.Create( company.RecNum ,fileAction, Draft);
   changes := TChanges.Create(company);
   latestFileNum := changes.GetLatestChangeFileNumber(objectClass, objectNum,action);

   if ( latestFileNum > 0) then
   begin
     for i := 0 to filesList.Count-1 do
     begin
        if (filesList.Items[i].FileInfo.RecNum = latestFileNum)
           and (result < filesList.Items[i].FileInfo.DecisionDate) then
        begin
          result := filesList.Items[i].FileInfo.DecisionDate;
        end;
     end;
   end;
  finally
  if (Assigned(filesList)) then
    filesList.Free();
  if (Assigned(changes)) then
    changes.Free();
  end;
end;

// THistory
//-------------------------------------------------
function THistory.EnumBlanks(CompanyNum: Integer; EnumProc: TBlankEnumProc): Integer;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  I: Integer;
begin
  FilesList := TFilesList.Create(CompanyNum, faAll, False);
  Result := FilesList.Count;
  try
    for I := 0 To FilesList.Count - 1 do
    begin
       FileItem := FilesList.Items[I];
      try
        If not EnumProc(FileItem) then
          Break;
      finally
        FileItem.Free;
      end;
    end;
  finally
    FilesList.Free;
  end;
end;

procedure THistory.UndoLastBlank(CompanyNum: Integer; FileType: TFileAction);
var
  FilesList: TFilesList;
  FileItem: TFileItem;
begin
  FilesList := TFilesList.Create(CompanyNum, FileType, False);
  try
    if FilesList.Count > 0 then
    begin
      FileItem := FilesList.Items[FilesList.Count - 1];
      fmMain.UndoFile(FileItem);
    end;
  finally
    FilesList.Free;
  end;
end;

procedure THistory.UndoFile(FileItem: TFileItem);
begin
   fmMain.UndoFile(FileItem);
end;

var
  LastAction: Integer;

function THistory.IsActionLastEx(CompanyNum: Integer; var FileItem: TFileItem): Boolean;
var
  FilesList: TFilesList;
  TempItem: TFileItem;
  I: Integer;
begin
  if (FileItem.FileInfo.Action = faZipCode)  then
  begin
    Result:= IsZipActionLast(FileItem.FileInfo.CompanyNum, FileItem.FileInfo.AdditionalObjectClass,FileItem.FileInfo.AdditionalObjectNum,  FileItem.FileInfo.DecisionDate);
  end
  else
  begin
    If FileItem.FileInfo.Action in MultiActions Then
      Result:= IsActionLast(FileItem.FileInfo.CompanyNum, FileItem.FileInfo.Action, FileItem.FileInfo.DecisionDate)
    else
      Result:= IsActionLast(FileItem.FileInfo.CompanyNum, FileItem.FileInfo.Action, FileItem.FileInfo.DecisionDate+1);

    If Result Then
    begin
      FilesList := TFilesList.Create(CompanyNum, faAll, FileItem.FileInfo.Draft);
      For I := 0 to FilesList.Count - 1 do
      begin
        TempItem:= FilesList.Items[I];
        If (FileItem.FileInfo.DecisionDate = TempItem.FileInfo.DecisionDate) and    // If there is any file at the same day
           CompareActions(FileItem.FFileInfo.Action, TempItem.FileInfo.Action) and  // That has an influence on the file that being deleted
           (FileItem.FileInfo.RecNum < TempItem.FileInfo.RecNum) Then               // And was saved latter than the deleted file
        begin
          Result := False;
          LastAction := Max(LastAction, TempItem.FileInfo.RecNum);
        end;
        TempItem.Free;
      end;
      FilesList.Free;
    end;
  end;
  Result:= Result or (FileItem.FileInfo.RecNum = LastAction);

  If not Result Then
  begin
    FilesList:= TFilesList.Create(CompanyNum, faAll, FileItem.FileInfo.Draft);
    FileItem.Free;
    FileItem:= nil;
    For I:= 0 To FilesList.Count-1 do
    begin
      FileItem:= FilesList.Items[I];
      If (FileItem.FileInfo.RecNum = LastAction) Then
      begin
        If IsActionLastEx(CompanyNum, FileItem) Then
          FileItem:= FilesList.Items[I];
        Break;
      end
      else
      begin
        FileItem.Free;
        FileItem:= nil;
      end;
    end;
    FilesList.Free;
  end;
end;

function THistory.IsActionLast(CompanyNum: Integer; Action: TFileAction; Date: TDateTime): Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  I: Integer;
begin
  LastAction := -1;
  Result := True;
  FilesList := TFilesList.Create(CompanyNum, faAll, False);
  try
    for I := 0 To FilesList.Count - 1 do
    begin
      FileItem:= FilesList.Items[I];
      If CompareDates(FileItem.FFileInfo, Date, Action) and CompareActions(Action, FileItem.FileInfo.Action) then
      begin
        Result:= False;
        LastAction:= Max(LastAction, FileItem.FileInfo.RecNum);
      end;
      FileItem.Free;
    end;
  finally
    FilesList.Free;
  end;
end;

function THistory.IsZipActionLast(CompanyNum: Integer; AdditionalObjectClass:TObjectClass; AdditionalObjectNum: integer; Date: TDateTime): Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  I: Integer;
begin
  LastAction:= -1;
  Result:= True;
  FilesList:= TFilesList.Create(CompanyNum, faZipCode, False);

  try
    for I:= 0 To FilesList.Count-1 do
    begin
      FileItem:= FilesList.Items[I];
      If CompareDates(FileItem.FFileInfo, Date, faZipCode)
         and (FileItem.FFileInfo.AdditionalObjectClass = AdditionalObjectClass)
         and (FileItem.FFileInfo.AdditionalObjectNum = AdditionalObjectNum) then
      begin
        Result:= False;
        LastAction:= Max(LastAction, FileItem.FileInfo.RecNum);
      end;
      FileItem.Free;
    end;
  finally
    FilesList.Free;
  end;
end;

function THistory.CompareDates(FileInfo: TFileInfo; Date: TDateTime; Action: TFileAction): Boolean;
begin
  If (not FileInfo.OldStyle) And (Action in MultiActions) Then
    Date:= Date + 1;
  Result:= FileInfo.DecisionDate >= Date;
end;

function THistory.CompareActions(Action1, Action2: TFileAction): Boolean;
begin
  Result:= (Action1 <> faNone) and
          ((Action1 = faNumber) or (Action2 = faDoh) or (Action1 = Action2) or (not (Action1 in MultiActions)) or
          ((Action1 in [faIncrease, faDecrease, faHakzaa]) And (Action2 in [faIncrease, faDecrease, faHakzaa, faHamara])) or
          ((Action1 in [faDirector, faDirectorInfo]) And (Action2 in [faDirector, faDirectorInfo])) or
          ((Action1 = faHamara) and (Action2 in [faIncrease, faDecrease, faHakzaa, faHakzaa, faHaavara, faStockHolderInfo, faHamara])) or
          ((Action1 in [faHakzaa, faHaavara, faStockHolderInfo]) and (Action2 in [faHakzaa, faHaavara, faStockHolderInfo, faHamara])));
end;

// DATA Merging
// -----------------------------------------------------------------------------

function CopyRecord(Source, Dest: TTable; RecNum: Integer): Integer;
var
  I: Integer;
begin
  Source.SetKey;
  Source.FieldByName('RecNum').AsInteger:= RecNum;
  Source.GotoKey;
  Dest.Append;
  for I:= 1 to Source.FieldCount-1 do
    Dest.Fields[I].Value:= Source.Fields[I].Value;
  Dest.Post;
  Result:= Dest.FieldByName('RecNum').AsInteger;
end;

procedure FileCopy(Source, Dest: TData; FileNum, OldCompanyNum, NewCompanyNum: Integer);
var
  Action: TFileAction;
  NewFileNum: Integer;
  NewObjectNum: Integer;
  Draft: Boolean;
  NewNewNum: Integer;
  SourceTable: TTable;

  function CopyDirector: Integer;
  var
    SourceTable: TTable;

    procedure SpecialMoveRecord(NewRec: Integer);
    var
      I: Integer;
    begin
       WriteChangeLog(#9#9#9#9'CopyDirector:SpecialMoveRecord in Forms: FileNum:' + IntToStr( NewFileNum)+
                      ', ObjectType:?' + ' ObjectNum:' + IntToStr(Result) + ', NewNum:'+IntToStr(NewRec));
      Dest.taForms.Append;
      For I := 1 to SourceTable.FieldCount - 1 do
        Dest.taForms.Fields[I].Value := SourceTable.Fields[I].Value;
      Dest.taForms.FieldByName('ObjectNum').AsInteger := Result;
      Dest.taForms.FieldByName('FileNum').AsInteger := NewFileNum;
      Dest.taForms.FieldByName('NewNum').AsInteger := NewRec;
      Dest.taForms.Post;
      WriteChangeLog(#9#9#9#9'Saved: RecNum: ' + Dest.taForms.FieldByName('RecNum').AsString);
    end;

  begin
    Result := CopyRecord(Source.taDirectors, Dest.taDirectors, NewNewNum);
    SourceTable := TTable.Create(Source);
    SourceTable.DatabaseName := Source.taForms.DatabaseName;
    SourceTable.TableName := Source.taForms.TableName;
    SourceTable.IndexName := 'ixFile';
    SourceTable.Open;
    try
      SourceTable.SetRange([FileNum], [FileNum]);
      SourceTable.First;
      while not SourceTable.EOF do
      begin
        If (SourceTable.FieldByName('ObjectType').AsString[1] = ObjToChar(ocDirector)) and
           (SourceTable.FieldByName('ObjectNum').AsInteger = NewNewNum) then
          case CharToAction(SourceTable.FieldByName('Action').AsString[1]) of
            acAddress: SpecialMoveRecord(CopyRecord(Source.taDirAddress, Dest.taDirAddress, SourceTable.FieldByName('NewNum').AsInteger));
            acTaagidAddress: SpecialMoveRecord(CopyRecord(Source.taTaagid, Dest.taTaagid, SourceTable.FieldByName('NewNum').AsInteger));
          else
            raise Exception.Create('Unknown Action at CopyDirector');
          end;
        SourceTable.Next;
      end;
    finally
      SourceTable.Close;
      SourceTable.Free;
    end;
  end;

  function CopyStockHolder: Integer;
  var
    SourceTable: TTable;

    procedure SpecialMoveRecord(NewRec: Integer);
    var
      I: Integer;
    begin
       WriteChangeLog(#9#9#9#9'CopyDirector: SpecialMoveRecord in Forms: FileNum:' + IntToStr( NewFileNum) + ', ObjectType:?' +
                     ', ObjectNum:' + IntToStr(Result) + ', NewNum:' + IntToStr(NewRec));
      Dest.taForms.Append;
      For I:= 1 to SourceTable.FieldCount-1 do
        Dest.taForms.Fields[I].Value:= SourceTable.Fields[I].Value;
      Dest.taForms.FieldByName('ObjectNum').AsInteger:= Result;
      Dest.taForms.FieldByName('FileNum').AsInteger:= NewFileNum;
      Dest.taForms.FieldByName('NewNum').AsInteger:= NewRec;
      Dest.taForms.Post;
      WriteChangeLog(#9#9#9#9'Saved: RecNum: ' + Dest.taForms.FieldByName('RecNum').AsString);
    end;

  begin
    Result:= CopyRecord(Source.taStockHolders, Dest.taStockHolders, NewNewNum);
    SourceTable:= TTable.Create(Source);
    SourceTable.DatabaseName:= Source.taForms.DatabaseName;
    SourceTable.TableName:= Source.taForms.TableName;
    SourceTable.IndexName:='ixFile';
    SourceTable.Open;
    try
      SourceTable.SetRange([FileNum], [FileNum]);
      SourceTable.First;
      while not SourceTable.EOF do
      begin
        If (SourceTable.FieldByName('ObjectType').AsString[1] = ObjToChar(ocStockHolder)) and
           (SourceTable.FieldByName('ObjectNum').AsInteger = NewNewNum) then
          case CharToAction(SourceTable.FieldByName('Action').AsString[1]) of
            acAddress:  SpecialMoveRecord(CopyRecord(Source.taStockAddress, Dest.taStockAddress, SourceTable.FieldByName('NewNum').AsInteger));
            acNewStock: SpecialMoveRecord(CopyRecord(Source.taStockNum, Dest.taStockNum, SourceTable.FieldByName('NewNum').AsInteger));
          else
            raise Exception.Create('Unknown Action at CopyStockHolder');
          end;
        SourceTable.Next;
      end;
    finally
      SourceTable.Close;
      SourceTable.Free;
    end;
  end;

  function FindNewStockHolder(RecNum: Integer): Integer;
  begin
    Result:= -1;
    Source.taStockHolders.SetKey;
    Source.taStockHolders.FieldByName('RecNum').AsInteger:= RecNum;
    If not Source.taStockHolders.GotoKey Then
      raise Exception.Create('Record not found at FindNewStockHolder');
    Source.taChanges.SetRange([ObjToChar(ocCompany), OldCompanyNum, ActionToChar(acNewStockHolder), EncodeDate(1900, 01,01)], [ObjToChar(ocCompany), OldCompanyNum, ActionToChar(acNewStockHolder), EncodeDate(3000, 12, 31)]);
    while not Source.taChanges.EOF do
    begin
      If Source.taChanges.FieldByName('NewNum').AsInteger= RecNum then
      begin
        Dest.taChanges.SetRange([ObjToChar(ocCompany),NewCompanyNum, ActionToChar(acNewStockHolder), Source.taChanges.FieldByName('Date').AsDateTime],[ObjToChar(ocCompany),NewCompanyNum, ActionToChar(acNewStockHolder), Source.taChanges.FieldByName('Date').AsDateTime]);
        while not Dest.taChanges.EOF do
        begin
          Dest.taStockHolders.SetKey;
          Dest.taStockHolders.FieldByName('RecNum').AsInteger:= Dest.taChanges.FieldByName('NewNum').AsInteger;
          Dest.taStockHolders.GotoKey;
          If Dest.taStockHolders.FieldByName('Zeut').AsString = Source.taStockHolders.FieldByName('Zeut').AsString Then
          begin
            Result:= Dest.taStockHolders.FieldByName('RecNum').AsInteger;
            Break;
          end;
          Dest.taChanges.Next;
        end;
        Dest.taChanges.CancelRange;
        Break;
      end;
      Source.taChanges.Next;
    end;
    Source.taChanges.CancelRange;
    If Result = -1 Then
      raise Exception.Create('Can''t find StockHolder at FindNewStockHolder');
  end;

  function FindNewDirector(RecNum: Integer): Integer;
  begin
    Result:= -1;
    Source.taDirectors.SetKey;
    Source.taDirectors.FieldByName('RecNum').AsInteger:= RecNum;
    If not Source.taDirectors.GotoKey Then
      raise Exception.Create('Record not found at FindNewDirector');
    Source.taChanges.SetRange([ObjToChar(ocCompany), OldCompanyNum, ActionToChar(acNewDirector), EncodeDate(1900, 01,01)], [ObjToChar(ocCompany), OldCompanyNum, ActionToChar(acNewDirector), EncodeDate(3000, 12, 31)]);
    while not Source.taChanges.EOF do
    begin
      If Source.taChanges.FieldByName('NewNum').AsInteger= RecNum then
      begin
        Dest.taChanges.SetRange([ObjToChar(ocCompany),NewCompanyNum, ActionToChar(acNewDirector), Source.taChanges.FieldByName('Date').AsDateTime],[ObjToChar(ocCompany),NewCompanyNum, ActionToChar(acNewDirector), Source.taChanges.FieldByName('Date').AsDateTime]);
        while not Dest.taChanges.EOF do
        begin
          Dest.taDirectors.SetKey;
          Dest.taDirectors.FieldByName('RecNum').AsInteger:= Dest.taChanges.FieldByName('NewNum').AsInteger;
          Dest.taDirectors.GotoKey;
          If Dest.taDirectors.FieldByName('Zeut').AsString = Source.taDirectors.FieldByName('Zeut').AsString Then
          begin
            Result:= Dest.taDirectors.FieldByName('RecNum').AsInteger;
            Break;
          end;
          Dest.taChanges.Next;
        end;
        Dest.taChanges.CancelRange;
        Break;
      end;
      Source.taChanges.Next;
    end;
    Source.taChanges.CancelRange;
    If Result = -1 Then
      raise Exception.Create('Can''t find Director at FindNewDirector');
  end;

  procedure MoveRecord;
  var
    I: Integer;
  begin
    WriteChangeLog(#9#9#9#9'MoveRecord in Forms: FileNum:' + IntToStr(NewFileNum) + ', ObjectType:?'+
                     ', ObjectNum:' + IntToStr(NewObjectNum) + ', NewNum:' + IntToStr(NewNewNum));
    Dest.taForms.Append;
    For I:= 1 to Source.taForms.FieldCount-1 do
      Dest.taForms.Fields[I].Value:= Source.taForms.Fields[I].Value;
    Dest.taForms.FieldByName('ObjectNum').AsInteger:= NewObjectNum;
    Dest.taForms.FieldByName('FileNum').AsInteger:= NewFileNum;
    Dest.taForms.FieldByName('NewNum').AsInteger:= NewNewNum;
    Dest.taForms.Post;
     WriteChangeLog(#9#9#9#9'Saved: RecNum: ' + Dest.taForms.FieldByName('RecNum').AsString);
  end;

begin
  SourceTable:= TTable.Create(Source);
  SourceTable.DatabaseName:= Source.taFiles.DatabaseName;
  SourceTable.TableName:= Source.taFiles.TableName;
  SourceTable.IndexName:= '';
  SourceTable.Open;
  try
    NewFileNum:= CopyRecord(SourceTable, Dest.taFiles, FileNum);
    Dest.taFiles.Edit;
    If Dest.taFiles.FieldByName('Company').AsInteger = OldCompanyNum Then
      Dest.taFiles.FieldByName('Company').AsInteger := NewCompanyNum;
    Dest.taFiles.Post;
    Action := CharToFileAction(SourceTable.FieldByName('Action').AsString[1]);
    Source.taForms.SetRange([FileNum], [FileNum]);
    Source.taForms.First;
    NewObjectNum := NewCompanyNum;
    Draft := Dest.taFiles.FieldByName('Draft').AsInteger = 1;
    Case Action of
      faAddingNew,
      faSignName,
      faEngName,
      faManager,
      faRishum:
        while not Source.taForms.EOF do
        begin
          If CharToObj(Source.taForms.FieldByName('ObjectType').AsString[1]) = ocCompany Then
          begin
            NewNewNum:= Source.taForms.FieldByName('NewNum').AsInteger;
            case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
              acAddress: NewNewNum:= CopyRecord(Source.taAddress, Dest.taAddress, NewNewNum);
              acNewManager: NewNewNum:= CopyRecord(Source.taManager, Dest.taManager, NewNewNum);
              acSignName: NewNewNum:= CopyRecord(Source.taSigName, Dest.taSigName, NewNewNum);
              acNewHon: NewNewNum:= CopyRecord(Source.taHon, Dest.taHon, NewNewNum);
              acNewMagish: NewNewNum:= CopyRecord(Source.taMagish, Dest.taMagish, NewNewNum);
              acNewMuhaz: NewNewNum:= CopyRecord(Source.taMuhaz, Dest.taMuhaz, NewNewNum);
              acNewDirector: NewNewNum:= CopyDirector;
              acNewStockHolder: NewNewNum:= CopyStockHolder;
            else
              raise Exception.Create('Unknown Action in faRishum at FileCopy');
            end;
            MoveRecord;
          end;
          Source.taForms.Next;
        end;

      faNumber:   ; // MoveRecord;
      faIncrease,
      faDecrease: while not Source.taForms.EOF do
                  begin
                    NewNewNum:= Source.taForms.FieldByName('NewNum').AsInteger;
                    case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                      acNewHon: NewNewNum:= CopyRecord(Source.taHon, Dest.taHon, NewNewNum);
                    else
                      raise Exception.Create('Unknown Action in faIncrease at FileCopy');
                    end;
                    MoveRecord;
                    Source.taForms.Next;
                  end;

      faHakzaa:   while not Source.taForms.EOF do
                  begin
                    NewNewNum:= Source.taForms.FieldByName('NewNum').AsInteger;
                    If CharToObj(Source.taForms.FieldByName('ObjectType').AsString[1]) = ocCompany Then
                    begin
                      case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                        acNewStockHolder: NewNewNum:= CopyStockHolder;
                        acNewHon: NewNewNum:= CopyRecord(Source.taHon, Dest.taHon, NewNewNum);
                      else
                        raise Exception.Create('Unknown Action in faHakzaa at FileCopy');
                      end;
                      MoveRecord;
                    end;
                    Source.taForms.Next;
                  end;

      faHaavara:  while not Source.taForms.EOF do
                  begin
                    NewNewNum:= Source.taForms.FieldByName('NewNum').AsInteger;
                    If CharToObj(Source.taForms.FieldByName('ObjectType').AsString[1]) = ocCompany Then
                      case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                        acNewStockHolder: begin
                                            NewNewNum:= CopyStockHolder;
                                            MoveRecord;
                                          end;
                      else
                        raise Exception.Create('Unknown Action in faHaavara at FileCopy');
                      end
                    else
                      If CharToObj(Source.taForms.FieldByName('ObjectType').AsString[1]) = ocStockHolder Then
                        case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                          acDelete: begin
                                      NewNewNum:= FindNewStockHolder(NewNewNum);
                                      NewObjectNum:= -1;
                                      MoveRecord;
                                      NewObjectNum:= NewCompanyNum;
                                    end;
                        end;
                    Source.taForms.Next;
                  end;

      faDirector: while not Source.taForms.EOF do
                  begin
                    NewNewNum:= Source.taForms.FieldByName('NewNum').AsInteger;
                    If CharToObj(Source.taForms.FieldByName('ObjectType').AsString[1]) = ocCompany Then
                      case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                        acNewDirector: begin
                                         NewNewNum:= CopyDirector;
                                         MoveRecord;
                                       end;
                      else
                        raise Exception.Create('Unknown Action in faDirector at FileCopy');
                      end
                    else If CharToObj(Source.taForms.FieldByName('ObjectType').AsString[1]) = ocDirector Then
                      case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                        acDelete: begin
                                    NewNewNum:= FindNewDirector(NewNewNum);
                                    NewObjectNum:= -1;
                                    MoveRecord;
                                    NewObjectNum:= NewCompanyNum;
                                  end;
                      end
                    else If CharToObj(Source.taForms.FieldByName('ObjectType').AsString[1]) = ocStock Then
                    begin
                      NewObjectNum:= -2;
                      NewNewNum:= 1;
                      MoveRecord;
                      NewObjectNum:= NewCompanyNum;
                    end;
                    Source.taForms.Next;
                  end;

      faAddress:  while not Source.taForms.EOF do
                  begin
                    NewNewNum:= Source.taForms.FieldByName('NewNum').AsInteger;
                    case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                      acAddress: NewNewNum:= CopyRecord(Source.taAddress, Dest.taAddress, NewNewNum);
                    else
                      raise Exception.Create('Unknown Action in faAddress at FileCopy');
                    end;
                    MoveRecord;
                    Source.taForms.Next;
                  end;

      faDoh:      while not Source.taForms.EOF do
                  begin
                    NewNewNum:= Source.taForms.FieldByName('NewNum').AsInteger;
                    case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                      acDelete: NewNewNum:= CopyRecord(Source.taDoh, Dest.taDoh, NewNewNum);
                    else
                      raise Exception.Create('Unknown Action in faDoh at FileCopy');
                    end;
                    MoveRecord;
                    Source.taForms.Next;
                  end;

      faStockHolderInfo:
                  while not Source.taForms.EOF do
                  begin
                    NewNewNum:= Source.taForms.FieldByName('NewNum').AsInteger;
                    case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                      acAddress: begin
                                   NewObjectNum:= FindNewStockHolder(Source.taForms.FieldByName('ObjectNum').AsInteger);
                                   NewNewNum:= CopyRecord(Source.taStockAddress, Dest.taStockAddress, NewNewNum);
                                   MoveRecord;
                                 end;
                      acNewName,
                      acNewZeut: begin
                                   NewObjectNum:= -1;
                                   NewNewNum:= -1;
                                   MoveRecord;
                                   Source.taForms.Prior;
                                   NewNewNum:= FindNewStockHolder(Source.taForms.FieldByName('ObjectNum').AsInteger);
                                   Source.taForms.Next;
                                   If not Draft then
                                   begin
                                     Dest.taChanges.Filter:= '([ObjectType] = '+QuotedStr(ObjToChar(ocStockHolder))+') and ([Action] = '+QuotedStr(Source.taForms.FieldByName('Action').AsString[1])+') and ([ResInt3] = '+IntToStr(FileNum)+')';
                                     Dest.taChanges.Filtered:= True;
                                     If Dest.taChanges.RecordCount <> 1 then
                                       raise Exception.Create('Record count error in faStockHolderInfo.acNewName or acNewZeut at FileCopy');
                                     Dest.taChanges.Edit;
                                     Dest.taChanges.FieldByName('ObjectNum').AsInteger:= NewNewNum;
                                     Dest.taChanges.FieldByName('NewNum').AsInteger:= Dest.taForms.FieldByName('RecNum').AsInteger;
                                     Dest.taChanges.Post;
                                     Dest.taChanges.Filtered:= False;
                                   end;
                                 end;
                    else
                      raise Exception.Create('Unknown Action in faStockHolderInfo at FileCopy');
                    end;
                    NewObjectNum:= NewCompanyNum;
                    Source.taForms.Next;
                  end;

      faDirectorInfo:
                  while not Source.taForms.EOF do
                  begin
                    NewNewNum:= Source.taForms.FieldByName('NewNum').AsInteger;
                    case CharToAction(Source.taForms.FieldByName('Action').AsString[1]) of
                      acAddress: begin
                                   NewObjectNum:= FindNewDirector(Source.taForms.FieldByName('ObjectNum').AsInteger);
                                   NewNewNum:= CopyRecord(Source.taDirAddress, Dest.taDirAddress, NewNewNum);
                                   MoveRecord;
                                 end;
                      acNewName,
                      acNewZeut: begin
                                   NewObjectNum:= -1;
                                   NewNewNum:= -1;
                                   MoveRecord;
                                   Source.taForms.Prior;
                                   NewNewNum:= FindNewDirector(Source.taForms.FieldByName('ObjectNum').AsInteger);
                                   Source.taForms.Next;
                                   If not Draft then
                                   begin
                                     Dest.taChanges.Filter:= '([ObjectType] = '+QuotedStr(ObjToChar(ocDirector))+') and ([Action] = '+QuotedStr(Source.taForms.FieldByName('Action').AsString[1])+') and ([ResInt3] = '+IntToStr(FileNum)+')';
                                     Dest.taChanges.Filtered:= True;
                                     If Dest.taChanges.RecordCount <> 1 then
                                       raise Exception.Create('Record count error in faDirectorInfo.acNewName or acNewZeut at FileCopy');
                                     Dest.taChanges.Edit;
                                     Dest.taChanges.FieldByName('ObjectNum').AsInteger:= NewNewNum;
                                     Dest.taChanges.FieldByName('NewNum').AsInteger:= Dest.taForms.FieldByName('RecNum').AsInteger;
                                     Dest.taChanges.Post;
                                     Dest.taChanges.Filtered:= False;
                                   end;
                                 end;
                    else
                      raise Exception.Create('Unknown Action in faDirectorInfo at FileCopy');
                    end;
                    NewObjectNum:= NewCompanyNum;
                    Source.taForms.Next;
                  end;

      faCompanyName,
      faZipcode: ;
    else
      raise Exception.Create('Unknown FileAction at FileCopy');
    end;

    If not Draft then
    begin
      Dest.taChanges.First;
      while not Dest.taChanges.EOF do
      begin
        If Dest.taChanges.FieldByName('ResInt3').AsInteger = FileNum Then
        begin
          Dest.taChanges.Edit;
          Dest.taChanges.FieldByName('ResInt1').AsInteger:= NewFileNum;
          Dest.taChanges.FieldByName('ResInt3').Value:= Null;
          Dest.taChanges.Post;
        end;
        Dest.taChanges.Next;
      end;
    end;
  finally
    SourceTable.Close;
    SourceTable.Free;
  end;
end;

procedure FilesCopy(Source, Dest: TData; CompanyNum, NewCompanyNum: Integer; const CompanyName: String);
var
  SourceTable: TTable;
begin
  SourceTable:= TTable.Create(Source);
  SourceTable.DatabaseName:= Source.taFiles.DatabaseName;
  SourceTable.TableName:= Source.taFiles.TableName;
  SourceTable.IndexName:= 'ixFile';
  SourceTable.Open;
  try
    SourceTable.Filter:= '([RecName] = '+QuotedStr(CompanyName)+') and ([Action] = ''0'')';
    SourceTable.Filtered:= True;
    SourceTable.First;
    while not SourceTable.EOF do
    begin
      FileCopy(Source, Dest, SourceTable.FieldByName('RecNum').AsInteger, CompanyNum, NewCompanyNum);
      SourceTable.Next;
    end;
    SourceTable.Filtered:= False;

    SourceTable.SetRange(['0', CompanyNum, #32, EncodeDate(1900, 01, 01)], ['0', CompanyNum, #127, EncodeDate(3000, 12, 31)]);
    SourceTable.First;
    while not SourceTable.EOF do
    begin
      FileCopy(Source, Dest, SourceTable.FieldByName('RecNum').AsInteger, CompanyNum, NewCompanyNum);
      SourceTable.Next;
    end;

    SourceTable.SetRange(['1', CompanyNum, #32, EncodeDate(1900, 01, 01)], ['1', CompanyNum, #127, EncodeDate(3000, 12, 31)]);
    SourceTable.First;
    while not SourceTable.EOF do
    begin
      FileCopy(Source, Dest, SourceTable.FieldByName('RecNum').AsInteger, CompanyNum, NewCompanyNum);
      SourceTable.Next;
    end;

  finally
    SourceTable.Close;
    SourceTable.Free;
  end;
end;

procedure ChangesCopy(Source, Dest: TData; ObjectClass: TObjectClass; ObjectNum, NewObjectNum: Integer);
var
  SourceTable: TTable;
  Action: TAction;
  NewNum: Integer;

  procedure MoveRecord;
  var
    I: Integer;
  begin
    Dest.taChanges.Append;
    For I := 1 to SourceTable.FieldCount - 1 do
      Dest.taChanges.Fields[I].Value:= SourceTable.Fields[I].Value;
    Dest.taChanges.FieldByName('ObjectNum').AsInteger:= NewObjectNum;
    Dest.taChanges.FieldByName('ResInt3').Value:= SourceTable.FieldByName('ResInt1').Value;
  end;

begin
  SourceTable:= TTable.Create(Source);
  SourceTable.DatabaseName:= Source.taChanges.DatabaseName;
  SourceTable.TableName:= Source.taChanges.TableName;
  SourceTable.IndexName:= 'ixHistory';
  SourceTable.Open;
  try
    SourceTable.SetRange([ObjToChar(ObjectClass), ObjectNum, #32, EncodeDate(1900, 01, 01)], [ObjToChar(ObjectClass), ObjectNum, #127, EncodeDate(3000, 12,31)]);
    SourceTable.First;
    while not SourceTable.EOF do
    begin
      Action:= CharToAction(SourceTable.FieldByName('Action').AsString[1]);
      NewNum:= SourceTable.FieldByName('NewNum').AsInteger;
      case ObjectClass of
        ocCompany: case Action of
                     acAddress:    begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taAddress, Dest.taAddress, NewNum);
                                      Dest.taChanges.Post;
                                    end;
                     acNewDirector: begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taDirectors, Dest.taDirectors, NewNum);
                                      Dest.taChanges.Post;
                                      ChangesCopy(Source, Dest, ocDirector, NewNum, Dest.taChanges.FieldByName('NewNum').AsInteger);
                                    end;
                     acNewManager:  begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taManager, Dest.taManager, NewNum);
                                      Dest.taChanges.Post;
                                    end;
                     acNewStockHolder:
                                    begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taStockHolders, Dest.taStockHolders, NewNum);
                                      Dest.taChanges.Post;
                                      ChangesCopy(Source, Dest, ocStockHolder, NewNum, Dest.taChanges.FieldByName('NewNum').AsInteger);
                                    end;
                     acSignName:    begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taSigName, Dest.taSigName, NewNum);
                                      Dest.taChanges.Post;
                                    end;
                     acNewHon:      begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taHon, Dest.taHon, NewNum);
                                      Dest.taChanges.Post;
                                    end;
                     acNewMagish:   begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taMagish, Dest.taMagish, NewNum);
                                      Dest.taChanges.Post;
                                    end;
                     acNewMuhaz:    begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taMuhaz, Dest.taMuhaz, NewNum);
                                      Dest.taChanges.Post;
                                    end;
                   else
                     raise Exception.Create('Unknown Action in ocCompany at ChangesCopy');
                   end;
        ocHon:   case Action of
                   acDelete:        begin
                                      MoveRecord;
                                      Dest.taChanges.Post;
                                    end;
                 else
                   raise Exception.Create('Unknown Action in ocHon at ChangesCopy');
                 end;
        ocDirector: case Action of
                      acAddress:    begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taDirAddress, Dest.taDirAddress, NewNum);
                                      Dest.taChanges.Post;
                                    end;
                      acTaagidAddress:
                                    begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taTaagid, Dest.taTaagid, NewNum);
                                      Dest.taChanges.Post;
                                    end;
                      acDelete:     begin
                                      MoveRecord;
                                      Dest.taChanges.Post;
                                    end;
                      acNewName:    begin
                                      MoveRecord;
                                      Dest.taChanges.Post;
                                      // Don't forget to copy the FileItem data too
                                    end;
                      acNewZeut:    begin
                                      MoveRecord;
                                      Dest.taChanges.Post;
                                      // Don't forget to copy the FileItem data too
                                    end;

                    else
                      raise Exception.Create('Unknown Action in ocDirector at ChangesCopy');
                    end;
        ocStockHolder: case Action of
                         acAddress: begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taStockAddress, Dest.taStockAddress, NewNum);
                                      Dest.taChanges.Post;
                                    end;
                         acNewStock:
                                    begin
                                      MoveRecord;
                                      Dest.taChanges.FieldByName('NewNum').AsInteger:= CopyRecord(Source.taStockNum, Dest.taStockNum, NewNum);
                                      Dest.taChanges.Post;
                                      ChangesCopy(Source, Dest, ocStock, NewNum, Dest.taChanges.FieldByName('NewNum').AsInteger);
                                    end;
                         acDelete:  begin
                                      MoveRecord;
                                      Dest.taChanges.Post;
                                    end;
                         acNewName: begin
                                      MoveRecord;
                                      Dest.taChanges.Post;
                                      // Don't forget to copy the FileItem data too
                                    end;
                         acNewZeut: begin
                                      MoveRecord;
                                      Dest.taChanges.Post;
                                      // Don't forget to copy the FileItem data too
                                    end;

                       else
                         raise Exception.Create('Unknown Action in ocStockHolder at ChangesCopy');
                       end;
        ocStock: case Action of
                   acDelete:        begin
                                      MoveRecord;
                                      Dest.taChanges.Post;
                                    end;
                 else
                   raise Exception.Create('Unknown Action in ocStock at ChangesCopy');
                 end;
        ocMuhaz: case Action of
                   acDelete:        begin
                                      MoveRecord;
                                      Dest.taChanges.Post;
                                    end;
                 else
                   raise Exception.Create('Unknown Action in ocMuhaz at ChangesCopy');
                 end;
      else
        raise Exception.Create('Unknown Object at ChangesCopy');
      end;
      SourceTable.Next;
    end;
  finally
    SourceTable.Close;
    SourceTable.Free;
  end;
end;

function CopyCompany(Source, Dest: TData; RecNum: Integer; const CompanyName: String): boolean;
var
  CompanyRecNum: Integer;
begin
  Result := True;
  try
    CompanyRecNum:= CopyRecord(Source.taName, Dest.taName, RecNum);
    ChangesCopy(Source, Dest, ocCompany, RecNum, CompanyRecNum);
    FilesCopy(Source, Dest, RecNum, CompanyRecNum, CompanyName);
  except
    result := False;
  end;
end;

{ Added on 07/03/2005 by ES - new log --> }
//==============================================================================
function ObjectToName( ObjVal : TObjectClass ) : String;
begin
  case ObjVal of
    ocCompany     : Result := 'Company';
    ocHon         : Result := 'Hon';
    ocDirector    : Result := 'Director';
    ocStockHolder : Result := 'StockHolder';
    ocStock       : Result := 'Stock';
    ocMuhaz       : Result := 'Muhaz';
    ocManager     : Result := 'Manager';
  end;
end;

//==============================================================================
function ActionToName( ActVal : TAction ) : String;
begin
  case ActVal of
    acAddress        : Result := 'Address';
    acTaagidAddress  : Result := 'TaagidAddress';
    acNewDirector    : Result := 'NewDirector';
    acNewManager     : Result := 'NewManager';
    acNewStockHolder : Result := 'NewStockHolder';
    acNewStock       : Result := 'NewStock';
    acSignName       : Result := 'SignName';
    acDelete         : Result := 'Delete';
    acNewHon         : Result := 'NewHon';
    acNewMagish      : Result := 'NewMagish';
    acNewMuhaz       : Result := 'NewMuhaz';
    acRecNum         : Result := 'RecNum';
    acNewName        : Result := 'NewName';
    acNewZeut        : Result := 'NewZeut';
    acSubmissionData : Result := 'SubmissionData';
  end;
end;

{TMultiDirList}
constructor TMultiDirList.CreateAndFill(FZeut, FFileName : string; PChangeDate: TDateTime);
begin
  inherited Create;
  Zeut := FZeut;
  FileName := FFileName;
  ChangeDate := PChangeDate;
  LoadByZeut(diDirector);
  LoadByNewZeut(diDirector);
  LoadByZeut(diStockHolder);
  LoadByNewZeut(diStockHolder);
  LoadByZeut(diManager);
  SearchForNewZeut(diManager);
  LoadByZeut(diSignName);
  if (FileName <> '') then
    FilterByFileName;
end;

//======================
procedure TMultiDirList.LoadByZeut(PersonType: TPersonType);
var
  Loaded: boolean;
  RecFound: integer;
  ObjectType : TObjectClass;
  Action: Taction;
  Table: TTable;
  CurrItem: integer;
  CompanyNum: string;
  FileNum: string;
begin
  case PersonType of
    diDirector:
    begin
      ObjectType := ocDirector;
      Table      := Data.taDirectors;
      Action     := acNewDirector;
    end;
    diStockHolder:
    begin
      ObjectType := ocStockHolder;
      Table      := Data.taStockHolders;
      Action     := acNewStockHolder;
    end;
    diManager :
    begin
      ObjectType := ocCompany;
      Table      := Data.taManager;
      Action     := acNewManager;
    end;
    diSignName:
    begin
      ObjectType := ocCompany;
      Table      := Data.taSigName;
      Action     := acSignName;
    end;
    else
      Exit;
  end;

  Loaded := (Count > 0);
  if Zeut <> '' then
    With Table do
    begin
      try
        CancelRange;
        if PersonType = diSignName then
        begin
          Filter:= '(SigZeut = ''' + Zeut + ''')'
        end
        else
          Filter:= '(Zeut = ''' + Zeut + ''')';

        Filtered:= True;
        First;
        while not EOF do
        begin
          RecFound := -1;
          CurrItem := AddNewRecord;

          PMultipleSelectRec(Items[CurrItem])^.DirectorNum := 0;
          PMultipleSelectRec(Items[CurrItem])^.StockHolderNum := 0;
          PMultipleSelectRec(Items[CurrItem])^.ManagerNum := 0;
          PMultipleSelectRec(Items[CurrItem])^.SignNameNum := 0;

          if PersonType = diDirector then
            PMultipleSelectRec(Items[CurrItem])^.DirectorNum := Table.FieldByName('RecNum').AsInteger
          else if PersonType = diStockHolder then
            PMultipleSelectRec(Items[CurrItem])^.StockHolderNum := Table.FieldByName('RecNum').AsInteger
          else if PersonType = diManager then
            PMultipleSelectRec(Items[CurrItem])^.ManagerNum := Table.FieldByName('RecNum').AsInteger
          else if PersonType = diSignName then
            PMultipleSelectRec(Items[CurrItem])^.SignNameNum := Table.FieldByName('RecNum').AsInteger;

          Data.taChanges.CancelRange;
          //first check there are no newer changes
          if (PersonType = diManager) or (PersonType = diSignName) then
          begin
            Data.taChanges.Filter := '([NewNum] = ' + Table.FieldByName('RecNum').AsString + ') AND ' +
              '([Action] = ''' + ActionToChar(Action) + ''')';

            Data.taChanges.Filtered :=True;
            if (Data.taChanges.RecordCount > 0) then
            begin
              CompanyNum := Data.taChanges.FieldByName('ObjectNum').AsString;
              Data.taChanges.Filtered := False;
              Data.taChanges.Filter := '([ObjectNum] = ' + CompanyNum +') AND ' +
                '([ObjectType] = ''' + ObjToChar(ObjectType) + ''') AND ' +
                '([Date] > ''' + DateToLongStr(ChangeDate) + ''')';
               CompanyNum := '';
            end
          end
          else
            Data.taChanges.Filter := '([ObjectNum] = '+ Table.FieldByName('RecNum').AsString+' ) AND ([ObjectType] ='''+ObjToChar(ObjectType)+''' ) AND ([Date] > '''+ DateToLongStr(ChangeDate) +''' ) ';
          Data.taChanges.Filtered := True;

          if (Data.taChanges.RecordCount = 0) then
          begin
            Data.taChanges.Filtered := False;
            if (PersonType = diManager) or (PersonType = diSignName) then
            begin
              Data.taChanges.Filter := '([NewNum] = ' + Table.FieldByName('RecNum').AsString + ') AND ' +
                '([Action] = ''' + ActionToChar(Action) + ''') AND ' +
                '([Date] <= ''' + DateToLongStr(ChangeDate) + ''')'
           end else
             Data.taChanges.Filter := '([ObjectNum] = ' + Table.FieldByName('RecNum').AsString + ') AND ' +
              '([ObjectType] = ''' + ObjToChar(ObjectType) + ''') AND ' +
              '([Date] <= ''' + DateToLongStr(ChangeDate) + ''')';

           Data.taChanges.Filtered := True;
           Data.taChanges.First;
           if (PersonType = diManager) or (PersonType = diSignName) then
             CompanyNum := Data.taChanges.FieldByName('ObjectNum').AsString
           else
             CompanyNum := Data.taChanges.FieldByName('ResInt1').AsString;
           FileNum := Data.taChanges.FieldByName('ResInt1').AsString;

           if (PersonType = diSignName) AND (CompanyNum <> '') then
           begin
             Data.taChanges.Filter := '([ObjectNum] = ' + CompanyNum + ') AND ' +
              '([NewNum] <> ' + Table.FieldByName('RecNum').AsString + ') AND ' +
              '([Action] = ''' + ActionToChar(Action) + ''') AND ' +
              '([Date] > ''' + Data.taChanges.FieldByName('Date').AsString + ''' )';
             Data.taChanges.Filtered := True;

             if (Data.taChanges.RecordCount > 0) then
               CompanyNum := '';
           end;

           if (CompanyNum = '') then
           begin
             Next;
             DeleteRecord(CurrItem);
             Continue;
           end;

           Data.taFiles.CancelRange;
           Data.taFiles.Filter := '[RecNum] = ' + FileNum; //(Data.taChanges.FieldByName('ResInt1').AsString);
           Data.taFiles.Filtered := True;
           Data.taFiles.First;
           PMultipleSelectRec(Items[CurrItem])^.CompanyNum := Data.taFiles.FieldByName('Company').AsInteger;
           if PMultipleSelectRec(Items[CurrItem])^.CompanyNum <= 0 then
           begin
             Data.taChanges.Filter := '([NewNum] = ' + Table.FieldByName('RecNum').AsString + ') AND ' +
              '([ObjectType] = ''' + ObjToChar(ocCompany) + ''') AND ' +
              '([Action] = ''' + ActionToChar(Action) + ''') AND ' +
              '([Date] <= ''' + DateToLongStr(ChangeDate) + ''')';
             Data.taChanges.Filtered := True;
             Data.taChanges.First;
             PMultipleSelectRec(Items[CurrItem])^.CompanyNum := Data.taChanges.FieldByName('ObjectNum').AsInteger;
           end;

           if Loaded then
             RecFound := FindRecord(PMultipleSelectRec(Items[CurrItem])^.CompanyNum);

           if RecFound <> -1 then // person in other role in this company
           begin
             if PersonType = diDirector then
             begin
               PMultipleSelectRec(Items[RecFound])^.DirectorNum := PMultipleSelectRec(Items[CurrItem])^.DirectorNum;
               PMultipleSelectRec(Items[RecFound])^.PersonType := diBoth; //the assumption is no manager/sign name can exist without being a manager/ stock holder
             end
             else if PersonType = diStockHolder then
             begin
               PMultipleSelectRec(Items[RecFound])^.StockHolderNum := PMultipleSelectRec(Items[CurrItem])^.StockHolderNum;
               PMultipleSelectRec(Items[RecFound])^.PersonType := diBoth; //the assumption is no manager/sign name can exist without being a manager/ stock holder
             end
             else if PersonType = diManager then
               PMultipleSelectRec(Items[RecFound])^.ManagerNum := PMultipleSelectRec(Items[CurrItem])^.ManagerNum
             else if PersonType = diSignName then
               PMultipleSelectRec(Items[RecFound])^.SignNameNum  := PMultipleSelectRec(Items[CurrItem])^.SignNameNum;

             DeleteRecord(CurrItem);
           end
           else if (PMultipleSelectRec(Items[CurrItem])^.CompanyNum = 0) then
           begin
             DeleteRecord(CurrItem);
           end
           else
           begin
             Data.taName.Filter := '([RecNum] = '+ IntToStr(PMultipleSelectRec(Items[CurrItem])^.CompanyNum)+') AND ([ResInt1] <> 1 )';
             Data.taName.Filtered := True;
             if (Data.taName.RecordCount = 0) then
             begin
               DeleteRecord(CurrItem);
             end
             else
             begin
               Data.taName.First;
               PMultipleSelectRec(Items[CurrItem])^.CompanyName := Data.taName.FieldByName('Name').AsString;
               PMultipleSelectRec(Items[CurrItem])^.PersonType:= PersonType;
             end;
           end;
          end
          else
          begin
            DeleteRecord(CurrItem);
          end;
          Next;
        end;
      finally
        Data.taName.Filtered    := False;
        Data.taChanges.Filtered := False;
        Data.taFiles.Filtered   := False;
        Table.Filtered          := False;
      end;
    end;
end;

//======================
procedure TMultiDirList.LoadByNewZeut(PersonType: TPersonType);
var
  PersonNum: integer;
  Loaded: boolean;
  RecFound: integer;
  ObjectType : TObjectClass;
  Action: Taction;
  CurrItem: integer;
begin
  Case PersonType of
    diDirector:
    begin
      ObjectType := ocDirector;
      Action     := acNewDirector;
    end;
    diStockHolder:
    begin
      ObjectType := ocStockHolder;
      Action     := acNewStockHolder;
    end;
    else
    Exit;
  end;

  Loaded := (Count>0);

  With Data.taForms do
  begin
    try
      CancelRange;
      Filter:= '([ResStr1] = '''+Zeut+''') AND  ([ObjectType] ='''+ObjToChar(ObjectType)+''' ) AND ([Action] = '''+ActionToChar(acNewZeut)+''')';
      Filtered:= True;
      First;
      while not EOF do
      begin
        RecFound:=-1;
        CurrItem :=AddNewRecord;

        PMultipleSelectRec(Items[CurrItem])^.DirectorNum:=0;
        PMultipleSelectRec(Items[CurrItem])^.StockHolderNum:=0;
        PMultipleSelectRec(Items[CurrItem])^.ManagerNum:=0;
        PMultipleSelectRec(Items[CurrItem])^.SignNameNum :=0;

        Data.taChanges.CancelRange;
        Data.taChanges.Filter :='([ResInt1] = '+ FieldByName('FileNum').AsString+' ) AND ([NewNum] = '+FieldByName('RecNum').AsString+
                                ' ) AND ([Action] = ''' + ActionToChar(acNewZeut)+''') AND ([ObjectType] = '''+ObjToChar(ObjectType)+''')';
        Data.taChanges.Filtered :=True;
        Data.taChanges.First;

        if Data.taChanges.RecordCount =0 then
        begin
          DeleteRecord(CurrItem);
          next;
          continue;
        end;
        if PersonType = diDirector then
          PMultipleSelectRec(Items[CurrItem])^.DirectorNum:= Data.taChanges.FieldByName('ObjectNum').AsInteger
        else
          PMultipleSelectRec(Items[CurrItem])^.StockHolderNum:= Data.taChanges.FieldByName('ObjectNum').AsInteger;

        Data.taFiles.CancelRange;
        Data.taFiles.Filter := '[RecNum] = '+ (FieldByName('FileNum').AsString);
        Data.taFiles.Filtered :=True;
        Data.taFiles.First;
        PMultipleSelectRec(Items[CurrItem])^.CompanyNum :=Data.taFiles.FieldByName('Company').AsInteger;
        if PMultipleSelectRec(Items[CurrItem])^.CompanyNum <= 0 then
        begin
          Data.taChanges.Filter :='([NewNum] = '+ FieldByName('RecNum').AsString+' ) AND ([ObjectType] ='''+ObjToChar(ocCompany)+''' ) AND ([Action] = '''+ActionToChar(Action)+''')';
          Data.taChanges.Filtered :=True;
          Data.taChanges.First;
          PMultipleSelectRec(Items[CurrItem])^.CompanyNum :=Data.taChanges.FieldByName('ObjectNum').AsInteger;
        end;

        if Loaded then
          RecFound:= FindRecord(PMultipleSelectRec(Items[CurrItem])^.CompanyNum);

        if RecFound <> -1 then // company already in list
        begin
          if PersonType = diDirector then
          begin
            PersonNum:=PMultipleSelectRec(Items[RecFound])^.DirectorNum;
            PMultipleSelectRec(Items[RecFound])^.DirectorNum:= PMultipleSelectRec(Items[CurrItem])^.DirectorNum;
          end else
          begin
            PersonNum:=PMultipleSelectRec(Items[RecFound])^.StockHolderNum;
            PMultipleSelectRec(Items[RecFound])^.StockHolderNum:= PMultipleSelectRec(Items[CurrItem])^.StockHolderNum;
          end;

          if  PersonNum <=0 then
            PMultipleSelectRec(Items[RecFound])^.PersonType:= diBoth
          else
            PMultipleSelectRec(Items[RecFound])^.PersonType:= PersonType;
          DeleteRecord(CurrItem);
        end
        else if PMultipleSelectRec(Items[CurrItem])^.CompanyNum = 0 then
          DeleteRecord(CurrItem)
        else
        begin
          Data.taName.Filter := '[RecNum] = '+ IntToStr(PMultipleSelectRec(Items[CurrItem])^.CompanyNum);
          Data.taName.Filtered := True;
          Data.taName.First;
          PMultipleSelectRec(Items[CurrItem])^.CompanyName := Data.taName.FieldByName('Name').AsString;
          PMultipleSelectRec(Items[CurrItem])^.PersonType:= PersonType;
        end;
        next;
      end;
    finally
      Data.taName.Filtered     :=False;
      Data.taChanges.Filtered  :=False;
      Data.taFiles.Filtered    :=False;
      Data.taForms.Filtered    :=False;
    end;
  end;
end;

//====================== 20.6.05 Tsahi: part of adding the multi choice to undo
procedure TMultiDirList.FilterByFileName;
  //===========
  function IsInFile(var PersonRec: TMultipleSelectRec; PersonType:TPersonType): boolean;
  var
    TableName: string;
    RecNum: integer;
    ObjectType: TObjectClass;
    FileNum: string;
    ActionType: TAction;
    OldChangesFilter: string;
    WasChangesFiltered: boolean;
  begin
    Result := False;
    FileNum:='';
    Case PersonType of
      diDirector:
      begin
        TableName := data.taDirectors.TableName;
        RecNum := PersonRec.DirectorNum;
        ObjectType := ocDirector;
        ActionType := acNewDirector;
      end;
      diStockHolder:
      begin
        TableName := data.taStockHolders.TableName;
        RecNum := PersonRec.StockHolderNum;
        ObjectType := ocStockHolder;
        ActionType := acNewStockHolder;
      end;
      diManager:
      begin
        TableName :=data.taManager.TableName;
        RecNum := PersonRec.ManagerNum;
        ObjectType :=ocCompany;
        ActionType := acNewManager;
      end;
      diSignName:
      begin
        TableName := data.taSigName.TableName;
        RecNum := PersonRec.SignNameNum;
        ObjectType := ocCompany;
        ActionType := acSignName;
      end;
      else
        Exit;
    end;

    OldChangesFilter :=  Data.taChanges.Filter;
    WasChangesFiltered := Data.taChanges.Filtered;

    if PersonType = diSignName then
    begin
      // Moshe 04/04/2016 - Fix one apostrophe
      Data.taFiles.Filter:= '([RecName] = '''+FilterStr(FileName)+''' ) AND ([Draft] = 0) AND ([Company] = '+IntToStr(PersonRec.CompanyNum)+')';
      Data.taFiles.Filtered:=True;
      if Data.taFiles.RecordCount>0 then
      begin
        Data.taFiles.First;
        while not Data.taFiles.EOF do
        begin
          FileNum:= Data.taFiles.FieldByName('RecNum').AsString;
          With  Data.taChanges do
          begin
            Filtered := False;
            Filter := ' ([ObjectType] = '''+ObjToChar(ObjectType)+''' ) AND ([Action] = '''+ActionToChar(ActionType)
                    +''') AND ([ResInt1] = '+FileNum+')' ;
            Filtered := True;
            Last;
            if RecordCount >0 then
              if  RecNum<>FieldByName('NewNum').AsInteger then
              begin
                PersonRec.SignNameNum :=FieldByName('NewNum').AsInteger;
                RecNum:=PersonRec.SignNameNum;
              end;
            Filtered := False;
          end;
          Data.taFiles.next;
        end;
      end;
    end;

    With  Data.taChanges do
    begin
      Filtered := False;
      if (PersonType = diSignName) or (PersonType = diManager) then
      begin
        Filter := ' ([ObjectType] = '''+ObjToChar(ObjectType)+''' ) AND ([NewNum] = '+IntToStr(RecNum)+' ) AND ([Action] = '''+ActionToChar(ActionType)+''' ) AND ([ResInt1] <> -1)  AND ([Date] <= '''+ DateToLongStr(ChangeDate) +''' )' ;//)
      end
      else
        Filter := ' ([ObjectType] = '''+ObjToChar(ObjectType)+''' ) AND ([ObjectNum] = '+IntToStr(RecNum)+' ) AND ([ResInt1] <> -1) AND ([Date] <= '''+ DateToLongStr(ChangeDate) +''' ) ';//);
      Filtered := True;

      if RecordCount>0 then
      begin
        Last;

        while ((Result=False) and (not Data.taChanges.BOF)) do
        begin
          FileNum:=FieldByName('ResInt1').AsString;
          if FileNum<>'' then
          begin
            // Moshe 04/04/2016 - Fix one apostrophe
            Data.taFiles.Filter:= '([RecNum] = '+FileNum+') AND ([RecName] = '''+FilterStr(FileName)+''' ) AND ([Draft] = 0)';
            Data.taFiles.Filtered:=True;
            if Data.taFiles.RecordCount>0 then
              Result:=True;
            Data.taFiles.Filtered:=False;
          end;
          Prior;
        end;
      end;
      Filtered := False;
    end;
    Data.taChanges.Filter := OldChangesFilter  ;
    Data.taChanges.Filtered  :=  WasChangesFiltered  ;
  end;

  //===========
  procedure RemoveEmptyFileRecord ;
  begin
     // Moshe 04/04/2016 - Fix one apostrophe
     Data.taFiles.Filter := '([RecName] = '''+FilterStr(FileName)+''' ) AND ([Draft] = 0) AND ([Action] = ''?'')';
     Data.taFiles.Filtered := True;
     if (Data.taFiles.RecordCount =1) then
     begin
       Data.taFiles.Edit;
       Data.taFiles.FieldByName('Draft').AsString:= '1';
       Data.taFiles.Post;
     end;
     Data.taFiles.Filtered := False;
  end;
var
  i: integer;
begin
  if (Count = 0) then
  begin
    RemoveEmptyFileRecord();
  end;

  for i := Count - 1 downto 0 do
  begin
    if (PMultipleSelectRec(Items[i])^.PersonType = diBoth) or (PMultipleSelectRec(Items[i])^.PersonType =diDirector) then
      if not IsInFile(PMultipleSelectRec(Items[i])^,diDirector) then
      begin
        PMultipleSelectRec(Items[i])^.DirectorNum :=0;
        if PMultipleSelectRec(Items[i])^.PersonType= diBoth then
          PMultipleSelectRec(Items[i])^.PersonType:=diStockHolder;
      end;
    if (PMultipleSelectRec(Items[i])^.PersonType = diBoth) or (PMultipleSelectRec(Items[i])^.PersonType =diStockHolder) then
      if not IsInFile(PMultipleSelectRec(Items[i])^,diStockHolder) then
      begin
        PMultipleSelectRec(Items[i])^.StockHolderNum :=0;
        if PMultipleSelectRec(Items[i])^.PersonType= diBoth then
          PMultipleSelectRec(Items[i])^.PersonType:=diDirector;
      end;

    if  not IsInFile(PMultipleSelectRec(Items[i])^,diManager) then
      PMultipleSelectRec(Items[i])^.ManagerNum :=0; //again, the supposition is that if person is neither director nor stockholder, they can't be a manager
    if not IsInFile(PMultipleSelectRec(Items[i])^,diSignName) then
      PMultipleSelectRec(Items[i])^.SignNameNum :=0; //again, the supposition is that if person is neither director nor stockholder, they can't be a manager

    if  (PMultipleSelectRec(Items[i])^.DirectorNum =0) and  (PMultipleSelectRec(Items[i])^.StockHolderNum =0) and
        (PMultipleSelectRec(Items[i])^.ManagerNum  =0) and  (PMultipleSelectRec(Items[i])^.SignNameNum =0) then
       DeleteRecord(i);
  end;
end;

//======================
function TMultiDirList.AddNewRecord : integer;
var
  pTemp: PMultipleSelectRec;
begin
  New(pTemp);
  result:=Add(pTemp);
end;

function  TMultiDirList.FindRecord(CompanyNum : integer): integer;
var
  i: integer;
  FoundIt: boolean;
begin
  Result := -1;
  FoundIt := False;
  i := 0;
  while (not(i >= Count-1)) and (not FoundIt) Do
  begin
    if (PMultipleSelectRec(Items[i])^).CompanyNum = CompanyNum then
    begin
      FoundIt := True;
      Result := i;
    end;
    inc(i);
  end;
end;

procedure TMultiDirList.DeleteRecord (Index: integer);
begin
  Dispose(Items[Index]);
  Delete(Index);
end;

procedure TMultiDirList.FreeList;
var
  i: integer;
begin
  if Count>0 then
    for i :=0 to count-1 do
     dispose(Items[i]);
end;

//----------------
function  TMultiDirList.PersonCount(PersonType: TPersonType):integer;
var
 i: integer;
 Counter: integer;
begin
  Counter := 0;
  for i := 0 to Count-1 do
  begin
    case PersonType of
      diDirector:    if (PMultipleSelectRec(Items[i])^.DirectorNum <> 0) then
                       inc(Counter);
      diStockHolder: if (PMultipleSelectRec(Items[i])^.StockHolderNum <> 0) then
                       inc(Counter);
      diBoth:        if (PMultipleSelectRec(Items[i])^.DirectorNum <> 0)
                         or (PMultipleSelectRec(Items[i])^.StockHolderNum <> 0) then
                       inc(Counter);
      diManager:     if (PMultipleSelectRec(Items[i])^.ManagerNum <> 0) then
                       inc(Counter);
      diSignName:    if (PMultipleSelectRec(Items[i])^.SignNameNum <> 0) then
                       inc(Counter);
    end;
  end;
  Result := Counter;
end;

//-----------------
procedure TMultiDirList.SearchForNewZeut (PersonType : TPersonType);
  //-----------------
  function FindNextChange(Rec: integer):integer;
  var
    ObjType: TObjectClass;
    ActType: TAction;
    NewRec: integer;
  begin
    Result := 0;
    Case PersonType of
      diManager:
      begin
        ObjType:=ocManager;
        ActType:=acAddress;
      end;
      else
        Exit;
    end;

    NewRec:=Rec;

    With Data.taChanges do
    begin
      Close;
      Filtered :=False;
      Filter:='(ObjectNum = '+IntToStr(Rec)+' ) AND (ObjectType = '''+ObjToChar(ObjType)
               +''') AND (Action = '''+ActionToChar(ActType)+''')';
      Filtered:=True;
      Open;
      Last;
      if RecordCount >0 then //not Data.taChanges.EOF then
        NewRec:=FieldByName('NewNum').AsInteger;
      Filtered :=False;
    end;
    if NewRec<>Rec then
      NewRec:= FindNextChange(NewRec);
    Result:=NewRec;
  end;
  //-----------------
var
  i: integer;
begin
  Case Persontype of
    diManager:
    begin
      for i:=0 to Count-1 do
      begin
        if PMultipleSelectRec(Items[i])^.ManagerNum >0 then
          PMultipleSelectRec(Items[i])^.ManagerNum:=FindNextChange(PMultipleSelectRec(Items[i])^.ManagerNum );
      end;
    end;
    diSignName:
    begin
      for i:=0 to Count-1 do
      begin
        if PMultipleSelectRec(Items[i])^.SignNameNum  >0 then
          PMultipleSelectRec(Items[i])^.SignNameNum :=FindNextChange(PMultipleSelectRec(Items[i])^.SignNameNum );
      end;
    end;
  end;
end;
//-------
function PersonTostr(Pers: TPersonType):string;
begin
  case Pers of
    diDirector:   Result:='�������';
    diStockHolder:Result:='��� �����';
    diBoth:       Result:='������� ���� �����';
    diManager:    Result:='���"�';
    diSignName:   Result:='���� ����';
  else
    Result:='';
  end;
end;
//---------

//v 9.0 Feb 2011: SubmissionData
{TSubmissionData}
constructor TSubmissionData.Create(Company :TCompany);
begin
   LoadData(Company);
end;

procedure TSubmissionData.LoadEnum(ObjectNum,NewNum:integer);
begin
  FRecNum := NewNum;
  Data.taRishumSubmission.Filtered:=False;
  Data.taRishumSubmission.Filter:='RecNum='+IntToStr(FRecNum);
  Data.taRishumSubmission.Filtered:=True;
  Data.taRishumSubmission.First;
  if Data.taRishumSubmission.Eof then
  begin
    Self.FDate:=EncodeDate(1900, 12, 31);
    Self.FRepresentativeName:='';
    Self.FRepresentativeRole:='';
    Self.FRepresentativePhone:='';
    Self.FRepresentativeMail:='';
    Self.FAltHebrewName1:='';
    Self.FAltEnglishName1:='';
    Self.FAltHebrewName2:='';
    Self.FAltEnglishName2:='';
    Self.FAltHebrewName3:='';
    Self.FAltEnglishName3:='';
    Self.FGetCertificate:=False;
    Self.FEMail:='';
    Self.FFax:='';
    Self.FGetCopies:=False;
    Self.FTakanonCopies:=0;
    Self.FCertificateCopies:=0;
    Self.FPayment:=0.0;
    Self.FEzel:='';
    Self.FDocsByMail:=False;
  end
  else
  begin
    Self.FDate:=Data.taRishumSubmission.FieldByName('Date').AsDateTime;
    Self.FRepresentativeName:=Data.taRishumSubmission.FieldByName('RepName').AsString;
    Self.FRepresentativeRole:=Data.taRishumSubmission.FieldByName('RepRole').AsString;
    Self.FRepresentativePhone:=Data.taRishumSubmission.FieldByName('RepPhone').AsString;
    Self.FRepresentativeMail:=Data.taRishumSubmission.FieldByName('RepMail').AsString;
    Self.FAltHebrewName1:=Data.taRishumSubmission.FieldByName('AltHebName1').AsString;
    Self.FAltEnglishName1:=Data.taRishumSubmission.FieldByName('AltEngName1').AsString;
    Self.FAltHebrewName2:=Data.taRishumSubmission.FieldByName('AltHebName2').AsString;
    Self.FAltEnglishName2:=Data.taRishumSubmission.FieldByName('AltEngName2').AsString;
    Self.FAltHebrewName3:=Data.taRishumSubmission.FieldByName('AltHebName3').AsString;
    Self.FAltEnglishName3:=Data.taRishumSubmission.FieldByName('AltEngName3').AsString;
    Self.FGetCertificate:=(Data.taRishumSubmission.FieldByName('GetCert').AsInteger=1);
    Self.FEMail:=Data.taRishumSubmission.FieldByName('EMail').AsString;
    Self.FFax:=Data.taRishumSubmission.FieldByName('Fax').AsString;
    Self.FGetCopies:=(Data.taRishumSubmission.FieldByName('GetCopies').AsInteger=1);
    Self.FTakanonCopies:=Data.taRishumSubmission.FieldByName('TakanonCopies').AsInteger;
    Self.FCertificateCopies:=Data.taRishumSubmission.FieldByName('CertCopies').AsInteger;
    Self.FPayment:=Data.taRishumSubmission.FieldByName('Payment').AsFloat;
    Self.FEzel:=Data.taRishumSubmission.FieldByName('Ezel').AsString;
    Self.FDocsByMail:=(Data.taRishumSubmission.FieldByName('DByMail').AsInteger=1);
  end;
  Data.taRishumSubmission.Filtered:=False;
end;

constructor  TSubmissionData.LoadFromFile(Parent:TCompany;FileItem:TFileItem);
var
  filter:string;
begin
  if FileItem.FileInfo.Draft then
  begin
    filter:= '([FileNum] = '''+ IntToStr(FileItem.FileInfo.RecNum)+''') AND ([ObjectType]='''+ ObjToChar(ocCompany)+ ''') AND ([ACTION] = '''+ ActionToChar(acSubmissionData)+''')';
    If Parent.RecNum <> -1 Then
      Filter:= Filter + ' AND ([ObjectNum] = '''+ IntToStr(Parent.RecNum)+''')';
    Data.taForms.Filter:=Filter;
    Data.taForms.Filtered:=True;
    Data.taForms.First;
    if Data.taForms.Eof then FRecNum:=-1
    else FRecNum:=Data.taForms.FieldByName('NewNum').AsInteger;
    Data.taForms.Filtered:=False;
  end
  else
    FRecNum:=FileItem.EnumChanges(ocCompany, Parent.RecNum, acSubMissionData, LoadEnum);
  LoadEnum(Parent.RecNum,FRecNum);
end;

//load data from db
procedure TSubmissionData.LoadData(Parent: TCompany);
var
  companyNum: integer;
begin
  if (Parent <> nil) then
  begin
    companyNum := GetUnregisteredCompanyNum(Parent);
    data.taRishumSubmission.Open;
    try
      if (Parent <> nil)  and (FilterCompanyRecord(companyNum)) then
      begin
        WriteChangeLog(#9#9#9#9'Loading Submission Data');
        With  data.taRishumSubmission do
        begin
          FillData(FieldByName('RecNum').AsInteger,
                    FieldByName('Date').AsDateTime,
                    FieldByName('RepName').AsString ,
                    FieldByName('RepRole').AsString ,
                    FieldByName('RepPhone').AsString,
                    FieldByName('RepMail').AsString,
                    FieldByName('AltHebName1').AsString,
                    FieldByName('AltEngName1').AsString,
                    FieldByName('AltHebName2').AsString,
                    FieldByName('AltEngName2').AsString,
                    FieldByName('AltHebName3').AsString,
                    FieldByName('AltEngName3').AsString,
                    (FieldByName('GetCert').AsInteger = 1),
                    FieldByName('EMail').AsString,
                    FieldByName('Fax').AsString,
                    FieldByName('GetCopies').AsInteger = 1,
                    FieldByName('TakanonCopies').AsInteger ,
                    FieldByName('CertCopies').AsInteger,
                    FieldByName('Payment').AsFloat,
                    FieldByName('Ezel').AsString,
                    (FieldByName('DByMail').AsInteger<>0));
          Self.isLoadedFromDb := True;
          Filtered := False;
        end;
      end;
    finally
    end;
  end;
end;

constructor TSubmissionData.CreateNew(Date: TDateTime;
                        RepresentativeName: string;
                        RepresentativeRole: string;
                        RepresentativePhone: string;
                        RepresentativeMail:string;
                        AltHebrewName1: string;
                        AltEnglishName1: string;
                        AltHebrewName2: string;
                        AltEnglishName2: string;
                        AltHebrewName3: string;
                        AltEnglishName3: string;
                        GetCertificate: boolean;
                        EMail: string;
                        Fax: string;
                        GetCopies: boolean;
                        TakanonCopies: integer;
                        CertificateCopies: integer;
                        Payment: double;
                        Ezel:string;
                        DocsByMail:boolean);
begin
  ExFillData(Date, RepresentativeName, RepresentativeRole, RepresentativePhone,
             RepresentativeMail, AltHebrewName1, AltEnglishName1, AltHebrewName2,
             AltEnglishName2, AltHebrewName3, AltEnglishName3, GetCertificate, EMail,
             Fax, GetCopies, TakanonCopies, CertificateCopies, Payment, Ezel, DocsByMail);
end;

function TSubmissionData.GetUnregisteredCompanyNum(Company: TCompany): integer;
begin
  result := 0;
  data.taName.Open;
  try
    With DATA.taName do
    begin
      Filtered := False;
      Filter := ' Name = '''+FilterStr(Company.Name)+''' AND ( CompNum = ''����'' OR CompNum= ''-1'' OR CompNum= ''0'' ) ';
      Filtered := True;
      if (not EOF) then
        result  := FieldByName('RecNum').AsInteger ;
      Filtered := False;
    end;
  finally
  end;
end;

procedure TSubmissionData.FillData(RecNum: integer;
                                   Date: TDateTime;
                                   RepresentativeName: string;
                                   RepresentativeRole: string;
                                   RepresentativePhone: string;
                                   RepresentativeMail: string;
                                   AltHebrewName1: string;
                                   AltEnglishName1: string;
                                   AltHebrewName2: string;
                                   AltEnglishName2: string;
                                   AltHebrewName3: string;
                                   AltEnglishName3: string;
                                   GetCertificate: boolean;
                                   EMail: string;
                                   Fax: string;
                                   GetCopies: boolean;
                                   TakanonCopies: integer;
                                   CertificateCopies: integer;
                                   Payment: double;
                                   Ezel:string;
                                   DocsByMail:boolean);
begin
  FRecNum  :=    RecNum;
  ExFillData(Date,
          RepresentativeName,
          RepresentativeRole,
          RepresentativePhone,
          RepresentativeMail,
          AltHebrewName1,
          AltEnglishName1,
          AltHebrewName2,
          AltEnglishName2,
          AltHebrewName3,
          AltEnglishName3,
          GetCertificate,
          EMail,
          Fax,
          GetCopies,
          TakanonCopies,
          CertificateCopies,
          Payment,
          Ezel,
          DocsByMail);
end;

procedure TSubmissionData.ExFillData(Date: TDateTime;
                                   RepresentativeName: string;
                                   RepresentativeRole: string;
                                   RepresentativePhone: string;
                                   RepresentativeMail: string;
                                   AltHebrewName1: string;
                                   AltEnglishName1: string;
                                   AltHebrewName2: string;
                                   AltEnglishName2: string;
                                   AltHebrewName3: string;
                                   AltEnglishName3: string;
                                   GetCertificate: boolean;
                                   EMail: string;
                                   Fax: string;
                                   GetCopies: boolean;
                                   TakanonCopies: integer;
                                   CertificateCopies: integer;
                                   Payment: double;
                                   Ezel:string;
                                   DocsByMail:boolean);
begin
   FDate                := Date;
   FRepresentativeName  := RepresentativeName;
   FRepresentativeRole  := RepresentativeRole ;
   FRepresentativePhone := RepresentativePhone ;
   FRepresentativeMail  := RepresentativeMail;
   FAltHebrewName1      := AltHebrewName1 ;
   FAltEnglishName1     := AltEnglishName1 ;
   FAltHebrewName2      := AltHebrewName2 ;
   FAltEnglishName2     := AltEnglishName2 ;
   FAltHebrewName3      := AltHebrewName3 ;
   FAltEnglishName3     := AltEnglishName3 ;
   FGetCertificate      := GetCertificate ;
   FEMail               := EMail ;
   FFax                 := Fax ;
   FGetCopies           := GetCopies ;
   FTakanonCopies       := TakanonCopies ;
   FCertificateCopies   := CertificateCopies ;
   FPayment             := Payment ;
   FEzel                := Ezel;
   FDocsByMail          := DocsByMail;
end;

{function TSubmissionData.GetCompanyNumber : integer;
begin
     result := -1; //if no company is available return -1;
     if (Self.FCompany <> nil) then
     begin
       result := Self.FCompany.RecNum;
     end;
end;}

procedure TSubmissionData.ExSaveData(FileItem: TfileItem; Parent: TCompany);
begin
  SaveData(Parent);
  FileItem.SaveChange(ocCompany, Parent.RecNum, acSubmissionData, RecNum);
end;

//simple save to db
procedure TSubmissionData.SaveData(Parent: TCompany);
begin
  WriteChangeLog(#9#9#9'--------------------------');
  WriteChangeLog(#9#9#9'Saving Submission Data (' + Parent.Name + ')');
  { <-- }
  if (isLoadedFromDb ) then
  begin
    SaveExisting(Parent);
  end
  else
  begin
    SaveNew(Parent);
  end;
end;

//filters table to
function TSubmissionData.FilterCompanyRecord(CompanyNumber: integer) : boolean;
begin
  result := False;
  if (CompanyNumber > 0) then
  begin
    With data.taRishumSubmission do
    begin
      Filtered := False;
      Filter := 'CompNum = '+ IntToStr(CompanyNumber);
      Filtered := True;
      result :=  (not EOF);
      if (EOF) then
      begin
        Filtered := False;
        if( Active) then
          Close;
      end;
    end;
  end;
end;

procedure TSubmissionData.SaveExisting(Parent: TCompany);
var
  companyNum: integer;
begin
  WriteChangeLog(#9#9#9#9'Existing Record' );
  companyNum := GetUnregisteredCompanyNum(Parent);

  data.taRishumSubmission.Open;
  try
    if (FilterCompanyRecord(companyNum)) then
    begin
      With DATA.taRishumSubmission do
      begin
         Edit;
         SaveFieldsToTable(companyNum);
         Post;

         FRecNum:= FieldByName('RecNum').AsInteger;
         WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr(FRecNum));
         Self.isLoadedFromDb := True;
         Filtered := False;
      end;
    end else //for some reason the item was not found. No worries, let's save this anew.
    begin
      SaveNew(Parent);
    end;
  finally
  end;
end;

procedure TSubmissionData.SaveNew(Parent: TCompany);
var
  companyNum: integer;
begin
  WriteChangeLog(#9#9#9#9'New Record');

  companyNum:= GetUnregisteredCompanyNum(Parent);
  if (companyNum > 0) then//company was saved
  begin
    data.taRishumSubmission.Open;
    try
      With DATA.taRishumSubmission do
      begin
         Append;
         SaveFieldsToTable(companyNum);
         Post;

         FRecNum:= FieldByName('RecNum').AsInteger;
         WriteChangeLog(#9#9#9#9'RecNum after save: ' + IntToStr( FRecNum ));
         Self.isLoadedFromDb := True;
      end;
    finally
    end;
  end;
end;

procedure TSubmissionData.SaveFieldsToTable(companyNum : integer);
begin
  With DATA.taRishumSubmission do
  begin
     FieldByName('CompNum').AsInteger       := companyNum;
     FieldByName('TakanonCopies').AsInteger := Self.TakanonCopies;
     FieldByName('CertCopies').AsInteger    := Self.CertificateCopies;
     FieldByName('RepName').AsString        := Self.RepresentativeName;
     FieldByName('RepRole').AsString        := Self.RepresentativeRole;
     FieldByName('RepPhone').AsString       := Self.RepresentativePhone;
     FieldByName('RepMail').AsString        := Self.RepresentativeMail;
     FieldByName('AltHebName1').AsString    := Self.AltHebrewName1;
     FieldByName('AltEngName1').AsString    := Self.AltEnglishName1;
     FieldByName('AltHebName2').AsString    := Self.AltHebrewName2;
     FieldByName('AltEngName2').AsString    := Self.AltEnglishName2;
     FieldByName('AltHebName3').AsString    := Self.AltHebrewName3;
     FieldByName('AltEngName3').AsString    := Self.AltEnglishName3;
     FieldByName('EMail').AsString          := Self.EMail;
     FieldByName('Fax').AsString            := Self.Fax;
     FieldByName('GetCert').AsInteger       := Ord(Self.GetCertificate);
     FieldByName('GetCopies').AsInteger     := Ord(Self.GetCopies);
     FieldByName('Date').AsDateTime         := Self.Date;
     FieldByName('Payment').AsFloat         := Self.Payment;
     FieldByName('Ezel').AsString           := Self.Ezel;
  end;
  SetBooleanValue(DATA.taRishumSubmission,'DByMail',Self.DocsByMail);
end;
{end of TSubmissionData}

function GetTableInstance (Table: TTable): TTable;
var
  resultTable: TTable;
begin
  resultTable:= TTable.Create(Data);
  resultTable.DatabaseName:= Table.DatabaseName;
  resultTable.TableName:= Table.TableName;
  resultTable.IndexName:= Table.IndexName;
  result :=resultTable;
end;

end.
