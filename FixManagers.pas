unit FixManagers;

interface
uses
  SysUtils,classes, dbtables, dialogs, DataM, DataObjects, Forms;
Type
  TChangeType = (ctNone, ctManagerAddress, ctManagerDel,ctManagerAdd);

  TManagerFile = class;
  PManagerFile = ^TManagerFile;  
  TManagerFiles = class;
  PManagerFiles = ^TManagerFiles;
  TManagerChange = class;

  TTables = class(TObject)
    ChangesTable  : TTable;
    FormsTable    : TTable;
    FilesTable    : TTable;
    ManagersTable : TTable;
    Constructor Create;
    Procedure Open;
    Procedure Close;
  Protected
    ChangesWasClosed  :boolean;
    FilesWasClosed    :boolean;
    FormsWasClosed    :boolean;
    ManagersWasClosed : boolean;
    Procedure OpenTable (Table: TTable; var TableWasClosed: boolean);
    Procedure CloseTable (Table: TTable; TableWasClosed: boolean);
  end;

  PTables = ^TTables;

  TManagerInstance = class (TObject)
    ManagerNum : integer;
    FileNum    : integer;
    Date       : TDateTime;
    IsDelete   : boolean
  end;

  TManagerContainer = class (Tlist)  //of TManagerInstance
    ManagerID : string;
    procedure EnterDuplicate ( ManagerChange: TManagerChange);
    procedure SortList;
    procedure Free;
  Protected
    function FindInstance (ManagerNum: integer): integer;
    function AddInstance (ManagerChange: TManagerChange): integer;
  end;

  TCompanyContainer = class (Tlist)   //of TManagerContainer
    CompanyNum : integer;
    procedure Connect ( TablesP: PTables);    
    procedure EnterDuplicate ( ManagerChange: TManagerChange);
    procedure Free;
    procedure Prepare;
  Protected
    RelevantTables: PTables;
    function FindManager (ManagerID: string): integer;
    function GetManagerID (ManagerNum: integer ):  string;
    function AddManager  (ManagerID: string): integer;
  end;

  TDuplicateHandler = class(TList) //holds a list of TCompanyContainer
    procedure EnterDuplicate ( ManagerChange: TManagerChange);
    procedure FixDuplicates;
    procedure Free;
    procedure Connect ( ManagerFiles: PManagerFiles ; TablesP: PTables);
    procedure Load;
  Protected
    OutputFiles: PManagerFiles;
    RelevantTables: PTables;
    function FindCompany (CompanyNum: integer): integer;
    function AddCompany  (CompanyNum: integer): integer;
    procedure GenerateChangeRecord (FileNum,ManagerNum: integer; ChangeDate: TDateTime);
    procedure AddOrphanFiles;
  end;

  TManagerChanges = class(TList)
  public
    procedure Load;
    procedure FixAll;
    procedure Free;
    procedure ConnectTables (TablesP:PTables);
    constructor Create (ParentP: TManagerFile);
  Protected
    ParentFile : TManagerFile;
    ChangesTable : TTable;
    ChangeType: TChangeType;
    RelevantTables: PTables;
    function GetChangeType : TChangeType;
  end;

  TManagerFile = class (TObject)
    FileNum    :integer;
    CompanyNum :integer;
    ManagerChanges: TManagerChanges;
    procedure Fix;
    constructor Create( FileNumP, CompanyNumP :integer; TablesP:PTables);
    procedure ConnectTables (TablesP:PTables);
    procedure Free;
  Protected
    RelevantTables: PTables;
  end;

  TManagerChange = class (TObject)
    IsDel : boolean;
    ManagerNum : integer;
    ParentFile : TManagerFile;
    ChangeDate :TDateTime;
    constructor Create (ParentP: TmanagerFile; IsDelP : boolean; ManagerNumP : integer; DateP: TDateTime; TablesP:PTables);
    function Check: Boolean;
    procedure Fix;
    procedure ConfigureObjects;
    procedure ConnectTables (TablesP:PTables);
  Protected
    ObjectType : TObjectClass;
    ObjectAction : TAction;
    ObjectNum   : integer;
    RelevantTables: PTables;
    FormsTable  : TTable;
    procedure AddChangeToTable;
  end;

  TManagerFiles = class (TList)
    procedure Load;
    procedure FixAll;
    procedure Free;
    procedure ConnectTables (TablesP:PTables);
    function  FindFile(FileNum : integer) :TManagerFile;
  Protected
    FilesTable: TTable;
    RelevantTables: PTables;
    function FileIsManager: boolean;
  end;

  TFixer = class(TObject)
  public
    ManagerFiles: TManagerFiles;
    DuplicateHandler: TDuplicateHandler;
    RelevantTables :TTables;
    constructor Create (Sender: TForm);
    procedure Free;
    Procedure Execute;
    procedure FixAddresses;
    procedure ChangeMistakenFiles;
    procedure FixForms (FileNum: integer);
  Private
    SenderForm: TForm;
    procedure RefreshForm;
  end;


  function SortByDate (Item1, Item2: Pointer): Integer;
  procedure FixAllManagers (Sender: TForm);
implementation
{TFixer}
//==============================================================================
constructor TFixer.Create (Sender: TForm);
begin
  inherited Create;
  SenderForm := Sender;
  ManagerFiles := TManagerFiles.Create;

  RelevantTables := TTables.Create;
  RelevantTables.Open;

  ChangeMistakenFiles;
  ManagerFiles.ConnectTables(@RelevantTables);

  DuplicateHandler:= TDuplicateHandler.Create;
  DuplicateHandler.Connect(@ManagerFiles,@RelevantTables);

  FixAddresses;
end;
//==============================================================================
procedure TFixer.Free;
begin
  DuplicateHandler.Free;
  ManagerFiles.Free;
end;
//==============================================================================
procedure TFixer.Execute;
begin
  RefreshForm;
  ManagerFiles.Load;
  RefreshForm;
  DuplicateHandler.Load;
  RefreshForm;
  DuplicateHandler.FixDuplicates;
  RefreshForm;
  ManagerFiles.FixAll;
  RefreshForm;  
end;
//==============================================================================*
procedure TFixer.FixAddresses;
var
  FileNum: integer;
begin
  if RelevantTables.ChangesTable <> nil then
    with RelevantTables.ChangesTable do
    begin
      Filtered := false;
      Filter   :='ObjectType = '''+ObjToChar(ocManager)+''' AND Action = '''+ActionToChar(acAddress)+'''';
      Filtered := true;
      First;
      While not EOF do
      begin
        Edit;
        FieldByName('NewNum').AsInteger :=FieldByName('ObjectNum').AsInteger;
        FieldByName('Action').AsString := ActionToChar(acDelete);
        FileNum :=FieldByName('ResInt1').AsInteger;
        FixForms(FileNum);
        Next;
      end;
      Filtered := false;
    end;

end;
//==============================================================================
procedure TFixer.FixForms (FileNum: integer);
begin
  if RelevantTables.FormsTable <> nil then
    with RelevantTables.FormsTable do
    begin
      Filtered := false;
      Filter   :='ObjectType = '''+ObjToChar(ocManager)+''' AND Action = '''
                 +ActionToChar(acAddress)+''' AND FileNum = '+IntToStr(FileNum);
      Filtered := true;
      First;
      While not EOF do
      begin
        Edit;
        FieldByName('NewNum').AsInteger :=FieldByName('ObjectNum').AsInteger;
        FieldByName('Action').AsString := ActionToChar(acDelete);
        FieldByName('ObjectNum').AsInteger := -1;
        Next;
      end;
      Filtered := false;
    end;
end;
//==============================================================================*
procedure TFixer.ChangeMistakenFiles;
var
  i : integer;
  sRec : string;
begin
  With RelevantTables do
    if (ChangesTable<>nil) then
    begin
      ChangesTable.Filtered := false;
      ChangesTable.Filter   :='((ObjectType = '''+ObjToChar(ocManager)
                  +''' AND Action = '''+ActionToChar(acDelete)
                  +''') OR (ObjectType = '''+ObjToChar(ocCompany)
                  +''' AND Action = '''+ActionToChar(acNewManager)+
                  ''')) AND (ResInt1 <> -1)';
      ChangesTable.Filtered := true;
      ChangesTable.First;
      While not ChangesTable.EOF do
      begin
        if (FilesTable<>nil) then
        begin
          FilesTable.Filtered := false;
          sRec := ChangesTable.FieldByName('ResInt1').AsString;
          if sRec <> EmptyStr then
          begin
            FilesTable.Filter   :='RecNum = '+sRec
                                  +' AND  Action  <> '''+FileActionToChar(faManager) +''''
                                  +' AND  Action  <> '''+FileActionToChar(faRishum)+'''';

            FilesTable.Filtered := true;
            for i := 0 to FilesTable.RecordCount-1 do
            begin
              FilesTable.Edit;
              FilesTable.FieldByName('Action').AsString := FileActionToChar(faManager);
              FilesTable.Post;
            end;
            FilesTable.Filtered := false;
          end;
        end;
        ChangesTable.Next;
      end;

      ChangesTable.Filtered := false;
    end;
end;
//==============================================================================
procedure TFixer.RefreshForm;
var
  i: integer;
begin
 If SenderForm <> nil then
   for  i:= 0 to 200 do
     SenderForm.Refresh;
end;
//==============================================================================
{TManagerFiles}
//==============================================================================
procedure TManagerFiles.Load;
begin
  if (FilesTable<>nil) then
    With FilesTable do
    begin
      First;
      While (not EOF) do
      begin
        if FileIsManager then
          Add(TManagerFile.Create(FieldByName('RecNum').AsInteger,FieldByName('Company').AsInteger, RelevantTables));
        Next;
      end;
    end;
end;
//==============================================================================
procedure TManagerFiles.FixAll;
var
  i: integer;
begin
  for i := 0 to Count-1 do
    TManagerFile(Items[i]).Fix;
end;
//==============================================================================
procedure TManagerFiles.Free;
var
  i: integer;
begin
  for i := 0 to Count-1 do
    TManagerFile(Items[i]).Free;

  inherited Free;
end;
//==============================================================================
function TManagerFiles.FileIsManager: boolean;
begin
  Result := false;
  if (FilesTable<>nil) then
    Result :=( CompareStr(FilesTable.FieldByName('Action').AsString , FileActionToChar(faManager)) = 0 );
end;
//==============================================================================
procedure TManagerFiles.ConnectTables (TablesP:PTables);
begin
  RelevantTables := TablesP;
  FilesTable := RelevantTables^.FilesTable;
end;
//==============================================================================*
function TManagerFiles.FindFile(FileNum : integer) :TManagerFile;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Count-1 do
    if TManagerFile(Items[i]).FileNum = FileNum then
    begin
      result := TManagerFile(Items[i]);
      break;
    end;
end;
//==============================================================================*
{TManagerFile}
//==============================================================================
procedure TManagerFile.Fix;
begin
  ManagerChanges.FixAll;
end;
//==============================================================================
constructor TManagerFile.Create( FileNumP, CompanyNumP :integer; TablesP:PTables);
begin
  FileNum    := FileNumP;
  CompanyNum := CompanyNumP;
  ConnectTables(TablesP);
  ManagerChanges:= TManagerChanges.Create(Self);
  ManagerChanges.ConnectTables(TablesP);
  ManagerChanges.Load;
end;
//==============================================================================
procedure TManagerFile.Free;
begin
  ManagerChanges.Free;
end;
//==============================================================================
procedure TManagerFile.ConnectTables (TablesP:PTables);
begin
  RelevantTables := TablesP;
end;

//==============================================================================
{TManagerChanges}
//==============================================================================
procedure TManagerChanges.Load;
var
  IsDelete : boolean;
begin
  if (ChangesTable<>nil) then
  begin
    With ChangesTable do
    begin
      Filtered := false;
      Filter :='ResInt1 = '+IntToStr(ParentFile.FileNum);
      Filtered := true;
      First;
      While (not EOF) do
      begin
        ChangeType := GetChangeType;
        if ChangeType<>ctNone then
        begin
          IsDelete := (ChangeType = ctManagerDel);
          Add(TManagerChange.Create(ParentFile, IsDelete,
              FieldByName('NewNum').AsInteger, FieldByName('Date').AsDateTime, RelevantTables));
        end;
        Next;
      end;
      Filtered := false;
    end;
  end;
end;
//==============================================================================
procedure TManagerChanges.FixAll;
var
  i: integer;
begin
  for i := 0 to Count-1 do
    TManagerChange(Items[i]).Fix;
end;
//==============================================================================
procedure TManagerChanges.Free;
var
  i: integer;
begin
  for i := 0 to Count-1 do
    TManagerChange(Items[i]).Free;

  inherited Free;
end;
//==============================================================================
constructor TManagerChanges.Create (ParentP: TManagerFile);
begin
  ParentFile := ParentP;
end;
//==============================================================================
function TManagerChanges.GetChangeType : TChangeType;
var
  ObjectType : TObjectClass;
  ObjectAction : TAction;
begin
  Result := ctNone;
  if (ChangesTable<>nil) then
  begin
    With ChangesTable do
    begin
      ObjectType := CharToObj(FieldByName('ObjectType').AsString[1]);
      ObjectAction := CharToAction(FieldByName('Action').AsString[1]);

      if (ObjectType = ocCompany) and  (ObjectAction = acNewManager) then
        Result := ctManagerAdd
      else if (ObjectType = ocManager) and  (ObjectAction = acDelete) then
        Result := ctManagerDel;
    end;
  end;
end;
//==============================================================================
procedure TManagerChanges.ConnectTables (TablesP:PTables);
begin
  RelevantTables := TablesP;
  ChangesTable := RelevantTables^.ChangesTable;
end;
{TManagerChange}
//==============================================================================
constructor TManagerChange.Create (ParentP: TManagerFile; IsDelP : boolean; ManagerNumP : integer; DateP: TDateTime; TablesP: PTables);
begin
  IsDel      := IsDelP;
  ManagerNum := ManagerNumP;
  ParentFile := ParentP;
  ChangeDate := DateP;
  ConnectTables(TablesP);
  ConfigureObjects;
end;
//==============================================================================*
function TManagerChange.Check: boolean;
begin
  Result := false;
  if (FormsTable<>nil) then
  begin
    With FormsTable do
    begin
      Filtered := false;
      Filter := 'FileNum = '+IntToStr(ParentFile.FileNum)+' AND ObjectType = '''+ObjToChar(ObjectType)
                 +''' AND ObjectNum = '+IntToStr(ObjectNum)+'AND Action = '''
                +ActionToChar(ObjectAction)+''' AND NewNum = '+IntToStr(ManagerNum);
      Filtered := True;
      Result := (RecordCount>0);
      Filtered := false;
    end;
  end;
end;
//==============================================================================*
procedure TManagerChange.Fix;
begin
  if not Check then
  begin
    if FormsTable <> nil then
    begin
      With FormsTable do
      begin
        Append;
        FieldByName('FileNum').AsInteger   := ParentFile.FileNum;
        FieldByName('ObjectType').AsString := ObjToChar(ObjectType);
        FieldByName('ObjectNum').AsInteger := ObjectNum;
        FieldByName('Action').AsString     := ActionToChar(ObjectAction);
        FieldByName('NewNum').AsInteger    := ManagerNum;
        Post;
      end;
    end;
  end;
end;
//==============================================================================
procedure TManagerChange.ConfigureObjects;
begin
if IsDel then
  begin
    ObjectType   := ocManager;
    ObjectAction := acDelete;
    ObjectNum    := -1;
  end else
  begin
    ObjectType   := ocCompany;
    ObjectAction := acNewManager;
    ObjectNum    := ParentFile.CompanyNum;
  end;
end;
//==============================================================================
procedure TManagerChange.ConnectTables (TablesP:PTables);
begin
  RelevantTables := TablesP;
  FormsTable := RelevantTables^.FormsTable;
end;
//==============================================================================*
procedure TManagerChange.AddChangeToTable;
//This is currently used only to add "delete manager" changes. The code reflects this.
begin
  ConfigureObjects;
  if RelevantTables.ChangesTable <> nil then
    with RelevantTables.ChangesTable do
    begin
      Append;
      FieldByName('ObjectType').AsString  := ObjToChar(ObjectType);
      FieldByName('ObjectNum').AsInteger  := ManagerNum;
      FieldByName('Action').AsString      := ActionToChar(ObjectAction);
      FieldByName('Date').AsDateTime      := ChangeDate;
      FieldByName('NewNum').AsInteger     := ManagerNum;
      FieldByName('ResInt1').AsInteger    := ParentFile.FileNum;
      Post;
    end;
end;
//==============================================================================*

{TTables}
//==============================================================================
Constructor TTables.Create;
begin
  ChangesTable  := Data.taChanges;
  FormsTable    := Data.taForms;
  FilesTable    := Data.taFiles;
  ManagersTable := Data.taManager;
end;
//==============================================================================
Procedure TTables.Open;
begin
  OpenTable(ChangesTable,ChangesWasClosed);
  OpenTable(FormsTable,FormsWasClosed);
  OpenTable(FilesTable,FilesWasClosed);
  OpenTable(ManagersTable,ManagersWasClosed);
end;

//==============================================================================
Procedure TTables.Close;
begin
  CloseTable(ChangesTable,ChangesWasClosed);
  CloseTable(FormsTable,FormsWasClosed);
  CloseTable(FilesTable,FilesWasClosed);
  CloseTable(ManagersTable,ManagersWasClosed);
end;
//==============================================================================
procedure TTables.OpenTable (Table: TTable; var TableWasClosed: boolean);
begin
  if (Table<>nil) then
  begin
    TableWasClosed := not Table.Active;
    if TableWasClosed then
      Table.Open;
  end
  else
    ShowMessage('��� ������ ����� ������� ������ ���� '+Table.TableName);
end;
//==============================================================================
procedure TTables.CloseTable (Table: TTable; TableWasClosed: boolean);
begin
  if (Table<>nil) then
  begin
    if TableWasClosed then
      Table.Close;
  end
  else
    ShowMessage('��� ������ ����� �������  ������ ���� '+Table.TableName);
end;
//==============================================================================
// No Object
//==============================================================================
procedure FixAllManagers(Sender: TForm);
var
  Fixer: TFixer;
begin
  Fixer := TFixer.Create(Sender);
  Fixer.Execute;
  Fixer.Free;
end;
//==============================================================================*
{TManagerContainer}
//==============================================================================
procedure TManagerContainer.EnterDuplicate ( ManagerChange: TManagerChange);
var
  InstanceNum: integer;
begin
   InstanceNum:= FindInstance (ManagerChange.ManagerNum);

   if InstanceNum<>-1 then
     if (TManagerInstance(Items[InstanceNum]).IsDelete) or (ManagerChange.IsDel) then
        Delete(InstanceNum)
   else else
     AddInstance(ManagerChange);
end;
//==============================================================================*
procedure TManagerContainer.SortList;
begin
  Sort(SortByDate);
end;
//==============================================================================
procedure TManagerContainer.Free;
var
  i: integer;
begin
  for i := 0 to Count-1 do
    TManagerInstance(Items[i]).Free;

  inherited Free;
end;
//==============================================================================
function TManagerContainer.FindInstance (ManagerNum: integer): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to Count-1 do
    if TManagerInstance(Items[i]).ManagerNum = ManagerNum then
    begin
      result := i;
      break;
    end;
end;
//==============================================================================
function TManagerContainer.AddInstance (ManagerChange: TManagerChange): integer;
var
  CurrIndex: integer;
begin
  CurrIndex := Add(TManagerInstance.Create);
  TManagerInstance(Items[CurrIndex]).ManagerNum :=  ManagerChange.ManagerNum;
  TManagerInstance(Items[CurrIndex]).FileNum    :=  ManagerChange.ParentFile.FileNum;
  TManagerInstance(Items[CurrIndex]).Date       :=  ManagerChange.ChangeDate;
  TManagerInstance(Items[CurrIndex]).IsDelete   :=  ManagerChange.IsDel;
  Result := CurrIndex;
end;
//==============================================================================
function SortByDate (Item1, Item2: Pointer): Integer;
begin
  Result := 0;
  if TManagerInstance(Item1).Date < TManagerInstance(Item2).Date then
    Result := -1
  else if TManagerInstance(Item1).Date > TManagerInstance(Item2).Date then
    Result := 1
  else // if dates are the same, sort by file number
    if TManagerInstance(Item1).FileNum < TManagerInstance(Item2).FileNum then
      Result := -1
    else if TManagerInstance(Item1).FileNum > TManagerInstance(Item2).FileNum then
      Result := 1
     else //if all else fails, sort by manager num
      if TManagerInstance(Item1).ManagerNum < TManagerInstance(Item2).ManagerNum then
        Result := -1
      else if TManagerInstance(Item1).ManagerNum > TManagerInstance(Item2).ManagerNum then
        Result := 1;

end;
//==============================================================================*
{TCompanyContainer}
//==============================================================================*
procedure TCompanyContainer.Free;
var
  i: integer;
begin
  for i := 0 to Count-1 do
    TManagerContainer(Items[i]).Free;

  inherited Free;
end;
//==============================================================================*
procedure TCompanyContainer.Prepare;
var
  i: integer;
begin
  for i := 0 to Count-1 do
    TManagerContainer(Items[i]).SortList;
end;
//==============================================================================*
function TCompanyContainer.FindManager (ManagerID: string): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to Count-1 do
    if (CompareStr(TManagerContainer(Items[i]).ManagerID ,ManagerID) = 0 ) then
    begin
      result := i;
      break;
    end;
end;
//==============================================================================*
function TCompanyContainer.AddManager  (ManagerID: string): integer;
var
  CurrIndex: integer;
begin
  CurrIndex := Add(TManagerContainer.Create);
  TManagerContainer(Items[CurrIndex]).ManagerID :=  ManagerID;
  Result :=CurrIndex;
end;
//==============================================================================*
procedure TCompanyContainer.EnterDuplicate ( ManagerChange: TManagerChange);
var
  ManagerIndex: integer;
  ManagerID : string;
begin
   ManagerID := GetManagerID(ManagerChange.ManagerNum);
   if StrToIntDef(ManagerID,0)<>0 then
   begin
     ManagerIndex := FindManager(ManagerID);

     if ManagerIndex = -1 then
       ManagerIndex:= AddManager(ManagerID);
     TManagerContainer(Items[ManagerIndex]).EnterDuplicate(ManagerChange);
   end;
end;
//==============================================================================*
function TCompanyContainer.GetManagerID (ManagerNum: integer ):  string;
begin
  result := '';
  If RelevantTables^.ManagersTable <> nil then
  Begin
    With  RelevantTables^.ManagersTable do
    begin
      Filtered := false;
      Filter   :='RecNum = '+IntToStr(ManagerNum);
      Filtered := true;
      First;
      Result := FieldByName('Zeut').AsString;
      Filtered := false;
    end;
  end;
end;
//==============================================================================
procedure TCompanyContainer.Connect ( TablesP: PTables);
begin
  RelevantTables :=TablesP;
end;
//==============================================================================*
{TDuplicateHandler}
//==============================================================================*
procedure TDuplicateHandler.EnterDuplicate ( ManagerChange: TManagerChange);
var
  CompanyIndex: integer;
begin
  CompanyIndex := FindCompany(ManagerChange.ParentFile.CompanyNum);

   if CompanyIndex = -1 then
     CompanyIndex:= AddCompany(ManagerChange.ParentFile.CompanyNum);
   TCompanyContainer(Items[CompanyIndex]).EnterDuplicate(ManagerChange);
end;
//==============================================================================*
procedure TDuplicateHandler.Free;
var
  i: integer;
begin
  for i := 0 to Count-1 do
    TCompanyContainer(Items[i]).Free;

  inherited Free;
end;
//==============================================================================*
procedure TDuplicateHandler.Connect ( ManagerFiles: PManagerFiles ; TablesP: PTables);
begin
  OutputFiles:= ManagerFiles ;
  RelevantTables := TablesP;
end;
//==============================================================================*
function TDuplicateHandler.FindCompany (CompanyNum: integer): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to Count-1 do
    if TCompanyContainer(Items[i]).CompanyNum = CompanyNum then
    begin
      result := i;
      break;
    end;
end;
//==============================================================================*
function TDuplicateHandler.AddCompany  (CompanyNum: integer): integer;
var
  CurrIndex: integer;
begin
  CurrIndex := Add(TCompanyContainer.Create);
  TCompanyContainer(Items[CurrIndex]).CompanyNum := CompanyNum;
  TCompanyContainer(Items[CurrIndex]).Connect(RelevantTables);
  Result := CurrIndex;
end;
//==============================================================================*
procedure TDuplicateHandler.GenerateChangeRecord (FileNum,ManagerNum: integer; ChangeDate: TDateTime);
var
  TempFile     : TManagerFile;
  ChangeIndex : integer;
begin
  TempFile :=(OutputFiles^.FindFile(FileNum));
  ChangeIndex:=TempFile.ManagerChanges.Add(TManagerChange.Create(TempFile, True, ManagerNum
                                                ,ChangeDate, RelevantTables));
  TManagerChange(TempFile.ManagerChanges.Items[ChangeIndex]).AddChangeToTable;
end;
//==============================================================================*
procedure TDuplicateHandler.FixDuplicates;
var
  i,j,k : integer;
  TempCompany  : TCompanyContainer;
  TempManager  : TManagerContainer;
  TempInstance : TManagerInstance;
  NextInstance : TManagerInstance;
begin
  for i := 0 to Count-1 do
  begin
    TempCompany:= TCompanyContainer(Items[i]);
    TempCompany.Prepare;
    for j := 0 to TempCompany.Count -1 do
    begin
      TempManager  := TManagerContainer(TempCompany.Items[j]);
      for k := 0 to TempManager.Count -2 do
      begin
        TempInstance := TManagerInstance(TempManager.Items[k]);
        NextInstance := TManagerInstance(TempManager.Items[k+1]);
        if not TempInstance.IsDelete then
          GenerateChangeRecord (NextInstance.FileNum,TempInstance.ManagerNum, NextInstance.Date)
      end;
    end;
  end;
end;
//==============================================================================*
procedure TDuplicateHandler.Load;
var
  i,j : integer;
  TempFile : TManagerFile;
  TempChanges: TManagerChanges;
begin
   AddOrphanFiles;
   for  i := 0 to OutputFiles^.Count -1 do
   begin
     TempFile := TManagerFile(OutputFiles^.Items[i]);
     TempChanges := TempFile.ManagerChanges;
     for  j := 0 to TempChanges.Count -1 do
     begin
       EnterDuplicate(TManagerChange(TempChanges.Items[j]));
     end;
   end;
end;
//==============================================================================*
procedure TDuplicateHandler.AddOrphanFiles;
var
  OrphansQuery: TQuery;
  TempFileNum: integer;
  TempCompanyNum : integer;
begin
  OrphansQuery := TQuery.Create(nil);
  With OrphansQuery Do
  Begin
    DatabaseName := RelevantTables^.ChangesTable.DatabaseName;
    SQL.Clear;
    SQL.Add('SELECT ResInt1 as FileNum,  ObjectNum as CompanyNum');
    SQL.Add('FROM Changes');
    SQL.Add('WHERE ObjectType = '''+ObjToChar(ocCompany)+'''');
    SQL.Add(  'AND Action = '''+ActionToChar(acNewManager)+'''');
    SQL.Add(  'AND ResInt1 <> -1');
    SQL.Add(  'AND ResInt1 NOT IN (SELECT RecNum');
    SQL.Add(                      'FROM Files)');

    Open;
    First;
    While not EOF do
    begin
      TempCompanyNum := FieldByName('CompanyNum').AsInteger;
      TempFileNum    := FieldByName('FileNum').AsInteger;
      OutputFiles^.Add(TManagerFile.Create(TempFileNum,TempCompanyNum,RelevantTables));
      Next;
    end;
    Free;
  end;
end;
//==============================================================================*
end.
