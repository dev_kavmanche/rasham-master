unit TakanonRep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CompanyInfoTemplate, HebForm, ExtCtrls, StdCtrls, HGrids, SdGrid, Mask,
  HComboBx, HEdit, ComCtrls, Buttons, HLabel, DataObjects, ShowCompany;

type
  TfmTakanonRep = class(TfmShowCompany)
  private
    { Private declarations }
  protected
     procedure EvPrintData; override;
  public
    { Public declarations }

  end;

var
  fmTakanonRep: TfmTakanonRep;

implementation

uses prTakanon;

{$R *.DFM}

procedure TfmTakanonRep.EvPrintData;
begin
  if not Assigned(Company) then exit;
  Application.CreateForm(TfrmQrTakanon, frmQrTakanon);
  frmQrTakanon.PrintExecute(Caption, True, Company);
  frmQrTakanon.Free;
end;


end.
