unit qrannr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, DataObjects, Db, memdset;

type
  TAnnualRepData=class
    private
       FDirectorsChanged,FDirectorsNotChanged:boolean;
       FMusmakhName,FMusmakhZeut,FMusmakhRole:string;
       FReportsApproved:boolean;
       FReportsDisplayed,FDoesNotHaveAnnualAssembly,FDoesNotHaveToMakeReports:boolean;
       FThereIsAnAccountant,FAccountantNameIs,FNoAccountant,FNonActive:boolean;
       FAccountantName,FAccountantAddress:string;
       FDaysWithoutAccountant:integer;
       FManagerName,FManagerID,FManagerAddress,FManagerPhone:string;
       FMemuneName,FMemuneID,FMemuneRole:string;
       FReportBrought,FReportSent:boolean;
       FReportSentAt:TDateTime;
       FMatzhirName,FMatzhirID,FMatzhirRole:string;
       FAdvName,FAdvSignName,FAdvSignID:string;
       FAdvMeametName,FAdvMeametAddress,FAdvMeametID,FAdvMeametLicence:string;
       FAccName,FAccSignName,FAccSignID:string;
       FAccMeametName,FAccMeametAddress,FAccMeametID,FAccMeametLicence:string;
       function GetManagerCount:integer;
       function GetManagerName(index:integer):string;
       function GetManagerID(index:integer):string;
       function GetManagerAddress(index:integer):string;
       function GetManagerPhone(index:integer):string;
    public
      property DirectorsChanged:boolean read FDirectorsChanged;
      property DirectorsNotChanged:boolean read FDirectorsNotChanged;
      property MusmakhName:string read FMusmakhName;
      property MusmakhZeut:string read FMusmakhZeut;
      property MusmakhRole:string read FMusmakhRole;
      property ReportsApproved:boolean read FReportsApproved;
      property ReportsDisplayed:boolean read FReportsDisplayed;
      property DoesNotHaveAnnualAssembly:boolean read FDoesNotHaveAnnualAssembly;
      property DoesNotHaveToMakeReports:boolean read FDoesNotHaveToMakeReports;
      property ThereIsAnAccountant:boolean read FThereIsAnAccountant;
      property AccountantNameIs:boolean read FAccountantNameIs;
      property NoAccountant:boolean read FNoAccountant;
      property NonActive:boolean read FNonActive;
      property AccountantName:string read FAccountantName;
      property AccountantAddress:string read FAccountantAddress;
      property DaysWithoutAccountant:integer read FDaysWithoutAccountant;
      property ManagerCount:integer read GetManagerCount;
      property ManagerName[Index:integer]:string read GetManagerName;
      property ManagerID[Index:integer]:string read GetManagerID;
      property ManagerAddress[Index:integer]:string read GetManagerAddress;
      property ManagerPhone[Index:integer]:string read GetManagerPhone;
      property MemuneName:string read FMemuneName;
      property MemuneID:string read FMemuneID;
      property MemuneRole:string read FMemuneRole;
      property ReportBrought:boolean read FReportBrought;
      property ReportSent:boolean read FReportSent;
      property ReportSentAt:TDateTime read FReportSentAt;
      property MatzhirName:string read FMatzhirName;
      property MatzhirID:string read FMatzhirID;
      property MatzhirRole:string read FMatzhirRole;
      property AdvName:string read FAdvName;
      property AdvSignName:string read FAdvSignName;
      property AdvSignID:string read FAdvSignID;
      property AdvMeametName:string read FAdvMeametName;
      property AdvMeametAddress:string read FAdvMeametAddress;
      property AdvMeametID:string read FAdvMeametID;
      property AdvMeametLicence:string read FAdvMeametLicence;
      property AccName:string read FAccName;
      property AccSignName:string read FAccSignName;
      property AccSignID:string read FAccSignID;
      property AccMeametName:string read FAccMeametName;
      property AccMeametAddress:string read FAccMeametAddress;
      property AccMeametID:string read FAccMeametID;
      property AccMeametLicence:string read FAccMeametLicence;

      constructor Create(ADirectorsChanged,ADirectorsNotChanged:boolean;AMusmakhName,AMusmakhZeut,AMusmakhRole:string;
       AReportsApproved,AReportsDisplayed,ADoesNotHaveAnnualAssembly,ADoesNotHaveToMakeReports,
       AThereIsAnAccountant,AAccountantNameIs,ANoAccountant,ANonActive:boolean;
       AAccountantName,AAccountantAddress:string;ADaysWithoutAccountant:integer;
       AManagerName,AManagerID,AManagerAddress,AManagerPhone,AMemuneName,AMemuneID,AMemuneRole:string;
       AReportBrought,AReportSent:boolean;AReportSentAt:TDateTime;
       AMatzhirName,AMatzhirID,AMatzhirRole,AAdvName,AAdvSignName,AAdvSignID,
       AAdvMeametName,AAdvMeametAddress,AAdvMeametID,AAdvMeametLicence,
       AAccName,AAccSignName,AAccSignID,AAccMeametName,AAccMeametAddress,AAccMeametID,AAccMeametLicence:string);
      destructor Free;
  end;

  TfmQrAnnualReport = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRImage13: TQRImage;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    QRImage8: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape7: TQRShape;
    sp2: TQRShape;
    sp1: TQRShape;
    lblTotalCapital: TQRLabel;
    lblStockName1: TQRLabel;
    lblStockNameA1: TQRLabel;
    lblStockType1: TQRLabel;
    lblRashum1: TQRLabel;
    lblAssigned1: TQRLabel;
    lblStockValue1: TQRLabel;
    QRLabel29: TQRLabel;
    QRShape11: TQRShape;
    sp6: TQRShape;
    sp5: TQRShape;
    sp7: TQRShape;
    sp8: TQRShape;
    lblStockHolderName1: TQRLabel;
    lblStockHolderID1: TQRLabel;
    lblStockHolderAddress1: TQRLabel;
    lblStockHolderAddressA1: TQRLabel;
    lblStockTypeA1: TQRLabel;
    lblStocksNum1: TQRLabel;
    lblSumNotPaid1: TQRLabel;
    edtCompanyName: TQRLabel;
    edtCompNum: TQRLabel;
    edtCompanyAddress: TQRLabel;
    edtCompanyPhone: TQRLabel;
    edtCompanyEmail: TQRLabel;
    edtREportIsUpToDateAt: TQRLabel;
    edtAnnualAssemblyDate: TQRLabel;
    edtTotalCapital: TQRLabel;
    edtStockName1: TQRLabel;
    edtSaidStockValue1: TQRLabel;
    edtStockTypeA1: TQRLabel;
    edtRashum1: TQRLabel;
    edtAssignedStocksNumber1: TQRLabel;
    edtTotalStockValue1: TQRLabel;
    edtStockHolderName1: TQRLabel;
    edtStockType1: TQRLabel;
    edtStockHolderID1: TQRLabel;
    edtStocksNum1: TQRLabel;
    edtStockHolderAddress1: TQRLabel;
    edtSumNotPaid1: TQRLabel;
    lblStockHolderName2: TQRLabel;
    edtStockHolderName2: TQRLabel;
    lblStockTypeA2: TQRLabel;
    edtStockType2: TQRLabel;
    lblStockHolderID2: TQRLabel;
    edtStockHolderID2: TQRLabel;
    lblStocksNum2: TQRLabel;
    edtStocksNum2: TQRLabel;
    lblStockHolderAddress2: TQRLabel;
    lblStockHolderAddressA2: TQRLabel;
    edtStockHolderAddress2: TQRLabel;
    lblSumNotPaid2: TQRLabel;
    edtSumNotPaid2: TQRLabel;
    lblStockHolderName3: TQRLabel;
    edtStockHolderName3: TQRLabel;
    lblStockTypeA3: TQRLabel;
    edtStockType3: TQRLabel;
    lblStockHolderID3: TQRLabel;
    edtStockHolderID3: TQRLabel;
    lblStocksNum3: TQRLabel;
    edtStocksNum3: TQRLabel;
    lblStockHolderAddress3: TQRLabel;
    lblStockHolderAddressA3: TQRLabel;
    edtStockHolderAddress3: TQRLabel;
    lblSumNotPaid3: TQRLabel;
    edtSumNotPaid3: TQRLabel;
    QRShape16: TQRShape;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRCompositeReport1: TQRCompositeReport;
    QuickRep4: TQuickRep;
    QRBand4: TQRBand;
    QRImage5: TQRImage;
    QRLabel161: TQRLabel;
    QRLabel189: TQRLabel;
    QRLabel190: TQRLabel;
    QRLabel191: TQRLabel;
    QRImage6: TQRImage;
    QRLabel192: TQRLabel;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabel195: TQRLabel;
    QRShape78: TQRShape;
    edtAdvName: TQRLabel;
    QRLabel196: TQRLabel;
    QRShape79: TQRShape;
    edtSignName: TQRLabel;
    QRLabel197: TQRLabel;
    QRShape80: TQRShape;
    edtSignID: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel199: TQRLabel;
    QRShape81: TQRShape;
    QRShape82: TQRShape;
    QRLabel200: TQRLabel;
    edtAdvMeametName: TQRLabel;
    QRShape83: TQRShape;
    QRLabel201: TQRLabel;
    edtAdvMeametAddress: TQRLabel;
    QRShape84: TQRShape;
    QRLabel202: TQRLabel;
    QRLabel203: TQRLabel;
    edtAdvMeametID: TQRLabel;
    QRLabel204: TQRLabel;
    edtAdvMeametLicence: TQRLabel;
    QRShape86: TQRShape;
    QRLabel205: TQRLabel;
    QRShape87: TQRShape;
    QRLabel206: TQRLabel;
    edtAccName: TQRLabel;
    QRShape88: TQRShape;
    QRLabel207: TQRLabel;
    edtSignName2: TQRLabel;
    QRShape89: TQRShape;
    QRLabel208: TQRLabel;
    edtSignID2: TQRLabel;
    QRShape90: TQRShape;
    QRLabel209: TQRLabel;
    QRShape91: TQRShape;
    QRLabel210: TQRLabel;
    QRShape92: TQRShape;
    QRLabel211: TQRLabel;
    QRLabel212: TQRLabel;
    QRShape93: TQRShape;
    QRShape94: TQRShape;
    QRLabel213: TQRLabel;
    QRShape95: TQRShape;
    edtAccMeametName: TQRLabel;
    edtAccMeametAddress: TQRLabel;
    edtAccMeametID: TQRLabel;
    edtAccMeametLicence: TQRLabel;
    QRLabel214: TQRLabel;
    QRShape96: TQRShape;
    QRShape97: TQRShape;
    QRLabel215: TQRLabel;
    QRLabel216: TQRLabel;
    QRShape98: TQRShape;
    QRLabel217: TQRLabel;
    QRLabel218: TQRLabel;
    QRShape99: TQRShape;
    QRLabel219: TQRLabel;
    QRLabel220: TQRLabel;
    QRShape100: TQRShape;
    QRLabel221: TQRLabel;
    QRLabel222: TQRLabel;
    QRShape101: TQRShape;
    QRLabel223: TQRLabel;
    QuickRep2: TQuickRep;
    QRBand2: TQRBand;
    QRImage1: TQRImage;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRImage2: TQRImage;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    edtTOtalMuhaz: TQRLabel;
    edtStocksNumInShtar1: TQRLabel;
    edtShtarNum1: TQRLabel;
    edtStocksNumInShtar2: TQRLabel;
    edtShtarNum2: TQRLabel;
    QRLabel75: TQRLabel;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRLabel76: TQRLabel;
    QRShape30: TQRShape;
    QRLabel77: TQRLabel;
    QRShape31: TQRShape;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    edtDirectorName1: TQRLabel;
    edtDirectorZeut1: TQRLabel;
    edtDirectorStartDate1: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    edtDirectorAddress1: TQRLabel;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    edtDirectorName2: TQRLabel;
    edtDirectorZeut2: TQRLabel;
    edtDirectorStartDate2: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    edtDirectorAddress2: TQRLabel;
    QRLabel88: TQRLabel;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel91: TQRLabel;
    edtDirectorName3: TQRLabel;
    edtDirectorZeut3: TQRLabel;
    edtDirectorStartDate3: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    edtDirectorAddress3: TQRLabel;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    edtDirectorName4: TQRLabel;
    edtDirectorZeut4: TQRLabel;
    edtDirectorStartDate4: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    edtDirectorAddress4: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRShape38: TQRShape;
    QRLabel102: TQRLabel;
    QRShape39: TQRShape;
    QRLabel103: TQRLabel;
    QRShape40: TQRShape;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    edtNoDirectorName1: TQRLabel;
    edtNoDirectorZeut1: TQRLabel;
    edtNoDirectorEndDate1: TQRLabel;
    QRShape41: TQRShape;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    edtNoDirectorName2: TQRLabel;
    edtNoDirectorZeut2: TQRLabel;
    edtNoDirectorEndDate2: TQRLabel;
    QRShape42: TQRShape;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    edtNoDirectorName3: TQRLabel;
    edtNoDirectorZeut3: TQRLabel;
    edtNoDirectorEndDate3: TQRLabel;
    QRShape43: TQRShape;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    edtNoDirectorName4: TQRLabel;
    edtNoDirectorZeut4: TQRLabel;
    edtNoDirectorEndDate4: TQRLabel;
    QRLabel118: TQRLabel;
    QRShape44: TQRShape;
    edtDirectorNotChanged: TQRLabel;
    QRLabel119: TQRLabel;
    QRShape45: TQRShape;
    edtDirectorsChanged: TQRLabel;
    QRLabel120: TQRLabel;
    edtAppendix2: TQRLabel;
    QuickRep3: TQuickRep;
    QRBand3: TQRBand;
    QRImage3: TQRImage;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel124: TQRLabel;
    QRImage4: TQRImage;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRShape46: TQRShape;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    edtMusmakhName: TQRLabel;
    edtMusmakhZeut: TQRLabel;
    edtMusmakhRole: TQRLabel;
    QRLabel134: TQRLabel;
    QRShape50: TQRShape;
    edtDirectorionApproved: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRShape51: TQRShape;
    edtReportsDisplayed: TQRLabel;
    QRLabel144: TQRLabel;
    QRShape52: TQRShape;
    edtDoesNotHaveAnnualAssembly: TQRLabel;
    QRLabel145: TQRLabel;
    QRShape53: TQRShape;
    edtDoesNotHaveToMakeReports: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRShape54: TQRShape;
    edtThereIsAnAccountant: TQRLabel;
    QRLabel156: TQRLabel;
    QRShape55: TQRShape;
    edtAccountantNameIs: TQRLabel;
    QRLabel157: TQRLabel;
    QRShape56: TQRShape;
    edtAccountantName: TQRLabel;
    QRLabel158: TQRLabel;
    QRShape57: TQRShape;
    edtAccountantAddress: TQRLabel;
    QRShape58: TQRShape;
    edtNoAccountant: TQRLabel;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    QRShape59: TQRShape;
    edtDaysWithoutAccountant: TQRLabel;
    QRLabel162: TQRLabel;
    QRShape60: TQRShape;
    edtNonActive: TQRLabel;
    QRLabel163: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    QRShape61: TQRShape;
    QRLabel167: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel169: TQRLabel;
    QRLabel170: TQRLabel;
    QRShape62: TQRShape;
    QRShape63: TQRShape;
    QRShape64: TQRShape;
    QRShape65: TQRShape;
    edtManagerName: TQRLabel;
    edtManagerID: TQRLabel;
    edtManagerAddress: TQRLabel;
    edtManagerPhone: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRLabel173: TQRLabel;
    QRLabel174: TQRLabel;
    QRShape66: TQRShape;
    QRLabel175: TQRLabel;
    QRShape67: TQRShape;
    QRLabel176: TQRLabel;
    QRShape68: TQRShape;
    QRLabel177: TQRLabel;
    QRShape70: TQRShape;
    edtMemuneName: TQRLabel;
    edtMemuneID: TQRLabel;
    edtMemuneRole: TQRLabel;
    QRLabel179: TQRLabel;
    QRLabel180: TQRLabel;
    QRLabel181: TQRLabel;
    QRLabel182: TQRLabel;
    QRShape73: TQRShape;
    QRLabel183: TQRLabel;
    edtDateBroughtReports: TQRLabel;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    QRShape69: TQRShape;
    QRLabel178: TQRLabel;
    QRShape74: TQRShape;
    QRLabel186: TQRLabel;
    QRShape75: TQRShape;
    QRLabel187: TQRLabel;
    QRShape76: TQRShape;
    edtMatzhirName: TQRLabel;
    edtMatzhirID: TQRLabel;
    edtMatzhirRole: TQRLabel;
    QRLabel188: TQRLabel;
    QRShape77: TQRShape;
    QuickRep5: TQuickRep;
    QRBand5: TQRBand;
    QRLabel224: TQRLabel;
    QRLabel225: TQRLabel;
    edtCompanyName2: TQRLabel;
    QRLabel226: TQRLabel;
    edtCompNum2: TQRLabel;
    QRLabel227: TQRLabel;
    lCaption: TQRLabel;
    QRBand6: TQRBand;
    GroupHeaderBand1: TQRBand;
    QRLabel228: TQRLabel;
    QRShape102: TQRShape;
    QRLabel229: TQRLabel;
    QRLabel230: TQRLabel;
    QRShape103: TQRShape;
    QRShape104: TQRShape;
    QRShape105: TQRShape;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel233: TQRLabel;
    QRLabel234: TQRLabel;
    QRLabel235: TQRLabel;
    QRShape161: TQRShape;
    QRLabel245: TQRLabel;
    QRLabel247: TQRLabel;
    QRLabel249: TQRLabel;
    edtAppendixTotalHon: TQRLabel;
    QRLabel259: TQRLabel;
    QRLabel250: TQRLabel;
    GroupHeaderBand2: TQRBand;
    QRShape106: TQRShape;
    QRShape107: TQRShape;
    QRLabel236: TQRLabel;
    QRShape108: TQRShape;
    QRLabel237: TQRLabel;
    QRLabel238: TQRLabel;
    QRShape109: TQRShape;
    QRLabel239: TQRLabel;
    QRLabel240: TQRLabel;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    QRShape110: TQRShape;
    QRLabel243: TQRLabel;
    QRShape111: TQRShape;
    QRShape112: TQRShape;
    QRLabel244: TQRLabel;
    QRShape113: TQRShape;
    QRLabel246: TQRLabel;
    QRLabel248: TQRLabel;
    QRLabel251: TQRLabel;
    QRShape114: TQRShape;
    QRShape115: TQRShape;
    QRShape116: TQRShape;
    QRLabel252: TQRLabel;
    QRLabel253: TQRLabel;
    QRLabel254: TQRLabel;
    QRLabel255: TQRLabel;
    QRLabel256: TQRLabel;
    QRLabel257: TQRLabel;
    QRLabel258: TQRLabel;
    QRBand7: TQRBand;
    QRLabel260: TQRLabel;
    QRShape118: TQRShape;
    QRShape119: TQRShape;
    QRLabel261: TQRLabel;
    QRLabel262: TQRLabel;
    QRLabel263: TQRLabel;
    QRBand8: TQRBand;
    QRShape120: TQRShape;
    QRShape121: TQRShape;
    QRLabel264: TQRLabel;
    QRShape123: TQRShape;
    QRLabel265: TQRLabel;
    QRLabel266: TQRLabel;
    QRShape124: TQRShape;
    QRLabel267: TQRLabel;
    QRLabel268: TQRLabel;
    QRLabel269: TQRLabel;
    QRLabel270: TQRLabel;
    QRShape126: TQRShape;
    QRLabel271: TQRLabel;
    QRShape127: TQRShape;
    QRShape129: TQRShape;
    QRLabel272: TQRLabel;
    QRShape130: TQRShape;
    QRLabel273: TQRLabel;
    QRLabel274: TQRLabel;
    QRLabel275: TQRLabel;
    QRShape131: TQRShape;
    QRShape132: TQRShape;
    QRLabel276: TQRLabel;
    QRLabel277: TQRLabel;
    QRShape133: TQRShape;
    QRLabel278: TQRLabel;
    QRShape134: TQRShape;
    QRLabel279: TQRLabel;
    QRBand9: TQRBand;
    QRShape142: TQRShape;
    QRLabel280: TQRLabel;
    QRShape139: TQRShape;
    QRShape141: TQRShape;
    QRLabel281: TQRLabel;
    QRLabel282: TQRLabel;
    QRShape143: TQRShape;
    QRLabel283: TQRLabel;
    QRLabel284: TQRLabel;
    QRLabel285: TQRLabel;
    QRLabel286: TQRLabel;
    QRShape145: TQRShape;
    QRShape146: TQRShape;
    QRLabel287: TQRLabel;
    QRLabel288: TQRLabel;
    QRShape148: TQRShape;
    QRLabel289: TQRLabel;
    QRShape149: TQRShape;
    QRLabel290: TQRLabel;
    QRBand10: TQRBand;
    QRShape151: TQRShape;
    QRShape152: TQRShape;
    QRShape153: TQRShape;
    QRShape150: TQRShape;
    QRLabel291: TQRLabel;
    QRLabel292: TQRLabel;
    QRLabel293: TQRLabel;
    QRLabel294: TQRLabel;
    QRLabel295: TQRLabel;
    QRLabel296: TQRLabel;
    QRLabel297: TQRLabel;
    PageFooterBand1: TQRBand;
    QRSubDetail1: TQRSubDetail;
    QRShape117: TQRShape;
    QRDBText10: TQRDBText;
    QRShape122: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRShape125: TQRShape;
    QRShape128: TQRShape;
    QRSubDetail2: TQRSubDetail;
    QRShape135: TQRShape;
    QRDBText5: TQRDBText;
    QRShape136: TQRShape;
    QRShape137: TQRShape;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRShape138: TQRShape;
    QRDBText6: TQRDBText;
    QRShape140: TQRShape;
    QRShape144: TQRShape;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRShape147: TQRShape;
    QRShape154: TQRShape;
    QRSubDetail3: TQRSubDetail;
    QRShape155: TQRShape;
    QRDBText15: TQRDBText;
    QRShape156: TQRShape;
    QRDBText16: TQRDBText;
    QRSubDetail4: TQRSubDetail;
    QRShape157: TQRShape;
    QRDBText17: TQRDBText;
    QRShape158: TQRShape;
    QRShape159: TQRShape;
    QRShape160: TQRShape;
    QRShape162: TQRShape;
    QRShape163: TQRShape;
    QRShape164: TQRShape;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRSubDetail5: TQRSubDetail;
    QRShape165: TQRShape;
    QRDBText24: TQRDBText;
    QRShape166: TQRShape;
    QRShape167: TQRShape;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRSubDetail6: TQRSubDetail;
    QRShape168: TQRShape;
    QRShape169: TQRShape;
    QRShape170: TQRShape;
    QRShape171: TQRShape;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    taMemPastDirector: TMemDataSet;
    taMemManager: TMemDataSet;
    taMemMuhaz: TMemDataSet;
    taMemDirector: TMemDataSet;
    taMemHon: TMemDataSet;
    taMemStocks: TMemDataSet;
    QRDBText2: TQRDBText;
    edtAppendix3: TQRLabel;
    QRLabel299: TQRLabel;
    QRLabel298: TQRLabel;
    QRLabel300: TQRLabel;
    sp3: TQRShape;
    lblAStockName2: TQRLabel;
    lblStockNameA2: TQRLabel;
    QRLabel304: TQRLabel;
    edtStockName2: TQRLabel;
    edtSaidStockValue2: TQRLabel;
    edtStockTypeA2: TQRLabel;
    lblRashum2: TQRLabel;
    edtRashum2: TQRLabel;
    lblAssigned2: TQRLabel;
    edtAssignedStocksNumber2: TQRLabel;
    lblStockValue2: TQRLabel;
    edtTotalStockValue2: TQRLabel;
    sp4: TQRShape;
    lblStockName3: TQRLabel;
    lblStockNameA3: TQRLabel;
    edtStockName3: TQRLabel;
    edtSaidStockValue3: TQRLabel;
    lblStockType3: TQRLabel;
    edtStockTypeA3: TQRLabel;
    lblRashum3: TQRLabel;
    edtRashum3: TQRLabel;
    lblAssigned3: TQRLabel;
    edtAssignedStocksNumber3: TQRLabel;
    lblStockValue3: TQRLabel;
    edtTotalStockValue3: TQRLabel;
    edtAppendix1: TQRLabel;
    edtAppendix4: TQRLabel;
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    ShowAppendix,BigHon,BigMuhaz,BigDirectors,BigNoDirectors,BigStocks,BigManagers:boolean;
    { Private declarations }
    procedure LoadData(Company:TCompany;AnnualRepData:TAnnualRepData);
  public
    { Public declarations }
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintExecute(const FileName: string; Company:TCompany;AnnualRepData:TAnnualRepData);
    procedure PrintEmpty(const FileName: String);
  end;

var
  fmQrAnnualReport: TfmQrAnnualReport;

implementation

uses PrinterSetup, utils2;
constructor TAnnualRepData.Create(ADirectorsChanged,ADirectorsNotChanged:boolean;AMusmakhName,AMusmakhZeut,AMusmakhRole:string;
       AReportsApproved,AReportsDisplayed,ADoesNotHaveAnnualAssembly,ADoesNotHaveToMakeReports,
       AThereIsAnAccountant,AAccountantNameIs,ANoAccountant,ANonActive:boolean;
       AAccountantName,AAccountantAddress:string;ADaysWithoutAccountant:integer;
       AManagerName,AManagerID,AManagerAddress,AManagerPhone,AMemuneName,AMemuneID,AMemuneRole:string;
       AReportBrought,AReportSent:boolean;AReportSentAt:TDateTime;
       AMatzhirName,AMatzhirID,AMatzhirRole,AAdvName,AAdvSignName,AAdvSignID,
       AAdvMeametName,AAdvMeametAddress,AAdvMeametID,AAdvMeametLicence,
       AAccName,AAccSignName,AAccSignID,AAccMeametName,AAccMeametAddress,AAccMeametID,AAccMeametLicence:string);
begin
  FDirectorsChanged:=ADirectorsChanged;
  FDirectorsNotChanged:=ADirectorsNotChanged;
  FMusmakhName:=AMusmakhName;
  FMusmakhZeut:=AMusmakhZeut;
  FMusmakhRole:=AMusmakhRole;
  FReportsApproved:=AReportsApproved;
  FReportsDisplayed:=AReportsDisplayed;
  FDoesNotHaveAnnualAssembly:=ADoesNotHaveAnnualAssembly;
  FDoesNotHaveToMakeReports:=ADoesNotHaveToMakeReports;
  FThereIsAnAccountant:=AThereIsAnAccountant;
  FAccountantNameIs:=AAccountantNameIs;
  FNoAccountant:=ANoAccountant;
  FNonActive:=ANonActive;
  FAccountantName:=AAccountantName;
  FAccountantAddress:=AAccountantAddress;
  FDaysWithoutAccountant:=ADaysWithoutAccountant;
  FManagerName:=AManagerName;
  FManagerID:=AManagerID;
  FManagerAddress:=AManagerAddress;
  FManagerPhone:=AManagerPhone;
  FMemuneName:=AMemuneName;
  FMemuneID:=AMemuneID;
  FMemuneRole:=AMemuneRole;
  FReportBrought:=AReportBrought;
  FReportSent:=AReportSent;
  FReportSentAt:=AReportSentAt;
  FMatzhirName:=AMatzhirName;
  FMatzhirID:=AMatzhirID;
  FMatzhirRole:=AMatzhirRole;
  FAdvName:=AAdvName;
  FAdvSignName:=AAdvSignName;
  FAdvSignID:=AAdvSignID;
  FAdvMeametName:=AAdvMeametName;
  FAdvMeametAddress:=AAdvMeametAddress;
  FAdvMeametID:=AAdvMeametID;
  FAdvMeametLicence:=AAdvMeametLicence;
  FAccName:=AAccName;
  FAccSignName:=AAccSignName;
  FAccSignID:=AAccSignID;
  FAccMeametName:=AAccMeametName;
  FAccMeametAddress:=AAccMeametAddress;
  FAccMeametID:=AAccMeametID;
  FAccMeametLicence:=AAccMeametLicence;
end;

destructor TAnnualRepData.Free;
begin
end;

function TAnnualRepData.GetManagerCount:integer;
begin
  Result:=GetCommaTextCount(FManagerName);
end;

function TAnnualRepData.GetManagerName(index:integer):string;
begin
  Result:=GetCommaText(FManagerName,index);
end;

function TAnnualRepData.GetManagerID(index:integer):string;
begin
  Result:=GetCommaText(FManagerID,index);
end;

function TAnnualRepData.GetManagerAddress(index:integer):string;
begin
  Result:=GetCommaText(FManagerAddress,index);
end;

function TAnnualRepData.GetManagerPhone(index:integer):string;
begin
   Result:=GetCommaText(FManagerPhone,index);
end;
{$R *.DFM}
procedure TfmQrAnnualReport.LoadData(Company:TCompany;AnnualRepData:TAnnualRepData);
var
  i,j,nStocks1,nStocks2,nStocks3:integer;
  totalCapital,totalMuhaz:double;
  strName,strID, strAddress,strType,strCount, strPaid:string;
  strZeut,strShtarValue:string;
  strDirName,strDirZeut,strDirStartDate,strDirAddress,strDirEndDate:string;
  DirCount1,DirCount2,p1,p2:integer;
  strStockName,strSaidStockValue,strStockType,strRashum,strTotalStockValue,strAssignedStocksNumber:string;
begin
  ShowAppendix:=False;
  BigHon:=False;
  BigMuhaz:=False;
  BigDirectors:=False;
  BigNoDirectors:=False;
  BigStocks:=False;
  BigManagers:=False;
  if (Company=Nil)or(AnnualRepData=nil) then
    begin
      for i:=0 to ComponentCount-1 do
        if (Components[i] is TQRLabel)and(uppercase(Copy(Components[i].Name,1,3))='EDT')then
          (Components[i] as TQRLabel).Caption:='';
      exit;
    end;
  SetChecked(edtDirectorNotChanged,AnnualRepData.DirectorsChanged,0);
  SetChecked(edtDirectorsChanged,AnnualRepData.DirectorsNotChanged,0);
  edtMusmakhName.Caption:=AnnualRepData.MusmakhName;
  edtMusmakhZeut.Caption:=FormatInt(AnnualRepData.MusmakhZeut);
  edtMusmakhRole.Caption:=AnnualRepData.MusmakhRole;
  SetChecked(edtDirectorionApproved,AnnualRepData.ReportsApproved,0);
  SetChecked(edtReportsDisplayed,AnnualRepData.ReportsDisplayed,0);
  SetChecked(edtDoesNotHaveAnnualAssembly,AnnualRepData.DoesNotHaveAnnualAssembly,0);
  SetChecked(edtDoesNotHaveToMakeReports,AnnualRepData.DoesNotHaveToMakeReports,0);
  SetChecked(edtThereIsAnAccountant,AnnualRepData.ThereIsAnAccountant,0);
  SetChecked(edtAccountantNameIs,AnnualRepData.AccountantNameIs,0);
  SetChecked(edtNoAccountant,AnnualRepData.NoAccountant,0);
  SetChecked(edtNonActive,AnnualRepData.NonActive,0);
  edtAccountantName.Caption:=AnnualRepData.AccountantName;
  edtAccountantAddress.Caption:=AnnualRepData.AccountantAddress;
  if AnnualRepData.DaysWithoutAccountant>0 then edtDaysWithoutAccountant.Caption:=IntToStr(AnnualRepData.DaysWithoutAccountant) else edtDaysWithoutAccountant.Caption:='';
  BigManagers:=(AnnualRepData.ManagerCount>1);
  if not BigManagers then
    edtAppendix3.Caption:='';
  edtManagerName.Caption:=AnnualRepData.ManagerName[0];
  edtManagerID.Caption:=FormatInt(AnnualRepData.ManagerID[0]);
  edtManagerAddress.Caption:=AnnualRepData.ManagerAddress[0];
  edtManagerPhone.Caption:=FormatInt(AnnualRepData.ManagerPhone[0]);
  edtMemuneName.Caption:=AnnualRepData.MemuneName;
  edtMemuneID.Caption:=FormatInt(AnnualRepData.MemuneID);
  edtMemuneRole.Caption:=AnnualRepData.MemuneRole;
 // SetChecked(edtVIDeclare,AnnualRepData.ReportBrought,0);
 // SetChecked(edtVReportsBrought,AnnualRepData.ReportSent,0);
  edtDateBroughtReports.Caption:=DateToStr(AnnualRepData.ReportSentAt);
  edtMatzhirName.Caption:=AnnualRepData.MatzhirName;
  edtMatzhirID.Caption:=FormatInt(AnnualRepData.MatzhirID);
  edtMatzhirRole.Caption:=AnnualRepdata.MatzhirRole;
  edtAdvName.Caption:=AnnualRepdata.AdvName;
  edtSignName.Caption:=AnnualRepdata.AdvSignName;
  edtSignID.Caption:=FormatInt(AnnualRepdata.AdvSignID);
  edtAdvMeametName.Caption:=AnnualRepdata.AdvMeametName;
  edtAdvMeametAddress.Caption:=AnnualRepdata.AdvMeametAddress;
  edtAdvMeametID.Caption:=FormatInt(AnnualRepdata.AdvMeametID);
  edtAdvMeametLicence.Caption:=FormatInt(AnnualRepData.AdvMeametLicence);
  edtAccName.Caption:=AnnualRepdata.AccName;
  edtSignName2.Caption:=AnnualRepdata.AccSignName;
  edtSignID2.Caption:=FormatInt(AnnualRepdata.AccSignID);
  edtAccMeametName.Caption:=AnnualRepdata.AccMeametName;
  edtAccMeametAddress.Caption:=AnnualRepdata.AccMeametAddress;
  edtAccMeametID.Caption:=FormatInt(AnnualRepdata.AccMeametID);
  edtAccMeametLicence.Caption:=FormatInt(AnnualRepdata.AccMeametLicence);

  edtCompanyName.Caption:=Company.Name;
  edtCompNum.Caption:=FormatInt(Company.Zeut);
  edtCompanyAddress.Caption:=FormatAddress(Company.Address.Street,Company.Address.Number,Company.Address.City,Company.Address.Index,Company.Address.Country);
  edtCompanyPhone.Caption:=FormatInt(Company.Address.Phone);
  edtCompanyEmail.Caption:=Company.CompanyInfo.Mail;
  edtREportIsUpToDateAt.Caption:=DateToStr(Company.CompanyInfo.RegistrationDate);
  edtAnnualAssemblyDate.Caption:=DateToStr(Company.ShowingDate);
  Company.HonList.Filter:=afActive;
  BigHon:=(Company.HonList.Count>3);
  totalCapital:=0;
  for i:=0 to Company.HonList.Count-1 do
    with Company.HonList.Items[i] as THonItem do totalCapital:=totalCapital+Value*Rashum;
  edtTotalCapital.Caption:=FloatToStr(totalCapital);
  for i:=0 to 2 do
    begin
      if (Company.HonList.Count>i)and (not BigHon) then
        begin
          strStockName:=Company.HonList.Items[i].Name;
          strSaidStockValue:=FloatToStr(THonItem(Company.HonList.Items[i]).Value);
          strStockType:=Company.HonList.Items[i].Zeut;
          strRashum:=FloatToStr(THonItem(Company.HonList.Items[i]).Rashum);
          strTotalStockValue:=FloatToStr(THonItem(Company.HonList.Items[i]).Rashum * THonItem(Company.HonList.Items[i]).Value);
          strAssignedStocksNumber:=FloatToStr(THonItem(Company.HonList.Items[i]).Mokza);
        end
      else
        begin
          strStockName:='';
          strSaidStockValue:='';
          strStockType:='';
          strRashum:='';
          strTotalStockValue:='';
          strAssignedStocksNumber:='';
        end;
       SetCaption(self,'edtStockName'+IntToStr(i+1),strStockName);
       SetCaption(self,'edtSaidStockValue'+IntToStr(i+1),strSaidStockValue);
       SetCaption(self,'edtStockTypeA'+IntToStr(i+1),strStockType);
       SetCaption(self,'edtRashum'+IntToStr(i+1),strRashum);
       SetCaption(self,'edtTotalStockValue'+IntToStr(i+1),strTotalStockValue);
       SetCaption(self,'edtAssignedStocksNumber'+IntToStr(i+1),strAssignedStocksNumber);
       if BigHon then
         begin
           SetCaption(self,'lblRashum'+IntToStr(i+1),'');
           SetCaption(self,'lblStockName'+IntToStr(i+1),'');
           SetCaption(self,'lblStockNameA'+IntToStr(i+1),'');
           SetCaption(self,'lblAssigned'+IntToStr(i+1),'');
           SetCaption(self,'lblStockValue'+IntToStr(i+1),'');
         end;
    end;

  strName:='';
  strID:='';
  strAddress:='';
  strType:='';
  strCount:='';
  strPaid:='';
  Company.StockHolders.Filter:=afActive;
  nStocks1:=0;
  nStocks2:=0;
  nStocks3:=0;
  for i:=0 to 2 do
    begin
      if Company.StockHolders.Count>i then
        begin
          case i of
            0:nStocks1:=TStockHolder(Company.StockHolders.Items[i]).Stocks.Count;
            1:nStocks2:=TStockHolder(Company.StockHolders.Items[i]).Stocks.Count;
            2:nStocks3:=TStockHolder(Company.StockHolders.Items[i]).Stocks.Count;
          end;
          strName:=Company.StockHolders.Items[i].Name;
          strID:=FormatInt(Company.StockHolders.Items[i].Zeut);
          strAddress:=FormatAddress(TStockHolder(Company.StockHolders.Items[i]).Address.Street,TStockHolder(Company.StockHolders.Items[i]).Address.Number,
                         TStockHolder(Company.StockHolders.Items[i]).Address.City,TStockHolder(Company.StockHolders.Items[i]).Address.Index,
                         TStockHolder(Company.StockHolders.Items[i]).Address.Country);
          TStockHolder(Company.StockHolders.Items[i]).Stocks.Filter:=afActive;
          strCount:=IntToStr(TStockHolder(Company.StockHolders.Items[i]).Stocks.Count);
          if TStockHolder(Company.StockHolders.Items[i]).Stocks.Count>0 then
            begin
              strType:=TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[0].Name;
              strPaid:=FloatToStr(TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[0]).Paid);
            end;
        end;
       SetCaption(self,'edtStockHolderName'+IntToStr(i+1),strName);
       SetCaption(self,'edtStockHolderID'+IntToStr(i+1),strID);
       SetCaption(self,'edtStockHolderAddress'+IntToStr(i+1),strAddress);
       SetCaption(self,'edtStockType'+IntToStr(i+1),strType);
       SetCaption(self,'edtStocksNum'+IntToStr(i+1),strCount);
       SetCaption(self,'edtSumNotPaid'+IntToStr(i+1),strPaid);
    end;
  BigStocks:=(Company.StockHolders.Count>3)or(nStocks1>1)or(nStocks2>1)or(nStocks3>1);
  if BigStocks then
    begin
      for i:=1 to 3 do
        begin
          SetCaption(self,'edtStockHolderName'+IntToStr(i),'');
          SetCaption(self,'edtStockHolderID'+IntToStr(i),'');
          SetCaption(self,'edtStockHolderAddress'+IntToStr(i),'');
          SetCaption(self,'edtStockType'+IntToStr(i),'');
          SetCaption(self,'edtStocksNum'+IntToStr(i),'');
          SetCaption(self,'edtSumNotPaid'+IntToStr(i),'');

          SetCaption(self,'lblStockHolderName'+IntToStr(i),'');
          SetCaption(self,'lblStockTypeA'+IntToStr(i),'');
          SetCaption(self,'lblStockHolderID'+IntToStr(i),'');
          SetCaption(self,'lblStocksNum'+IntToStr(i),'');
          SetCaption(self,'lblStockHolderAddress'+IntToStr(i),'');
          SetCaption(self,'lblStockHolderAddressA'+IntToStr(i),'');
          SetCaption(self,'lblSumNotPaid'+IntToStr(i),'');
        end;

    end;

  if not BigHon then
//  if (Company.HonList.Count<=1) and (Company.StockHolders.Count<=3) and (nStocks1<=1) and (nStocks2<=1) and (nStocks3<=1) then
    edtAppendix1.Caption:='';
  if not BigStocks then
    edtAppendix4.Caption:='';
  totalMuhaz:=0;
  Company.MuhazList.Filter:=afActive;
  BigMuhaz:=(Company.MuhazList.Count>2);
  for i:=0 to Company.MuhazList.Count-1 do
    totalMuhaz:=totalMuhaz+TMuhazItem(Company.MuhazList.Items[i]).ShtarValue;
  edtTOtalMuhaz.Caption:=FloatToStr(totalMuhaz);
  for i:=0 to 1 do
    begin
      if Company.MuhazList.Count>i then
        begin
          strZeut:=Company.MuhazList.Items[i].Zeut;
          strShtarValue:=FloatToStr(TMuhazItem(Company.MuhazList.Items[i]).ShtarValue);
        end
      else
        begin
          strZeut:='';
          strShtarValue:='';
        end;
      SetCaption(self,'edtStocksNumInShtar'+IntToStr(i+1),strShtarValue);
      SetCaption(self,'edtShtarNum'+IntToStr(i+1),strZeut);
    end;
  Company.Directors.Filter:=afActive;
  DirCount1:=Company.Directors.Count;
  BigDirectors:=(DirCount1>4);
  for i:=0 to 3 do
    begin
      if Company.Directors.Count>i then
        begin
          strDirName:=Company.Directors.Items[i].Name;
          strDirZeut:=FormatInt(Company.Directors.Items[i].Zeut);
          strDirStartDate:=FormatDateTime('dd/mm/yyyy',TDirector(Company.Directors.Items[i]).Date1);
          strDirAddress:=FormatAddress(TDirector(Company.Directors.Items[i]).Address.Street, TDirector(Company.Directors.Items[i]).Address.Number,
                                 TDirector(Company.Directors.Items[i]).Address.City,TDirector(Company.Directors.Items[i]).Address.Index,
                                 TDirector(Company.Directors.Items[i]).Address.Country);
        end
      else
        begin
          strDirName:='';
          strDirZeut:='';
          strDirStartDate:='';
          strDirAddress:='';
        end;
      SetCaption(self,'edtDirectorName'+IntToStr(i+1),strDirName);
      SetCaption(self,'edtDirectorZeut'+IntToStr(i+1),strDirZeut);
      SetCaption(self,'edtDirectorStartDate'+IntToStr(i+1),strDirStartDate);
      SetCaption(self,'edtDirectorAddress'+IntToStr(i+1),strDirAddress);
    end;

  Company.Directors.Filter:=afNonActive;
  DirCount2:=Company.Directors.Count;
  BigNoDirectors:=(DirCount2>4);
  for i:=0 to 3 do
    begin
      if Company.Directors.Count>i then
        begin
          strDirName:=Company.Directors.Items[i].Name;
          strDirZeut:=FormatInt(Company.Directors.Items[i].Zeut);
          strDirEndDate:=FormatDateTime('dd/mm/yyyy',TDirector(Company.Directors.Items[i]).Date2);
        end
      else
        begin
          strDirName:='';
          strDirZeut:='';
          strDirEndDate:='';
        end;
     SetCaption(self,'edtNoDirectorName'+IntToStr(i+1),strDirName);
     SetCaption(self,'edtNoDirectorZeut'+IntToStr(i+1),strDirZeut);
     SetCaption(self,'edtNoDirectorEndDate'+IntToStr(i+1),strDirEndDate);
    end;
  if not (BigDirectors or BigNoDirectors or BigMuhaz) then
 // if (DirCount1<=4)and(DirCount2<=4)and(Company.Muhazlist.Count<=2)then
    edtAppendix2.Caption:='';
  ShowAppendix:=(BigHon or BigStocks or BigDirectors or BigNoDirectors or BigMuhaz or BigManagers);
  if ShowAppendix then
    begin
       StartTable(taMemHon);
       StartTable(taMemStocks);
       StartTable(taMemDirector);
       StartTable(taMemPastDirector);
       StartTable(taMemMuhaz);
       StartTable(taMemManager);
    end;
  if BigHon then
    begin

      Company.HonList.Filter:=afActive;
      for i:=0 to Company.HonList.Count-1 do
        begin
          taMemHon.Append;
          taMemHon.FieldByName('Name').AsString:=Company.HonList.Items[i].Name;
          taMemHon.FieldByName('Type').AsString:=Company.HonList.Items[i].Zeut;
          taMemHon.FieldByName('Value').AsFloat:=THonItem(Company.HonList.Items[i]).Value;
          taMemHon.FieldByName('Rashum').AsFloat:=THonItem(Company.HonList.Items[i]).Rashum;
          taMemHon.FieldByName('Mokza').AsFloat:=THonItem(Company.HonList.Items[i]).Mokza;
          taMemHon.Post;
        end;
    end;
  if BigStocks then
    begin
      Company.StockHolders.Filter:=afActive;
      for i:=0 to Company.StockHolders.Count-1 do
        begin
          TStockHolder(Company.StockHolders.Items[i]).Stocks.Filter:=afActive;
          for j:=0 to TStockHolder(Company.StockHolders.Items[i]).Stocks.Count-1 do
            begin
              taMemStocks.Append;
              taMemStocks.FieldByName('Name').AsString:=Company.StockHolders.Items[i].Name;
              taMemStocks.FieldByName('Zeut').AsString:=FormatInt(Company.StockHolders.Items[i].Zeut);
              taMemStocks.FieldByName('City').AsString:=TStockHolder(Company.StockHolders.Items[i]).Address.City;
              taMemStocks.FieldByName('Street').AsString:=TStockHolder(Company.StockHolders.Items[i]).Address.Street;
              taMemStocks.FieldByName('Number').AsString:=FormatInt(TStockHolder(Company.StockHolders.Items[i]).Address.Number);
              taMemStocks.FieldByName('Index').AsString:=FormatInt(TStockHolder(Company.StockHolders.Items[i]).Address.Index);
              taMemStocks.FieldByName('StockName').AsString:=TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j].Name;
              taMemStocks.FieldByName('StockCount').AsFloat:=TSTock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count;
              taMemStocks.FieldByName('StockPaid').AsFloat:=TSTock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Paid;
              taMemStocks.Post;
            end;
        end;
    end;
  if BigDirectors then
    begin
      Company.Directors.Filter:=afActive;
      for i:=0 to Company.Directors.Count-1 do
        begin
          taMemDirector.Append;
          taMemDirector.FieldByName('Name').AsString:=Company.Directors.Items[i].Name;
          taMemDirector.FieldByName('Zeut').AsString:=FormatInt(Company.Directors.Items[i].Zeut);
          taMemDirector.FieldByName('City').AsString:=TDirector(Company.Directors.Items[i]).Address.City;
          taMemDirector.FieldByName('Street').AsString:=TDirector(Company.Directors.Items[i]).Address.Street;
          taMemDirector.FieldByName('Number').AsString:=FormatInt(TDirector(Company.Directors.Items[i]).Address.Number);
          taMemDirector.FieldByName('Index').AsString:=FormatInt(TDirector(Company.Directors.Items[i]).Address.Index);
          taMemDirector.FieldByName('Date1').AsDateTime:=TDirector(Company.Directors.Items[i]).Date1;
          taMemDirector.Post;
        end;
    end;
 if BigNoDirectors then
   begin
     Company.Directors.Filter:=afNonActive;
     for i:=0 to Company.Directors.Count-1 do
       begin
         taMemPastDirector.Append;
         taMemPastDirector.FieldByName('Name').AsString:=Company.Directors.Items[i].Name;
         taMemPastDirector.FieldByName('Zeut').AsString:=FormatInt(Company.Directors.Items[i].Zeut);
         taMemPastDirector.FieldByName('Date').AsDateTime:=TDirector(Company.Directors.Items[i]).Date2;
         taMemPastDirector.Post;
       end;
   end;
 if BigMuhaz then
   begin
     Company.MuhazList.Filter:=afActive;
     for i:=0 to Company.MuhazList.Count-1 do
       begin
         taMemMuhaz.Append;
         taMemMuhaz.FieldByName('ShtarValue').AsFloat:=TMuhazItem(Company.MuhazList.Items[i]).ShtarValue;
         taMemMuhaz.FieldByName('Zeut').AsString:=FormatInt(Company.MuhazList.Items[i].Zeut);
         taMemMuhaz.Post;
       end;
   end;
 if BigManagers then
   begin
     for i:=0 to AnnualRepData.ManagerCount-1 do
       begin
         taMemManager.Append;
         taMemManager.FieldByName('Name').AsString:=AnnualRepData.ManagerName[i];
         taMemManager.FieldByName('Zeut').AsString:=FormatInt(AnnualRepData.ManagerID[i]);
         taMemManager.FieldByName('Address').AsString:=AnnualRepData.ManagerAddress[i];
         taMemManager.FieldByName('Telephone').AsString:=FormatInt(AnnualRepData.ManagerPhone[i]);
         taMemManager.Post;
       end;
   end;
 if ShowAppendix then
   begin
     edtCompanyName2.Caption:=Company.Name;
     edtCompNum2.Caption:=Company.Zeut;
     edtAppendixTotalHon.Caption:=FloatToStr(totalCapital);
   end;
 p1:=0;
 p2:=0;
 if BigHon then
   p1:=1
 else if BigStocks then
   p1:=5;
 if BigStocks then
   p2:=8
 else if BigHon then
   p2:=4;
 if (p1>0)and(p2>0)then
   for i:=p1 to p2 do
     (FindCOmponent('sp'+IntToStr(i)) as TQRShape).Visible:=False;
end;

procedure TfmQrAnnualReport.PrintProc(DoPrint: Boolean);
var i:integer;
begin
   for i:=1 to 5 do
       fmPrinterSetup.FormatQR(FindComponent('QuickRep'+IntToStr(i)) as TQuickRep);
   fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
   if DoPrint then
     QRCompositeReport1.Print
   else
     QRCompositeReport1.Preview;
   if ShowAppendix then
    // begin
      // Application.ProcessMessages;
       if DoPrint then
         QuickRep5.Print
       else
         QuickREp5.Preview;
      // Application.ProcessMessages;
    // end;  
end;

procedure TfmQrAnnualReport.PrintExecute(const FileName: string; Company:TCompany;AnnualRepData:TAnnualRepData);
begin
   LoadData(Company,AnnualRepData);
    fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQrAnnualReport.PrintEmpty(const FileName: String);
begin
   LoadData(Nil,Nil);
   fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQrAnnualReport.QRCompositeReport1AddReports(Sender: TObject);
var i:integer;
begin
  for i:=1 to 4 do
    QRCompositeReport1.Reports.Add(Self.FindComponent('QuickRep'+IntToStr(i)) as TQuickRep);
end;



procedure TfmQrAnnualReport.FormCreate(Sender: TObject);
begin
   ShowAppendix:=False;
   BigHon:=False;
   BigMuhaz:=False;
   BigDirectors:=False;
   BigNoDirectors:=False;
   BigStocks:=False;
   BigManagers:=False;
end;

procedure TfmQrAnnualReport.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QuickRep2.NewPage;
end;

procedure TfmQrAnnualReport.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QuickRep3.NewPage;
end;

procedure TfmQrAnnualReport.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QuickRep4.NewPage;
end;

procedure TfmQrAnnualReport.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QuickRep5.NewPage;
end;

end.
