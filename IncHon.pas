unit IncHon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  HonTemplate, HebForm, ExtCtrls, HGrids, SdGrid, ComCtrls, Buttons,
  StdCtrls, Mask, HLabel, DataObjects;

type
  TfmIncHon = class(TfmHonTemplate)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  procedure StockNames;
  protected
    procedure EvFillDefaultData; override;
  public
    { Public declarations }
  end;

var
  fmIncHon: TfmIncHon;

implementation

uses DataM;

{$R *.DFM}

procedure TfmIncHon.EvFillDefaultData;
begin
  inherited;
  nGrid.RowCount := 1;
  nGrid.Rows[0].Clear;
  Exit;

  DATA.taStocks.First;
  While Not DATA.taStocks.EOF Do
    begin
      With nGrid do
        begin
          Cells[0,RowCount-1] := IntToStr(RowCount);
          Cells[1,RowCount-1] := DATA.taStocks.FieldByName('STOCK').AsString;
          RowCount:= RowCount + 1;
        end;
      DATA.taStocks.Next;
    end;
  If nGrid.RowCount > 1 then
    nGrid.RowCount:= nGrid.RowCount - 1;
end;

procedure TfmIncHon.FormCreate(Sender: TObject);
begin
  FileAction:= faIncrease;
  inherited;
  IncAction:= True;
  if (Company <> nil) then
    StockNames;
end;

// Moshe 21/12/2017
// Only used shares
procedure TfmIncHon.StockNames;
var
  i, j: integer;
  HonItem: THonItem;
  Name, All: string;
begin
  All := '';
  for i := 0 to Company.HonList.Count-1 do
  begin
    HonItem := Company.HonList.Items[i] as THonItem;
    if (HonItem.Active) then
    begin
      Name := HonItem.Name;
      if (Pos(',' + Name + ',', All) < 1) then
      begin
        All := All + ',' + Name + ',';
        nGrid.Cells[0, nGrid.RowCount-1] := IntToStr(nGrid.RowCount);
        nGrid.Cells[1, nGrid.RowCount-1] := Name;
        nGrid.RowCount := nGrid.RowCount + 1;
      end;
    end;
  end;
end;

end.
