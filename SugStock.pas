unit SugStock;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HGrids, SdGrid, HebForm, ExtCtrls, ComCtrls, Buttons,
  StdCtrls, HLabel;

type
  TfmSugStock = class(TfmTemplate)
    Grid: TSdGrid;
    gb21: TPanel;
    gb22: TPanel;
    Bevel2: TBevel;
    procedure gb21Click(Sender: TObject);
    procedure gb22Click(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure EvFillDefaultData; override;
    procedure EvDeleteEmptyRows; override;
    function EvSaveData(Draft: Boolean): boolean; override;
  public
    { Public declarations }
  end;

var
  fmSugStock: TfmSugStock;

implementation

{$R *.DFM}

uses DataM, cSTD, Dialog2;

procedure TfmSugStock.EvDeleteEmptyRows;
var
  I: Integer;
begin
  For I:= Grid.RowCount-1 Downto 0 do
    if RemoveSpaces(Grid.Cells[0,I], rmBoth) = '' Then
      Grid.LineDeleteByIndex(I);
end;

procedure TfmSugStock.EvFillDefaultData;
begin
  Grid.RowCount:= 1;
  Grid.ResetLine(0);
  With Data.taStocks do
    begin
      First;
      While not EOF do
        begin
          Grid.Cells[0, Grid.RowCount-1]:= FieldByName('Stock').AsString;
          Grid.RowCount:= Grid.RowCount+1;
          Next;
        end;
    end;
  If Grid.RowCount>1 Then
    Grid.RowCount:= Grid.RowCount-1;
end;

function TfmSugStock.EvSaveData(Draft: Boolean): Boolean;
var
  I: Integer;
begin
  try
    With Data.taStocks do
    begin
      Close;
      Exclusive:= True;
      EmptyTable;
      Exclusive:= False;
      Open;
      For I:= 0 To Grid.RowCount-1 Do
        begin
          Append;
          FieldByName('Stock').AsString:= Grid.Cells[0,I];
          Post;
        end;
      fmDialog2.Show;
      Result:= True;
    end;
  finally
  end;
end;

procedure TfmSugStock.gb21Click(Sender: TObject);
begin
  inherited;
  Grid.LineDelete;
  DataChanged:= True;
end;

procedure TfmSugStock.gb22Click(Sender: TObject);
begin
  inherited;
  Grid.LineInsert;
end;

end.
