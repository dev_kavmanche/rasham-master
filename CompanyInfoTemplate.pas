{
Unit Name: CompanyInfoTemplate
Purpose  : show Company Info form
Author   : N/A
MODIFICATION HISTORY
DATE         BY    TASK      VERSION     DESCRIPTION
2019/08/26   sts   17674     1.7.7.0     fixed company data; code refactoring
}

unit CompanyInfoTemplate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, StdCtrls, HComboBx, HEdit, HebForm, ExtCtrls, ComCtrls,
  Buttons, HLabel, HGrids, SdGrid, Mask, DataObjects, cSTD;

type
  TfmCompanyInfoTemplate = class(TfmTemplate)
    Shape130: TShape;
    Shape3: TShape;
    Shape9: TShape;
    Shape13: TShape;
    Shape17: TShape;
    Shape19: TShape;
    Shape1: TShape;
    Shape2: TShape;
    l1: THebLabel;
    HebLabel2: THebLabel;
    ed: TShape;
    Shape6: TShape;
    Shape5: TShape;
    HebLabel6: THebLabel;
    HebLabel3: THebLabel;
    Shape10: TShape;
    Shape14: TShape;
    HebLabel7: THebLabel;
    Shape15: TShape;
    HebLabel8: THebLabel;
    HebLabel9: THebLabel;
    Shape18: TShape;
    Shape16: TShape;
    HebLabel10: THebLabel;
    Shape20: TShape;
    Shape8: TShape;
    Shape12: TShape;
    Shape11: TShape;
    Shape7: TShape;
    HebLabel4: THebLabel;
    HebLabel5: THebLabel;
    e11: THebEdit;
    e12: TEdit;
    e13: THebEdit;
    e15: THebComboBox;
    e17: THebEdit;
    e19: TEdit;
    e18: THebEdit;
    e110: TEdit;
    e16: TEdit;
    e14: THebEdit;
    TabSheet2: TTabSheet;
    Bevel2: TBevel;
    Grid2: TSdGrid;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    HebLabel11: THebLabel;
    HebLabel12: THebLabel;
    Panel10: TPanel;
    HebLabel13: THebLabel;
    HebLabel14: THebLabel;
    Panel11: TPanel;
    Panel26: TPanel;
    nGrid2: THebStringGrid;
    gb21: TPanel;
    gb22: TPanel;
    TabSheet3: TTabSheet;
    Shape38: TShape;
    Shape30: TShape;
    Shape39: TShape;
    Shape33: TShape;
    Shape42: TShape;
    Shape31: TShape;
    Shape40: TShape;
    Shape34: TShape;
    HebLabel17: THebLabel;
    Shape41: TShape;
    HebLabel21: THebLabel;
    Shape29: TShape;
    Shape27: TShape;
    Shape35: TShape;
    Shape36: TShape;
    Shape37: TShape;
    HebLabel19: THebLabel;
    HebLabel18: THebLabel;
    Shape28: TShape;
    HebLabel15: THebLabel;
    HebLabel22: THebLabel;
    Bevel3: TBevel;
    HebLabel20: THebLabel;
    Shape32: TShape;
    HebLabel16: THebLabel;
    Grid3: TSdGrid;
    e33: TEdit;
    e36: TEdit;
    e32: THebEdit;
    e38: THebComboBox;
    e37: THebComboBox;
    e31: THebComboBox;
    e34: THebEdit;
    Panel17: TPanel;
    Panel16: TPanel;
    Panel15: TPanel;
    Panel14: TPanel;
    Panel13: TPanel;
    Panel27: TPanel;
    nGrid3: THebStringGrid;
    gb31: TPanel;
    gb32: TPanel;
    e35: THebEdit;
    TabSheet4: TTabSheet;
    Shape55: TShape;
    Shape45: TShape;
    Shape65: TShape;
    Shape64: TShape;
    Shape62: TShape;
    Shape51: TShape;
    Shape52: TShape;
    Shape53: TShape;
    HebLabel28: THebLabel;
    HebLabel31: THebLabel;
    Shape63: TShape;
    HebLabel32: THebLabel;
    Shape61: TShape;
    HebLabel33: THebLabel;
    Shape66: TShape;
    HebLabel27: THebLabel;
    Shape60: TShape;
    HebLabel23: THebLabel;
    Shape43: TShape;
    Shape44: TShape;
    Shape46: TShape;
    HebLabel24: THebLabel;
    Shape47: TShape;
    Shape48: TShape;
    HebLabel25: THebLabel;
    Shape49: TShape;
    Shape50: TShape;
    HebLabel26: THebLabel;
    Shape57: TShape;
    HebLabel30: THebLabel;
    Shape56: TShape;
    HebLabel29: THebLabel;
    Shape54: TShape;
    Shape21: TShape;
    Shape23: TShape;
    HebLabel52: THebLabel;
    pnDir: TPanel;
    Shape59: TShape;
    Shape78: TShape;
    Shape75: TShape;
    Shape72: TShape;
    Shape70: TShape;
    Shape69: TShape;
    Shape80: TShape;
    HebLabel40: THebLabel;
    HebLabel35: THebLabel;
    HebLabel34: THebLabel;
    Shape67: TShape;
    Shape68: TShape;
    Shape79: TShape;
    HebLabel37: THebLabel;
    Shape71: TShape;
    HebLabel36: THebLabel;
    Shape73: TShape;
    HebLabel38: THebLabel;
    HebLabel39: THebLabel;
    Shape77: TShape;
    Shape76: TShape;
    Shape74: TShape;
    HebLabel41: THebLabel;
    e412: THebEdit;
    e414: THebEdit;
    e417: THebComboBox;
    e416: TEdit;
    e413: TEdit;
    e418: THebComboBox;
    e415: THebEdit;
    e41: THebComboBox;
    e42: THebComboBox;
    e43: TMaskEdit;
    e44: TMaskEdit;
    Panel19: TPanel;
    Panel28: TPanel;
    e45: THebEdit;
    e46: TEdit;
    e49: TEdit;
    e47: THebEdit;
    e410: THebComboBox;
    e411: THebComboBox;
    TempGrid4: TSdGrid;
    e48: THebEdit;
    e46b: TEdit;
    TabSheet5: TTabSheet;
    Shape88: TShape;
    Bevel4: TBevel;
    Shape89: TShape;
    HebLabel45: THebLabel;
    Shape87: TShape;
    Shape86: TShape;
    HebLabel44: THebLabel;
    Shape84: TShape;
    HebLabel42: THebLabel;
    Shape85: TShape;
    HebLabel43: THebLabel;
    HebLabel46: THebLabel;
    e53: THebEdit;
    e52: TEdit;
    e51: THebEdit;
    Panel23: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel29: TPanel;
    grid5: TSdGrid;
    gb51: TPanel;
    gb52: TPanel;
    TempGrid31: TSdGrid;
    TempGrid32: TSdGrid;
    TabSheet6: TTabSheet;
    Shape127: TShape;
    Shape91: TShape;
    Shape95: TShape;
    HebLabel53: THebLabel;
    Shape96: TShape;
    HebLabel56: THebLabel;
    Shape99: TShape;
    Shape113: TShape;
    HebLabel65: THebLabel;
    Shape114: TShape;
    HebLabel66: THebLabel;
    Shape115: TShape;
    HebLabel67: THebLabel;
    Shape116: TShape;
    Shape117: TShape;
    Shape118: TShape;
    HebLabel68: THebLabel;
    Shape119: TShape;
    HebLabel69: THebLabel;
    Shape120: TShape;
    Shape121: TShape;
    HebLabel70: THebLabel;
    Shape122: TShape;
    Shape123: TShape;
    HebLabel71: THebLabel;
    Shape124: TShape;
    Shape125: TShape;
    HebLabel72: THebLabel;
    Shape126: TShape;
    HebLabel73: THebLabel;
    Shape128: TShape;
    Shape129: TShape;
    HebLabel74: THebLabel;
    e71: TCheckBox;
    e72: TCheckBox;
    e73: TRadioButton;
    e75: TRadioButton;
    e75a: THebEdit;
    e77: TEdit;
    e76: THebEdit;
    e78: THebEdit;
    e79: THebEdit;
    HebLabel1a: THebLabel;
    Shape4: TShape;
    e31a: THebComboBox;
    Shape22: TShape;
    Shape24: TShape;
    HebLabel1b: THebLabel;
    Shape25: TShape;
    e72a: TCheckBox;
    Shape58: TShape;
    HebLabel47: THebLabel;
    Shape81: TShape;
    e72b: TCheckBox;
    Grid2a: TSdGrid;
    gb22a: TPanel;
    gb21a: TPanel;
    Bevel5: TBevel;
    Panel3: TPanel;
    Panel12: TPanel;
    Panel18: TPanel;
    Panel20: TPanel;
    Shape8234: TShape;
    HebLabel1354: THebLabel;
    nGrid2a: THebStringGrid;
    Shape83: TShape;
    btSearchStockHolders: TButton;
    Shape131: TShape;
    btSearchDirectors: TButton;
    btSearchSignName: TButton;
    Shape132: TShape;
    gbSearch: TPanel;
    Shape82a: TShape;
    e12a: TMaskEdit;
    HebLabel48a: THebLabel;
    Shape90a: TShape;
    ShapeEngNameEdit: TShape;
    ShapeEngNameLabel: TShape;
    lblEngName: THebLabel;
    edEnglishName: THebEdit;
    Panel1: TPanel;
    procedure gb21Click(Sender: TObject);
    procedure gb22Click(Sender: TObject);
    procedure Panel17Click(Sender: TObject);
    procedure Panel16Click(Sender: TObject);
    procedure Panel28Click(Sender: TObject);
    procedure Panel19Click(Sender: TObject);
    procedure e42Change(Sender: TObject);
    procedure e41Change(Sender: TObject);
    procedure e31Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Grid2Enter(Sender: TObject);
    procedure Grid2KeyPress(Sender: TObject; var Key: Char);
    procedure Grid2SelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
    procedure nGridDblClick(Sender: TObject);
    procedure nGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btSearchStockHoldersClick(Sender: TObject);
    procedure btSearchDirectorsClick(Sender: TObject);
    procedure btSearchSignNameClick(Sender: TObject);
    procedure gbSearchClick(Sender: TObject);
    procedure ZeutFieldExit(Sender: TObject);
    procedure grid5SetEditText(Sender: TObject; ACol, ARow: Integer; const Value: String);
    procedure CheckManagerID;
    procedure grid5Exit(Sender: TObject);
    procedure e51Change(Sender: TObject);
    procedure e45Change(Sender: TObject);
    procedure e412Change(Sender: TObject);
    procedure e32Change(Sender: TObject);
    procedure e11Change(Sender: TObject);
    procedure Grid2KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure grid5KeyPress(Sender: TObject; var Key: Char);
    procedure Grid2LineReset(Sender: TObject; Row: Integer);
    procedure e75aChange(Sender: TObject);
  private
    { Private declarations }
    FCompany: TCompany;
    FDate: TDateTime;
  protected
    FFilesList: TFilesList;
    FTakanon2,
    FTakanon2_1,
    FTakanon2_2,
    FTakanon2_3,
    FTakanon2_4,
    FTakanon4,
    FTakanon5,
    FTakanon7: String;
    ManagerID: string;
    CurrManager: integer;
    SubmissionData: TSubmissionData;
    procedure EraseFile;
    // Events
    procedure EvPrintData; override;
    procedure EvFillData(Company: TCompany); virtual;
    procedure EvPageIndexChange; override;
    function Save3(Index: Integer): Boolean;
    procedure Load3(Index: Integer);
    function Save4(Index: Integer): Boolean;
    procedure Load4(Index: Integer);
    procedure EvFocusNext(Sender: TObject); override;
    function EvSaveData(Draft: Boolean): boolean; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    procedure EvDeleteEmptyRows; override;
    function EvCollectData: TCompany; virtual;
    procedure GoToCurrRecord ; override;

    property Date: TDateTime read FDate write FDate;
    property Company: TCompany read FCompany write FCompany;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); virtual;
  end;

var
  fmCompanyInfoTemplate: TfmCompanyInfoTemplate;

implementation

uses
  dialog, Util, DataM, dialog2, PeopleSearch, ShowCompany, Rishum,
  PrintCompanyInfo, utils2;

{$R *.DFM}

procedure TfmCompanyInfoTemplate.DeleteFile(FileItem: TFileItem);
var
  TempCompany: TCompany;
  TempFileItem: TFileItem;
  FilesList: TFilesList;
  FileInfo: TFileInfo;

procedure EraseDraftFile;
var
  I: Integer;
  OldCompany: TCompany;
  FileItem: TFileItem;
begin
  For I:= 0 To FilesList.Count-1 do
  begin
    if FilesList.FileInfo[I].FileName = FileInfo.FileName Then
    begin
      FileItem:= FilesList.Items[I];
      OldCompany:= TCompany.LoadFromFile(nil, FileItem);
      OldCompany.FileDelete(FileItem);
      FileItem.Free;
      OldCompany.Free;
      Break;
    end;
  end
end;

begin
  If Not (FileItem.FileInfo.Action in [faRishum, faAddingNew]) Then
    raise Exception.Create('Invalid file type at TfmCompanyInfoTemplate.DeleteFile');
  If Not FileItem.FileInfo.Draft Then
  begin
    TempCompany := TCompany.LoadFromFile(nil, FileItem);
    TempCompany.FileDelete(FileItem);

    FileInfo:= TFileInfo.Create;
    FileInfo.Assign(FileItem.FileInfo);
    FileInfo.Draft:= True;

    FilesList:= TFilesList.Create(-1, faRishum, True);
    EraseDraftFile;
    FilesList.Add(FileInfo);

    TempFileItem:= TFileItem.Create(FilesList, FileInfo);
    TempCompany.ExSaveData(TempFileItem, nil);
   // TempCompany.Magish.SaveData(TempCompany, TempFileItem);

    TempCompany.Free;
    TempFileItem.Free;
    FilesList.Free;
  end
  Else
  begin
    TempCompany := TCompany.LoadFromFile(nil, FileItem);
    TempCompany.FileDelete(FileItem);
    TempCompany.Free;
  end;
end;

procedure TfmCompanyInfoTemplate.EraseFile;
var
  I: Integer;
  FileItem: TFileItem;
begin
  If Company = nil Then
    Exit;

  For I:= 0 to FFilesList.Count-1 do
    If FFilesList.FileInfo[I].FileName = Company.Name Then
      begin
        FileItem := FFilesList.Items[I];
        Company.FileDelete(FileItem);
        Company.Free;
        FileItem.Free;
        Break;
      end;
end;

procedure TfmCompanyInfoTemplate.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);

  procedure SpecialCheckID(const ID: String; ComboBox: THebComboBox; ItemIndex: Integer; Edit: TEdit);
  begin
    If (not CheckID(ID)) and
       (not Silent) and (not ErrorTimer.Enabled) Then
      begin
        If PageIndex<> Page Then
          PageIndex:= Page;
        ComboBox.ItemIndex:= ItemIndex;
        ComboBox.OnChange(ComboBox);
        If ErrorTimer.Tag= 0 Then
          try
            Edit.SetFocus;
          except end;

        If Edit.Color = clWindow Then
          Edit.Color:= clBtnFace
        Else
          Edit.Color:= clWindow;

        If Application.MessageBox('��� ��!'+#13+#10+'���� ����� ����/���� ���� ����.'+#13+#10+'��� ��� ��� �����?',
                                  PChar(Caption), MB_YESNO + MB_IconHand + MB_RTLREADING + MB_RIGHT) <> IDYES then
          begin
            ErrorAt:= Edit;
            raise Exception.Create('zeut');
          end;
      end;
  end;

var
  Companies: TCompanies;
  I, D, J: Integer;
  LastStockHolder: TStockHolder;
  HonList: THonList;
  MuhazList: TMuhazList;
  Stocks: TStocks;
  Directors: TDirectors;
  StockHolders: TStockHolders;
  UsedStocks: Double;  // You will like it !!!
begin
  Inherited EvCheckPage(Page, Silent, Extended, Draft);

  Case Page Of
    0: CheckEdit(Silent, e11);
    1: begin
         CheckGrid(Page, Silent, Extended, Grid2);
         CheckGrid(Page, Silent, Extended, Grid2a);
       end;
    2: CheckGrid(Page, Silent, Extended, Grid3);
    4: CheckGrid(Page, Silent, Extended, Grid5);
  end;

  If Not Extended Then
    Exit;

  Case Page Of
    0: begin
         Companies:= TCompanies.Create(Now, False, False);
         If Self is TfmRishum then
           Companies.Filter:= afAll
         Else
           Companies.Filter:= afActive;

         try
           For I:= 0 To Companies.Count-1 Do
             If (Companies.Names[I] = e11.Text) and
               (Not (ClassNameIs('TfmEditCompanyInfo') and Assigned(Company) and (Companies.RecNums[I] = Company.RecNum))) Then
               begin
                 If Not Silent Then
                   With e11 do
                     If Color = clWindow Then
                       Color := clBtnFace
                     Else
                       Color:= clWindow;

                 ErrorAt:= e11;
                 raise Exception.Create('�� ����� ��� ���� ����� ����');
               end;

           Companies.Filter:= afActive;
           For I:= 0 To Companies.Count-1 Do
             If (Companies.Numbers[I] = e12.Text) and (not Draft)and(not FFromDraft) and
               (Not (ClassNameIs('TfmEditCompanyInfo') and Assigned(Company) and (Companies.RecNums[I] = Company.RecNum))) Then
               begin
                 If Not Silent Then
                   With e11 do
                     If Color = clWindow Then
                       Color := clBtnFace
                     Else
                       Color:= clWindow;

                 ErrorAt:= e12;
                 raise Exception.Create('���� ����� ��� ���� ����� ����');
               end;
         finally
           Companies.Free;
         end;
       end;
    1: begin
         HonList:= THonList.CreateNew;
         try
           With Grid2 Do
             For I:= 0 To RowCount-1 Do
               begin
                 If MyStrToFloat(Cells[2,I]) < MyStrToFloat(Cells[3,I]) Then
                   begin
                     If Not Silent Then
                       begin
                         Row:= I;
                         Col:= 3;
                       end;
                     ErrorAt:= Grid2;
                     raise Exception.Create('��� ������ ������ ���� ���� ����� ���� ���� �����');
                   end;

                 If HonList.FindHonItem(Cells[0,I], MyStrToFloat(Cells[1,I])) = nil Then
                   With Rows[I] Do
                     HonList.Add(THonItem.CreateNew(Date, Strings[0], MyStrToFloat(Strings[1]),
                         MyStrToFloat(Strings[2]), MyStrToFloat(Strings[3]), MyStrToFloat(Strings[4]),
                         MyStrToFloat(Strings[5]), MyStrToFloat(Strings[6]), MyStrToFloat(Strings[7]), True))
                 Else
                   begin
                     If Not Silent Then
                       begin
                         Row:= I;
                         Col:= 0;
                       end;
                     ErrorAt:= Grid2;
                     raise Exception.Create('�� ���� ����� ���� ���� ���� ���� ���');
                   end;
               end;

           MuhazList:= TMuhazList.CreateNew;
           try
             With Grid2a Do
               For I:= 0 to RowCount-1 do
                 If RemoveSpaces(Cells[0,I], rmBoth) <> '' Then
                   begin
                     If MuhazList.FindByZeut(Grid2a.Cells[2,I]) = Nil Then
                       If DoubleIndexOf(Grid2, Cells[0,I], Cells[1,I], 0) <> -1 then
                         If RemoveSpaces(Cells[2,I],rmBoth) <> '' Then
                           MuhazList.Add( TMuhazItem.CreateNew(Date, Cells[0,I], Cells[2,I], MyStrToFloat(Cells[1,I]), MyStrToFloat(Cells[3,I]), True))
                         Else
                           begin
                             If Not Silent Then
                               Begin
                                 Col:= 2;
                                 Row:= I;
                               end;
                             ErrorAt:= Grid2a;
                             raise Exception.Create('���� ����� ��'' ���');
                           end
                       Else
                         begin
                           If not Silent Then
                             begin
                               Col:= 0;
                               Row:= I;
                             end;
                           ErrorAt:= Grid2a;
                           raise Exception.Create('���� �� �����');
                         end
                     Else
                       begin
                         If Not Silent then
                           begin
                             Col:= 0;
                             Row:= I;
                           end;
                         ErrorAt:= Grid2a;
                         raise Exception.Create('������ ��� ����� ���� ���� ���� ���');
                       end;
                   end;

             For I:= 0 to HonList.Count-1 do
               If MuhazList.CountMuhaz((HonList.Items[I] as THonItem).Name, (HonList.Items[I] as THonItem).Value) > (HonList.Items[I] as THonItem).Mokza Then
                 begin
                   If Not Silent Then
                     begin
                       Grid2a.Col := 3;
                       Grid2a.Row := DoubleIndexOf(Grid2a, (HonList.Items[I] as THonItem).Name, FormatNumber(FloatToStr((HonList.Items[I] as THonItem).Value),  15, 4, True), 0);
                     end;
                   ErrorAt:= Grid2a;
                   raise Exception.Create('���� ������� �� ��� �����');
                 end;
           finally
             MuhazList.Free;
           end;
         finally
           HonList.Free;
         end;
       end;
    2: begin
         Save3(StrToInt(e31.Text));
         StockHolders:= TStockHolders.CreateNew;
         try
           With TempGrid31 Do
             For I:= 0 To RowCount -1 Do
               If RemoveSpaces(Cells[0,I], rmBoth)<> '' Then
                 If StockHolders.FindByZeut(Cells[2,I]) = Nil Then
                 begin
                   SpecialCheckID(Cells[2,I], e31, e31.Items.IndexOf(Cells[0,I]), e33);
                   Stocks:= TStocks.CreateNew;
                   With TempGrid32 Do
                     For J:= 0 To RowCount-1 Do
                       If Cells[0,J] = TempGrid31.Cells[0,I] Then
                         If Stocks.FindStock(Cells[1,J], MyStrToFloat(Cells[2,J])) = Nil Then
                           If DoubleIndexOf(Grid2, Cells[1,J], Cells[2,J], 0) <> -1 then
                             Stocks.Add(TStock.CreateNew(Date, Cells[1,J], MyStrToFloat(Cells[2,J]), MyStrToFloat(Cells[3,J]), MyStrToFloat(Cells[4,J]), True))
                           Else
                           begin
                             If Not Silent Then
                             begin
                               e31.ItemIndex:= e31.Items.IndexOf(Cells[0,J]);
                               e31Change(e31);
                               Grid3.Row:= DoubleIndexOf(Grid3, Cells[1,J], Cells[2,J], 0);
                               Grid3.Col:= 0;
                             end;
                             ErrorAt:= Grid3;
                             raise Exception.Create('���� �� �����');
                           end
                         Else
                         begin
                           If Not Silent Then
                           begin
                             e31.ItemIndex:= e31.Items.IndexOf(Cells[0,J]);
                             e31Change(e31);
                             Grid3.Row:= DoubleIndexOf(Grid3, Cells[1,J], Cells[2,J], 0);
                             Grid3.Col:= 0;
                           end;
                           ErrorAt:= Grid3;
                           raise Exception.Create('�� ���� ����� ���� ���� ���� ���� ���');
                         end;

                   StockHolders.Add( TStockHolder.CreateNew(Date, Cells[1,I], Cells[2,I], Cells[8,I] = '1',
                      TStockHolderAddress.CreateNew(Cells[3,I], Cells[4,I], Cells[5,I], Cells[6,I], Cells[7,I]),
                      Stocks, True));
                 end
                 Else
                 begin
                   If Not Silent Then
                   begin
                     e31.ItemIndex:= e31.Items.IndexOf(Cells[0,I]);
                     e31Change(e31);
                     With e33 Do
                       If Color = clWindow Then
                         Color:= clBtnFace
                       Else
                         Color:= clWindow;
                   end;
                   ErrorAt:= e33;
                   raise Exception.Create('���� ���� �� ��� ���� ���� ����� ���');
                 end;

           // Of course You like it, but
           // The nightmare is not over yet :)
           HonList:= THonList.CreateNew;
           MuhazList:= TMuhazList.CreateNew;
           try
             With Grid2 Do
               For I:= 0 To RowCount-1 Do
                 With Rows[I] Do
                   HonList.Add(THonItem.CreateNew(Date, Strings[0], MyStrToFloat(Strings[1]),
                       MyStrToFloat(Strings[2]), MyStrToFloat(Strings[3]), MyStrToFloat(Strings[4]),
                       MyStrToFloat(Strings[5]), MyStrToFloat(Strings[6]), MyStrToFloat(Strings[7]), True));

             With Grid2a Do
               For I:= 0 To RowCount-1 Do
                 MuhazList.Add( TMuhazItem.CreateNew(Date, Cells[0,I], Cells[2,I], MyStrToFloat(Cells[1,I]), MyStrToFloat(Cells[3,I]), True));

             For I:= 0 To HonList.Count-1 Do
               begin
                 UsedStocks:= MuhazList.CountMuhaz((HonList.Items[I] as THonItem).Name, (HonList.Items[I] as THonItem).Value);

                 LastStockHolder:= Nil;
                 for D:= 0 to StockHolders.Count-1 Do
                   with (StockHolders.Items[D] as TStockHolder) do
                     for J:= 0 To Stocks.Count-1 Do
                       if (Stocks.Items[J].Name = HonList.Items[I].Name) And (TStock(Stocks.Items[J]).Value = THonItem(HonList.Items[I]).Value) Then
                       begin
                         UsedStocks:= UsedStocks+ TStock(Stocks.Items[J]).Count;
                         LastStockHolder:= (StockHolders.Items[D] as TStockHolder);
                       end;

                 If UsedStocks < THonItem(HonList.Items[I]).Mokza Then
                 begin
                   If Not Silent Then
                     try
                       e31.ItemIndex:= e31.Items.IndexOf(TempGrid31.Cells[0, TempGrid31.Cols[2].IndexOf(LastStockHolder.Zeut)]);
                       e31Change(e31);
                       Grid3.Row:= DoubleIndexOf(Grid3, HonList.Items[I].Name, FormatNumber(FloatToStr(THonItem(HonList.Items[I]).Value), 15,4,True),0);
                       Grid3.Col:= 2;
                     except
                     end;
                   ErrorAt:= Grid3;
                   raise Exception.Create('��"� ������ ������� ���� ������� ������ ����� ������');
                 end;

                 If UsedStocks > THonItem(HonList.Items[I]).Mokza Then
                 begin
                   If Not Silent Then
                     try
                       e31.ItemIndex:= e31.Items.IndexOf(TempGrid31.Cells[0, TempGrid31.Cols[2].IndexOf(LastStockHolder.Zeut)]);
                       e31Change(e31);
                       Grid3.Row:= DoubleIndexOf(Grid3, HonList.Items[I].Name, FormatNumber(FloatToStr(THonItem(HonList.Items[I]).Value), 15,4,True),0);
                       Grid3.Col:= 2;
                     except
                     end;
                   ErrorAt:= Grid3;
                   raise Exception.Create('��"� ������ ������� ��� ������� ������ ����� ������');
                 end;
               end;
           finally
             MuhazList.Free;
             HonList.Free;
           end;
         finally
           StockHolders.Free;
         end;
       end;
    3: begin
         Save4(StrToInt(e41.Text));
         Directors:= TDirectors.CreateNew;
         try
           With TempGrid4 Do
             For I:= 0 To RowCount-1 Do
               If RemoveSpaces(Cells[0,I],rmBoth) <> '' Then
                 If Directors.FindByZeut(Cells[5,I]) = nil Then
                 begin
                   CurrRow:=I;
                   SpecialCheckID(Cells[5,I], e41, e41.Items.IndexOf(Cells[0,I]), e46);
                   If Cells[1,I] = '1' Then
                     SpecialCheckID(Cells[13,I], e41, e41.Items.IndexOf(Cells[0,I]), e413);
                   Directors.Add( TDirector.CreateNew(Date, Cells[4,I], Cells[5,I], StrToDate(Cells[2,I]), StrToDate(Cells[3,I]),
                      TDirectorAddress.CreateNew(Cells[7,I], Cells[8,I], Cells[9,I], Cells[10,I], Cells[11,I], Cells[6,I]), Nil, True));
                   If StrToDate(Cells[2,I]) > StrToDate(Cells[3,I]) Then
                     begin
                       If Not Silent Then
                         begin
                           e41.ItemIndex:= e41.Items.IndexOf(Cells[0,I]);
                           e41Change(e41);
                           With e44 Do
                             If Color = clWindow Then
                               Color:= clBtnFace
                             Else
                               Color:= clWindow
                         end;
                       ErrorAt:= e44;
                       raise Exception.Create('����� ����� ����� ��� ������ ����');
                     end;
                 end
                 Else
                   begin
                     If Not Silent Then
                       begin
                         e41.ItemIndex:= StrToInt(Cells[0,I]);
                         e41Change(e41);
                         With e46 Do
                           If Color = clWindow Then
                             Color:= clBtnFace
                           Else
                             Color:= clWindow
                       end;
                     ErrorAt:= e46;
                     raise Exception.Create('���� ���� �� ��� ���� �������� ���');
                   end;
         finally
           if Directors<>nil then
             Directors.Free;
         end;
       end;
  end;
end;

procedure TfmCompanyInfoTemplate.EvFocusNext(Sender: TObject);
begin
  if Sender = e11  then
  if e12.Visible   then e12.SetFocus
                   else e13.SetFocus;
  if Sender = e12  then e12a.SetFocus;
  if Sender = e12a then e13.SetFocus;
  if Sender = e13  then e14.SetFocus;
  if Sender = e14  then e15.SetFocus;
  if Sender = e15  then e16.SetFocus;
  if Sender = e16  then e17.SetFocus;
  if Sender = e17  then e18.SetFocus;
  if Sender = e18  then e19.SetFocus;
  if Sender = e19  then e110.SetFocus;
  if Sender = e110 then pnB1Click(pnB2);

  if Sender = e31  then e31a.SetFocus;
  if Sender = e31a then e32.SetFocus;
  if Sender = e32  then e33.SetFocus;
  if Sender = e33  then e34.SetFocus;
  if Sender = e34  then e35.SetFocus;
  if Sender = e35  then e36.SetFocus;
  if Sender = e36  then e37.SetFocus;
  if Sender = e37  then e38.SetFocus;
  if Sender = e38  then Grid3.SetFocus;

  if Sender = e41  then e42.SetFocus;
  if Sender = e42  then e45.SetFocus;
  if Sender = e45  then e46.SetFocus;
  if Sender = e46  then e46b.SetFocus;
  if Sender = e46b then e43.SetFocus;
  if Sender = e43  then e44.SetFocus;
  if Sender = e44  then e47.SetFocus;
  if Sender = e47  then e48.SetFocus;
  if Sender = e48  then e49.SetFocus;
  if Sender = e49  then e410.SetFocus;
  if Sender = e410 then e411.SetFocus;
  if Sender = e411 then
    if pnDir.Visible then
      e412.SetFocus
    else
    begin
      fmDialog.memo.lines.Clear;
      fmDialog.memo.lines.add('��� ������ ����� ������� ���� ?');
      fmDialog.ShowModal;
      if fmDialog.DialogResult = drYES then
        Panel28Click(nil)
      else
        pnB1Click(pnB5);
    end;

  if Sender = e412  then e413.SetFocus;
  if Sender = e413  then e414.SetFocus;
  if Sender = e414  then e415.SetFocus;
  if Sender = e415  then e416.SetFocus;
  if Sender = e416  then e417.SetFocus;
  if Sender = e417  then e418.SetFocus;
  if Sender = e418  then
  begin
    fmDialog.memo.lines.Clear;
    fmDialog.memo.lines.add('��� ������ ����� ������� ���� ?');
    fmDialog.ShowModal;
    if fmDialog.DialogResult = drYES then
      Panel28Click(nil)
    else
      pnB1Click(pnB5);
  end;
  if Sender = e51 then e52.SetFocus;
  if Sender = e52 then e53.SetFocus;
  if Sender = e53 then Grid5.SetFocus;
  if Sender = e71 then e72.SetFocus;
  if Sender = e72 then e72a.SetFocus;
  If Sender = e72a then e72b.SetFocus;
  If Sender = e72b then e73.SetFocus;
  if Sender = e73 then e75.SetFocus;
  if Sender = e75 then e75a.SetFocus;
  if Sender = e75a then e76.SetFocus;
  if Sender = e76 then e77.SetFocus;
  if Sender = e77 then e78.SetFocus;
  if Sender = e78 then e79.SetFocus;
end;

procedure TfmCompanyInfoTemplate.gb21Click(Sender: TObject);
begin   
  inherited;
  If ReadOnly Then
    Exit;

  if Sender = gb21 then
    Grid2.LineDelete;

  if Sender = gb21a Then
    Grid2a.LineDelete;

  if Sender = gb31 then
    Grid3.LineDelete;

  if Sender = gb51 then
    Grid5.LineDelete;
  DataChanged:= True;
end;

procedure TfmCompanyInfoTemplate.gb22Click(Sender: TObject);
begin
  inherited;

  if ReadOnly then
    Exit;

  if Sender = gb22 then
    Grid2.LineInsert;

  if Sender = gb22a then
    Grid2a.LineInsert;

  if Sender = gb32 then
    Grid3.LineInsert;

  if Sender = gb52 then
    Grid5.LineInsert;

  DataChanged := True;
end;

procedure TfmCompanyInfoTemplate.Panel17Click(Sender: TObject);
begin
  inherited;
  If ReadOnly Then
    Exit;
  If Save3(StrToInt(e31.Text)) Then
    begin
      e31.Items.Add(IntToStr(e31.Items.Count + 1));
      e31.ItemIndex := e31.Items.Count - 1;
      e31Change(nil);
      e33.Text:= '';
    end;
  e32.SetFocus;
end;

procedure TfmCompanyInfoTemplate.Panel16Click(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  If ReadOnly Then
    Exit;
  EvTimerOver;
  Loading:= True;
  if e31.Items.Count = 1 then
  begin
    e32.Text := '';    e33.Text := '';    e34.Text := '';
    e35.Text := '';    e36.Text := '';    e37.Text := '';
    e38.Text := '�����';

    Grid3.RowCount := 1;
    Grid3.ResetLine(0);
  end;

  While (TempGrid32.Cols[0].IndexOf(e31.Text)<>-1) do
    TempGrid32.LineDeleteByIndex(TempGrid32.Cols[0].IndexOf(e31.Text));
  While (TempGrid31.Cols[0].IndexOf(e31.Text)<>-1) do
    TempGrid31.LineDeleteByIndex(TempGrid31.Cols[0].IndexOf(e31.Text));

  for i:=0 to TempGrid32.RowCount-1 do
    try
      if (StrToInt(TempGrid32.Cells[0,i]) > StrToInt(e31.Text)) then
        TempGrid32.Cells[0,i] := IntToStr(StrToInt(TempGrid32.Cells[0,i])-1);
    except
    end;

  for i:=0 to TempGrid31.RowCount-1 do
    try
      if (StrToInt(TempGrid31.Cells[0,i]) > StrToInt(e31.Text)) then
        TempGrid31.Cells[0,i] := IntToStr(StrToInt(TempGrid31.Cells[0,i])-1);
    except
    end;

  if (e31.ItemIndex = e31.Items.Count-1) and (e31.Text<>'1') then
    e31.ItemIndex := e31.ItemIndex-1;

  e31.Tag := StrToInt(e31.Text);
  Load3(StrToInt(e31.Text));

  if e31.Items.Count>1 then
    e31.Items.Delete(e31.Items.Count-1);
  Loading:= False;
  DATAChanged:= True;
end;

procedure TfmCompanyInfoTemplate.Panel28Click(Sender: TObject);
begin
  inherited;
  If ReadOnly Then
    Exit;
  If Save4(StrToInt(e41.Text)) then
    begin
      e41.Items.Add(IntToStr(e41.Items.Count+1));
      e41.ItemIndex := e41.Items.Count-1;
      e41Change(nil);
//      e46.Text:= '0';Tsahi 26.07.05
      e46.Text:= '';
    end;
  e45.SetFocus;
end;


procedure TfmCompanyInfoTemplate.Panel19Click(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  If ReadOnly Then
    Exit;
  EvTimerOver;
  Loading:= True;
  if e41.Items.Count = 1 then
    begin
      e42.ItemIndex := 0;
      e43.Text := DateToLongStr(Now);
      e44.Text := DateToLongStr(Now);
      e45.Text := '';    e46.Text := '';
      e47.Text := '';    e48.Text := '';    e49.Text := '';
      e410.Text := '';
      e411.Text := '�����';

      e412.Text := '';   e413.Text := '';   e414.Text := '';
      e415.Text := '';   e416.Text := '';   e417.Text := '';
      e418.Text := '�����';
   end;

  While (TempGrid4.Cols[0].IndexOf(e41.Text)<>-1) do
    TempGrid4.LineDeleteByIndex(TempGrid4.Cols[0].IndexOf(e41.Text));

  for i:=0 to TempGrid4.RowCount-1 do
    try
      if (StrToInt(TempGrid4.Cells[0,i]) > StrToInt(e41.Text)) then
        TempGrid4.Cells[0,i] := IntToStr(StrToInt(TempGrid4.Cells[0,i])-1);
    except end;

  if (e41.ItemIndex = e41.Items.Count-1) and (e41.Text<>'1') then
    e41.ItemIndex := e41.ItemIndex-1;

  e41.Tag := StrToInt(e41.Text);
  Load4(StrToInt(e41.Text));

  If e41.Items.Count>1 then
    e41.Items.Delete(e41.Items.Count-1);
  Loading:= False;
  DATAChanged:= True;
end;

procedure TfmCompanyInfoTemplate.Load3(Index: Integer);
var
  I,GIndex : integer;
begin
  GIndex := TempGrid31.Cols[0].IndexOf(IntToStr(Index));

  if GIndex = -1 then
  begin
    e32.Text := '';
//      e33.Text:= '0';Tsahi 26.07.05
    e33.Text:= '';
    e34.Text := '';
    e35.Text := '';
    e36.Text := '0';
    e37.Text := '';
    e38.Text := '�����';
    e31a.ItemIndex:= 0;

    Grid3.RowCount := 1;
    Grid3.ResetLine(0);
  end
  else
  begin
    e31a.ItemIndex:= StrToInt(TempGrid31.Cells[8,GIndex]);
    e32.Text    := TempGrid31.Cells[1,GIndex];
    e33.Text    := TempGrid31.Cells[2,GIndex];
    e34.Text    := TempGrid31.Cells[3,GIndex];
    e35.Text    := TempGrid31.Cells[4,GIndex];
    e36.Text    := TempGrid31.Cells[5,GIndex];
    e37.Text    := TempGrid31.Cells[6,GIndex];
    e38.Text    := TempGrid31.Cells[7,GIndex];

    Grid3.RowCount := 1;
    Grid3.ResetLine(0);

    for i := 0 to TempGrid32.RowCount - 1 do
      if TempGrid32.Cells[0, i] = IntToStr(Index) then
      begin
        Grid3.Cells[0, Grid3.RowCount - 1] := TempGrid32.Cells[1, i];
        Grid3.Cells[1, Grid3.RowCount - 1] := TempGrid32.Cells[2, i];
        Grid3.Cells[2, Grid3.RowCount - 1] := TempGrid32.Cells[3, i];
        Grid3.Cells[3, Grid3.RowCount - 1] := TempGrid32.Cells[4, i];

        Grid3.RowCount := Grid3.RowCount + 1;
      end;
    if Grid3.RowCount > 1 then
      Grid3.RowCount := Grid3.RowCount - 1;
  end;

  Grid3.Repaint;
end;

function TfmCompanyInfoTemplate.Save3(Index: Integer): Boolean;
var
  i,GIndex   : integer;
begin
  If (RemoveSpaces(e32.Text,rmBoth)='') And (e33.Text='') Then
  begin
    Result:= False;
    Exit;
  end;

  GIndex := TempGrid31.Cols[0].IndexOf(IntToStr(Index));
  if GIndex = -1 then
  begin
    TempGrid31.RowCount := TempGrid31.RowCount+1;
    GIndex := TempGrid31.RowCount-1;
  end;

  TempGrid31.Cells[0,GIndex] := IntToStr(Index);
  TempGrid31.Cells[1,GIndex] := e32.Text;
  TempGrid31.Cells[2,GIndex] := e33.Text;
  TempGrid31.Cells[3,GIndex] := e34.Text;
  TempGrid31.Cells[4,GIndex] := e35.Text;
  TempGrid31.Cells[5,GIndex] := e36.Text;
  TempGrid31.Cells[6,GIndex] := e37.Text;
  TempGrid31.Cells[7,GIndex] := e38.Text;
  TempGrid31.Cells[8,GIndex] := IntToStr(e31a.ItemIndex);

  While (TempGrid32.Cols[0].IndexOf(IntToStr(Index))<>-1) do
    TempGrid32.LineDeleteByIndex(TempGrid32.Cols[0].IndexOf(IntToStr(Index)));

  TempGrid32.RowCount:= TempGrid32.RowCount + 1;

  for i:=0 to Grid3.RowCount-1 do
    if RemoveSpaces(Grid3.Cells[0,i],rmBoth)<>'' then
    begin
      TempGrid32.Cells[0,TempGrid32.RowCount-1] := IntToStr(Index);
      TempGrid32.Cells[1,TempGrid32.RowCount-1] := Grid3.Cells[0,i];
      TempGrid32.Cells[2,TempGrid32.RowCount-1] := Grid3.Cells[1,i];
      TempGrid32.Cells[3,TempGrid32.RowCount-1] := Grid3.Cells[2,i];
      TempGrid32.Cells[4,TempGrid32.RowCount-1] := Grid3.Cells[3,i];
      TempGrid32.RowCount := TempGrid32.RowCount+1;
    end;
  TempGrid32.RowCount:= TempGrid32.RowCount -1;
  Result:= True;
end;

function TfmCompanyInfoTemplate.Save4(Index: Integer): Boolean;
var
  GIndex   : integer;
begin
  if (RemoveSpaces(e45.Text, rmBoth) = '') and ((e46.Text = '') or (e46.Text = '0')) then
  begin
    Result := False;
    Exit;
  end;

  GIndex := TempGrid4.Cols[0].IndexOf(IntToStr(Index));
  if GIndex = -1 then
  begin
    TempGrid4.RowCount := TempGrid4.RowCount+1;
    GIndex := TempGrid4.RowCount-1;
  end;

  TempGrid4.Cells[0,GIndex]  := IntToStr(Index);
  TempGrid4.Cells[1,GIndex]  := IntToStr(e42.ItemIndex);
  TempGrid4.Cells[2,GIndex]  := e43.Text;
  TempGrid4.Cells[3,GIndex]  := e44.Text;
  TempGrid4.Cells[4,GIndex]  := e45.Text;
  TempGrid4.Cells[5,GIndex]  := e46.Text;
  TempGrid4.Cells[6,GIndex]  := e46b.Text;
  TempGrid4.Cells[7,GIndex]  := e47.Text;
  TempGrid4.Cells[8,GIndex]  := e48.Text;
  TempGrid4.Cells[9,GIndex]  := e49.Text;
  TempGrid4.Cells[10,GIndex] := e410.Text;
  TempGrid4.Cells[11,GIndex] := e411.Text;
  TempGrid4.Cells[12,GIndex] := e412.Text;
  TempGrid4.Cells[13,GIndex] := e413.Text;
  TempGrid4.Cells[14,GIndex] := e414.Text;
  TempGrid4.Cells[15,GIndex] := e415.Text;
  TempGrid4.Cells[16,GIndex] := e416.Text;
  TempGrid4.Cells[17,GIndex] := e417.Text;
  TempGrid4.Cells[18,GIndex] := e418.Text;
  Result:= True;
end;

procedure TfmCompanyInfoTemplate.Load4(Index: Integer);
var
  GIndex : integer;
begin
  GIndex := TempGrid4.Cols[0].IndexOf(IntToStr(Index));

  If GIndex = -1 then
  begin
    e42.ItemIndex := 0;
    e42Change(e42);
    e43.Text := DateToLongStr(Now);
    e44.Text := DateToLongStr(Now);

    e45.Text := '';    e46.Text := '';   e46b.Text := '';
    e47.Text := '';    e48.Text := '';    e49.Text := '0';
    e410.Text := '';
    e411.Text := '�����';

    e412.Text := '';   e413.Text := '';   e414.Text := '';
    e415.Text := '';   e416.Text := '0';   e417.Text := '';
    e418.Text := '�����';
  end
  else
  begin
    e42.ItemIndex := StrToInt(TempGrid4.Cells[1,GIndex]);
    e42Change(e42);
    e43.Text      := TempGrid4.Cells[2,GIndex];
    e44.Text      := TempGrid4.Cells[3,GIndex];
    e45.Text      := TempGrid4.Cells[4,GIndex];
    e46.Text      := TempGrid4.Cells[5,GIndex];
    e46b.Text     := TempGrid4.Cells[6,GIndex];
    e47.Text      := TempGrid4.Cells[7,GIndex];
    e48.Text      := TempGrid4.Cells[8,GIndex];
    e49.Text      := TempGrid4.Cells[9,GIndex];
    e410.Text     := TempGrid4.Cells[10,GIndex];
    e411.Text     := TempGrid4.Cells[11,GIndex];
    e412.Text     := TempGrid4.Cells[12,GIndex];
    e413.Text     := TempGrid4.Cells[13,GIndex];
    e414.Text     := TempGrid4.Cells[14,GIndex];
    e415.Text     := TempGrid4.Cells[15,GIndex];
    e416.Text     := TempGrid4.Cells[16,GIndex];
    e417.Text     := TempGrid4.Cells[17,GIndex];
    e418.Text     := TempGrid4.Cells[18,GIndex];
  end;
end;

procedure TfmCompanyInfoTemplate.e42Change(Sender: TObject);
begin
  inherited;
  pnDIr.Visible := e42.ItemIndex <> 0;
  if Not Loading then
    DATAchanged := true;
end;

procedure TfmCompanyInfoTemplate.e41Change(Sender: TObject);
var
  Old_DATAChanged: Boolean;
begin
  inherited;

  if Loading then
    exit;

  OLD_DATAChanged:= DATAChanged;
  Loading:= True;
  Save4(e41.Tag);
  Load4(StrToInt(e41.Text));
  E41.Tag := StrToInt(e41.Text);
  e42Change(nil);
  Loading:= False;
  DATAChanged:= OLD_DATAChanged;
end;

procedure TfmCompanyInfoTemplate.e31Change(Sender: TObject);
var
  Old_DATAChanged: Boolean;
begin
  inherited;
  If Loading then
    Exit;

  OLD_DATAChanged:= DATAChanged;
  Loading:= True;
  Save3(e31.Tag);
  Load3(StrToInt(e31.Text));
  E31.Tag := StrToInt(e31.Text);
  Loading:= False;
  DATAChanged:= OLD_DATAChanged;
end;

function TfmCompanyInfoTemplate.EvCollectData: TCompany;
var
  Directors: TDirectors;
  Stocks: TStocks;
  StockHolders: TStockHolders;
  HonList: THonList;
  MuhazList: TMuhazList;
  Taagid: TTaagid;
  Managers: TManagers;
  Option1: Byte;
  Option3: Char;
  I, J: Integer;
  Date: TDateTime;
  CompanyInfo: TCompanyInfo;
begin
  WriteChangeLog(#9 + 'Collect data start');
  //==> Tsahi 06/11/05. added so drafts can be saved with 00/00/0000 dates
  Date:= StrToDateDef(e12a.Text,0);
  //==>

  // Page 2 - Saving HonList
  WriteChangeLog(#9 + 'Saving HonList');
  HonList:= THonList.CreateNew;
  For I:= 0 To Grid2.RowCount-1 Do
  begin
    With Grid2.Rows[I] Do
      HonList.Add(THonItem.CreateNew(Date, Strings[0], MyStrToFloat(Strings[1]),
         MyStrToFloat(Strings[2]), MyStrToFloat(Strings[3]), MyStrToFloat(Strings[4]),
         MyStrToFloat(Strings[5]), MyStrToFloat(Strings[6]), MyStrToFloat(Strings[7]), True));
  end;

  // Saving Muhaz List
  WriteChangeLog(#9 + 'Saving Muhaz List');
  MuhazList:= TMuhazList.CreateNew;
  For I:= 0 to Grid2a.RowCount - 1 do
  begin
    with Grid2a.Rows[I] do
      If RemoveSpaces(Strings[0], rmBoth) <> '' then
        MuhazList.Add( TMuhazItem.CreateNew(Date, Strings[0], Strings[2], MyStrToFloat(Strings[1]), MyStrToFloat(Strings[3]), True));
  end;

  // Page 3 - Saving StockHolders
  WriteChangeLog(#9 + 'Saving StockHolders');
  StockHolders := TStockHolders.CreateNew;
  Save3(StrToInt(e31.Text));
  For I:= 0 To TempGrid31.RowCount-1 Do
  begin
    If RemoveSpaces(TempGrid31.Cells[0,I], rmBoth) <> '' then
    begin
      // Phase 1 - Collecting Stocks
      Stocks:= TStocks.CreateNew;
      With TempGrid32 Do
        For J:= 0 To RowCount-1 Do
        begin
          If Cells[0,J] = TempGrid31.Cells[0,I] Then
            Stocks.Add(TStock.CreateNew(Date, Cells[1,J], MyStrToFloat(Cells[2,J]), MyStrToFloat(Cells[3,J]),
              MyStrToFloat(Cells[4,J]), True));
        end;

      // Phase 2 - Adding Person Info
      With TempGrid31 do
        StockHolders.Add(TStockHolder.CreateNew(Date, Cells[1,I], Cells[2,I], Cells[8,I] = '1',
           TStockHolderAddress.CreateNew(Cells[3,I], Cells[4,I], Cells[5,I], Cells[6,I], Cells[7,I]),
           Stocks, True));
    end;
  end;

  // Page 4  - Saving Directors
  WriteChangeLog(#9 + 'Saving Directors');
  Directors:= TDirectors.CreateNew;
  Save4(StrToInt(e41.Text));
  With TempGrid4 do
    For I:= 0 To RowCount-1 Do
    begin
      If RemoveSpaces(Cells[0,I], rmBoth) <> '' Then
      begin
        // Check - If Taagid - Then Load it's data
        If Cells[1,I] = '1' Then
          Taagid:= TTaagid.CreateNew(Cells[14,I], Cells[15,I], Cells[16,I], Cells[17,I], Cells[18,I], Cells[12,I], Cells[13,I])
        Else
          Taagid:= Nil;
        //==> Tsahi 06/11/05. added StrToDateDef so drafts can be saved with 00/00/0000 dates
        Directors.Add( TDirector.CreateNew(Date, Cells[4,I], Cells[5,I], StrToDateDef(Cells[2,I],0), StrToDateDef(Cells[3,I],0),
          TDirectorAddress.CreateNew(Cells[7,I], Cells[8,I], Cells[9,I], Cells[10,I], Cells[11,I], Cells[6,I]),
          Taagid, True));
        //<==
      end;
    end;

  // Page 5 - Saving Managers
  WriteChangeLog(#9 + 'Saving Managers');
  Managers := TManagers.CreateNew;
  With Grid5 do
    For I:= 0 To RowCount-1 Do
    begin
      Managers.Add(TManager.CreateNew(Date, Cells[0,I], Cells[1,I], Cells[2,I], Cells[3,I], Cells[4,I]));
    end;

  // Page 6 - Saving Options
  WriteChangeLog(#9 + 'Saving Options');
  Option1:= 0;

  if e71.Checked then
    Option1:= Option1 or 1;

  if e72.Checked then
    Option1 := Option1 or 2;

  if e72a.Checked then
    Option1 := Option1 or 4;

  If e72b.Checked then
    Option1 := Option1 or 8;

  if e73.Checked then
    Option3 := '1'
  Else if e75.Checked then
    Option3 := '3'
  else
    Option3 := '2';

  // Moshe 01/11/2016 Task 12538
  // Create CompanyInfo
  CompanyInfo := TCompanyInfo.CreateNew(Date);
  CompanyInfo.Option1          := Option1;
  CompanyInfo.Option3          := Option3;
  CompanyInfo.OtherInfo        := e75a.Text;
  CompanyInfo.SigName          := e51.Text;
  CompanyInfo.SigZeut          := e52.Text;
  CompanyInfo.SigTafkid        := e53.Text;
  CompanyInfo.LawName          := e76.Text;
  CompanyInfo.LawAddress       := e78.Text;
  CompanyInfo.LawZeut          := e77.Text;
  CompanyInfo.LawLicence       := e79.Text;
  CompanyInfo.Takanon2_1       := FTakanon2_1;
  CompanyInfo.Takanon2_2       := FTakanon2_2;
  CompanyInfo.Takanon2_3       := FTakanon2_3;
  CompanyInfo.Takanon2_4       := FTakanon2_4;
  CompanyInfo.Takanon4         := FTakanon4;
  CompanyInfo.Takanon5         := FTakanon5;
  CompanyInfo.Takanon7         := FTakanon7;
  CompanyInfo.EnglishName      := edEnglishName.Text;
  CompanyInfo.RegistrationDate := Date;

  if (FCompany <> nil) then
  begin
    if Assigned(FCompany)then
    begin
      if FCompany.CompanyInfo <> nil then
      begin
        if Assigned(FCompany.CompanyInfo) then
        begin
          try
            CompanyInfo.CompanyName2 := FCompany.CompanyInfo.CompanyName2;
          except
            CompanyInfo.CompanyName2 := '';
          end;
          try
            CompanyInfo.CompanyName3 := FCompany.CompanyInfo.CompanyName3;
          except
            CompanyInfo.CompanyName3 := '';
          end;
          try
            CompanyInfo.Mail := FCompany.CompanyInfo.Mail;
          except
            CompanyInfo.Mail := '';
          end;
          try
            CompanyInfo.SharesRestrictIndex := FCompany.CompanyInfo.SharesRestrictIndex;
          except
            CompanyInfo.SharesRestrictIndex := -1;
          end;
          try
            CompanyInfo.DontOfferIndex := FCompany.CompanyInfo.DontOfferIndex;
          except
            CompanyInfo.DontOfferIndex := -1;
          end;
          try
            CompanyInfo.ShareHoldersNoIndex := FCompany.CompanyInfo.ShareHoldersNoIndex;
          except
            CompanyInfo.SharesRestrictIndex := -1;
          end;
          try
            CompanyInfo.SharesRestrictChapters := FCompany.CompanyInfo.SharesRestrictChapters;
          except
            CompanyInfo.SharesRestrictChapters := '';
          end;
          try
            CompanyInfo.DontOfferChapters := FCompany.CompanyInfo.DontOfferChapters;
          except
            CompanyInfo.DontOfferChapters := '';
          end;
          try
            CompanyInfo.ShareHoldersNoChapters := FCompany.CompanyInfo.ShareHoldersNoChapters;
          except
            CompanyInfo.ShareHoldersNoChapters := '';
          end;
          try
            CompanyInfo.LimitedSignatoryIndex := FCompany.CompanyInfo.LimitedSignatoryIndex;
          except
            CompanyInfo.LimitedSignatoryIndex := -1;
          end;
          try
             CompanyInfo.LegalSectionsChapters := FCompany.CompanyInfo.LegalSectionsChapters;
          except
             CompanyInfo.LegalSectionsChapters := '';
          end;
          try
            CompanyInfo.SignatorySectionsChapters := FCompany.CompanyInfo.SignatorySectionsChapters;
          except
            CompanyInfo.SignatorySectionsChapters := '';
          end;
          try
            CompanyInfo.ConfirmedCopies := FCompany.CompanyInfo.ConfirmedCopies;
          except
            CompanyInfo.ConfirmedCopies := '';
          end;
          try
            CompanyInfo.MoreCopies := FCompany.CompanyInfo.MoreCopies;
          except
            CompanyInfo.MoreCopies := '';
          end;
          try
            CompanyInfo.RegulationItemsIndex := FCompany.CompanyInfo.RegulationItemsIndex;
          except
            CompanyInfo.RegulationItemsIndex := -1;
          end;
        end;
      end;
    end;
  end;

  // Final - Saving all data!
  WriteChangeLog( #9 + 'Final - Saving all data');

  Result := TCompany.CreateNew(
      Date,
      e11.Text,
      e12.Text,
      TCompanyAddress.CreateNew(e13.Text, e14.Text, e16.Text, e15.Text, '�����', e17.Text, e18.Text, e19.Text, e110.Text),
      Directors,
      StockHolders,
      HonList,
      MuhazList,
      Managers,
      CompanyInfo,
      Self.SubmissionData,
      nil);
  WriteChangeLog( #9 + 'Collect data end');
end;

procedure TfmCompanyInfoTemplate.EvFillData(Company: TCompany);
var
  I,J: Integer;
begin
  Loading:= True;
  With Company Do
  begin
    // Phase 1
    e11.Text:= Name;
    e12.Text:= Zeut;
    if CompanyInfo <> nil then
      e12a.Text:= DateToLongStr(CompanyInfo.RegistrationDate);
    e13.Text:= Address.Street;
    e14.Text:= Address.Number;
    e15.Text:= Address.City;
    e16.Text:= Address.Index;
    e17.Text:= Address.POB;
    e18.Text:= Address.Ezel;
    e19.Text:= Address.Phone;
    e110.Text:= Address.Fax;

    // Phase 2
    HonList.Filter:= afActive;
    Grid2.RowCount:= HonList.Count;
    Grid2.ResetLine(0);

    For I:= 0 to HonList.Count-1 do
    begin
      Grid2.Cells[0,I]:= THonItem(HonList.Items[I]).Name;
      Grid2.Cells[1,I]:= FormatNumber(FloatToStr(THonItem(HonList.Items[I]).Value), 15,4,True);
      Grid2.Cells[2,I]:= FormatNumber(FloatToStr(THonItem(HonList.Items[I]).Rashum),15,2,True);
      Grid2.Cells[3,I]:= FormatNumber(FloatToStr(THonItem(HonList.Items[I]).Mokza),15,2,True);
      Grid2.Cells[4,I]:= FormatNumber(FloatToStr(THonItem(HonList.Items[I]).Cash),15,2,True);
      Grid2.Cells[5,I]:= FormatNumber(FloatToStr(THonItem(HonList.Items[I]).NonCash),15,2,True);
      Grid2.Cells[6,I]:= FormatNumber(FloatToStr(THonItem(HonList.Items[I]).Part),15,2,True);
      Grid2.Cells[7,I]:= FormatNumber(FloatToStr(THonItem(HonList.Items[I]).PartValue),15,2,True);
    end;

    MuhazList.Filter:= afActive;
    Grid2a.RowCount:= MuhazList.Count;

    Grid2a.ResetLine(0);

    For I:= 0 To MuhazList.Count-1 do
    begin
      Grid2a.Cells[0,I]:= TMuhazItem(MuhazList.Items[I]).Name;
      Grid2a.Cells[1,I]:= FormatNumber(FloatToStr(TMuhazItem(MuhazList.Items[I]).Value),15,4,True);
      Grid2a.Cells[2,I]:= TMuhazItem(MuhazList.Items[I]).Zeut;
      Grid2a.Cells[3,I]:= FormatNumber(FloatToStr(TMuhazItem(MuhazList.Items[I]).ShtarValue),15,2,True);
    end;

    // Phase 3
    e31.Items.Clear;
    TempGrid31.RowCount:= 1;
    TempGrid31.Rows[0].Clear;
    TempGrid32.RowCount:= 1;
    TempGrid32.Rows[0].Clear;
    if Self is TfmShowCompany Then
      StockHolders.Filter:= afActive
    Else
      StockHolders.Filter:= afAll;
    For I:= StockHolders.Count-1 DownTo 0 do
      With TStockHolder(StockHolders.Items[I]) Do
      begin
        // Address info
        TempGrid31.RowCount:= TempGrid31.RowCount+1;
        TempGrid31.Cells[0,TempGrid31.RowCount-1]:= IntToStr(TempGrid31.RowCount-1);
        e31.Items.Add(TempGrid31.Cells[0,TempGrid31.RowCount-1]);
        TempGrid31.Cells[1,TempGrid31.RowCount-1]:= Name;
        TempGrid31.Cells[2,TempGrid31.RowCount-1]:= Zeut;
        TempGrid31.Cells[3,TempGrid31.RowCount-1]:= Address.Street;
        TempGrid31.Cells[4,TempGrid31.RowCount-1]:= Address.Number;
        TempGrid31.Cells[5,TempGrid31.RowCount-1]:= Address.Index;
        TempGrid31.Cells[6,TempGrid31.RowCount-1]:= Address.City;
        TempGrid31.Cells[7,TempGrid31.RowCount-1]:= Address.Country;
        If Taagid then
          TempGrid31.Cells[8,TempGrid31.RowCount-1]:= '1'
        Else
          TempGrid31.Cells[8,TempGrid31.RowCount-1]:= '0';

        // Stocks
        Stocks.Filter:= afActive;
        For J:= Stocks.Count-1 Downto 0 do
          With Stocks.Items[J] as TStock Do
          begin
             TempGrid32.RowCount:= TempGrid32.RowCount+1;
             TempGrid32.Cells[0, TempGrid32.RowCount - 1]:= TempGrid31.Cells[0,TempGrid31.RowCount-1];
             TempGrid32.Cells[1, TempGrid32.RowCount - 1]:= Name;
             TempGrid32.Cells[2, TempGrid32.RowCount - 1]:= FormatNumber(FloatToStr(Value),15,4,True);
             TempGrid32.Cells[3, TempGrid32.RowCount - 1]:= FormatNumber(FloatToStr(Count),15,2,True);
             TempGrid32.Cells[4, TempGrid32.RowCount - 1]:= FormatNumber(FloatToStr(Paid),15,2,True);
          end;
      end;
    If e31.Items.Count= 0 Then
      e31.Items.Add('1');
    e31.ItemIndex:= 0;
    e31.Tag:=1;

    // Phase 4
    e41.Items.Clear;
    TempGrid4.RowCount:=1;
    TempGrid4.Rows[0].Clear;
    Directors.Filter:= afActive;
    For I:= Directors.Count-1 DownTo 0 Do
      With Directors.Items[I] as TDirector Do
      begin
        TempGrid4.RowCount:= TempGrid4.RowCount+1;
        TempGrid4.Rows[TempGrid4.RowCount-1].Clear;
        TempGrid4.Cells[0,TempGrid4.RowCount-1]:= IntToStr(TempGrid4.RowCount-1);
        e41.Items.Add(TempGrid4.Cells[0,TempGrid4.RowCount-1]);
        If Taagid<> nil Then
          TempGrid4.Cells[1,TempGrid4.RowCount-1]:= '1'
        Else
          TempGrid4.Cells[1,TempGrid4.RowCount-1]:= '0';
        TempGrid4.Cells[2,TempGrid4.RowCount-1]:= DateToLongStr(Date1);
        TempGrid4.Cells[3,TempGrid4.RowCount-1]:= DateToLongStr(Date2);
        TempGrid4.Cells[4,TempGrid4.RowCount-1]:= Name;
        TempGrid4.Cells[5,TempGrid4.RowCount-1]:= Zeut;
        TempGrid4.Cells[6,TempGrid4.RowCount-1]:= Address.Phone;
        TempGrid4.Cells[7,TempGrid4.RowCount-1]:= Address.Street;
        TempGrid4.Cells[8,TempGrid4.RowCount-1]:= Address.Number;
        TempGrid4.Cells[9,TempGrid4.RowCount-1]:= Address.Index;
        TempGrid4.Cells[10,TempGrid4.RowCount-1]:= Address.City;
        TempGrid4.Cells[11,TempGrid4.RowCount-1]:= Address.Country;
        If Taagid<>nil Then
        begin
          TempGrid4.Cells[12,TempGrid4.RowCount-1]:= Taagid.Name;
          TempGrid4.Cells[13,TempGrid4.RowCount-1]:= Taagid.Zeut;
          TempGrid4.Cells[14,TempGrid4.RowCount-1]:= Taagid.Street;
          TempGrid4.Cells[15,TempGrid4.RowCount-1]:= Taagid.Number;
          TempGrid4.Cells[16,TempGrid4.RowCount-1]:= Taagid.Index;
          TempGrid4.Cells[17,TempGrid4.RowCount-1]:= Taagid.City;
          TempGrid4.Cells[18,TempGrid4.RowCount-1]:= Taagid.Country;
        end;
      end;
    If e41.Items.Count=0 Then
      e41.Items.Add('1');
    e41.ItemIndex:= 0;
    e41.Tag:=1;

    // Phase 5a
    if CompanyInfo <> nil then
    begin
      With CompanyInfo Do
      begin
        e51.Text:= SigName;
        e52.Text:= SigZeut;
        e53.Text:= SigTafkid;

        // Page 6
        e76.Text:= LawName;
        e77.Text:= LawZeut;
        e78.Text:= LawAddress;
        e79.Text:= LawLicence;
        e75a.Text:= OtherInfo;

        e71.Checked:= (Option1 and 1) > 0;
        e72.Checked:= (Option1 and 2) > 0;
        e72a.Checked:= (Option1 and 4) > 0;
        e72b.Checked:= (Option1 and 8) > 0;

        e73.Checked:= (Option3 = '1');
        e75.Checked:= (Option3 = '3');

        //english name
        edEnglishName.Text := EnglishName;
      end;

      FTakanon2_1 := CompanyInfo.Takanon2_1;
      FTakanon2_2 := CompanyInfo.Takanon2_2;
      FTakanon2_3 := CompanyInfo.Takanon2_3;
      FTakanon2_4 := CompanyInfo.Takanon2_4;
      FTakanon4 := CompanyInfo.Takanon4;
      FTakanon5 := CompanyInfo.Takanon5;
      FTakanon7 := CompanyInfo.Takanon7;

    end;
    // Phase 5b
    grid5.RowCount := 1;
    Grid5.Rows[0].Clear;
    Managers.Filter := afActive;
    For I := 0 To Managers.Count-1 do
      With Managers.Items[I] As TManager Do
      begin
        grid5.Cells[0, Grid5.RowCount-1] := Name;
        grid5.Cells[1, Grid5.RowCount-1] := Zeut;
        grid5.Cells[2, Grid5.RowCount-1] := Address;
        grid5.Cells[3, Grid5.RowCount-1] := Phone;
        grid5.Cells[4, Grid5.RowCount-1] := Mail;
        grid5.RowCount := grid5.RowCount+1;
      end;
      If Grid5.RowCount > 1 Then
        Grid5.RowCount := Grid5.RowCount-1;
  end;
  Load3(1);
  Load4(1);

  Self.SubmissionData := nil; //no need to load it at this stage (but necessary to clear previous)
  Loading:= False;
end;

function TfmCompanyInfoTemplate.EvSaveData(Draft: Boolean): Boolean;
var
  OldCompany, NewCompany: TCompany;
  TempFilesList: TFilesList;
  OldFile, FileItem: TFileItem;
  FileInfo: TFileInfo;
  Index: Integer;
//  TempFilesList: TFilesList;
begin
//  inherited EvSaveData(Draft);
  try
    WriteChangeLog( '========================');
    WriteChangeLog( 'Save data event start');
    NewCompany := EvCollectData;
    If Draft Then
    begin
      WriteChangeLog( #9 + 'Draft file');
      FFilesList:= TFilesList.Create({StrToInt(Company.Zeut)}-1, faAddingNew, True);
      EraseFile;
      FileInfo:= TFileInfo.Create;
      FileInfo.Action:= faAddingNew;
      FileInfo.FileName:= e11.Text;
      FileInfo.DecisionDate:= Now;
      FileInfo.NotifyDate:= Now;
      FileInfo.Draft:= Draft;
      FileInfo.CompanyNum:= StrToInt(NewCompany.Zeut);
      FFilesList.Add(FileInfo);
      FileItem:= TFileItem.Create(FFilesList, FileInfo);
      NewCompany.ExSaveData(FileItem, Nil);
      Self.Company := TCompany.LoadFromFile(nil, FileItem);
      FileName:= FileInfo.FileName;
      FileItem.Free;
      FFilesList.Free;
      SetFileIsDraft(True, StrToInt(Company.Zeut));
    end
    Else
    begin
      WriteChangeLog( #9 + 'Permanent file');
      NewCompany.SaveData(Nil, True, False, -1);
      If FileIsDraft and (FileName <> '') and (CurrentFileRecNum <> -1) Then
      begin
        TempFilesList:= TFilesList.Create(CurrentFileRecNum, faAddingNew, True);
        try
          For Index:= 0 To TempFilesList.Count-1 do
            If TempFilesList.FileInfo[Index].CompanyNum = CurrentFileRecNum Then
            begin
              OldFile:= TempFilesList.Items[Index];
              OldCompany := TCompany.LoadFromFile(nil, OldFile);
              OldCompany.FileDelete(OldFile);
              OldFile.Free;
              OldCompany.Free;
            end;
        finally
          TempFilesList.Free;
        end;
      end;
      SetFileIsDraft(Draft, StrToInt(NewCompany.Zeut));
      // TempFilesList:= TempFilesList.Create(StrToInt(Company.Zeut), faAddingNew, True
    end;
    FileName:= e11.Text;
    NewCompany.Free;
    fmDialog2.Show;
  finally
  end;
//  inherited EvSaveData(Draft);
  Result:= True;
  WriteChangeLog( 'Save data event end');
end;

procedure TfmCompanyInfoTemplate.EvDeleteEmptyRows;
var
  I: Integer;
begin
  // Grid2
  With Grid2 Do
    For I:= RowCount-1 DownTo 0 Do
      If (RemoveSpaces(Cells[0,I], rmBoth) = '') And CheckNumberField(Cells[1,I]) And CheckNumberField(Cells[2,I]) And
          CheckNumberField(Cells[3,I]) And CheckNumberField(Cells[4,I]) And CheckNumberField(Cells[5,I]) And
          CheckNumberField(Cells[6,I]) And CheckNumberField(Cells[7,I]) Then
        LineDeleteByIndex(I);

  // Grid2a
  With Grid2a Do
    For I:= RowCount-1 DownTo 0 Do
      If (RemoveSpaces(Cells[0,I], rmBoth) = '') And CheckNumberField(Cells[1,I]) And (RemoveSpaces(Cells[2,I], rmBoth) = '') And
          CheckNumberField(Cells[3,I]) Then
        LineDeleteByIndex(I);

  // Grid3
  With Grid3 Do
    For I:= RowCount-1 DownTo 0 Do
      If (RemoveSpaces(Cells[0,I], rmBoth) = '') And CheckNumberField(Cells[1,I]) And CheckNumberField(Cells[2,I]) And
          CheckNumberField(Cells[3,I]) Then
        LineDeleteByIndex(I);

  // Grid5
  With Grid5 Do
    For I:= RowCount-1 DownTo 0 Do
      If (RemoveSpaces(Cells[0,I], rmBoth) = '') And CheckNumberField(Cells[1,I]) And (RemoveSpaces(Cells[2,I], rmBoth) = '') And
         (RemoveSpaces(Cells[3,I], rmBoth) = '') Then
        LineDeleteByIndex(I);
end;

procedure TfmCompanyInfoTemplate.FormCreate(Sender: TObject);
const
  StockHolderText:string='�� ��� ������';
  DirectorText:string='�� ��������';
  CorpDirectorText:string='�� �������� ���� �����';
begin
  inherited;
  Loading:= True;
  
  FieldNames.Clear;
  FieldNames.Add(l1.Caption+'='+e11.Name);
  FieldNames.Add(lblEngName.Caption+'='+edEnglishName.Name);
  FieldNames.Add(HebLabel2.Caption+'='+e12.Name);
  FieldNames.Add(HebLabel48a.Caption+'='+e12a.Name);
  FieldNames.Add(HebLabel3.Caption+'='+e13.Name); //street
  FieldNames.Add(HebLabel4.Caption+'='+e14.Name); //house
  FieldNames.Add(HebLabel6.Caption+'='+e15.Name);//city
  FieldNames.Add(HebLabel5.Caption+'='+e16.Name);//zip
  FieldNames.Add(HebLabel7.Caption+'='+e17.Name);//POB
  FieldNames.Add(HebLabel8.Caption+'='+e18.Name);//Ezel
  FieldNames.Add(HebLabel9.Caption+'='+e19.Name);//Phone
  FieldNames.Add(HebLabel10.Caption+'='+e110.Name);//Fax
  FieldNames.Add('����� ���'+'='+Grid2.Name);//halukat hon
  FieldNames.Add(HebLabel1354.Caption+'='+Grid2a.Name);//muhaz
  FieldNames.Add(HebLabel22.Caption+'='+e31.Name);// num of stockholder
  FieldNames.Add(HebLabel1a.Caption+'='+e31a.Name);//stockholder is corporation
  FieldNames.Add(HebLabel15.Caption+'='+e32.Name);//stockholder name
  FieldNames.Add(HebLabel21.Caption+' '+StockHolderText+'='+e33.Name);//stockholder ID
  FieldNames.Add(HebLabel18.Caption+' '+StockHolderText+'='+e34.Name);//stockholder street
  FieldNames.Add(HebLabel16.Caption+' '+StockHolderText+'='+e35.Name);//stockholder house
  FieldNames.Add(HebLabel17.Caption+' '+StockHolderText+'='+e36.Name);//stockholder ZIP
  FieldNames.Add(HebLabel19.Caption+' '+StockHolderText+'='+e37.Name);//stockholder city
  FieldNames.Add(HebLabel20.Caption+' '+StockHolderText+'='+e38.Name);//stockholder country
  FieldNames.Add('������ ������� ��� ������'+'='+Grid3.Name);//stockholder stocks
  FieldNames.Add(HebLabel28.Caption+'='+e41.Name);//director number
  FieldNames.Add(HebLabel31.Caption+'='+e42.Name);//director is corporation
  FieldNames.Add(HebLabel27.Caption+' '+DirectorText+'='+e45.Name);//director name
  FieldNames.Add(HebLabel23.Caption+' '+DirectorText+'='+e46.Name);//director ID
  FieldNames.Add(HebLabel52.Caption+' '+DirectorText+'='+e46b.Name);//director phone
  FieldNames.Add(HebLabel32.Caption+' '+DirectorText+'='+e43.Name);//director minui date
  FieldNames.Add(HebLabel33.Caption+' '+DirectorText+'='+e44.Name);//director start date
  FieldNames.Add(HebLabel26.Caption+' '+DirectorText+'='+e47.Name);//director street
  FieldNames.Add(HebLabel25.Caption+' '+DirectorText+'='+e48.Name);//director house
  FieldNames.Add(HebLabel24.Caption+' '+DirectorText+'='+e49.Name);//director ZIP
  FieldNames.Add(HebLabel30.Caption+' '+DirectorText+'='+e410.Name);//director city
  FieldNames.Add(HebLabel29.Caption+' '+DirectorText+'='+e411.Name);//director country
  FieldNames.Add(HebLabel40.Caption+' '+CorpDirectorText+'='+e412.Name);//corporation director name
  FieldNames.Add(HebLabel39.Caption+' '+CorpDirectorText+'='+e413.Name);//corporation director ID
  FieldNames.Add(HebLabel35.Caption+' '+CorpDirectorText+'='+e414.Name);//corporation director street
  FieldNames.Add(HebLabel36.Caption+' '+CorpDirectorText+'='+e415.Name);//corporation director house
  FieldNames.Add(HebLabel38.Caption+' '+CorpDirectorText+'='+e416.Name);//corporation director ZIP
  FieldNames.Add(HebLabel34.Caption+' '+CorpDirectorText+'='+e417.Name);//corporation director city
  FieldNames.Add(HebLabel37.Caption+' '+CorpDirectorText+'='+e418.Name);//corporation director country
  FieldNames.Add(HebLabel43.Caption+' '+HebLabel42.Caption+'='+e51.Name);//signer name
  FieldNames.Add(HebLabel43.Caption+' '+HebLabel44.Caption+'='+e52.Name);//signer ID
  FieldNames.Add(HebLabel43.Caption+' '+HebLabel45.Caption+'='+e53.Name);//signer role
  FieldNames.Add('������ ������'+'='+grid5.Name);//managers
  FieldNames.Add(HebLabel70.Caption+':'+HebLabel72.Caption+'='+e76.Name);//lawyer name
  FieldNames.Add(HebLabel70.Caption+':'+HebLabel71.Caption+'='+e77.Name);//lawyer ID
  FieldNames.Add(HebLabel70.Caption+':'+HebLabel73.Caption+'='+e78.Name);//lawyer address
  FieldNames.Add(HebLabel70.Caption+':'+HebLabel74.Caption+'='+e79.Name);//lawyer licence
  Date := EncodeDate(1999, 01, 01);
  DATA.taCity.First;
  While Not DATA.taCity.EOF do
  begin
    e15.Items.Add(DATA.taCity.Fields[0].Text);
    e37.Items.Add(DATA.taCity.Fields[0].Text);
    e410.Items.Add(DATA.taCity.Fields[0].Text);
    e417.Items.Add(DATA.taCity.Fields[0].Text);
    DATA.taCity.Next;
  end;

  DATA.taCountry.First;
  While Not DATA.taCountry.EOF do
  begin
    e38.Items.Add(DATA.taCountry.Fields[0].Text);
    e411.Items.Add(DATA.taCountry.Fields[0].Text);
    e418.Items.Add(DATA.taCountry.Fields[0].Text);
    DATA.taCountry.Next;
  end;

  DATA.taStocks.First;
  While Not Data.taStocks.EOF do
  begin
    nGrid2.Cells[0,nGrid2.RowCount-1] := IntToStr(nGrid2.RowCount);
    nGrid2.Cells[1,nGrid2.RowCount-1] := Data.taStocks.Fields[0].Text;
    nGrid2.RowCount := nGrid2.RowCount+1;
    Data.taStocks.Next;
  end;
  if nGrid2.RowCount > 1 then
    nGrid2.RowCount := nGrid2.RowCount-1;

  e38.Text := '�����';
  e411.Text := '�����';
  e418.Text := '�����';

  e42.ItemIndex  := 0;
  e31a.ItemIndex := 0;
  e31.ItemIndex  := 0;
  e41.ItemIndex  := 0;
  Grid2.ResetLine(0);
  Grid2a.ResetLine(0);
  Grid3.ResetLine(0);
  Grid5.ResetLine(0);
  ManagerID :='';
  CurrManager :=0;
  Loading := False;
end;

procedure TfmCompanyInfoTemplate.Grid2Enter(Sender: TObject);
var
  CanSelect : boolean;
  I: Integer;
begin
  inherited;
  if Sender = Grid2 then
  begin
    nGrid2a.Visible:= False;
    if (Grid2.Row = 0) and (Grid2.Col = 0) then
      Grid2SelectCell(Grid2,0,0,CanSelect);
  end;

  if Sender = Grid3 then
    if (Grid3.Row = 0) and (Grid3.Col = 0) then
      Grid2SelectCell(Grid3,0,0,CanSelect);

  if Sender = Grid2a then
  begin
    nGrid2.Visible:= False;
    EvDeleteEmptyRows;
    nGrid2a.RowCount:= 0;
    nGrid2a.Rows[0].Clear;
    For I:= 0 To Grid2.RowCount-1 Do
      try
        If (nGrid2.Cols[1].IndexOf(Grid2.Cells[0,I])<>-1) And (MyStrToFloat(Grid2.Cells[3,I]) > 0) Then
        begin
          nGrid2a.Cells[0, nGrid2a.RowCount-1]:= IntToStr(nGrid2a.RowCount);
          nGrid2a.Cells[1, nGrid2a.RowCount-1]:= Grid2.Cells[0,I];
          nGrid2a.Cells[2, nGrid2a.RowCount-1]:= Grid2.Cells[1,I];
          nGrid2a.RowCount:= nGrid2a.RowCount+1;
        end;
      Except
      End;
    If nGrid2a.RowCount>1 Then
      nGrid2a.RowCount:= nGrid2a.RowCount-1;

    if (Grid2a.Row = 0) and (Grid2a.Col = 0) then
      Grid2SelectCell(Grid2a,0,0,CanSelect);
  end;
end;

procedure TfmCompanyInfoTemplate.Grid2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if ReadOnly then
    Exit;
  if (Key = #13) then
  begin
    if ((Sender as TSDGrid).EditorMode = False) then
      (Sender as TSDGrid).EditorMode := True
    else
    begin
      nGrid2.Visible  := False;
      nGrid3.Visible  := False;
      nGrid2a.Visible := False;
    end;
    //Grid2.Col := 0;
    {if (Grid2.PCol = Grid2.ColCount-1) then
      Grid2.PCol := 0;
    if (Grid2.Col = Grid2.ColCount-1) then
      Grid2.Col := 0;
      }
  end;
end;

procedure TfmCompanyInfoTemplate.grid5KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if ReadOnly then
    Exit;
  if (Key = #13) then
  begin
    if (Grid5.EditorMode = False) then
      Grid5.EditorMode := True
  end;
  if (Key > #32) then
  case Grid5.Col of
    0: if Length(Grid5.Cells[0, Grid5.Row]) > 79 then
         Key:= #0;
    1: if (not (Key in ['0'..'9'])) then
         Key:= #0;
    2: if Length(Grid5.Cells[2, Grid5.Row]) > 59 then
         Key:= #0;
    3: if Length(Grid5.Cells[3, Grid5.Row]) > 14 then
         Key:= #0;
    4: if Length(Grid5.Cells[4, Grid5.Row]) > 119 then
         Key:= #0;
  end;
end;

procedure TfmCompanyInfoTemplate.Grid2SelectCell(Sender: TObject; Col,
  Row: Integer; var CanSelect: Boolean);
var
  i,Found        : integer;
begin
  inherited;
  // GRID 2
  if Sender=Grid2 then
    begin
      if  Grid2.PCol = 1 then
        Grid2.Cells[Grid2.Pcol,Grid2.Prow] := FormatNumber(Grid2.Cells[Grid2.Pcol,Grid2.Prow],10,4,True);
      if (Grid2.PCol = 2) or (Grid2.PCol = 3) or (Grid2.PCol = 4)
          or (Grid2.PCol = 5) or (Grid2.PCol = 6) or (Grid2.PCol = 7) then
        Grid2.Cells[Grid2.Pcol,Grid2.Prow] := FormatNumber(Grid2.Cells[Grid2.Pcol,Grid2.Prow],10,2,True);

      if (grid2.Pcol=0) then
        begin
          Found:=-1;
            for i:=0 to nGrid2.RowCount-1 do
              if (Grid2.Cells[0,Grid2.PRow]=nGrid2.Cells[0,i]) and (RemoveSpaces(nGrid2.Cells[0,I],rmBoth) <>'') then
                Found:=i;
          if Found<>-1 then
            Grid2.Cells[0,Grid2.PRow]:=nGrid2.Cells[1,Found];
        end;

      If Visible then // Delphi Bug-Fix
        nGrid2.Visible:= Col=0;
    end;

  // GRID 2a
  if Sender=Grid2a then
    begin
      if  Grid2a.PCol = 1 then
        Grid2a.Cells[Grid2a.Pcol,Grid2a.Prow] := FormatNumber(Grid2a.Cells[Grid2a.Pcol,Grid2a.Prow],10,4,True);
      if (Grid2a.PCol = 3) then
        Grid2a.Cells[Grid2a.Pcol,Grid2a.Prow] := FormatNumber(Grid2a.Cells[Grid2a.Pcol,Grid2a.Prow],10,2,True);

      if (Grid2a.Pcol=0) then
        begin
          Found:=-1;
            for i:=0 to nGrid2a.RowCount-1 do
              if (Grid2a.Cells[0,Grid2a.PRow]=nGrid2a.Cells[0,i]) and (RemoveSpaces(nGrid2a.Cells[0,I], rmBoth) <> '') then
                Found:=i;
          if Found<>-1 then
            begin
              Grid2a.Cells[0,Grid2a.PRow]:=nGrid2a.Cells[1,Found];
              Grid2a.Cells[1,Grid2a.PRow]:=nGrid2a.Cells[2,Found];
            end;
        end;

      If Visible Then      // Delphi Bug-Fix
        nGrid2a.Visible:= Col=0;
    end;

  // GRID 3
  if Sender=Grid3 then
    begin
      if  Grid3.PCol = 1 then
        Grid3.Cells[Grid3.Pcol,Grid3.Prow] := FormatNumber(Grid3.Cells[Grid3.Pcol,Grid3.Prow],10,4,True);
      if (Grid3.PCol = 2) or (Grid3.PCol = 3) then
        Grid3.Cells[Grid3.Pcol,Grid3.Prow] := FormatNumber(Grid3.Cells[Grid3.Pcol,Grid3.Prow],10,2,True);

      if (grid3.Pcol=0) then
        begin
          Found:=-1;
          for i:=0 to nGrid3.RowCount-1 do
            if (Grid3.Cells[0,Grid3.PRow]=nGrid3.Cells[0,i]) and (RemoveSpaces(nGrid3.Cells[0,i], rmBoth) <> '') then
              Found:=i;
          if Found<>-1 then
            begin
              Grid3.Cells[0,Grid3.PRow]:=nGrid3.Cells[1,Found];
              Grid3.Cells[1,Grid3.PRow]:=nGrid3.Cells[2,Found];
            end;
        end;

      If Visible Then          // Delphi Bug-Fix
        nGrid3.Visible:= Col=0;
    end;

  // GRID 5
  if Sender=Grid5 then
    begin
      CheckManagerID;
      if Grid5.PCol = 1 then
        Grid5.Cells[Grid5.Pcol,Grid5.Prow] := Trim(Grid5.Cells[Grid5.Pcol,Grid5.Prow]);
       // Grid5.Cells[Grid5.Pcol,Grid5.Prow] := FormatNumber(Grid5.Cells[Grid5.Pcol,Grid5.Prow],9,0,True);
    end;
end;

procedure TfmCompanyInfoTemplate.nGridDblClick(Sender: TObject);
var
  VKKEY: Word;
begin
  Inherited;
  If ReadOnly Then
    Exit;

  VKKEY:= VK_NONAME;
  if Sender = nGrid2 then
    begin
      Grid2.Cells[0,Grid2.Row] := nGrid2.Cells[1,nGrid2.Row];
      Grid2.Col := 1;
      Grid2.SetFocus;
      GridKeyDown(Grid2, VKKEY, []);
    end
  else
    if Sender = nGrid3 then
      begin
        Grid3.Cells[0,Grid3.Row] := nGrid3.Cells[1,nGrid3.Row];
        Grid3.Cells[1,Grid3.Row] := nGrid3.Cells[2,nGrid3.Row];
        Grid3.Col := 2;
        Grid3.SetFocus;
        GridKeyDown(Grid3, VKKEY, []);
      end
    Else
      if Sender = nGrid2a then
        begin
          Grid2a.Cells[0, Grid2a.Row] := nGrid2a.Cells[1, nGrid2a.Row];
          Grid2a.Cells[1, Grid2a.Row] := nGrid2a.Cells[2, nGrid2a.Row];
          Grid2a.Col := 2;
          Grid2a.SetFocus;
          GridKeyDown(Grid2a, VKKEY, []);
        end;
end;

procedure TfmCompanyInfoTemplate.nGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (key = VK_RETURN) then
    nGridDblClick(Sender);

  if (key = VK_ESCAPE) then
    if Sender = nGrid2 then
      begin
        Grid2.Col := 1;
        Grid2.SetFocus;
      end
    else
      if Sender = nGrid3 then
        begin
          Grid3.Col := 2;
          Grid3.SetFocus;
        end
      Else
        If Sender = nGrid2a then
          begin
            Grid2a.Col := 2;
            Grid2a.SetFocus;
          end;
end;

procedure TfmCompanyInfoTemplate.EvPageIndexChange;
var
  I: Integer;
  ctl:TWinControl;
begin
  inherited;

  Case PageIndex Of
      0: ctl:=e11;
      1: ctl:=Grid2;
      2: ctl:=e32;
      3: ctl:=e45;
      4: ctl:=e51;
      5: ctl:=e71;
      else ctl:=nil;
  end;

  if ctl<>nil then
    if CanFocusOnControl(ctl)then
      ctl.SetFocus;
//  if ctl=nil then
    //ShowMessage('No control to focus on')
//  else if ctl.CanFocus then
   // ctl.SetFocus;
 // else
    //ShowMessage('Cannot focus on '+ctl.Name+','+GetDisabledParentName(ctl));
  if PageIndex<>2 then exit;
  // Fill nGrid
  EvDeleteEmptyRows;
  nGrid3.RowCount:= 0;
  nGrid3.Rows[0].Clear;
  For I:= 0 To Grid2.RowCount-1 Do
    try
      If (nGrid2.Cols[1].IndexOf(Grid2.Cells[0,I])<>-1) And (MyStrToFloat(Grid2.Cells[3,I]) > 0) Then
        begin
          nGrid3.Cells[0, nGrid3.RowCount-1]:= IntToStr(nGrid3.RowCount);
          nGrid3.Cells[1, nGrid3.RowCount-1]:= Grid2.Cells[0,I];
          nGrid3.Cells[2, nGrid3.RowCount-1]:= Grid2.Cells[1,I];
          nGrid3.RowCount:= nGrid3.RowCount+1;
        end;
      Except
      End;
  If nGrid3.RowCount>1 Then
    nGrid3.RowCount:= nGrid3.RowCount-1;
end;

procedure TfmCompanyInfoTemplate.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  If Assigned(Company) Then
    begin
      Company.Free;
      Company:= Nil;       // Do not change this line - see "ShowCompany" unit
    end;
end;

procedure TfmCompanyInfoTemplate.btSearchStockHoldersClick(
  Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= EvCollectData;
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
    begin
      TempCompany.Free;
      e32.SetFocus;
      Exit;
    end;

  Loading:= True;
  If FResult is TStockHolder Then
    With FResult as TStockHolder do
      begin
        if Taagid Then
          e31a.ItemIndex:= 1
        Else
          e31a.ItemIndex:= 0;
        e32.Text:= Name;
        e33.Text:= Zeut;
        e34.Text:= Address.Street;
        e35.Text:= Address.Number;
        e36.Text:= Address.Index;
        e37.Text:= Address.City;
        e38.Text:= Address.Country;
      end;

  if FResult is TDirector Then
    With FResult as TDirector do
      begin
        if Taagid<> nil Then
          e31a.ItemIndex:= 1
        Else
          e31a.ItemIndex:= 0;
        e32.Text:= Name;
        e33.Text:= Zeut;
        e34.Text:= Address.Street;
        e35.Text:= Address.Number;
        e36.Text:= Address.Index;
        e37.Text:= Address.City;
        e38.Text:= Address.Country;
      end;
  TempCompany.Free;
  Loading:= False;
  DataChanged:= True;
  e32.SetFocus;
end;

procedure TfmCompanyInfoTemplate.btSearchDirectorsClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= EvCollectData;
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
    begin
      e45.SetFocus;
      TempCompany.Free;
      Exit;
    end;

  Loading:= True;
  If FResult is TStockHolder Then
    With FResult as TStockHolder do
      begin
        if Taagid Then
          begin
            e42.ItemIndex:= 1;
            e42Change(e42);
            e412.Text:= '';
            e413.Text:= '';
            e414.Text:= '';
            e415.Text:= '';
            e416.Text:= '';
            e417.Text:= '';
            e418.Text:= '�����';
          end
        Else
          begin
            e42.ItemIndex:= 0;
            e42Change(e42);
          end;
        e45.Text:= Name;
        e46.Text:= Zeut;
        e46b.Text:= '';
        e43.Text:= DateToLongStr(Now);
        e44.Text:= DateToLongStr(Now);
        e47.Text:= Address.Street;
        e48.Text:= Address.Number;
        e49.Text:= Address.Index;
        e410.Text:= Address.City;
        e411.Text:= Address.Country;
      end;

  if FResult is TDirector Then
    With FResult as TDirector do
      begin
        if Taagid<> nil Then
          begin
            e42.ItemIndex:= 1;
            e42Change(e42);
            e412.Text:= Taagid.Name;
            e413.Text:= Taagid.Zeut;
            e414.Text:= Taagid.Street;
            e415.Text:= Taagid.Number;
            e416.Text:= Taagid.Index;
            e417.Text:= Taagid.City;
            e418.Text:= Taagid.Country;
          end
        Else
          begin
            e42.ItemIndex:= 0;
            e42Change(e42);
          end;
        e45.Text:= Name;
        e46.Text:= Zeut;
        e46b.Text:= Address.Phone;
        e43.Text:= DateToLongStr(Now);
        e44.Text:= DateToLongStr(Now);
        e47.Text:= Address.Street;
        e48.Text:= Address.Number;
        e49.Text:= Address.Index;
        e410.Text:= Address.City;
        e411.Text:= Address.Country;
      end;
  TempCompany.Free;
  Loading:= False;
  DataChanged:= True;
  e45.SetFocus;
end;

procedure TfmCompanyInfoTemplate.btSearchSignNameClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= EvCollectData;
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  if FResult = nil Then
    begin
      e51.SetFocus;
      TempCompany.Free;
      Exit;
    end;

  Loading:= True;
  With FResult do
    begin
      e51.Text:= Name;
      e52.Text:= Zeut;
    end;

  TempCompany.Free;
  Loading:= False;
  DataChanged:= True;
  e51.SetFocus;
end;

procedure TfmCompanyInfoTemplate.gbSearchClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= EvCollectData;
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
    begin
      Grid5.SetFocus;
      TempCompany.Free;
      Exit;
    end;

  Loading:= True;
  If FResult is TStockHolder Then
    With FResult as TStockHolder do
    begin
      If Grid5.Cells[0,Grid5.RowCount-1] <> '' Then
        Grid5.RowCount := Grid5.RowCount+1;
      Grid5.Cells[0,Grid5.RowCount-1] := Name;
      Grid5.Cells[1,Grid5.RowCount-1] := Zeut;
      Grid5.Cells[2,Grid5.RowCount-1] := Address.Street+' '+Address.Number+' '+ Address.City+' '+Address.Index+' '+Address.Country;
      Grid5.Cells[3,Grid5.RowCount-1] := '';
      Grid5.Cells[4,Grid5.RowCount-1] := '';
    end;

  If FResult is TDirector Then
    With FResult as TDirector do
    begin
      If Grid5.Cells[0,Grid5.RowCount-1] <> '' Then
        Grid5.RowCount := Grid5.RowCount+1;
      Grid5.Cells[0,Grid5.RowCount-1] := Name;
      Grid5.Cells[1,Grid5.RowCount-1] := Zeut;
      Grid5.Cells[2,Grid5.RowCount-1] := Address.Street+' '+Address.Number+' '+ Address.City+' '+Address.Index+' '+Address.Country;
      Grid5.Cells[3,Grid5.RowCount-1] := Address.Phone;
      Grid5.Cells[4,Grid5.RowCount-1] := '';
    end;

  TempCompany.Free;
  Loading:= False;
  DataChanged:= True;
  Grid5.SetFocus;
end;

procedure TfmCompanyInfoTemplate.EvPrintData;
var
  Company: TCompany;
begin
  Company:= EvCollectData;
  Application.CreateForm(TfmQRCompanyInfo, fmQRCompanyInfo);
  try
    fmQRCompanyInfo.PrintExecute(Company.Name, Company);
  finally
    fmQRCompanyInfo.Free;
    Company.Free;
  end;
end;

procedure TfmCompanyInfoTemplate.ZeutFieldExit(Sender: TObject);
begin
  inherited;
{  if StrToIntDef(TEdit(Sender).Text,1) = 0 then
  begin
    Application.MessageBox(PChar('���� ���� ���� ���� ����� 0.'+#10#13+'�� �����.           '),
                           PChar('����'),MB_OK);
    TEdit(Sender).SetFocus;
  end;}
end;

procedure TfmCompanyInfoTemplate.grid5SetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: String);
begin
  inherited;
  if (Acol = 1 ) then
  begin
    ManagerID:=Value;
    CurrManager:=ARow;
  end;
end;

//================Tsahi 9.11.05 to set focus to correct Director after an error
procedure TfmCompanyInfoTemplate.GoToCurrRecord;
begin
  Case PageControl.ActivePage.PageIndex of
   2:
   begin//stockholders
     e31.ItemIndex :=CurrRow-1;
     e31Change(e31);
   end;
   3:
   begin//Directors
     e41.ItemIndex :=CurrRow-1;
     e41Change(e41);
   end;
  else
  end;
end;
//================================// Tsahi 10/11/05 fixes problem with inappropriate message when entering manager id
procedure TfmCompanyInfoTemplate.CheckManagerID;
begin
  if (StrToIntDef(ManagerID,1) = 0) then
  begin
    Application.MessageBox(PChar('���� ���� ���� ���� ����� 0.'+#10#13+'�� �����.           '),
                           PChar('����'),MB_OK);
    Grid5.cells[1,CurrManager] := '';
  end;
  ManagerID:='';
  CurrManager:=0;
end;
//================================
procedure TfmCompanyInfoTemplate.grid5Exit(Sender: TObject);
begin
  inherited;
  CheckManagerID
end;
//==>21/05/06 Tsahi: changed name exditboxes OnChange events. Added ChangeNameFull
//                   to each, to indicate if the Zeut field needs to be chacked.
//                   NameFull is added to the hint if the name edit box is not empty.
//================================
procedure TfmCompanyInfoTemplate.e51Change(Sender: TObject);
begin
  inherited;
  ChangeNameFull(TCustomEdit(e51),TCustomEdit(e52));
  NonCriticalChange(Sender);
end;
//================================
procedure TfmCompanyInfoTemplate.e45Change(Sender: TObject);
begin
  inherited;
  ChangeNameFull(TCustomEdit(e45),TCustomEdit(e46));
  CriticalEditChange(Sender);
end;
//================================
procedure TfmCompanyInfoTemplate.e412Change(Sender: TObject);
begin
  inherited;
  ChangeNameFull(TCustomEdit(e412),TCustomEdit(e413));
  NonCriticalChange(Sender);
end;
//================================
procedure TfmCompanyInfoTemplate.e32Change(Sender: TObject);
begin
  inherited;
  ChangeNameFull(TCustomEdit(e32),TCustomEdit(e33));
  CriticalEditChange(Sender);
end;
//================================
procedure TfmCompanyInfoTemplate.e11Change(Sender: TObject);
begin
  inherited;
  ChangeNameFull(TCustomEdit(e11),TCustomEdit(e12));
  CriticalEditChange(Sender);

end;
//================================
//<==end of change 21/05/06 Tsahi

procedure TfmCompanyInfoTemplate.Grid2KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  //if (Key = VK_RETURN) then
  if (Key = 13) then
  begin
    {if (Grid2.PCol = Grid2.ColCount-1) then
      Grid2.PCol := 0;
    if (Grid2.Col = Grid2.ColCount-1) then
      Grid2.Col := 0;}
  end;
end;

procedure TfmCompanyInfoTemplate.Grid2LineReset(Sender: TObject; Row: Integer);
begin
  inherited;
  Grid2.Col := 0;
end;


/// TfmCompanyInfoTemplate.e75aChange
/// propose: if edit edit control changed - set check appropriate radio button; call default change method;
/// changes for tasks: 17674
procedure TfmCompanyInfoTemplate.e75aChange(Sender: TObject);
begin
  inherited;
  e75.Checked := true;
  NonCriticalChange(Sender);
end;

end.

