unit EditCompanyInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CompanyInfoTemplate, HebForm, ExtCtrls, StdCtrls, Mask, HGrids, SdGrid,
  HComboBx, HEdit, ComCtrls, Buttons, HLabel, DataObjects;

type
  TfmEditCompanyInfo = class(TfmCompanyInfoTemplate)
    btBrowse: TButton;
    HebLabel1: THebLabel;
    Shape82: TShape;
    procedure FormCreate(Sender: TObject);
    procedure btBrowseClick(Sender: TObject);
  private
    { Private declarations }
  protected
    function EvSaveData(Draft: Boolean): boolean; override;
    procedure EvFillData(Company: TCompany); override;
  public
    { Public declarations }
  end;

var
  fmEditCompanyInfo: TfmEditCompanyInfo;

implementation

uses dialog, Companies, dialog2, DataM;

{$R *.DFM}


function TfmEditCompanyInfo.EvSaveData(Draft: Boolean): Boolean;
var
  TempCompany: TCompany;
//  OldRecNum: Integer;
//  FilesList: TFilesList;
begin
  Result:= False;
  try
    If Draft Then
      Exit;
  //  OldRecNum:= Company.RecNum;
    Company.FileDelete(nil);
    Company.Free;
    TempCompany:= EvCollectData;
    TempCompany.SaveData(nil, True, False, -1);
    Company:= TCompany.Create(TempCompany.RecNum, TempCompany.CompanyInfo.RegistrationDate+1, False);
    FileName:= TempCompany.Name;
    SetFileIsDraft(False, -1);
    TempCompany.Free;
  {  FilesList:= TFilesList.Create(OldRecNum, faAll, True);
    FilesList.EvCompanyNumberChange(OldRecNum, Company.RecNum);
    FilesList.Free;}
    fmDialog2.Show;
    Result:= True;
  finally
  end;
end;

procedure TfmEditCompanyInfo.FormCreate(Sender: TObject);
begin
  inherited;
  if Tag<0 then
    Exit;
  btBrowse.Click;
  If Company=nil then
    Close;
end;

procedure TfmEditCompanyInfo.btBrowseClick(Sender: TObject);
var
  TempCompany: TCompany;
  History: THistory;
begin
  inherited;
  If DATAChanged Then
    begin
      fmDialog.memo.lines.Clear;
      fmDialog.memo.lines.add('��� ��');
      fmDialog.memo.lines.add('����� ��� ����� ���� ������ ������.');
      fmDialog.memo.lines.add('��� ����� �� ������� ?');
      fmDialog.ShowModal;
      if fmDialog.DialogResult = drYES then
        begin
          If CheckErrors(False,True, False) Then
            Exit
          Else
            b1.Click;
        end;
      If fmDialog.DialogResult = drCancel Then
        Exit;
    end;
  TempCompany:= fmCompanies.Execute(Caption, afActive);
  If TempCompany <> Nil Then
    begin
      History:= THistory.Create;
      try
        If (Not ReadOnly) and (Not History.IsActionLast(TempCompany.RecNum, faNumber, TempCompany.CompanyInfo.RegistrationDate)) then
          begin
            MessageBox(Handle, '��� ��'+#13
                              +'���� ����� ����� ����� ����� ������ ������ ����� ��'+#13
                              +'(���� - ��"� ����, ����� ����� ������).'+#13
                              +'���� ����� �� ����� ����� �� ����, ���� ��, �� �������'+#13
                              +'������� ������ - ������� ����� "������ | ����� ������".',
                                  '��� ����', MB_RIGHT or MB_RTLREADING or MB_OK or MB_ICONHAND);
            TempCompany.Free;
            TempCompany:= Nil;
          end;
      finally
        History.Free;
      end;
      If Company<> nil Then
        Company.Free;
      Company:= TempCompany;
      If Company<>Nil then
        begin
          EvFillData(Company);
          FileName:= Company.Name;
          SetFileIsDraft(False, -1);
        end;
    end;
end;
procedure TfmEditCompanyInfo.EvFillData(Company: TCompany);
begin
  inherited;
end;
end.
