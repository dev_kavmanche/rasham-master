unit DecHon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  HonTemplate, HebForm, ExtCtrls, HGrids, SdGrid, ComCtrls, Buttons,
  StdCtrls, Mask, HLabel, BlankTemplate, DataObjects, Util;

type
  TfmDecHon = class(TfmHonTemplate)
    procedure FormCreate(Sender: TObject);
    procedure GridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    procedure EvDataLoaded; override; // Moshe 06/08/2018
  public
    { Public declarations }
  end;

var
  fmDecHon: TfmDecHon;

implementation

uses
  HDialogs;

{$R *.DFM}

procedure TfmDecHon.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
  i: integer;
begin
  inherited;
  If Extended Then
    For I:= 0 to Grid.RowCount-1 do
      With Company.HonList.FindHonItem(Grid.Cells[0,I], StrToFloat(Grid.Cells[2,I])) do
        If (Rashum-Mokza) < StrToFloat(Grid.Cells[1,I]) then
        begin
          If Not Silent Then
          begin
            Grid.Row:= I;
            Grid.Col:= 1;
          end;
          ErrorAt:= Grid;
          Raise Exception.Create('��� ������ ����� �� ���� ����� ��� ���� ������ ������');
        end;
end;

procedure TfmDecHon.FormCreate(Sender: TObject);
begin
  FileAction := faDecrease;
  inherited;
  IncAction := False;
end;

procedure TfmDecHon.EvDataLoaded;
var
  i: integer;
begin
  inherited;

  // Moshe 06/08/2018 - Show Hon
  // Moshe 17/09/2018 - Show Hon list

  Company.HonList.Filter := afActive;
  nGrid.RowCount := Company.HonList.Count;
  nGrid.ColWidths[2] := 0;
  if (Grid.Cells[0, 0] = '') then
    Grid.ResetLine(0);

  for i := 0 to Company.HonList.Count-1 do
  begin
    nGrid.Cells[0,i] := IntToStr(i+1);
    nGrid.Cells[1,i] := THonItem(Company.HonList.Items[I]).Name;
    nGrid.Cells[2,i] := FloatToStr(THonItem(Company.HonList.Items[I]).Value);
  end;
end;

procedure TfmDecHon.GridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
begin
  inherited;

  if (Pos(IntToStr(Col), Grid.ProtectedCells) <> 0) then
  begin
    Grid.Options := Grid.Options - [goEditing];
    CanSelect := False;
  end
  else
  begin
    Grid.Options := Grid.Options + [goEditing];
  end;
end;

end.

