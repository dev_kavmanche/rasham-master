{***************************************************************
 *
 * Unit Name: About
 * Purpose  : show "About..." dialog
 * Author   :N/A

History
date        version  by   task     description
01/08/2019  1.7.7    sts           fixed version data
23/12/2020  1.8.4    sts  22057    refactoring    

}


unit About;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, HebForm, HLabel, HMemo;

type
  TAboutBox = class(TForm)
    Panel1: TPanel;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    Comments: TLabel;
    OKButton: TButton;
    Bevel1: TBevel;
    Label1: TLabel;
    lVersion: TLabel;
    HebLabel2: THebLabel;
    HebLabel3: THebLabel;
    HebLabel4: THebLabel;
    HebLabel5: THebLabel;
    lSize: TLabel;
    lDate: TLabel;
    lExePath: TLabel;
    lDbPath: TLabel;
    HebLabel6: THebLabel;
    Button1: TButton;
    Panel2: TPanel;
    btRegister: TButton;
    lCustomNum: TLabel;
    HebLabel7: THebLabel;
    lLicense: TLabel;
    HebLabel8: THebLabel;
    HebMemo1: THebMemo;
    HebMemo2: THebMemo;
    procedure btRegisterClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox: TAboutBox;

implementation

uses Register, Main, Help, DataM, Util, HPSConsts;

{$R *.DFM}

procedure TAboutBox.btRegisterClick(Sender: TObject);
begin
  Application.CreateForm(TfmRegister, fmRegister);
  fmRegister.ShowModal;
  fmRegister.Free;
  Close;
end;

procedure TAboutBox.FormShow(Sender: TObject);
var
  SearchRec : TSearchRec;
begin
  Panel2.Visible     := Not fmMain.Registered;
  HebMemo2.Visible   := fmMain.DemoMode;
  HebMemo1.Visible   := Not HebMemo2.Visible;
  lCustomNum.Caption := FloatToStrF(abs(fmMain.GetVolumeSerNum), ffNumber, 30, 0);
  lLicense.Caption   := FloatToStrF(fmMain.RegNum, ffNumber, 30, 0);
  lVersion.Caption   := fmMain.ExeVersion;
  FindFirst(ParamStr(0),faAnyFile,SearchRec);
  lSize.Caption      := FloatToStrF(SearchRec.Size, ffNumber, 10, 0) + ' Byte';
  lDate.Caption      := DateToLongStr(FileDateToDateTime(FileAge(ParamStr(0))));
  lExePath.Caption   := ExtractFilePath(ParamStr(0));
  lDbPath.Caption    := Data.taName.Database.Directory;
end;

procedure TAboutBox.SpeedButton3Click(Sender: TObject);
begin
  ShowHelp(HelpContext);
end;

procedure TAboutBox.FormCreate(Sender: TObject);
begin
  HebLabel6.Caption := HPSAddress + ', ' + HPSCity + '. ��: ' + HPSPhone + ', ���: ' +
                       HPSFax + #13 + ' E-Mail: ' + HPSEMail;
end;

end.
 
