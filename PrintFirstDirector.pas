{
History:
DATE        VERSION   BY   TASK      DESCRIPTION
21/06/2021  1.8.6     sts  23127     changes in report
}

unit PrintFirstDirector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, ExtCtrls, Qrctrls, DataObjects;

type
  TfmQRFirstDirector = class(TForm)
    qrFirstDirector1: TQuickRep;
    QRBand1: TQRBand;
    qrFirstDirector2: TQuickRep;
    QRLabel1: TQRLabel;
    QRLabel5: TQRLabel;
    QRBand2: TQRBand;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    l1: TQRLabel;
    QRLabel10: TQRLabel;
    edtDirLast: TQRLabel;
    QRLabel11: TQRLabel;
    l4: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel15: TQRLabel;
    l5: TQRLabel;
    QRLabel17: TQRLabel;
    l6: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel19: TQRLabel;
    l7: TQRLabel;
    QRLabel21: TQRLabel;
    l8: TQRLabel;
    QRLabel22: TQRLabel;
    l9: TQRLabel;
    QRLabel24: TQRLabel;
    l11: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    edtTaagidFirst: TQRLabel;
    l13: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel78: TQRLabel;
    l20: TQRLabel;
    l21: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel80: TQRLabel;
    l22: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel87: TQRLabel;
    l23: TQRLabel;
    l24: TQRLabel;
    QRLabel79: TQRLabel;
    l25: TQRLabel;
    l26: TQRLabel;
    l27: TQRLabel;
    l28: TQRLabel;
    QRBand4: TQRBand;
    l10: TQRLabel;
    l10a: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel51: TQRLabel;
    QRImage1: TQRImage;
    QRImage2: TQRImage;
    QRLabel3: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel54: TQRLabel;
    edtDirFirst: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel43: TQRLabel;
    l14: TQRLabel;
    QRLabel38: TQRLabel;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRLabel47: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel63: TQRLabel;
    QRShape18: TQRShape;
    QRLabel64: TQRLabel;
    QRShape19: TQRShape;
    QRLabel65: TQRLabel;
    edtTaagidLast: TQRLabel;
    QRShape20: TQRShape;
    QRBand3: TQRBand;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRShape140: TQRShape;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRImage3: TQRImage;
    QRImage4: TQRImage;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel66: TQRLabel;
    QRBand5: TQRBand;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRShape21: TQRShape;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel76: TQRLabel;
    QRShape26: TQRShape;
  private
    { Private declarations }

  public
    { Public declarations }
    procedure PrintEmptyProc(DoPrint: Boolean);
    procedure PrintEmpty(const FileName: String);
    procedure PrintExecute(Company: TCompany; Director: TDirector; DecisionDate: TDateTime; DoPrint: Boolean);
  end;

var
  fmQRFirstDirector: TfmQRFirstDirector;

implementation

{$R *.DFM}

uses Util, PrinterSetup;

procedure TfmQRFirstDirector.PrintEmptyProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(qrFirstDirector1);
  fmPrinterSetup.FormatQR(qrFirstDirector2);
  If DoPrint then
    begin
      qrFirstDirector1.Print;
      qrFirstDirector2.Print;
    end
  Else
    begin
      qrFirstDirector1.Preview;
      Application.ProcessMessages;
      qrFirstDirector2.Preview;
      Application.ProcessMessages;
    end;
end;

procedure TfmQRFirstDirector.PrintEmpty(const FileName: String);
begin
  fmPrinterSetup.Execute(FileName, PrintEmptyProc);
end;

procedure TfmQRFirstDirector.PrintExecute(Company: TCompany; Director: TDirector; DecisionDate: TDateTime; DoPrint: Boolean);
var
  FirstName, LastName: string;
begin
  l1.Caption := Company.Name;
  if (Director.Taagid = nil) then
  begin
    SplitName(Director.Name, FirstName, LastName);
    edtDirLast.Caption := LastName;
    edtDirFirst.Caption := FirstName;
    l4.Caption := Director.Zeut;
    l21.Caption := Director.Name;
    l22.Caption := Director.Zeut;
  end
  else
  begin
    SplitName(Director.Taagid.Name, FirstName, LastName);
    edtTaagidLast.Caption := LastName;
    edtTaagidFirst.Caption := FirstName;
    l5.Caption := Director.Name;
    l6.Caption := Director.Zeut;
    l13.Caption := Director.Taagid.Zeut;
    l21.Caption := Director.Taagid.Name;
    l22.Caption := Director.Taagid.Zeut;
  end;
  l7.Caption := Director.Address.Country;
  l8.Caption := Director.Address.City;
  l9.Caption := Director.Address.Street;
  l10a.Caption := Director.Address.Number;
  l10.Caption := Director.Address.Index;
  l11.Caption := Director.Address.Phone;
  l14.Caption := DateToLongStr(DecisionDate);
  l20.Caption := Company.CompanyInfo.LawName;

  l25.Caption:= Company.CompanyInfo.LawName;
  l26.Caption:= Company.CompanyInfo.LawAddress;
  if Company.CompanyInfo.LawZeut<> '0' then
    l27.Caption:= Company.CompanyInfo.LawZeut;
  if Company.CompanyInfo.LawLicence<> '0' then
  l28.Caption:= Company.CompanyInfo.LawLicence;

  fmPrinterSetup.FormatQR(qrFirstDirector1);
  fmPrinterSetup.FormatQR(qrFirstDirector2);
  If DoPrint then
    begin
      qrFirstDirector1.Print;
      qrFirstDirector2.Print;
    end
  Else
    begin
      qrFirstDirector1.Preview;
      Application.ProcessMessages;
      qrFirstDirector2.Preview;
      Application.ProcessMessages;
    end;
end;

end.
