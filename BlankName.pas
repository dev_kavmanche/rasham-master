{
MODIFICATION HISTORY
DATE         BY    TASK      VERSION     DESCRIPTION
04/12/2016   Moshe 12868
13/10/2021   sts   23822     1.8.4       todo: wrong company record number when company draft list is opened  
}
unit BlankName;

interface

uses
  Windows, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HEdit, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls,
  Mask, HLabel, DataObjects, DBTables, HGroupBx, HRadGrup;

type
  TfmBlankName = class(TfmBlankTemplate)
    Shape5: TShape;
    Shape6: TShape;
    l1: THebLabel;
    edtCompanyName1: THebEdit;
    HebLabel48: THebLabel;
    Shape135: TShape;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtCompanyName2: THebEdit;
    edtCompanyName3: THebEdit;
    grbRegulationItems: THebRadioGroup;
    Shape78: TShape;
    Shape75: TShape;
    Shape69: TShape;
    Shape80: TShape;
    HebLabel40: THebLabel;
    HebLabel35: THebLabel;
    Shape68: TShape;
    Shape79: TShape;
    HebLabel38: THebLabel;
    HebLabel39: THebLabel;
    Shape77: TShape;
    Shape76: TShape;
    edtSigName: THebEdit;
    edtMagishName: THebEdit;
    edtMagishPhone: TEdit;
    edtSigZehut: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure b1Click(Sender: TObject);
    procedure btPrintClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FFilesList: TFilesList;
    FFileItem: TFileItem;

    procedure EvPrintData; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    function EvLoadData: Boolean; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvDataLoaded; override;
    //--> Change Company Name functionality improvement == Added on 06/11/2001 by Elijah Shlieserman
    function GetChangeFileName( FAction : TFileAction; CurFileName : string ) : string;
    function CheckValues: boolean;
    procedure FillData;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
    class procedure EvUndoFile(FileItem: TFileItem);
  end;

var
  fmBlankName: TfmBlankName;

implementation

uses
  sysUtils, dialog2, LoadDialog, PrintName, DataM, utils2;

{$R *.DFM}

procedure TfmBlankName.FormCreate(Sender: TObject);
begin
  FileAction:= faCompanyName;
  ShowStyle:= afActive;
  inherited;
end;

procedure TfmBlankName.FormDestroy(Sender: TObject);
begin
  FFileItem.Free;
  FFilesList.Free;
  inherited;
end;

function TfmBlankName.EvLoadData: Boolean;
begin
  Result := False;
  FFileItem := fmLoadDialog.ExecuteDraft(Self, FFilesList, Company.RecNum, faCompanyName);               //sts23822
  If FFileItem = Nil Then
    Exit;
  DecisionDate := (FFileItem.FileInfo.DecisionDate);
  NotifyDate := (FFileItem.FileInfo.NotifyDate);
  if Assigned(Company) then
    Company.Free;
  Company := TCompany.LoadFromFile(nil, FFileItem);
  FillData;
  Result := True;
end; // TfmBlankName.EvLoadData

procedure TfmBlankName.FillData;
begin
  edtCompanyName1.Text := Company.Name;
  edtCompanyName2.Text := Company.CompanyInfo.CompanyName2;
  edtCompanyName3.Text := Company.CompanyInfo.CompanyName3;
  edtSigName.Text      := Company.CompanyInfo.SigName;
  edtSigZehut.Text     := Company.CompanyInfo.SigZeut;
  grbRegulationItems.ItemIndex := Company.CompanyInfo.RegulationItemsIndex;
  edtMagishName.Text  := Company.Magish.Name;
  edtMagishPhone.Text := Company.Magish.Phone;
end;

procedure TfmBlankName.DeleteFile(FileItem: TFileItem);
begin
  If Not FileItem.FileInfo.Draft Then
    Raise Exception.Create('This file is not Draft at TfmBlankName.DeleteFile');
  If (FileItem.FileInfo.Action <> faCompanyName) Then
    raise Exception.Create('Invalid file type at TfmBlankName.DeleteFile');
  FileItem.DeleteFile;
end;

class procedure TfmBlankName.EvUndoFile(FileItem: TFileItem);
var
  Company: TCompany;
begin
  Company := TCompany.Create(FileItem.FileInfo.CompanyNum, FileItem.FileInfo.DecisionDate + 1, False);
  try
    Company.SetNumber(Company.Zeut, FileItem.FileInfo.FileName);
  finally
    Company.Free;
  end;
  FileItem.DeleteFile;
end;

procedure TfmBlankName.EvFocusNext(Sender: TObject);
begin
  If Sender = e13 then
    edtCompanyName1.SetFocus;
end;

procedure TfmBlankName.EvDataLoaded;
begin
  Inherited;
  If Not DoNotClear Then
    FillData;
end;

//--> Change Company Name functionality improvement == Added on 06/11/2001 by Elijah Shlieserman
//==============================================================================
function TfmBlankName.GetChangeFileName( FAction : TFileAction; CurFileName : string ) : string;
begin
  with TTable.Create(Self) do
  begin
    DatabaseName := Data.taFiles.DatabaseName;
    TableName := Data.taFiles.TableName;
    IndexName := Data.taFiles.IndexName;
    Open;
    SetRange([0, Company.RecNum, Char(Byte(FAction) + Byte('0')), StrToDateTime('01/01/1900' )],
             [0, Company.RecNum, Char(Byte(FAction) + Byte('0')), DecisionDate] );
    Last;
    if Locate( 'RecName', CurFileName, [] )  then
    begin
      Prior;
      if FieldByName( 'RecName' ).AsString <> '' then
        Result := FieldByName( 'RecName' ).AsString
      else
        Result := Company.Name;
    end
    else
      Result := 'error!';
    Close;
    Free;
  end;
end;
//<--

function TfmBlankName.EvSaveData(Draft: Boolean): Boolean;
var
  TempFileList: TFilesList;
  FileInfo: TFileInfo;
  FileItem: TFileItem;
  Magish: TMagish;
  OldFile: TFileItem;
  i: Integer;
  CompInfoRecNum: integer;
  Changes: TChanges;
  CompanyInfo: TCompanyInfo;
  MagishRecNum: integer;
  saved: boolean;
begin
  // Moshe 04/12/2016 Task 12868
  Result := False;
  saved := False;
  FileInfo := nil;
  FileItem := nil;
  try
    try
      if (FFilesList = nil) then
        FFilesList := TFilesList.Create(Company.RecNum, faCompanyName, Draft);
      FileInfo := TFileInfo.Create;
      FileInfo.Action := faCompanyName;
      FileInfo.Draft := Draft;
      If Draft Then
        FileInfo.FileName := edtCompanyName1.Text
      Else
        FileInfo.FileName := Company.Name;
      //--> Change Company Name functionality improvement == Added on 06/11/2001 by Elijah Shlieserman
      //FileInfo.PreviousName := lName.Caption;
      //<--
      FileInfo.DecisionDate := DecisionDate;
      FileInfo.NotifyDate := Now;
      FileInfo.CompanyNum := Company.RecNum;
      if (not FFilesList.FileExists(FileInfo.FileName, i)) then
        FFilesList.Add(FileInfo);
      FileItem := TFileItem.Create(FFilesList, FileInfo);
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;

    Changes := TChanges.Create(Company);
    CompanyInfo := nil;
    try
      CompInfoRecNum := Changes.GetLastChange(ocCompany, Company.RecNum, acSignName);
    if (CompInfoRecNum > 0) then
      CompanyInfo := TCompanyInfo.Create(CompInfoRecNum, Company)
    else
      CompanyInfo := TCompanyInfo.CreateNew(DecisionDate);

    CompanyInfo.Option1                   := Company.CompanyInfo.Option1;
    CompanyInfo.Option3                   := Company.CompanyInfo.Option3;
    CompanyInfo.OtherInfo                 := Company.CompanyInfo.OtherInfo;
    CompanyInfo.SigTafkid                 := Company.CompanyInfo.SigTafkid;
    CompanyInfo.LawName                   := Company.CompanyInfo.LawName;
    CompanyInfo.LawAddress                := Company.CompanyInfo.LawAddress;
    CompanyInfo.LawZeut                   := Company.CompanyInfo.LawZeut;
    CompanyInfo.LawLicence                := Company.CompanyInfo.LawLicence;
    CompanyInfo.Takanon2_1                := Company.CompanyInfo.Takanon2_1;
    CompanyInfo.Takanon2_2                := Company.CompanyInfo.Takanon2_2;
    CompanyInfo.Takanon2_3                := Company.CompanyInfo.Takanon2_3;
    CompanyInfo.Takanon2_4                := Company.CompanyInfo.Takanon2_4;
    CompanyInfo.Takanon4                  := Company.CompanyInfo.Takanon4;
    CompanyInfo.Takanon5                  := Company.CompanyInfo.Takanon5;
    CompanyInfo.Takanon7                  := Company.CompanyInfo.Takanon7;
    CompanyInfo.EnglishName               := Company.CompanyInfo.EnglishName;
    CompanyInfo.RegistrationDate          := Company.CompanyInfo.RegistrationDate;
    CompanyInfo.Mail                      := Company.CompanyInfo.Mail;
    CompanyInfo.SharesRestrictIndex       := Company.CompanyInfo.SharesRestrictIndex;
    CompanyInfo.DontOfferIndex            := Company.CompanyInfo.DontOfferIndex;
    CompanyInfo.ShareHoldersNoIndex       := Company.CompanyInfo.ShareHoldersNoIndex;
    CompanyInfo.SharesRestrictChapters    := Company.CompanyInfo.SharesRestrictChapters;
    CompanyInfo.DontOfferChapters         := Company.CompanyInfo.DontOfferChapters;
    CompanyInfo.ShareHoldersNoChapters    := Company.CompanyInfo.ShareHoldersNoChapters;
    CompanyInfo.LimitedSignatoryIndex     := Company.CompanyInfo.LimitedSignatoryIndex;
    CompanyInfo.LegalSectionsChapters     := Company.CompanyInfo.LegalSectionsChapters;
    CompanyInfo.SignatorySectionsChapters := Company.CompanyInfo.SignatorySectionsChapters;
    CompanyInfo.ConfirmedCopies           := Company.CompanyInfo.ConfirmedCopies;
    CompanyInfo.MoreCopies                := Company.CompanyInfo.MoreCopies;
    CompanyInfo.CompanyName2              := edtCompanyName2.Text;
    CompanyInfo.CompanyName3              := edtCompanyName3.Text;
    CompanyInfo.SigName                   := edtSigName.Text;
    CompanyInfo.SigZeut                   := edtSigZehut.Text;
    CompanyInfo.RegulationItemsIndex      := grbRegulationItems.ItemIndex;
    if CompInfoRecNum > 0 then
      saved := CompanyInfo.UpdateData(Company, CompInfoRecNum)
    else
    begin
      if (Assigned(FileItem)) then
        CompanyInfo.SaveData(Company, False, FileItem.FileInfo.RecNum)
      else
        saved := False;
    end;
    finally
      if (Assigned(Changes)) then
        Changes.Free;
      if (Assigned(CompanyInfo)) then
        CompanyInfo.Free;
    end;
    MagishRecNum := FileItem.GetLastChange(FileItem.FileInfo.RecNum, ocCompany, Company.RecNum, acNewMagish);
    Magish := TMagish.CreateNew(edtMagishName.Text, Company.Magish.Zeut, Company.Magish.Street, Company.Magish.Number,
	                              Company.Magish.Index, Company.Magish.City,Company.Magish.Country, edtMagishPhone.Text,
                                Company.Magish.Mail, Company.Magish.Fax, Company.Magish.Taagid);
    if (MagishRecNum > 0) then
    begin
      if not Magish.UpdateData(MagishRecNum) then
	    saved := False;
    end
    else
      Magish.SaveData(Company, FileItem);
    Magish.Free;

    If Not Draft Then
      Company.SetNumber(Company.Zeut, edtCompanyName1.Text);
    If (FileName <> '') and FileIsDraft and (not Draft) Then  // If Saving a draft file as non-draft
    begin
      if (Assigned(FileInfo)) then
      begin
        TempFileList := TFilesList.Create(FileInfo.CompanyNum, FileInfo.Action, True);
        try
          If TempFileList.FileExists(FileName, I) Then
          begin
            OldFile := TempFileList.Items[I];
            try
              DeleteFile(OldFile);    // Delete a draft file
            finally
              if (Assigned(OldFile)) then 
			          OldFile.Free;
            end;
          end;
        finally
          if (Assigned(TempFileList)) then 
		    TempFileList.Free;
        end;
      end;
    end;

    FileIsDraft := Draft;
    FileName := edtCompanyName1.Text;
    if saved then 
   	  fmDialog2.Show;
    Result := True;
  except 
    on E: Exception do
      ShowMessage(E.Message);
  end;
end; // TfmBlankName.EvSaveData

procedure TfmBlankName.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
  Companies: TCompanies;
  I: Integer;
begin
  ErrorAt := nil;
  Inherited EvCheckPage(Page, Silent, Extended, Draft);
  if ErrorAt <> nil then 
    exit;

  If Not Extended Then
    Exit;
  if (Company.Name = edtCompanyName1.Text) then
  begin
    if not Silent Then
      If edtCompanyName1.Color = clWindow Then
        edtCompanyName1.Color := clBtnFace
      else
        edtCompanyName1.Color := clWindow;
    ErrorAt := edtCompanyName1;
    raise Exception.Create('��� ���� ��� ��� ���');
  end;

  Companies := TCompanies.Create(Now, False, False);
  Companies.Filter := afActive;
  try
    For I := 0 To Companies.Count - 1 Do
      If (Companies.Names[I] = edtCompanyName1.Text) and
        (Not (Assigned(Company) and (Companies.RecNums[I] = Company.RecNum))) Then
      begin
        If Not Silent Then
          If edtCompanyName1.Color = clWindow Then
            edtCompanyName1.Color := clBtnFace
          Else
            edtCompanyName1.Color := clWindow;

        ErrorAt := edtCompanyName1;
        raise Exception.Create('�� ����� ��� ���� ����� ����');
      end;
  finally
    Companies.Free;
  end;
end;

procedure TfmBlankName.EvPrintData;
var
  Yes, No: string;
begin
  Application.CreateForm(TfmQRName, fmQRName);
  //--> Change Company Name functionality improvement == Added on 06/11/2001 by Elijah Shlieserman
  //REM by ES>>  fmQRName.PrintExecute(Company, e12.Text);
  Yes := '';
  No := '';
  if (grbRegulationItems.ItemIndex = 0) then
    No := 'X';
  if (grbRegulationItems.ItemIndex = 1) then
    Yes := 'X';
  fmQRName.PrintExecute(
    lName.Caption,
    lNumber.Caption,
    DateToStr(DecisionDate),
    edtCompanyName1.Text,
    edtCompanyName2.Text,
    edtCompanyName3.Text,
    No,
    Yes,
    edtSigName.Text,
    edtSigZehut.Text,
    edtMagishName.Text,
    edtMagishPhone.Text
    );
  fmQRName.Free;
end;

procedure TfmBlankName.b1Click(Sender: TObject);
var
  OldCompanyName: string;
  Draft: boolean;
begin
  // Moshe 19/12/16
  Draft := True;
  if Sender <> nil then
    if Sender is TComponent then
      if uppercase((Sender as TComponent).Name) = 'B1' then
        Draft := False;
  if not Draft then
    if (not CheckValues) then
      Exit;

  //--> Old name display during the change name session == Added on 01/11/2001 by Elijah
  OldCompanyName := lName.Caption;
  inherited;
  lName.Caption := OldCompanyName;
end;

procedure TfmBlankName.btPrintClick(Sender: TObject);
begin
  // Moshe 19/12/16
  if (not CheckValues) then
    Exit;
  inherited;
end;

function TfmBlankName.CheckValues: boolean;
const
  C_MUST = '��� ��� ����';
var
  ctl: TWinControl;
  msg: string;
begin
  ctl := Nil;
  msg := '';
  Result := False;
  if (edtCompanyName1.Text = '') then
  begin
    ctl := edtCompanyName1;
    msg := l1.Caption + ' (1)';
  end
  else if (edtCompanyName2.Text = '') then
  begin
    ctl := edtCompanyName2;
    msg := l1.Caption + ' (2)';
  end
  else if (edtCompanyName3.Text = '') then
  begin
    ctl := edtCompanyName3;
    msg := l1.Caption + ' (3)';
  end
  else if (grbRegulationItems.ItemIndex < 0) then
  begin
    ctl := grbRegulationItems;
    msg := grbRegulationItems.Caption;
  end
  else if (edtSigName.Text = '') then
  begin
    ctl := edtSigName;
    msg := HebLabel40.Caption;
  end
  else if IsEmptyNumber(edtSigZehut.Text) then
  begin
    ctl := edtSigZehut;
    msg := HebLabel39.Caption;
  end
  else if (edtMagishName.Text = '') then
  begin
    ctl := edtMagishName;
    msg := HebLabel35.Caption;
  end
  else if IsEmptyNumber(edtMagishPhone.Text) then
  begin
    ctl := edtMagishPhone;
    msg := HebLabel38.Caption;
  end;

  if msg <> '' then
    MessageDlg(msg + ' ' + C_MUST, mtError, [mbOk], 0);
  if ctl <> nil then
    ctl.SetFocus;
  if (msg = '') and (ctl = nil) then
    Result := True;
end;

end.
