unit PrintSetup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, HLabel,
  HRadio, HGroupBx, HRadGrup, HListBox, HChkBox;

type
  TfmPrintSetup = class(TfmTemplate)
    rgCaptions: THebRadioGroup;
    grPrintEmpty: THebGroupBox;
    lbBlankList: THebListBox;
    cbPrintDateInDoh: THebCheckBox;
    cbIDCheck: THebCheckBox;
    cbAutoLongPrint: THebCheckBox;
    procedure FormShow(Sender: TObject);
    procedure lbBlankListClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbPrintDateInDohClick(Sender: TObject);
  private
    { Private declarations }
  protected
    function EvSaveData(Draft: Boolean): Boolean; override;
    procedure EvPrintData; override;
  public
    { Public declarations }
  end;

var
  fmPrintSetup: TfmPrintSetup;

implementation

{$R *.DFM}

uses DataM, Dialog2, PrintAddress, PrintName, PrintFirstDirector,
  PrintFirstStockHolder, PrintHaavara, PrinterSetup, PrintDirectors,
  PrintIncHon, PrintDecHon, PrintDoh, PrintHakzaa, PrintRishum, Main,
  PrintRishumRegSubmit, PrintRishumQuickSubmit, printannr, prTakanon,
  PrintSignName, utils2, RishumNew, qrTakanonChange, PrintAnnualReport,
  QrTakanonChange2,
  PrintEmptyTakanon;

function TfmPrintSetup.EvSaveData(Draft: Boolean): Boolean;
begin
  fmMain.UserOptions.PrintFullCaptions := rgCaptions.ItemIndex;
  fmMain.UserOptions.PrintDatesInDoh := cbPrintDateInDoh.Checked;
  fmMain.UserOptions.IDCheck := cbIDCheck.Checked;
  fmMain.UserOptions.CheckLongNamesOnPrint := cbAutoLongPrint.Checked;

  fmMain.NeedCheckID:= cbIDCheck.Checked;
  fmPrinterSetup.FormCreate(Self);
  fmDialog2.Show;
  Result:= True;
end;

procedure TfmPrintSetup.EvPrintData;
var
  Filename: String;
begin
  if lbBlankList.ItemIndex = -1 then
    Exit;
  Filename:= lbBlankList.Items[lbBlankList.ItemIndex];
  case lbBlankList.ItemIndex of
    0: begin
////         Application.CreateForm(TfmQRRishum, fmQRRishum);
////         fmQRRishum.PrintEmpty(Filename);
////         fmQRRishum.Free;

         {Application.CreateForm(TQRPrintRishumRegSubmit, QRPrintRishumRegSubmit);
         QRPrintRishumRegSubmit.PrintEmpty( Filename);
         QRPrintRishumRegSubmit.Free;   }
         
         Application.CreateForm(TfrmQrRishumNewComp,frmQrRishumNewComp);
         frmQrRishumNewComp.PrintEmpty(Filename);
         frmQrRishumNewComp.Free;
       end;
    1: begin
        Application.CreateForm(TQRPrintRishumQuickSubmit, QRPrintRishumQuickSubmit);
        QRPrintRishumQuickSubmit.PrintEmpty(Filename);
        QRPrintRishumQuickSubmit.Free;
    end;
    2: begin
         Application.CreateForm(TfmQRHakzaa, fmQRHakzaa);
         fmQRHakzaa.PrintEmpty(Filename);
         fmQRHakzaa.Free;
       end;
    3: begin
         Application.CreateForm(TQrAnnualReport, QRAnnualReport);
         QRAnnualReport.PrintEmpty(Filename);
         QRAnnualReport.Free;
        { Application.CreateForm(TfrmPrintAnnualReport,frmPrintAnnualReport);
         frmPrintAnnualReport.Printempty(Filename);
         frmPrintAnnualReport.Free; }
        { Application.CreateForm(TfmQRDoh, fmQRDoh);
         fmQRDoh.PrintEmpty(Filename);
         fmQRDoh.Free; }
       end;
    4: begin
         Application.CreateForm(TfmQRDecHon, fmQRDecHon);
         fmQRDecHon.PrintEmpty(Filename);
         fmQRDecHon.Free;
       end;
    5: begin
         Application.CreateForm(TfmQRIncHon, fmQRIncHon);
         fmQRIncHon.PrintEmpty(Filename);
         fmQRIncHon.Free;
       end;
    6: begin
         Application.CreateForm(TfmQRDirectors, fmQRDirectors);
         fmQRDirectors.PrintEmpty(Filename);
         fmQRDirectors.Free;
       end;
    7: begin
         Application.CreateForm(TfmQRHaavara, fmQRHaavara);
         fmQRHaavara.PrintEmpty(Filename);
         fmQRHaavara.Free;
       end;
    8: begin
         Application.CreateForm(TfmQRAddress, fmQRAddress);
         fmQRAddress.PrintEmpty(Filename);
         fmQRAddress.Free;
       end;
    9: begin
         Application.CreateForm(TfmQRName, fmQRName);
         fmQRName.PrintEmpty(Filename);
         fmQRName.Free;
       end;
    10: begin
         Application.CreateForm(TfmQRFirstStockHolder, fmQRFirstStockHolder);
         fmQRFirstStockHolder.PrintEmpty(Filename, False);
         fmQRFirstStockHolder.Free;
       end;
   11: begin
         Application.CreateForm(TfmQRFirstStockHolder, fmQRFirstStockHolder);
         fmQRFirstStockHolder.PrintEmpty(Filename, True);
         fmQRFirstStockHolder.Free;
       end;
   12: begin
         Application.CreateForm(TfmQRFirstDirector, fmQRFirstDirector);
         fmQRFirstDirector.PrintEmpty(Filename);
         fmQRFirstDirector.Free;
       end;
   13: begin
         Application.CreateForm(TfmQrSignName,fmQrSignName);
         fmQrSignName.PrintEmpty(Filename);
         fmQrSignName.Free;
       end;
   14: begin
         Application.CreateForm(TfmQrTakanonChange2,fmQrTakanonChange2);
         fmQrTakanonChange2.PrintEmpty(Filename);
         fmQrTakanonChange2.Free;
       end;
   15: begin
         Application.CreateForm(TEmptyTakanonForm, EmptyTakanonForm);
         EmptyTakanonForm.PrintEmpty(Filename);
         EmptyTakanonForm.Free;
       end;
   else begin
           if not MyWinExec(Filename+'.doc')then
             if not MyWinExec(Filename+'.docx')then
              MessageDlg('�� ���� ������ ���� ��',mtError,[mbOk],0);
        end;
  end;
end;

procedure TfmPrintSetup.FormShow(Sender: TObject);
begin
  Loading := True;
  rgCaptions.ItemIndex := fmMain.UserOptions.PrintFullCaptions;
  cbPrintDateInDoh.Checked := fmMain.UserOptions.PrintDatesInDoh;
  cbIDCheck.Checked := fmMain.UserOptions.IDCheck;
  cbAutoLongPrint.Checked := fmMain.UserOptions.CheckLongNamesOnPrint;
  btPrint.Enabled := True;
  lbBlankList.ItemIndex := 0;
  Loading := False;
end;

procedure TfmPrintSetup.lbBlankListClick(Sender: TObject);
begin
  inherited;
  btPrint.Enabled := True;
end;

procedure TfmPrintSetup.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
    SpeedButton1Click(Sender);
  inherited;
end;

procedure TfmPrintSetup.cbPrintDateInDohClick(Sender: TObject);
begin
  inherited;
  if (not Loading) then
    DATAchanged := True;
  fmMain.UserOptions.PrintDatesInDoh := cbPrintDateInDoh.Checked;
end;

end.
