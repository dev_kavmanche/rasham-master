unit DataM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Bde, FileCtrl;
type
  TData = class(TDataModule)
    taTaagid: TTable;
    taStockNum: TTable;
    taStockHolders: TTable;
    taStockAddress: TTable;
    taSigName: TTable;
    taName: TTable;
    taManager: TTable;
    taHon: TTable;
    taDirectors: TTable;
    taDirAddress: TTable;
    taChanges: TTable;
    taAddress: TTable;
    taCity: TTable;
    taCountry: TTable;
    taStocks: TTable;
    taForms: TTable;
    taFiles: TTable;
    taMagish: TTable;
    taDoh: TTable;
    taMuhaz: TTable;
    taAutoInc: TTable;
    qGeneric: TQuery;
    qGeneric2: TQuery;
    dbRasham: TDatabase;
    taCityCITY: TStringField;
    taRishumSubmission: TTable;
    procedure AppendAutoInc(DataSet: TDataSet);
    procedure InitAutoIncrement(DataSet: TDataSet);
    procedure DataCreate(Sender: TObject);
  private
    { Private declarations }
    procedure AddBlobField(DataSet: TDataSet; const FieldName: DBINAME);
    procedure AddStringField(DataSet: TDataSet; const FieldName: DBINAME; const FieldSize: integer);
    procedure AddIntegerField(DataSet: TDataSet; const FieldName: DBINAME);
    procedure ChangeFieldSize(FieldName: string; FieldSize: integer; Table:TTable );
    function AddTableField(tbl:TTable;const fieldName,fieldType:string):boolean;
    procedure StartTables;
    procedure DeleteLockFiles;
    function ExecSQL2(const sqlCommand:string;var error:string):boolean;
  public
    { Public declarations }
    procedure UpdateTableFields;
    function SQLSelectWithDateRange(const sqlCommand,paramName1,paramName2:string;dateRangeStart,dateRangeEnd:TDateTime;var error:string):boolean;
    function SQLSelect2(const sqlCommand:string;var error:string):boolean;
    function SQLSelect(const sqlCommand:string;var msg:string;tbl:TTable;var TableWasOpen:boolean):boolean;
    function SQLClose:boolean;
    function SQLFieldByName(const FieldName:string):TField;
    function SQLBOF:boolean;
    function SQLEOF:boolean;
    function SQLPrev:boolean;
    function SQLNext:boolean;
    function SQLFirst:boolean;
    function SQLLast:boolean;
    function SQLFields(FieldNo:integer):TField;
    function SQLQueryIsEmpty:boolean;
    function SQLRowsAffected:integer;
    function ExecSQL(const sqlCommand:string;tbl:TTable):boolean;
    procedure TablesActivate(Value: Boolean);
    procedure TablesReset;
    function CheckNetworkExists: boolean;
    procedure DeleteTempFiles;
    function DeleteByRecNum(tbl:TTable;ARecNum:integer):boolean;
    function DeleteByFilter(tbl:TTable;const filter:string):boolean;
    function DeleteByIntField(tbl:TTable;const FieldName:string;IntValue:integer):boolean;
    function SQLRecordCount:integer;
    function GetMaxRecNum(Table:TTable):integer;
  end;

var
  Data: TData;

implementation

uses
  Util,
  utils2;

procedure DisplayFieldList(t:TTable);
var
  s:string;
  i:integer;
begin
  s:='Fields of '+t.TableName;
  for i:=0 to t.FieldCount-1 do
    s:=s+#10+#13+t.Fields[i].FieldName;
  MessageDlg(s,mtInformation,[mbOk],0);
end;

{$R *.DFM}
function TData.GetMaxRecNum(Table:TTable):integer;
var
  WasOpen:boolean;
  msg:string;
begin
  Result:=-1;
  msg:='';
  if not SQLSelect('SELECT MAX(RecNum) FROM '+GetTableName4SQL(Table),msg,Table,WasOpen) then
    begin
      if msg<>'' then MessageDlg(msg,mtError,[mbOk],0);
      exit;
    end;
  SQLFirst;
  if not SQLEOf then
    Result:=SQLFields(0).AsInteger;
  Table.Active:=WasOpen;

end;

function TData.DeleteByIntField(tbl:TTable;const FieldName:string;IntValue:integer):boolean;
var
  Table:TTable;
  found,ok:boolean;
begin
  ok:=TRue;
  Table:=TTable.Create(Self);
  try
    Table.DatabaseName:=tbl.DatabaseName;
     Table.TableName:=tbl.TableName;
  Table.Open;
  if Table.FieldByName(FieldName)=nil then
    begin
      Table.Free;
      Result:=False;
      exit;
    end;
  repeat
    found:=False;
    Table.First;
    while (not Table.Eof)and(not found)do
      begin
        if Table.FieldByName(FieldName).AsInteger=IntValue then
          begin
            found:=True;
            Table.delete;
          end
        else
          Table.Next;
      end;
    until not found;
  except
    ok:=False;
  end;
  Table.Free;
  Result:=ok;
end;

function TData.DeleteByFilter(tbl:TTable;const filter:string):boolean;
var
  table:TTable;
  recs:TStringList;
  i,code,v{,l}:integer;
  ok:boolean;
begin
  recs:=TStringlist.Create;
  Table:= TTable.Create(Self);
  ok:=True;
  try
    Table.DatabaseName:= tbl.DatabaseName;
    Table.TableName:= tbl.TableName;
    Table.IndexName:= tbl.IndexName;
    table.Open;
    table.Filter:=filter;
    table.Filtered:=True;
    table.First;
    while not table.Eof do
      begin
        recs.Add(table.FieldByName('RecNum').AsString);
        table.Next;
      end;
    table.Close;
    for i:=0 to recs.Count-1 do
      begin
        val(recs.Strings[i],v,code);
        if code=0 then
          if not DeleteByRecNum(tbl,v) then ok:=False;
      end;
  finally
    Table.Free;
    recs.Free;
  end;
  Result:=ok;
end;

function TData.DeleteByRecNum(tbl:TTable;ARecNum:integer):boolean;
begin
  Result:=True;
  if tbl<>nil then
    Result:=ExecSQL('DELETE FROM '+GetTableName4SQL(tbl)+' WHERE RecNum='+IntToStr(ARecNum),tbl);
end;

procedure TData.DeleteTempFiles;
const
  MAX_ENDINGS=7;
// numbers
  endings:array[1..MAX_ENDINGS] of string=('DB','FAM','MB','PX','TV','XG0','YG0');
var
  ExistentTableNames,FilesToDelete,Errors:TStringList;
  i,j,error,l,code,p,v:integer;
  sr:TSearchRec;
  pattern,strFileName,ending,strMessage:string;
  found:boolean;
  lfn: string;
begin
  lfn := GetLogFileName;

  ExistentTableNames := TStringList.Create;
  FilesToDelete := TStringLIst.Create;
  Errors := TStringList.Create;
  try
    for i := 0 to ComponentCount-1 do
      if Components[i] is TTable then
          ExistentTableNames.Add(uppercase(ExtractPureFileName((Components[i] as TTable).TableName)));
    Pattern := MakeFullPath(dbRasham.Directory, '*.*');
    error := FindFirst(pattern, faAnyFile, sr);
    while error = 0 do
    begin
      strFileName:=uppercase(ExtractPureFileName(sr.Name));
      found := False;
      i := 0;
      while (i < ExistentTableNames.Count) and (not found) do
        if strFileName = ExistentTableNames.Strings[i] then
          found := true
        else
          inc(i);
      if not found then
         FilesToDelete.Add(sr.Name);
      error := FindNext(sr);
    end;
    writelog('files to delete: ' + FilesToDelete.CommaText, lfn);
    for i := 0 to FilesToDelete.Count - 1 do
    begin
      strFileName := FilesToDelete[i];
      p := pos('.', strFileName);
      found := False;
      if p > 0 then
      begin
        l := length(strFileName);
        ending := uppercase(Copy(strFileName, p + 1, l - p));
        val(ending,v,code);
        if (code = 0) and (v > 0) then
          found := True;
        j := 1;
        while (j <= MAX_ENDINGS) and (not found) do
          if ending = endings[j] then
            found := True
          else
            inc(j);
      end;
      if found then
      begin
        strFileName := dbRasham.Directory;
        l := length(strFileName);
        if l <= 0 then
          strFileName := '\'
        else if strFileName[l] <> '\' then
          strFileName := strFileName + '\';
        strFileName := StrFileName + FilesToDelete[i];
        try
          DeleteFile(strFileName);
        except
          writelog('failed to delete ' + strFileName, lfn);
          Errors.Add(strFileName);
        end;
      end;
    end;
    if Errors.Count > 0 then
      strMessage := 'Error deleting files'
    else
      strMessage := '';
    for i := 0 to Errors.Count - 1 do
      strMessage := strMessage + #10#13 + Errors.Strings[i];
    if strMessage <> '' then
      MessageDlg(strMessage, mtError, [mbOk], 0);
  finally
    ExistentTableNames.Free;
    FilesToDelete.Free;
    Errors.Free;
  end;
end;

procedure TData.UpdateTableFields;
var
  WasOpen:boolean;
begin
  writelog('UpdateTableFields taDirectors', GetLogFileName);
  try
    WasOpen := taDirectors.Active;
    if not WasOpen then
      taDirectors.Open;
    ChangeFieldSize('Name',80, taDirectors);
    InitAutoIncrement(taDirectors);
    if not WasOpen then
      taDirectors.Close;
  except
    writelog('fail!!!', GetLogFileName);
  end;

  try
    writelog('UpdateTableFields taMagish', GetLogFileName);
    WasOpen := taMagish.Active;
    if not WasOpen then
      taMagish.Open;
    AddTableField(taMagish,'Mail','CHAR(120)');
    AddTableField(taMagish,'Fax','CHAR(15)');
    ChangeFieldSize('Index',7, taMagish);
    InitAutoIncrement(taMagish);
    if not WasOpen then
      taMagish.Close;
  except
    writelog('fail!!!', GetLogFileName);
  end;

  try
    writelog('UpdateTableFields taTaagid', GetLogFileName);
    WasOpen := taTaagid.Active;
    if not WasOpen then taTaagid.Open;
    ChangeFieldSize('Index',7, taTaagid);
    ChangeFieldSize('Name',80, taTaagid);
    InitAutoIncrement(taTaagid);
    if not WasOpen then
      taTaagid.Close;
  except
    writelog('fail!!!', GetLogFileName);
  end;

  try
    writelog('UpdateTableFields taFiles', GetLogFileName);
    WasOpen := taFiles.Active;
    if not Wasopen then
      taFiles.Open;
    ChangeFieldSize('RecName',83, taFiles);
    ChangeFieldSize('ResStr1',60, taFiles);
    InitAutoIncrement(taFiles);
    if not WasOpen then
      taFiles.Close;
  except
    writelog('fail!!!', GetLogFileName);
  end;

  try
    writelog('UpdateTableFields taForms', GetLogFileName);
    WasOpen := taForms.Active;
    if not WasOpen then
      taForms.Open;
    ChangeFieldSize('ResStr2',80, taForms);
    InitAutoIncrement(taForms);
    if not WasOpen then
      taForms.Close;
  except
    writelog('fail!!!', GetLogFileName);
  end;

  try
    writelog('UpdateTableFields taSigName', GetLogFileName);
    WasOpen := taSigName.Active;
    if not WasOpen then
      taSigName.Open;

    AddBlobField(taSigName, 'Takanon2');
    AddBlobField(taSigName, 'Takanon4');
    AddBlobField(taSigName, 'Takanon5');
    AddBlobField(taSigName, 'Takanon7');

    AddBlobField(taSigName, 'Takanon2_1');
    AddBlobField(taSigName, 'Takanon2_2');
    AddBlobField(taSigName, 'Takanon2_3');
    AddBlobField(taSigName, 'Takanon2_4');

    AddTableField(taSigName,'ResIntl','INTEGER');
    AddTableField(taSigName,'CompanyName2','CHAR(80)');
    AddTableField(taSigName,'CompanyName3','CHAR(80)');
    AddTableField(taSigName,'Mail','CHAR(120)');
    AddTableField(taSigName,'SharesRestrictIndex','INTEGER');
    AddTableField(taSigName,'DontOfferIndex','INTEGER');
    AddTableField(taSigName,'ShareHoldersNoIndex','INTEGER');
    AddTableField(taSigName,'SharesRestrictChapters','CHAR(40)');
    AddTableField(taSigName,'DontOfferChapters','CHAR(40)');
    AddTableField(taSigName,'ShareHoldersNoChapters','CHAR(40)');
    AddTableField(taSigName,'LimitedSignatoryIndex','INTEGER');
    AddTableField(taSigName,'LegalSectionsChapters','CHAR(40)');
    AddTableField(taSigName,'SignatorySectionsChapters','CHAR(40)');
    AddTableField(taSigName,'ConfirmedCopies','CHAR(10)');
    AddTableField(taSigName,'MoreCopies','CHAR(10)');
    AddTableField(taSigName,'RegulationItemsIndex','INTEGER');
    InitAutoIncrement(taSigName);
    if not WasOpen then
      taSigName.Close;
  except
    writelog('fail!!!', GetLogFileName);
  end;

  try
    writelog('UpdateTableFields taManager', GetLogFileName);
    WasOpen := taManager.Active;
    if not WasOpen then
      taManager.Open;
    InitAutoIncrement(taManager);
    AddTableField(taManager,'Mail','CHAR(120)');
    if not WasOpen then
      taManager.Close;
  except
    writelog('fail!!!', GetLogFileName);
  end;

  try
    writelog('UpdateTableFields taRishumSubmission', GetLogFileName);
    WasOpen := taRishumSubmission.Active;
    if not WasOpen then
      taRishumSubmission.Open;

    AddTableField(taRishumSubmission,'Ezel','CHAR(120)');
    AddTableField(taRishumSubmission,'RepMail','CHAR(255)');
    AddTableField(taRishumSubmission,'DByMail','INTEGER');
    if not WasOpen then
      taRishumSubmission.Close;
  except
    writelog('fail!!!', GetLogFileName);
  end;

  try
    writelog('UpdateTableFields taDoh', GetLogFileName);
    WasOpen := taDoh.Active;
    if not WasOpen then
      taDoh.Open;
    InitAutoIncrement(taDoh);
    AddTableField(taDoh,'MemaleRole', 'CHAR(50)');
    if not WasOpen then
      taDoh.Close;
  except
    writelog('fail!!!', GetLogFileName);
  end;
end;

procedure TData.StartTables;
var
  WasOpen: boolean;
begin
  writelog('startTables', GetLogFileName);
  WasOpen := taChanges.Active;
  if not WasOpen then
    taChanges.Open;
    if taChanges.FieldByName('ResInt1').AsString = '' Then
      while not taChanges.EOF do
      begin
        taChanges.Edit;
        taChanges.FieldByName('ResInt1').AsInteger := -1;
        //Post;
        taChanges.Next;
      end;
  writelog('startTables before autoinc', GetLogFileName);
  InitAutoIncrement(taChanges);
  if not Wasopen then
    taChanges.Close;
  writelog('startTables before updatetables', GetLogFileName);
  UpdateTableFields;
end;

function TData.SQLRowsAffected: integer;
begin
  Result := qGeneric.RowsAffected;
end;

function TData.SQLFirst:boolean;
begin
  Result:=True;
  try
    qGeneric.First;
  except
    Result:=False;
  end;
end;

function TData.SQLLast:boolean;
begin
  Result:=True;
  try
    qGeneric.First;
  except
    Result:=False;
  end;
end;

function TData.SQLFields(FieldNo:integer):TField;
begin
  Result:=nil;
  if qGeneric.FieldCount<=FieldNo then exit;
  try
    Result:=qGeneric.Fields[FieldNo];
  except
    Result:=nil;
  end;
end;

function TData.SQLQueryIsEmpty:boolean;
begin
  try
    Result:=(qGeneric.RecordCount<=0);
  except
    Result:=True;
  end;
end;


function TData.SQLSelectWithDateRange(const sqlCommand,paramName1,paramName2:string;dateRangeStart,dateRangeEnd:TDateTime;var error:string):boolean;
begin
  Result:=True;
  error:='';
  try
    if qGeneric.Active then qGeneric.Close;
    qGeneric.SQL.Text:=sqlCommand;
    if trim(paramName1)<>'' then
       qGeneric.ParamByName(paramName1).AsDateTime:=dateRangeStart;
    if trim(paramName2)<>'' then
      qGeneric.ParamByName(paramName2).AsDateTime:=dateRangeEnd;
    qGeneric.Prepare;
    qGeneric.Open;
  except
    on E:Exception do
    begin
      error:=E.Message;
      Result:=False;
    end;
  end;
end;

function TData.SQLRecordCount:integer;
begin
  Result := qGeneric.RecordCount;
end;

function TData.SQLSelect2(const sqlCommand:string;var error:string):boolean;
begin
  Result := True;
  error := '';
  try
    if qGeneric.Active then
      qGeneric.Close;
    qGeneric.SQL.Text := sqlCommand;
    qGeneric.Prepare;
    qGeneric.Open;
  except
    on E:Exception do
    begin
      error := E.Message;
      Result := False;
    end;
  end;
end;

function TData.SQLSelect(const sqlCommand: string; var msg: string;
                         tbl: TTable; var TableWasOpen: boolean): boolean;
var
  wo,ok: boolean;
  filename: string;
begin
  msg := '';
  Result := True;
  wo := False;
  if tbl <> nil then
    wo := tbl.Active;
  TableWasOpen := wo;
  if wo then
    tbl.Close;
  ok := SQLSelect2(sqlCommand,msg);
  if not ok then
  begin
    if IsLCKError(msg)then
    begin
      filename := ExtractFileNameFromError(msg);
      if filename = '' then
        ok := False
      else
        ok := DoDeleteFile(filename,msg);
      if ok then
        ok := SQLSelect2(sqlCommand,msg);
    end;
  end;
  Result := ok;
  if not ok then
  begin
    if wo then
      tbl.Open;
    TableWasOPen := tbl.Active;
  end;
  if (not ok) and (msg <> '') then
    MessageDlg(msg, mtError, [mbOk], 0);
end;

function TData.SQLClose:boolean;
begin
  Result:=True;
  try
    if qGeneric.Active then
      qGeneric.Close;
    if qGeneric.Prepared then
      qGeneric.Unprepare;
  except
    Result:=False;
  end;
end;

function TData.SQLFieldByName(const FieldName:string):TField;
begin
  try
    Result:=qGeneric.FieldByName(FieldName);
  except
    Result:=nil;
  end;
end;

function TData.SQLBOF:boolean;
begin
  try
    Result:=qGeneric.BOF;
  except
    Result:=True;
  end;
end;

function TData.SQLEOF:boolean;
begin
  try
    Result:=qGeneric.EOF;
  except
    Result:=True;
  end;
end;

function TData.SQLPrev:boolean;
begin
  Result:=True;
  try
    qGeneric.Prior;
  except
    Result:=False;
  end;
end;

function TData.SQLNext:boolean;
begin
  Result:=True;
  try
    qGeneric.Next;
  except
    Result:=False;
  end;
end;

function TData.ExecSQL2(const sqlCommand:string;var error:string):boolean;
begin
  Result:=True;
  error:='';
  try
    if qGeneric.Active then
      qGeneric.Close;
    qGeneric.SQL.Text:=sqlCommand;
    qGeneric.ExecSQL;
  except
    on E: Exception do
    begin
      error := E.Message;
      Result := False;
    end;
  end;
end;

function TData.ExecSQL(const sqlCommand: string; tbl: TTable): boolean;
var
  wo, ok: boolean;
  error, LCKFileName: string;
begin
  wo := False;
  if tbl <> nil then
    wo := tbl.Active;
  if wo then
    tbl.Close;
  ok := ExecSQL2(sqlCommand,error);
  if not ok then
  begin
    if IsLCKError(error)then
    begin
      LCKFileName := ExtractFileNameFromError(error);
      if LCKFileName = '' then
        ok := False
      else
        ok := DoDeleteFile(LCKFileName, error);
      if ok then
        ok := ExecSQL2(sqlCommand, error);
    end;
  end;
  Result := ok;
  if wo then
    tbl.Open;
  if (not ok) and (error <> '') then
    MessageDlg(error, mtError, [mbOk], 0);
end;

function TData.AddTableField(tbl:TTable;const fieldName,fieldType:string):boolean;
var
  strTableName: string;
  l,p: integer;
  WasOpen: boolean;
begin
  WasOpen := tbl.Active;
  Result := True;
  if not WasOpen then
    tbl.Open;
  if tbl.FindField(fieldName) <> nil then
  begin
    if not WasOpen then tbl.Close;
    exit;
  end;
  strTableName := uppercase(trim(tbl.TableName));
  l := length(strTableName);
  if l > 0 then
    if pos('.DB', strTablename) = l - 2 then
      strTableName := Copy(strTableName, 1, l - 3);
  p := lastpos('\', strTableName);
  if p > 0 then
    strTableName := Copy(strTableName, p + 1, l);
  if not ExecSQL('ALTER TABLE ' + strTableName + ' ADD COLUMN ' + fieldName + ' ' + fieldType, tbl) then
    Result := False;
end;

procedure TData.TablesActivate(Value: Boolean);
var
  I: Integer;
  TableList: TStringList;
  OldIndex: String;
  RebuildAutoInc: Boolean;
  LogFileName,s:string;
begin
  LogFileName := GetLogFileName;
  TableList := TStringList.Create;
  I := 0;
  try
    try
      for I := 0 to ComponentCount-1 do begin
        If (Components[I] is TTable) and (Components[I]<>taAutoInc) then
          with (Components[I] as TTable) do
          begin
            WriteLog('Adding ' +TableName+' to list...',LogFileName);
            try
              Active := Value;
              if Active then
                s := 'Open'
              else
                s := 'Close';
              s := s + ' ' + TableName;
              WriteLog(s, LogFileName);
              if FieldDefs.Count<=0 then
                WriteLog('No fields at ' + TableName, LogFileName)
              else if Active and (Fields[0].FieldName = 'RecNum') then
              begin
                OldIndex := IndexName;
                IndexFieldNames := 'RecNum';
                Last;
                IndexName := OldIndex;
                First;
              end;
            except
              on E: Exception do
                WriteLog('Error adding ' + TableName + ' to list, ' + E.Message, LogFileName);
            end;
          end; // with
      end; // for
      I := 0;
    except
      on e: Exception do
      begin
        WriteLog(e.Message,LogFileName);
        if e is EDBEngineError then
        begin
          RebuildDB('Rasham', LogFileName);//'C:\RashLog.txt');
          TablesActivate(Value);
          exit;
        end
        else
        begin
          WriteLog('Error opening ' + (Components[I] as TTable).TableName, LogFileName);//'C:\RashLog.txt');
          Raise;
        end;
      end;
    end;


    // Init AutoInc
    if Value then
    begin
      try
        RebuildAutoInc:= False;
        taAutoInc.Open;
        for I:= 0 to TableList.Count-1 do
        begin
          WriteLog('Init autoinc for '+TableList.Strings[i],LogFileName);
          if taAutoInc.FieldByName(TableList[I]).AsInteger< Integer(TableList.Objects[I]) then  // table has bigger value than autoinc
          begin
            RebuildAutoInc:= True;
            Break;
          end;
        end;
      except
        RebuildAutoInc:= True;
      end;

      if RebuildAutoInc then
      begin
        WriteLog('Rebuild autoinc...',LogFileName);
        taAutoInc.Close;
        taAutoInc.FieldDefs.Clear;
        for I := 0 to TableList.Count - 1 do
          taAutoInc.FieldDefs.Add(TableList[I], ftInteger,0,False);
        taAutoInc.CreateTable;
        taAutoInc.Open;
        taAutoInc.Append;
        for I := 0 to TableList.Count - 1 do
          taAutoInc.Fields[I].AsInteger := Integer(TableList.Objects[I]);
        taAutoInc.Post;
      end;
    end
    else
      taAutoInc.Close;
  finally
    TableList.Free;
  end;
end;

procedure TData.TablesReset;
var
  i: Integer;
begin
  for I:= 0 to ComponentCount-1 do
    If Components[I] is TTable then
      with (Components[I] as TTable) do
      begin
        Filtered:= False;
        Filter:= '';
        CancelRange;
      end;
end;

procedure TData.AppendAutoInc(DataSet: TDataSet);
var
  Value: Integer;
begin
  if not taAutoInc.Active then
    taAutoInc.Open;
  taAutoInc.Refresh;

  { Added on 07/03/2005 by ES - new log --> }
  WriteLog(#9#9#9 + 'AutoInc edited: ' + (DataSet as TTable).TableName, MainPath + 'NewCompanyCreate.log');
  WriteChangeLog(#9#9#9 + 'Table Changed: ' + (DataSet as TTable).TableName);
  { <-- }
  if (DataSet.FieldDefs[0].Name='RecNum') and (DataSet.FieldByName('RecNum').IsNull) then
  begin
    { Added on 07/03/2005 by ES - new log --> }
    WriteLog(#9#9#9#9 + 'Field value in AutoInc before save: ' +
              taAutoInc.FieldByName(LowerCase((DataSet as TTable).TableName)).AsString, MainPath + 'NewCompanyCreate.log');
    { <-- }
    Value := taAutoInc.FieldByName(LowerCase((DataSet as TTable).TableName)).AsInteger+1;
    taAutoInc.Edit;
    taAutoInc.FieldByName(LowerCase((DataSet as TTable).TableName)).AsInteger:= Value;
    taAutoInc.Post;
    { Added on 07/03/2005 by ES - new log --> }
    WriteLog(#9#9#9#9 + 'Field value in AutoInc after save: ' + intToStr(Value), MainPath + 'NewCompanyCreate.log');
    { <-- }
    DataSet.FieldByName('RecNum').AsInteger:= Value;
    WriteLog(#9#9#9#9 + '    ' + 'AutoInc value saved back to table:' + IntToStr(Value), MainPath + 'NewCompanyCreate.log');
  end;
end;

procedure TData.InitAutoIncrement(DataSet: TDataSet);
var
  AfterOpenProc: TDataSetNotifyEvent;
  Rec: ChangeRec;
begin
  DataSet.FieldDefs.Update;
  if (DataSet.FieldDefs[0].Name='RecNum') and (DataSet.FieldDefs[0].DataType=ftAutoInc) then
  begin
    AfterOpenProc:= DataSet.AfterOpen;
    DataSet.Close;
    (DataSet as TTable).Exclusive:= True;
    DataSet.AfterOpen:= nil;  // Stop the recursive procedure
    DataSet.Open;
    ZeroMemory(@Rec, SizeOf(Rec));
    Rec.iType:= fldPDXLONG;
    ChangeField(DataSet as TTable, DataSet.FieldByName('RecNum'), Rec);
    DataSet.AfterOpen:=AfterOpenProc;
  end;
end;

procedure TData.AddBlobField(DataSet: TDataSet; const FieldName: DBINAME);
var
  Rec: ChangeRec;
begin
  if DataSet.FindField(FieldName)<>nil then exit;
  ZeroMemory(@Rec, SizeOf(Rec));
  Rec.szName := FieldName;
  Rec.iType  :=  fldBLOB;
  Rec.iLength :=1;
  Rec.iSubType:= fldstMEMO;
  AddField(DataSet as TTable, Rec);
end;

procedure TData.AddStringField(DataSet: TDataSet; const FieldName: DBINAME; const FieldSize: integer);
var
  Rec: ChangeRec;
begin
  if DataSet.FindField(FieldName)<>nil then exit;
  ZeroMemory(@Rec, SizeOf(Rec));
  Rec.szName    := FieldName;
  Rec.iType     := fldZSTRING;
  Rec.iLength   := FieldSize;
  Rec.iSubType  := fldstMEMO;
  AddField(DataSet as TTable, Rec);
end;

procedure TData.AddIntegerField(DataSet: TDataSet; const FieldName: DBINAME);
var
  Rec: ChangeRec;
begin
  if DataSet.FindField(FieldName)<>nil then
    exit;
  ZeroMemory(@Rec, SizeOf(Rec));
  Rec.szName    := FieldName;
  Rec.iType     := fldINT32;
  AddField(DataSet as TTable, Rec);
end;

//==============================================================================
function TData.CheckNetworkExists: boolean;
begin
  Result := DirectoryExists(dbRasham.Directory);
end;
//==============================================================================

procedure TData.ChangeFieldSize(FieldName: string; FieldSize: integer; Table:TTable ) ;
var
  rec: ChangeRec;
  wo: boolean;
begin
  wo := Table.Active;
  if not wo then
    Table.Open;
  If Table.FieldByName(FieldName).Size <> FieldSize Then
  begin
    Table.Close;
    Table.Exclusive := True;
    Table.Open;
    ZeroMemory(@rec, SizeOf(Rec));
    Rec.iLength := FieldSize;
    ChangeField(Table as TTable, Table.FieldByName(FieldName), rec);
  end;
  if not wo then
    Table.Close;
end;

procedure TData.DataCreate(Sender: TObject);
begin
  writeLog('TData.DataCreate - DeleteTempFiles', GetLogFileName);
  try
    DeleteTempFiles;
    writeLog('TData.DataCreate - StartTables', GetLogFileName);
    StartTables;
  finally
    writeLog('TData.DataCreate - end', GetLogFileName);
  end;
end;

procedure TData.DeleteLockFiles;
var
  AvailableDrives: string;
  l,i: integer;
begin
  AvailableDrives := GetDriveLetters;
  l := length(AvailableDrives);
  for i := 1 to l do
    DeletePdxLockFile(AvailableDrives[i]+':\', GetWinDir);
end;

end.

