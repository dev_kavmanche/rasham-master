unit PeopleSearch;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  LoadSaveTemplate, HebForm, StdCtrls, HGrids, SdGrid, HEdit, ExtCtrls,
  HLabel, HRadio, DataObjects;

type
  TfmPeopleSearch = class(TfmLoadSaveTemplate)
    eCompany: THebEdit;
    btBrowse: TButton;
    btShowAll: TButton;
    procedure sdNamesSelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure edNameKeyPress(Sender: TObject; var Key: Char);
    procedure btOkClick(Sender: TObject);
    procedure btBrowseClick(Sender: TObject);
    procedure btShowAllClick(Sender: TObject);
  private
    { Private declarations }
    DoNotUpdate: Boolean;
    FCompanyList: TList;
    FCompany: TCompany;
    FResult: TListedClass;
    procedure FillData(Company: TCompany; Append: Boolean);
  public
    { Public declarations }
    function Execute(var Company: TCompany): TListedClass;
  end;

var
  fmPeopleSearch: TfmPeopleSearch;

implementation

{$R *.DFM}

uses Util, dialog, Companies;

procedure TfmPeopleSearch.FillData(Company: TCompany; Append: Boolean);
var
  I, Index: Integer;
  DirZeut, DirList, StockZeut, StockList: TStringList;

  procedure AddCompany(Company: TCompany);
  var
    I: Integer;
  begin
    Company.Directors.Filter:= afAll;
    For I:= Company.Directors.Count-1 DownTo 0 do
      begin
        DirList.Add(Company.Directors.Items[I].Name);
        DirZeut.Add(Company.Directors.Items[I].Zeut);
      end;
    Company.StockHolders.Filter:= afAll;
    For I:= Company.StockHolders.Count-1 Downto 0 do
      begin
        StockList.Add(Company.StockHolders.Items[I].Name);
        StockZeut.Add(Company.StockHolders.Items[I].Zeut);
      end;
  end;

begin
  If Not Append Then
    Screen.Cursor:= crHourGlass;
  try
    If Not Append Then
      begin
        FCompany.Free;
        FCompany:= Company;
        If (Company <> nil) and ((Company.Directors.Count > 0) Or (Company.StockHolders.Count > 0)) Then
          eCompany.Text:= Company.Name
        else
          begin
            btBrowse.Click;
            Exit;
          end;

        sdNames.RowCount:=1;
        sdNames.Rows[0].Clear;
      end;

    DirZeut:= TStringList.Create;
    DirList:= TStringList.Create;
    StockZeut:= TStringList.Create;
    StockList:= TStringList.Create;

    AddCompany(FCompany);

    For I:= 0 to DirList.Count-1 do
      if sdNames.Cols[1].IndexOf(DirZeut.Strings[I])=-1 then
        begin
          sdNames.Cells[0,sdNames.RowCount-1]:= DirList.Strings[I];
          sdNames.Cells[1,sdNames.RowCount-1]:= DirZeut.Strings[I];
          sdNames.Cells[2,sdNames.RowCount-1]:= '�������';
          sdNames.Rows[sdNames.RowCount-1].Objects[0]:= FCompany;
          sdNames.RowCount:= sdNames.RowCount+1;
          sdNames.Rows[sdNames.RowCount-1].Clear;
        end;

    For I:= 0 to StockList.Count-1 do
      begin
        Index:= sdNames.Cols[1].IndexOf(StockZeut.Strings[I]);
        If Index =-1 then
          begin
            sdNames.Cells[0,sdNames.RowCount-1]:= StockList.Strings[I];
            sdNames.Cells[1,sdNames.RowCount-1]:= StockZeut.Strings[I];
            sdNames.Cells[2,sdNames.RowCount-1]:= '��� �����';
            sdNames.Rows[sdNames.RowCount-1].Objects[0]:= FCompany;
            sdNames.RowCount:= sdNames.RowCount+1;
            sdNames.Rows[sdNames.RowCount-1].Clear;
          end
        Else
          If sdNames.Cells[2, Index] = '�������' Then
            sdNames.Cells[2, Index] := '���''/��� �����';
      end;

    if sdNames.RowCount>1 Then
      sdNames.RowCount:= sdNames.RowCount-1;
    If Not Append Then
      begin
        //SortGrid(sdNames, 0, -1, stString, stString);
        SortGrid(sdNames, '0,STRING,UP');
        sdNames.Repaint;
      end;
  finally
    If Not Append Then
      Screen.Cursor:= crDefault;
  end;
end;

function TfmPeopleSearch.Execute(var Company: TCompany): TListedClass;
var
  I: Integer;
begin
  FResult:= nil;
  FillData(Company, False);
  if eCompany.Text <> '' Then
    begin
      ShowModal;
      If Assigned(FCompanyList) and (Not Assigned(FResult)) Then
        begin
          For I:= 0 To FCompanyList.Count-1 do
            TCompany(FCompanyList.Items[I]).Free;
          FCompanyList.Free;
          FCompanyList:= nil;
        end;
    end;
  Company:= FCompany;
  Result:= FResult;
end;

procedure TfmPeopleSearch.sdNamesSelectCell(Sender: TObject; Col,
  Row: Integer; var CanSelect: Boolean);
begin
  If DoNotUpdate Then
    Exit;
  inherited;
  edName.Text:= sdNames.Cells[0,Row];
  edNumber.Text:= sdNames.Cells[1,Row];
end;

procedure TfmPeopleSearch.edNameKeyPress(Sender: TObject; var Key: Char);
type
  TLegals = set of char;
Const
  Legals : TLegals = ['0'..'9','/',#8];
Var
  Tmp: String;
  I: Integer;
  Col: Integer;
begin
  inherited;
  If (Key=#13) Then
    If (Sender = edName) Then
      begin
        edNumber.SetFocus;
        Key:= #0;
      end
    Else
      begin
        btOk.Click;
      end;

  If (Sender = edNumber) And (Not (Key in Legals)) then
    Key := #0;

  If (Sender As TCustomEdit).SelLength>0 then
    begin
       Tmp:=(Sender As TCustomEdit).Text;
       Delete(Tmp, (Sender As TCustomEdit).SelStart+1, (Sender As TCustomEdit).SelLength);
       Tmp:=Tmp+Key;
    end
  else
    Tmp:=(Sender As TCustomEdit).Text + Key;

  If Sender = edNumber then
    Col:= 1
  Else
    Col:= 0;

  I:=0;
  While (Pos(Tmp,sdNames.Cells[Col,I])<>1) and (I< sdNames.RowCount) Do
    Inc(I);
  if I< sdNames.RowCount then
    begin
      edName.Text:=sdNames.Cells[0,I];
      edNumber.Text:= sdNames.Cells[1,I];
      (Sender As TCustomEdit).SelStart:=Length(Tmp);
      (Sender As TCustomEdit).SelLength:=Length((Sender As TCustomEdit).Text)-Length(Tmp);
      DoNotUpdate:= True;
      try
        sdNames.Row:= I;
      finally
        DoNotUpdate:= False;
      end;
      Key:=#0;
    end
  Else
    If Sender = edName then
      edNumber.Text:= ''
    Else
      edName.Text:= '';
end;

procedure TfmPeopleSearch.btOkClick(Sender: TObject);
var
  Line, Index: Integer;

  procedure CheckCompany(Company: TCompany);
  var
    ListClass: TListObject;
  begin
    if sdNames.Cells[2,Line] = '��� �����' Then
      ListClass:= Company.StockHolders
    Else
      ListClass:= Company.Directors;

    ListClass.Filter:= afAll;
    FResult:= ListClass.FindByZeut(sdNames.Cells[1,Line]);
    If FResult<>nil then
      Close;
  end;

begin
  if edNumber.Text<> '' Then
    Line:= sdNames.Cols[1].IndexOf(edNumber.Text)
  Else
    Line:= sdNames.Cols[0].IndexOf(edName.Text);

  If Line=-1 Then
    begin
      fmDialog.ActiveControl:= fmDialog.Button3;
      fmDialog.Caption:= '�����';
      fmDialog.memo.lines.Clear;
      fmDialog.memo.lines.add('��� ��');
      fmDialog.memo.lines.add('�� �� �� ���� ����� �������');
      fmDialog.Button1.Visible:= false;
      fmDialog.Button2.Visible:= False;
      fmDialog.Button3.Caption:= '�����';
      fmDialog.ShowModal;
      fmDialog.Button3.Caption:= '�����';
      fmDialog.Button1.Visible:= True;
      fmDialog.Button2.Visible:= True;
      fmDialog.Caption:= '����';
    end
  else
    If not Assigned(FCompanyList) Then
      CheckCompany(FCompany)
    else
      begin
        FCompany:= TCompany(sdNames.Rows[Line].Objects[0]);
        CheckCompany(FCompany);
        For Index:= 0 To FCompanyList.Count-1 do
          If FCompanyList.Items[Index] <> FCompany Then
            TCompany(FCompanyList.Items[Index]).Free;
        FCompanyList.Free;
        FCompanyList:= nil;
      end;
end;

procedure TfmPeopleSearch.btBrowseClick(Sender: TObject);
var
  TempCompany: TCompany;
  I: Integer;
begin
  inherited;
  If Assigned(FCompanyList) Then
    begin
      For I:= 0 To FCompanyList.Count-1 do
        TCompany(FCompanyList.Items[I]).Free;
      FCompanyList.Free;
      FCompanyList:= nil;
    end;
    
  TempCompany:= fmCompanies.Execute(Caption, afActive);
  If TempCompany <> Nil Then
    FillData(TempCompany, False);

  try
    sdNames.SetFocus;
  except end;
end;

procedure TfmPeopleSearch.btShowAllClick(Sender: TObject);
var
  Companies: TCompanies;
  I: Integer;
begin
  inherited;
  Screen.Cursor:= crHourGlass;
  DoNotUpdate:= True;
  try
    If Not Assigned(FCompanyList) Then
      begin
        FCompany.Free;
        FCompany:= nil;

        FCompanyList:= TList.Create;
        Companies:= TCompanies.Create(EncodeDate(3000,01,01), False, True);
        try
          For I:= 0 To Companies.Count-1 do
            FCompanyList.Add(Companies.Items[I]);
        finally
          Companies.Free;
        end;
      end;

    // sdNames.Visible:= False;
    sdNames.RowCount:=1;
    sdNames.Rows[0].Clear;

    For I:= 0 To FCompanyList.Count-1 do
      begin
        FCompany:= TCompany(FCompanyList.Items[I]);
        FillData(FCompany, True);
        sdNames.RowCount:= sdNames.RowCount + 1;
        sdNames.Rows[sdNames.RowCount-1].Clear;
      end;

    If sdNames.RowCount > 1 Then
      sdNames.RowCount:= sdNames.RowCount - 1;
    FCompany:= nil;
    //SortGrid(sdNames, 0, -1, stString, stString);
    SortGrid(sdNames,'0,STRING,UP');
    try
      sdNames.SetFocus;
    except end;
  finally
    // sdNames.Visible:= True;
    eCompany.Text:= '�� ������';
    DoNotUpdate:= False;
    Screen.Cursor:= crDefault;
  end;
end;

end.
