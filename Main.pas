{
Unit Name: Main
Purpose  : main application form
Author   : N/A
MODIFICATION HISTORY:
DATE        VERSION   BY   TASK      DESCRIPTION
26/08/2019  1.7.7     sts            code refactoring; added ExeVersion property
23/12/2020  1.8.4     sts  22057     refactoring
}

unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Menus, HMenus, HebForm, DataObjects, Util,
  RichEdit2;

const
  C_PREV_VERSION = 1600;
  C_CURRENT_VER = 1770;
  {Special installation - Lock Up on 01/08/2003
   ATTENTION! Unit ExpiryExtend has to be unREMed  -->}
 { ExpiryDate : TDateTime = 37834;   { 01/08/2003 }
  C_HEB_RASHAM_REG : String = 'Software\HpSoft\��� ����\VersionInfo';
  C_RASHAM_REG : String = 'Software\HpSoft\Rasham\VersionInfo';
  {<--}

type
  TfmMain = class(TForm)
    HebMainMenu1: THebMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    HebForm1: THebForm;
    N9: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    MyBitmap: TImage;
    N14: TMenuItem;
    N15: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N8: TMenuItem;
    N10: TMenuItem;
    N13: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    N31: TMenuItem;
    MDITimer: TTimer;
    N4: TMenuItem;
    N7: TMenuItem;
    N32: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    N38: TMenuItem;
    N39: TMenuItem;
    N40: TMenuItem;
    rePrintTakanon2: TRichEdit98;
    rePrintTakanon4: TRichEdit98;
    rePrintTakanon5: TRichEdit98;
    rePrintTakanon7: TRichEdit98;
    N42: TMenuItem;
    nEngName: TMenuItem;
    nZipCode: TMenuItem;
    N43: TMenuItem;
    miExit: TMenuItem;
    miTakanon: TMenuItem;
    miTakanonChange: TMenuItem;
    miTakanonOutput: TMenuItem;
    N41: TMenuItem;
    N44: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure MDITimerTimer(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N37Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N42Click(Sender: TObject);
    procedure nEngNameClick(Sender: TObject);
    procedure nZipCodeClick(Sender: TObject);
    procedure miExitClick(Sender: TObject);
    procedure miTakanonOutputClick(Sender: TObject);
    procedure miTakanonChangeClick(Sender: TObject);
  private
    { Private declarations }
    KeyNameToUse, RashamRegToUse: string;
    FClientInstance, FPrevClientProc : TFarProc;
    function GetRegistered: Boolean;
    function GetRegNum: Integer;
    function CheckKey(Key: Integer): Boolean;
    function RegChange: Integer;
    function GetExeVerion: string;   //sts17674
    {Special installation - Lock Up on 01/08/2003 -->}
    function GetExpired : Boolean;
    procedure ClientWndProc(var Message : TMessage);
    procedure SetExpired( NewExpired : Boolean );
    procedure GetRegKeysToUse;
    procedure FileVersion;
    procedure CheckForUpdate;
  public
    { Public declarations }
    NeedCheckID: Boolean;
    UserOptions: TUserOptions;
    function DemoMode: Boolean;
    function GetVersion: Double;
    function GetHelpPath: String;
    function GetVolumeSerNum: Integer;
    function GetTextWidth(const Text: String; Font: TFont): Integer;
    function SetRegistered(RegNum: Integer): Boolean;

    procedure ShowCompanyInfo(Company: TCompany; Date: TDateTime);
    procedure UndoFile(FileItem: TFileItem);

    property Registered : Boolean read GetRegistered;
    property RegNum     : Integer read GetRegNum;
    property ExeVersion : string  read GetExeVerion;        //sts17674
    {Special installation - Lock Up on 01/08/2003 -->}
    property Expired    : Boolean read GetExpired       write SetExpired;
    {<--}
  end;

var
  fmMain: TfmMain;

implementation

uses Registry, dialog, Companies, uMenu, NewCompanyInfo, EditCompanyInfo, Rishum,
  IncHon, DecHon, BlankDirectors, BlankStocks, BlankHaavara, ShowCompany, Number,
  Doh, BookDirectors, BookStock, BlankAddress, About, BlankUndo, BlankName, UpdateTables,
  StockHolderAddress, BlankDirectorAddress, SugStock, SignName, Lawer,
  BackUp, Help, DataM, CompaniesDisabler, RoeHeshbon, FMXUtils, dialog2,
  PrintSetup, unMerge, unManager, unHamara, ExpiryExtend, HPSConsts,
  BlankEngName, BlankZip, utils2, TakanonChange, HDialogs, TakanonRep ;

{$R *.DFM}

const
  C_HEB_KEY_NAME ='Software\HPSoft\��� ����';
  C_KEY_NAME ='Software\HPSoft\Rasham';

function TfmMain.GetTextWidth(const Text: String; Font: TFont): Integer;
begin
  Canvas.Font.Assign(Font);
  Result:= Canvas.TextWidth(Text);
end;

procedure TfmMain.ShowCompanyInfo(Company: TCompany; Date: TDateTime);
begin
  fmShowCompany.Execute(Date,Company.RecNum);
end;

procedure TfmMain.ClientWndProc(var Message : TMessage);
var
  MyDC       : hDC;
  Row        : Integer;
  Column     : Integer;
begin
  with Message do
    case Msg of
      WM_ERASEBKGND :
      begin
        MyDC := TWMEraseBkGnd(Message).DC;
        for Row := 0 to ClientHeight div MyBitmap.Picture.Height do
          for Column := 0 to ClientWidth div MyBitmap.Picture.Width do
            BitBlt(MyDC, Column * MyBitmap.Picture.Width, Row * MyBitmap.Picture.Height,
              MyBitmap.Picture.Width, MyBitmap.Picture.Height,
              MyBitmap.Picture.Bitmap.Canvas.Handle, 0, 0, SRCCOPY);
        Result := 1;
      end;
      WM_VSCROLL, WM_HSCROLL :
      begin
        InvalidateRect(ClientHandle, nil, True);
        Result := CallWindowProc(FPrevClientProc, ClientHandle, Msg,
        wParam, lParam);
      end;
    else
      Result := CallWindowProc(FPrevClientProc, ClientHandle, Msg,
      wParam, lParam);
    end;
end;

procedure TfmMain.FormCreate(Sender: TObject);
var
  LogFileName: string;
begin
  LogFileName:=GetLogFileName;
  WriteLog('VersionUpdate', LogFileName);
  GetRegKeysToUse();
  VersionUpdate(Self);
  WriteLog('Data.TablesActivate <- True', LogFileName);
  Data.TablesActivate(True);
  WriteLog('Set Wallpaper', LogFileName);
  try
    FClientInstance := MakeObjectInstance(ClientWndProc);
    FPrevClientProc := Pointer(GetWindowLong(ClientHandle, GWL_WNDPROC));
    SetWindowLong(ClientHandle, GWL_WNDPROC, LongInt(FClientInstance));
  except
    WriteLog('Wallpaper failed', LogFileName);
  end;

  WriteLog('Create User Options Object', LogFileName);
  UserOptions:= TUserOptions.Create(Data.taName.Database.Directory+'Account.dat');
  NeedCheckID:= UserOptions.IDCheck;
  WriteLog('fmMain Create Done', LogFileName);//'C:\RashLog.txt');
    { Added 06/03/2005 by ES - new log --> }
    MainPath := ExtractFilePath( Application.ExeName );
    UserComp := Format( '<%s from %s>', [GetUserFromWindows, GetComputerNetName] );
    { <-- }
  FileVersion;

  {$IFDEF MOSHE}
    N41.Visible := True;
  {$ENDIF}

end; // TfmMain.FormCreate

procedure TfmMain.MDITimerTimer(Sender: TObject);

  procedure ProcessMenu(CurNode: TMenuItem);
  var
    I: Integer;
  begin
    If CurNode.Tag = 2 Then
      CurNode.Enabled:= fmMenu.Enabled
    Else
      For I:= 0 To CurNode.Count-1 do
        ProcessMenu(CurNode.Items[I]);
  end;

begin
  If fmMenu = nil Then
    Exit;

  If MDIChildCount > 1 Then
  begin
    If fmMenu.WindowState = wsNormal Then
    begin
      fmMenu.WindowState:= wsMinimized;
      fmMenu.Enabled:= False;
      ProcessMenu(HebMainMenu1.Items);
    end;
  end
  else If fmMenu.WindowState = wsMinimized Then
  begin
    fmMenu.Enabled:= True;
    fmMenu.WindowState:= wsNormal;
    ProcessMenu(HebMainMenu1.Items);
  end;
end;

procedure TfmMain.N2Click(Sender: TObject);
begin
  If Sender = N4 Then
  begin
    Application.CreateForm(TAboutBox, AboutBox);
    AboutBox.ShowModal;
    AboutBox.Free;
  end
  Else If Sender = N38 Then
    Application.CreateForm(TfmPrintSetup, fmPrintSetup)
  Else If Sender = N34 then
    ShowHelp(-1)
  Else If Sender = N2 Then
    Application.CreateForm(TfmNewCompanyInfo, fmNewCompanyInfo)
  Else If Sender = N3 Then
    Application.CreateForm(TfmEditCompanyInfo, fmEditCompanyInfo)
  Else If Sender = N8 Then
    Application.CreateForm(TfmRishum, fmRishum)
  Else If Sender = N13 Then
    Application.CreateForm(TfmIncHon, fmIncHon)
  Else If Sender = N16 then
    Application.CreateForm(TfmDecHon, fmDecHon)
  Else If Sender = N12 then
    Application.CreateForm(TfmDirectors, fmDirectors)
  Else If Sender = N17 Then
    Application.CreateForm(TfmStocks, fmStocks)
  Else If Sender = N18 Then
    Application.CreateForm(TfmHaavara, fmHaavara)
  Else If Sender = N9 Then
    fmShowCompany.Execute(EncodeDate(3000,01,02), -1)
  Else If Sender = N10 Then
    Application.CreateForm(TfmNumber, fmNumber)
  Else If Sender = N15 Then
    Application.CreateForm(TfmDoh, fmDoh)
  Else If Sender = N24 Then
    Application.CreateForm(TfmBookDirectors, fmBookDirectors)
  Else If Sender = N22 Then
    Application.CreateForm(TfmBookStocks, fmBookStocks)
  Else If Sender = N26 Then
    Application.CreateForm(TfmBlankAddress, fmBlankAddress)
  // <----------------------------| Line cut
  Else If Sender = N7 Then
    Application.CreateForm(TfmUndoBlank, fmUndoBlank)
  Else If Sender = N27 then
    Application.CreateForm(TfmBlankName, fmBlankName)
  Else If Sender = N21 then
    Application.CreateForm(TfmStockAddress, fmStockAddress)
  Else If Sender = N23 then
    Application.CreateForm(TfmDirectorAddress, fmDirectorAddress)
  Else If (Sender = N30) Or (Sender = N19) then
    Application.CreateForm(TfmSugStock, fmSugStock)
  Else If Sender = N29 then
    Application.CreateForm(TfmSignName, fmSignName)
  Else If Sender = N31 then
    Application.CreateForm(TfmLawer, fmLawer)
  Else If Sender = N33 then
    Application.CreateForm(TfmBackUp, fmBackUp)
  Else If Sender = N35 Then
    Application.CreateForm(TfmCompaniesDisabler, fmCompaniesDisabler)
  Else If Sender = N36 then
    Application.CreateForm(TfmRoeHeshbon, fmRoeHeshbon)
  else If Sender = N39 then
    Application.CreateForm(TfmMergeModule, fmMergeModule)
  else If Sender = N40 then
    Application.CreateForm(TfmManager, fmManager)
  Else If Sender = N41 Then
    Application.CreateForm(TfmHamara, fmHamara);
end;

procedure TfmMain.UndoFile(FileItem: TFileItem);
begin
  case FileItem.FileInfo.Action of
    faIncrease,
    faDecrease,
    faDirector,
    faHakzaa,
    faHaavara,
    faHamara,
    faDoh,
    faStockHolderInfo,
    faDirectorInfo,
    faManager,
    faSignName,
    faEngName,
    faAddress,
    faTakanon:     TChanges.UndoFile(FileItem);
    faNumber:      TfmNumber.EvUndoFile(FileItem);
    faCompanyName: TfmBlankName.EvUndoFile(FileItem);
    faZipCode:     TfmBlankZip.EvUndoFile(FileItem);
  else
    Raise Exception.Create('������ �� ������ ���� ��� �� : '+ IntToStr(Byte(FileItem.FileInfo.Action)));
  end;
end;

function TfmMain.CheckKey(Key: Integer): Boolean;
begin
  Result:= Abs(Abs(GetVolumeSerNum) * CaptionToInt('��� ����') * Trunc(GetVersion)) = Key;
end;

function TfmMain.GetRegistered: Boolean;
begin
  Result:= CheckKey(RegNum);
end;

function TfmMain.SetRegistered(RegNum: Integer): Boolean;
var
  Registry: TRegIniFile;
begin
  if Not Registered then
  begin
    Registry:= TRegIniFile.Create(KeyNameToUse);
    Registry.WriteInteger('Permitions', 'RegKey', RegNum);
    Registry.Free;
    Result:= Registered;
  end
  Else
    Result:= True;
end;

function TfmMain.GetVolumeSerNum: Integer;
var
  Temp1, Temp2: Integer;
begin
  GetVolumeInformation('c:\', nil, 0, @Result, Temp1, Temp2, nil, 0);
  If Result = 0 Then
    Result := 1234567;
  Result := Result * RegChange;
end;

function TfmMain.GetRegNum: Integer;
var
  Registry: TRegIniFile;
begin
  Registry:= TRegIniFile.Create(KeyNameToUse);
  try
    Result:= Registry.ReadInteger('Permitions','RegKey',0);
  finally
    Registry.Free;
  end;
end;

function TfmMain.GetHelpPath: String;
var
  Registry: TRegIniFile;
begin
  Registry := TRegIniFile.Create(KeyNameToUse);
  Result := Registry.ReadString('Path','Help', ExtractFilePath(ParamStr(0)) + 'Help\');
  If Result[Length(Result)] <> '\' Then
    Result := Result + '\';
  Registry.Free;
end;

function TfmMain.GetVersion: Double;
var
  Registry: TRegIniFile;
begin
  Registry:= TRegIniFile.Create(KeyNameToUse);
  Result:= Registry.ReadInteger('VersionInfo','Version', C_CURRENT_VER) / 100;
  Registry.Free;
end;

procedure TfmMain.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F1 then
    ShowHelp(HelpContext);
end;

procedure TfmMain.CheckForUpdate;
var
  RegStatus: Boolean;
  Registry: TRegIniFile;
  CurrVerAsDouble: double;
  PrevVer: double;
  OldVersion: double;
begin
  RegStatus := Registered;
  PrevVer := (C_PREV_VERSION / 100);
  CurrVerAsDouble := (C_CURRENT_VER / 100);
  OldVersion := GetVersion;

  if (OldVersion < PrevVer) then
  begin
    MessageDlg('�� ���� ����� ����� ' + Float2Str(OldVersion, 2) + ' .',
      mtError, [mbOk], 0);
    Close;
    Exit;
  end;

  if (OldVersion < CurrVerAsDouble) then
  begin
    Registry:= TRegIniFile.Create(KeyNameToUse);
    Registry.WriteInteger('VersionInfo', 'Version', C_CURRENT_VER);
    Registry.Free;

    if RegStatus then
      if SetRegistered(Abs(Abs(GetVolumeSerNum) * CaptionToInt('��� ����') * Trunc(GetVersion))) then
      begin
        fmDialog2.Panel1.Caption:= '������ ������ ������';
        fmDialog2.Show;
      end;
  end;
end;

procedure TfmMain.FormShow(Sender: TObject);

  procedure AutoBackUp;

    function DateToBackUp(Date: TDateTime): String;
    var
      Year,Month,Day : word;
      function adZ(inp:string):string;
      begin
        if length(inp) = 1 then
          adZ:='0' + inp
        else
          adZ:=inp;
      end;
    begin
      try
        DecodeDate(Date,Year,Month,Day);
        Result := adZ(IntToStr(Day)) + '-' + adZ(IntToStr(Month)) + '-' + adZ(IntToStr(Year mod 1000));
      except
        on EconvertError do
          Result:='01-01-00';
      end;
    end;

  var
    FileName: String;
    Dir: String;
    LogFileName:string;
  begin
    LogFileName := GetLogFileName;
    WriteLog('Check if back-up needed', LogFileName);
    Dir := ExtractFilePath(ParamStr(0)) + 'BackUp\';
    FileName := DateToBackUp(Now) + '.rbk';
    if (UserOptions.BackUpEvery > 0) and (Not FileExists(Dir+FileName)) and
       ((UserOptions.LastBackUp+UserOptions.BackUpEvery) <= Round(Now)) Then
    begin
      WriteLog('BackUp Needed', LogFileName);
      Application.CreateForm(TfmBackUp, fmBackUp);
      Application.ProcessMessages;
      fmBackUp.BackUpExecute(Dir + FileName);
      fmBackUp.Close;
      UserOptions.LastBackUp := Round(Now);
      WriteLog('BackUp Done', LogFileName);
    end
    else
      WriteLog('BackUp not needed', LogFileName);
  end;

type
  TRegData = Record
    FirstUseDate,
    LastUsed: Integer;
  end;
var
  Registry: TRegIniFile;
  RegData: TRegData;
  LogFileName:string;
begin
  LogFileName:=GetLogFileName;
  If not Registered Then
    begin
      Registry:= TRegIniFile.Create(KeyNameToUse+'\VersionInfo');
      RegData.FirstUseDate:= Trunc(Date);
      RegData.LastUsed:= Trunc(Date);
      try
        If Registry.ValueExists('Update') Then
          Registry.ReadBinaryData('Update', RegData, SizeOf(RegData));
      except
      end;

      Application.CreateForm(TAboutBox, AboutBox);
      AboutBox.ShowModal;
      AboutBox.Free;

      If Trunc(Date)< RegData.LastUsed Then
        Inc(RegData.LastUsed)  // If User Changed the system time
      Else
        RegData.LastUsed:= Trunc(Date);

      If (RegData.LastUsed > (RegData.FirstUseDate + 21)) and (not Registered) then
        try
          RegData.FirstUseDate:= StrToInt(ParamStr(1));
          RegData.LastUsed:= Trunc(Date);
          If (RegData.LastUsed > (RegData.FirstUseDate + 21)) then
            raise Exception.Create('');
        except
          RegData.LastUsed:= Trunc(EncodeDate(2000,01,01));
          RegData.FirstUseDate:= Trunc(EncodeDate(1900, 01,12));
          MessageBox(Handle, '�� ���� ������ ������ ��� ���� ��� �����.' + #13
                             + '������ ������ �� ������ ���'' ������ ���. ' + HPSPhone,
                             '����� �.�.�', MB_RIGHT or MB_RTLREADING or MB_OK or MB_ICONHAND);
          Close;
        end;

      Registry.WriteBinaryData('Update', RegData, SizeOf(RegData));
      Registry.Free;
    end;
  {<--}
  try
    StrToFloat('123.45');
  except
    MessageDlg('                      !!!��� ��' +
         #13 +
         #13 + '����� ����� ������� ������ ����' +
         #13 + '   ����� �� ����� ����� �� ������' +
         #13 + '               123.45' + ' :������ ������� ���� ' +
         #13 + '        ���� ���� ����� ���� ����� ��' +
         #13 + '            ''����� ����� �� ��� ����� ���' +
         #13 + '                                                .' + HPSPhone
             ,mtError, [mbOk], 0);
  end;

  try
    StrToDate('31/12/1997');
  except
    MessageDlg('                      !!!��� ��'+
         #13+
         #13+'����� ������ ������� ������ ����'+
         #13+'   ����� �� ����� ������ �� ������'+
         #13+'      .dd/mm/yyyy' +' :������ ������� ���� '+
         #13+'         ���� ���� ����� ���� ����� ��'+
         #13+'             ''����� ����� �� ��� ����� ���'+
         #13+'                                                .' + HPSPhone, mtError, [mbOk], 0);
  end;

  WriteLog('Check LocalShare', LogFileName);
  if CheckBdeBadSettings then
    begin
      WriteLog( #9 + 'Local Share = FALSE', LogFileName);
      if Application.MessageBox( PChar('������ ������ ����� ����� ����� Local Share' + #13#10 +
                                       '����� ����� �� ���� �������.' + #13#10 +
                                       '���� ��� �� ������� ������ �� ������ �����.' + #13#10 +
                                       '�� ������ ���'' ������ ������ '+HPSPhone+ #13#10 + #13#10 +
                                       '��� ��� ��� ������ �� ������?'), '�����!',
                                        MB_YESNO + MB_ICONEXCLAMATION + MB_RIGHT + MB_RTLREADING ) = IDNO then
        Close;
    end
  else
    WriteLog( #9 + 'Local Share = TRUE', LogFileName);
  AutoBackUp;
end;

procedure TfmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  SearchRec: TSearchRec;
  Error: Integer;
  I: Integer;
begin
  Action := caFree;
  For I := 0 To MDIChildCount - 1 do
  begin
    If Assigned(MDIChildren[I].OnClose) Then
      MDIChildren[I].OnClose(Sender, Action);
    if Action = caNone Then
      Exit;
  end;

  try
    Error:= FindFirst(Data.taName.Database.Directory + 'Temp*.*', faAnyFile, SearchRec);
    While Error = 0 do
    begin
      DeleteFile(Data.taName.Database.Directory+SearchRec.Name);
      Error := FindNext(SearchRec);
    end;
  except
  end;
end;

function TfmMain.RegChange: Integer;
var
  Registry: TRegIniFile;
begin
  Registry := TRegIniFile.Create(KeyNameToUse);
  Result := Registry.ReadInteger('Update', 'Mode', 1);
  Registry.Free;
end;

function TfmMain.DemoMode: Boolean;
var
  Registry: TRegIniFile;
begin
  Registry:= TRegIniFile.Create(KeyNameToUse);
  Result:= Registry.ReadBool('Permitions', 'DemoMode', False);
  Registry.Free;
end;

procedure TfmMain.N37Click(Sender: TObject);
begin
  If MDIChildCount > 1 Then
    Exit;

  fmDialog.memo.Clear;
  fmDialog.memo.Lines.Add('��� ��');
  fmDialog.memo.Lines.Add('����� �� ����� �� �� ������');
  fmDialog.memo.Lines.Add('������ �� ����� �������');
  fmDialog.memo.Lines.Add('��� ������ ������?');
  fmDialog.Button3.Visible := False;
  fmDialog.ShowModal;
  fmDialog.Button3.Visible := True;
  if fmDialog.DialogResult = drYes Then
  begin
    ExecuteFile('idkun.exe', '', ExtractFilePath(ParamStr(0)), SW_SHOWDEFAULT);
    Close;
  end;
end;

procedure TfmMain.FormDestroy(Sender: TObject);
begin
  UserOptions.Free;
end;

{Special installation - Lock Up on 01/08/2003 -->}
//==============================================================================
function TfmMain.GetExpired : Boolean;
var
  RegInfo : TRegistry;
  ExpiryData : Boolean;
begin
  RegInfo := TRegistry.Create;
  RegInfo.OpenKey(RashamRegToUse, True);
  if RegInfo.ValueExists('HpsInfo') then
  begin
      RegInfo.ReadBinaryData('HpsInfo', ExpiryData, SizeOf(ExpiryData));
      Result := ExpiryData;
  end
  else
    Result := False;

  RegInfo.Free;
end;

//==============================================================================
procedure TfmMain.SetExpired(NewExpired : Boolean);
var
  RegInfo : TRegistry;
begin
  RegInfo := TRegistry.Create;
  try
    RegInfo.OpenKey( RashamRegToUse, True );
    RegInfo.WriteBinaryData( 'HpsInfo', NewExpired, SizeOf(NewExpired));
  finally
    RegInfo.Free;
  end;
end;

//==============================================================================
{function TfmMain.NoExpirationData : Boolean;
var
    RegInfo : TRegistry;
begin
    RegInfo := TRegistry.Create;
    RegInfo.OpenKey( RashamRegToUse, True );
    Result := NOT RegInfo.ValueExists( 'HpsInfo' );
    RegInfo.Free;
end;    }
//==============================================================================

procedure TfmMain.GetRegKeysToUse();
var
  RegInfo : TRegistry;
begin
  RegInfo := TRegistry.Create;
  try
    if (RegInfo.KeyExists(C_KEY_NAME)) then
    begin
      RashamRegToUse := C_RASHAM_REG;
      KeyNameToUse := C_KEY_NAME;
    end
    else
    begin
      RashamRegToUse := C_HEB_RASHAM_REG;
      KeyNameToUse := C_HEB_KEY_NAME;
    end;
  finally
    RegInfo.Free;                      //sts17674  - bug fix
  end;
end;
{<--}

procedure TfmMain.N42Click(Sender: TObject);
begin
//  Application.CreateForm(TfmInternet,fmInternet);
//  fmInternet.ShowModal;
end;

procedure TfmMain.nEngNameClick(Sender: TObject);
begin
  Application.CreateForm(TfmBlankEngName, fmBlankEngName)
end;

procedure TfmMain.nZipCodeClick(Sender: TObject);
begin
  Application.CreateForm(TfmBlankZip, fmBlankZip);
end;

procedure TfmMain.miExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmMain.miTakanonOutputClick(Sender: TObject);
begin
  Application.CreateForm(TfmTakanonRep, fmTakanonRep);
  fmTakanonRep.Execute(Now, -1);
end;

procedure TfmMain.miTakanonChangeClick(Sender: TObject);
begin
  Application.CreateForm(TfmTakanonChange, fmTakanonChange);
end;

/// TfmMain.GetExeVerion
/// propouse: extract file version from exe
/// changes for tasks: sts17674
function TfmMain.GetExeVerion: string;
var
  Filename: string;
  VerInfo: Pointer;
  Len, BufSize: Integer;
  Dest: PVSFixedFileInfo;
  V1, V2, V3, V4: Word;
begin
  result := 'N/A';
  Filename := Application.ExeName;
  V1 := 0;
  V2 := 0;
  V3 := 0;
  V4 := 0;
  BufSize := GetFileVersionInfoSize(PChar(Filename), Len);
  if BufSize > 0 then
  begin
    GetMem(VerInfo, BufSize);
    try
      if GetFileVersionInfo(PChar(Filename), 0, BufSize, VerInfo) then
        if VerQueryValue(VerInfo, '\', Pointer(Dest), Len) then
        begin
          V1 := Dest^.dwFileVersionMS shr 16;
          V2 := Dest^.dwFileVersionMS and $FFFF;
          V3 := Dest^.dwFileVersionLS shr 16;
          V4 := Dest^.dwFileVersionLS and $FFFF;
        end;
    finally
      FreeMem(VerInfo, BufSize);
    end;
    result := format('%d%d.%d.%d', [V1, V2, V3, V4]);
  end;
end;

//Moshe 27/01/2018
procedure TfmMain.FileVersion;
begin
  Self.Caption := Self.Caption + ' ���� ' + ExeVersion;
end;

end.
