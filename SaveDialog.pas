unit SaveDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  LoadDialog, HebForm, StdCtrls, HGrids, SdGrid, HEdit, ExtCtrls, HLabel,
  DataObjects, BlankTemplate;

type
  TfmSaveDialog = class(TfmLoadDialog)
    procedure btOkClick(Sender: TObject);
    procedure edFileNameKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sdNamesSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
  private
    { Private declarations }
    FFileInfo: TFileInfo;
  public
    { Public declarations }
    function Execute(Sender: TfmBlankTemplate; var FilesList: TFilesList; FileInfo: TFileInfo): TFileItem;
  end;

var
  fmSaveDialog: TfmSaveDialog;

implementation

uses dialog, cSTD;

{$R *.DFM}

function TfmSaveDialog.Execute(Sender: TfmBlankTemplate; var FilesList: TFilesList; FileInfo: TFileInfo): TFileItem;
begin
  FSender:= Sender;
  btDelete.Visible:= FileInfo.Draft;
  If FileInfo.Draft Then
    Caption:= '����� ���� �����'
  Else
    Caption:= '����� ���� ����';
  FFileInfo:= FileInfo;
  FFilesList:= FilesList;
  FillData;
  FileItem:= Nil;
  edFileName.Text:= Sender.FileName;
  edFileName.SelectAll;
  ShowModal;
  FilesList:= FFilesList;
  Result:= FileItem;
end;

procedure TfmSaveDialog.btOkClick(Sender: TObject);
var
  Line: Integer;
  OldFile: TFileItem;
  TempFileList: TFilesList;
  I: Integer;
begin
  If RemoveSpaces(edFileName.Text, rmBoth)='' Then
    Exit;
  Line:= sdNames.Cols[0].IndexOf(edFileName.Text);
  If Line <> -1 Then
    begin
      if btDelete.Visible Then
        begin
          fmDialog.ActiveControl:= fmDialog.Button1;
          fmDialog.Button3.Visible:= False;
          fmDialog.memo.lines.Clear;
          fmDialog.memo.lines.add('��� ��');
          fmDialog.memo.lines.add('�� ����� �� ��� ���� ����� �������');
          fmDialog.memo.lines.add('��� ��� ��� ����� ��� �� ?');
          fmDialog.ShowModal;
          fmDialog.Button3.Visible:= True;
          if fmDialog.DialogResult=drYes then
            begin
              sdNames.Row:= Line;
              btDeleteClick(btOk);
            end;
        end
      Else
        begin
          fmDialog.ActiveControl:= fmDialog.Button3;
          fmDialog.Caption:= '�����';
          fmDialog.memo.lines.Clear;
          fmDialog.memo.lines.add('��� ��');
          fmDialog.memo.lines.add('�� ����� �� ��� ���� ����� �������');
          fmDialog.Button1.Visible:= false;
          fmDialog.Button2.Visible:= False;
          fmDialog.Button3.Caption:= '�����';
          fmDialog.ShowModal;
          fmDialog.Button3.Caption:= '�����';
          fmDialog.Button1.Visible:= True;
          fmDialog.Button2.Visible:= True;
          fmDialog.Caption:= '����';
        end;
    end
  Else
    begin
      FFileInfo.FileName:= edFileName.Text;
      FFilesList.Add(FFileInfo);
      FileItem:= TFileItem.Create(FFilesList, FFileInfo);

      If (FSender.FileName <> '') and FSender.FileIsDraft and (not FFileInfo.Draft) Then  // If Saving a draft file as non-draft
        begin
          TempFileList:= TFilesList.Create(FFileInfo.CompanyNum, FFileInfo.Action, True);
          try
            If TempFileList.FileExists(FSender.FileName, I) Then
              begin
                OldFile:= TempFileList.Items[I];
                try
                  FSender.DeleteFile(OldFile);                                                // Delete a draft file
                finally
                  OldFile.Free;
                end;
              end;
          finally
            TempFileList.Free;
          end;
        end;

      FSender.FileName:= FFileInfo.FileName;
      FSender.FileIsDraft:= FFileInfo.Draft;
      Close;
    end;
end;

procedure TfmSaveDialog.edFileNameKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = #13 Then
    btOk.Click;
end;

procedure TfmSaveDialog.FormShow(Sender: TObject);
begin
  inherited;
  edFileName.SetFocus;
end;

procedure TfmSaveDialog.FormCreate(Sender: TObject);
begin
  FileItem := nil;
  inherited;
end;

procedure TfmSaveDialog.sdNamesSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
begin
  inherited;
  if (edFileName.Text = '') then
    edFileName.Text := sdNames.Cells[0,Row];
end;

end.
