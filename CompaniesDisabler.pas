unit CompaniesDisabler;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, HLabel,
  HGrids, SdGrid, DataObjects;

type
  TfmCompaniesDisabler = class(TfmTemplate)
    DisabledGrid: TSdGrid;
    EnabledGrid: TSdGrid;
    btDisable: TButton;
    btEnable: TButton;
    Shape59: TShape;
    HebLabel41: THebLabel;
    HebLabel1: THebLabel;
    Shape1: TShape;
    btChangeInfo: TButton;
    procedure btDisableClick(Sender: TObject);
    procedure DisabledGridEnter(Sender: TObject);
    procedure btChangeInfoClick(Sender: TObject);
  private
    { Private declarations }
    function GetCompany(SourceGrid: TSdGrid; Companies: TCompanies): TCompany;
  protected
    procedure EvFillDefaultData; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
  public
    { Public declarations }
  end;

var
  fmCompaniesDisabler: TfmCompaniesDisabler;

implementation

{$R *.DFM}
uses Util, ChangeInfo, DataM;

const
  RishumStr= '���� ������';

procedure TfmCompaniesDisabler.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
  Companies: TCompanies;
  I: Integer;
begin
  // Inherited;
  If Not Extended Then
    Exit;

  case Page of
    0: begin
         Companies:= TCompanies.Create(EncodeDate(2900,01,01), False, False);
         For I:= 0 To Companies.Count-1 Do
           begin
             If Companies.Names[I] = DisabledGrid.Cells[0, DisabledGrid.Row] Then
               begin
                 ErrorAt:= DisabledGrid;
                 raise Exception.Create('�� ����� ��� ���� ����� ����');
               end;

             If (Companies.Numbers[I] <> RishumStr) And
                (Companies.Numbers[I] = DisabledGrid.Cells[1, DisabledGrid.Row]) then
               begin
                 ErrorAt:= DisabledGrid;
                 raise Exception.Create('���� ����� ��� ���� ����� ����');
               end;
           end;
         Companies.Free;
       end;
  end;
end;

procedure TfmCompaniesDisabler.EvFillDefaultData;
var
  Companies: TCompanies;
  I: Integer;
begin
  // inherited;

  // Fill Enabled Companies
  Companies:= TCompanies.Create(EncodeDate(2900, 01,01), False, False);
  EnabledGrid.RowCount:= Companies.Count;
  EnabledGrid.Rows[0].Clear;
  For I:= 0 To Companies.Count-1 do
    begin
      EnabledGrid.Cells[0,I]:= Companies.Names[I];
      if (Companies.Numbers[I] = '-1') or (Companies.Numbers[I] = '����') Then
        EnabledGrid.Cells[1,I]:= RishumStr
      Else
        EnabledGrid.Cells[1,I]:= Companies.Numbers[I];
    end;
  Companies.Free;
  //SortGrid(EnabledGrid, 0,-1, stString, stString);
  SortGrid(EnabledGrid,'0,STRING,UP');
  EnabledGrid.Repaint;

  // Fill Disabled Companies
  Companies:= TCompanies.Create(EncodeDate(2900, 01,01), True, False);
  DisabledGrid.RowCount:= Companies.Count;
  DisabledGrid.Rows[0].Clear;
  For I:= 0 To Companies.Count-1 do
    begin
      DisabledGrid.Cells[0,I]:= Companies.Names[I];
      if (Companies.Numbers[I] = '-1') or (Companies.Numbers[I] = '����') Then
        DisabledGrid.Cells[1,I]:= RishumStr
      Else
        DisabledGrid.Cells[1,I]:= Companies.Numbers[I];
    end;
  Companies.Free;
  //SortGrid(DisabledGrid, 0, -1, stString, stString);
  SortGrid(DisabledGrid,'0,STRING,UP');
  DisabledGrid.Repaint;
end;

procedure TfmCompaniesDisabler.btDisableClick(Sender: TObject);
Var
  Companies: TCompanies;
  Company: TCompany;
begin
  try
    If Sender= btDisable Then
      begin
        Companies:= TCompanies.Create(EncodeDate(2900, 01,01), False, False);
        Company:= GetCompany(EnabledGrid, Companies);
      end
    Else
      begin
        if CheckErrors(False, True, False) then
          Exit;
        Companies:= TCompanies.Create(EncodeDate(2900, 01,01), True, False);
        Company:= GetCompany(DisabledGrid, Companies);
      end;

    If Company <> nil then
      begin
        If Sender=btDisable Then
          Company.Disabled:= True
        Else
          Company.Disabled:= False;
        Company.Free;
      end;

    Companies.Free;
  finally
  end;
    
  EvFillDefaultData;
  if Sender = btDisable Then
    EnabledGrid.SetFocus
  Else
    DisabledGrid.SetFocus;
end;

procedure TfmCompaniesDisabler.DisabledGridEnter(Sender: TObject);
begin
  btDisable.Enabled:= Sender = EnabledGrid;
  btEnable.Enabled:= Sender = DisabledGrid;
  btChangeInfo.Enabled:= Sender = DisabledGrid;
end;

function TfmCompaniesDisabler.GetCompany(SourceGrid: TSdGrid; Companies: TCompanies): TCompany;
var
  I: Integer;
begin
  Result:= nil;
  For I:= 0 To Companies.Count-1 do
    If (SourceGrid.Cells[0, SourceGrid.Row] = Companies.Names[I]) And
      ((SourceGrid.Cells[1, SourceGrid.Row] = Companies.Numbers[I]) or
      ((SourceGrid.Cells[1, SourceGrid.Row] = RishumStr) And
       ((Companies.Numbers[I] = '-1') or (Companies.Numbers[I] = '����')))) Then
      begin
        Result:= Companies.Items[I];
        Break;
      end;
end;

procedure TfmCompaniesDisabler.btChangeInfoClick(Sender: TObject);
var
  Companies: TCompanies;
  Company: TCompany;
begin
  try
    Application.CreateForm(TfmChangeInfo, fmChangeInfo);
    Companies:= TCompanies.Create(EncodeDate(2900, 01,01), True, False);
    Company:= GetCompany(DisabledGrid, Companies);
    If Company<> nil Then
      fmChangeInfo.Execute(Company);
    Companies.Free;
  finally
  end;

  EvFillDefaultData;
  DisabledGrid.SetFocus;
end;

end.
