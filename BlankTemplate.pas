{
MODIFICATION HISTORY
DATE         BY    TASK      VERSION     DESCRIPTION
}
unit BlankTemplate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, StdCtrls, Mask, HebForm, ExtCtrls, HLabel, Buttons,
  ComCtrls, DataObjects, HGrids, SdGrid, BDE, Db, DBTables;

type
  TfmBlankTemplate = class(TfmTemplate)
    Shape66: TShape;
    Shape65: TShape;
    HebLabel33: THebLabel;
    Shape61: TShape;
    Shape64: TShape;
    HebLabel32: THebLabel;
    Shape37: TShape;
    Shape36: TShape;
    HebLabel19: THebLabel;
    Shape1: TShape;
    Shape2: TShape;
    HebLabel1: THebLabel;
    Shape3: TShape;
    Shape4: TShape;
    HebLabel2: THebLabel;
    lNumber: THebLabel;
    lName: THebLabel;
    e13: TMaskEdit;
    e14: TMaskEdit;
    btBrowse: TButton;
    btShow: TSpeedButton;
    procedure btBrowseClick(Sender: TObject);
    procedure btShowClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure e13Exit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure B1Click(Sender: TObject);
  private
    { Private declarations }
    LoadingBlank: Boolean;
    FShowStyle: TActivityFilter;
    FNotifyDate: TDateTime;
    FDecisionDate: TDateTime;
    FFileAction: TFileAction;
    FCompany: TCompany;
    FDoNotClear: Boolean;

    procedure SetDate(Index: Integer; Value: TDateTime);
    function GetDate(Index: Integer): TDateTime;
  protected
    // Properties
    property DoNotClear: Boolean read FDoNotClear;
    property NotifyDate: TDateTime index 1 read GetDate write SetDate;
    property DecisionDate: TDateTime index 2 read GetDate write SetDate;
    property ShowStyle: TActivityFilter read FShowStyle write FShowStyle default afActive;
    property Company: TCompany read FCompany write FCompany;
    // Events
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvDataLoaded; virtual;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft:boolean); override;
    // Bullshits
    procedure GoToCurrRecord; override;
    procedure ForceDecisionDate(Value: TDateTime);
    procedure ManagerChange(Zeut: string; ChangeName, changeZeut: boolean; NewName, NewZeut: string; FileItem: TFileItem);
  public
    { Public declarations }
    property FileAction: TFileAction read FFileAction write FFileAction;
    procedure DeleteFile(FileItem: TFileItem); virtual; abstract;
  end;


var
  fmBlankTemplate: TfmBlankTemplate;


implementation

uses utils2,Companies, Util, Dialog, Main, BookStock, DataM;

{$R *.DFM}

procedure TfmBlankTemplate.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
  History: THistory;
begin
  CheckDate(Silent, e13);
  CheckDate(Silent, e14);
  Inherited;
  If Not Extended then
    Exit;

  History:= THistory.Create;
  try
    If (FileAction <> faZipCode) AND (Not History.IsActionLast(Company.RecNum, FileAction, DecisionDate)) then
    begin
      if not Silent then
        with e13 do
          if Color = clWindow then
            Color:= clBtnFace
          Else
            Color:= clWindow;
      ErrorAt:= e13;
      raise Exception.Create('����� ������ ����� ���� ' + DateToLongStr(DecisionDate));
    end;
  finally
    History.Free;
  end;
end;

function TfmBlankTemplate.GetDate(Index: Integer): TDateTime;
begin
  if Index = 1 then
    try
      Result := StrToDate(e14.Text);
      FNotifyDate := Result;
    except
      Result := FNotifyDate;
    end
  else
    try
      Result := StrToDate(e13.Text);
      FDecisionDate := Result;
    except
      Result := FDecisionDate;
    end;
end;

procedure TfmBlankTemplate.EvFocusNext(Sender: TObject);
begin
  If (Sender = e13) and (e14.Visible) Then
    e14.SetFocus;
end;

procedure TfmBlankTemplate.SetDate(Index: Integer; Value: TDateTime);
var
  TempCompany: TCompany;
begin
  case Index of
  1: begin
       FNotifyDate := Value;
       e14.Text := DateToLongStr(Value);
     end;
  2: begin
       if (Company.CompanyInfo <> nil) and (Trunc(Company.CompanyInfo.RegistrationDate) > Trunc(Value)) then
       begin
         DecisionDate := Company.CompanyInfo.RegistrationDate;
         Exit;
       end;
       FDecisionDate := Value;
       e13.Text := DateToLongStr(Value);
       if Assigned(Company) and (Company.ShowingDate <> Value) then
       begin
         if (FileAction in MultiActions) or (Self is TfmBookStocks) or (FileAction = faDoh) then
           Value := Value + 1;

         TempCompany := TCompany.Create(Company.RecNum, Value, False);
         Company.Free;
         Company := TempCompany;
         LoadingBlank := True;
         EvDataLoaded;
         LoadingBlank := False;
       end;
     end;
  end;
end;

procedure TfmBlankTemplate.ForceDecisionDate(Value: TDateTime);
begin
  FDecisionDate := Value;
  e13.Text := DateToLongStr(Value);
end;

procedure TfmBlankTemplate.btBrowseClick(Sender: TObject);
var
  TempCompany: TCompany;
begin
  inherited;
  If DATAChanged Then
  begin
    fmDialog.memo.lines.Clear;
    fmDialog.memo.lines.add('��� ��');
    fmDialog.memo.lines.add('����� ��� ����� ���� ������ ������.');
    fmDialog.memo.lines.add('��� ����� �� ������� ?');
    fmDialog.ShowModal;
    if fmDialog.DialogResult = drYES then
      begin
        If CheckErrors(False,True, False) Then
          Exit
        Else
          b1.Click;
      end;
    If fmDialog.DialogResult = drCancel Then
      Exit;
  end;
 // EvLoadData;
  TempCompany:= fmCompanies.Execute(Caption, ShowStyle);
  If TempCompany <> Nil Then
  begin
    If Company<> nil Then
      Company.Free;
    Company:= TempCompany;
    EvDataLoaded;
  end;
end;

procedure TfmBlankTemplate.EvDataLoaded;
begin
  If Not LoadingBlank Then
  begin
    FileIsDraft:= True;
    NotifyDate:= Now;
    DecisionDate:= Now;
  end;
  lName.Caption:= Company.Name;
  lNumber.Caption:= Company.Zeut;
end;

procedure TfmBlankTemplate.btShowClick(Sender: TObject);
begin
  inherited;
  fmMain.ShowCompanyInfo(Company, DecisionDate);
end;

procedure TfmBlankTemplate.FormCreate(Sender: TObject);
begin
  inherited;
  if Tag < 0 then
    Exit;

  btBrowse.Click;
  if Company = nil then
    Close;
end;

procedure TfmBlankTemplate.FormShow(Sender: TObject);
begin
  inherited;
  if CanFocusOnControl(e13) then
    e13.SetFocus;
end;

procedure TfmBlankTemplate.e13Exit(Sender: TObject);
var
  date: TDateTime;
begin
  FDoNotClear := True;
  try
    inherited;
    if DataChanged then
    try
      date := StrToDate(e13.Text);
      if (DecisionDate <> date) then
        DecisionDate := date;
    except
    end;
  finally
    FDoNotClear := False;
  end;
end;

procedure TfmBlankTemplate.FormDestroy(Sender: TObject);
begin
  inherited;
  If Assigned(Company) Then
    Company.Free;
end;

procedure TfmBlankTemplate.B1Click(Sender: TObject);
var
  TempCompany: TCompany;
begin
  FDoNotClear:= True;
  try
    inherited;
    FDoNotClear:= True;
    TempCompany:= TCompany.Create(Company.RecNum, Company.ShowingDate, False);
    Company.Free;
    Company:= TempCompany;
    LoadingBlank:= True;
    EvDataLoaded;
    LoadingBlank:= False;
  finally
    FDoNotClear:= False;
  end;
end;

procedure TfmBlankTemplate.GoToCurrRecord;
begin
end;

// Moshe 28/01/2019
procedure TfmBlankTemplate.ManagerChange(Zeut: string; ChangeName, changeZeut: boolean; NewName, NewZeut: string; FileItem: TFileItem);
var
  Manager: TManager;
begin
  if (Company.Managers <> nil) then
  begin
    Manager := Company.Managers.FindByZeut(Zeut) as TManager;
    if (Manager <> nil) then
    begin
      if (ChangeName) then
        Manager.Name := NewName;
      if (ChangeZeut) then
        Manager.Zeut := NewZeut;
      Manager.ExSaveData(FileItem, Company);
    end;
  end;
end; // TfmBlankTemplate.ManagerChange

end.

