unit RishumQuickRequest;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,  ShellExt,
  HEdit, StdCtrls, Buttons, ExtCtrls, HebForm,  DataObjects, Util, PrintRishumQuickSubmit;

type
  TfmRishumQuickRequest = class(TForm)
    HebForm1: THebForm;
    pnlTitle: TPanel;
    pnlMagish: TPanel;
    pnlCompany: TPanel;
    pnlButtons: TPanel;
    bnCancel: TBitBtn;
    bnPrint: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    edEnglishName: THebEdit;
    edHebrewName: THebEdit;
    edFamilyName: THebEdit;
    edForename: THebEdit;
    edFax: THebEdit;
    edEMail: THebEdit;
    procedure bnCancelClick(Sender: TObject);
    procedure bnPrintClick(Sender: TObject);
  private
    { Private declarations }
    FileName: string;
    Company :TCompany;
    function GatherData:TQuickSubmissionData ;
  public
    { Public declarations }
    procedure Open(LoadingFileName: string; LoadingCompany :TCompany);
  end;

var
  fmRishumQuickRequest: TfmRishumQuickRequest;

implementation

{$R *.DFM}

procedure TfmRishumQuickRequest.Open(LoadingFileName: string; LoadingCompany :TCompany);
var
 CompanyNames: TSplitCompanyName;
 iPos :integer;
begin
   Self.Company := LoadingCompany;
   Self.FileName := LoadingFileName;

   if (Company.CompanyInfo.EnglishName = '') then
   begin
      CompanyNames := SplitCompanyName(Company.Name);
      Self.edEnglishName.Text := CompanyNames.EnglishName;
      Self.edHebrewName.Text := CompanyNames.HebrewName;
   end else if (Company.CompanyInfo <> nil ) then
   begin
      Self.edHebrewName.Text := Company.Name;
      Self.edEnglishName.Text := Company.CompanyInfo.EnglishName;
   end;


   iPos :=Pos(' ',Company.Magish.Name);
   Self.edForename.Text := Copy(Company.Magish.Name,0, iPos);
   Self.edFamilyName.Text := Copy(Company.Magish.Name, iPos, Length(Company.Magish.Name));

   ShowModal;
end;

procedure TfmRishumQuickRequest.bnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfmRishumQuickRequest.bnPrintClick(Sender: TObject);
begin

    Application.CreateForm(TQRPrintRishumQuickSubmit, QRPrintRishumQuickSubmit);
    QRPrintRishumQuickSubmit.PrintExecute( FileName, Company, GatherData());
    QRPrintRishumQuickSubmit.Free;

    Close; //on printing complete, close this screen
end;


function TfmRishumQuickRequest.GatherData : TQuickSubmissionData ;
var
   QuickSubmissionData: TQuickSubmissionData;
begin
   QuickSubmissionData.HebrewName := edHebrewName.Text ;
   QuickSubmissionData.EnglishName:=   edEnglishName.Text;
   QuickSubmissionData.MagishEMail  := edEMail.Text;
   QuickSubmissionData.MagishFax :=    edFax.Text;
   QuickSubmissionData.MagishForename := edForename.Text;
   QuickSubmissionData.MagishFamilyname := edFamilyname.Text;

   Result :=  QuickSubmissionData;
end;
end.
