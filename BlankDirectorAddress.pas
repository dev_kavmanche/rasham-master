unit BlankDirectorAddress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, DataObjects, HGrids, SdGrid, HComboBx, HEdit, HChkBox;

type
  TfmDirectorAddress = class(TfmBlankTemplate)
    Shape55: TShape;
    Shape5: TShape;
    Shape6: TShape;
    HebLabel3: THebLabel;
    Shape7: TShape;
    HebLabel4: THebLabel;
    Shape8: TShape;
    Shape44: TShape;
    Shape46: TShape;
    HebLabel24: THebLabel;
    Shape47: TShape;
    Shape48: TShape;
    HebLabel25: THebLabel;
    Shape49: TShape;
    Shape50: TShape;
    HebLabel26: THebLabel;
    Shape57: TShape;
    HebLabel30: THebLabel;
    Shape56: TShape;
    HebLabel29: THebLabel;
    Shape54: TShape;
    Shape21: TShape;
    Shape23: TShape;
    HebLabel52: THebLabel;
    pnDir: TPanel;
    Shape59: TShape;
    Shape78: TShape;
    Shape75: TShape;
    Shape72: TShape;
    Shape70: TShape;
    Shape69: TShape;
    Shape80: TShape;
    HebLabel40: THebLabel;
    HebLabel35: THebLabel;
    HebLabel34: THebLabel;
    Shape67: TShape;
    Shape68: TShape;
    Shape79: TShape;
    HebLabel37: THebLabel;
    Shape71: TShape;
    HebLabel36: THebLabel;
    Shape73: TShape;
    HebLabel38: THebLabel;
    HebLabel39: THebLabel;
    Shape77: TShape;
    Shape76: TShape;
    Shape74: TShape;
    HebLabel41: THebLabel;
    e412: THebEdit;
    e414: THebEdit;
    e417: THebComboBox;
    e416: TEdit;
    e413: TEdit;
    e418: THebComboBox;
    e415: THebEdit;
    e43: TMaskEdit;
    e44: TMaskEdit;
    e49: TEdit;
    e47: THebEdit;
    e410: THebComboBox;
    e411: THebComboBox;
    TempGrid4: TSdGrid;
    e48: THebEdit;
    e46b: TEdit;
    Shape9: TShape;
    Shape10: TShape;
    HebLabel5: THebLabel;
    Shape11: TShape;
    Shape12: TShape;
    HebLabel6: THebLabel;
    e32: THebComboBox;
    e33: THebComboBox;
    Shape13: TShape;
    Shape14: TShape;
    e39: THebEdit;
    cbChangeName: THebCheckBox;
    Shape15: TShape;
    Shape16: TShape;
    edZeut: THebEdit;
    cbZeut: THebCheckBox;
    procedure CriticalEditChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbChangeNameClick(Sender: TObject);
    procedure cbZeutClick(Sender: TObject);
  private
    { Private declarations }
    CurrentZeut: string;
    ZeutChange: boolean;
    procedure LoadEnum1(ObjectNum, NewNum: Integer);
    procedure LoadEnum2(ObjectNum, NewNum: Integer);
    procedure DeleteEnum1(ObjectNum, NewNum: Integer);
    procedure DeleteEnum2(ObjectNum, NewNum: Integer);
    procedure FillReadOnlyFields;
  protected
    function EvSaveData(Draft: Boolean): Boolean; override;
    function EvLoadData: Boolean; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvFillDefaultData; override;
    procedure EvDataLoaded; override;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmDirectorAddress: TfmDirectorAddress;

implementation

uses DataM, Util, cSTD, SaveDialog, LoadDialog, dialog2, dialog, MultipleDirectorsChoice;

{$R *.DFM}

var
  TempFileItem: TFileItem;

procedure TfmDirectorAddress.LoadEnum2(ObjectNum, NewNum: Integer);
var
  Taagid: TTaagid;
  I: Integer;
begin
  For I:= 0 To Company.Directors.Count-1 do
    If Company.Directors.Items[I].RecNum = ObjectNum Then
      begin
        pnDir.Visible:= True;
        Taagid:= TTaagid.Create(NewNum);
        e412.Text:= Taagid.Name;
        e413.Text:= Taagid.Zeut;
        e414.Text:= Taagid.Street;
        e415.Text:= Taagid.Number;
        e416.Text:= Taagid.Index;
        e417.Text:= Taagid.City;
        e418.Text:= Taagid.Country;
        Taagid.Free;
      end;
end;

procedure TfmDirectorAddress.LoadEnum1(ObjectNum, NewNum: Integer);
var
  Director: TDirector;
  Address: TDirectorAddress;
  I: Integer;
begin
  For I:= 0 To Company.Directors.Count-1 do
    If Company.Directors.Items[I].RecNum = ObjectNum Then
      begin
        Director:= Company.Directors.Items[I] as TDirector;
        e33.ItemIndex:= e33.Items.IndexOf(Director.Zeut);
        e32.ItemIndex:= e33.ItemIndex;
        Address:= TDirectorAddress.Create(NewNum);
        e46b.Text:= Address.Phone;
        e43.Text:= DateToLongStr(Director.Date1);
        e44.Text:= DateToLongStr(Director.Date2);
        e47.Text:= Address.Street;
        e48.Text:= Address.Number;
        e49.Text:= Address.Index;
        e410.Text:= Address.City;
        e411.Text:= Address.Country;
        e39.Text:= e32.Text;
        edZeut.Text:=e33.Text;
        cbZeut.Checked:=False;
        cbChangeName.Checked:= False;
        cbChangeNameClick(Self);
        Address.Free;
      end;
end;

function TfmDirectorAddress.EvLoadData: Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  TmpStr: String;
begin
  Result:= False;
  FileItem:= fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faDirectorInfo);
  If FileItem = Nil Then
    Exit;
  DecisionDate:= (FileItem.FileInfo.DecisionDate);
  NotifyDate:= (FileItem.FileInfo.NotifyDate);
  TempFileItem:= FileItem;
  Company.Directors.Filter:= afAll;
  pnDir.Visible:= False;
  FileItem.EnumChanges(ocDirector, -1, acAddress, LoadEnum1);
  FileItem.EnumChanges(ocDirector, -1, acTaagidAddress, LoadEnum2);
  If FileItem.LoadNameChange(TmpStr) Then
    begin
      cbChangeName.Checked:= True;
      cbChangeNameClick(Self);
      e39.Text:= TmpStr;
    end;
  FileItem.Free;
  FilesList.Free;
end;


procedure TfmDirectorAddress.DeleteEnum1(ObjectNum, NewNum: Integer);
var
  Address: TDirectorAddress;
  I: Integer;
begin
  For I:= 0 To Company.Directors.Count-1 do
    If Company.Directors.Items[I].RecNum = ObjectNum Then
      begin
        Address:= TDirectorAddress.Create(NewNum);
        Address.FileDelete(Company.Directors.Items[I] as TDirector, TempFileItem);
        Address.Free;
      end;
end;

procedure TfmDirectorAddress.DeleteEnum2(ObjectNum, NewNum: Integer);
var
  Taagid: TTaagid;
  I: Integer;
begin
  For I:= 0 To Company.Directors.Count-1 do
    If Company.Directors.Items[I].RecNum = ObjectNum Then
      begin
        Taagid:= TTaagid.Create(NewNum);
        Taagid.FileDelete(Company.Directors.Items[I] as TDirector, TempFileItem);
        Taagid.Free;
      end;
end;

procedure TfmDirectorAddress.DeleteFile(FileItem: TFileItem);
begin
  TempFileItem:= FileItem;
  If Not FileItem.FileInfo.Draft Then
    Raise Exception.Create('This file is not Draft at TfmDirectorAddres.DeleteFile');
  If (FileItem.FileInfo.Action <> faDirectorInfo) Then
    raise Exception.Create('Invalid file type at TfmDirectorAddress.DeleteFile');
  Company.Directors.Filter:= afAll;
  FileItem.EnumChanges(ocDirector, -1, acAddress, DeleteEnum1);
  FileItem.EnumChanges(ocDirector, -1, acTaagidAddress, DeleteEnum2);
  FileItem.DeleteFile;
end;

function TfmDirectorAddress.EvSaveData(Draft: Boolean): Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  FileInfo: TFileInfo;
  Director: TDirector;
  Address: TDirectorAddress;
  Taagid: TTaagid;
  Changes: TChanges;
  RecNum: Integer;
  i: integer;
  SelectedDirectors: TMultipleSelectRec;
  LastChange, bSaveStockHolders : boolean;
  SaveManagers, SaveStockHolders: TDialogResult;
begin
  Result := False;
  try
    if (cbZeut.Checked) And ((Trim(edZeut.Text)='') or (Trim(edZeut.Text)='0')) then
    begin
      Application.MessageBox(PChar('."�� ���� ����� ���� ���� ��� �� "0' + #10+#13 +
                                   '                       .��� ��� ���'),'���� �������' ,MB_OK);
      Exit;
    end;

    Address := TDirectorAddress.CreateNew( e47.Text, e48.Text, e49.Text, e410.Text, e411.Text, e46b.Text);
    if pnDir.Visible then
      Taagid := TTaagid.CreateNew(e414.Text, e415.Text, e416.Text, e417.Text, e418.Text, e412.Text, e413.Text)
    else
      Taagid := nil;
    FilesList := TFilesList.Create(Company.RecNum, faDirectorInfo, Draft);
    FileInfo := TFileInfo.Create;
    FileInfo.Action := faDirectorInfo;
    FileInfo.Draft := Draft;
    FileInfo.DecisionDate := DecisionDate;
    FileInfo.NotifyDate := NotifyDate;
    FileInfo.CompanyNum := Company.RecNum;
    FileItem := fmSaveDialog.Execute(Self, FilesList, FileInfo);
    if FileItem <> nil then
    begin
      Director := Company.Directors.FindByZeut(e33.Text) as TDirector;
      FileItem.FileInfo.Draft := True;
      Address.ExSaveData(FileItem, Director, DecisionDate);
      if Assigned(Taagid) then
        Taagid.ExSaveData(FileItem, Director, DecisionDate);
      if cbChangeName.Checked then
      begin
        RecNum :=FileItem.SaveNameChange(e39.Text,acNewName,ocDirector);
        if (not Draft) and (Trim(e39.Text) <> '') and (e39.Text <> Director.Name) then
        begin
          Changes := TChanges.Create(Company);
          try
            Changes.SaveChanges(ocDirector, Director.RecNum, acNewName, RecNum, DecisionDate, FileItem.FileInfo.RecNum);
          finally
            Changes.Free;
          end;
        end;
      end;
      //==> Tsahi : Zeut change
      if (cbZeut.Checked) then
      begin
        RecNum := FileItem.SaveNameChange(edZeut.Text, acNewZeut, ocDirector);
        if (not Draft) and (Trim(edZeut.Text) <> '') and (edZeut.Text <> Director.Zeut) then
        begin
          Changes := TChanges.Create(Company);
          try
            Changes.SaveChanges(ocDirector, Director.RecNum, acNewZeut, RecNum, DecisionDate, FileItem.FileInfo.RecNum);
          finally
            Changes.Free;
          end;
        end;
      end;

     //==>Tsahi : multiple directors save
      if not Draft then
      begin
        // Moshe 14/10/2018
        //Application.CreateForm(TfmMultipleDirectorsChoice,fmMultipleDirectorsChoice);
        fmMultipleDirectorsChoice := TfmMultipleDirectorsChoice.Create(Self);
        fmMultipleDirectorsChoice.ShowFull( Director.Zeut ,'',Director.RecNum ,diDirector, self.DecisionDate);
        if (fmMultipleDirectorsChoice.ModalResult = mrAbort) then
          Exit;

       //==>Tsahi: 20.6.05 : changed so as to ask second question of user (stockholder) whenever appropriate
        if fmMultipleDirectorsChoice.ResultList.PersonCount(diStockHolder)>0 Then
        begin
          fmDialog.memo.Clear;
          fmDialog.memo.Lines.Add('');
          fmDialog.memo.Lines.Add('������� �� ��� �� ��� ����� �����.');
          fmDialog.memo.Lines.Add('��� ����� �� ����� �� ���� �����?');
          fmDialog.Button3.Visible:= False;
          fmDialog.ShowModal;
          fmDialog.Button3.Visible:= True;
        end;
        SaveStockHolders:=fmDialog.DialogResult;

        //==>Tsahi: 28.6.05 : changed so as to ask third questions of user (managers) whenever appropriate
        if (fmMultipleDirectorsChoice.ResultList.PersonCount(diManager)>0) or (fmMultipleDirectorsChoice.ResultList.PersonCount(diSignName)>0) Then
        begin
          fmDialog.memo.Clear;
          fmDialog.memo.Lines.Add('');
          fmDialog.memo.Lines.Add('������� �� ��� �� ���"� �� ���� ���� �����.');
          fmDialog.memo.Lines.Add('��� ����� �� ����� �� ����"�/���� ����?');
          fmDialog.Button3.Visible:= False;
          fmDialog.ShowModal;
          fmDialog.Button3.Visible:= True;
        end;
        SaveManagers:=fmDialog.DialogResult;

        for i:=0 to fmMultipleDirectorsChoice.ResultList.Count-1 do
        begin
          if SaveStockHolders =drNo then
              PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.StockHolderNum:=0;
          if SaveManagers =drNo then
          begin
            PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.ManagerNum :=0;
            PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.SignNameNum :=0;
          end;
        end;
        //<==
        //<==
        if ((fmMultipleDirectorsChoice.ModalResult <> mrCancel) or  (fmDialog.DialogResult=drYes)) and (fmMultipleDirectorsChoice.ResultList.Count >= 1) then
        begin
          Screen.Cursor := crHourGlass;
          LastChange := False;
          for i := 0 to fmMultipleDirectorsChoice.ResultList.Count-1 do
          begin
            if i = fmMultipleDirectorsChoice.ResultList.Count-1 then
              LastChange := True;
            SelectedDirectors := (PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^);
            Director.SaveDataToAll(SelectedDirectors, Address, e39.Text, edZeut.Text, cbChangeName.Checked, cbZeut.Checked, LastChange, True,FileInfo);
            ManagerChange(Director.Zeut, cbChangeName.Checked, cbZeut.Checked, e39.Text, edZeut.Text, FileItem);
          end;
        end;
        //<==
        Address.SaveData(Director, DecisionDate, False, FileItem.FileInfo.RecNum);

        if Assigned(Taagid) Then
           Taagid.SaveData(Director, DecisionDate, False, FileItem.FileInfo.RecNum);

        CurrentZeut:=Director.Zeut; //Tsahi : Zeut change
        ZeutChange:=True;
        cbZeut.Checked:=false;
      end;

      fmDialog2.Show;
      Result := True;
    end
  finally
    Screen.Cursor := crDefault;
    FilesList.Free;
    FileItem.Free;
    Address.Free;
    Taagid.Free;

    if fmMultipleDirectorsChoice <> nil then
    begin
      fmMultipleDirectorsChoice.Free;
      fmMultipleDirectorsChoice := nil;
    end;
  end;
end; // TfmDirectorAddress.EvSaveData

procedure TfmDirectorAddress.EvFillDefaultData;
begin
  inherited;
  DATA.taCity.First;
  While Not DATA.taCity.EOF do
    begin
      e410.Items.Add(DATA.taCity.Fields[0].Text);
      e417.Items.Add(DATA.taCity.Fields[0].Text);
      DATA.taCity.Next;
    end;

  DATA.taCountry.First;
  While Not DATA.taCountry.EOF do
    begin
      e411.Items.Add(DATA.taCountry.Fields[0].Text);
      e418.Items.Add(DATA.taCountry.Fields[0].Text);
      DATA.taCountry.Next;
    end;
end;

procedure TfmDirectorAddress.FillReadOnlyFields;
var
  Director: TDirector;
begin
  Company.Directors.Filter:= afActive;
  Director:= Company.Directors.FindByZeut(e33.Text) as TDirector;
  try
    e39.Text:= Director.Name;
    edZeut.Text:=Director.Zeut;
    cbZeut.Checked:=False;
    cbChangeName.Checked:= False;
    cbChangeNameClick(Self);
    e46b.Text:= Director.Address.Phone;
    e43.Text:= DateToLongStr(Director.Date1);
    e44.Text:= DateToLongStr(Director.Date2);
    e47.Text:= Director.Address.Street;
    e48.Text:= Director.Address.Number;
    e49.Text:= Director.Address.Index;
    e410.Text:= Director.Address.City;
    e411.Text:= Director.Address.Country;
    pnDir.Visible:= Director.Taagid<>nil;
    If pnDir.Visible then
      begin
        e412.Text:= Director.Taagid.Name;
        e413.Text:= Director.Taagid.Zeut;
        e414.Text:= Director.Taagid.Street;
        e415.Text:= Director.Taagid.Number;
        e416.Text:= Director.Taagid.Index;
        e417.Text:= Director.Taagid.City;
        e418.Text:= Director.Taagid.Country;
      end;
  except end;
end;

procedure TfmDirectorAddress.EvFocusNext(Sender: TObject);
begin
//  Inherited;
  If Sender = e13   Then e33.SetFocus;
  if Sender = e33   then e32.SetFocus;
  if Sender = e32   then e46b.SetFocus;
  if Sender = e46b  then e43.SetFocus;
  if Sender = e43   then e44.SetFocus;
  if Sender = e44   then e47.SetFocus;
  if Sender = e47   then e48.SetFocus;
  if Sender = e48   then e49.SetFocus;
  if Sender = e49   then e410.SetFocus;
  if Sender = e410  then e411.SetFocus;
  if Sender = e411  then
    if e39.Enabled Then
      e39.SetFocus
    else
      EvFocusNext(e39);

  if Sender = e39   then
    if edZeut.Enabled Then
      edZeut.SetFocus
    else
      EvFocusNext(edZeut);

  if Sender = edZeut   then
    if pnDir.Visible  then e412.SetFocus
                    else e33.SetFocus;

  if Sender = e412  then e413.SetFocus;
  if Sender = e413  then e414.SetFocus;
  if Sender = e414  then e415.SetFocus;
  if Sender = e415  then e416.SetFocus;
  if Sender = e416  then e417.SetFocus;
  if Sender = e417  then e418.SetFocus;
  if Sender = e418  then e33.SetFocus;
end;

procedure TfmDirectorAddress.EvDataLoaded;
var
  I: Integer;
  Old33Text: String;
begin
  inherited;
  if ZeutChange then
    Old33Text:=CurrentZeut
  else
    Old33Text:= e33.Text;

  ZeutChange:=false;

  e32.Items.Clear;
  e33.Items.Clear;

  if Not DoNotClear Then
    begin
      e32.Text:= '';
      e33.Text:= '';
    end;
  Company.Directors.Filter:= afActive;
  For I:= 0 To Company.Directors.Count-1 do
    begin
      e33.Items.Add(Company.Directors.Items[I].Zeut);
      e32.Items.Add(Company.Directors.Items[I].Name);
    end;
  If DoNotClear Then
    begin
      e33.ItemIndex:= e33.Items.IndexOf(Old33Text);
      e32.ItemIndex:= e33.ItemIndex;
    end;
end;

procedure TfmDirectorAddress.CriticalEditChange(Sender: TObject);
var
  OldValue: Boolean;
begin
  OldValue:= Loading;
  inherited;
  Loading:= True;  
  if Sender= e33 then
    begin
      e32.ItemIndex:= e33.ItemIndex;
      FillReadOnlyFields;
    end;
  if Sender= e32 then
    begin
      e33.ItemIndex:= e32.ItemIndex;
      FillReadOnlyFields;
    end;
  Loading:= OldValue;
end;

procedure TfmDirectorAddress.FormCreate(Sender: TObject);
begin
  FileAction:= faDirectorInfo;
  ShowStyle:= afActive;
  ZeutChange:=false;
  inherited;
end;

procedure TfmDirectorAddress.cbChangeNameClick(Sender: TObject);
begin
  e39.Enabled:= cbChangeName.Checked;
end;

procedure TfmDirectorAddress.cbZeutClick(Sender: TObject);
begin
  inherited;
  edZeut.Enabled :=cbZeut.Checked;
end;

end.
