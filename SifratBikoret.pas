unit SifratBikoret;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
  Bikoret, Sum, I, J: Integer;
begin
  Edit2.Text:='';

  If Length(Edit1.text) <> 8 Then
    Exit;

  Sum:= 0;
  For I:= 1 To 8 do
    begin
      If (I mod 2) = 0 Then
        begin
          J:= StrToInt(Edit1.Text[I]) * 2;
          If J>9 Then
            J:= J - 9;
        end
      else
        J:= StrToInt(Edit1.Text[I]);
      Inc(Sum, J);
    end;

  bikoret:= 10 - (sum mod 10);
  If bikoret = 10 Then
    Bikoret:= 0;

  Edit2.Text:= Edit1.Text + IntToStr(Bikoret);
end;

end.
