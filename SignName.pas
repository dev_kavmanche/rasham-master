unit SignName;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, StdCtrls, HEdit, HebForm, ExtCtrls, ComCtrls, Buttons,
  Mask, HLabel;

type
  TfmSignName = class(TfmBlankTemplate)
    Shape88: TShape;
    Shape89: TShape;
    HebLabel45: THebLabel;
    Shape87: TShape;
    Shape86: TShape;
    HebLabel44: THebLabel;
    Shape84: TShape;
    HebLabel42: THebLabel;
    Shape85: TShape;
    e53: THebEdit;
    e52: TEdit;
    e51: THebEdit;
    procedure FormCreate(Sender: TObject);
    procedure e52Exit(Sender: TObject);
  private
    { Private declarations }
    function GetLatestChangeDate(CompanyNum: integer): TDateTime;
  protected
    procedure EvDataLoaded; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvPrintData;override;
  public
    { Public declarations }
  end;

var
  fmSignName: TfmSignName;

implementation

{$R *.DFM}

uses Dialog2, DataObjects, DataM, PrintSignName;

procedure TfmSignName.EvPrintData;
var
  sig: TSignature;
begin
   sig.Name := e51.Text;
   sig.Zeut := e52.Text;
   sig.Tafkid := e53.Text;
   Application.CreateForm(TfmQrSignName, fmQrSignName);
   fmQrSignName.PrintExecute('', Company, sig);
   fmQrSignName.Free;
end;

function TfmSignName.EvSaveData(Draft: Boolean): Boolean;
var
  CompanyInfo: TCompanyInfo;
  LatestChangeDate: TDateTime;
begin
  Result := False;
  LatestChangeDate := GetLatestChangeDate(Company.RecNum);
  if (LatestChangeDate < DecisionDate) then
  begin
    CompanyInfo := TCompanyInfo.CreateNew(DecisionDate);
    try
      CompanyInfo.Option1                   := Company.CompanyInfo.Option1;
      CompanyInfo.Option3                   := Company.CompanyInfo.Option3;
      CompanyInfo.OtherInfo                 := Company.CompanyInfo.OtherInfo;
      CompanyInfo.SigName                   := e51.Text;
      CompanyInfo.SigZeut                   := e52.Text;
      CompanyInfo.SigTafkid                 := e53.Text;
      CompanyInfo.LawName                   := Company.CompanyInfo.LawName;
      CompanyInfo.LawAddress                := Company.CompanyInfo.LawAddress;
      CompanyInfo.LawZeut                   := Company.CompanyInfo.LawZeut;
      CompanyInfo.LawLicence                := Company.CompanyInfo.LawLicence;
      CompanyInfo.Takanon2_1                := Company.CompanyInfo.Takanon2_1;
      CompanyInfo.Takanon2_2                := Company.CompanyInfo.Takanon2_2;
      CompanyInfo.Takanon2_3                := Company.CompanyInfo.Takanon2_3;
      CompanyInfo.Takanon2_4                := Company.CompanyInfo.Takanon2_4;
      CompanyInfo.Takanon4                  := Company.CompanyInfo.Takanon4;
      CompanyInfo.Takanon5                  := Company.CompanyInfo.Takanon5;
      CompanyInfo.Takanon7                  := Company.CompanyInfo.Takanon7;
      CompanyInfo.EnglishName               := Company.CompanyInfo.EnglishName;
      CompanyInfo.RegistrationDate          := Company.CompanyInfo.RegistrationDate;
      CompanyInfo.CompanyName2              := Company.CompanyInfo.CompanyName2;
      CompanyInfo.CompanyName3              := Company.CompanyInfo.CompanyName3;
      CompanyInfo.Mail                      := Company.CompanyInfo.Mail;
      CompanyInfo.SharesRestrictIndex       := Company.CompanyInfo.SharesRestrictIndex;
      CompanyInfo.DontOfferIndex            := Company.CompanyInfo.DontOfferIndex;
      CompanyInfo.ShareHoldersNoIndex       := Company.CompanyInfo.ShareHoldersNoIndex;
      CompanyInfo.SharesRestrictChapters    := Company.CompanyInfo.SharesRestrictChapters;
      CompanyInfo.DontOfferChapters         := Company.CompanyInfo.DontOfferChapters;
      CompanyInfo.ShareHoldersNoChapters    := Company.CompanyInfo.ShareHoldersNoChapters;
      CompanyInfo.LimitedSignatoryIndex     := Company.CompanyInfo.LimitedSignatoryIndex;
      CompanyInfo.LegalSectionsChapters     := Company.CompanyInfo.LegalSectionsChapters;
      CompanyInfo.SignatorySectionsChapters := Company.CompanyInfo.SignatorySectionsChapters;
      CompanyInfo.ConfirmedCopies           := Company.CompanyInfo.ConfirmedCopies;
      CompanyInfo.MoreCopies                := Company.CompanyInfo.MoreCopies;
      CompanyInfo.RegulationItemsIndex      := Company.CompanyInfo.RegulationItemsIndex;

      CompanyInfo.SaveData(Company, False, -1);
      fmDialog2.Show;
      Result := True;
    finally
      CompanyInfo.Free;
    end;
  end else
  begin
    Application.MessageBox(PChar('���� ����� ����� ���� ����� ������ ����� ����'+#10#13+
                                 '������ '+DateToStr(LatestChangeDate)+
                                 ' ������ ���� ������ ����� ������ : '+DateToStr(DecisionDate)+#10#13+
                                 ' �� ���� ���� �� ������'  ),
                                 PChar('����'),
                                 MB_OK);
  end;
end; // TfmSignName.EvSaveData

procedure TfmSignName.EvFocusNext(Sender: TObject);
begin
  Inherited;
  If Sender = e13 then e51.SetFocus;
  If Sender = e51 then e52.SetFocus;
  If Sender = e52 then e53.SetFocus;
  If Sender = e53 then e51.SetFocus;
end;

procedure TfmSignName.EvDataLoaded;
begin
  inherited;
  If Not DoNotClear Then
    begin
      Loading:= True;
      e51.Text:= Company.CompanyInfo.SigName;
      e52.Text:= Company.CompanyInfo.SigZeut;
      e53.Text:= Company.CompanyInfo.SigTafkid;
      Loading:= False;
    end;
end;

procedure TfmSignName.FormCreate(Sender: TObject);
begin
  FileAction := faNone;
  ShowStyle := afActive;
  inherited;
end;

procedure TfmSignName.e52Exit(Sender: TObject);
begin
  inherited;
  if StrToIntDef(TEdit(Sender).Text,1) = 0 then
  begin
    Application.MessageBox(PChar('���� ���� ���� ���� ����� 0.'+#10#13+'�� �����.           '),
                           PChar('����'),MB_OK);
    TEdit(Sender).SetFocus;
  end;
end;

function TfmSignName.GetLatestChangeDate(CompanyNum: integer): TDateTime;
begin
  with data.qGeneric do
  begin
    SQL.Clear;
    SQL.Add('SELECT "'+ data.taChanges.TableName+'"."Date" ');
    SQL.Add('FROM '+ data.taChanges.TableName );
    SQL.Add('WHERE (ObjectType = '''+ ObjToChar(ocCompany)+''') AND (ObjectNum = '+IntToStr(CompanyNum)+') AND (Action = '''+ ActionToChar(acSignName)+''') ');
    SQL.Add('ORDER BY "'+ data.taChanges.TableName+'"."Date"  DESC' );
    ExecSQL;
    Open;
    First;
    Result := FieldByName('Date').AsDateTime
  end;
end;

end.
