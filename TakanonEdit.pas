unit TakanonEdit;

////////////////////////////////////////////////////
// History:
//   VG19075 15.12.2019 ver 1.7.7
//   code refactoring, max lines number support
//   for rich edit
////////////////////////////////////////////////////

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, HLabel,
  HRichEdt, RxCombos, HMemo;

//VG19075
const
  C_UNLIMITED_LINES   = 0;
  C_DEFAULT_MAX_LINES = 3;

type
  TfmTakanonEdit = class(TfmTemplate)
    btUnderline: TSpeedButton;
    btItalic: TSpeedButton;
    btBold: TSpeedButton;
    FontSize: TEdit;
    UpDown1: TUpDown;
    cbFont: TFontComboBox;
    reEditor: THebRichEdit;
    procedure cbFontChange(Sender: TObject);
    procedure FontSizeChange(Sender: TObject);
    procedure btItalicClick(Sender: TObject);
    procedure btBoldClick(Sender: TObject);
    procedure btUnderlineClick(Sender: TObject);
    procedure FontSizeKeyPress(Sender: TObject; var Key: Char);
    procedure reEditorSelectionChange(Sender: TObject);
    procedure reEditorChange(Sender: TObject);
    procedure reEditorKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure b1Click(Sender: TObject);
  private
    { Private declarations }
    FUpdating: Boolean;
    FTakanon: String;
    //VG19075
    FMaxLines: Integer; //limit for rich edit
    function GetMaxLines: Integer;
    procedure SetMaxLines(nLines: Integer);

    function CurrText: TTextAttributes;
    procedure EvFillDefaultData; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
  public
    { Public declarations }
    //VG19075
    property MaxLines: Integer read GetMaxLines write SetMaxLines;
    procedure Initialize;
    function Execute(var Takanon: String; const CaptionStr: String): Boolean;
  end;

var
  fmTakanonEdit: TfmTakanonEdit;

implementation

uses Util, dialog2;

{$R *.DFM}

function TfmTakanonEdit.EvSaveData(Draft: Boolean): Boolean;
begin
  if (reEditor.Lines.Count = 0) or
     (Trim(reEditor.Lines[reEditor.Lines.Count-1]) <> '') then
  begin
    reEditor.Lines.Add('');
    if (reEditor.Lines.Count = 0) or
       (Trim(reEditor.Lines[reEditor.Lines.Count-1]) <> '') then
      reEditor.Lines.Add('');
    reEditor.Lines.Delete(reEditor.Lines.Count-1);
  end;
  try
    FTakanon := reEditor.Lines.Text;
  except
  end;
  fmDialog2.Show;
  Result := True;
end;

function TfmTakanonEdit.Execute(var Takanon: String; const CaptionStr: String): Boolean;
begin
  FTakanon:= Takanon;
  reEditor.Text := FTakanon;
  DataChanged := False;
  FUpdating := True;  // FUpdating will be False in FormShow method (it fixes a bug with a font change)
  lCaption.Caption := CaptionStr;
  ShowModal;
  Result:= Takanon <> FTakanon;
  Takanon:= FTakanon;  // If NOT saved it should remain as it.
end;

procedure TfmTakanonEdit.EvFillDefaultData;
begin
  inherited;
  CurrText.Style:= [];
  CurrText.Name:= 'David';
  CurrText.Size:= 13;
  CurrText.Charset:= HEBREW_CHARSET;
  reEditorSelectionChange(Self);
  DataChanged:= False;
end;

procedure TfmTakanonEdit.cbFontChange(Sender: TObject);
begin
  inherited;
  if FUpdating then
    Exit;
  CurrText.Name:= cbFont.FontName;
  reEditor.SetFocus;
end;

function TfmTakanonEdit.CurrText: TTextAttributes;
begin
  Result := TTextAttributes(reEditor.SelAttributes);
end;

procedure TfmTakanonEdit.FontSizeChange(Sender: TObject);
begin
  inherited;
  if FUpdating then
    Exit;
  CurrText.Size:= UpDown1.Position;
  reEditor.SetFocus;
end;

procedure TfmTakanonEdit.btItalicClick(Sender: TObject);
begin
  inherited;
  if FUpdating then
    Exit;
  if btItalic.Down then
    CurrText.Style:= CurrText.Style + [fsItalic]
  else
    CurrText.Style:= CurrText.Style - [fsItalic];
  reEditor.SetFocus;
end;

procedure TfmTakanonEdit.btBoldClick(Sender: TObject);
begin
  inherited;
  if FUpdating then
    Exit;
  if btBold.Down then
    CurrText.Style:= CurrText.Style + [fsBold]
  else
    CurrText.Style:= CurrText.Style - [fsBold];
  reEditor.SetFocus;
end;

procedure TfmTakanonEdit.btUnderlineClick(Sender: TObject);
begin
  inherited;
  if FUpdating then
    Exit;
  if btUnderline.Down then
    CurrText.Style:= CurrText.Style + [fsUnderline]
  else
    CurrText.Style:= CurrText.Style - [fsUnderline];
  reEditor.SetFocus;
end;

procedure TfmTakanonEdit.FontSizeKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (Key>#31) and (not (Key in ['0'..'9'])) then
    Key:= #0;
end;

procedure TfmTakanonEdit.reEditorSelectionChange(Sender: TObject);
begin
  inherited;
  try
    FUpdating:= True;
    btBold.Down:= fsBold in CurrText.Style;
    btItalic.Down:= fsItalic in CurrText.Style;
    btUnderline.Down:= fsUnderline in CurrText.Style;
    UpDown1.Position:= CurrText.Size;
    //FontSize.Text:= IntToStr(CurrText.Size);
    cbFont.FontName:= CurrText.Name;
  finally
    FUpdating:= False;
  end;
end;

procedure TfmTakanonEdit.reEditorChange(Sender: TObject);
begin
  inherited;
  if Loading or FUpdating then
    Exit;

  //VG19075
  if (FMaxLines > C_UNLIMITED_LINES) then
  begin
    if (reEditor.Lines.Count > FMaxLines) then
      reEditor.Lines.Delete(FMaxLines);
  end;
  DataChanged:= True;
end;

procedure TfmTakanonEdit.reEditorKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key>#31 then
    DataChanged:= True;
end;

//VG19075
function TfmTakanonEdit.GetMaxLines: Integer;
begin
  Result := FMaxLines;
end;

//VG19075
procedure TfmTakanonEdit.SetMaxLines(nLines: Integer);
begin
  FMaxLines := nLines;
end;

//VG19075
procedure TfmTakanonEdit.Initialize;
begin
  FUpdating:= False;
  FMaxLines := C_UNLIMITED_LINES;
end;

procedure TfmTakanonEdit.FormShow(Sender: TObject);
begin
  inherited;
//VG19075
//  FUpdating:= False;
end;

procedure TfmTakanonEdit.b1Click(Sender: TObject);
begin
//VG19075
  reEditor.OnChange := nil;
  inherited;
  Close;
end;

end.
