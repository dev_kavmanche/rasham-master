unit PrintDoh;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Qrctrls, QuickRpt, DataObjects, Doh, Db, memdset, StdCtrls,
  HLabel;

type
  TfmQRDoh = class(TForm)
    qrDoh1: TQuickRep;
    QRBand1: TQRBand;
    QRShape27: TQRShape;
    QRShape30: TQRShape;
    QRShape5: TQRShape;
    QRShape2: TQRShape;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape3: TQRShape;
    QRLabel8: TQRLabel;
    l1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape19: TQRShape;
    QRLabel27: TQRLabel;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRLabel28: TQRLabel;
    QRShape23: TQRShape;
    QRLabel29: TQRLabel;
    QRLabel40: TQRLabel;
    l4: TQRLabel;
    QRLabel38: TQRLabel;
    QRShape4: TQRShape;
    l3: TQRLabel;
    QRLabel45: TQRLabel;
    QRShape24: TQRShape;
    QRLabel23: TQRLabel;
    QRShape25: TQRShape;
    QRLabel47: TQRLabel;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRShape26: TQRShape;
    QRLabel24: TQRLabel;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRLabel25: TQRLabel;
    QRShape31: TQRShape;
    QRLabel26: TQRLabel;
    QRLabel39: TQRLabel;
    QRShape32: TQRShape;
    QRLabel42: TQRLabel;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape38: TQRShape;
    QRShape40: TQRShape;
    QRLabel41: TQRLabel;
    QRShape41: TQRShape;
    QRLabel43: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
    l18: TQRLabel;
    l27: TQRLabel;
    l35: TQRLabel;
    l19: TQRLabel;
    l20: TQRLabel;
    l29: TQRLabel;
    l37: TQRLabel;
    l21: TQRLabel;
    l30: TQRLabel;
    l38: TQRLabel;
    l22: TQRLabel;
    l31: TQRLabel;
    l39: TQRLabel;
    l22a: TQRLabel;
    l31a: TQRLabel;
    l39a: TQRLabel;
    l28: TQRLabel;
    l36: TQRLabel;
    QRBand2: TQRBand;
    imV: TImage;
    QRShape6: TQRShape;
    QRLabel9: TQRLabel;
    QRShape7: TQRShape;
    l2: TQRLabel;
    l3a: TQRLabel;
    QRLabel12: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLabel13: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRLabel14: TQRLabel;
    QRShape12: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    l5: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    l6: TQRLabel;
    l10: TQRLabel;
    l14: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape17: TQRShape;
    l15: TQRLabel;
    l11: TQRLabel;
    l7: TQRLabel;
    l16: TQRLabel;
    l12: TQRLabel;
    l8: TQRLabel;
    l9: TQRLabel;
    l13: TQRLabel;
    l17: TQRLabel;
    QRShape18: TQRShape;
    QRShape20: TQRShape;
    l14a: TQRLabel;
    l10a: TQRLabel;
    l6a: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel91: TQRLabel;
    QRShape42: TQRShape;
    QRShape63: TQRShape;
    QRShape64: TQRShape;
    QRShape65: TQRShape;
    QRLabel44: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    qrDoh2: TQuickRep;
    QRBand3: TQRBand;
    QRBand4: TQRBand;
    QRLabel127: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    l24: TQRLabel;
    l32: TQRLabel;
    l40: TQRLabel;
    l25: TQRLabel;
    l33: TQRLabel;
    l41: TQRLabel;
    l26: TQRLabel;
    l34: TQRLabel;
    l42: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    cb7: TQRImage;
    QRLabel72: TQRLabel;
    l78: TQRLabel;
    QRLabel74: TQRLabel;
    l79: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    cb8: TQRImage;
    QRLabel78: TQRLabel;
    l80: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    cb9: TQRImage;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    l81: TQRLabel;
    QRLabel86: TQRLabel;
    l82: TQRLabel;
    QRLabel99: TQRLabel;
    l83: TQRLabel;
    QRLabel102: TQRLabel;
    l84: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    l85: TQRLabel;
    QRLabel120: TQRLabel;
    l86: TQRLabel;
    QRLabel122: TQRLabel;
    l87: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    l88: TQRLabel;
    QRLabel136: TQRLabel;
    l89: TQRLabel;
    QRLabel138: TQRLabel;
    l90: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRLabel144: TQRLabel;
    cb10: TQRImage;
    ls2: TQRLabel;
    QRLabel147: TQRLabel;
    l91: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    l92: TQRLabel;
    QRLabel153: TQRLabel;
    l93: TQRLabel;
    QRLabel155: TQRLabel;
    l94: TQRLabel;
    QRLabel157: TQRLabel;
    QRLabel73: TQRLabel;
    lBigHon: TQRLabel;
    lBigStocks: TQRLabel;
    lBigManager: TQRLabel;
    qrDoh3: TQuickRep;
    QRBand5: TQRBand;
    QRLabel118: TQRLabel;
    QRLabel137: TQRLabel;
    lCompanyName: TQRLabel;
    QRLabel201: TQRLabel;
    lCompanyZeut: TQRLabel;
    QRBand6: TQRBand;
    GroupHeaderBand1: TQRBand;
    QRLabel75: TQRLabel;
    QRShape87: TQRShape;
    QRLabel79: TQRLabel;
    QRLabel85: TQRLabel;
    QRShape88: TQRShape;
    QRShape89: TQRShape;
    QRShape90: TQRShape;
    QRLabel98: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel131: TQRLabel;
    QRDBText10: TQRDBText;
    QRShape91: TQRShape;
    QRShape92: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRShape93: TQRShape;
    QRShape94: TQRShape;
    QRShape95: TQRShape;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    GroupHeaderBand2: TQRBand;
    QRShape99: TQRShape;
    QRShape100: TQRShape;
    QRLabel135: TQRLabel;
    QRShape101: TQRShape;
    QRLabel139: TQRLabel;
    QRLabel146: TQRLabel;
    QRShape102: TQRShape;
    QRLabel148: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel156: TQRLabel;
    QRShape103: TQRShape;
    QRLabel171: TQRLabel;
    QRShape104: TQRShape;
    QRShape105: TQRShape;
    QRLabel172: TQRLabel;
    QRShape106: TQRShape;
    QRLabel173: TQRLabel;
    QRLabel174: TQRLabel;
    QRLabel175: TQRLabel;
    QRShape107: TQRShape;
    QRShape108: TQRShape;
    QRShape109: TQRShape;
    QRLabel176: TQRLabel;
    QRLabel177: TQRLabel;
    QRLabel178: TQRLabel;
    QRLabel179: TQRLabel;
    QRLabel180: TQRLabel;
    QRLabel181: TQRLabel;
    QRLabel182: TQRLabel;
    QRShape96: TQRShape;
    QRShape110: TQRShape;
    QRShape111: TQRShape;
    QRShape97: TQRShape;
    QRShape98: TQRShape;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRShape112: TQRShape;
    QRShape113: TQRShape;
    QRShape114: TQRShape;
    QRDBText15: TQRDBText;
    QRShape115: TQRShape;
    QRDBText16: TQRDBText;
    QRBand7: TQRBand;
    QRLabel183: TQRLabel;
    QRShape118: TQRShape;
    QRShape119: TQRShape;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    QRLabel186: TQRLabel;
    QRShape116: TQRShape;
    QRDBText17: TQRDBText;
    QRBand8: TQRBand;
    QRShape120: TQRShape;
    QRShape121: TQRShape;
    QRLabel187: TQRLabel;
    QRShape123: TQRShape;
    QRLabel188: TQRLabel;
    QRLabel189: TQRLabel;
    QRShape124: TQRShape;
    QRLabel190: TQRLabel;
    QRLabel191: TQRLabel;
    QRLabel192: TQRLabel;
    QRLabel193: TQRLabel;
    QRShape126: TQRShape;
    QRLabel194: TQRLabel;
    QRShape127: TQRShape;
    QRShape129: TQRShape;
    QRLabel195: TQRLabel;
    QRShape130: TQRShape;
    QRLabel196: TQRLabel;
    QRLabel197: TQRLabel;
    QRLabel198: TQRLabel;
    QRShape131: TQRShape;
    QRShape132: TQRShape;
    QRLabel199: TQRLabel;
    QRLabel200: TQRLabel;
    QRShape133: TQRShape;
    QRLabel203: TQRLabel;
    QRShape134: TQRShape;
    QRLabel204: TQRLabel;
    QRShape117: TQRShape;
    QRShape122: TQRShape;
    QRShape125: TQRShape;
    QRShape128: TQRShape;
    QRShape135: TQRShape;
    QRShape136: TQRShape;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRShape137: TQRShape;
    QRDBText24: TQRDBText;
    QRBand9: TQRBand;
    QRLabel205: TQRLabel;
    QRShape139: TQRShape;
    QRShape141: TQRShape;
    QRLabel206: TQRLabel;
    QRShape142: TQRShape;
    QRLabel207: TQRLabel;
    QRShape143: TQRShape;
    QRLabel208: TQRLabel;
    QRLabel209: TQRLabel;
    QRLabel210: TQRLabel;
    QRLabel211: TQRLabel;
    QRShape145: TQRShape;
    QRShape146: TQRShape;
    QRLabel212: TQRLabel;
    QRLabel213: TQRLabel;
    QRShape148: TQRShape;
    QRLabel214: TQRLabel;
    QRShape149: TQRShape;
    QRLabel215: TQRLabel;
    QRShape138: TQRShape;
    QRShape140: TQRShape;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRShape144: TQRShape;
    QRBand10: TQRBand;
    QRLabel217: TQRLabel;
    QRLabel218: TQRLabel;
    QRLabel219: TQRLabel;
    QRLabel220: TQRLabel;
    QRLabel221: TQRLabel;
    QRLabel222: TQRLabel;
    QRShape150: TQRShape;
    QRShape151: TQRShape;
    QRShape152: TQRShape;
    QRShape153: TQRShape;
    QRShape147: TQRShape;
    QRShape154: TQRShape;
    QRShape155: TQRShape;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    taMemHon: TMemDataSet;
    taMemStocks: TMemDataSet;
    taMemMuhaz: TMemDataSet;
    taMemDirector: TMemDataSet;
    taMemPastDirector: TMemDataSet;
    taMemManager: TMemDataSet;
    QRLabel145: TQRLabel;
    lCaption: TQRLabel;
    PageFooterBand1: TQRBand;
    QRSysData1: TQRSysData;
    QRLabel202: TQRLabel;
    QRLabel223: TQRLabel;
    lPageCount: TQRLabel;
    QRLabel224: TQRLabel;
    lPageCount1: TQRLabel;
    QRLabel225: TQRLabel;
    QRLabel226: TQRLabel;
    lPageCount2: TQRLabel;
    QRLabel228: TQRLabel;
    QRLabel229: TQRLabel;
    qrDoh2a: TQuickRep;
    QRBand11: TQRBand;
    QRLabel159: TQRLabel;
    QRLabel161: TQRLabel;
    QRLabel163: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel166: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel169: TQRLabel;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel233: TQRLabel;
    lLawyer: TQRLabel;
    lFullNameLawyer: TQRLabel;
    QRLabel236: TQRLabel;
    lIDNumLawyer: TQRLabel;
    QRLabel238: TQRLabel;
    QRLabel239: TQRLabel;
    QRLabel240: TQRLabel;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    lLawyer2: TQRLabel;
    QRLabel248: TQRLabel;
    lLawyerLicense: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel246: TQRLabel;
    lLawyerAddress: TQRLabel;
    lLawyerID: TQRLabel;
    lPageCount3: TQRLabel;
    QRLabel254: TQRLabel;
    DetailBand1: TQRBand;
    QRLabel255: TQRLabel;
    QRLabel216: TQRLabel;
    QRShape156: TQRShape;
    QRShape157: TQRShape;
    lPhone: TQRLabel;
    QRShape158: TQRShape;
    QRLabel227: TQRLabel;
    lStockVal1: TQRLabel;
    lStockVal2: TQRLabel;
    lStockVal3: TQRLabel;
    QRLabel234: TQRLabel;
    QRLabel235: TQRLabel;
    QRShape159: TQRShape;
    lTotalHon1: TQRLabel;
    lTotalHon2: TQRLabel;
    lTotalHon3: TQRLabel;
    QRLabel230: TQRLabel;
    QRLabel237: TQRLabel;
    QRLabel243: TQRLabel;
    lAccountant: TQRLabel;
    lFullNameAccountant: TQRLabel;
    lIdNumAccountant: TQRLabel;
    QRLabel252: TQRLabel;
    QRLabel256: TQRLabel;
    QRLabel257: TQRLabel;
    QRLabel258: TQRLabel;
    lAccountant2: TQRLabel;
    QRLabel260: TQRLabel;
    lAccountantLicense: TQRLabel;
    QRLabel262: TQRLabel;
    QRLabel263: TQRLabel;
    lAccountantAddress: TQRLabel;
    lAccountantId: TQRLabel;
    QRLabel251: TQRLabel;
    QRShape160: TQRShape;
    lblHonText1: TQRLabel;
    lblFullTotalHon: TQRLabel;
    lblHonText2: TQRLabel;
    lblHonText3: TQRLabel;
    lblHonText4: TQRLabel;
    QRShape161: TQRShape;
    QRLabel245: TQRLabel;
    QRLabel247: TQRLabel;
    QRLabel249: TQRLabel;
    lblAppendixTotalHon: TQRLabel;
    QRLabel259: TQRLabel;
    QRLabel250: TQRLabel;
    QRLabel261: TQRLabel;
    QRLabel253: TQRLabel;
    QRLabel264: TQRLabel;
    qrDoh1a: TQuickRep;
    QRImage1: TQRImage;
    QRLabel32: TQRLabel;
    QRShape48: TQRShape;
    QRLabel50: TQRLabel;
    QRShape52: TQRShape;
    QRLabel52: TQRLabel;
    QRShape56: TQRShape;
    QRLabel56: TQRLabel;
    QRLabel104: TQRLabel;
    QRShape49: TQRShape;
    l48: TQRLabel;
    l55: TQRLabel;
    l62: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel51: TQRLabel;
    QRShape46: TQRShape;
    l49: TQRLabel;
    l56: TQRLabel;
    l63: TQRLabel;
    l64: TQRLabel;
    lBigDirectors: TQRLabel;
    l50: TQRLabel;
    l51: TQRLabel;
    l58: TQRLabel;
    l65: TQRLabel;
    l52: TQRLabel;
    l59: TQRLabel;
    l66: TQRLabel;
    QRShape57: TQRShape;
    l67: TQRLabel;
    l60: TQRLabel;
    l53: TQRLabel;
    l54: TQRLabel;
    l61: TQRLabel;
    QRShape58: TQRShape;
    l68: TQRLabel;
    QRShape55: TQRShape;
    QRShape50: TQRShape;
    QRLabel57: TQRLabel;
    QRShape61: TQRShape;
    QRLabel58: TQRLabel;
    QRShape62: TQRShape;
    QRLabel59: TQRLabel;
    QRShape67: TQRShape;
    QRLabel60: TQRLabel;
    QRShape66: TQRShape;
    QRLabel106: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel100: TQRLabel;
    QRShape60: TQRShape;
    QRShape68: TQRShape;
    QRShape69: TQRShape;
    QRShape51: TQRShape;
    QRShape53: TQRShape;
    QRShape47: TQRShape;
    l57: TQRLabel;
    QRShape54: TQRShape;
    QRShape59: TQRShape;
    QRLabel55: TQRLabel;
    QRLabel107: TQRLabel;
    QRShape72: TQRShape;
    QRLabel108: TQRLabel;
    QRShape76: TQRShape;
    QRLabel110: TQRLabel;
    QRShape80: TQRShape;
    QRLabel123: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel119: TQRLabel;
    QRShape71: TQRShape;
    l70: TQRLabel;
    QRShape77: TQRShape;
    l73: TQRLabel;
    l72: TQRLabel;
    l69: TQRLabel;
    QRShape74: TQRShape;
    QRShape79: TQRShape;
    QRShape75: TQRShape;
    l75: TQRLabel;
    l76: TQRLabel;
    QRShape78: TQRShape;
    QRShape70: TQRShape;
    QRShape73: TQRShape;
    l71: TQRLabel;
    l74: TQRLabel;
    QRShape82: TQRShape;
    QRShape83: TQRShape;
    QRShape81: TQRShape;
    QRLabel126: TQRLabel;
    QRLabel125: TQRLabel;
    QRShape84: TQRShape;
    QRShape85: TQRShape;
    QRShape86: TQRShape;
    QRLabel124: TQRLabel;
    l77: TQRLabel;
    lBigPastDirectors: TQRLabel;
    QRLabel266: TQRLabel;
    QRLabel267: TQRLabel;
    QRLabel268: TQRLabel;
    QRLabel269: TQRLabel;
    QRLabel270: TQRLabel;
    QRLabel271: TQRLabel;
    QRShape162: TQRShape;
    QRLabel272: TQRLabel;
    lMail: TQRLabel;
    QRShape163: TQRShape;
    QRShape164: TQRShape;
    QRLabel170: TQRLabel;
    QRShape165: TQRShape;
    QRShape166: TQRShape;
    QRShape167: TQRShape;
    QRLabel160: TQRLabel;
    QRShape168: TQRShape;
    QRLabel162: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel167: TQRLabel;
    QRLabel273: TQRLabel;
    QRLabel274: TQRLabel;
    QRLabel275: TQRLabel;
    lReportYear: TQRLabel;
    QRLabel277: TQRLabel;
    QRShape169: TQRShape;
    QRLabel278: TQRLabel;
    QRBand12: TQRBand;
    QRLabel11: TQRLabel;
    l43: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape45: TQRShape;
    QRLabel30: TQRLabel;
    QRShape43: TQRShape;
    l44: TQRLabel;
    l46: TQRLabel;
    QRShape39: TQRShape;
    QRShape44: TQRShape;
    lBigMuhaz: TQRLabel;
    l45: TQRLabel;
    l47: TQRLabel;
    QRLabel265: TQRLabel;
    QRLabel276: TQRLabel;
    QRLabel279: TQRLabel;
    QRLabel17: TQRLabel;
    cb1: TQRImage;
    QRLabel61: TQRLabel;
    QRLabel19: TQRLabel;
    cb2: TQRImage;
    QRLabel22: TQRLabel;
    QRLabel49: TQRLabel;
    cb3: TQRImage;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    cb4: TQRImage;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    cb5: TQRImage;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    cb6: TQRImage;
    QRLabel53: TQRLabel;
    QRLabel280: TQRLabel;
    QRShape170: TQRShape;
    QRShape171: TQRShape;
    QRShape172: TQRShape;
    QRShape173: TQRShape;
    procedure QRSysData1Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    FCompany: TCompany;
    FDecisionDate, FNotifyDate, FLastDoh : TDateTime;
    FSender: TfmDoh;
    FDoPrint: Boolean;
    LastHonItem,
    LastMuhazItem,
    LastStockHolder,
    LastActiveDirector,
    LastStock,
    LastNonActiveDirector,
    LastManager: Integer;
    BigManagers, BigHon, BigMuhaz, BigDirectors, BigStocks, BigPastDirectors: Boolean;
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintEmptyProc(DoPrint: Boolean);
    procedure PrintNextPage1;
    procedure PrintNextPage1a;
    procedure PrintNextPage2;
    procedure PrintNextPage2a;
    procedure CountBigValues;
    procedure PrintBigLists;
  public
    { Public declarations }
    procedure PrintEmpty(const FileName: String);
    procedure PrintExecute(const FileName: String; Company: TCompany; Sender: TfmDoh; DecisionDate, NotifyDate, LastDoh: TDateTime);
  end;

var
  fmQRDoh: TfmQRDoh;

implementation

{$R *.DFM}

uses Util, PrinterSetup, Main;

procedure TfmQRDoh.PrintEmpty(const FileName: String);
begin
  fmPrinterSetup.Execute(FileName, PrintEmptyProc);
end;

procedure TfmQRDoh.PrintExecute(const FileName: String; Company: TCompany; Sender: TfmDoh; DecisionDate, NotifyDate, LastDoh: TDateTime);
begin
  FCompany:= Company;
  FLastDoh:= LastDoh;
  FSender:= Sender;
  FDecisionDate:= DecisionDate;
  FNotifyDate:= NotifyDate;
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQRDoh.PrintEmptyProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(qrDoh1);
  fmPrinterSetup.FormatQR(qrDoh1a);
  fmPrinterSetup.FormatQR(qrDoh2);
  fmPrinterSetup.FormatQR(qrDoh2a);  
  If DoPrint Then
    begin
      qrDoh1.Print;
      qrDoh1a.Print;
      qrDoh2.Print;
      qrDoh2a.Print;
    end
  Else
    begin
      qrDoh1.Preview;
      Application.ProcessMessages;
      qrDoh1a.Preview;
      Application.ProcessMessages;
      qrDoh2.Preview;
      Application.ProcessMessages;
      qrDoh2a.Preview;
      Application.ProcessMessages;
    end;
end;

procedure TfmQRDoh.PrintProc(DoPrint: Boolean);
var
  I: Integer;
  MuhazCount: Double;
begin
  FDoPrint:= DoPrint;
  FCompany.HonList.Filter:= afActive;
  FCompany.StockHolders.Filter:= afActive;
  LastStock:= 0;
  LastHonItem:=0;
  LastStockHolder:=0;
  LastActiveDirector:=0;
  LastNonActiveDirector:=0;
  LastMuhazItem:= 0;
  MuhazCount:= 0;
  For I:= 0 to FCompany.MuhazList.Count-1 do
    MuhazCount:= MuhazCount+ TMuhazItem(FCompany.MuhazList.Items[I]).ShtarValue;
  if MuhazCount = 0 Then
    l43.Caption:= ''
  Else
    l43.Caption:= FloatToStrF(MuhazCount, ffNumber, 15, 2);
  PrintNextPage1;
  LastManager:= 0;
  PrintNextPage1a;
  PrintNextPage2;
  PrintNextPage2a;  
  if BigDirectors or BigHon or BigManagers or BigMuhaz or BigPastDirectors or BigStocks then
    PrintBigLists;

{  fmPrinterSetup.FormatQR(qrDoh1);
  fmPrinterSetup.FormatQR(qrDoh2);
  fmPrinterSetup.FormatQR(qrDoh2a);
  If FDoPrint then
    begin
      qrDoh1.Print;
      qrDoh2.Print;
      qrDoh2a.Print;
    end
  Else
    begin
      qrDoh1.Preview;
      Application.ProcessMessages;
      qrDoh2.Preview;
      Application.ProcessMessages;
      qrDoh2a.Preview;
      Application.ProcessMessages;
    end;
 }
  PrintEmptyProc(DoPrint);
  if BigDirectors or BigHon or BigManagers or BigMuhaz or BigPastDirectors or BigStocks then
    If FDoPrint then
      qrDoh3.Print
    Else
      begin
        qrDoh3.Preview;
        Application.ProcessMessages;
      end;
end;

procedure TfmQRDoh.CountBigValues;

  function CheckLongNames(List: TListObject; MaxWidth: Integer): Boolean;
  var
    I: Integer;
  begin
    Result:= False;
    if fmMain.UserOptions.CheckLongNamesOnPrint then
      for I:= 0 to List.Count-1 do
        if MaxWidth < fmMain.GetTextWidth(List.Items[I].Name, l18.Font) then
          begin
            Result:= True;
            Break;
          end;
  end;

var
  Count, I: integer;
begin
  FCompany.Directors.Filter:= afActive;
  BigDirectors:= (FCompany.Directors.Count > 3) or (CheckLongNames(FCompany.Directors, 208));
  BigHon:= FCompany.HonList.Count > 3;
  BigMuhaz:= FCompany.MuhazList.Count >2;
  Count:= 0;
  FCompany.Directors.Filter:= afNonActive;
  for I:= 0 to FCompany.Directors.Count-1 do
    if FCompany.Directors.Items[I].LastChanged >= FLastDoh then
      Inc(Count);
  BigPastDirectors:= Count > 3;

  Count:=0;
  For I:= 0 to FCompany.StockHolders.Count -1 do
    Inc(Count, (FCompany.StockHolders.Items[I] as TStockHolder).Stocks.Count);

  BigStocks:= (Count > 3) or (CheckLongNames(FCompany.StockHolders, 144));
  BigManagers:= FSender.Grid2.RowCount > 1;
end;

procedure TfmQRDoh.PrintNextPage1;
var
  totalHon, fullTotalHon: double;
begin
  CountBigValues;
{  If (LastHonItem>= FCompany.HonList.Count) and
     (LastStockHolder>= FCompany.StockHolders.Count) and
     (LastActiveDirector>= FCompany.Directors.Count) and
     (LastNonActiveDirector>= NonActiveDirectorCount) and
     (LastMuhazItem>=FCompany.MuhazList.Count) then
    Exit;}
  l1.Caption:= FCompany.Name;
  l2.Caption:= FCompany.Zeut;
  l3a.Caption:= FCompany.Address.Street+' '+ FCompany.Address.Number;
  l3.Caption:= FCompany.Address.City+' '+ FCompany.Address.Index;
  //==>Tsahi 18.6.06: added phone details
  lPhone.Caption:= FCompany.Address.Phone;
  //<==
  // Moshe 28/12/2015
  lMail.Caption := FSender.edMail.Text;

  If fmPrinterSetup.PrintDateInDoh Then
    begin
      l4.Caption:= DateToLongStr(FNotifyDate);
      l5.Caption:= DateToLongStr(FDecisionDate);
    end
  Else
    begin
      l4.Caption:= '';
      l5.Caption:= '';
    end;

  // Fill HonList
  fullTotalHon :=0;

  try
    if BigHon then
      Abort;  // Raises an exception

    with FCompany.HonList.Items[LastHonItem] as THonItem do
      begin
        // Line #1
        l6.Caption  := Name;
        l6a.Caption := FloatToStrF(Value, ffNumber, 15,4);
        l8.Caption  := FloatToStrF(Rashum, ffNumber, 15,2);
        l9.Caption  := FloatToStrF(Mokza, ffNumber, 15,2);
        //==>18/06/06 Tsahi:Added Total Hon to report
        totalHon := Rashum*Value;
        lTotalHon1.Caption := FloatToStrF(totalHon, ffNumber, 15,4);
        fullTotalHon := fullTotalHon+ totalHon;
        //<==
      end;
    Inc(LastHonItem);
  except
    l6.Caption  := '';
    l6a.Caption := '';
    l8.Caption  := '';
    l9.Caption  := '';
    //==>18/06/06 Tsahi:Added Total Hon to report
    lTotalHon1.Caption := '';
    //<==
  end;

  try
    if BigHon then
      Abort;  // Raises an exception
    with FCompany.HonList.Items[LastHonItem] as THonItem do
      begin
        // Line #2
        l10.Caption  := Name;
        l10a.Caption := FloatToStrF(Value, ffNumber, 15,4);
        l12.Caption  := FloatToStrF(Rashum, ffNumber, 15,2);
        l13.Caption  := FloatToStrF(Mokza, ffNumber, 15,2);
        //==>18/06/06 Tsahi:Added Total Hon to report
        totalHon := Rashum*Value;
        lTotalHon2.Caption := FloatToStrF(totalHon, ffNumber, 15,4);
        fullTotalHon := fullTotalHon+ totalHon;
        //<==
      end;
    Inc(LastHonItem);
  except
    l10.Caption  := '';
    l10a.Caption := '';
    l12.Caption  := '';
    l13.Caption  := '';
    //==>18/06/06 Tsahi:Added Total Hon to report
    lTotalHon2.Caption := '';
    //<==
  end;

  try
    if BigHon then
      Abort;  // Raises an exception
    with FCompany.HonList.Items[LastHonItem] as THonItem do
      begin
        // Line #3
        l14.Caption  := Name;
        l14a.Caption := FloatToStrF(Value, ffNumber, 15,4);
        l16.Caption  := FloatToStrF(Rashum, ffNumber, 15,2);
        l17.Caption  := FloatToStrF(Mokza, ffNumber, 15,2);
        //==>18/06/06 Tsahi:Added Total Hon to report
        totalHon := Rashum*Value;
        lTotalHon3.Caption := FloatToStrF(totalHon, ffNumber, 15,4);
        fullTotalHon := fullTotalHon+ totalHon;
        //<==
      end;
    Inc(LastHonItem);
  except
    l14.Caption  := '';
    l14a.Caption := '';
    l16.Caption  := '';
    l17.Caption  := '';
    //==>18/06/06 Tsahi:Added Total Hon to report
    lTotalHon3.Caption := '';
    //<==
  end;

  if not BigHon then
  begin
    lblFullTotalHon.Caption := FloatToStrF(fullTotalHon, ffNumber, 15,4);
  end;

  // Fill StockHolders
  try
    if BigStocks then
      Abort;  // Raises an exception
    // Line #1
    With FCompany.StockHolders.Items[LastStockHolder] as TStockHolder do
      begin
        with Stocks.Items[LastStock] as TStock do
          begin
            l24.Caption:= Name;
            l25.Caption:= FloatToStrF(Count, ffNumber, 15, 2);
            l26.Caption:= FloatToStrF(Paid, ffNumber, 15, 2);
          end;
        l18.Caption:= Name;
        l19.Caption:= Zeut;
        l20.Caption:= Address.City;
        l21.Caption:= Address.Street;
        l22.Caption:= Address.Number;
        l22a.Caption:= Address.Index;

        Inc(LastStock);
        If LastStock >= Stocks.Count then
          begin
            LastStock:= 0;
            inc(LastStockHolder);
          end;
      end;
  except
    l18.Caption:= '';
    l19.Caption:= '';
    l20.Caption:= '';
    l21.Caption:= '';
    l22.Caption:= '';
    l22a.Caption:= '';
    l24.Caption:= '';
    l25.Caption:= '';
    l26.Caption:= '';
  end;

  try
    if BigStocks then
      Abort;  // Raises an exception
    // Line #2
    With FCompany.StockHolders.Items[LastStockHolder] as TStockHolder do
      begin
        with Stocks.Items[LastStock] as TStock do
          begin
            l32.Caption:= Name;
            l33.Caption:= FloatToStrF(Count, ffNumber, 15, 2);
            l34.Caption:= FloatToStrF(Paid, ffNumber, 15, 2);
          end;
        l27.Caption:= Name;
        l28.Caption:= Zeut;
        l29.Caption:= Address.City;
        l30.Caption:= Address.Street;
        l31.Caption:= Address.Number;
        l31a.Caption:= Address.Index;

        Inc(LastStock);
        If LastStock >= Stocks.Count then
          begin
            LastStock:= 0;
            inc(LastStockHolder);
          end;
      end;
  except
    l27.Caption:= '';
    l28.Caption:= '';
    l29.Caption:= '';
    l30.Caption:= '';
    l31.Caption:= '';
    l31a.Caption:= '';
    l32.Caption:= '';
    l33.Caption:= '';
    l34.Caption:= '';
  end;

  try
    if BigStocks then
      Abort;  // Raises an exception
    // Line #3
    With FCompany.StockHolders.Items[LastStockHolder] as TStockHolder do
      begin
        with Stocks.Items[LastStock] as TStock do
          begin
            l40.Caption:= Name;
            l41.Caption:= FloatToStrF(Count, ffNumber, 15, 2);
            l42.Caption:= FloatToStrF(Paid, ffNumber, 15, 2);
          end;
        l35.Caption:= Name;
        l36.Caption:= Zeut;
        l37.Caption:= Address.City;
        l38.Caption:= Address.Street;
        l39.Caption:= Address.Number;
        l39a.Caption:= Address.Index;

        Inc(LastStock);
        If LastStock >= Stocks.Count then
          begin
            LastStock:= 0;
            inc(LastStockHolder);
          end;
      end;
  except
    l35.Caption:= '';
    l36.Caption:= '';
    l37.Caption:= '';
    l38.Caption:= '';
    l39.Caption:= '';
    l39a.Caption:= '';
    l40.Caption:= '';
    l41.Caption:= '';
    l42.Caption:= '';
  end;

  lBigDirectors.Enabled:= BigDirectors;
  lBigHon.Enabled:= BigHon;
  lBigMuhaz.Enabled:= BigMuhaz;
  lBigStocks.Enabled:= BigStocks;
  lBigPastDirectors.Enabled:= BigPastDirectors;
//  PrintNextPage1;
end;

procedure TfmQRDoh.PrintNextPage1a;
begin
  // Fill MuhazList
  try
    if BigMuhaz then
      Abort;  // Raises an exception
    // Line #1
    With FCompany.MuhazList.Items[LastMuhazItem] as TMuhazItem do
      begin
        l44.Caption:= FloatToStrF(ShtarValue, ffNumber, 15, 2);
        l45.Caption:= Zeut;
      end;
    Inc(LastMuhazItem);
  except
    l44.Caption:= '';
    l45.Caption:= '';
  end;

  try
    if BigMuhaz then
      Abort;  // Raises an exception
    // Line #2
    With FCompany.MuhazList.Items[LastMuhazItem] as TMuhazItem do
      begin
        l46.Caption:= FloatToStrF(ShtarValue, ffNumber, 15, 2);
        l47.Caption:= Zeut;
      end;
  except
    l46.Caption:= '';
    l47.Caption:= '';
  end;
  Inc(LastMuhazItem);

  // Fill Active Directors
  FCompany.Directors.Filter:= afActive;
  try
    if BigDirectors then
      Abort;  // Raises an exception
    // Line #1
    With FCompany.Directors.Items[LastActiveDirector] as TDirector do
      begin
        l48.Caption:= Name;
        l49.Caption:= Zeut;
        l50.Caption:= Address.City;
        l51.Caption:= Address.Street;
        l52.Caption:= Address.Number;
        If Address.Index<>'0' then
          l53.Caption:= Address.Index;
        l54.Caption:= DateToLongStr(Date1);
      end;
    Inc(LastActiveDirector);
  except
    l48.Caption:= '';
    l49.Caption:= '';
    l50.Caption:= '';
    l51.Caption:= '';
    l52.Caption:= '';
    l53.Caption:= '';
    l54.Caption:= '';
  end;

  try
    if BigDirectors then
      Abort;  // Raises an exception
    // Line #2
    With FCompany.Directors.Items[LastActiveDirector] as TDirector do
      begin
        l55.Caption:= Name;
        l56.Caption:= Zeut;
        l57.Caption:= Address.City;
        l58.Caption:= Address.Street;
        l59.Caption:= Address.Number;
        If Address.Index<>'0' then
          l60.Caption:= Address.Index;
        l61.Caption:= DateToLongStr(Date1);
      end;
    Inc(LastActiveDirector);
  except
    l55.Caption:= '';
    l56.Caption:= '';
    l57.Caption:= '';
    l58.Caption:= '';
    l59.Caption:= '';
    l60.Caption:= '';
    l61.Caption:= '';
  end;

  try
    if BigDirectors then
      Abort;  // Raises an exception
    // Line #3
    With FCompany.Directors.Items[LastActiveDirector] as TDirector do
      begin
        l62.Caption:= Name;
        l63.Caption:= Zeut;
        l64.Caption:= Address.City;
        l65.Caption:= Address.Street;
        l66.Caption:= Address.Number;
        If Address.Index<>'0' then
          l67.Caption:= Address.Index;
        l68.Caption:= DateToLongStr(Date1);
      end;
  except
    l62.Caption:= '';
    l63.Caption:= '';
    l64.Caption:= '';
    l65.Caption:= '';
    l66.Caption:= '';
    l67.Caption:= '';
    l68.Caption:= '';
  end;
  Inc(LastActiveDirector);

  // Fill NonActive Directors
  FCompany.Directors.Filter:= afNonActive;
  try
    if BigPastDirectors then
      Abort;  // Raises an exception
    // Line #1
    While FCompany.Directors.Items[LastNonActiveDirector].LastChanged < FLastDoh do
      Inc(LastNonActiveDirector);
    With FCompany.Directors.Items[LastNonActiveDirector] as TDirector do
      begin
        l69.Caption:= Name;
        l70.Caption:= Zeut;
        l71.Caption:= DateToLongStr(LastChanged);
      end;
    Inc(LastNonActiveDirector);
  except
    l69.Caption:= '';
    l70.Caption:= '';
    l71.Caption:= '';
  end;

  try
    if BigPastDirectors then
      Abort;  // Raises an exception
    // Line #2
    While FCompany.Directors.Items[LastNonActiveDirector].LastChanged < FLastDoh do
      Inc(LastNonActiveDirector);
    With FCompany.Directors.Items[LastNonActiveDirector] as TDirector do
      begin
        l72.Caption:= Name;
        l73.Caption:= Zeut;
        l74.Caption:= DateToLongStr(LastChanged);
      end;
    Inc(LastNonActiveDirector);
  except
    l72.Caption:= '';
    l73.Caption:= '';
    l74.Caption:= '';
  end;

  try
    if BigPastDirectors then
      Abort;  // Raises an exception
    // Line #3
    While FCompany.Directors.Items[LastNonActiveDirector].LastChanged < FLastDoh do
      Inc(LastNonActiveDirector);
    With FCompany.Directors.Items[LastNonActiveDirector] as TDirector do
      begin
        l75.Caption:= Name;
        l76.Caption:= Zeut;
        l77.Caption:= DateToLongStr(LastChanged);
      end;
    Inc(LastNonActiveDirector);
  except
    l75.Caption:= '';
    l76.Caption:= '';
    l77.Caption:= '';
  end;

  with FSender do
  begin
    If bx1.Checked then
      cb1.Picture.Assign(imV.Picture);
    If bx2.Checked then
      cb2.Picture.Assign(imV.Picture);
    If bx3.Checked then
      cb3.Picture.Assign(imV.Picture);
    If bx4.Checked then
      cb4.Picture.Assign(imV.Picture);
    If bx5.Checked then
      cb5.Picture.Assign(imV.Picture);
    If bx6.Checked then
      cb6.Picture.Assign(imV.Picture);
  end;

end; // PrintNextPage1a

procedure TfmQRDoh.PrintNextPage2;
begin
  with FSender do
    begin
      {If (LastManager>0) and (LastManager>= Grid2.RowCount) then
        Exit;   }
      lBigManager.Enabled:= BigManagers;

      If bx7.Checked then
        begin
          cb7.Picture.Assign(imV.Picture);
          l78.Caption:= e15.Text;
          l79.Caption:= e16.Text;
        end;
      If bx8.Checked then
        begin
          cb8.Picture.Assign(imV.Picture);
          l80.Caption:= e17.Text;
        end;
      If bx9.Checked then
        cb9.Picture.Assign(imV.Picture);

      If bx31.Checked then
        ls2.Font.Style:= [fsStrikeOut];
      //If bx32.Checked then
        //ls1.Font.Style:= [fsStrikeOut];

      if not BigManagers then
        If Grid2.RowCount>LastManager then
          begin
            l81.Caption:= Grid2.Cells[0,LastManager];
            l82.Caption:= Grid2.Cells[1,LastManager];
            l83.Caption:= Grid2.Cells[2,LastManager];
            l84.Caption:= Grid2.Cells[3,LastManager];
          end;

      l85.Caption:= e21.Text;
      l86.Caption:= e22.Text;
      l87.Caption:= e23.Text;

      l88.Caption:= e24.Text;
      if e25.Text<>'0' then
        l89.Caption:= e25.Text;

      l90.Caption:= e26.Text;
      If fmPrinterSetup.PrintDateInDoh Then
        l91.Caption:= e31.Text
      Else
        l91.Caption:= '';
      l92.Caption:= e32.Text;
      l93.Caption:= e33.Text;
    end;

  Inc(LastManager);

  //PrintNextPage2;
end;

procedure TfmQRDoh.PrintNextPage2a;
begin
  //==>13.6.05 Tsahi
  with FSender do
  begin
    lLawyer.Caption:= edLawyer.Text;
    lLawyer2.Caption:= edLawyer2.Text;
    lFullNameLawyer.Caption:= edNameLawyer.Text;
    lIDNumLawyer.Caption:= edIDNumLawyer.Text;
    lLawyerAddress.Caption:= edLawyerAddress.Text;
    lLawyerID.Caption:= edLawyerID.Text;
    lLawyerLicense.Caption:= edLicenseLawyer.Text;

    if lLawyerID.Caption='0' then
      lLawyerID.Caption:='';
    if lIDNumLawyer.Caption='0' then
      lIDNumLawyer.Caption:='';
    if lLawyerLicense.Caption='0' then
      lLawyerLicense.Caption:='';

    //==>tsahi 11.9.08: added to comply with changes made by Rasham Ha-Havarot
    lAccountant.Caption:= edAccountant.Text;
    lAccountant2.Caption:= edAccountant2.Text;
    lFullNameAccountant.Caption:= edNameAccountant.Text;
    lIDNumAccountant.Caption:= edIDNumAccountant.Text;
    lAccountantAddress.Caption:= edAccountantAddress.Text;
    lAccountantID.Caption:= edAccountantID.Text;
    lAccountantLicense.Caption:= edLicenseAccountant.Text;

    // Moshe 28/12/2015
  //  lReportYear.Caption := edReportYear.Text;

    if lAccountantID.Caption='0' then
      lAccountantID.Caption:='';
    if lIDNumAccountant.Caption='0' then
      lIDNumAccountant.Caption:='';
    if lAccountantLicense.Caption='0' then
      lAccountantLicense.Caption:='';
    //<== tsahi 11.9.08
  end;
  //<==
  //PrintNextPage2;
end;

procedure TfmQRDoh.PrintBigLists;
var
  I, J: Integer;
  CaptionList: TStringList;
  fullTotalHon: double;
begin
  lCompanyName.Caption:= FCompany.Name;
  lCompanyZeut.Caption:= FCompany.Zeut;
  taMemDirector.Close;
  taMemHon.Close;
  taMemMuhaz.Close;
  taMemPastDirector.Close;
  taMemStocks.Close;
  taMemManager.Close;
  taMemDirector.EmptyTable;
  taMemHon.EmptyTable;
  taMemMuhaz.EmptyTable;
  taMemPastDirector.EmptyTable;
  taMemStocks.EmptyTable;
  taMemManager.EmptyTable;
  taMemDirector.Open;
  taMemHon.Open;
  taMemMuhaz.Open;
  taMemPastDirector.Open;
  taMemStocks.Open;
  taMemManager.Open;
  CaptionList:= TStringList.Create;
  fullTotalHon := 0;

  if BigHon then
    begin
      CaptionList.Add('��� �����');
      for I:= 0 to FCompany.HonList.Count-1 do
        begin
          taMemHon.Append;
          with FCompany.HonList.Items[I] as THonItem do
            begin
              taMemHon.FieldByName('Name').AsString:= Name;
              taMemHon.FieldByName('Value').AsString := FloatToStrF(Value, ffNumber, 15,4);
              taMemHon.FieldByName('Rashum').AsString := FloatToStrF(Rashum, ffNumber, 15,2);
              taMemHon.FieldByName('Mokza').AsString := FloatToStrF(Mokza, ffNumber, 15,2);
              fullTotalHon := fullTotalHon +(Rashum * Value);
            end;
          taMemHon.Post;
        end;

        lblAppendixTotalHon.Caption :=  FloatToStrF(fullTotalHon, ffNumber, 15,4);
        lblFullTotalHon.Caption := FloatToStrF(fullTotalHon, ffNumber, 15,4); // if we got here, we need to fill in the other field too
    end;

  if BigStocks then
    begin
      CaptionList.Add('���� �����');
      for I:= 0 to FCompany.StockHolders.Count-1 do
        with FCompany.StockHolders.Items[I] as TStockHolder do
          for J:= 0 to Stocks.Count-1 do
            begin
              taMemStocks.Append;
              with Stocks.Items[J] as TStock do
                begin
                  taMemStocks.FieldByName('StockName').AsString:= Name;
                  taMemStocks.FieldByName('StockCount').AsString:= FloatToStrF(Count, ffNumber, 15, 2);
                  taMemStocks.FieldByName('StockPaid').AsString:= FloatToStrF(Paid, ffNumber, 15, 2);
                end;
              taMemStocks.FieldByName('Name').AsString:= Name;
              taMemStocks.FieldByName('Zeut').AsString:= Zeut;
              taMemStocks.FieldByName('City').AsString:= Address.City;
              taMemStocks.FieldByName('Street').AsString:= Address.Street;
              taMemStocks.FieldByName('Number').AsString:= Address.Number;
              If Address.Index<>'0' then
                taMemStocks.FieldByName('Index').AsString:= Address.Index;
              taMemStocks.Post;
            end;
    end;

  if BigMuhaz then
    begin
      CaptionList.Add('����� ����"�');
      for I:= 0 to FCompany.MuhazList.Count-1 do
        begin
          taMemMuhaz.Append;
          With FCompany.MuhazList.Items[I] as TMuhazItem do
            begin
              taMemMuhaz.FieldByName('ShtarValue').AsString:= FloatToStrF(ShtarValue, ffNumber, 15, 2);
              taMemMuhaz.FieldByName('Zeut').AsString:= Zeut;
            end;
          taMemMuhaz.Post;
        end;
    end;
        
  if BigDirectors then
    begin
      CaptionList.Add('��������� ������');
      FCompany.Directors.Filter:= afActive;
      for I:= 0 to FCompany.Directors.Count-1 do
        begin
          taMemDirector.Append;
          With FCompany.Directors.Items[I] as TDirector do
            begin
              taMemDirector.FieldByName('Name').AsString:= Name;
              taMemDirector.FieldByName('Zeut').AsString:= Zeut;
              taMemDirector.FieldByName('City').AsString:= Address.City;
              taMemDirector.FieldByName('Street').AsString:= Address.Street;
              taMemDirector.FieldByName('Number').AsString:= Address.Number;
              If Address.Index<>'0' then
                taMemDirector.FieldByName('Index').AsString:= Address.Index;
              taMemDirector.FieldByName('Date1').AsString:= DateToLongStr(Date1);
            end;
          taMemDirector.Post;
        end;
    end;

  if BigPastDirectors then
    begin
      CaptionList.Add('��������� ����� ����');
      FCompany.Directors.Filter:= afNonActive;
      for I:= 0 to FCompany.Directors.Count-1 do
        if FCompany.Directors.Items[I].LastChanged >= FLastDoh then
          begin
            taMemPastDirector.Append;
            With FCompany.Directors.Items[I] as TDirector do
              begin
                taMemPastDirector.FieldByName('Name').AsString:= Name;
                taMemPastDirector.FieldByName('Zeut').AsString:= Zeut;
                taMemPastDirector.FieldByName('Date').AsString:= DateToLongStr(LastChanged);
              end;
            taMemPastDirector.Post;
          end;
      FCompany.Directors.Filter:= afActive;  // Just in case
    end;


  if BigManagers then
    begin
      CaptionList.Add('������');
      for I:= 0 to FSender.Grid2.RowCount-1 do
        begin
          taMemManager.Append;
          taMemManager.FieldByName('Name').AsString := FSender.Grid2.Cells[0,I];
          taMemManager.FieldByName('Zeut').AsString := FSender.Grid2.Cells[1,I];
          taMemManager.FieldByName('Address').AsString := FSender.Grid2.Cells[2,I];
          taMemManager.FieldByName('Telephone').AsString := FSender.Grid2.Cells[3,I];
          taMemManager.Post;
        end;
    end;

  lCaption.Caption:= '.';
  for I:= 0 to CaptionList.Count-1 do
    begin
      lCaption.Caption:= lCaption.Caption + CaptionList[I];
      if I < CaptionList.Count-1 then
        lCaption.Caption:= lCaption.Caption+', ';
    end;
  CaptionList.Free;
  fmPrinterSetup.FormatQR(qrDoh3);
  qrDoh3.Prepare;
  lPageCount.Caption:= IntToStr(qrDoh3.QRPrinter.PageCount+3);
  lPageCount1.Caption:= lPageCount.Caption;
  lPageCount2.Caption:= lPageCount.Caption;
  lPageCount3.Caption:= lPageCount.Caption;
  qrDoh3.QRPrinter.Free;
  qrDoh3.QRPrinter:= nil;
end;

procedure TfmQRDoh.QRSysData1Print(sender: TObject; var Value: String);
begin
  Value:= IntToStr(StrToInt(Value)+3);
end;

end.
