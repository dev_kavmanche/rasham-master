unit PrintTakanonold;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, Db, DBTables, DataObjects;

type
  TfmQRTakanon = class(TForm)
    qrTakanon2: TQuickRep;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    l1: TQRLabel;
    QRLabel31: TQRLabel;
    QRl1: TQRLabel;
    QRl2: TQRLabel;
    QRl3: TQRLabel;
    QRl4: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRl5: TQRLabel;
    QRLabel67: TQRLabel;
    QRl6: TQRLabel;
    QRl7: TQRLabel;
    l2: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    qrTakanon3: TQuickRep;
    QRBand6: TQRBand;
    QRLabel35: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel37: TQRLabel;
    QRBand5: TQRBand;
    QRLabel18: TQRLabel;
    QRDBText5: TQRDBText;
    QRBand4: TQRSubDetail;
    QRLabel19: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    taHon: TTable;
    taStockNM: TTable;
    taStockHolders: TTable;
    QRShape1: TQRShape;
    qrTakanon4: TQuickRep;
    QRBand7: TQRBand;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel78: TQRLabel;
    l3: TQRLabel;
    QRLabel81: TQRLabel;
    QRBand8: TQRBand;
    QRBand9: TQRBand;
    QRLabel87: TQRLabel;
    l4: TQRLabel;
    l24: TQRLabel;
    QRLabel79: TQRLabel;
    l5: TQRLabel;
    QRLabel88: TQRLabel;
    l6: TQRLabel;
    QRLabel89: TQRLabel;
    l7: TQRLabel;
    QRLabel90: TQRLabel;
    l8: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel26: TQRLabel;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    qrTakanon1: TQuickRep;
    QRBand12: TQRBand;
    QRLabel59: TQRLabel;
    QRBand10: TQRBand;
    QRMemo1: TQRMemo;
    QRMemo2: TQRMemo;
    procedure taStockNMFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure PrintProc(DoPrint: Boolean; Company: TCompany; DecisionDate: TDateTime);
  end;

var
  fmQRTakanon: TfmQRTakanon;

implementation

uses Util, PrinterSetup;

{$R *.DFM}

procedure TfmQRTakanon.PrintProc(DoPrint: Boolean; Company: TCompany; DecisionDate: TDateTime);
var
  I,J: Integer;
begin
  l1.Caption:= Company.Name;
  l2.Caption:= Company.CompanyInfo.OtherInfo;
  l3.Caption:= Company.CompanyInfo.LawName;
  l5.Caption:= Company.CompanyInfo.LawName;
  l6.Caption:= Company.CompanyInfo.LawAddress;

  if Company.CompanyInfo.LawZeut<> '0' then
    l7.Caption:= Company.CompanyInfo.LawZeut;
  if Company.CompanyInfo.LawLicence<> '0' then
    l8.Caption:= Company.CompanyInfo.LawLicence;

  l2.Enabled:= l2.Caption <> '';
  QRl1.Enabled:= (Company.CompanyInfo.Option1 and 1) > 0;

  QRl2.Enabled:= (Company.CompanyInfo.Option1 and 2) > 0;

  QRl3.Enabled:= (Company.CompanyInfo.Option1 and 4) > 0;

  QRl4.Enabled:= (Company.CompanyInfo.Option1 and 8) > 0;

  QRl5.Enabled:= Company.CompanyInfo.Option3 = '1';

  QRl6.Enabled:= Company.CompanyInfo.Option3 = '2';

  QRl7.Enabled:= (Company.CompanyInfo.Option3 = '3') or l2.Enabled;
  l2.Enabled:= QRl7.Enabled;
  
  with taHon do
    begin
      with FieldDefs do
        begin
          Clear;
          Add('Name', ftString, 40, False);
          Add('Value',ftString, 20, False);
          Add('Rashum', ftString, 20, False);
          Add('Mokza',ftString,20,False);
        end;
      CreateTable;
      Open;
      For I:= 0 to Company.HonList.Count-1 do
        With Company.HonList.Items[I] as THonItem do
          begin
            Append;
            FieldByName('Name').AsString:= MakeHebStr(Name, False);
            FieldByName('Value').AsString:= FloatToStrF(Value, ffNumber, 15, 4);
            FieldByName('Rashum').AsString:= FloatToStrF(Rashum, ffNumber, 15, 2);
            FieldByName('Mokza').AsString:= FloatToStrF(Mokza, ffNumber, 15, 2);
            Post;
          end;
    end;
  With taStockHolders do
    begin
      With FieldDefs do
        begin
          Clear;
          Add('Name', ftString, 40, False);
          Add('Zeut', ftString, 9, True);
          Add('Address', ftString, 160, False);
        end;
      CreateTable;
      Open;
    end;
  With taStockNM do
    begin
      With FieldDefs do
        begin
          Clear;
          Add('Zeut', ftString, 9, True);
          Add('Name', ftString, 40, False);
          Add('Value', ftString, 20, False);
          Add('Count', ftString, 20, False);
        end;
      CreateTable;
      Open;
    end;

  For I:= 0 to Company.StockHolders.Count-1 do
    With Company.StockHolders.Items[I] as TStockHolder do
      begin
        taStockHolders.Append;
        taStockHolders.FieldByName('Name').AsString:= MakeHebStr(Name, False);
        taStockHolders.FieldByName('Zeut').AsString:= Zeut;
        taStockHolders.FieldByName('Address').AsString:= MakeHebStr(Address.Street+' '+Address.Number+' '+Address.City+' '+Address.Index+' '+Address.Country, True);
        taStockHolders.Post;
        For J:= 0 to Stocks.Count-1 do
          With taStockNM do
            begin
              Append;
              FieldByName('Zeut').AsString:= Zeut;
              With Stocks.Items[J] as TStock do
                begin
                  FieldByName('Name').AsString:= MakeHebStr(Name, False);
                  FieldByName('Value').AsString:= FloatToStrF(Value, ffNumber, 15, 4);
                  FieldByName('Count').AsString:= FloatToStrF(Count, ffNumber, 15, 2);
                end;
              Post;
            end;
      end;

  fmPrinterSetup.FormatQR(qrTakanon1);
  fmPrinterSetup.FormatQR(qrTakanon2);
  fmPrinterSetup.FormatQR(qrTakanon3);
  fmPrinterSetup.FormatQR(qrTakanon4);
  if DoPrint then
    begin
      qrTakanon1.Print;
      qrTakanon2.Print;
      qrTakanon3.Print;
      qrTakanon4.Print;
    end
  Else
    begin
      qrTakanon1.Preview;
      Application.ProcessMessages;
      qrTakanon2.Preview;
      Application.ProcessMessages;
      qrTakanon3.Preview;
      Application.ProcessMessages;
      qrTakanon4.Preview;
      Application.ProcessMessages;
    end;
end;

procedure TfmQRTakanon.taStockNMFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept:= taStockHolders.FieldByName('Zeut').AsString = taStockNM.FieldByName('Zeut').AsString;
end;

end.
