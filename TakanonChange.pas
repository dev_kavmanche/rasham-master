unit TakanonChange;

{$A+,B-,C+,D+,E-,F-,G+,H+,I+,J+,K-,L+,M-,N+,O-,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y-,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}

interface

////////////////////////////////////////////////////
// History:
//   VG19075 15.12.2019 ver 1.7.7
//   added btTakanon7 to page 6, function ExecuteTakanon
////////////////////////////////////////////////////

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HEdit, HComboBx,
  DataObjects, HGroupBx, HRadGrup, SdGrid, HGrids, HRadio, HChkBox;

type
  THamaraInfo = Record
    FDraftHon: THonList;
    FDraftStockHolders: TStockHolders;
    FDraftMuhaz: TMuhazList;
    FNonDraftHonNew, FNonDraftHonOld: THonList;
    FNonDraftStocksAndHolders: TList;
    FNonDraftMuhazOld, FNonDraftMuhazNew: TMuhazList;
  end;

  TfmTakanonChange = class(TfmBlankTemplate)
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    Shape107: TShape;
    Shape82: TShape;
    Shape90: TShape;
    Shape92: TShape;
    Shape93: TShape;
    Shape94: TShape;
    HebLabel54: THebLabel;
    Shape97: TShape;
    HebLabel55: THebLabel;
    Shape98: TShape;
    HebLabel57: THebLabel;
    Shape100: TShape;
    HebLabel58: THebLabel;
    Shape101: TShape;
    Shape102: TShape;
    Shape103: TShape;
    HebLabel59: THebLabel;
    Shape104: TShape;
    Shape105: TShape;
    HebLabel60: THebLabel;
    Shape106: TShape;
    Shape108: TShape;
    Shape109: TShape;
    HebLabel63: THebLabel;
    Shape110: TShape;
    Shape111: TShape;
    Shape112: TShape;
    HebLabel64: THebLabel;
    HebLabel61: THebLabel;
    HebLabel62: THebLabel;
    Shape133: TShape;
    cbTaagid: THebComboBox;
    edtMagishDate: TMaskEdit;
    edtMagishName: THebEdit;
    edtZehut: TEdit;
    edtIndex: TEdit;
    edtStreet: THebEdit;
    cbCities: THebComboBox;
    cbCountries: THebComboBox;
    edtNumber: THebEdit;
    edtPhone: TEdit;
    btSearch: TButton;
    Shape5: TShape;
    Shape6: TShape;
    Shape7: TShape;
    HebLabel3: THebLabel;
    Shape10: TShape;
    HebLabel5: THebLabel;
    edtFax: TEdit;
    edtMail: TEdit;
    Panel1: TPanel;
    Shape24: TShape;
    Shape91: TShape;
    Shape95: TShape;
    Shape96: TShape;
    Shape99: TShape;
    Shape113: TShape;
    HebLabel65: THebLabel;
    Shape25: TShape;
    Shape58: TShape;
    Shape81: TShape;
    chkTakanon2_1: TCheckBox;
    chkTakanon2_2: TCheckBox;
    chkTakanon2_3: TCheckBox;
    chkTakanon2_4: TCheckBox;
    btnTakanon2_1: TSpeedButton;
    btnTakanon2_2: TSpeedButton;
    btnTakanon2_3: TSpeedButton;
    btnTakanon2_4: TSpeedButton;
    Panel2: TPanel;
    Shape114: TShape;
    HebLabel66: THebLabel;
    Shape115: TShape;
    HebLabel67: THebLabel;
    Shape116: TShape;
    Shape117: TShape;
    Shape118: TShape;
    HebLabel68: THebLabel;
    Shape119: TShape;
    HebLabel69: THebLabel;
    Shape120: TShape;
    rbSharesLimited: TRadioButton;
    rbSharesUnlimited: TRadioButton;
    edtOtherInfo: THebEdit;
    Panel3: TPanel;
    grbLimitedSignatory: THebRadioGroup;
    edtLegalSections: THebEdit;
    edtSignatorySections: THebEdit;
    PageControl1: TPageControl;
    TabSheet8: TTabSheet;
    Shape8: TShape;
    Shape9: TShape;
    Shape11: TShape;
    Shape12: TShape;
    HebLabel20: THebLabel;
    HebLabel21: THebLabel;
    HebLabel22: THebLabel;
    HebLabel23: THebLabel;
    HebLabel6: THebLabel;
    HebLabel7: THebLabel;
    HebLabel8: THebLabel;
    HebLabel9: THebLabel;
    HebLabel10: THebLabel;
    HebLabel11: THebLabel;
    edSourceCount: TEdit;
    edDestCount: TEdit;
    cbDestName: THebComboBox;
    cbDestValue: THebComboBox;
    cbSourceName: THebComboBox;
    rbAll: THebRadioButton;
    rbChoose: THebRadioButton;
    cbSourceValue: THebComboBox;
    edCount: TEdit;
    TabSheet9: TTabSheet;
    Shape13: TShape;
    HebLabel12: THebLabel;
    HebLabel13: THebLabel;
    Grid3: TSdGrid;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    btReset: TButton;
    Panel7: TPanel;
    edReset: TEdit;
    pnButtons1: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    TabSheet5: TTabSheet;
    Shape59: TShape;
    Panel9: TPanel;
    Panel8: TPanel;
    Panel11: TPanel;
    Panel26: TPanel;
    Grid: TSdGrid;
    nGrid: THebStringGrid;
    gb21: TPanel;
    gb22: TPanel;
    grbSharesRestrictions: THebGroupBox;
    edtSharesRestrictionsChapters: THebEdit;
    HebLabel4: THebLabel;
    rbSharesRestrictionsYes: THebRadioButton;
    rbSharesRestrictionsNo: THebRadioButton;
    grbDontOffer: THebGroupBox;
    edtDontOfferChapters: THebEdit;
    HebLabel14: THebLabel;
    rbDontOfferYes: THebRadioButton;
    rbDontOfferNo: THebRadioButton;
    grpShareHoldersNumber: THebGroupBox;
    edtShareHoldersNumberChapters: THebEdit;
    HebLabel15: THebLabel;
    rbShareHoldersNumberYes: THebRadioButton;
    rbShareHoldersNumberNo: THebRadioButton;
    HebCheckBox3: THebLabel;
    HebCheckBox2: THebLabel;
    HebCheckBox1: THebLabel;
    btTakanon7: TButton;
    procedure btnTakanon2_2Click(Sender: TObject);
    procedure chkTakanon2_2Click(Sender: TObject);
    procedure btnTakanon2_3Click(Sender: TObject);
    procedure chkTakanon2_3Click(Sender: TObject);
    procedure btnTakanon2_4Click(Sender: TObject);
    procedure chkTakanon2_4Click(Sender: TObject);
    procedure B1Click(Sender: TObject);
    procedure gb22Click(Sender: TObject);
    procedure gb21Click(Sender: TObject);
    procedure btResetClick(Sender: TObject);
    procedure Grid3SelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
    procedure cbSourceNameChange(Sender: TObject);
    procedure cbDestNameChange(Sender: TObject);
    procedure edCountKeyPress(Sender: TObject; var Key: Char);
    procedure Panel13Click(Sender: TObject);
    procedure Panel12Click(Sender: TObject);
    procedure rbChooseClick(Sender: TObject);
    procedure nGridDblClick(Sender: TObject);
    procedure GridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure GridSetEditText(Sender: TObject; ACol, ARow: Integer; const Value: String);
    procedure btShowClick(Sender: TObject);
    procedure btSearchClick(Sender: TObject);
    procedure GridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridClick(Sender: TObject);
    procedure Grid3SetEditText(Sender: TObject; ACol, ARow: Integer; const Value: String);
    procedure edtIndexDblClick(Sender: TObject);
    procedure btPrintClick(Sender: TObject);
    procedure edtMagishNameDblClick(Sender: TObject);
    //VG19075
    procedure btTakanon7Click(Sender: TObject);
  private
    { Private declarations }
    FRecNum:integer;
    FTakanon2_1,
    FTakanon2_2,
    FTakanon2_3,
    FTakanon2_4,
    FTakanon4,
    FTakanon5,
    FTakanon7: String;

    CellSelected: boolean;
    StocksListLoaded: boolean;

    procedure Clear;
    procedure FillData(Company: TCompany);
    procedure FillMagish(Magish: TMagish);
    procedure FillCompanyInfo(CompanyInfo: TCompanyInfo);
    procedure EmptyHon;
    procedure FillCountries;
    procedure FillCities;

    function CheckStocks(Silent: boolean): boolean;
    procedure SetAmount(Amount: Double);
    procedure EvFillSugList;
    procedure EvFillGrid3(Amount: Double);
    procedure FillStocksList;
    function CheckDoubleStocks(ARow: integer; Silent, ma: boolean): boolean;
    function CheckAllDoubleStocks(Silent: boolean): boolean;
    procedure UpdateHonListFromAddHon(CompanyHonList: THonList);
    function CollectCompanyInfo(CompanyInfo: TCompanyInfo): TCompanyInfo;
    function CollectSubmissionData(FormerSubmissionData:TSubmissionData):TSubmissionData;
    procedure UpdateLists(CompanyHonList: THonList; CompanyMuhazList: TMuhazList; CompanyStockHolders: TStockHolders);
    function CollectMagish:TMagish;
    function GetOldStockHolders(Company: TCompany): TStockHolders;
    function GetOldHonList(Company: TCompany): THonList;
    function GetOldMuhazList(Company: TCompany): TMuhazList;
    function GetOldCompanyAddress(Company: TCompany): TCompanyAddress;
    function GetDirectors(Company: TCompany): TDirectors;
    function GetManagers(Company: TCompany): TManagers;
    function CheckExistentStocks(Company: TCompany): boolean;
    procedure SwitchMButtons(HIsActive: boolean);
    procedure CalcTotalStockValues;
    function HamaraData(Draft: Boolean): THamaraInfo;
    function SaveHamara(FileItem: TFileItem): Boolean;
    function SaveHamaraSimple(FileItem: TFileItem): Boolean;
    procedure SaveNewHon(FileItem: TFileItem);
    procedure CheckText(WinControl: TWinControl; Text, Caption: string);
    procedure LoadHamara(FileItem: TFileItem);
    procedure AddToGrid3(const Name, Zeut: string; Value: Double);
    procedure LoadNewHon(FileItem: TFileitem);
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    procedure LoadHonDeleted(FileItem: TFileItem; LoMukza: Double);
    procedure FillHonSource(HonItem: THonItem; LoMukza: Double);
    //VG19075
    function ExecuteTakanon(var Takanon: String; const DefaultTakanon, Caption: String): Boolean;
  protected
    { Protected declarations }
    procedure EvDataLoaded; override;
    function EvLoadData: Boolean; override;
    function EvSaveData(Draft: Boolean): boolean; override;
    procedure EvDeleteEmptyRows; override;
    procedure EvPrintData; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    function EvCollectData(Draft: Boolean): TCompany;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmTakanonChange: TfmTakanonChange;

implementation

uses
  DataM, QRTakanonChange2, LoadDialog, Dialog,
  SaveDialog, Dialog2, Main, PeopleSearch, HDialogs,
  Util, utils2, cSTD,
//VG19075
  TakanonEdit;
//  RisCompany, qrTakanonChange;

type
  TStocksAndHolder = class
    StockHolder: TStockHolder;
    Stocks: TStocks;
  end;

const
  CLoMukza = '��� �� �����';
  CMuhaz = '��� ���"� ����';

{$R *.DFM}

procedure TfmTakanonChange.EvDataLoaded;
var
  i: Integer;
  S: String;
begin
  if (Loading) then
    Exit;

  Loading := True;

  Clear;

  FRecNum := Company.RecNum;
  e13.Text := DateToStr(Date);
  e14.Text := e13.Text;
  lName.Caption := Company.Name;
  lNumber.Caption := Company.Zeut;
  cbTaagid.ItemIndex := 0;

  FillCompanyInfo(Company.CompanyInfo);
  EmptyHon;
  FillCountries;
  FillCities;
  FillMagish(Company.Magish);
  FillStocksList;

  S := cbSourceName.Text;
  cbSourceName.Items.Clear;
  if (Company.HonList <> nil) then
  begin
    Company.HonList.Filter := afActive;
    for i := 0 to Company.HonList.Count-1 do
    begin
      if (cbSourceName.Items.IndexOf(Company.HonList.Items[i].Name) = -1) then
        cbSourceName.Items.Add(Company.HonList.Items[i].Name);
    end;
  end;

  cbSourceName.ItemIndex := cbSourceName.Items.IndexOf(S);
  if not DoNotClear then
  begin
    if (cbSourceName.ItemIndex = -1) and (cbSourceName.Items.Count > 0) then
      cbSourceName.ItemIndex := 0;
    cbSourceNameChange(cbSourceName);
    EvFillSugList;
    if (cbDestName.Items.Count > 0) then
    begin
      cbDestName.ItemIndex := cbDestName.Items.IndexOf(cbSourceName.Text);
      cbDestNameChange(cbDestName);
    end;
    rbAll.Checked := True;
    edSourceCount.Text := '1';
    edDestCount.Text := '1';
    edCount.Text := '100';
    edReset.Text := '100';
    EvFillGrid3(0);
  end;
  Loading := False;

end; // TfmTakanonChange.EvDataLoaded

procedure TfmTakanonChange.Clear;
var
  i: integer;
begin
  for i := 0 to Self.ComponentCount-1 do
  begin
    if (Components[i] is TEdit) then
      (Components[i] as TEdit).Text := '';
    if (Components[i] is THebEdit) then
      (Components[i] as THebEdit).Text := '';
    if (Components[i] is THebComboBox) then
      (Components[i] as THebComboBox).Text := '';
  end;
end;

procedure TfmTakanonChange.SwitchMButtons(HIsActive: boolean);
var
  pnlActive,pnlInactive:TPanel;
begin
  if HIsActive then
  begin
    pnlActive := Panel13;
    pnlInactive := Panel12;
  end
  else
  begin
    pnlActive := Panel12;
    pnlInactive := Panel13;
  end;
  pnlActive.Color := $00ACAC00;
  pnlActive.BevelInner := bvLowered;
  pnlActive.BevelOuter := bvLowered;
  pnlInactive.Color := clTeal;
  pnlInactive.BevelInner := bvRaised;
  pnlInactive.BevelOuter := bvRaised;
end;

procedure TfmTakanonChange.EmptyHon;
var i:integer;
begin
  Grid.RowCount := Grid.FixedRows + 1;
  for i := Grid.FixedCols to Grid.ColCount-1 do
    Grid.Cells[i,Grid.FixedRows] := '';
end;

procedure TfmTakanonChange.EvPrintData;
var
  lCompany: TCompany;
begin
  Application.CreateForm(TfmQrTakanonChange2, fmQrTakanonChange2);
  lCompany := nil;
  try
    // Moshe 23/01/2019 - Reload company for full stock list
    lCompany := TCompany.Create(Company.RecNum, Company.ShowingDate, False);
    fmQrTakanonChange2.PrintExecute(Caption, True, lCompany, StrToDate(e13.Text));
  finally
    if (lCompany <> nil) then lCompany.Free;
    fmQrTakanonChange2.Free;
  end;
end;

procedure TfmTakanonChange.UpdateHonListFromAddHon(CompanyHonList: THonList);
var
  i: integer;
  AName: string;
  ARashum, AValue: double;
  Item: THonItem;
  dtNow: TDateTime;
begin
  dtNow := Now;
  CompanyHonList.Filter := afActive;
  for i := Grid.FixedRows to Grid.RowCount-1 do
  begin
    AName := Trim(Grid.Cells[Grid.FixedCols,i]);
    if AName<>'' then
    begin
      ARashum := str2Float(Grid.Cells[Grid.FixedCols+1,i]);
      AValue := str2Float(Grid.Cells[Grid.FixedCols+2,i]);
      Item := CompanyHonList.FindHonItem(AName, AValue);
      if (Item <> nil) then
        Item.DeActivate(Now);
      CompanyHonList.Add(THonItem.CreateNew(dtNow, AName, AValue, ARashum, 0,0,0,0,0, True));
    end;
  end;
end;

function TfmTakanonChange.CheckExistentStocks(Company: TCompany): boolean;
var
  i: integer;
begin
  Result := False;
  for i := Grid.FixedRows to Grid.RowCount-1 do
  begin
    if (Company.HonList.FindHonItem(Trim(Grid.Cells[Grid.FixedCols,i]), Str2Float(Grid.Cells[Grid.FixedCols+2,i])) <> nil) then
      Exit;
  end;
  Result := True;
end;

procedure TfmTakanonChange.CheckText(WinControl: TWinControl; Text, Caption: string);
const
  C_MustField = '��� ��� ����';
begin
  if (WinControl = nil) then
    Exit;
  if (Text = '') then
  begin
    ErrorAt := WinControl;
    pnB1Click(pnB1);
    raise Exception.Create(Caption + ' ' + C_MustField);
  end;
end;

procedure TfmTakanonChange.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
const
  C_MustField = '��� ��� ����';
  C_ThisField = '��� ��';
  C_MustChoose = '���� ����� ����� ������';
var
  i: Integer;
  SugName, msg: String;
  AllZero: Boolean;
  SugValue: Double;
  HonItem: THonItem;
  Stockholder: TStockHolder;
  Stock: TStock;
  MuhazItem: TMuhazItem;
begin
  if Loading then
    Exit;
  ErrorAt := nil;

  case Page of
  0: begin
       CheckText(edtMagishName,  edtMagishName.Text, HebLabel57.Caption);
       CheckText(edtZehut,       edtZehut.Text,      HebLabel58.Caption);
       CheckText(edtPhone,       edtPhone.Text,      HebLabel64.Caption);
       CheckText(cbCities,       cbCities.Text,      HebLabel62.Caption);
       CheckText(edtMail,        edtMail.Text,       HebLabel5.Caption);
     end;

  1: begin
       if (not chkTakanon2_1.Checked) and (not chkTakanon2_2.checked) and
          (not chkTakanon2_3.Checked) and (not chkTakanon2_4.checked) then
       begin
         ErrorAt := chkTakanon2_1;
         msg := HebLabel65.Caption + ': ' + C_MustChoose;
         pnB1Click(pnB2);
         raise Exception.Create(msg);
         Exit;
       end;
     end;

  2: begin
       if ((not rbSharesLimited.Checked) and (not rbSharesUnlimited.Checked)) then
       begin
         ErrorAt := nil;
         msg := HebLabel66.Caption + ': ' + C_MustChoose;
         pnB1Click(pnB3);
         raise Exception.Create(msg);
         Exit;
       end;
     end;

  3: begin // Hamara
       if (rbAll.Checked and (PageControl1.ActivePage.PageIndex = 1)) then
         Exit;
       if (Company.HonList = nil) then
         Exit;
       if (Company.HonList.Count = 0) then
         Exit;

       Company.HonList.Filter := afActive;
       SugName := cbSourceName.Text;
       SugValue := Str2Float(cbSourceValue.Text);
       HonItem := Company.HonList.FindHonItem(SugName, SugValue);

       Company.StockHolders.Filter := afActive;
       Company.MuhazList.Filter := afActive;

       if (not Assigned(HonItem)) then
       begin
         if (not Silent) then
         begin
           if (cbSourceName.Color = clWindow) then
             cbSourceName.Color := clBtnFace
           else
             cbSourceName.Color := clWindow;
         end;
         pnB1Click(pnB4);
         SwitchMButtons(True);
         PageControl1.ActivePage := Self.TabSheet8;
         ErrorAt := cbSourceName;
         raise Exception.Create('��� ���� �� �� ���� �����');
         Exit;
       end;

       if (cbDestName.Text = '') then
       begin
         if (not Silent) then
         begin
           if (cbDestName.Color = clWindow) then
             cbDestName.Color := clBtnFace
           else
             cbDestName.Color := clWindow;
         end;
         ErrorAt := cbDestName;
         pnB1Click(pnB5);
         raise Exception.Create('��� ��� ���');
         Exit;
       end;

       if Str2Float(edSourceCount.Text) <= 0 then
       begin
         if not Silent then
           with edSourceCount do
             if Color = clWindow then
               Color := clBtnFace
             else
               Color := clWindow;
         pnB1Click(pnB5);
         SwitchMButtons(True);
         PageControl1.ActivePage := Self.TabSheet8;
         ErrorAt := edSourceCount;
         raise Exception.Create('��� ���� �� ����');
         Exit;
       end;

       if Str2Float(edDestCount.Text)<= 0 then
       begin
         if not Silent then
           with edDestCount do
             if Color = clWindow then
               Color := clBtnFace
             else
               Color := clWindow;
         pnB1Click(pnB5);
         SwitchMButtons(True);
         PageControl1.ActivePage := Self.TabSheet8;
         ErrorAt := edDestCount;
         raise Exception.Create('��� ���� �� ����');
         Exit;
       end;

       if rbAll.Checked and (Str2Float(edCount.Text) > 100) then
       begin
         if not Silent then
           with edCount do
             if Color = clWindow then
               Color := clBtnFace
             else
               Color := clWindow;
         pnB1Click(pnB5);
         SwitchMButtons(True);
         PageControl1.ActivePage := Self.TabSheet8;
         ErrorAt := edCount;
         raise Exception.Create('�� ���� ����� ���� �-100% �������.');
         Exit;
       end;

       if rbAll.Checked then
         Exit;

       CheckGrid(Page, Silent, Extended, Grid3);
       AllZero := True;
       for i := 0 to Grid3.RowCount-1 do
       begin
         if (Trim(Grid3.Cells[0,i]) <> '') and (Str2Float(Grid3.Cells[3,i]) <> 0) then
         begin
           AllZero := False;
           if Str2Float(Grid3.Cells[3,i])<0 then
           begin
             if not Silent then
             begin
               Grid3.Row := i;
               Grid3.Col := 3;
             end;
             pnB1Click(pnB5);
             SwitchMButtons(False);
             PageControl1.ActivePage := TabSheet9;
             ErrorAt := Grid3;
             raise Exception.Create('���� ������ ����� ���� ����� ����� ������.');
             Exit;
           end
           else if Grid3.Cells[0,i] = cLoMukza then
           begin
             if (HonItem=nil) or ((HonItem.Mokza + Str2Float(Grid3.Cells[3,i])) > HonItem.Rashum) then
             begin
               If not Silent Then
               begin
                 Grid3.Row := i;
                 Grid3.Col := 3;
               end;
               pnB1Click(pnB5);
               SwitchMButtons(False);
               PageControl1.ActivePage := TabSheet9;
               ErrorAt := Grid3;
               raise Exception.Create('����� "'+cLoMukza+'" ����� ����� ���� ����� ����� ����� ���"� ������');
               Exit;
             end;
           end
           else if Grid3.Cells[0,i] = cMuhaz then
           begin
             MuhazItem := Company.MuhazList.FindByZeut(Grid3.Cells[1,i]) as TMuhazItem;
             if not Assigned(MuhazItem) then
             begin
               if not Silent then
               begin
                 Grid3.Row := i;
                 Grid3.Col := 0;
               end;
               pnB1Click(pnB5);
               SwitchMButtons(False);
               PageControl1.ActivePage := TabSheet9;
               ErrorAt := Grid3;
               raise Exception.Create('��� �� �� ����');
               Exit;
             end;
           end
           else
           begin
             StockHolder := Company.StockHolders.FindByZeut(Grid3.Cells[1,i]) as TStockHolder;
             if Assigned(StockHolder) then
             begin
               StockHolder.Stocks.Filter := afActive;
               Stock := Stockholder.Stocks.FindStock(SugName, SugValue);
               if Assigned(Stock) then
               begin
                 if Stock.Count < Str2Float(Grid3.Cells[3,i]) then
                 begin
                   if not Silent then
                   begin
                     Grid3.Row := i;
                     Grid3.Col := 3;
                   end;
                   pnB1Click(pnB5);
                   SwitchMButtons(False);
                   PageControl1.ActivePage := TabSheet9;
                   ErrorAt := Grid3;
                   raise Exception.Create('���� ����� '+ StockHolder.Name+' �� �� '+FloatToStr(Stock.Count)+ ' �����.');
                   Exit;
                 end;
               end
               else
               begin
                 if not Silent then
                 begin
                   Grid3.Row := i;
                   Grid3.Col := 3;
                 end;
                 pnB1Click(pnB5);
                 SwitchMButtons(False);
                 PageControl1.ActivePage := TabSheet9;
                 ErrorAt := Grid3;
                 raise Exception.Create('���� ����� '+StockHolder.Name+' ��� ����� ���� '+cbSourceName.Text+' '+cbSourceValue.Text);
                 Exit;
               end;
             end
             else
             begin
               if not Silent then
               begin
                 Grid3.Row := I;
                 Grid3.Col := 0;
               end;
               pnB1Click(pnB5);
               SwitchMButtons(False);
               PageControl1.ActivePage := TabSheet9;
               ErrorAt := Grid3;
               raise Exception.Create('����� ��� ��� ����� ��� '+ Grid3.Cells[0,I]);
               Exit;
             end;
           end;  // StockHolders Check
         end; // If Grid3 Row is not empty
       end; // for

         // This should be outside "for"
       if AllZero then
       begin
         if Not Silent then
         begin
           Grid3.Row := 0;
           Grid3.Col := 3;
         end;
         pnB1Click(pnB5);
         SwitchMButtons(False);
         PageControl1.ActivePage := TabSheet9;
         ErrorAt := Grid3;
         raise Exception.Create('����� ����� ����� ����� ����� ����');
         Exit;
       end;
       if (not CheckExistentStocks(Company))or(not CheckAllDoubleStocks(Silent)) then
       begin
         ErrorAt := Grid;
         raise Exception.Create('������ ������');
         Exit;
       end;
     end; // 4: begin

  5: begin // 175
       if ((not rbSharesRestrictionsYes.Checked) and (not rbSharesRestrictionsNo.Checked)) then
       begin
         ErrorAt := grbSharesRestrictions;
         msg := grbSharesRestrictions.Caption + ': ' + C_MustChoose;
         pnB1Click(pnB6);
         raise Exception.Create(msg);
         Exit;
       end;

       if ((not rbDontOfferYes.Checked) and (not rbDontOfferNo.Checked)) then
       begin
         ErrorAt := grbDontOffer;
         msg := grbDontOffer.Caption + ': ' + C_MustChoose;
         pnB1Click(pnB6);
         raise Exception.Create(msg);
         Exit;
       end;

       if ((not rbShareHoldersNumberYes.Checked) and (not rbShareHoldersNumberNo.Checked)) then
       begin
         ErrorAt := grpShareHoldersNumber;
         msg := grpShareHoldersNumber.Caption + ': ' + C_MustChoose;
         pnB1Click(pnB6);
         raise Exception.Create(msg);
         Exit;
       end;
     end; // 5: begin

  6: begin
       if grbLimitedSignatory.ItemIndex < 0 then
       begin
         ErrorAt := grbLimitedSignatory;
         msg := grbLimitedSignatory.Caption + ': ' + C_MustChoose;
         pnB1Click(pnB7);
         raise Exception.Create(msg);
         Exit;
       end;
     end; // 6: begin

  end;   // Of Case PAGE

end; // TfmTakanonChange.EvCheckPage

procedure TfmTakanonChange.EvFillSugList;
var
  s: string;
begin
  cbDestName.Items.Clear;
  Data.taStocks.First;
  while not Data.taStocks.EOF do
  begin
    s := Trim(Data.taStocks.Fields[0].Text);
    if cbDestName.Items.IndexOf(s) = -1 then
      cbDestName.Items.Add(s);
    Data.taStocks.Next;
  end;
end;

procedure TfmTakanonChange.DeleteFile(FileItem: TFileItem);
begin
  MessageDlg('�� ���� ����� �����  ���� ��', mtError, [mbOk], 0);
end;

procedure TfmTakanonChange.SetAmount(Amount: Double);
var
  I: Integer;
begin
  Amount := Amount/100; // Convert Percents
  for I := 0 to Grid3.RowCount-1 do
    if Grid3.Cells[0,I] = cMuhaz then
      if Amount=0 then
        Grid3.Cells[3,I] := '0'
      else
        Grid3.Cells[3,I] := '1'
    else
      Grid3.Cells[3,I] := FormatNumber(FloatToStr(Str2Float(Grid3.Cells[2,I])*Amount), 15,2,True);
end;

procedure TfmTakanonChange.FillStocksList;
var
  i: integer;
  IsOpen: boolean;
begin
  if StocksListLoaded then
    Exit;
  if (nGrid.ColCount < nGrid.FixedCols+2) then
    nGrid.ColCount := nGrid.FixedCols+2;
  IsOpen := DATA.taStocks.Active;
  if (not IsOpen) then
    DATA.taStocks.Open;
  if (DATA.taStocks.RecordCount > 0) then
  begin
    nGrid.RowCount := DATA.taStocks.RecordCount+nGrid.FixedRows;
    DATA.taStocks.First;
    i := nGrid.FixedRows;
    while (not DATA.taStocks.EOF) do
    begin
      nGrid.Cells[nGrid.FixedCols, i+nGrid.FixedRows] := IntToStr(i + 1 - nGrid.FixedRows);
      nGrid.Cells[nGrid.FixedCols+1, i+nGrid.FixedRows] := Data.taStocks.Fields[0].AsString;
      DATA.taStocks.Next;
      inc(i);
    end;
  end
  else
  begin
    nGrid.RowCount := 1 + nGrid.FixedRows;
    for i := nGrid.FixedCols to nGrid.ColCount-1 do
      nGrid.Cells[i,nGrid.FixedRows] := '';
  end;
  if (not IsOpen) then
    Data.taStocks.Close;
  StocksListLoaded := True;
end; // TfmTakanonChange.FillStocksList

procedure TfmTakanonChange.FillCities;
begin
  DATA.taCity.First;
  while (not DATA.taCity.EOF) do
  begin
    cbCities.Items.Add(DATA.taCity.Fields[0].Text);
    DATA.taCity.Next;
  end;
end;

procedure TfmTakanonChange.FillCountries;
begin
  DATA.taCountry.First;
  while (not DATA.taCountry.EOF) do
  begin
    cbCountries.Items.Add(DATA.taCountry.Fields[0].Text);
    DATA.taCountry.Next;
  end;
  cbCountries.Text := '�����';
end;

function TfmTakanonChange.EvLoadData: Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileitem;
  TmpCompany: TCompany;
begin
  Result := False;
  if DataChanged then
  begin
    fmDialog.memo.lines.Clear;
    fmDialog.memo.lines.add('��� ��');
    fmDialog.memo.lines.add('����� ��� ����� ���� ������ ������.');
    fmDialog.memo.lines.add('? ��� ����� �� ������� ����');
    fmDialog.ShowModal;
    if (fmDialog.DialogResult <> drYES) then
      Exit;
  end;

  FileItem := fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faTakanon);
  if (FileItem = nil) then
    Exit;

  Loading := True;
  DecisionDate := (FileItem.FileInfo.DecisionDate);
  NotifyDate := (FileItem.FileInfo.NotifyDate);

  try
    try
      if FileItem.FileInfo.Draft then
        TmpCompany := TCompany.LoadFromFileTakanonChange(FileItem, FRecNum)
      else
        TmpCompany := TCompany.LoadFromFile(Company, FileItem);
      if Assigned(Company) then
        Company.Free;
      Company := TmpCompany;
      Company.HonList.Filter := afActive;
    except on E: Exception do
      begin
        ShowMessage(E.Message);
        Exit;
      end;
    end;
    FillData(Company);
    FileName := FileItem.FileInfo.FileName;
    FileIsDraft := FileItem.FileInfo.Draft;
    LoadHamara(FileItem);
    LoadNewHon(FileItem);
    PageIndex := 0;
    Loading := False;
  finally
    FileItem.Free;
    FilesList.Free;
  end;
  Result := True;
end; // TfmTakanonChange.EvLoadData

procedure TfmTakanonChange.FillData(Company: TCompany);
begin
  if (Company = nil) then
    FillMagish(nil)
  else
    FillMagish(Company.Magish);
  FillCompanyInfo(Company.CompanyInfo);
  EmptyHon;

end; // TfmTakanonChange.FillData

procedure TfmTakanonChange.FillMagish(Magish: TMagish);
begin
  if (Magish = nil) then
  begin
    edtMagishName.Text := '';
    cbTaagid.ItemIndex := 0;
    edtZehut.Text    := '';
    edtPhone.Text    := '';
    edtStreet.Text   := '';
    edtNumber.Text   := '';
    edtIndex.Text    := '';
    cbCities.Text    := '';
    cbCountries.Text := '�����';
    edtMail.Text     := '';
    edtFax.Text      := '';
  end
  else
  begin
    edtMagishName.Text := Magish.Name;
    if (Magish.Taagid) then
      cbTaagid.ItemIndex := 1
    else
      cbTaagid.ItemIndex := 0;
    edtZehut.Text    := Magish.Zeut;
    edtPhone.Text    := Magish.Phone;
    edtStreet.Text   := Magish.Street;
    edtNumber.Text   := Magish.Number;
    edtIndex.Text    := Magish.Index;
    cbCities.Text    := Magish.City;
    cbCountries.Text := Magish.Country;
    edtMail.Text     := Magish.Mail;
    edtFax.Text      := Magish.Fax;
  end;
end;

procedure TfmTakanonChange.FillCompanyInfo(CompanyInfo: TCompanyInfo);
var
  isNil: boolean;
begin
  isNil := False;
  if CompanyInfo=nil then
    isNil := True;
  if (isNil) then
  begin
    e13.Text := DateToStr(Now);

    chkTakanon2_1.Checked := False;
    chkTakanon2_2.Checked := False;
    chkTakanon2_3.Checked := False;
    chkTakanon2_4.Checked := False;

    FTakanon2_1 := DefaultTakanon2;
    FTakanon2_2 := DefaultTakanon2;
    FTakanon2_3 := DefaultTakanon2;
    FTakanon2_4 := DefaultTakanon2;
    FTakanon4  := DefaultTakanon2;
    FTakanon5  := DefaultTakanon2;
    FTakanon7  := DefaultTakanon2;

    rbSharesLimited.Checked   := False;
    rbSharesUnlimited.Checked := False;
    edtOtherInfo.Text         := '';

    edtSharesRestrictionsChapters.Text := '';
    edtDontOfferChapters.Text          := '';
    edtShareHoldersNumberChapters.Text := '';
    grbLimitedSignatory.ItemIndex      := -1;
    edtLegalSections.Text              := '';
    edtSignatorySections.Text          := '';
  end
  else
  begin
    e13.Text := DateToStr(Now);//CompanyInfo.RegistrationDate);

    chkTakanon2_1.Checked := (CompanyInfo.Option1 and 1) > 0;
    chkTakanon2_2.Checked := (CompanyInfo.Option1 and 2) > 0;
    chkTakanon2_3.Checked := (CompanyInfo.Option1 and 4) > 0;
    chkTakanon2_4.Checked := (CompanyInfo.Option1 and 8) > 0;

    FTakanon2_1 := CompanyInfo.Takanon2_1;
    FTakanon2_2 := CompanyInfo.Takanon2_2;
    FTakanon2_3 := CompanyInfo.Takanon2_3;
    FTakanon2_4 := CompanyInfo.Takanon2_4;
    FTakanon4   := CompanyInfo.Takanon4;
    FTakanon5   := CompanyInfo.Takanon5;
    FTakanon7   := CompanyInfo.Takanon7;

    rbSharesLimited.Checked            := CompanyInfo.Option3 = '1';
    rbSharesUnlimited.Checked          := CompanyInfo.Option3 = '3';
    if (CompanyInfo.Option3 = '3') then
      edtOtherInfo.Text                := CompanyInfo.OtherInfo;

    rbSharesRestrictionsYes.Checked    := (CompanyInfo.SharesRestrictIndex = 0);
    rbDontOfferYes.Checked             := (CompanyInfo.DontOfferIndex = 0);
    rbShareHoldersNumberYes.Checked    := (CompanyInfo.ShareHoldersNoIndex = 0);

    if (rbSharesRestrictionsYes.Checked) then
      edtSharesRestrictionsChapters.Text := CompanyInfo.SharesRestrictChapters;

    if (rbDontOfferYes.Checked) then
      edtDontOfferChapters.Text          := CompanyInfo.DontOfferChapters;

    if (rbShareHoldersNumberYes.Checked) then
      edtShareHoldersNumberChapters.Text := CompanyInfo.ShareHoldersNoChapters;

    grbLimitedSignatory.ItemIndex      := CompanyInfo.LimitedSignatoryIndex;
    if (grbLimitedSignatory.ItemIndex = 1) then
      edtLegalSections.Text            := CompanyInfo.LegalSectionsChapters;
    if (grbLimitedSignatory.ItemIndex = 2) then
      edtSignatorySections.Text        := CompanyInfo.SignatorySectionsChapters;

    rbSharesRestrictionsNo.Checked     := (not rbSharesRestrictionsYes.Checked);
    rbDontOfferNo.Checked              := (not rbDontOfferYes.Checked);
    rbShareHoldersNumberNo.Checked     := (not rbShareHoldersNumberYes.Checked);
  end;
end;

procedure TfmTakanonChange.btnTakanon2_2Click(Sender: TObject);
begin
  if (chkTakanon2_2.Checked) then
  begin
    DataChanged := True;
    ExecuteTakanon(FTakanon2_2, DefaultTakanon2, btnTakanon2_2.Caption);
  end;
end;

procedure TfmTakanonChange.chkTakanon2_2Click(Sender: TObject);
begin
  inherited;
  if (not Loading) then
    btnTakanon2_2Click(Sender);
end;

procedure TfmTakanonChange.btnTakanon2_3Click(Sender: TObject);
begin
  if (chkTakanon2_3.Checked) then
  begin
    DataChanged := True;
    ExecuteTakanon(FTakanon2_3, DefaultTakanon2, btnTakanon2_3.Caption);
  end;
end;

procedure TfmTakanonChange.chkTakanon2_3Click(Sender: TObject);
begin
  inherited;
  if (not Loading) then
    btnTakanon2_3Click(Sender);
end;

procedure TfmTakanonChange.btnTakanon2_4Click(Sender: TObject);
begin
  if (chkTakanon2_4.Checked) then
  begin
    DataChanged := True;
    ExecuteTakanon(FTakanon2_4, DefaultTakanon2, btnTakanon2_4.Caption);
  end;
end;

procedure TfmTakanonChange.chkTakanon2_4Click(Sender: TObject);
begin
  inherited;
  if (not Loading) then
    btnTakanon2_4Click(Sender);
end;

procedure TfmTakanonChange.EvFillGrid3(Amount: Double);
var
  i: Integer;
  Stock: TStock;
  SelName: String;
  SelValue: Double;
  HonItem: THonItem;
  MuhazItem: TMuhazItem;
begin
  Grid3.RowCount := 1;
  Grid3.ResetLine(0);
  Company.StockHolders.Filter := afActive;
  if (Company.HonList.Count < 1) or (cbSourceName.ItemIndex = -1) then
  begin
    Grid3.Options := Grid3.Options-[goEditing];
  end
  else
  begin
    SelName := cbSourceName.Text;
    SelValue := Str2Float(cbSourceValue.Text);
    HonItem := Company.HonList.FindHonItem(SelName, SelValue);
    if Assigned(HonItem) and (HonItem.Rashum > HonItem.Mokza) then
    begin
      Grid3.Cells[0,Grid3.RowCount-1] := cLoMukza;
      Grid3.Cells[1,Grid3.RowCount-1] := '';
      Grid3.Cells[2,Grid3.RowCount-1] := FormatNumber(FloatToStr(HonItem.Rashum-HonItem.Mokza),15,2,True);
      Grid3.Cells[3,Grid3.RowCount-1] := '0.00';
      Grid3.RowCount := Grid3.RowCount+1;
    end;

    Company.MuhazList.Filter := afActive;
    for i := 0 to Company.MuhazList.Count-1 do
    begin
      MuhazItem := Company.MuhazList.Items[i] as TMuhazItem;
      If (MuhazItem.Name = SelName) and (MuhazItem.Value = SelValue) then
      begin
        Grid3.Cells[0,Grid3.RowCount-1] := cMuhaz;
        Grid3.Cells[1,Grid3.RowCount-1] := MuhazItem.Zeut;
        Grid3.Cells[2,Grid3.RowCount-1] := '1';
        Grid3.Cells[3,Grid3.RowCount-1] := '0.00';
        Grid3.RowCount := Grid3.RowCount+1;
      end;
    end;

    for i := 0 to Company.StockHolders.Count-1 do
      with Company.StockHolders.Items[i] as TStockHolder do
      begin
        Stocks.Filter := afActive;
        Stock := Stocks.FindStock(SelName, SelValue);
        if Assigned(Stock) then
        begin
          Grid3.Cells[0,Grid3.RowCount-1] := Company.StockHolders.Items[I].Name;
          Grid3.Cells[1,Grid3.RowCount-1] := Company.StockHolders.Items[I].Zeut;
          Grid3.Cells[2,Grid3.RowCount-1] := FormatNumber(FloatToStr(Stock.Count), 15, 2, True);
          Grid3.Cells[3,Grid3.RowCount-1] := '0.00';
          Grid3.RowCount := Grid3.RowCount+1;
        end;
      end;
  end;
  if Grid3.RowCount > 1 then
    Grid3.RowCount := Grid3.RowCount-1;
  SetAmount(Amount);
end;

function TfmTakanonChange.GetOldStockHolders(Company: TCompany): TStockHolders;
var
  res: TStockHolders;
  sh, s: TStockHolder;
  i, j: integer;
  stocks: TStocks;
  AStock: TSTock;
  addr: tStockHolderAddress;
begin
  res := TStockHolders.CreateNew;
  Company.StockHolders.Filter := afAll;
  for i := 0 to Company.StockHolders.Count-1 do
  begin
    s := TStockHolder(Company.StockHolders.Items[i]);
    stocks := TSTocks.CreateNew;
    s.Stocks.Filter := afAll;
    for j := 0 to s.Stocks.Count-1 do
    begin
      AStock := TStock(s.Stocks.Items[j]);
      stocks.Add(TStock.CreateNew(AStock.LastCHanged, AStock.Name, AStock.Value, AStock.Count, AStock.Paid, AStock.Active));
    end;
    addr := TStockHolderAddress.CreateNew(s.Address.Street, s.Address.Number, s.Address.Index, s.Address.City, s.Address.Country);
    sh := TStockHolder.CreateNew(s.LastChanged, s.Name, s.Zeut, s.Taagid, addr, stocks, s.Active);
    res.Add(sh);
  end;
  Result := res;
end;

function TfmTakanonChange.GetOldMuhazList(Company: TCompany): TMuhazList;
var
  res:TMuhazList;
  i:integer;
  OldItem,NewItem:TMuhazItem;
begin
  res := TMuhazList.CreateNew;
  Company.MuhazList.Filter := afAll;
  for i := 0 to Company.MuhazList.Count-1 do
  begin
    OldItem := TMuhazItem(Company.MuhazList.Items[i]);
    NewItem := TMuhazItem.CreateNew(OldItem.LastChanged,OldItem.Name, OldItem.Zeut,OldItem.Value, OldItem.ShtarValue,OldItem.Active);
    res.Add(NewItem);
  end;
  Result := res;
end;

function TfmTakanonChange.GetOldHonList(Company: TCompany): THonList;
var
  i: integer;
  List: THonList;
  OldItem, NewItem: THonItem;
begin
  List := THonList.CreateNew;
  Company.HonList.Filter := afAll;
  for i := 0 to Company.HonList.Count-1 do
  begin
    OldItem := THonItem(Company.HonList.Items[i]);
    NewItem := THonItem.CreateNew(OldItem.LastChanged, OldItem.Name, OldItem.Value, OldItem.Rashum, OldItem.Mokza,
      OldItem.Cash, OldItem.NonCash, OldItem.Part, OldItem.PartValue,OldItem.Active);
    List.Add(NewItem);
  end;
  Result := List;
end;

function TfmTakanonChange.GetOldCompanyAddress(Company: TCompany): TCompanyAddress;
begin
  with Company.Address do
    Result := TCompanyAddress.CreateNew(Street, Number, Index, City, Country, POB, Ezel, Phone, Fax);
end;

function TfmTakanonChange.GetDirectors(Company: TCompany): TDirectors;
var
  res:TDirectors;
  i:integer;
  OldItem,NewItem:TDirector;
  taagid:TTaagid;
  address:TDirectorAddress;
begin
  res := TDirectors.CreateNew;
  Company.Directors.Filter := afAll;
  for i := 0 to Company.Directors.Count-1 do
  begin
    OldItem := TDirector(Company.Directors.Items[i]);
    address := TDirectorAddress.CreateNew(OldItem.Address.Street, OldItem.Address.Number,
      OldItem.Address.Index, OldItem.Address.City, OldItem.Address.Country, OldItem.Address.Phone);
    if OldItem.Taagid=nil then
      taagid := nil
    else
      taagid := TTaagid.CreateNew(OldItem.Taagid.Street, OldItem.Taagid.Number, OldItem.Taagid.Index,
        OldItem.Taagid.City, OldItem.Taagid.Country, OldItem.Taagid.Name, OldItem.Taagid.Zeut);
    // Moshe 2017-12-25 OldItem.Name
    NewItem := TDirector.CreateNew(OldItem.LastChanged, OldItem.Name, OldItem.Zeut, OldItem.Date1,
      OldItem.Date2, address, taagid, OldItem.Active);
    res.Add(NewItem);
  end;
  Result := res;
end;

function  TfmTakanonChange.GetManagers(Company: TCompany): TManagers;
var
  res:TManagers;
  i:integer;
  OldItem,NewItem:TManager;
begin
  res := TManagers.CreateNew;
  Company.Managers.Filter := afAll;
  for i := 0 to Company.Managers.Count-1 do
  begin
    OldItem := TManager(Company.Managers.Items[i]);
    NewItem := TManager.CreateNew(OldItem.LastChanged, OldItem.Name, OldItem.Zeut, OldItem.Address,
      OldItem.Phone, OldItem.Mail);
    res.Add(NewItem);
  end;
  Result := res;
end;

function TfmTakanonChange.EvCollectData(Draft: Boolean): TCompany;
var
  FormerCompany: TCompany;
  CompanyStockHolders: TStockHolders;
  CompanyHonList: THonList;
  CompanyMuhazList: TMuhazList;
begin
  FormerCompany := TCompany.Create(FRecNum, Company.ShowingDate, False);
  CompanyStockHolders := GetOldStockHolders(FormerCompany);
  CompanyHonList := GetOldHonList(FormerCompany);
  CompanyMuhazList := GetOldMuhazList(FormerCompany);
  UpdateHonListFromAddHon(CompanyHonList);

  // Moshe 07/02/2018 - Comment. 15/08/2018 Cancel the comment
  if (cbDestName.Text <> cbSourceName.Text) or
     (Str2Float(cbDestValue.Text) <> Str2Float(cbSourceValue.Text)) then
    UpdateLists(CompanyHonList, CompanyMuhazList, CompanyStockHolders);

  Result := TCompany.CreateNew(
    Company.ShowingDate,
    FormerCompany.Name,
    FormerCompany.Zeut,
    GetOldCompanyAddress(FormerCompany),
    GetDirectors(FormerCompany),
    CompanyStockHolders,
    CompanyHonList,
    CompanyMuhazList,
    GetManagers(FormerCompany),
    CollectCompanyInfo(Company.CompanyInfo),
    CollectSubmissionData(FormerCompany.SubmissionData),
    CollectMagish);
  FormerCompany.Free;
end;

function TfmTakanonChange.CollectCompanyInfo(CompanyInfo: TCompanyInfo): TCompanyInfo;
var
  Option1: Byte;
  Option3: Char;
begin
  Result := nil;
  try
    Option1 := 0;
    if (chkTakanon2_1.Checked) then
      Option1 := Option1 or 1;
    if (chkTakanon2_2.Checked) then
      Option1 := Option1 or 2;
    if (chkTakanon2_3.Checked) then
      Option1 := Option1 or 4;
    if (chkTakanon2_4.Checked) then
      Option1 := Option1 or 8;

    Option3 := '1';
    if (rbSharesUnlimited.Checked) then
      Option3 := '3';
    CompanyInfo.OtherInfo                 := '';
    if (Option3 = '3') then
      CompanyInfo.OtherInfo               := edtOtherInfo.Text;

    CompanyInfo.RegistrationDate          := StrToDate(e13.Text);
    CompanyInfo.Option1                   := Option1;
    CompanyInfo.Option3                   := Option3;
    CompanyInfo.Takanon2_1                := FTakanon2_1;
    CompanyInfo.Takanon2_2                := FTakanon2_2;
    CompanyInfo.Takanon2_3                := FTakanon2_3;
    CompanyInfo.Takanon2_4                := FTakanon2_4;
    CompanyInfo.Takanon4                  := FTakanon4;
    CompanyInfo.Takanon5                  := FTakanon5;
    CompanyInfo.Takanon7                  := FTakanon7;

    // Moshe 14/08/2018
    CompanyInfo.SharesRestrictChapters := '';
    CompanyInfo.DontOfferChapters := '';
    CompanyInfo.ShareHoldersNoChapters := '';

    CompanyInfo.SharesRestrictIndex := iif(rbSharesRestrictionsYes.Checked, 0, 1);
    if (rbSharesRestrictionsYes.Checked) then
      CompanyInfo.SharesRestrictChapters := edtSharesRestrictionsChapters.Text;

    CompanyInfo.DontOfferIndex := iif(rbDontOfferYes.Checked, 0, 1);
    if (rbDontOfferYes.Checked) then
      CompanyInfo.DontOfferChapters := edtDontOfferChapters.Text;

    CompanyInfo.ShareHoldersNoIndex := iif(rbShareHoldersNumberYes.Checked, 0, 1);
    if (rbShareHoldersNumberYes.Checked) then
      CompanyInfo.ShareHoldersNoChapters := edtShareHoldersNumberChapters.Text;

    // Moshe 13/08/2018
    CompanyInfo.LegalSectionsChapters := '';
    CompanyInfo.SignatorySectionsChapters := '';
    CompanyInfo.LimitedSignatoryIndex := grbLimitedSignatory.ItemIndex;
    if (grbLimitedSignatory.ItemIndex = 1) then
      CompanyInfo.LegalSectionsChapters := edtLegalSections.Text;
    if (grbLimitedSignatory.ItemIndex = 2) then
      CompanyInfo.SignatorySectionsChapters := edtSignatorySections.Text;

    // Moshe 27/01/2019
    CompanyInfo.LastChanged := Now;

    Result := CompanyInfo;
  except on E: Exception do
    WriteLog('Error TfmTakanonChange.CollectCompanyInfo: ' + E.Message, GetLogFileName);
  end;
end; // TfmTakanonChange.CollectCompanyInfo

function TfmTakanonChange.CollectSubmissionData(FormerSubmissionData:TSubmissionData): TSubmissionData;
begin
  Result := TSubmissionData.Create(nil);
  Result.ExFillData(
    StrToDate(e14.Text),
    edtMagishName.Text,
    FormerSubmissionData.RepresentativeRole,
    edtPhone.Text,
    edtMail.Text,
    FormerSubmissionData.AltHebrewName1,
    FormerSubmissionData.AltEnglishName1,
    FormerSubmissionData.AltHebrewName2,
    FormerSubmissionData.AltEnglishName2,
    FormerSubmissionData.AltHebrewName3,
    FormerSubmissionData.AltEnglishName3,
    FormerSubmissionData.GetCertificate,
    edtMail.Text,
    edtFax.Text,
    FormerSubmissionData.GetCopies,
    FormerSubmissionData.TakanonCopies,
    FormerSubmissionData.CertificateCopies,
    FormerSubmissionData.Payment,
    FormerSubmissionData.Ezel,
    FormerSubmissionData.DocsByMail);
end;

procedure TfmTakanonChange.UpdateLists(CompanyHonList: THonList; CompanyMuhazList: TMuhazList; CompanyStockHolders: TStockHolders);
var
  SugName, StockName, StockZeut: string;
  SugValue, Hamara, ChangeAmount, LoMukza, CountAll: double;
  i: integer;
  MuhazItem: TMuhazItem;
 // Stocks:TStocks;
  StockHolder: TStockHolder;
  StockSourceOld, StockSourceNew, StockDestOld, StockDestNew: TStock;
  DestName: string;
  DestValue, Paid: double;
  HonItemSourceOld, HonItemSourceNew, HonItemDestOld, HonItemDestNew: THonItem;
begin
  SugName := Trim(cbSourceName.Text);
  SugValue := Str2Float(cbSourceValue.Text);
  DestName := Trim(cbDestName.Text);
  DestValue := Str2Float(cbDestValue.Text);
  if (SugName = DestName) and (SugValue = DestValue) then Exit;

  if (rbAll.Checked) then
    EvFillGrid3(Str2Float(edCount.Text));

  Hamara := Str2Float(edDestCount.Text) / Str2Float(edSourceCount.Text);
  LoMukza := 0;
  CountAll := 0;
  for I := Grid3.FixedRows to Grid3.RowCount-1 do
  begin
    ChangeAmount := Str2Float(Grid3.Cells[Grid3.FixedCols+3,I]);
    if ChangeAmount>0 then
    begin
      StockName := Trim(Grid3.Cells[Grid3.FixedCols,i]);
      StockZeut := Trim(Grid3.Cells[Grid3.FixedCols+1,I]);
      if StockName=cLoMukza then
      begin
        LoMukza := ChangeAmount;
        CountAll := CountAll+ChangeAmount;
      end
      else if StockName=cMuhaz then
      begin
        MuhazItem := CompanyMuhazList.FindByZeut(StockZeut) as TMuhazItem;
        if MuhazItem<>nil then
        begin
          MuhazItem.Deactivate(DecisionDate);
          CountAll := CountAll+MuhazItem.ShtarValue;
          CompanyMuhazList.Add(TMuhazItem.CreateNew(DecisionDate, cbDestName.Text, StockZeut, Str2Float(cbDestValue.Text), MuhazItem.ShtarValue * Hamara , True));
        end;
        CompanyMuhazList.Add(TMuhazItem.CreateNew(DecisionDate, SugName, StockZeut, SugValue, 1, True));
      end
      else if StockName<>'' then
      begin
        CountAll := CountAll+ChangeAmount;
        StockHolder := CompanyStockHolders.FindByZeut(StockZeut) as TStockHolder;
        if StockHolder<>nil then
        begin
          StockDestOld := StockHolder.Stocks.FindStock(DestName,DestValue);
          StockSourceOld := StockHolder.Stocks.FindStock(SugName, SugValue);
          Paid := 0;
          if StockDestOld<>nil then
          begin
            Paid := StockDestOld.Paid;
            StockDestOld.DeActivate(DecisionDate);
          end;
          StockSourceNew := nil;
          if StockSourceOld<>nil then
          begin
            StockSourceOld.DeActivate(DecisionDate);
            if StockSourceOld.Count > ChangeAmount then
              StockSourceNew := TStock.CreateNew(DecisionDate, StockSourceOld.Name,
                  StockSourceOld.Value, StockSourceOld.Count-ChangeAmount, StockSourceOld.Paid, True);
          end;
          if StockSourceNew=nil then
            StockDestNew := TStock.CreateNew(DecisionDate, DestName, DestValue, ChangeAmount*Hamara, Paid, True)
          else
            StockDestNew := TStock.CreateNew(DecisionDate, DestName, DestValue,
                        StockSourceNew.Count + ChangeAmount*Hamara, Paid, True);
          if StockSourceNew<>nil then
            StockHolder.Stocks.Add(StockSourceNew);
          if StockDestNew<>nil then
            StockHolder.Stocks.Add(StockDestNew);
        end; //if stockholder<>nil
      end; //if stockname<>nil
    end; //if changeamount>0
  end; //for

   { if rbAll.Checked then
        TempPart := MyStrToFloat(edCount.Text)
      else
        TempPart := -1;  }
  HonItemSourceNew := nil;
  HonItemSourceOld := CompanyHonlist.FindHonItem(SugName, SugValue);
  if HonItemSourceOld <> nil then
  begin
    if HonItemSourceOld.Rashum > CountAll then
      HonItemSourceNew := THonItem.CreateNew(DecisionDate, HonItemSourceOld.Name, HonItemSourceOld.Value,
        HonItemSourceOld.Rashum-CountAll, HonItemSourceOld.Mokza-CountAll+LoMukza, HonItemSourceOld.Cash,
        HonItemSourceOld.NonCash, HonItemSourceOld.Part, HonItemSourceOld.PartValue, True);

    HonItemSourceOld.DeActivate(DecisionDate);
  end;
  HonItemDestOld := Company.HonList.FindHonItem(DestName, DestValue);
  if HonItemDestOld = nil then
    HonItemDestNew := THonItem.CreateNew(DecisionDate, DestName, DestValue, CountAll*Hamara,
      (CountAll-LoMukza)*Hamara, 0, 0, 0, 0, True)
  else
  begin
    HonItemDestNew := THonItem.CreateNew(DecisionDate, DestName, DestValue,
      HonItemDestOld.Rashum+CountAll*Hamara, HonItemDestOld.Mokza + (CountAll-LoMukza)*Hamara,
      HonItemDestOld.Cash, HonItemDestOld.NonCash, HonItemDestOld.Part, HonItemDestOld.PartValue, True);
    HonItemDestOld.Deactivate(DecisionDate);
  end;
  if HonItemSourceNew <> nil then
    CompanyHonList.Add(HonItemSourceNew);
  if HonItemDestNew <> nil then
    CompanyHonList.Add(HonItemDestNew);

end; // TfmTakanonChange.UpdateLists

function TfmTakanonChange.CollectMagish:TMagish;
begin
  Result := TMagish.CreateNew(edtMagishName.Text,edtZehut.Text,edtStreet.Text,edtNumber.Text,
      edtIndex.Text,cbCities.Text,cbCountries.Text,edtPhone.Text,edtMail.Text,edtFax.Text,(cbTaagid.ItemIndex=1));
end;

function TfmTakanonChange.EvSaveData(Draft: Boolean): boolean;
var
  Magish: TMagish;
  MagishRecNum, CompInfoRecNum: integer;
  Saved: boolean;
  FileInfo: TFileInfo;
  FilesList: TFilesList;
  FileItem: TFileItem;
  i: integer;
  TempCompany: TCompany;
begin
  Result := False;
  Saved := False;
  FilesList := nil;
  FileItem := nil;
  Magish := nil;

  if (CheckErrors(False, True, False)) then
    Exit;
  if (not CheckStocks(False)) then
    Exit;

  try
    EvDeleteEmptyRows;

    FileInfo := TFileInfo.Create;
    FileInfo.FileName := Company.Name;
    FileInfo.DecisionDate := StrToDate(e13.Text);
    FileInfo.NotifyDate := StrToDate(e14.Text);
    FileInfo.Draft := Draft;
    FileInfo.Action := faTakanon;
    FileInfo.CompanyNum := FRecNum;
    FilesList := TFilesList.Create(FRecNum, faTakanon, Draft);

    FileItem := fmSaveDialog.Execute(Self, FilesList, FileInfo);
    if (not Assigned(FileItem)) then Exit;

    Company.LastChanged := Now;
    CollectCompanyInfo(Company.CompanyInfo);

    if (not Draft) then
    begin
      Company.CompanyInfo.ExSaveData(FileItem, Company);
      Company.Address.ExSaveData(FileItem, Company, FileItem.FileInfo.DecisionDate);
      SaveHamara(FileItem);
      FileItem.FileInfo.Draft := Draft;
      SaveNewHon(FileItem);
      FileItem.SaveChange(ocCompany, Company.RecNum, acRecNum, Company.RecNum);
      Saved := True;
    end;

    if (Draft or (not Saved)) then
    begin
      if (not FilesList.FileExists(FileInfo.FileName, i)) then
      begin
        FilesList.Add(FileInfo);
        FileItem.FileInfo.RecNum := FileInfo.RecNum;
        Company.ExSaveData(FileItem, nil);
      end
      else
      begin
        CompInfoRecNum := FileItem.GetDataRecNum(FRecNum, faTakanon, ocCompany, acSignName);
        if (CompInfoRecNum > 0) then
          Saved := Company.CompanyInfo.UpdateData(Company, CompInfoRecNum);
        if ((CompInfoRecNum < 1) or (not Saved)) then
          Company.CompanyInfo.ExSaveData(FileItem, Company);
      end;
    end;

    if (edtMagishName.Text <> '') then
    begin
      Magish := TMagish.CreateNew(
        edtMagishName.Text,
        edtZehut.Text,
        edtStreet.Text,
        edtNumber.Text,
        edtIndex.Text,
        cbCities.Text,
        cbCountries.Text,
        edtPhone.Text,
        edtMail.Text,
        edtFax.Text,
        (cbTaagid.ItemIndex = 1));
      if (Draft) then
      begin
        MagishRecNum := FileItem.GetLastChange(FileItem.FileInfo.RecNum, ocCompany, Company.RecNum, acNewMagish);
        if (MagishRecNum > 0) then
          Magish.UpdateData(MagishRecNum)
        else
          Magish.SaveData(Company, FileItem);
      end
      else
      begin
        Magish.SaveFixed(Company, FileItem.FileInfo.RecNum);
      end;
    end;

    // Moshe 25/12/2017
    TempCompany := TCompany.Create(Company.RecNum, NotifyDate, False);
    Company.Free;
    Company := TempCompany;
    DataChanged := False;
    if (not Draft) then
    begin
      B1.Enabled := False;
      B2.Enabled := False;
    end;
    fmDialog2.Show;
    Result := True;

  finally
    if (Magish <> nil) then
      Magish.Free;
    //if (Assigned(FileInfo)) then FileInfo.Free; FileInfo := nil;
    if (Assigned(FileItem)) then
      FileItem.Free;
    if (Assigned(FilesList)) then
      FilesList.Free;
  end;
end; // TfmTakanonChange.EvSaveData

procedure TfmTakanonChange.B1Click(Sender: TObject);
var
  Draft: boolean;
begin
  try
    Draft := False;
    if (Sender <> nil) then
      if (Sender is TSpeedButton) then
        if (UpperCase((Sender as TSpeedButton).Name) = 'B2') then
          Draft := True;
    EvDeleteEmptyRows;
    if (EvSaveData(Draft)) then
      DataChanged := False;
  finally
    B1.Down := False;
    B2.Down := False;
  end;
end;

function TfmTakanonChange.CheckStocks(Silent: boolean): boolean;
var
  i, j: Integer;
  vi, vj: double;
begin
  Result := False;

  for i := 0 To Grid.RowCount-1 do
  begin
    for j := i+1 to Grid.RowCount-1 do
    begin
      try
        vi := StrToFloat(Grid.Cells[2, i]);
      except
        vi := -1;
      end;

      try
        vj := StrToFloat(Grid.Cells[2, j]);
      except
        vj := -2;
      end;

      if ((Grid.Cells[0, i] = Grid.Cells[0, j]) and (vi = vj)) then
      begin
        PageControl.ActivePage := TabSheet5;
        Grid.Row := j;
        Grid.Col := 0;
        if (not Silent) then
          HebMessageDlg(
            '�� ���� ����� ���� ���� ���� ���� ���',
            mtError, [mbOk], 0);
        Exit;
      end;
    end;

    if ((Grid.Cells[0, i] <> '') and (nGrid.Cols[1].IndexOf(Grid.Cells[0, i]) = -1)) then
    begin
      PageControl.ActivePage := TabSheet5;
      Grid.Row := i;
      Grid.Col := 0;
      if (not Silent) then
        HebMessageDlg(
          '���� �� �����',
          mtError, [mbOk], 0);
        Exit;
    end;
  end;
  Result := True;
end; // TfmTakanonChange.CheckStocks


procedure TfmTakanonChange.EvDeleteEmptyRows;
var
  Row: Integer;
begin
  for Row := Grid.RowCount-1 downto Grid.FixedRows do
  begin
    if ((RemoveSpaces(Grid.Cells[0, Row], rmBoth) = '') or
        (CheckNumberField(Grid.Cells[1, Row])) or
        (CheckNumberField(Grid.Cells[2, Row]))) then
    begin
      Grid.LineDeleteByIndex(Row);
    end;
  end;
end;

procedure TfmTakanonChange.gb22Click(Sender: TObject);
begin
  if Trim(Grid.Cells[Grid.FixedCols,Grid.FixedRows]) <> '' then
    Grid.LineInsert;
end;

procedure TfmTakanonChange.gb21Click(Sender: TObject);
begin
  if (Grid.RowCount > Grid.FixedRows+1) then
  begin
    Grid.LineDelete;
    nGrid.Visible := False;
  end
  else
    EmptyHon;
end;

procedure TfmTakanonChange.btResetClick(Sender: TObject);
begin
  SetAmount(Str2Float(edReset.Text));
end;

procedure TfmTakanonChange.Grid3SelectCell(Sender: TObject; Col,
  Row: Integer; var CanSelect: Boolean);
begin
  inherited;
  if (Company.HonList=Nil)or(Company.StockHolders=Nil) then
    Exit;
  Company.StockHolders.Filter := afActive;
  Grid3.Options := Grid3.Options-[goEditing];
  if (Company.HonList.Count>0) and (cbSourceName.ItemIndex<>-1) and (Col = 3) then
    Grid3.Options := Grid3.Options+[goEditing];
end;

procedure TfmTakanonChange.cbSourceNameChange(Sender: TObject);

  procedure EvFillValues;
  var
    i: Integer;
  begin
    if Company.HonList = nil then
      Exit;
    Company.HonList.Filter := afActive;
    cbSourceValue.Items.Clear;
    for i := 0 to Company.HonList.Count-1 do
    begin
      if (Company.HonList.Items[i].Name = cbSourceName.Text) then
        cbSourceValue.Items.Add(FormatNumber(FloatToStr((Company.HonList.Items[i] as THonItem).Value), 15, 4, True));
    end;
  end;

var
  OldText: String;
begin
  CriticalEditChange(Sender);
  if Sender = cbSourceName then
  begin
    OldText := cbSourceValue.Text;
    EvFillValues;
    cbSourceValue.ItemIndex := cbSourceValue.Items.IndexOf(OldText);
    if (cbSourceValue.ItemIndex = -1) and (cbSourceName.Items.Count > 0) then
      cbSourceValue.ItemIndex := 0;
  end;
  EvFillGrid3(Str2Float(edCount.Text));
end;

procedure TfmTakanonChange.cbDestNameChange(Sender: TObject);
var
  i: Integer;
begin
  if (Company.HonList = nil) then
    Exit;
  CriticalEditChange(Sender);
  cbDestValue.Items.Clear;

  for i := 0 to Company.HonList.Count-1 do
  begin
    with Company.HonList.Items[i] as THonItem do
      if (Name = cbDestName.Text) then
        cbDestValue.Items.Add(FormatNumber(FloatToStr(Value),15,4,True));
  end;

  if (cbDestValue.Items.Count > 0) then
    cbDestValue.ItemIndex := 0;
end;

procedure TfmTakanonChange.edCountKeyPress(Sender: TObject; var Key: Char);
begin
  ControlKeyPress(Sender, Key);
  if Key <> #0 then
    rbAll.Checked := True;
end;

procedure TfmTakanonChange.Panel13Click(Sender: TObject);
begin
  SwitchMButtons(True);
  PageControl1.ActivePage := TabSheet8;
end;

procedure TfmTakanonChange.Panel12Click(Sender: TObject);
begin
  SwitchMButtons(False);
  PageControl1.ActivePage := TabSheet9;
end;

procedure TfmTakanonChange.rbChooseClick(Sender: TObject);
var
  h:integer;
  ck:boolean;
begin
  CriticalEditChange(Sender);
  ck := (rbChoose.Checked) and (not rbAll.Checked);
  if ck then
    h := 0
  else
    h := 40;
  PageControl1.Height := 196+h;
  pnButtons1.Visible := ck;
end;

procedure TfmTakanonChange.nGridDblClick(Sender: TObject);
begin
  if not nGrid.Visible then
    Exit;

  CriticalEditChange(Sender);
  if Company.HonList.FindHonItem(Trim(nGrid.Cells[nGrid.FixedCols+1, nGrid.Row]),
                    Str2Float(Grid.Cells[Grid.FixedCols+2, Grid.Row])) = nil then
    if CheckDoubleStocks(Grid.Row, False, True) then
      Grid.Cells[Grid.FixedCols, Grid.Row] := nGrid.Cells[nGrid.FixedCols+1, nGrid.Row];

  nGrid.Visible := False;
  CheckStocks(False);
end;

function TfmTakanonChange.CheckAllDoubleStocks(Silent:boolean):boolean;
var i:integer;
begin
  Result := False;
  for i := 0 to Grid.RowCount-1 do
    if not CheckDoubleStocks(i,Silent,True) then
      Exit;
  Result := True;
end;

function TfmTakanonChange.CheckDoubleStocks(ARow:integer;Silent,ma:boolean):boolean;
var
  AName:string;
  AValue:Double;
  i:integer;
  found:boolean;
  MakeActive:boolean;
begin
  AName := Trim(Grid.Cells[0,ARow]);
  AValue := Str2Float(Grid.Cells[2,ARow]);
  i := 0;
  found := False;
  repeat
    if (Trim(Grid.Cells[0,i])=AName)and(Str2Float(Grid.Cells[2,i])=AValue)and(i<>ARow)then
      found := true
    else
      inc(i);
  until (i>=Grid.RowCount)or(found);
  Result := not found;
  if not found then
    Exit;
  if not Silent then
    MessageDlg('�� ���� ������ ����� ���',mtError,[mbOK],0);
  if not ma then Exit;
  MakeActive := False;
  if ActiveControl=nil then
    MakeActive := True
  else if ActiveControl.Name<>Grid.Name then
    MakeActive := True;
  if PageControl.ActivePage.Name<>TabSheet5.Name then
    pnB1Click(pnB5);
  if PageControl1.ActivePage.Name<>TabSheet9.Name then
  begin
      SwitchMButtons(False);
     PageControl1.ActivePage := TabSheet9;
  end;
  if Grid.Row<>ARow then
    Grid.Row := ARow;
  if Grid.Col<>0 then
    Grid.Col := 0;
  if MakeActive then
    ActiveControl := Grid;
end;

procedure TfmTakanonChange.GridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
var
  c0, c1, c2, c3: integer;
begin
  if CellSelected then
    Exit;

  CellSelected := True;
  c0 := Grid.FixedCols;
  c1 := Grid.FixedCols+1;
  c2 := Grid.FixedCols+2;
  c3 := Grid.FixedCols+3;
  if ((Col=c1)or(Col=c2))then
    Grid.Options := Grid.Options+[goEditing]
  else
    Grid.Options := Grid.Options-[goEditing];
  Grid.Cells[c3,Row] := Float2Str(Str2Float(Grid.Cells[c1,Row])*Str2Float(Grid.Cells[c2,Row]),2);

  if (Company.HonList.FindHonItem(Trim(Grid.Cells[c0,Row]), Str2Float(Grid.Cells[c2,Row])) <> nil) then
    MessageDlg('����� ������',mtError,[mbOK],0)
  else
    CheckDoubleStocks(Row,False,False);

  nGrid.Visible := Col=0;
  CellSelected := False;
end;

procedure TfmTakanonChange.FormCreate(Sender: TObject);
begin
  FRecNum := -1;
  CellSelected := False;
  StocksLIstLoaded := False;
  FileAction := faTakanon;
  ShowStyle := afActive;

  SwitchMButtons(True);
  PageControl1.ActivePage := TabSheet8;
  inherited;
  FillStocksLIst;
end;

procedure TfmTakanonChange.GridSetEditText(Sender: TObject; ACol, ARow: Integer; const Value: String);
begin
  CriticalEditChange(Sender);
  if (ACol = 0) or (ACol = 2) then
  begin
    if (not CheckDoubleStocks(ARow, False, False)) then
      Grid.Cells[ACol, ARow] := '';
  end;
end;

procedure TfmTakanonChange.btShowClick(Sender: TObject);
begin
  fmMain.ShowCompanyInfo(Company, DecisionDate);
end;

procedure TfmTakanonChange.btSearchClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany := Company;
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
  begin
    edtMagishName.SetFocus;
    TempCompany.Free;
    Exit;
  end;

  Loading := True;
  if (FResult is TStockHolder) then
    with FResult as TStockHolder do
    begin
      if (Taagid) Then
        cbTaagid.ItemIndex := 1
      else
        cbTaagid.ItemIndex := 0;
      edtMagishName.Text := Name;
      edtZehut.Text := Zeut;
      edtPhone.Text := '';
      edtStreet.Text := Address.Street;
      edtNumber.Text := Address.Number;
      edtIndex.Text := Address.Index;
      cbCities.Text := Address.City;
      cbCountries.Text := Address.Country;
    end;

  if (FResult is TDirector) then
    with FResult as TDirector do
    begin
      if Taagid <> nil Then
        cbTaagid.ItemIndex := 1
      else
        cbTaagid.ItemIndex := 0;
      edtMagishName.Text := Name;
      edtZehut.Text := Zeut;
      edtPhone.Text := Address.Phone;
      edtStreet.Text := Address.Street;
      edtNumber.Text := Address.Number;
      edtIndex.Text := Address.Index;
      cbCities.Text := Address.City;
      cbCountries.Text := Address.Country;
    end;
  Loading := False;
  DataChanged := True;
  edtMagishName.SetFocus;
end; // TfmTakanonChange.btSearchClick

procedure TfmTakanonChange.CalcTotalStockValues;
var
  i:integer;
begin
  for i := Grid.FixedRows to Grid.RowCount-1 do
  begin
    Grid.Cells[Grid.FixedCols+3, i] := Float2Str(Str2Float(Grid.Cells[Grid.FixedCols+1,i]) *
      Str2Float(Grid.Cells[Grid.FixedCols+2, i]), 2);
  end;
end;

procedure TfmTakanonChange.GridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  CalcTotalStockValues;
  DataChanged := True;
end;

procedure TfmTakanonChange.GridClick(Sender: TObject);
begin
  inherited;
  if (Grid.PCol <> Grid.FixedCols) or (nGrid.Visible) then
    Exit;
  nGrid.Visible := True;
  Application.ProcessMessages;
end;

procedure TfmTakanonChange.Grid3SetEditText(Sender: TObject; ACol, ARow: Integer; const Value: String);
begin
  inherited;
  if ACol = Grid3.FixedCols+3 then
    DataChanged := True;
end;

function TfmTakanonChange.HamaraData(Draft: Boolean): THamaraInfo;
var
  SugName: String;
  SugValue: Double;
  HonItemSourceOld, HonItemSourceNew, HonItemDestOld: THonItem;
  TempPart: Double;
  Hamara: Double;
  LoMukza: Double;
  Stocks: TStocks;
  CountAll: Double;
  ChangeAmount: Double;
  MuhazZeut: String;
  i: Integer;
  MuhazItem: TMuhazItem;
  StockHolder: TStockHolder;
  StockSourceOld, StockSourceNew, StockDestOld: TStock;
  StocksAndHolder: TStocksAndHolder;
begin
  Result.FDraftHon := nil;
  try
    if (rbAll.Checked) then
    begin
      if ((cbSourceName.Text = cbDestName.Text) and (cbSourceValue.Text = cbDestValue.Text)) then
        Exit;
      EvFillGrid3(MyStrToFloat(edCount.Text));
    end;
    SugName := cbSourceName.Text;
    SugValue := MyStrToFloat(cbSourceValue.Text);
    HonItemSourceOld := Company.HonList.FindHonItem(SugName, SugValue);
    Hamara := MyStrToFloat(edDestCount.Text) / MyStrToFloat(edSourceCount.Text);
    Result.FDraftHon := THonList.CreateNew;
    Result.FDraftMuhaz := TMuhazList.CreateNew;
    Result.FDraftStockHolders := TStockHolders.CreateNew;

    if (not Draft) then
    begin
      Result.FNonDraftHonNew := THonList.CreateNew;
      Result.FNonDraftHonOld := THonList.CreateNew;
      Result.FNonDraftStocksAndHolders := TList.Create;
      Result.FNonDraftMuhazNew := TMuhazList.CreateNew;
      Result.FNonDraftMuhazOld := TMuhazList.CreateNew;
    end;

    LoMukza := 0;
    CountAll := 0;

    for i := 0 to Grid3.RowCount-1 do
    begin
      ChangeAmount := MyStrToFloat(Grid3.Cells[3, i]);
      if (ChangeAmount > 0) then
      begin
        if (Grid3.Cells[0, i] = cLoMukza) then
        begin
          LoMukza := ChangeAmount;
          CountAll := CountAll+ChangeAmount;
        end
        else if (Grid3.Cells[0, i] = cMuhaz) then
        begin
          MuhazZeut := Grid3.Cells[1,i];
          Result.FDraftMuhaz.Add(TMuhazItem.CreateNew(DecisionDate, SugName, MuhazZeut, SugValue, 1, True));
          if (not Draft) then
          begin
            MuhazItem := Company.MuhazList.FindByZeut(MuhazZeut) as TMuhazItem;
            MuhazItem.DeActivate(DecisionDate);
            CountAll := CountAll+MuhazItem.ShtarValue;
            Result.FNonDraftMuhazOld.Add(MuhazItem);
            Result.FNonDraftMuhazNew.Add(
              TMuhazItem.CreateNew(
                DecisionDate,
                cbDestName.Text,
                MuhazZeut,
                MyStrToFloat(cbDestValue.Text),
                MuhazItem.ShtarValue * Hamara ,
                True));
          end;
        end
        else
        begin
          CountAll := CountAll+ChangeAmount;
          Stocks := TStocks.CreateNew;
          Stocks.Add(TStock.CreateNew(DecisionDate, SugName, SugValue, MyStrToFloat(Grid3.Cells[3, i]), 0, True));
          Result.FDraftStockHolders.Add(
            TStockHolder.CreateNew(
              DecisionDate,
              Grid3.Cells[0, i],
              Grid3.Cells[1, i],
              False,
              TStockHolderAddress.CreateNew('','','','',''),
              Stocks,
              True));
          if (not Draft) then
          begin
            StockHolder := Company.StockHolders.FindByZeut(Grid3.Cells[1,i]) as TStockHolder;
            StocksAndHolder := TStocksAndHolder.Create;
            StocksAndHolder.Stocks := TStocks.CreateNew;
            StocksAndHolder.StockHolder := StockHolder;
            StockSourceOld := StockHolder.Stocks.FindStock(SugName, SugValue);
            StockSourceOld.DeActivate(DecisionDate);
            StocksAndHolder.Stocks.Add(StockSourceOld);
            if (StockSourceOld.Count > ChangeAmount) then
              StockSourceNew := TStock.CreateNew(DecisionDate, StockSourceOld.Name, StockSourceOld.Value, StockSourceOld.Count-ChangeAmount, StockSourceOld.Paid, True)
            else
              StockSourceNew := nil;

            StockDestOld := StockHolder.Stocks.FindStock(cbDestName.Text, MyStrToFloat(cbDestValue.Text));
            if (Assigned(StockDestOld)) then
            begin
              if (StockDestOld = StockSourceOld) then
              begin
                if Assigned(StockSourceNew) then // not changing 100% of stocks
                begin
                  StocksAndHolder.Stocks.Add(TStock.CreateNew(DecisionDate, StockDestOld.Name, StockDestOld.Value, StockSourceNew.Count + ChangeAmount*Hamara, StockDestOld.Paid, True));
                  StockSourceNew:= nil;
                end
                else
                  StocksAndHolder.Stocks.Add(TStock.CreateNew(DecisionDate, StockDestOld.Name, StockDestOld.Value, ChangeAmount*Hamara, StockDestOld.Paid, True))
              end
              else
              begin
                StockDestOld.DeActivate(DecisionDate);
                StocksAndHolder.Stocks.Add(StockDestOld);
                StocksAndHolder.Stocks.Add(TStock.CreateNew(DecisionDate, StockDestOld.Name, StockDestOld.Value, StockDestOld.Count+ChangeAmount*Hamara, StockDestOld.Paid, True));
              end
            end
            else
            begin
              StocksAndHolder.Stocks.Add(TStock.CreateNew(DecisionDate, cbDestName.Text, MyStrToFloat(cbDestValue.Text), ChangeAmount*Hamara, 0, True));
            end;

            if (Assigned(StockSourceNew)) then
              StocksAndHolder.Stocks.Add(StockSourceNew);
            Result.FNonDraftStocksAndHolders.Add(StocksAndHolder);
          end;
        end;
      end;
    end;

    if (rbAll.Checked) then
      TempPart := MyStrToFloat(edCount.Text)
    else
      TempPart := -1;
    Result.FDraftHon.Add(
      THonItem.CreateNew(
        DecisionDate,
        SugName,
        SugValue,
        1,
        LoMukza,
        MyStrToFloat(edSourceCount.Text),
        MyStrToFloat(edDestCount.Text),
        TempPart,
        0,
        True));
    Result.FDraftHon.Add(
      THonItem.CreateNew(
        DecisionDate,
        cbDestName.Text,
        MyStrToFloat(cbDestValue.Text),
        2, 0, 0, 0, 0, 0, True));

    if (not Draft) then
    begin
      HonItemSourceOld.DeActivate(DecisionDate);
      Result.FNonDraftHonOld.Add(HonItemSourceOld);
      if (HonItemSourceOld.Rashum > CountAll) then
        HonItemSourceNew := THonItem.CreateNew(
          DecisionDate,
          HonItemSourceOld.Name,
          HonItemSourceOld.Value,
          HonItemSourceOld.Rashum - CountAll,
          HonItemSourceOld.Mokza - CountAll+LoMukza,
          HonItemSourceOld.Cash,
          HonItemSourceOld.NonCash,
          HonItemSourceOld.Part,
          HonItemSourceOld.PartValue, True)
      else
        HonItemSourceNew := nil;
      HonItemDestOld := Company.HonList.FindHonItem(cbDestName.Text, MyStrToFloat(cbDestValue.Text));
      if (Assigned(HonItemDestOld)) then
      begin
        if (HonItemDestOld = HonItemSourceOld) then   // If Dest and Source are same
        begin
          if (Assigned(HonItemSourceNew)) then  // Not changing 100%
          begin
            Result.FNonDraftHonNew.Add(
              THonItem.CreateNew(
                DecisionDate,
                HonItemSourceNew.Name,
                HonItemSourceNew.Value,
                HonItemSourceNew.Rashum + CountAll * Hamara,
                HonItemSourceNew.Mokza + (CountAll - LoMukza) * Hamara,
                HonItemSourceNew.Cash,
                HonItemSourceNew.NonCash,
                HonItemSourceNew.Part,
                HonItemSourceNew.PartValue,
                True));
            HonItemSourceNew := nil;
          end
          else
            Result.FNonDraftHonNew.Add(
              THonItem.CreateNew(
                DecisionDate,
                HonItemSourceOld.Name,
                HonItemSourceOld.Value,
                CountAll * Hamara,
                (CountAll - LoMukza) * Hamara,
                HonItemSourceOld.Cash,
                HonItemSourceOld.NonCash,
                HonItemSourceOld.Part,
                HonItemSourceOld.PartValue,
                True))
        end
        else
        begin
          HonItemDestOld.DeActivate(DecisionDate);
          Result.FNonDraftHonOld.Add(HonItemDestOld);
          Result.FNonDraftHonNew.Add(
            THonItem.CreateNew(
              DecisionDate,
              HonItemDestOld.Name,
              HonItemDestOld.Value,
              HonItemDestOld.Rashum + CountAll * Hamara,
              HonItemDestOld.Mokza + (CountAll - LoMukza) * Hamara,
              HonItemDestOld.Cash,
              HonItemDestOld.NonCash,
              HonItemDestOld.Part,
              HonItemDestOld.PartValue,
              True));
        end
      end
      else
      begin
        Result.FNonDraftHonNew.Add(
          THonItem.CreateNew(
            DecisionDate,
            cbDestName.Text,
            MyStrToFloat(cbDestValue.Text),
            CountAll * Hamara,
            (CountAll - LoMukza) * Hamara,
            0, 0, 0, 0,
            True));
      end;

      if (Assigned(HonItemSourceNew)) then
        Result.FNonDraftHonNew.Add(HonItemSourceNew);
    end;
  except on E: Exception do
    WriteLog('Error TfmTakanonChange.HamaraData: ' + E.Message, GetLogFileName);
  end;
end; // TfmTakanonChange.HamaraData

procedure TfmTakanonChange.edtIndexDblClick(Sender: TObject);
begin
  {$IFDEF MOSHE}
    edtMagishName.Text := '����� ���';
    edtZehut.Text := '812345021';
    edtPhone.Text := '054-8741230';
    edtStreet.Text := '��� �����';
    edtNumber.Text := '7';
    edtIndex.Text := '70812';
    cbCities.Text := '���� ����';
    cbCountries.Text := '�����';
    edtMail.Text := 'Mail@There.com';
    chkTakanon2_1.Checked := True;
    cbDestValue.Text := '1.0000';
  {$ENDIF}
end;

function TfmTakanonChange.SaveHamara(FileItem: TFileItem): Boolean;
var
  i, j: Integer;
  HamaraInfo: THamaraInfo;
  StocksAndHolder: TStocksAndHolder;
  Draft: boolean;
begin
  Result := False;
  if (FileItem = nil) then
    Exit;
  if ((cbSourceName.Text = cbDestName.Text) and (cbSourceValue.Text = cbDestValue.Text)) then
    Exit;

  if (rbAll.Checked and (StrToInt(edCount.Text) = 100)) then
  begin
    Result := SaveHamaraSimple(FileItem);
    Exit;
  end;

  try
    Draft := FileItem.FileInfo.Draft;
    HamaraInfo := HamaraData(Draft);
    if (HamaraInfo.FDraftHon = nil) then
    begin
      Result := True;
      Exit;
    end;
    try
      try
        FileItem.FileInfo.Draft := True;
        HamaraInfo.FDraftHon.ExSaveData(FileItem, Company);
        HamaraInfo.FDraftMuhaz.ExSaveData(FileItem, Company);
        HamaraInfo.FDraftStockHolders.ExSaveData(FileItem, Company);
        if (not Draft) then
        begin
          HamaraInfo.FNonDraftHonNew.SaveData(Company, FileItem.FileInfo.RecNum);
          HamaraInfo.FNonDraftHonOld.SaveData(Company, FileItem.FileInfo.RecNum);
          HamaraInfo.FNonDraftMuhazNew.SaveData(Company, FileItem.FileInfo.RecNum);
          HamaraInfo.FNonDraftMuhazOld.SaveData(Company, FileItem.FileInfo.RecNum);
          for i := 0 to HamaraInfo.FNonDraftStocksAndHolders.Count-1 do
          begin
            StocksAndHolder := TStocksAndHolder(HamaraInfo.FNonDraftStocksAndHolders[i]);
            StocksAndHolder.Stocks.SaveData(StocksAndHolder.StockHolder, FileItem.FileInfo.RecNum);
          end;
        end;
      finally
        Result := True;
      end;
    finally
      HamaraInfo.FDraftHon.Free;
      HamaraInfo.FDraftStockHolders.Free;
      HamaraInfo.FDraftMuhaz.Free;
      if (not Draft) then
      begin
        HamaraInfo.FNonDraftHonNew.Free;
        HamaraInfo.FNonDraftHonOld.Deconstruct;
        HamaraInfo.FNonDraftMuhazOld.Deconstruct;
        HamaraInfo.FNonDraftMuhazNew.Free;
        for i := 0 to HamaraInfo.FNonDraftStocksAndHolders.Count-1 do
        begin
          StocksAndHolder := TStocksAndHolder(HamaraInfo.FNonDraftStocksAndHolders[i]);
          for j := 0 to StocksAndHolder.Stocks.Count-1 do
          begin
            if (StocksAndHolder.Stocks.Items[j].Active) then
              StocksAndHolder.Stocks.Items[j].Free;
          end;
          StocksAndHolder.Stocks.Deconstruct;
        end;
        HamaraInfo.FNonDraftStocksAndHolders.Free;
      end;
    end;
  except on E: Exception do
    WriteLog('Error TfmTakanonChange.SaveHamara: ' + E.Message, GetLogFileName);
  end;
end; // TfmTakanonChange.SaveHamara

function TfmTakanonChange.SaveHamaraSimple(FileItem: TFileItem): Boolean;
var
  SourceName, DestName: string;
  SourceValue, DestValue: Double;
  HonItem, NewHon: THonItem;
  i: integer;
  StockHolder, NewHolder: TStockHolder;
  Stock, NewStock: TStock;
  NewStocks: TStocks;
begin
  Result := False;

  try
    // Delete old hon
    Company.HonList.Filter := afActive;
    SourceName := cbSourceName.Text;
    SourceValue := Str2Float(cbSourceValue.Text);
    HonItem := Company.HonList.FindHonItem(SourceName, SourceValue);
    if (HonItem <> nil) then
    begin
      HonItem.LastChanged := Date;
      HonItem.Active := False;
      HonItem.SaveData(Company, False, False, FileItem.FileInfo.RecNum);
      FileItem.SaveChange(ocHon, HonItem.RecNum, acDelete, HonItem.RecNum);

      Company.StockHolders.Filter := afActive;
      for i := 0 to Company.StockHolders.Count-1 do
      begin
        StockHolder := Company.StockHolders.Items[i] as TStockHolder;
        StockHolder.Stocks.Filter := afActive;
        Stock := StockHolder.Stocks.FindStock(SourceName, SourceValue);
        if (Stock <> nil) then
        begin
          Stock.LastChanged := Date;
          Stock.Active := False;
          Stock.SaveData(StockHolder, False, FileItem.FileInfo.Draft, FileItem.FileInfo.RecNum);
        end;
      end;
    end;

    // Update new hon
    DestName := cbDestName.Text;
    DestValue := Str2Float(cbDestValue.Text);
    NewHon := Company.HonList.FindHonItem(DestName, DestValue);
    if (NewHon = nil) then
    begin
      NewHon := THonItem.CreateNew(DecisionDate, DestName, DestValue, HonItem.Rashum, HonItem.Mokza, HonItem.Cash,
        HonItem.NonCash, HonItem.Part, HonItem.PartValue, True);
      try
        NewHon.ExSaveData(FileItem, Company);

        Company.StockHolders.Filter := afActive;
        for i := 0 to Company.StockHolders.Count-1 do
        begin
          StockHolder := Company.StockHolders.Items[i] as TStockHolder;
          StockHolder.Stocks.Filter := afActive;
          Stock := StockHolder.Stocks.FindStock(SourceName, SourceValue);
          if (Stock <> nil) then
          begin
            NewStock := TStock.CreateNew(DecisionDate, DestName, Stock.Value, Stock.Count, Stock.Paid, True);
            NewStocks := TStocks.CreateNew;
            NewStocks.Add(NewStock);
            NewHolder := TStockHolder.CreateNew(DecisionDate, StockHolder.Name, StockHolder.Zeut, StockHolder.Taagid,
              StockHolder.Address, NewStocks, True);
            NewHolder.ExSaveData(FileItem, Company);
          end;
        end;
      finally
        NewHon.Free;
      end;
    end;

    Result := True;

  except on E: Exception do
    WriteLog('Error TfmTakanonChange.SaveHamaraSimple: ' + E.Message, GetLogFileName);
  end;
end; // TfmTakanonChange.SaveHamaraSimple

procedure TfmTakanonChange.SaveNewHon(FileItem: TFileItem);
var
  i: integer;
  HonList: THonList;
begin
  try
    HonList := THonList.CreateNew;
    try
      FileItem.FileInfo.Takanon := True;
      for i := 0 to Grid.RowCount-1 do
      begin
        if (Grid.Cells[0, i] = '') then
          Continue;
        if (Company.HonList.FindHonItem(Grid.Cells[0, i], StrToFloat(Grid.Cells[2, i])) = nil) then
        begin
          HonList.Add(
            THonItem.CreateNew(
              StrToDate(e13.Text),
              Grid.Cells[0, i],
              StrToFloat(Grid.Cells[2, i]),
              StrToFloat(Grid.Cells[1, i]),
              0, 0, 0, 0, 0,
              True)
          );
        end;
      end;
      if (HonList.Count > 0) then
        HonList.ExSaveData(FileItem, Company);
    finally
      FileItem.FileInfo.Takanon := False;
      if (Assigned(HonList)) then
        HonList.Free;
    end;
  except on E: Exception do
    WriteLog('Error TfmTakanonChange.SaveNewHon: ' + E.Message, GetLogFileName);
  end;
end; // TfmTakanonChange.SaveNewHon

procedure TfmTakanonChange.btPrintClick(Sender: TObject);
begin
  if (B1.Enabled) then
    DataChanged := False;
  if (DataChanged) then
  begin
    fmDialog.memo.Lines.Clear;
    fmDialog.memo.Lines.Add('��� ��');
    fmDialog.memo.Lines.Add('������� ����� ��� �����.');
    fmDialog.memo.Lines.Add('��� ������ ?');
    if (fmDialog.ShowModal <> idYES) then
      Exit;
  end;
  EvPrintData;
end;

// Moshe 09/12/2018
procedure TfmTakanonChange.LoadHamara(FileItem: TFileItem);
var
  i: integer;
  MuhazList: TMuhazList;
  StockHolders: TStockHolders;
  StockHolder: TStockHolder;
  HonList: THonList;
  HonItem: THonItem;
  LoMukza: Double;
begin
  LoMukza := 0;
  try
    MuhazList := TMuhazList.LoadFromFile(Company, FileItem);
    StockHolders := TStockHolders.LoadFromFile(Company, FileItem);
    HonList := THonList.LoadFromFile(Company, FileItem);

  // Moshe 16/12/2018
  // 2 first Hon items are Hamara
  // Moshe 31/01/2019
  // First Hon is Hamara
  // Deleted Hon is the origion

    if (HonList.Count > 0) then
    begin
    //---
      HonItem := HonList.Items[0] as THonItem;
      cbDestName.Text := HonItem.Name;
      cbDestValue.Text:= FormatNumber(FloatToStr(HonItem.Value), 15, 4, True);
      edSourceCount.Text := '1'; // FormatNumber(FloatToStr(HonItem.Cash), 15, 4, True);
      edDestCount.Text := '1'; // Moshe 22/09/2019: FormatNumber(FloatToStr(HonItem.NonCash), 15, 4, True);
      if (HonItem.Part = -1) then
      begin
        rbChoose.Checked := True;
        edCount.Text := '100';
      end
      else
      begin
        rbAll.Checked := True;
        edCount.Text := FloatToStr(HonItem.Part);
      end;
      rbChooseClick(rbChoose);
    //---
    end;
    LoadHonDeleted(FileItem, LoMukza);

    if (rbChoose.Checked) then
    begin
      EvFillGrid3(0);
      AddToGrid3(cLoMukza, '', LoMukza);
      for i :=0 to StockHolders.Count-1 do
      begin
        StockHolder := StockHolders.Items[i] as TStockHolder;
        AddToGrid3(StockHolder.Name, StockHolder.Zeut, (StockHolder.Stocks.Items[0] as TStock).Count);
      end;

      for i := 0 to MuhazList.Count-1 do
      begin
        AddToGrid3(cMuhaz,(MuhazList.Items[i] as TMuhazItem).Zeut, 1);
      end;
    end
    else
      EvFillGrid3(MyStrToFloat(edCount.Text));

  except on E: Exception do
    WriteLog('Error TfmTakanonChange.LoadHamara: ' + E.Message, GetLogFileName);
  end;
end; // TfmTakanonChange.LoadHamara

procedure TfmTakanonChange.FillHonSource(HonItem: THonItem; LoMukza: Double);
begin
  try
    cbSourceName.ItemIndex := cbSourceName.Items.IndexOf(HonItem.Name);
    if (cbSourceName.ItemIndex = -1) then
      cbSourceName.ItemIndex := cbSourceName.Items.Add(HonItem.Name);
    cbSourceValue.ItemIndex := cbSourceValue.Items.IndexOf(FormatNumber(FloatToStr(HonItem.Value), 15, 4, True));
    if (cbSourceValue.ItemIndex = -1) then
      cbSourceValue.ItemIndex := cbSourceValue.Items.Add(FormatNumber(FloatToStr(HonItem.Value), 15, 4, True));
    //VG19075
    //LoMukza := HonItem.Mokza;

    // Moshe 28/01/2019 - Who understand ?!
    edSourceCount.Text := '1';
    if (HonItem.Cash <> 0) then
      edSourceCount.Text := FormatNumber(FloatToStr(HonItem.Cash), 15, 4, True);
    edDestCount.Text := '1';
    if (HonItem.NonCash <> 0)  then
      edDestCount.Text := FormatNumber(FloatToStr(HonItem.NonCash), 15, 4, True);

    if (HonItem.Part = -1) then
    begin
      rbChoose.Checked := True;
      edCount.Text := '100';
    end
    else
    begin
      rbAll.Checked := True;
      edCount.Text := FloatToStr(HonItem.Part);
    end;
    rbChooseClick(rbChoose);
  except on E: Exception do
    WriteLog('Error TfmTakanonChange.FillHonSource: ' + E.Message, GetLogFileName);
  end;
end; // TfmTakanonChange.FillHonSource

procedure TfmTakanonChange.LoadHonDeleted(FileItem: TFileItem; LoMukza: Double);
var
  NewNum: integer;
  HonItem: THonItem;
begin
  NewNum := 0;
  try
    Data.qGeneric.Close;
    Data.qGeneric.SQL.Text :=
      'SELECT NewNum, RecNum FROM Forms ' +
      'WHERE FileNum=' + IntToStr(FileItem.FileInfo.RecNum) + ' AND ' +
        'ObjectType=''' + ObjToChar(ocHon) + ''' AND ' +
        'Action=''' + ActionToChar(acDelete) + '''' +
      'ORDER BY RecNum DESC ';
    Data.qGeneric.Open;
    if (not Data.qGeneric.EOF) then
      NewNum := Data.qGeneric.FieldByName('NewNum').AsInteger;
    Data.qGeneric.Close;
    if (NewNum > 0) then
    begin
      HonItem := THonItem.Create(NewNum, Company, FileItem.FileInfo.DecisionDate);
      FillHonSource(HonItem, LoMukza);
    end;
  except on E: Exception do
    WriteLog('Error TfmTakanonChange.LoadHonDeleted: ' + E.Message, GetLogFileName);
  end;
end; // TfmTakanonChange.LoadHonDeleted

procedure TfmTakanonChange.AddToGrid3(const Name, Zeut: String; Value: Double);
var
  Row: integer;
  i: integer;
begin
  Row := -1;
  for i := 0 to Grid3.RowCount-1 do
  begin
    if ((Grid3.Cells[0,i] = Name) and (Grid3.Cells[1,i] = Zeut)) then
    begin
      Row := i;
      Break;
    end;
  end;
  if (Row = -1) then
  begin
    Row := Grid3.RowCount;
    Grid3.RowCount := Row + 1;
    Grid3.Cells[0, Row] := Name;
    Grid3.Cells[1, Row] := Zeut;
    Grid3.Cells[2, Row] := '0.00';
  end;
  Grid3.Cells[3, Row] := FormatNumber(FloatToStr(Value), 15, 2, True);
end; // TfmTakanonChange.AddToGrid3

procedure TfmTakanonChange.LoadNewHon(FileItem: TFileItem);
begin
  Grid.RowCount := 0;
  FileItem.EnumTakanonChanges(ocCompany, Company.RecNum, acNewHon, LoadEnum);
  //Grid.LineDeleteByIndex(0);
  Grid.Repaint;
end;

procedure TfmTakanonChange.LoadEnum(ObjectNum, NewNum: Integer);
var
  HonItem: THonItem;
begin
  HonItem := THonItem.Create(NewNum, Company, DecisionDate);
  try
    if (Grid.RowCount = 1) then
      if (Grid.Cells[0,1] <> '') then
        Grid.RowCount := Grid.RowCount + 1;
    Grid.Cells[0, Grid.RowCount-1] := HonItem.Name;
    Grid.Cells[1, Grid.RowCount-1] := FormatNumber(FloatTostr(HonItem.Rashum), 15, 2, True);
    Grid.Cells[2, Grid.RowCount-1] := FormatNumber(FloatToStr(HonItem.Value),15, 4, True);
    Grid.Cells[3, Grid.RowCount-1] := FloatToStrF(HonItem.Rashum*HonItem.Value, ffNumber, 15, 4);
  finally
    HonItem.Free;
  end;
end;

procedure TfmTakanonChange.edtMagishNameDblClick(Sender: TObject);
begin
  btSearchClick(Sender);
end;

//VG19075
procedure TfmTakanonChange.btTakanon7Click(Sender: TObject);
begin
  inherited;
  DataChanged := ExecuteTakanon(FTakanon7, DefaultTakanon7, (Sender as TButton).Caption);
end;
//VG19075
function TfmTakanonChange.ExecuteTakanon(var Takanon: String; const DefaultTakanon, Caption: String): Boolean;
begin
  if (Takanon = '') then
    Takanon := DefaultTakanon;
  Application.CreateForm(TfmTakanonEdit, fmTakanonEdit);
  fmTakanonEdit.Initialize;
  fmTakanonEdit.MaxLines := C_DEFAULT_MAX_LINES; //defined in TakanonEdit
  Result := fmTakanonEdit.Execute(Takanon, '����� ����� - ����� ' + Caption);
  fmTakanonEdit.Free;
  fmTakanonEdit := nil;
end;

end.

