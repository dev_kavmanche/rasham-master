unit Lawer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HEdit;

type
  TfmLawer = class(TfmBlankTemplate)
    Shape127: TShape;
    Shape121: TShape;
    HebLabel70: THebLabel;
    Shape122: TShape;
    Shape123: TShape;
    HebLabel71: THebLabel;
    Shape124: TShape;
    Shape125: TShape;
    HebLabel72: THebLabel;
    Shape126: TShape;
    HebLabel73: THebLabel;
    Shape128: TShape;
    Shape129: TShape;
    HebLabel74: THebLabel;
    e77: TEdit;
    e76: THebEdit;
    e78: THebEdit;
    e79: THebEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure EvDataLoaded; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    procedure EvFocusNext(Sender: TObject); override;
  public
    { Public declarations }
  end;

var
  fmLawer: TfmLawer;

implementation

{$R *.DFM}

uses Dialog2, DataObjects, DataM;

procedure TfmLawer.EvFocusNext(Sender: TObject);
begin
  Inherited;
  If Sender = e13 then e76.SetFocus;
  If Sender = e76 then e77.SetFocus;
  If Sender = e77 then e78.SetFocus;
  If Sender = e78 then e79.SetFocus;
  If Sender = e79 then e76.SetFocus;
end;

procedure TfmLawer.EvDataLoaded;
begin
  Inherited;
  If Not DoNotClear Then
    begin
      Loading:= True;
      e76.Text:= Company.CompanyInfo.LawName;
      e77.Text:= Company.CompanyInfo.LawZeut;
      e78.Text:= Company.CompanyInfo.LawAddress;
      e79.Text:= Company.CompanyInfo.LawLicence;
      Loading:= False;
    end;
end;

function TfmLawer.EvSaveData(Draft: Boolean): Boolean;
var
  CompanyInfo: TCompanyInfo;
begin
  try
    CompanyInfo := TCompanyInfo.CreateNew(DecisionDate);

    CompanyInfo.Option1                   := Company.CompanyInfo.Option1;
    CompanyInfo.Option3                   := Company.CompanyInfo.Option3;
    CompanyInfo.OtherInfo                 := Company.CompanyInfo.OtherInfo;
    CompanyInfo.SigName                   := Company.CompanyInfo.SigName;
    CompanyInfo.SigZeut                   := Company.CompanyInfo.SigZeut;
    CompanyInfo.SigTafkid                 := Company.CompanyInfo.SigTafkid;
    CompanyInfo.LawName                   := e76.Text;
    CompanyInfo.LawAddress                := e78.Text;
    CompanyInfo.LawZeut                   := e77.Text;
    CompanyInfo.LawLicence                := e79.Text;
    CompanyInfo.Takanon2_1                := Company.CompanyInfo.Takanon2_1;
    CompanyInfo.Takanon2_2                := Company.CompanyInfo.Takanon2_2;
    CompanyInfo.Takanon2_3                := Company.CompanyInfo.Takanon2_3;
    CompanyInfo.Takanon2_4                := Company.CompanyInfo.Takanon2_4;
    CompanyInfo.Takanon4                  := Company.CompanyInfo.Takanon4;
    CompanyInfo.Takanon5                  := Company.CompanyInfo.Takanon5;
    CompanyInfo.Takanon7                  := Company.CompanyInfo.Takanon7;
    CompanyInfo.EnglishName               := Company.CompanyInfo.EnglishName;
    CompanyInfo.RegistrationDate          := Company.CompanyInfo.RegistrationDate;
    CompanyInfo.CompanyName2              := Company.CompanyInfo.CompanyName2;
    CompanyInfo.CompanyName3              := Company.CompanyInfo.CompanyName3;
    CompanyInfo.Mail                      := Company.CompanyInfo.Mail;
    CompanyInfo.SharesRestrictIndex       := Company.CompanyInfo.SharesRestrictIndex;
    CompanyInfo.DontOfferIndex            := Company.CompanyInfo.DontOfferIndex;
    CompanyInfo.ShareHoldersNoIndex       := Company.CompanyInfo.ShareHoldersNoIndex;
    CompanyInfo.SharesRestrictChapters    := Company.CompanyInfo.SharesRestrictChapters;
    CompanyInfo.DontOfferChapters         := Company.CompanyInfo.DontOfferChapters;
    CompanyInfo.ShareHoldersNoChapters    := Company.CompanyInfo.ShareHoldersNoChapters;
    CompanyInfo.LimitedSignatoryIndex     := Company.CompanyInfo.LimitedSignatoryIndex;
    CompanyInfo.LegalSectionsChapters     := Company.CompanyInfo.LegalSectionsChapters;
    CompanyInfo.SignatorySectionsChapters := Company.CompanyInfo.SignatorySectionsChapters;
    CompanyInfo.ConfirmedCopies           := Company.CompanyInfo.ConfirmedCopies;
    CompanyInfo.MoreCopies                := Company.CompanyInfo.MoreCopies;
    CompanyInfo.RegulationItemsIndex      := Company.CompanyInfo.RegulationItemsIndex;

    CompanyInfo.SaveData(Company, False, -1);
    CompanyInfo.Free;
    fmDialog2.Show;
    Result:= True;
  finally
  end;
end;

procedure TfmLawer.FormCreate(Sender: TObject);
begin
  FileAction:= faNone;
  ShowStyle:= afActive;
  inherited;
end;

end.
