unit LoadSaveTemplate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  HebForm, StdCtrls, HEdit, HLabel, ExtCtrls, HGrids, SdGrid;

type
  TfmLoadSaveTemplate = class(TForm)
    Bevel1: TBevel;
    sdNames: TSdGrid;
    PanelCaption: TPanel;
    Shape37: TShape;
    Shape36: TShape;
    HebLabel19: THebLabel;
    Shape1: TShape;
    Shape2: TShape;
    HebLabel1: THebLabel;
    Shape8: TShape;
    HebLabel4: THebLabel;
    edName: THebEdit;
    edNumber: TEdit;
    btOk: TButton;
    btCancel: TButton;
    HebForm1: THebForm;
    edFileName: THebEdit;
    btDelete: TButton;
    procedure btCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sdNamesDblClick(Sender: TObject);
    procedure sdNamesKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLoadSaveTemplate: TfmLoadSaveTemplate;

implementation

{$R *.DFM}

procedure TfmLoadSaveTemplate.btCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfmLoadSaveTemplate.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:= caHide;
end;

procedure TfmLoadSaveTemplate.sdNamesDblClick(Sender: TObject);
begin
  If sdNames.Row> -1 then
    btOk.Click;
end;

procedure TfmLoadSaveTemplate.sdNamesKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key=#13 then
    begin
      sdNamesDblClick(Sender);
      Key:= #0;
    end;
end;

procedure TfmLoadSaveTemplate.FormShow(Sender: TObject);
var
  CanSelect: Boolean;
begin
  sdNames.SetFocus;
  If (sdNames.Row >=0) and Assigned(sdNames.OnSelectCell) Then
    sdNames.OnSelectCell(sdNames, 0, sdNames.Row, CanSelect);
end;

end.
