unit LoadDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  LoadSaveTemplate, HebForm, StdCtrls, HGrids, SdGrid, HEdit, ExtCtrls,
  HLabel, DataObjects, BlankTemplate;

type
  TfmLoadDialog = class(TfmLoadSaveTemplate)
    procedure btOkClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
  private
    { Private declarations }
  protected
    FSender: TfmBlankTemplate;
    FFilesList: TFilesList;
    FileItem: TFileItem;
    procedure FillData;
  public
    { Public declarations }
    function Execute(Sender: TfmBlankTemplate; var FilesList: TFilesList; CompanyNum: Integer; Action: TFileAction): TFileItem;
    function ExecuteDraft(Sender: TfmBlankTemplate; var FilesList: TFilesList; CompanyNum: Integer; Action: TFileAction): TFileItem;
  end;

var
  fmLoadDialog: TfmLoadDialog;

implementation

{$R *.DFM}

uses Util, dialog;

function TfmLoadDialog.Execute(Sender: TfmBlankTemplate; var FilesList: TFilesList; CompanyNum: Integer; Action: TFileAction): TFileItem;
begin
  FSender:= Sender;
  Result:= Nil;
  with fmDialog Do
  begin
    memo.lines.Clear;
    memo.lines.add('��� ����� ������ ����');
    Button1.Caption:= '������';
    Button2.Caption:= '�����';
    ShowModal;
    Button1.Caption:= '��';
    Button2.Caption:= '��';
    if DialogResult = drYES then
    begin
      btDelete.Visible:= False;
      Self.Caption:= '���� ����� - ����';
      FilesList:= TFilesList.Create(CompanyNum, Action, False);
    end
    Else If DialogResult = drNo then
    begin
      btDelete.Visible:= True;
      Self.Caption:= '���� ����� - �����';
      FilesList:= TFilesList.Create(CompanyNum, Action, True);
    end
    Else
      Exit;
  end;
  FFilesList:= FilesList;
  FillData;
  FileItem:= Nil;
  ShowModal;
  FilesList:= FFilesList;
  Result:= FileItem;
end;

function TfmLoadDialog.ExecuteDraft(Sender: TfmBlankTemplate; var FilesList: TFilesList; CompanyNum: Integer; Action: TFileAction): TFileItem;
begin
  FSender := Sender;
  btDelete.Visible := True;
  Self.Caption := '���� ����� - �����';
  FilesList := TFilesList.Create(CompanyNum, Action, True);
  FFilesList := FilesList;
  FillData;
  FileItem := nil;
  ShowModal;
  FilesList := FFilesList;
  Result := FileItem;
end; // TfmLoadDialog.ExecuteDraft

procedure TfmLoadDialog.FillData;
var
  I: Integer;
begin
  sdNames.RowCount:= FFilesList.Count;
  sdNames.Rows[0].Clear;
  For I := 0 To FFilesList.Count - 1 Do
  begin
    sdNames.Cells[0,I]:= FFilesList.FileInfo[I].FileName;
    sdNames.Cells[1,I]:= DateToLongStr(FFilesList.FileInfo[I].DecisionDate);
  end;
  SortGrid(sdNames,'0,STRING,UP');
  sdNames.Repaint;
end;

procedure TfmLoadDialog.btOkClick(Sender: TObject);
var
  Line, I: Integer;
begin
  inherited;
  Line:= -1;
  For I := 0 To FFilesList.Count - 1 do
    if sdNames.Cells[0,sdNames.Row] = FFilesList.FileInfo[I].FileName Then
      Line:= I;

  If (Line = -1) Or (Line>= FFilesList.Count) Then
  begin
    fmDialog.ActiveControl:= fmDialog.Button3;
    fmDialog.Caption:= '�����';
    fmDialog.memo.lines.Clear;
    fmDialog.memo.lines.add('��� ��');
    fmDialog.memo.lines.add('���� �� �� ���� ����� �������');
    fmDialog.Button1.Visible:= false;
    fmDialog.Button2.Visible:= False;
    fmDialog.Button3.Caption:= '�����';
    fmDialog.ShowModal;
    fmDialog.Button3.Caption:= '�����';
    fmDialog.Button1.Visible:= True;
    fmDialog.Button2.Visible:= True;
    fmDialog.Caption:= '����';
    FileItem:= Nil;
  end
  Else
  begin
    try
      FileItem := FFilesList.Items[Line];
    except
      FileItem := Nil;
    end;
    If Sender = btOk then
    begin
      If FileItem <> nil then
      begin
        FSender.FileName:= FileItem.FileInfo.FileName;
        FSender.FileIsDraft:= FileItem.FileInfo.Draft;
      end;
      Close;
    end;
  end;
end;

procedure TfmLoadDialog.btDeleteClick(Sender: TObject);
var
  TempFilesList: TFilesList;
begin
  inherited;
  btOkClick(btDelete); // Do not use btOk.Click it would call TfmSaveDialog.btOkClick
  If FileItem= nil then
    Exit;

  If Sender<>btOK Then
  begin
    fmDialog.ActiveControl:= fmDialog.Button1;
    fmDialog.memo.lines.Clear;
    fmDialog.memo.lines.add('��� ����� ����');
    fmDialog.memo.Lines.Add(FileItem.FileInfo.FileName);
    fmDialog.Button3.Visible:= false;
    fmDialog.ShowModal;
    fmDialog.Button3.Visible:= True;
    if fmDialog.DialogResult = drNo Then
    begin
      FileItem.Free;
      FileItem:= nil;
      Exit;
    end;
  end;

  FSender.DeleteFile(FileItem);
  edFileName.Text:= FileItem.FileInfo.FileName;
  TempFilesList:= TFilesList.Create(FileItem.FileInfo.CompanyNum, FileItem.FileInfo.Action, FileItem.FileInfo.Draft);
  FileItem.Free;
  FFilesList.Free;
  FileItem:= Nil;
  FFilesList:= TempFilesList;
  FillData;
  if Sender = btOK Then          // See SaveDialog.pas at btOkClick
    btOk.Click;
end;

end.
