unit select;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TfmSelect = class(TForm)
    Entries: TListBox;
    Bevel1: TBevel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelect: TfmSelect;

implementation

{$R *.DFM}

procedure TfmSelect.Button1Click(Sender: TObject);
begin
Close;
end;

procedure TfmSelect.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if Entries.ItemIndex=-1 then Entries.ItemIndex:=0;
end;

procedure TfmSelect.FormShow(Sender: TObject);
begin
 Entries.ItemIndex := 0;
 Entries.SetFocus;
end;

end.
