unit PrintTakanon;

{
History:
date        dest. ver  by   task     description
21/12/2020  1.8.4      sts  22057    refactoring;
}


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, Db, DBTables, DataObjects, memdset, qrrich98;

type
  TfmQRTakanon = class(TForm)
    qrTakanon2: TQuickRep;
    qrTakanon3: TQuickRep;
    Band5: TQRBand;
    QRLabel46: TQRLabel;
    QRBand5: TQRBand;
    QRLabel18: TQRLabel;
    QRDBText5: TQRDBText;
    QRBand4: TQRSubDetail;
    QRLabel19: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    taHon: TTable;
    taStockNM: TTable;
    taStockHolders: TTable;
    QRShape1: TQRShape;
    qrTakanon4: TQuickRep;
    qrTakanon1: TQuickRep;
    QRBand12: TQRBand;
    QRLabel59: TQRLabel;
    QRBand10: TQRBand;
    QRMemo1: TQRMemo;
    QRMemo2: TQRMemo;
    QRCompositeReport1: TQRCompositeReport;
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    l1: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    GroupFooterBand1: TQRBand;
    GroupHeaderBand1: TQRBand;
    QRLabel3: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRSubDetail2: TQRSubDetail;
    Band4: TQRBand;
    taMem2: TMemDataSet;
    taMem1: TMemDataSet;
    QRSubDetail3: TQRSubDetail;
    QRDBText1: TQRDBText;
    QRSubDetail4: TQRSubDetail;
    GroupFooterBand3: TQRBand;
    GroupHeaderBand2: TQRBand;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    l3: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel81: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel26: TQRLabel;
    QRDBText11: TQRDBText;
    l24: TQRLabel;
    QRLabel79: TQRLabel;
    l5: TQRLabel;
    QRLabel88: TQRLabel;
    l6: TQRLabel;
    QRLabel89: TQRLabel;
    l7: TQRLabel;
    QRLabel90: TQRLabel;
    l8: TQRLabel;
    QRLabel91: TQRLabel;
    l4: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel37: TQRLabel;
    Band2: TQRBand;
    Band7: TQRBand;
    QRLabel11: TQRLabel;
    reQRTakanon2: TQRRichText98;
    reQRTakanon5: TQRRichText98;
    reQRTakanon7: TQRRichText98;
    QRSubDetail5: TQRSubDetail;
    GroupFooter4: TQRBand;
    QRl7: TQRLabel;
    l2: TQRLabel;
    reQRTakanon4: TQRRichText98;
    QRDBText14: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel67: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel111: TQRLabel;
    QRDBText15: TQRDBText;
    QRLabel110: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel112: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel12: TQRLabel;
    lbTotalHon: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    procedure taStockNMFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure QRSubDetail3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure PrintProc(DoPrint: Boolean; Company: TCompany; DecisionDate: TDateTime);
  end;

var
  fmQRTakanon: TfmQRTakanon;

implementation

uses
  Util, PrinterSetup, Main;

{$R *.DFM}

procedure TfmQRTakanon.PrintProc(DoPrint: Boolean; Company: TCompany; DecisionDate: TDateTime);
var
  I,J: Integer;
  Strs: TStringList;
  FileName: String;
  TotalValue: double;
  TotalHon: double;
begin
  l1.Caption := Company.Name;
  l3.Caption := Company.CompanyInfo.LawName;
  l5.Caption := Company.CompanyInfo.LawName;
  l6.Caption := Company.CompanyInfo.LawAddress;
  if Company.CompanyInfo.LawZeut <> '0' then
    l7.Caption := Company.CompanyInfo.LawZeut;
  if Company.CompanyInfo.LawLicence <> '0' then
    l8.Caption := Company.CompanyInfo.LawLicence;

  taMem1.Close;
  taMem1.EmptyTable;
  taMem1.Open;
  taMem2.Close;
  taMem2.EmptyTable;
  taMem2.Open;

  If (Company.CompanyInfo.Option1 and 1) > 0 Then
    taMem1.AppendRecord(['.��� ���� 32 (1) ���� - ����� ��� ����� ����']);

  If (Company.CompanyInfo.Option1 and 2) > 0 Then
    taMem1.AppendRecord(['.��� ���� 32 (2) ���� - ����� ��� ����� ����, ���� �������� �������� ����']);

  If (Company.CompanyInfo.Option1 and 4) > 0 Then
    taMem1.AppendRecord(['.��� ���� 32 (3) ���� - �����  �������� �������� ����']);

  If (Company.CompanyInfo.Option1 and 8) > 0 Then
    taMem1.AppendRecord(['.����� ����� ������ ��������, �������� ����, ����, ��� ���� ����� ��� ����� �� ���� ������']);

  If Company.CompanyInfo.Option3 = '1' Then
    taMem2.AppendRecord(['.������ ������'])
  else
    taMem2.AppendRecord(['(���� ������ ������. ����� ��� ���� ������� ������ (��"�, ��"�, ������ ������']);

  l2.Caption:= Company.CompanyInfo.OtherInfo;
  l2.Enabled:= (Company.CompanyInfo.Option3 = '3') or (Company.CompanyInfo.OtherInfo<>'');
  QRl7.Enabled:= l2.Enabled;
  if l2.Enabled then
    reQRTakanon4.Top:= 36;

  TotalHon :=0;
  taHon.FieldDefs.Clear;
  taHon.FieldDefs.Add('Name', ftString, 40, False);
  taHon.FieldDefs.Add('Value',ftString, 20, False);
  taHon.FieldDefs.Add('Rashum', ftString, 20, False);
  taHon.FieldDefs.Add('Mokza',ftString,20,False);
  taHon.FieldDefs.Add('TotalValue', ftString, 20, False);
  taHon.CreateTable;
  taHon.Open;
  For I := 0 to Company.HonList.Count - 1 do
    With Company.HonList.Items[I] as THonItem do
    begin
      TotalValue := Value * Rashum;
      TotalHon := TotalHon + TotalValue;
      taHon.Append;
      taHon.FieldByName('Name').AsString:= MakeHebStr(Name, False);
      taHon.FieldByName('Value').AsString:= FloatToStrF(Value, ffNumber, 15, 4);
      taHon.FieldByName('Rashum').AsString:= FloatToStrF(Rashum, ffNumber, 15, 2);
      taHon.FieldByName('Mokza').AsString:= FloatToStrF(Mokza, ffNumber, 15, 2);
      taHon.FieldByName('TotalValue').AsString:= FloatToStrF(TotalValue, ffNumber, 15, 4);
      taHon.Post;
    end;

  lbTotalHon.Caption := FloatToStrF(TotalHon, ffNumber, 15,4);

  taStockHolders.FieldDefs.Clear;
  taStockHolders.FieldDefs.Add('Name', ftString, 40, False);
  taStockHolders.FieldDefs.Add('Zeut', ftString, 9, True);
  taStockHolders.FieldDefs.Add('Address', ftString, 160, False);
  taStockHolders.CreateTable;
  taStockHolders.Open;

  taStockNM.FieldDefs.Clear;
  taStockNM.FieldDefs.Add('Zeut', ftString, 9, True);
  taStockNM.FieldDefs.Add('Name', ftString, 40, False);
  taStockNM.FieldDefs.Add('Value', ftString, 20, False);
  taStockNM.FieldDefs.Add('Count', ftString, 20, False);
  taStockNM.CreateTable;
  taStockNM.Open;

  For I:= 0 to Company.StockHolders.Count - 1 do
    With Company.StockHolders.Items[I] as TStockHolder do
    begin
      taStockHolders.Append;
      taStockHolders.FieldByName('Name').AsString := MakeHebStr(Name, False);
      taStockHolders.FieldByName('Zeut').AsString := Zeut;
      taStockHolders.FieldByName('Address').AsString:= MakeHebStr(Address.Street + ' ' + Address.Number + ' '
                                                    + Address.City + ' ' + Address.Index + ' ' + Address.Country, True);
      taStockHolders.Post;
      For J:= 0 to Stocks.Count - 1 do
      begin
        taStockNM.Append;
        taStockNM.FieldByName('Zeut').AsString := Zeut;
        With Stocks.Items[J] as TStock do
        begin
          taStockNM.FieldByName('Name').AsString := MakeHebStr(Name, False);
          taStockNM.FieldByName('Value').AsString := FloatToStrF(Value, ffNumber, 15, 4);
          taStockNM.FieldByName('Count').AsString := FloatToStrF(Count, ffNumber, 15, 2);
        end;
        taStockNM.Post;
      end;
    end;

  Strs:= TStringList.Create;
  FileName:= GetTempFileName('.rtf');
  try
    Strs.Text:= Company.CompanyInfo.Takanon2;
    Strs.SaveToFile(FileName);
    fmMain.rePrintTakanon2.Visible:= True;
    fmMain.rePrintTakanon2.Lines.LoadFromFile(FileName);
    reQRTakanon2.ParentRichEdit:= fmMain.rePrintTakanon2;
    band2.Enabled:= Trim(reQRTakanon2.Lines.Text) <> '';
    fmMain.rePrintTakanon2.Visible:= False;

    Strs.Text:= Company.CompanyInfo.Takanon4;
    Strs.SaveToFile(FileName);
    fmMain.rePrintTakanon4.Visible:= True;
    fmMain.rePrintTakanon4.Lines.LoadFromFile(FileName);
    reQRTakanon4.ParentRichEdit:= fmMain.rePrintTakanon4;
    groupFooter4.Enabled:= l2.Enabled or (Trim(reQRTakanon4.Lines.Text) <> '');
    fmMain.rePrintTakanon4.Visible:= False;

    Strs.Text:= Company.CompanyInfo.Takanon5;
    Strs.SaveToFile(FileName);
    fmMain.rePrintTakanon5.Visible:= True;
    fmMain.rePrintTakanon5.Lines.LoadFromFile(FileName);
    reQRTakanon5.ParentRichEdit:= fmMain.rePrintTakanon5;
    fmMain.rePrintTakanon5.Visible:= False;

    Strs.Text:= Company.CompanyInfo.Takanon7;
    Strs.SaveToFile(FileName);
    fmMain.rePrintTakanon7.Visible:= True;
    fmMain.rePrintTakanon7.Lines.LoadFromFile(FileName);
    reQRTakanon7.ParentRichEdit:= fmMain.rePrintTakanon7;
    band7.Enabled:= Trim(reQRTakanon7.Lines.Text) <> '';
    fmMain.rePrintTakanon7.Visible:= False;
  finally
    Strs.Free;
    DeleteFile(FileName);
  end;

  fmPrinterSetup.FormatQR(qrTakanon1);
  fmPrinterSetup.FormatQR(qrTakanon2);
  fmPrinterSetup.FormatQR(qrTakanon3);
  fmPrinterSetup.FormatQR(qrTakanon4);
  fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
  if DoPrint then
  begin
    qrTakanon1.Print;
    QRCompositeReport1.Print;
    qrTakanon4.Print;
  end
  Else
  begin
    qrTakanon1.Preview;
    Application.ProcessMessages;
    QRCompositeReport1.Preview;
    Application.ProcessMessages;
    qrTakanon4.Preview;
    Application.ProcessMessages;
  end;
end;

procedure TfmQRTakanon.taStockNMFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept:= taStockHolders.FieldByName('Zeut').AsString = taStockNM.FieldByName('Zeut').AsString;
end;

procedure TfmQRTakanon.QRCompositeReport1AddReports(Sender: TObject);
begin
  QRCompositeReport1.Reports.Add(qrTakanon2);
  QRCompositeReport1.Reports.Add(qrTakanon3);
end;

procedure TfmQRTakanon.QRSubDetail3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  if taMem1.RecordCount = 0 then
    QRSubDetail3.Height:= 0;
end;

end.
