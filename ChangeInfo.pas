unit ChangeInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Number, HebForm, ExtCtrls, Buttons, StdCtrls, HEdit, ComCtrls, HLabel, DataObjects,
  Mask, HComboBx;

type
  TfmChangeInfo = class(TfmNumber)
    lNumber: THebLabel;
    Shape4: TShape;
  private
    { Private declarations }
  protected
    function EvSaveData(Draft: Boolean): Boolean; override;
  public
    { Public declarations }
    procedure Execute(ACompany: TCompany);
  end;

var
  fmChangeInfo: TfmChangeInfo;

implementation

{$R *.DFM}

uses
  Dialog2;

function TfmChangeInfo.EvSaveData(Draft: Boolean): Boolean;
begin
  Company.SetNumber(e13.Text, e12.Text);
  fmdialog2.Show;
  Result:= True;
end;

procedure TfmChangeInfo.Execute(ACompany: TCompany);
begin
  Loading:= True;
  Company:= ACompany;
  lName.Caption:= ACompany.Name;
  If ACompany.Zeut = '-1' Then
    lNumber.Caption:= '����'
  Else
    lNumber.Caption:= ACompany.Zeut;
  e12.Text:= lName.Caption;
  e13.Text:= lNumber.Caption;
  e13.Enabled:= lNumber.Caption <> '����';
  Loading:= False;
  ShowModal;
end;

end.
