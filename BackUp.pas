unit BackUp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, HLabel,
  Compress, Db, HChkBox,  DBTables, umEdit;

type
  TfmBackUp = class(TfmTemplate)
    dgSave: TSaveDialog;
    dgLoad: TOpenDialog;
    comp: TCompress;
    Progress: TProgressBar;
    lbAction: THebLabel;
    lbPercent: TLabel;
    HebLabel1: THebLabel;
    TabSheet2: TTabSheet;
    HebLabel6: THebLabel;
    HebLabel7: THebLabel;
    HebLabel5: THebLabel;
    Button3: TButton;
    Button4: TButton;
    btRestore: TBitBtn;
    btBackup: TBitBtn;
    btOptions: TSpeedButton;
    TabSheet3: TTabSheet;
    HebLabel13: THebLabel;
    btOk: TButton;
    btCancel: TButton;
    cbAutoBackUp: THebCheckBox;
    umNumberEdit1: TumNumberEdit;
    udUpdateEvery: TUpDown;
    SmallProgress: TProgressBar;
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure compShowProgress(var PercentageDone: Integer);
    procedure FormCreate(Sender: TObject);
    procedure btOptionsClick(Sender: TObject);
    procedure umNumberEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure cbAutoBackUpClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
    CompSize,
    TotalSize : Integer;
    ShowSmallProgress: boolean;
    SmallProgressCounter: integer;
    FinalProgress :integer;
    procedure DoBackup;
  protected
    function EvSaveData(Draft: Boolean): boolean; override;
    function EvLoadData: Boolean; override;
  public
    { Public declarations }
    procedure BackUpExecute(const FileName: String);
  end;

var
  fmBackUp: TfmBackUp;

implementation

uses DataM, Util, Main, UpdateTables, FixProblems;

{$R *.DFM}

procedure TfmBackUp.BackUpExecute(const FileName: String);
begin
  dgSave.FileName:= FileName;
  b2.Click;
end;

function TfmBackUp.EvLoadData: Boolean;
begin
  CompSize:=0;
  Result:= False;
  if dgLoad.Execute then
    PageIndex:= 1;
end;

function TfmBackUp.EvSaveData(Draft: Boolean): Boolean;
begin
  Result:= False;
  if Draft or dgSave.Execute then
    begin
      TabSheet1.Refresh;
      FileName:= dgSave.FileName;
      DoBackup;
      Progress.Position:= 100;
      SmallProgress.Position:= 100;
      lbPercent.Caption:= '100 %';
      If not Draft then
        MessageDlg('����� ������� ������ ������', mtInformation, [mbOk], -1);
//      Result:= True;
      Close;
    end;
end;

procedure TfmBackUp.Button4Click(Sender: TObject);
begin
  inherited;
  PageIndex:= 0;
end;

procedure TfmBackUp.DoBackup;
var
  Files: TStringList;

  procedure AddTable(Table: TTable);
  var
    Error     : Integer;
    SearchRec : TsearchRec;
  begin
    Error:=FindFirst(Table.Database.Directory+Copy(Table.TableName,1,Length(Table.TableName) - Length(ExtractFileExt(Table.TableName)))+'.*',faAnyFile,SearchRec);
    While Error=0 do
     begin
       FinalProgress :=100;
       TotalSize:=TotalSize+SearchRec.Size;
       Files.Add(Table.Database.Directory+SearchRec.Name);
       Error:=FindNext(SearchRec);
     end;
  end;

  procedure AddFiles;
  var
    I: Integer;
  begin
    For I:= 0 To Data.ComponentCount-1 do
      If Data.Components[I] is TTable then
        AddTable(Data.Components[I] as TTable);
  end;

begin
  if FileExists(dgSave.FileName) then
    If Not DeleteFile(dgSave.FileName) Then
      Raise Exception.Create('Cannot delete file: '+dgSave.FileName);

  Screen.Cursor:=crHourglass;
  lbAction.Caption:='����� ����� ���';
  lbAction.Refresh;
  Files:= TStringList.Create;
  try
    AddFiles;
    comp.TargetPath:= Data.taName.Database.Directory;
    Data.TablesActivate(False);
    comp.CompressFiles(dgSave.FileName,Files,coLZH5);
    Data.TablesActivate(True);
  finally
    Screen.Cursor:= crDefault;
    lbAction.Caption:= '';
    Files.Free;
  end;
end;

// Restore
procedure TfmBackUp.Button3Click(Sender: TObject);
var
  Files     : TStringList;
  i         : integer;
  Info      : TcompressedFileInfo;
begin
  inherited;
  PageIndex:= 0;
  DataChanged:= False;
  FinalProgress := 50;
  FileName:= dgLoad.FileName;
  Screen.Cursor:=crHourglass;
  lbAction.Caption:='����� ����� ���';
  lbAction.Refresh;
  Files:=TStringList.Create;
  try
    comp.TargetPath:= Data.taName.Database.Directory;
    comp.ScanCompressedFile(dgLoad.FileName,Files);
    TotalSize:=0;
    for i:=0 to Files.Count-1 do
      begin
       Info:=TCompressedFileinfo(Files.objects[i]);
       TotalSize:=TotalSize+Info.CompressedSize;
      end;
    Data.TablesActivate(False);
    try
      comp.ExpandFiles(dgLoad.FileName, Files);
      progress.Position:=50;
      lbPercent.Caption:='50 %';
      SmallProgress.Position:=0;
      ShowSmallProgress := True;
      SmallProgressCounter := 0;
      SmallProgress.Visible := true;
      // Moshe 08/03/2015 - This prevent restore
      //FixDB(False, Self);   //Tsahi 11/10/05 : Fix Director saving problems if that DB is from the period before the fix
      ShowSmallProgress := false;
      progress.Position:=100;
      SmallProgress.Position:=100;
      lbPercent.Caption:='100 %';
      Data.UpdateTableFields;
      MessageDlg('����� ������� ������ ������', mtInformation, [mbOK], -1);
    except
      MessageDlg('�� ���� ����� �����, ���� �� �� �������� ���� ���� �� ������', mtError, [mbOK], -1);
    end;
    VersionUpdate(Self); //Tsahi 24.7.05: if tables are from older version, they should be updated
    Data.TablesActivate(True);
    Close;
  finally
    Files.Free;
    Screen.Cursor:= crDefault;
    lbAction.Caption:= '';
  end;
end;

procedure TfmBackUp.compShowProgress(var PercentageDone: Integer);
var
  Prog: Integer;
begin
  inherited;
  CompSize:=CompSize+8*1024;
  Prog:=Round(CompSize/TotalSize*FinalProgress);
  if Prog>FinalProgress then
    prog:=FinalProgress;
  progress.Position:=Prog;
  lbPercent.Caption:=IntToStr(Prog)+' %';
  lbPercent.Refresh;
end;

procedure TfmBackUp.FormCreate(Sender: TObject);
begin
  inherited;
  ShowSmallProgress := false;
  CreateDir(ExtractFilePath(ParamStr(0))+'BackUp\');
  dgSave.InitialDir:= ExtractFilePath(ParamStr(0))+'BackUp\';
  dgLoad.InitialDir:= ExtractFilePath(ParamStr(0))+'BackUp\';
  btCancel.Click;
end;

procedure TfmBackUp.btOptionsClick(Sender: TObject);
begin
  inherited;
  PageIndex:= 2;
end;

procedure TfmBackUp.umNumberEdit1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key='-' then
    Key:=#0;
end;

procedure TfmBackUp.cbAutoBackUpClick(Sender: TObject);
begin
  inherited;
  udUpdateEvery.Enabled:= cbAutoBackUp.Checked;
  umNumberEdit1.Enabled:= cbAutoBackUp.Checked;
end;

procedure TfmBackUp.btCancelClick(Sender: TObject);
begin
  inherited;
  cbAutoBackUp.Checked:= fmMain.UserOptions.BackUpEvery>0;
  udUpdateEvery.Position:= fmMain.UserOptions.BackUpEvery;
  PageIndex:= 0;
end;

procedure TfmBackUp.btOkClick(Sender: TObject);
begin
  inherited;
  if cbAutoBackUp.Checked then
    fmMain.UserOptions.BackUpEvery:= udUpdateEvery.Position
  else
    fmMain.UserOptions.BackUpEvery:= 0;
  PageIndex:= 0;
end;

procedure TfmBackUp.FormPaint(Sender: TObject);
begin
  inherited;
  if ShowSmallProgress then
  begin
    If SmallProgress.Position = 100 then
      SmallProgress.Position := 0
    else
      SmallProgress.Position := SmallProgress.Position +1;

    if SmallProgressCounter =100 then
    begin
      SmallProgressCounter := 0;
      if Progress.Position <= 95 then
        Progress.Position := Progress.Position+1;
    end else
      inc(SmallProgressCounter);
    lbPercent.Caption := IntToStr(Progress.Position)+'%';
  end;
end;

end.
