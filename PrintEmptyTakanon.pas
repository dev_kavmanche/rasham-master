unit PrintEmptyTakanon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, Db, memdset;

type
  TEmptyTakanonForm = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRImage4: TQRImage;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRImage3: TQRImage;
    QRLabel6: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel9: TQRLabel;
    QuickRep2: TQuickRep;
    QRBand3: TQRBand;
    QRLabel90: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel95: TQRLabel;
    lblNIS: TQRLabel;
    QRLabel20: TQRLabel;
    QRCompositeReport1: TQRCompositeReport;
    QuickRep3: TQuickRep;
    QRBand8: TQRBand;
    QRLabel28: TQRLabel;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel53: TQRLabel;
    QRShape2: TQRShape;
    QRShape11: TQRShape;
    QRLabel18: TQRLabel;
    QRShape16: TQRShape;
    QRLabel21: TQRLabel;
    QRShape20: TQRShape;
    QRLabel22: TQRLabel;
    QRShape21: TQRShape;
    QRLabel23: TQRLabel;
    QRShape22: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape59: TQRShape;
    QRShape60: TQRShape;
    QRShape61: TQRShape;
    QRLabel8: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel56: TQRLabel;
    QRShape62: TQRShape;
    QRShape63: TQRShape;
    QRLabel58: TQRLabel;
    QRShape64: TQRShape;
    QRLabel25: TQRLabel;
    QRShape65: TQRShape;
    QRLabel36: TQRLabel;
    QRShape66: TQRShape;
    QRShape67: TQRShape;
    QRShape68: TQRShape;
    QRShape69: TQRShape;
    QRLabel37: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel59: TQRLabel;
    QRShape70: TQRShape;
    QRLabel60: TQRLabel;
    QRShape71: TQRShape;
    QRShape72: TQRShape;
    QRShape73: TQRShape;
    QRShape74: TQRShape;
    QRLabel62: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel92: TQRLabel;
    QRShape75: TQRShape;
    QRShape76: TQRShape;
    QRShape77: TQRShape;
    QRShape78: TQRShape;
    QRShape79: TQRShape;
    QRShape80: TQRShape;
    QRShape84: TQRShape;
    QRShape85: TQRShape;
    QRShape86: TQRShape;
    QRShape87: TQRShape;
    QRShape88: TQRShape;
    QRShape89: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRLabel11: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape7: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRShape40: TQRShape;
    QRShape81: TQRShape;
    QRShape82: TQRShape;
    QRShape83: TQRShape;
    QRShape90: TQRShape;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRShape91: TQRShape;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRShape92: TQRShape;
    QRLabel107: TQRLabel;
    QRShape93: TQRShape;
    QRShape94: TQRShape;
    QRLabel108: TQRLabel;
    QRShape95: TQRShape;
    QRShape96: TQRShape;
    QRShape97: TQRShape;
    QRShape98: TQRShape;
    QRShape99: TQRShape;
    QRShape100: TQRShape;
    QRShape101: TQRShape;
    QRShape102: TQRShape;
    QRShape103: TQRShape;
    QRShape104: TQRShape;
    QRShape105: TQRShape;
    QRShape106: TQRShape;
    QRShape107: TQRShape;
    QRShape108: TQRShape;
    QRShape109: TQRShape;
    QRShape110: TQRShape;
    QRLabel61: TQRLabel;
    QRLabel69: TQRLabel;
    QRShape41: TQRShape;
    QRLabel71: TQRLabel;
    QRShape42: TQRShape;
    QRLabel72: TQRLabel;
    QRShape43: TQRShape;
    QRShape44: TQRShape;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRShape54: TQRShape;
    QRLabel88: TQRLabel;
    QRShape56: TQRShape;
    QRLabel89: TQRLabel;
    QRShape57: TQRShape;
    QRShape58: TQRShape;
    QRLabel109: TQRLabel;
    QRShape111: TQRShape;
    QRShape55: TQRShape;
    QRShape112: TQRShape;
    QRShape113: TQRShape;
    QRShape114: TQRShape;
    QRLabel19: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRShape23: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRShape115: TQRShape;
    QRShape116: TQRShape;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRShape117: TQRShape;
    QRShape118: TQRShape;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRShape119: TQRShape;
    QRShape120: TQRShape;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRShape121: TQRShape;
    QRShape122: TQRShape;
    QRMemo2: TQRMemo;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    QRShape123: TQRShape;
    QRShape124: TQRShape;
    QRLabel70: TQRLabel;
    QRLabel77: TQRLabel;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel159: TQRLabel;
    QRLabel161: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRLabel170: TQRLabel;
    QRLabel40: TQRLabel;
    procedure QRCompositeReport1AddReports(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure PrintEmpty(const Filename: string);
    procedure PrintProc(DoPrint: boolean);
  end;

var
  EmptyTakanonForm: TEmptyTakanonForm;

implementation

uses
  PrinterSetup;
   
{$R *.DFM}

procedure TEmptyTakanonForm.QRCompositeReport1AddReports(Sender: TObject);
begin
  QRCompositeReport1.Reports.Add(QuickRep1);
  QRCompositeReport1.Reports.Add(QuickRep2);
  QRCompositeReport1.Reports.Add(QuickRep3);
end;

procedure TEmptyTakanonForm.PrintEmpty(const Filename: string);
begin
  fmPrinterSetup.Execute(Filename, PrintProc);
end;

procedure TEmptyTakanonForm.PrintProc(DoPrint: boolean);
begin
  fmPrinterSetup.FormatQR(QuickRep1);
  fmPrinterSetup.FormatQR(QuickRep2);
  fmPrinterSetup.FormatQR(QuickRep3);
  fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
  if DoPrint then
    QRCompositeReport1.Print
  else
  begin
    QuickRep1.Preview;
    Application.ProcessMessages;

    QuickRep2.Preview;
    Application.ProcessMessages;

    QuickRep3.Preview;
    Application.ProcessMessages;
  end;
end;

end.
