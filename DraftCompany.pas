unit DraftCompany;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  RisCompany, HebForm, StdCtrls, HGrids, SdGrid, HEdit, ExtCtrls, HLabel,
  DataObjects, CompanyInfoTemplate, Db, DBTables;

type
  TfmDraftCompany = class(TfmRisCompany)
  private
    { Private declarations }
  protected
    procedure FillData; override;
  public
    { Public declarations }
    function Execute(Sender: TfmCompanyInfoTemplate): TFileItem;
  end;

var
  fmDraftCompany: TfmDraftCompany;

implementation

{$R *.DFM}

uses Util;

procedure TfmDraftCompany.FillData;
var
  I: Integer;
begin
  sdNames.Rows[0].Clear;
  FilesList:= TFilesList.Create(-1, faAddingNew, True);
  sdNames.RowCount:= FilesList.Count;
  For I:= 0 To FilesList.Count-1 Do
    begin
      sdNames.Cells[0,I]:= FilesList.FileInfo[I].FileName;
      sdNames.Cells[1,I]:= DateToLongStr(FilesList.FileInfo[I].DecisionDate);
    end;
  //SortGrid(sdNames, 0, -1 ,stString, stString);
  SortGrid(sdNames, '0,STRING,UP');
  sdNames.Repaint;
end;

function TfmDraftCompany.Execute(Sender: TfmCompanyInfoTemplate): TFileItem;
begin
  FSender:= Sender;
  FileItem:= Nil;
  FillData;
  ShowModal;
  Result:= FileItem;
  FilesList.Free;
end;

end.
