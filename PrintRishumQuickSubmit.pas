unit PrintRishumQuickSubmit;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, DataObjects;

type

  TQuickSubmissionData = record
     MagishEMail,
     MagishFax,
     MagishForename,
     MagishFamilyname,
     HebrewName,
     EnglishName: string;
  end;

  TQRPrintRishumQuickSubmit = class(TQuickRep)
    QRBand2: TQRBand;
    QRLabel3: TQRLabel;
    lblHebrewName: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    lblEnglishName: TQRLabel;
    QRLabel8: TQRLabel;
    lblMagishFamilyName: TQRLabel;
    lblMaishForename: TQRLabel;
    lblMagishId: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    lblMagishCity: TQRLabel;
    QRLabel20: TQRLabel;
    lblMagishStreet: TQRLabel;
    QRLabel22: TQRLabel;
    lblMagishHouseNo: TQRLabel;
    QRLabel24: TQRLabel;
    lblMagishZip: TQRLabel;
    QRLabel26: TQRLabel;
    lblMagishPhone: TQRLabel;
    QRLabel28: TQRLabel;
    lblMagishFax: TQRLabel;
    QRLabel30: TQRLabel;
    lblMagishEMail: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    lblCompanyAt: TQRLabel;
    QRLabel35: TQRLabel;
    lblCompanyPOB: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    lbRashumStocks: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    lblLawyerName: TQRLabel;
    QRLabel57: TQRLabel;
    lblLawyerAddress: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel61: TQRLabel;
    lblLawyerId: TQRLabel;
    QRLabel63: TQRLabel;
    lblLawyerLicense: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    lblCompanyCity: TQRLabel;
    QRLabel70: TQRLabel;
    lblCompanyStreet: TQRLabel;
    QRLabel72: TQRLabel;
    lblCompanyHouseNo: TQRLabel;
    QRLabel74: TQRLabel;
    lblCompanyZip: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    lbMukzeStocks: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRImage3: TQRImage;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRImage4: TQRImage;
    QRLabel11: TQRLabel;
    QRMemo1: TQRMemo;
    QRShape2: TQRShape;
    QRLabel19: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel27: TQRLabel;
  private
    function GetRashumStocksString(Company: TCompany): string;
    function GetMukzeStocksString(Company: TCompany): string;
    function GetNumeralString(num: double): string;
    function GetHebrewString(num: double): string;
  public
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintExecute(const FileName: string; Company: TCompany; QuickSubmissionData:TQuickSubmissionData);
    procedure PrintEmpty(const FileName: String);
  end;

var
  QRPrintRishumQuickSubmit: TQRPrintRishumQuickSubmit;

implementation

uses PrinterSetup;

{$R *.DFM}

    procedure TQRPrintRishumQuickSubmit.PrintExecute(const FileName: string; Company: TCompany; QuickSubmissionData:TQuickSubmissionData);
    begin
         Self.lblHebrewName.Caption := QuickSubmissionData.HebrewName;
         Self.lblEnglishName.Caption := QuickSubmissionData.EnglishName;
         Self.lblMagishFamilyName.Caption := QuickSubmissionData.MagishFamilyname;
         Self.lblMaishForename.Caption :=  QuickSubmissionData.MagishForename;
         Self.lblMagishId.Caption := Company.Magish.Zeut;
         Self.lblMagishCity.Caption :=Company.Magish.City;
         Self.lblMagishStreet.Caption := Company.Magish.Street;
         Self.lblMagishHouseNo.Caption := Company.Magish.Number;
         Self.lblMagishZip.Caption := Company.Magish.Index;
         Self.lblMagishPhone.Caption :=Company.Magish.Phone;
         Self.lblMagishFax.Caption := QuickSubmissionData.MagishFax;
         Self.lblMagishEMail.Caption := QuickSubmissionData.MagishEMail;
         Self.lblCompanyCity.Caption := Company.Address.City;
         Self.lblCompanyStreet.Caption :=Company.Address.Street;
         Self.lblCompanyHouseNo.Caption :=Company.Address.Number;
         Self.lblCompanyZip.Caption :=Company.Address.Index;
         Self.lblCompanyAt.Caption :=Company.Address.Ezel;
         Self.lblCompanyPOB.Caption :=Company.Address.POB;
         Self.lblLawyerName.Caption :=Company.CompanyInfo.LawName;
         Self.lblLawyerId.Caption :=  Company.CompanyInfo.LawZeut;
         Self.lblLawyerAddress.Caption := Company.CompanyInfo.LawAddress;
         Self.lblLawyerLicense.Caption := Company.CompanyInfo.LawLicence;
         Self.lbRashumStocks.Caption := GetRashumStocksString(Company);
         Self.lbMukzeStocks.Caption := GetMukzeStocksString(Company);
//.��� ����� ����� 100,000 (��� ���) �"�, ������ �100,000 (��� ���)  ����� ���� 1 �"� �� ���
//.����� 1,000 (���) �����.
         fmPrinterSetup.Execute(FileName, PrintProc);
    end;

    procedure TQRPrintRishumQuickSubmit.PrintProc(DoPrint: Boolean);
    begin
      fmPrinterSetup.FormatQR(Self);
      If DoPrint then
      begin
        Self.Print;
      end
      Else
      begin
        Self.Preview;
        Application.ProcessMessages;
      end;
    end;

    procedure TQRPrintRishumQuickSubmit.PrintEmpty(const FileName: String);
    begin
       fmPrinterSetup.Execute(FileName, PrintProc);
    end;

    function TQRPrintRishumQuickSubmit.GetRashumStocksString(Company: TCompany): string;
    var
      honRashum: double;
      numeralString,
      hebrewString: string;
    begin
      honRashum := THonItem(Company.HonList.Items[0]).Rashum;
      numeralString := GetNumeralString(honRashum);
      hebrewString := GetHebrewString(honRashum);
      result := '.��� ����� ����� '+ numeralString+' ('+hebrewString+') �"�, ������ �'+numeralString+ ' (' + hebrewString+')  ����� ���� 1 �"� �� ���';
    end;

    function TQRPrintRishumQuickSubmit.GetMukzeStocksString(Company: TCompany): string;
    var
      honMukze: double;
      numeralString,
      hebrewString: string;
    begin
      honMukze := THonItem(Company.HonList.Items[0]).Mokza;
      numeralString := GetNumeralString(honMukze);
      hebrewString := GetHebrewString(honMukze);
      result := '.����� '+numeralString+' ('+hebrewString+') �����.';
    end;

    function  TQRPrintRishumQuickSubmit.GetNumeralString(num: double): string;
    begin
       //only 3 possibilities. maybe 4. so no real need for a comlex algorithm
       if (num = 100) then
          result := '100'
       else if (num = 1000) then
          result := '1,000'
       else if (num = 100000) then
          result := '100,000'
       else if (num = 1000000) then
          result := '1,000,000'
       else
         // so we know we're doing something wrong
         raise Exception.Create('GetNumeralString failed because number of stocks was '+FloatToStr(num));
    end;

    function  TQRPrintRishumQuickSubmit.GetHebrewString(num: double): string;
    begin
       //only 3 possibilities. maybe 4. so no real need for a comlex algorithm
       if (num = 100) then
          result := '���'
       else if (num = 1000) then
          result := '���'
       else if (num = 100000) then
          result := '��� ���'
       else if (num = 1000000) then
          result := '�����'
       else
         // so we know we're doing something wrong
         raise Exception.Create('GetHebrewString failed because number of stocks was '+FloatToStr(num));
    end;

end.
