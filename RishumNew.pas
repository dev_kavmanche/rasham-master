{
History  :
DATE        VERSION   BY   TASK     DESCRIPTION
26/12/2019  1.7.7     VG   19364    code refactoring
}
unit RishumNew; 

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, Qrctrls, ExtCtrls, DataObjects;

type
  TfrmQrRishumNewComp = class(TQuickRep)
    QRLabel1: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    lblMagishName: TQRLabel;
    lblMagishAddress: TQRLabel;
    lblMagishPhone: TQRLabel;
    lblMagishMail: TQRLabel;
    QRShape6: TQRShape;
    QRLabel13: TQRLabel;
    lblEzel: TQRLabel;
    QRLabel14: TQRLabel;
    lblHebCompanyName: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape7: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRShape8: TQRShape;
    lblEngCompanyName: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    lblAltHebName1: TQRLabel;
    lblAltEngName1: TQRLabel;
    QRShape12: TQRShape;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    lblAltHebName2: TQRLabel;
    lblAltEngName2: TQRLabel;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    lblAltEngName3: TQRLabel;
    lblAltHebName3: TQRLabel;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRLabel38: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel46: TQRLabel;
    QRShape18: TQRShape;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRBand1: TQRBand;
    QRImage4: TQRImage;
    QRImage3: TQRImage;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    lblDate: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel173: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRLabel60: TQRLabel;
    QRLabel28: TQRLabel;
  private
    { Private declarations }
    FCompany: TCompany;
    procedure PrintEmptyProc(DoPrint: boolean);
  public
    { Public declarations }
    procedure PrintEmpty(Filename: string);
    procedure LoadData(ACompany: TCompany);
    procedure PrintExecute(Filename: string; DoPrint: boolean; ACompany: TCompany);
    procedure PrintProc(DoPrint: boolean);
  end;

var
  frmQrRishumNewComp: TfrmQrRishumNewComp;

implementation

uses PrinterSetup, PrintRishum, utils2;

{$R *.DFM}

procedure TfrmQrRishumNewComp.PrintProc(DoPrint: boolean);
var
  dt: TDateTime;
begin
  fmPrinterSetup.FormatQR(Self);
  If DoPrint then
  begin
    Self.Print;
  end
  else
  begin
    Self.Preview;
    Application.ProcessMessages;
  end;
  dt := Now;
  if (FCompany <> nil) then
    if (FCompany.CompanyInfo <> nil) then
      dt := FCompany.CompanyInfo.RegistrationDate;
  if (Double(dt) = 0.0) then
    dt := Now;

  // Moshe 20/12/2016
  try
    Application.CreateForm(TfmQRRishum, fmQRRishum);
    try
      fmQRRishum.PrintExecute(FCompany, dt);
      fmQRRishum.PrintProc(DoPrint);
    finally
      fmQRRishum.Free;
    end;
  except on E: Exception do
    MessageDlg(E.Message, mtError, [mbOK], -1);
  end;
end;

procedure TfrmQrRishumNewComp.PrintEmptyProc(DoPrint: boolean);
begin
  fmPrinterSetup.FormatQR(Self);
  If DoPrint then
  begin
    Self.Print;
  end
  else
  begin
    Self.Preview;
    Application.ProcessMessages;
  end;
  try
    Application.CreateForm(TfmQRRishum, fmQRRishum);
    try
      fmQRRishum.EmptyData;
      fmQRRishum.PrintEmptyProc(DoPrint);
    finally
      fmQRRishum.Free;
    end;
  except on E: Exception do
    MessageDlg(E.Message, mtError, [mbOK], -1);
  end;
end; // TfrmQrRishumNewComp.PrintEmptyProc

procedure TfrmQrRishumNewComp.PrintEmpty(Filename: string);
begin
  LoadData(nil);
  fmPrinterSetup.Execute(Filename, PrintEmptyProc);
end;

procedure TfrmQrRishumNewComp.PrintExecute(Filename: string; DoPrint: boolean; ACompany: TCompany);
begin
  LoadData(ACompany);
  fmPrinterSetup.Execute(Filename, PrintProc);
end;

procedure TfrmQrRishumNewComp.LoadData(ACompany: TCompany);
var
  dt: TDateTime;
  address, s: string;
begin
  FCompany := ACompany;
  if ACompany=Nil then
  begin
    lblDate.Caption := EMPTY_DATE;
    lblMagishName.Caption := '';
    lblMagishAddress.Caption := '';
    lblMagishPhone.Caption := '';
    lblMagishMail.Caption := '';
    lblEzel.Caption := '';
    lblHebCompanyName.Caption := '';
    lblEngCompanyName.Caption := '';
    lblAltHebName1.Caption := '';
    lblAltEngName1.Caption := '';
    lblAltHebName2.Caption := '';
    lblAltEngName2.Caption := '';
    lblAltHebName3.Caption := '';
    lblAltEngName3.Caption := '';
    Exit;
  end;

  if (ACompany.SubmissionData.Date=0.0)then
    dt := Now
  else
    dt := ACompany.SubmissionData.Date;

  lblDate.Caption := DateToStr(dt);
  lblMagishName.Caption := ACompany.SubmissionData.RepresentativeName;
  address := trim(ACompany.Magish.Street);
  s := trim(ACompany.Magish.Number);
  if (address<>'')and(s<>'')and(s<>'0')then
    address := address+' ';
  if s<>'0' then
    address := address+s;
  s := trim(ACompany.Magish.City);
  if (address<>'')and(s<>'')then
    address := address+' ';
  address := address+s;
  s := trim(ACompany.Magish.Index);
  if (address<>'')and(s<>'')and(s<>'0')then
    address := address+' ';
  if (s<>'0')then
    address := address+s;
  s := trim(ACompany.Magish.Country);
  if (address <> '') and (s <> '') then
    address := address+' ';
  address := address+s;
  lblMagishAddress.Caption := address;
  lblMagishPhone.Caption := ACompany.SubmissionData.RepresentativePhone;
  lblMagishMail.Caption := ACompany.SubmissionData.RepresentativeMail;
  lblEzel.Caption := ACompany.SubmissionData.Ezel;
  lblHebCompanyName.Caption := ACompany.Name;
  lblEngCompanyName.Caption := ACompany.CompanyInfo.EnglishName;
  lblAltHebName1.Caption := ACompany.SubmissionData.AltHebrewName1;
  lblAltEngName1.Caption := ACompany.SubmissionData.AltEnglishName1;
  lblAltHebName2.Caption := ACompany.SubmissionData.AltHebrewName2;
  lblAltEngName2.Caption := ACompany.SubmissionData.AltEnglishName2;
  lblAltHebName3.Caption := ACompany.SubmissionData.AltHebrewName3;
  lblAltEngName3.Caption := ACompany.SubmissionData.AltEnglishName3;
end;

end.

