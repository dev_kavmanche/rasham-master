unit BlankAddress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HComboBx, HEdit, DataObjects;

type
  TfmBlankAddress = class(TfmBlankTemplate)
    Shape9: TShape;
    Shape13: TShape;
    Shape17: TShape;
    Shape19: TShape;
    Shape6: TShape;
    Shape5: TShape;
    HebLabel6: THebLabel;
    HebLabel3: THebLabel;
    Shape10: TShape;
    Shape14: TShape;
    HebLabel7: THebLabel;
    Shape15: TShape;
    HebLabel8: THebLabel;
    HebLabel9: THebLabel;
    Shape18: TShape;
    Shape16: TShape;
    HebLabel10: THebLabel;
    Shape20: TShape;
    Shape8: TShape;
    Shape12: TShape;
    Shape11: TShape;
    Shape7: TShape;
    HebLabel4: THebLabel;
    HebLabel5: THebLabel;
    e11: THebEdit;
    e15: THebComboBox;
    e17: THebEdit;
    e19: TEdit;
    e18: THebEdit;
    e110: TEdit;
    e16: TEdit;
    e12: THebEdit;
    Shape130: TShape;
    HebLabel49: THebLabel;
    Shape136: TShape;
    edtMail: TEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure FillAddress(Address: TCompanyAddress);
  protected
    procedure EvPrintData; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    function EvLoadData: Boolean; override;
    procedure EvFillDefaultData; override;
    procedure EvDataLoaded; override;
    procedure EvFocusNext(Sender: TObject); override;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmBlankAddress: TfmBlankAddress;

implementation

{$R *.DFM}

uses Util, cSTD, DataM, SaveDialog, LoadDialog, dialog2, PrintAddress;

procedure TfmBlankAddress.DeleteFile(FileItem: TFileItem);
var
  Address: TCompanyAddress;
begin
  If not FileItem.FileInfo.Draft Then
    raise Exception.Create('This file is not Draft at TfmBlankAddress.DeleteFile');
  If (FileItem.FileInfo.Action <> faAddress) Then
    raise Exception.Create('Invalid file type at TfmBlankAddress.DeleteFile');
  Address:= TCompanyAddress.LoadFromFile(Company.RecNum, FileItem);
  Address.FileDelete(Company, FileItem);
  Address.Free;
  FileItem.DeleteFile;
end;

function TfmBlankAddress.EvLoadData: Boolean;
var
  FileItem: TFileItem;
  FilesList: TFilesList;
  Address: TCompanyAddress;
begin
  Result := False;
  FileItem := fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faAddress);
  If FileItem = Nil Then
    Exit;
  DecisionDate := (FileItem.FileInfo.DecisionDate);
  NotifyDate := (FileItem.FileInfo.NotifyDate);
  Address := TCompanyAddress.LoadFromFile(Company.RecNum, FileItem);
  FillAddress(Address);
  Address.Free;
  FileItem.Free;
  FilesList.Free;
  Result := True;
end;

function TfmBlankAddress.EvSaveData(Draft: Boolean): Boolean;
  function CollectData: TCompanyAddress;
  begin
    Result:= TCompanyAddress.CreateNew(e11.Text, e12.Text, e16.Text, e15.Text, '�����', e17.Text, e18.Text, e19.Text, e110.Text)
  end;

var
  FilesList: TFilesList;
  FileInfo: TFileInfo;
  FileItem: TFileItem;
  Address: TCompanyAddress;
begin
  Result := False;
  Address := CollectData;
  FilesList := TFilesList.Create(Company.RecNum, faAddress, Draft);
  try
    FileInfo := TFileInfo.Create;
    FileInfo.Action := faAddress;
    FileInfo.Draft := Draft;
    FileInfo.DecisionDate := DecisionDate;
    FileInfo.NotifyDate := NotifyDate;
    FileInfo.CompanyNum := Company.RecNum;
    FileItem := fmSaveDialog.Execute(Self, FilesList, FileInfo);

    // Moshe 10/01/2018
    if(Trim(UpperCase(Company.CompanyInfo.Mail)) <> Trim(UpperCase(edtMail.Text))) then
    begin
      Company.CompanyInfo.Mail := edtMail.Text;
      Company.CompanyInfo.UpdateData(Company, Company.CompanyInfo.RecNum);
    end;

    if FileItem <> nil then
    begin
      FileName := FileItem.FileInfo.FileName;
      Address.ExSaveData(FileItem, Company, DecisionDate);
      FileItem.Free;
      fmDialog2.Show;
      Result := True;
    end
    else
      FileInfo.Free;
  finally
    try
    finally
      Address.Free;
      FilesList.Free;
    end;
  end;
end; // TfmBlankAddress.EvSaveData

procedure TfmBlankAddress.FillAddress(Address: TCompanyAddress);
begin
  e11.Text:= Address.Street;
  e12.Text:= Address.Number;
  e15.Text:= Address.City;
  e16.Text:= Address.Index;
  e17.Text:= Address.POB;
  e18.Text:= Address.Ezel;
  e19.Text:= Address.Phone;
  e110.Text:= Address.Fax;
end;

procedure TfmBlankAddress.EvDataLoaded;
begin
  inherited;
  Loading := True;
  if (not DoNotClear) then
    FillAddress(Company.Address);
  edtMail.Text := Company.CompanyInfo.Mail;
  Loading := False;
end;

procedure TfmBlankAddress.EvFillDefaultData;
begin
  inherited;
  DATA.taCity.First;
  While Not DATA.taCity.EOF do
    begin
      e15.Items.Add(DATA.taCity.Fields[0].Text);
      DATA.taCity.Next;
    end;
end;

procedure TfmBlankAddress.EvFocusNext(Sender: TObject);
begin
  Inherited;
  If Sender = e14  then e11.SetFocus;
  If Sender = e11  then e12.SetFocus;
  if Sender = e12  then e15.SetFocus;
  if Sender = e15  then e16.SetFocus;
  if Sender = e16  then e17.SetFocus;
  if Sender = e17  then e18.SetFocus;
  if Sender = e18  then e19.SetFocus;
  if Sender = e19  then e110.SetFocus;
  if Sender = e110 then e11.SetFocus;
end;

procedure TfmBlankAddress.FormCreate(Sender: TObject);
begin
  FileAction:= faAddress;
  ShowStyle:= afActive;
  inherited;
end;

procedure TfmBlankAddress.EvPrintData;
begin
  Application.CreateForm(TfmQRAddress, fmQRAddress);
  fmQRAddress.PrintExecute(FileName, Company, TCompanyAddress.CreateNew(e11.Text, e12.Text, e16.Text, e15.Text, '�����', e17.Text, e18.Text, e19.Text, e110.Text)
                           , NotifyDate, True);
  fmQRAddress.Free;
end;

end.
