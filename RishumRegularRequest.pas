{
History
date        dest. ver   by         description
21/12/2020  ver 1.8.4   sts        code refactoring
}
unit RishumRegularRequest;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DataObjects,
  StdCtrls, ExtCtrls, Mask, Buttons, HComboBx, HEdit,PrintRishumRegSubmit,
  HebForm, util, HGroupBx, HRadGrup, HLabel;

type
  TfmRishumRegularRequest = class(TForm)
    Label1: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    edPhone: THebEdit;
    edRepName: THebEdit;
    edHebrewName: THebEdit;
    edEnglishName: THebEdit;
    edAdditionalHebrewName1: THebEdit;
    edTakanonCopiesNum: THebEdit;
    Label17: TLabel;
    edAdditionalEnglishName1: THebEdit;
    Label18: TLabel;
    edAdditionalHebrewName2: THebEdit;
    Label19: TLabel;
    edAdditionalEnglishName2: THebEdit;
    Label20: TLabel;
    edAdditionalHebrewName3: THebEdit;
    Label21: TLabel;
    edAdditionalEnglishName3: THebEdit;
    edDate: TMaskEdit;
    pnlDate: TPanel;
    pnlRep: TPanel;
    bnCancel: TBitBtn;
    bnPrint: TBitBtn;
    HebForm1: THebForm;
    Label2: TLabel;
    edEMail: THebEdit;
    Label3: TLabel;
    Label4: TLabel;
    edEzel: THebEdit;
    rgGetDocuments: THebRadioGroup;
    HebLabel1: THebLabel;
    procedure bnPrintClick(Sender: TObject);
    procedure bnCancelClick(Sender: TObject);
    procedure SaveData(RishumData : TRishumData );
    procedure LoadSubmissionData(SubmissionData: TSubmissionData);
  private
    { Private declarations}
    Company : TCompany ;
    DecisionDate: TDateTime;
    FileName: string;
    function GatherData:TRishumData ;
    function GetErrorCtl(var ErrorMessage:string):TWinControl;
  public
    { Public declarations  }
    procedure Open(LoadingFileName: string; LoadingCompany :TCompany; LoadingDecisionDate: TDateTime);
  end;

var
  fmRishumRegularRequest: TfmRishumRegularRequest;

implementation

uses PrintRishum, rishumnew, prTakanon, utils2;

{$R *.DFM}

procedure TfmRishumRegularRequest.Open(LoadingFileName: string; LoadingCompany :TCompany; LoadingDecisionDate: TDateTime);
var
 CompanyNames: TSplitCompanyName;
begin
  Self.Company := LoadingCompany;
  Self.DecisionDate :=  LoadingDecisionDate;
  Self.FileName := LoadingFileName;
  Self.edDate.Text := DateToStr(Now);

  if (Company.CompanyInfo.EnglishName = '') then
  begin
     CompanyNames := SplitCompanyName(Company.Name);
     Self.edEnglishName.Text := CompanyNames.EnglishName;
     Self.edHebrewName.Text := CompanyNames.HebrewName;
  end
  else if (Company.CompanyInfo <> nil ) then
  begin
     Self.edHebrewName.Text := Company.Name;
     Self.edEnglishName.Text := Company.CompanyInfo.EnglishName;
  end;
  LoadSubmissionData(Company.SubmissionData);
  ShowModal;
end;

function TfmRishumRegularRequest.GetErrorCtl(var ErrorMessage:string):TWinControl;
var
  s:string;
  l,p,v,code:integer;
begin
  ErrorMessage := '';
  Result := Nil;
  s := trim(edRepName.text);
  if s = '' then
  begin
     ErrorMessage :='�� ������ �� �� ���� �����';
     result := edRepName;
     exit;
  end;
  s := trim(edPhone.Text);
  if s = '' then
  begin
    ErrorMessage := '�� ������ ��. �����';
    Result := edPhone;
    exit;
  end;
  s := trim(edEMail.text);
  if s <> '' then
  begin
    p := pos('@',s);
    l := length(s);
    if (l < 3) or (p <= 1) or (p >= l)then
    begin
      ErrorMessage := '����� ���� �������� �� �����';
      Result := edEmail;
      exit;
    end;
  end;
  s := trim(edTakanonCopiesNum.text);
  if s = '' then
  begin
    ErrorMessage := '�� ������ ��. ����� ������';
    Result := edTakanonCopiesNum;
    exit;
  end;
  val(s, v, code);
  if (v <= 0)then
  begin
    ErrorMessage := '��. ����� ����� �� ����';
    Result := edTakanonCopiesNum;
    exit;
  end;
end;

procedure TfmRishumRegularRequest.bnPrintClick(Sender: TObject);
var
  RishumData: TRishumData;
  ErrorMsg: string;
  ErrorCmp: TWinControl;
  Filename: string;
begin
  ErrorCmp := GetErrorCtl(ErrorMsg);
  if (ErrorMsg <> '') and (ErrorCmp <> nil) then
  begin
    Application.MessageBox(PChar(ErrorMsg), '�����', MB_OK + MB_ICONERROR + MB_RTLREADING + MB_RIGHT);
    ActiveControl := ErrorCmp;
    Exit;
  end;
  FileName := '���� ������ ����';
  RishumData := GatherData();
  SaveData(RishumData);
  try
    Application.CreateForm(TfrmQrRishumNewComp, frmQrRishumNewComp);
    frmQrRishumNewComp.PrintExecute(FileName, True, Company);
  finally
    if assigned(frmQrRishumNewComp) then
      frmQrRishumNewComp.Free;
  end;
  Close; //on printing complete, close this screen
end;

procedure TfmRishumRegularRequest.bnCancelClick(Sender: TObject);
begin
  Close;
end;


function TfmRishumRegularRequest.GatherData: TRishumData ;
var
   RishumData: TRishumData;
begin
   RishumData.RegistrationDate :=  Self.edDate.Text;
   RishumData.RepName  :=  Self.edRepName.Text;
   RishumData.RepDescription :=  '';//Self.edDescription.Text; Eden 23/11/2016
   RishumData.RepPhone :=  Self.edPhone.Text;
   RishumData.RepMail  :=  Self.edEMail.Text;
   RishumData.HebrewName :=  Self.edHebrewName.Text;
   RishumData.EnglishName :=  Self.edEnglishName.Text;
   RishumData.AdditionalHebrewName1 :=  Self.edAdditionalHebrewName1.Text;
   RishumData.AdditionalHebrewName2 :=  Self.edAdditionalHebrewName2.Text;
   RishumData.AdditionalHebrewName3 :=  Self.edAdditionalHebrewName3.Text;
   RishumData.AdditionalEnglishName1 :=  Self.edAdditionalEnglishName1.Text;
   RishumData.AdditionalEnglishName2 :=  Self.edAdditionalEnglishName2.Text;
   RishumData.AdditionalEnglishName3 :=  Self.edAdditionalEnglishName3.Text;
   RishumData.EMail :=  Self.edEMail.Text;
   RishumData.Fax :=  '';//Self.edFax.Text;  Eden 23/11/2016
   RishumData.CertificateIsInterested :=  true;//Self.chkGetCertificate.Checked; Eden 23/11/2016
   RishumData.CopiesIsInterested :=  true;//Self.chkGetCopies.Checked; Eden 23/11/2016
   RishumData.TakanonCopiesNum :=  Self.edTakanonCopiesNum.Text;
   RishumData.CertificateCopiesNum :=  '1';//Self.edCertificateCopiesNum.Text;  Eden 23/11/2016
   RishumData.Payment := '0.0';//Self.edPayment.Text;   Eden 23/11/2016
   RishumData.Ezel :=edEzel.Text;

   Result :=  RishumData;
end;

procedure TfmRishumRegularRequest.SaveData(RishumData: TRishumData);
begin
  self.Company.SubmissionData.ExFillData(
                                   StrToDateTime(RishumData.RegistrationDate),
                                   RishumData.RepName,
                                   RishumData.RepDescription,
                                   RishumData.RepPhone,
                                   RishumData.RepMail,
                                   RishumData.AdditionalHebrewName1,
                                   RishumData.AdditionalEnglishName1,
                                   RishumData.AdditionalHebrewName2,
                                   RishumData.AdditionalEnglishName2,
                                   RishumData.AdditionalHebrewName3,
                                   RishumData.AdditionalEnglishName3,
                                   RishumData.CertificateIsInterested,
                                   RishumData.Email,
                                   RishumData.Fax,
                                   RishumData.CopiesIsInterested,
                                   StrToInt(RishumData.TakanonCopiesNum),
                                   StrToInt(RishumData.CertificateCopiesNum),
                                   StrToFloat(RishumData.Payment), RishumData.Ezel,
                                   (rgGetDocuments.ItemIndex = 0));
  self.Company.SubmissionData.SaveData(Self.Company);
end;

procedure TfmRishumRegularRequest.LoadSubmissionData(SubmissionData: TSubmissionData);
begin
  if( SubmissionData <> nil) then
  begin
    if ( SubmissionData.Date > 0) then
    begin
      Self.edDate.Text :=   DateToStr(SubmissionData.Date); //date should change?
    end;

    Self.edRepName.Text := SubmissionData.RepresentativeName;
    // Self.edDescription.Text := '';//SubmissionData.RepresentativeRole;  Eden 23/11/2016
    Self.edPhone.Text := SubmissionData.RepresentativePhone;
    Self.edEMail.Text  := SubmissionData.RepresentativeMail;
    Self.edAdditionalHebrewName1.Text := SubmissionData.AltHebrewName1;
    Self.edAdditionalHebrewName2.Text := SubmissionData.AltHebrewName2;
    Self.edAdditionalHebrewName3.Text := SubmissionData.AltHebrewName3;
    Self.edAdditionalEnglishName1.Text := SubmissionData.AltEnglishName1;
    Self.edAdditionalEnglishName2.Text := SubmissionData.AltEnglishName2;
    Self.edAdditionalEnglishName3.Text := SubmissionData.AltEnglishName3;
    Self.edEzel.Text:=SubmissionData.Ezel;
  end;
end;

end.
