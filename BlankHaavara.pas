unit BlankHaavara;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, Util, DataObjects, cSTD, HComboBx, HGrids, SdGrid, HEdit;

type
  TfmHaavara = class(TfmBlankTemplate)
    Shape7: TShape;
    Shape27: TShape;
    Shape28: TShape;
    HebLabel15: THebLabel;
    Bevel3: TBevel;
    HebLabel4: THebLabel;
    Shape8: TShape;
    HebLabel5: THebLabel;
    Shape5: TShape;
    HebLabel3: THebLabel;
    Shape6: TShape;
    Shape9: TShape;
    HebLabel6: THebLabel;
    Shape10: TShape;
    Shape11: TShape;
    HebLabel7: THebLabel;
    Shape42: TShape;
    Shape41: TShape;
    HebLabel21: THebLabel;
    Grid: TSdGrid;
    Panel15: TPanel;
    Panel13: TPanel;
    Panel27: TPanel;
    gb31: TPanel;
    gb32: TPanel;
    e17: THebComboBox;
    e16: THebComboBox;
    nGrid: THebStringGrid;
    Panel3: TPanel;
    e15: THebComboBox;
    e18: THebComboBox;
    TabSheet2: TTabSheet;
    Shape38: TShape;
    Shape39: TShape;
    Shape33: TShape;
    Shape12: TShape;
    Shape31: TShape;
    Shape40: TShape;
    Shape34: TShape;
    HebLabel17: THebLabel;
    Shape13: TShape;
    HebLabel8: THebLabel;
    Shape14: TShape;
    Shape35: TShape;
    Shape15: TShape;
    Shape16: TShape;
    Shape17: TShape;
    HebLabel10: THebLabel;
    HebLabel20: THebLabel;
    Shape32: TShape;
    HebLabel16: THebLabel;
    Shape18: TShape;
    HebLabel11: THebLabel;
    e25: TEdit;
    e22: THebEdit;
    e27: THebComboBox;
    e26: THebComboBox;
    e23: THebEdit;
    Panel2: TPanel;
    Panel16: TPanel;
    e24: THebEdit;
    e21: THebEdit;
    HebLabel18: THebLabel;
    HebLabel9: THebLabel;
    Panel28: TPanel;
    HebLabel12: THebLabel;
    Shape19: TShape;
    e31a: THebComboBox;
    Shape22: TShape;
    Shape20: TShape;
    Shape21: TShape;
    btSearch: TButton;
    procedure FormCreate(Sender: TObject);
    procedure gb31Click(Sender: TObject);
    procedure gb32Click(Sender: TObject);
    procedure GridEnter(Sender: TObject);
    procedure GridSelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure GridKeyPress(Sender: TObject; var Key: Char);
    procedure nGridDblClick(Sender: TObject);
    procedure nGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CriticalEditChange(Sender: TObject);
    procedure Panel16Click(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
    procedure ControlKeyPress(Sender: TObject; var Key: Char);
    procedure Panel28Click(Sender: TObject);
    procedure btSearchClick(Sender: TObject);
  private
    { Private declarations }
    FNewStockHolder: Boolean;
    procedure FillnGrid;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    function CollectStocks: TStocks;
  protected
    procedure EvPrintData; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    function EvLoadData: Boolean; override;
    procedure EvDeleteEmptyRows; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    procedure EvFillDefaultData; override;
    procedure EvDataLoaded; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure SetNewStockHolder(Value: Boolean);
    property NewStockHolder: Boolean read FNewStockHolder write SetNewStockHolder;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmHaavara: TfmHaavara;

implementation

{$R *.DFM}

uses Dialog2, DataM, SaveDialog, LoadDialog, PrintHaavara, PeopleSearch;


procedure TfmHaavara.DeleteFile(FileItem: TFileItem);
var
  StockHolders: TStockHolders;
begin
  If Not FileItem.FileInfo.Draft Then
    Raise Exception.Create('This file is not Draft at TfmHaavara.DeleteFile');
  If (FileItem.FileInfo.Action <> faHaavara) Then
    raise Exception.Create('Invalid file type at TfmHaavara.DeleteFile');
  StockHolders:= TStockHolders.LoadFromFile(Company, FileItem);
  StockHolders.FileDelete(FileItem);
  StockHolders.Free;
  FileItem.DeleteFile;
end;

procedure TfmHaavara.LoadEnum(ObjectNum, NewNum: Integer);
var
  I: Integer;
begin
  Company.StockHolders.Filter:= afAll;
  For I:= 0 To Company.StockHolders.Count-1 do
    if Company.StockHolders.Items[I].RecNum = NewNum then
    begin
      e15.Text:=Company.StockHolders.Items[I].Name;
      e16.Text:=Company.StockHolders.Items[I].Zeut;
      Exit;
    end;
end;

function TfmHaavara.EvLoadData: Boolean;

  procedure LoadStockHolder(StockHolder: TStockHolder);
  var
    I: Integer;
  begin
    Company.StockHolders.Filter:= afActive;
    If Company.StockHolders.FindByZeut(StockHolder.Zeut)= nil Then
    begin
      e21.Text:= StockHolder.Zeut;
      e22.Text:= StockHolder.Name;
      e23.Text:= StockHolder.Address.Street;
      e24.Text:= StockHolder.Address.Number;
      e25.Text:= StockHolder.Address.Index;
      e26.Text:= StockHolder.Address.City;
      e27.Text:= StockHolder.Address.Country;
      If StockHolder.Taagid then
        e31a.ItemIndex:= 1
      Else
        e31a.ItemIndex:= 0;
      NewStockHolder:= True;
    end
    Else
      NewStockHolder:= False;
    e17.Text:= StockHolder.Name;
    e18.Text:= StockHolder.Zeut;
    Grid.RowCount:= StockHolder.Stocks.Count;
    For I:= 0 to StockHolder.Stocks.Count-1 do
      With StockHolder.Stocks.Items[I] as TStock do
      begin
        Grid.Cells[0,I]:= Name;
        Grid.Cells[1,I]:= FormatNumber(FloatToStr(Value),15,4,True);
        Grid.Cells[2,I]:= FormatNumber(FloatToStr(Count),15,2,True);
        Grid.Cells[3,I]:= FormatNumber(FloatToStr(Paid),15,2,True);
      end;
  end;

var
  FileItem: TFileItem;
  FilesList: TFilesList;
  StockHolders: TStockHolders;
begin
  Result:= False;
  FileItem:= fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faHaavara);
  If FileItem = Nil Then
    Exit;
  EvDataLoaded;
  DecisionDate:= (FileItem.FileInfo.DecisionDate);
  NotifyDate:= (FileItem.FileInfo.NotifyDate);
  StockHolders:= TStockHolders.LoadFromFile(Company, FileItem);
  LoadStockHolder(StockHolders.Items[0] as TStockHolder);
  Grid.Repaint;
  FileItem.EnumChanges(ocStockHolder, -1, acDelete, LoadEnum);
  FileItem.Free;
  FilesList.Free;
  FillnGrid;
  Result:= True;
end;

procedure TfmHaavara.EvPrintData;
var
  FirstStockHolder, SecondStockHolder: TStockHolder;
  Stocks: TStocks;
begin
  Application.CreateForm(TfmQRHaavara, fmQRHaavara);
  try
    Company.StockHolders.Filter := afAll;
    FirstStockHolder := Company.StockHolders.FindByZeut(e16.Text) as TStockHolder;
    if NewStockHolder then
    begin
      SecondStockHolder := TStockHolder.CreateNew(DecisionDate, e17.Text, e18.Text, e31a.ItemIndex=1,
          TStockHolderAddress.CreateNew(e23.Text, e24.Text, e25.Text, e26.Text, e27.Text), CollectStocks, True);
      Stocks := SecondStockHolder.Stocks;
    end
    else
    begin
      SecondStockHolder := Company.StockHolders.FindByZeut(e18.Text) as TStockHolder;
      Stocks := CollectStocks;
    end;

    fmQRHaavara.PrintExecute(FileName, Company, FirstStockHolder, SecondStockHolder, Stocks, DecisionDate, NotifyDate);

    if NewStockHolder then
      SecondStockHolder.Free
    else
      Stocks.Free;
  finally
    fmQRHaavara.Free;
  end;
end;

function TfmHaavara.CollectStocks: TStocks;
var
  I: Integer;
begin
  Result:= TStocks.CreateNew;
  For I:= 0 To Grid.RowCount-1 Do
    Result.Add( TStock.CreateNew(DecisionDate, Grid.Cells[0,I], StrToFloat(Grid.Cells[1,I]),
                StrToFloat(Grid.Cells[2,I]), StrToFloat(Grid.Cells[3,I]), True));
end;

function TfmHaavara.EvSaveData(Draft: Boolean): Boolean;

  function CollectNewStockHolder: TStockHolder;
  begin
    Result:= TStockHolder.CreateNew(DecisionDate, e17.Text, e18.Text, e31a.ItemIndex = 1,
       TStockHolderAddress.CreateNew(e23.Text, e24.Text, e25.Text, e26.Text, e27.Text), CollectStocks, True);
  end;

  function CollectSecondStockHolder(var NonDraftTemp: TStocks): TStocks;
  var
    StockHolder: TStockHolder;
    I: Integer;
    ChangedStocks: TStocks;
    TempStock: TStock;
  begin
    StockHolder:= (Company.StockHolders.FindByZeut(e18.Text) as TStockHolder);
    StockHolder.Stocks.Filter:= afActive;
    Result:= TStocks.CreateNew;
    NonDraftTemp:= TStocks.CreateNew;
    ChangedStocks:= CollectStocks;
    try
      For I:= 0 To ChangedStocks.Count-1 Do
        with ChangedStocks.Items[I] as TStock do
        begin
          TempStock:= StockHolder.Stocks.FindStock(Name, Value);
          If TempStock<> nil then
          begin
            TempStock.DeActivate(DecisionDate);
            NonDraftTemp.Add(TempStock);
            Result.Add(TStock.CreateNew(DecisionDate, Name, Value, TempStock.Count+Count, TempStock.Paid+Paid, True));
          end
          Else
            Result.Add(TStock.CreateNew(DecisionDate, Name, Value, Count, Paid, True));
        end;
    finally
      ChangedStocks.Free;
    end;
  end;

  function CollectFirstStockHolder(var NonDraftTemp: TStocks): TStocks;

     function Positive(Value: Double): Double;
     begin
       If Value>0 Then
         Result:= Value
       Else
         Result:= 0;
     end;

  var
    StockHolder: TStockHolder;
    I: Integer;
    ChangedStocks: TStocks;
    TempStock: TStock;
  begin
    StockHolder:= (Company.StockHolders.FindByZeut(e16.Text) as TStockHolder);
    StockHolder.Stocks.Filter:= afActive;
    Result:= TStocks.CreateNew;
    NonDraftTemp:= TStocks.CreateNew;
    ChangedStocks:= CollectStocks;
    try
      For I:= 0 To ChangedStocks.Count-1 Do
        with ChangedStocks.Items[I] as TStock do
        begin
          TempStock:= StockHolder.Stocks.FindStock(Name, Value);
          If TempStock<> nil then
          begin
            TempStock.DeActivate(DecisionDate);
            NonDraftTemp.Add(TempStock);
            If TempStock.Count-Count > 0 Then
              Result.Add(TStock.CreateNew(DecisionDate, Name, Value, TempStock.Count - Count, Positive(TempStock.Paid - Paid), True));
          end;
        end;
    finally
      ChangedStocks.Free;
    end;
  end;

var
  FileItem: TFileItem;
  FileInfo: TFileInfo;
  FilesList: TFilesList;
  NonDraftSecondNew, NonDraftSecondTemp, NonDraftFirstNew, NonDraftFirstTemp: TStocks;
  StockHolderNew: TStockHolder;
begin
  Result:= False;
  try
    Company.StockHolders.Filter:= afActive;
    NonDraftFirstNew := CollectFirstStockHolder(NonDraftFirstTemp);
    StockHolderNew:= CollectNewStockHolder;
    If NewStockHolder Then
    begin
      NonDraftSecondNew:= nil;
      NonDraftSecondTemp:= nil;
    end
    Else
      NonDraftSecondNew:= CollectSecondStockHolder(NonDraftSecondTemp);
    try
      FilesList:= TFilesList.Create(Company.RecNum, faHaavara, Draft);
      FileInfo:= TFileInfo.Create;
      FileInfo.Action:= faHaavara;
      FileInfo.Draft:= Draft;
      FileInfo.DecisionDate:= DecisionDate;
      FileInfo.NotifyDate:= NotifyDate;
      FileInfo.CompanyNum:= Company.RecNum;
      FileItem:= fmSaveDialog.Execute(Self, FilesList, FileInfo);
      If FileItem<> Nil then
      begin
        FileName:= FileItem.FileInfo.FileName;
        FileItem.FileInfo.Draft:= True;
        if not Draft Then
        begin
          NonDraftFirstTemp.SaveData(Company.StockHolders.FindByZeut(e16.Text), FileItem.FileInfo.RecNum);
          NonDraftFirstNew.SaveData(Company.StockHolders.FindByZeut(e16.Text), FileItem.FileInfo.RecNum);
          If Not NewStockHolder then
          begin
            NonDraftSecondNew.SaveData(Company.StockHolders.FindByZeut(e18.Text), FileItem.FileInfo.RecNum);
            NonDraftSecondTemp.SaveData(Company.StockHolders.FindByZeut(e18.Text), FileItem.FileInfo.RecNum);
          end
          Else
            StockHolderNew.SaveData(Company, True, False, FileItem.FileInfo.RecNum);
          If NewStockHolder Then
            NewStockHolder:= False;
        end;

        StockHolderNew.ExSaveData(FileItem, Company);
        With (Company.StockHolders.FindByZeut(e16.Text) as TStockHolder) do
          FileItem.SaveChange(ocStockHolder,-1, acDelete,RecNum);

        FileItem.Free;
        fmDialog2.Show;
        Result:= True;
      end
      Else
        FileInfo.Free;
    Finally
      NonDraftFirstTemp.Deconstruct;
      NonDraftFirstNew.Free;
      If Assigned(NonDraftSecondTemp) then
        NonDraftSecondTemp.Deconstruct;
      If Assigned(NonDraftSecondNew) then
        NonDraftSecondNew.Free;
      StockHolderNew.Free;
      FilesList.Free;
    end;
  finally
  end;
end;

procedure TfmHaavara.SetNewStockHolder(Value: Boolean);
begin
  If Value Then
  begin
    e18.Text := e21.Text;
    e17.Text := e22.Text;
  end
  Else
  begin
    e21.Text := '0';
    e22.Text := '';
    e23.Text := '';
    e24.Text := '';
    e25.Text := '0';
    e26.Text := '';
    e27.Text := '�����';
    e31a.ItemIndex := 0;
  end;
  FNewStockHolder := Value;
  e17.Enabled := Not Value;
  e18.Enabled := Not Value;
  PageIndex := 0;
end;

procedure TfmHaavara.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
  I: Integer;
  Stocks: TStocks;
  StockHolder: TStockHolder;
  TempNum, ConvertNum: Double;
begin
  If NewStockholder then
  begin
    e21.Hint:= 'number;empty';
    e22.Hint:= 'empty';
  end
  Else
  begin
    e21.Hint:='';
    e22.Hint:='';
  end;

  Inherited;

  if Page=0 Then
  begin
    CheckGrid(Page, Silent, Extended, Grid);

    If Extended Then
      Company.StockHolders.Filter:= afActive
    else
      Company.StockHolders.Filter:= afAll;
    StockHolder:= (Company.StockHolders.FindByZeut(e16.Text) as TStockHolder);
    If (StockHolder=nil) or (StockHolder.Name<> e15.Text) then
    begin
      If Not Silent Then
        If e16.Color = clWindow Then
          e16.Color := clBtnFace
        Else
          e16.Color:= clWindow;

      ErrorAt:= e16;
      If StockHolder = nil Then
        raise Exception.Create('��� ����� �� �.�. '+e16.Text+' ���� ���� ����� ��.')
      else
        raise Exception.Create('��� ����� ��� '+e15.Text+' ���� ���� ����� ��.');
    end;

    If Not NewStockHolder then
    begin
      StockHolder:= (Company.StockHolders.FindByZeut(e18.Text) as TStockHolder);
      If (StockHolder = nil) Or (StockHolder.Name<> e17.Text) Then
      begin
        If Not Silent Then
          If e17.Color = clWindow Then
            e17.Color := clBtnFace
          Else
            e17.Color:= clWindow;

        ErrorAt:= e17;
        If StockHolder = nil Then
          raise Exception.Create('��� ����� �� �� �.�. '+e18.Text+' ���� ���� ����� ��.')
        else
          raise Exception.Create('��� ����� �� ��� '+e17.Text+' ���� ���� ����� ��.');
      end;
    end;

    If e18.Text = e16.Text Then
    begin
      If Not Silent Then
        If e18.Color = clWindow Then
          e18.Color := clBtnFace
        Else
          e18.Color:= clWindow;
      ErrorAt:= e18;
      raise Exception.Create('�� ���� ����� ���� ����� ����� ����� ���� �� ���');
    end;
  end;

  If Not Extended Then
    Exit;

  Case Page of
    0: begin
         Stocks:= TStocks.CreateNew;
         try
           For I:= 0 To Grid.RowCount-1 Do
             If Stocks.FindStock(Grid.Cells[0,I], StrToFloat(Grid.Cells[1,I])) = nil then
             begin
               Stocks.Add( TStock.CreateNew(DecisionDate, Grid.Cells[0,I], StrToFloat(Grid.Cells[1,I]),
                  StrToFloat(Grid.Cells[2,I]), StrToFloat(Grid.Cells[3,I]), True));
               Try
                 TempNum:= StrToFloat(nGrid.Cells[3, DoubleIndexOf(nGrid, Grid.Cells[0,I], Grid.Cells[1,I], 1)]);
               Except
                 TempNum:= 0;
               end;
               //==>Tsahi 15.06.2005 solves the problem with tranfering parts of stocks
               //  (StrToFloat is Extended while TempNum is Double. This causes an inaccuracy when comparing.)
               ConvertNum:=StrToFloat(Grid.Cells[2,I]);
               If TempNum < ConvertNum Then
               //<==
               begin
                 If not Silent Then
                 begin
                   Grid.Row:= I;
                   Grid.Col:= 2;
                 end;
                 ErrorAt:= Grid;
                 raise Exception.Create('���� ������ ������� ����� ������� ������ ������');
               end;
             end
             Else
             Begin
               If Not Silent Then
               begin
                 Grid.Row:= I;
                 Grid.Col:= 0;
               end;
               ErrorAt:= Grid;
               raise Exception.Create('�� ���� ����� ���� ���� ���� ���� ���');
             end;
         finally
           Stocks.Free;
         end;
       end;
    1: If NewStockHolder and (Company.StockHolders.FindByZeut(e22.Text) <> nil) then
       begin
         If Not Silent then
           If e22.Color = clWindow Then
             e22.Color:= clBtnFace
           Else
             e22.Color:= clWindow;
         ErrorAt:= e22;
         raise Exception.Create('���� ���� �� ��� ���� ���� ����� ���');
       end;
  end;
end;

procedure TfmHaavara.EvFocusNext(Sender: TObject);
begin
  Inherited;
  if Sender = e14 then
    If PageIndex = 0 then
      e15.SetFocus
    Else
      e21.SetFocus;
  If Sender = e15 Then e16.SetFocus;
  If Sender = e16 Then
    If NewStockHolder then
      Grid.SetFocus
    Else
      e17.SetFocus;
  If Sender = e17 Then e18.SetFocus;
  If Sender = e18 Then Grid.SetFocus;
  If Sender = e21 Then e22.SetFocus;
  If Sender = e22 Then e23.SetFocus;
  If Sender = e23 Then e24.SetFocus;
  If Sender = e24 Then e25.SetFocus;
  If Sender = e25 Then e26.SetFocus;
  If Sender = e26 Then e27.SetFocus;
  If Sender = e27 Then e31a.SetFocus;
end;

procedure TfmHaavara.EvFillDefaultData;
begin
  Inherited;
  DATA.taCity.First;
  While Not DATA.taCity.EOF do
  begin
    e26.Items.Add(DATA.taCity.Fields[0].Text);
    DATA.taCity.Next;
  end;

  DATA.taCountry.First;
  While Not DATA.taCountry.EOF do
  begin
    e27.Items.Add(DATA.taCountry.Fields[0].Text);
    DATA.taCountry.Next;
  end;
end;

procedure TfmHaavara.EvDataLoaded;
var
  I: Integer;
begin
  Inherited;
  e15.Items.Clear;
  e16.Items.Clear;
  e17.Items.Clear;
  e18.Items.Clear;
  Company.StockHolders.Filter:= afActive;
  For I:= 0 To Company.StockHolders.Count-1 do
  begin
    e15.Items.Add(Company.StockHolders.Items[I].Name);
    e17.Items.Add(Company.StockHolders.Items[I].Name);
    e16.Items.Add(Company.StockHolders.Items[I].Zeut);
    e18.Items.Add(Company.StockHolders.Items[I].Zeut);
  end;

  If DoNotClear Then
    FillnGrid
  Else
  begin
    e15.Text:='';
    e16.Text:='0';
    e17.Text:='';
    e18.Text:='0';
    e22.Text:= '';
    e23.Text:= '';
    e24.Text:= '';
    e26.Text:= '';
    e27.Text:= '�����';
    e31a.ItemIndex:= 0;
    NewStockHolder:= False;
    Grid.RowCount:= 1;
    Grid.ResetLine(0);
    nGrid.RowCount:= 1;
    nGrid.Rows[0].Clear;
    Grid.Repaint;
    nGrid.Repaint;
  end;
end;

procedure TfmHaavara.EvDeleteEmptyRows;
var
  I: Integer;
begin
  With Grid Do
    For I := RowCount - 1 DownTo 0 Do
      If (RemoveSpaces(Cells[0, I], rmBoth) = '') And CheckNumberField(Cells[1, I])
          And CheckNumberField(Cells[2, I]) And CheckNumberField(Cells[3,I]) Then
        LineDeleteByIndex(I);
end;

procedure TfmHaavara.FormCreate(Sender: TObject);
begin
  FileAction:= faHaavara;
  ShowStyle:= afActive;
  inherited;
end;

procedure TfmHaavara.FillnGrid;
var
  I: Integer;
begin
  nGrid.RowCount := 1;
  nGrid.Rows[0].Clear;
  With Company.StockHolders.FindByZeut(e16.Text) as TStockHolder do
    try
      Stocks.Filter := afActive;
      nGrid.RowCount := Stocks.Count;
      For I := 0 To Stocks.Count - 1 do
        With Stocks.Items[I] as TStock do
        begin
          nGrid.Cells[0, I] := IntToStr(I + 1);
          nGrid.Cells[1, I] := Name;
          nGrid.Cells[2, I] := FormatNumber(FloatToStr(Value), 15, 4, True);
          nGrid.Cells[3, I] := FormatNumber(FloatToStr(Count), 15, 2, True);
          nGrid.Cells[4, I] := FormatNumber(FloatToStr(Paid),  15, 2, True);
        end;
    except
    end;
  nGrid.Repaint;
end;

procedure TfmHaavara.gb31Click(Sender: TObject);
begin
  inherited;
  Grid.LineDelete;
end;

procedure TfmHaavara.gb32Click(Sender: TObject);
begin
  inherited;
  Grid.LineInsert;
end;

procedure TfmHaavara.GridEnter(Sender: TObject);
var
 CanSelect: Boolean;
begin
  inherited;
  if (Grid.Row = 0) and (Grid.Col = 0) then
    GridSelectCell(Grid, 0, 0, CanSelect);
end;

procedure TfmHaavara.GridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
var
  I, Found: Integer;
begin
  inherited;
  if Sender = Grid then
  begin
    if Grid.PCol = 1 then
      Grid.Cells[Grid.Pcol, Grid.Prow] := FormatNumber(Grid.Cells[Grid.Pcol, Grid.Prow], 10, 4, True);
    if (Grid.PCol = 2) or (Grid.PCol = 3) then
      Grid.Cells[Grid.Pcol, Grid.Prow] := FormatNumber(Grid.Cells[Grid.Pcol,Grid.Prow], 10, 2, True);

    if (Grid.Pcol = 0) then
    begin
      Found := -1;
      for i := 0 to nGrid.RowCount - 1 do
        if (Grid.Cells[0, Grid.PRow] = nGrid.Cells[0, i]) And (RemoveSpaces(nGrid.Cells[0, I], rmBoth) <> '') then
          Found := i;
      if Found <> -1 then
      begin
        Grid.Cells[0, Grid.PRow] := nGrid.Cells[1, Found];
        Grid.Cells[1, Grid.PRow] := nGrid.Cells[2, Found];
      end;
    end;
    nGrid.Visible := (Col = 0);
  end;
end;

procedure TfmHaavara.GridKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (key=#13) Then
    If ((Sender as TSDGrid).EditorMode = false) then
      (Sender as TSDGrid).EditorMode := True
    Else
      nGrid.Visible:= False;
end;

procedure TfmHaavara.nGridDblClick(Sender: TObject);
var
  VKKEY: Word;
begin
  inherited;
  VKKEY:= VK_NONAME;
  Grid.Cells[0, Grid.Row] := nGrid.Cells[1, nGrid.Row];
  Grid.Cells[1, Grid.Row] := nGrid.Cells[2, nGrid.Row];
  Grid.Col := 1;
  Grid.SetFocus;
  GridKeyDown(Grid, VKKEY, []);
end;

procedure TfmHaavara.nGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (key = VK_RETURN) then
    nGridDblClick(Sender);

  if (key = VK_ESCAPE) then
  begin
    Grid.Col := 1;
    Grid.SetFocus;
  end;
end;

procedure TfmHaavara.CriticalEditChange(Sender: TObject);
begin
  inherited;
  if Sender = e15 then
  begin
    If e15.ItemIndex <> -1 Then
      e16.ItemIndex := e15.ItemIndex;
    FillnGrid;
  end;
  if Sender = e16 then
  begin
    If e16.ItemIndex <> -1 Then
      e15.ItemIndex := e16.ItemIndex;
    FillnGrid;
  end;
  if Sender = e17 then
  begin
    If e17.ItemIndex <> -1 Then
      e18.ItemIndex := e17.ItemIndex;
    FillnGrid;
  end;
  if Sender = e18 then
  begin
    If e18.ItemIndex <> -1 Then
      e17.ItemIndex := e18.ItemIndex;
    FillnGrid;
  end;
end;

procedure TfmHaavara.Panel16Click(Sender: TObject);
begin
  inherited;
  NewStockHolder:= False;
end;

procedure TfmHaavara.Panel2Click(Sender: TObject);
begin
  inherited;
  If e18.Items.IndexOf(e21.Text) = -1 Then
    NewStockHolder:= True
  Else
  begin
    e18.ItemIndex:= e18.Items.IndexOf(e21.Text);
    CriticalEditChange(e18);
    NewStockHolder:= False;
  end;
end;

procedure TfmHaavara.ControlKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Sender = e15 then
  begin
    If e15.ItemIndex <> -1 Then
      e16.ItemIndex := e15.ItemIndex;
    FillnGrid;
  end;
  if Sender = e16 then
  begin
    If e16.ItemIndex <> -1 Then
      e15.ItemIndex := e16.ItemIndex;
    FillnGrid;
  end;
  if Sender= e17 then
  begin
    If e17.ItemIndex <> -1 Then
      e18.ItemIndex := e17.ItemIndex;
    FillnGrid;
  end;
  if Sender= e18 then
  begin
    If e18.ItemIndex <> -1 Then
      e17.ItemIndex := e18.ItemIndex;
    FillnGrid;
  end;
end;

procedure TfmHaavara.Panel28Click(Sender: TObject);
begin
  inherited;
  pnB1Click(pnB2);
end;

procedure TfmHaavara.btSearchClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= TCompany.Create(Company.RecNum, Company.ShowingDate, False);
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
  begin
    e22.SetFocus;
    Exit;
  end;

  Loading:= True;
  If FResult is TStockHolder Then
    With FResult as TStockHolder do
    begin
      if Taagid Then
        e31a.ItemIndex:= 1
      Else
        e31a.ItemIndex:= 0;
      e22.Text:= Name;
      e21.Text:= Zeut;
      e23.Text:= Address.Street;
      e24.Text:= Address.Number;
      e25.Text:= Address.Index;
      e26.Text:= Address.City;
      e27.Text:= Address.Country;
    end;

  if FResult is TDirector Then
    With FResult as TDirector do
    begin
      if Taagid<> nil Then
        e31a.ItemIndex:= 1
      Else
        e31a.ItemIndex:= 0;
      e22.Text:= Name;
      e21.Text:= Zeut;
      e23.Text:= Address.Street;
      e24.Text:= Address.Number;
      e25.Text:= Address.Index;
      e26.Text:= Address.City;
      e27.Text:= Address.Country;
    end;
  TempCompany.Free;
  Loading:= False;
  DataChanged:= True;
  e22.SetFocus;
end;

end.
