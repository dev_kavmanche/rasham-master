unit PrintRishumRegSubmit;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, DataObjects;

type
 TRishumData = Record
        RegistrationDate : string;
        RepName          : string;
        RepDescription   : string;
        RepPhone         : string;
        RepMail          : string;
        HebrewName :string;
        EnglishName: string;
        AdditionalHebrewName1: string;
        AdditionalHebrewName2: string;
        AdditionalHebrewName3 : string;
        AdditionalEnglishName1 : string;
        AdditionalEnglishName2 : string;
        AdditionalEnglishName3  : string;
        EMail : string;
        Fax : string;
        CertificateIsInterested : boolean;
        CopiesIsInterested : boolean;
        TakanonCopiesNum : string;
        CertificateCopiesNum : string;
        Payment: string;
        Ezel:string;
    end;

  TQRPrintRishumRegSubmit = class(TQuickRep)
    QRLabel2: TQRLabel;
    lblDate: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    lblMagishName: TQRLabel;
    QRLabel7: TQRLabel;
    lblAddress: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    lblRepName: TQRLabel;
    lblRepDescription: TQRLabel;
    lblRepPhone: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    lblHebrewName: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    lblEnglishName: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    lblHebrewAdditionalName1: TQRLabel;
    lblHebrewAdditionalName2: TQRLabel;
    QRLabel20: TQRLabel;
    lblEnglishAdditionalName2: TQRLabel;
    lblHebrewAdditionalName3: TQRLabel;
    lblEnglishAdditionalName1: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    lblEMail: TQRLabel;
    QRLabel33: TQRLabel;
    lblFax: TQRLabel;
    QRLabel35: TQRLabel;
    lblCertificateInterested: TQRLabel;
    lblCertificateNotInterested: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    lblCopiesInterested: TQRLabel;
    QRLabel42: TQRLabel;
    lblTakanonCopiesNum: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    lblPayment: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    lblCertificateCopiesNum: TQRLabel;
    QRLabel54: TQRLabel;
    lblEnglishAdditionalName3: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRBand1: TQRBand;
    QRShape1: TQRShape;
    lblRepMail: TQRLabel;
    QRLabel16: TQRLabel;
  //regular submission form
  private
    FCompany : TCompany ;
    FDecisionDate: TDateTime;
    FFileName: string;
  public 
    procedure PrintProc(DoPrint: Boolean);
    procedure EmptyPrintProc(DoPrint: Boolean);
    procedure PrintExecute(const FileName: string; Company: TCompany; DecisionDate: TDateTime;   RishumData: TRishumData);
    procedure PrintEmpty(const FileName: String);
  end;

var
  QRPrintRishumRegSubmit: TQRPrintRishumRegSubmit;

implementation

uses
  Dialogs,
  PrinterSetup, PrintRishum;

{$R *.DFM}
procedure TQRPrintRishumRegSubmit.PrintProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(Self);
  If DoPrint then
    begin
      Self.Print;
    end
  Else
    begin
      Self.Preview;
      Application.ProcessMessages;
    end;
  try
    Application.CreateForm(TfmQRRishum, fmQRRishum);
    try
    fmQRRishum.PrintExecute(FCompany, FDecisionDate);
    fmQRRishum.PrintProc(DoPrint);
    finally
    fmQRRishum.Free;
    end;
  except on E: Exception do
    MessageDlg(E.Message, mtError, [mbOK], -1);
  end;
end;

procedure TQRPrintRishumRegSubmit.EmptyPrintProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(Self);
  If DoPrint then
    begin
      Self.Print;
    end
  Else
    begin
      Self.Preview;
      Application.ProcessMessages;
    end;

    Application.CreateForm(TfmQRRishum, fmQRRishum);
    fmQRRishum.PrintEmpty('');
    fmQRRishum.PrintEmptyProc(DoPrint);
    fmQRRishum.Free;
end;
procedure TQRPrintRishumRegSubmit.PrintExecute(const FileName: string; Company: TCompany; DecisionDate: TDateTime; RishumData: TRishumData);
begin
     Self.FCompany := Company;
     Self.FDecisionDate :=  DecisionDate;
     Self.FFileName := FileName;

     Self.lblDate.Caption := RishumData.RegistrationDate;
     Self.lblMagishName.Caption := Company.Magish.Name;
     Self.lblAddress.Caption  := Company.Magish.Street + ' '+Company.Magish.Number+', '+Company.Magish.City+' '+Company.Magish.Country+' '+Company.Magish.Index;

     Self.lblRepName.Caption  :=  RishumData.RepName;
     Self.lblRepDescription.Caption  := RishumData.RepDescription;
     Self.lblRepPhone.Caption :=    RishumData.RepPhone;
     Self.lblRepMail.Caption  :=    Rishumdata.RepMail;
     Self.lblHebrewName.Caption :=  RishumData.HebrewName;
     Self.lblEnglishName.Caption := RishumData.EnglishName   ;
     Self.lblHebrewAdditionalName1.Caption :=RishumData.AdditionalHebrewName1;
     Self.lblHebrewAdditionalName2.Caption :=RishumData.AdditionalHebrewName2;
     Self.lblHebrewAdditionalName3.Caption :=RishumData.AdditionalHebrewName3;
     Self.lblEnglishAdditionalName1.Caption :=RishumData.AdditionalEnglishName1;
     Self.lblEnglishAdditionalName2.Caption :=RishumData.AdditionalEnglishName2;
     Self.lblEnglishAdditionalName3.Caption :=RishumData.AdditionalEnglishName3;
     Self.lblEMail.Caption := RishumData.EMail;
     Self.lblFax.Caption := RishumData.Fax;

     if RishumData.CertificateIsInterested then
     begin
          Self.lblCertificateInterested.Caption :=  'X';
          Self.lblCertificateNotInterested.Caption := '';
     end else
     begin
          Self.lblCertificateInterested.Caption :=  '';
          Self.lblCertificateNotInterested.Caption := 'X';
     end;

     if RishumData.CertificateIsInterested then
     begin
          Self.lblCopiesInterested.Caption := 'X';
     end
     else begin
          Self.lblCopiesInterested.Caption := '';     
     end;

     Self.lblTakanonCopiesNum.Caption := RishumData.TakanonCopiesNum;
     Self.lblCertificateCopiesNum.Caption :=  RishumData.CertificateCopiesNum;
     Self.lblPayment.Caption :=  RishumData.Payment;

     fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TQRPrintRishumRegSubmit.PrintEmpty(const FileName: String);
begin
   fmPrinterSetup.Execute(FileName, EmptyPrintProc);
end;

end.
