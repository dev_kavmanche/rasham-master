unit qrTakanonChange;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, Db, memdset, DataObjects;

type
  TfmQrTakanonChange = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRImage4: TQRImage;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRImage3: TQRImage;
    lblDate: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    edtCompanyName: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel8: TQRLabel;
    edtCompNum: TQRLabel;
    QRLabel10: TQRLabel;
    edtAsefaDate: TQRLabel;
    QRLabel11: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    taMemPurposes: TMemDataSet;
    QRDBText1: TQRDBText;
    QRDBRichText1: TQRDBRichText;
    QRSubDetail2: TQRSubDetail;
    QRLabel12: TQRLabel;
    taMemResponsibility: TMemDataSet;
    QRDBText2: TQRDBText;
    QRBand2: TQRBand;
    QRSubDetail3: TQRSubDetail;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape1: TQRShape;
    lblNoValue: TQRLabel;
    QRLabel17: TQRLabel;
    QRShape2: TQRShape;
    lblValue: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape3: TQRShape;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    edtTotal: TQRLabel;
    taMemHon: TMemDataSet;
    QRDBText3: TQRDBText;
    QRLabel21: TQRLabel;
    QRDBText4: TQRDBText;
    QRShape4: TQRShape;
    QRBand3: TQRBand;
    QRSubDetail4: TQRSubDetail;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    edtTotal2: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    taMemHonDetails: TMemDataSet;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRBand4: TQRBand;
    QRSubDetail5: TQRSubDetail;
    QRLabel32: TQRLabel;
    taMem175: TMemDataSet;
    QRDBText12: TQRDBText;
    QuickRep2: TQuickRep;
    QRBand6: TQRBand;
    QRLabel38: TQRLabel;
    edtLimitIndex: TQRLabel;
    edtLimitCHapters: TQRLabel;
    QRCompositeReport1: TQRCompositeReport;
    QRLabel33: TQRLabel;
    edtAsefaDate2: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    edtMagishName: TQRLabel;
    edtMagishIsTaagid: TQRLabel;
    edtMagishZeut: TQRLabel;
    edtMagishStreet: TQRLabel;
    edtHagashaDate: TQRLabel;
    edtMagishPhone: TQRLabel;
    edtMagishHouse: TQRLabel;
    edtMagishZip: TQRLabel;
    edtMagishCity: TQRLabel;
    edtMagishCountry: TQRLabel;
    edtMagishMail: TQRLabel;
    edtMagishFax: TQRLabel;
    QRBand5: TQRBand;
    QRShape17: TQRShape;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    procedure QRDBText12Print(sender: TObject; var Value: String);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    procedure LoadData(ACompany:TCompany);
    procedure PrintProc(DoPrint:boolean);
  public
    { Public declarations }
    procedure PrintExecute(FileName:string;DoPrint:boolean;ACompany:TCompany);
    procedure PrintEmpty(const FileName: String);
  end;

var
  fmQrTakanonChange: TfmQrTakanonChange;

implementation
uses utils2, PrinterSetup;
{$R *.DFM}
procedure TfmQrTakanonChange.PrintEmpty(const FileName: String);
begin
  LoadData(Nil);
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQrTakanonChange.PrintExecute(FileName:string;DoPrint:boolean;ACompany:TCompany);
begin
  LoadData(ACompany);
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQrTakanonChange.PrintProc(DoPrint:boolean);
var i:integer;
begin
  for i:=1 to 2 do
       fmPrinterSetup.FormatQR(FindComponent('QuickRep'+IntToStr(i)) as TQuickRep);
   fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
   if DoPrint then
       QRCompositeReport1.Print
   else
     QRCompositeReport1.Preview;
end;

procedure TfmQrTakanonChange.LoadData(ACompany:TCompany);
var
  v,code,i:integer;
  str:string;
  total:Double;
  FirstRecord:boolean;
begin
  StartTable(taMemPurposes);
  StartTable(taMemResponsibility);
  StartTable(taMemHon);
  StartTable(taMemHonDetails);
  StartTable(taMem175);
  if ACompany=Nil then
    begin
      lblDate.Caption:=EMPTY_DATE;
    //  lblDate2.Caption:=EMPTY_DATE;
      edtCompanyName.Caption:=pad('_',20);
      edtCompNum.Caption:=pad('_',10);
      edtAsefaDate.Caption:=EMPTY_DATE;
      edtAsefaDate2.Caption:=EMPTY_DATE;
      AppendStr2(taMemPurposes,pad('_',20),pad('_',20));
      AppendStr(taMemResponsibility,pad('_',20));
      SetChecked(lblNoValue,False,0);
      SetChecked(lblValue,False,0);
      edtTotal.Caption:='';
      for i:=1 to 2 do
        AppendStr2(taMemHon,HebNum(i),'');
      edtTotal2.Caption:=pad('_',20);

      taMemHonDetails.Append;
      taMemHonDetails.Post;

      AppendStr(taMem175,'');

      edtLimitIndex.Caption:=pad('_',100);
      edtLimitChapters.Caption:='';

      edtMagishName.Caption:=pad('_',30);
      edtMagishIsTaagid.Caption:=pad('_',2);
      edtMagishZeut.Caption:=pad('_',10);
      edtHagashaDate.Caption:=EMPTY_DATE;
      edtMagishPhone.Caption:=EMPTY_PHONE;
      edtMagishStreet.Caption:=pad('_',30);
      edtMagishHouse.Caption:=pad('_',5);
      edtMagishZip.Caption:=pad('_',10);
      edtMagishCity.Caption:=pad('_',20);
      edtMagishCountry.Caption:=pad('_',20);
      edtMagishMail.Caption:=EMPTY_MAIL;
      edtMagishFax.Caption:=EMPTY_PHONE;
    end
  else
    begin
      str:=MyDate2Str(Now);
      lblDate.Caption:=str;
     // lblDate2.Caption:=str;
      edtCompanyName.Caption:=ACompany.Name;
      edtCompNum.Caption:=ACompany.Zeut;
      str:=MyDate2str(ACompany.ShowingDate);
      edtAsefaDate.Caption:=str;
      edtAsefaDate2.Caption:=str;
      if (ACompany.CompanyInfo.Option1 and 1)>0 then
        AppendStr2(taMemPurposes,'��� ���� 32 (1) ����',ACompany.CompanyInfo.Takanon2_1);
      if (ACompany.CompanyInfo.Option1 and 2)>0 then
        AppendStr2(taMemPurposes,'��� ���� 32 (2) ����',ACompany.CompanyInfo.Takanon2_2);
      if (ACompany.CompanyInfo.Option1 and 4)>0 then
        AppendStr2(taMemPurposes,'��� ���� 32 (3) ����',ACompany.CompanyInfo.Takanon2_3);
      if (ACompany.CompanyInfo.Option1 and 8)>0 then
        AppendStr2(taMemPurposes,'���� �������',ACompany.CompanyInfo.Takanon2_4);
      val(ACompany.CompanyInfo.Option3,v,code);
      if code<>0 then v:=0;
      case v of
        1:str:='������ ������';
        2:str:='���� ������ ������. ����� ��� ���� ������� ������ (��"�, ��"�)';
        3:str:=ACompany.CompanyInfo.OtherInfo;
        else str:='';
      end;
      AppendStr(taMemResponsibility,str);
      total:=0;
      for i:=0 to ACompany.HonList.Count-1 do
        begin
          AppendStr2(taMemHon,HebNum(i+1),ACompany.HonList.Items[i].Name);
          total:=total+THonItem(ACompany.HonList.Items[i]).Value*THonItem(ACompany.HonList.Items[i]).Rashum;
        end;
      FirstRecord:=True;
      for i:=0 to ACompany.HonList.Count-1 do
        begin
          taMemHonDetails.Append;
          if FirstRecord then
            begin
              taMemHonDetails.FieldByName('Total').AsString:=FormatFloat('########0.00',total);
              FirstRecord:=False;
            end
          else
            taMemHonDetails.FieldByName('Total').AsString:='';
          taMemHonDetails.FieldByName('Name').AsString:=ACompany.HonList.Items[i].Name;
          taMemHonDetails.FieldByName('Value').AsFloat:=THonItem(ACompany.HonList.Items[i]).Value;
          taMemHonDetails.FieldByName('Zeut').AsString:=ACompany.HonList.Items[i].Zeut;
          taMemHonDetails.FieldByName('Rashum').AsFloat:=THonItem(ACompany.HonList.Items[i]).Rashum;
          taMemHonDetails.FieldByName('Mokza').AsFloat:=THonItem(ACompany.HonList.Items[i]).Mokza;
          taMemHonDetails.FieldByName('TotalValue').AsFloat:=THonItem(ACompany.HonList.Items[i]).Value*THonItem(ACompany.HonList.Items[i]).Rashum;
          taMemHonDetails.Post;
        end;
      SetChecked(lblNoValue,(total<=0),0);
      SetChecked(lblValue,(total>0),0);
      edtTotal.Caption:=FormatFloat('#########0.00',total);
      if total>0 then
        edtTotal2.Caption:=FormatFloat('#########0.00',total)+' �"�'
      else
        edtTotal2.Caption:='��� ��� ����';
      
      taMem175.AppendRecord(['@������ �� ����� �����']);
      case ACompany.CompanyInfo.SharesRestrictIndex of
        0:taMem175.AppendRecord([':����� ����� ����� �� ����� ������ ����� �����, ����� �������']);
        1:taMem175.AppendRecord(['����� ����� ���� ����� �� ����� ������ ����� �����']);
      end;
      str:=trim(ACompany.CompanyInfo.SharesRestrictChapters);
      if str<>'' then
        AppendStr(taMem175,str);

      taMem175.AppendRecord(['@����� �� ���� ������ �� ����� �� ����� ���']);
      case ACompany.CompanyInfo.DontOfferIndex of
        0:taMem175.AppendRecord([':����� ����� ���� ���� ������ �� ����� �� ����� ��� ����� �������']);
        1:taMem175.AppendRecord(['����� ����� ���� ���� ���� ������ �� ����� �� ����� ���']);
      end;
      str:=trim(ACompany.CompanyInfo.DontOfferChapters);
      if str<>'' then
        AppendStr(taMem175,str);

      taMem175.AppendRecord(['@����� �� ���� ���� ������ �����']);
      case ACompany.CompanyInfo.ShareHoldersNoIndex of
        0:taMem175.AppendRecord([':����� ����� ����� �� ���� ���� ������ ����� �� ������ �������']);
        1:taMem175.AppendRecord(['����� ����� ���� ����� �� ���� ���� ������ ����� �� ������, ���� ����� �����']);
      end;
      str:=trim(ACompany.CompanyInfo.ShareHoldersNoChapters);
      if str<>'' then
        AppendStr(taMem175,str);

      case ACompany.CompanyInfo.LimitedSignatoryIndex of
        0:begin
             edtLimitIndex.Caption:='�� ������ ������ ������ �� ������ �������';
             edtLimitChapters.Caption:='';
          end;
        1:begin
             edtLimitIndex.Caption:='������ ������ ������ �� ������ ������� ����� �������';
             edtLimitChapters.Caption:=trim(ACompany.CompanyInfo.LegalSectionsChapters);
          end;
        2:begin
             edtLimitIndex.Caption:='������ ������� ������ ���� ������� ����� �������';
             edtLimitChapters.Caption:=trim(ACompany.CompanyInfo.SignatorySectionsChapters);
          end;
        else
          begin
            edtLimitIndex.Caption:='';
            edtLimitChapters.Caption:='';
          end;
      end;//end case

      edtMagishName.Caption:=ACompany.Magish.Name;
      if ACompany.Magish.Taagid then
        edtMagishIsTaagid.Caption:='��'
      else
        edtMagishIsTaagid.Caption:='��';
      edtMagishZeut.Caption:=ACompany.Magish.Zeut;
      edtHagashaDate.Caption:=MyDate2Str(ACompany.SubmissionData.Date);
      edtMagishPhone.Caption:=ACompany.Magish.Phone;
      edtMagishStreet.Caption:=ACompany.Magish.Street;
      edtMagishHouse.Caption:=ACompany.Magish.Number;
      edtMagishZip.Caption:=ACompany.Magish.Index;
      edtMagishCity.Caption:=ACompany.Magish.City;
      edtMagishCountry.Caption:=ACompany.Magish.Country;
      edtMagishMail.Caption:=ACompany.Magish.Mail;
      edtMagishFax.Caption:=ACompany.Magish.Fax;
    end;
end;

procedure TfmQrTakanonChange.QRDBText12Print(sender: TObject;
  var Value: String);
begin
  (Sender as TQRDBText).Font.Style:=GetTitleFontStyle(Value);
end;

procedure TfmQrTakanonChange.QRCompositeReport1AddReports(Sender: TObject);
var i:integer;
begin
  for i:=1 to 2 do
    QRCompositeReport1.Reports.Add(Self.FindComponent('QuickRep'+IntToStr(i)) as TQuickRep);
end;

procedure TfmQrTakanonChange.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QuickRep1.NewPage;
end;

end.
