// Unit Name: uAppSysUtils
// Purpose :  decalration of general class with usefull sytem methods
/// Author :  Stanislav Segal
/// History : created  05/09/2019  ver 1.0.0
//

unit uAppSysUtils;

interface

uses
  TypInfo,
  Windows
  ;

const
  EVENTLOG_SUCCESS          = $0000;  {EXTERNALSYM EVENTLOG_SUCCESS}
  EVENTLOG_ERROR_TYPE       = $0001;  {EXTERNALSYM EVENTLOG_ERROR_TYPE}
  EVENTLOG_WARNING_TYPE     = $0002;  {EXTERNALSYM EVENTLOG_WARNING_TYPE}
  EVENTLOG_INFORMATION_TYPE = $0004;  {EXTERNALSYM EVENTLOG_INFORMATION_TYPE}
  EVENTLOG_AUDIT_SUCCESS    = $0008;  {EXTERNALSYM EVENTLOG_AUDIT_SUCCESS}
  EVENTLOG_AUDIT_FAILURE    = $0010;  {EXTERNALSYM EVENTLOG_AUDIT_FAILURE}

type

  TIntegerSet = set of 0..SizeOf(Integer) * 8 - 1;
  DWORDLONG = Longint;

  PMemoryStatusEx = ^TMemoryStatusEx;
  TMemoryStatusEx = packed record
    dwLength: DWORD;
    dwMemoryLoad: DWORD;
    dwTotalPhys: DWORDLONG;
    dwAvailPhys: DWORDLONG;
    dwTotalPageFile: DWORDLONG;
    dwAvailPageFile: DWORDLONG;
    dwTotalVirtual: DWORDLONG;
    dwAvailVirtual: DWORDLONG;
    dwAvailExtendedVirtual: DWORDLONG;
  end;

  PWkstaInfo102 = ^TWkstaInfo102;
  TWkstaInfo102 = record
    platformId: DWORD;
    computerName: LPWSTR;
    lanGroup: LPWSTR;
    verMajor: DWORD;
    verMinor: DWORD;
    lanRoot: LPWSTR;
    loggedOnUsers: DWORD;
  end;

  TAppSysUtils = class
    /// purpose: write a message to windows event log
    class procedure WriteWinEventLog(const aEventSource, aMsg: string;
      const aEventType: Integer);
    //purpose: check, is application's command line parameters contain given param
    class function IsCommandLineParamExist(const aPrm: string): Boolean;
    class function GetTheComputerName: string;
    class function GetTheUserName: string;
    class function GetTheUserDomain: string;
    class function GetIPAddress: String;
    class function RamMonitor: TMemoryStatus;
    class function RamMonitorEx: TMemoryStatusEx;  // for os64bit
    /// convert an enum item to it string name
    /// using  S := SetToString(TypeInfo(TFontStyles), fsBold);
    class function EnumItemToString(aInfo: PTypeInfo; const aValue: integer): string;
    /// convert enumeration to comma delimeted string;
    /// using  S := EmumToString(TypeInfo(TFontStyles));
    class function EmumToString(aInfo: PTypeInfo): string;
    /// convert set of ... to it string representation [..,...]
    /// using  S := SetToString(TypeInfo(TFontStyles), Control.Font.Style);
    class function SetToCommaString(aInfo: PTypeInfo; const aSetValue): string;
  end;

implementation

uses
  Winsock
  ,SysUtils
  ;
{ TAppSysUtils }

  function GlobalMemoryStatusEx(var lpBuffer: TMemoryStatusEx): bool;
           stdcall; external kernel32;

  function NetWkstaGetInfo(ServerName: LPWSTR; Level: DWORD; BufPtr: Pointer): Longint;
           stdcall; external 'netapi32.dll' Name 'NetWkstaGetInfo';


/// WriteWinEventLog
/// parameters: const aMsg: string
/// result: none
/// Exceptions: none
/// purpose: write a message to windows event log, log entry location:
///          EventViewer-> Windwos Logs -> Application ->  source: "Commit Export uility"
/// tasks:                   = EVENTLOG_INFORMATION_TYPE
class procedure TAppSysUtils.WriteWinEventLog(const aEventSource, aMsg: string;
   const aEventType: Integer);
var
  handle: THandle;
  msgPtr: Pointer;
begin
  msgPtr := PChar(aMsg);
  handle := RegisterEventSource(nil, PChar(aEventSource));
  if handle > 0 then
  begin
    try
      // EventViewer-> Windwos Logs -> Application ->  source: aEventSource string
      ReportEvent(handle, aEventType, 0, 0, nil, 1, 0, @msgPtr, nil);
    finally
      DeregisterEventSource(handle);
    end;
  end;
end;

/// IsCommandLineParamExist
/// parameters: const aPrm: string
/// result: boolean
/// Exceptions: none
/// purpose: check, is application's command line parameters contain given param
/// tasks:
class function TAppSysUtils.IsCommandLineParamExist(const aPrm: string): Boolean;
var
  i: Integer;
begin
  result := False;
  i:= 1;
  while (not result) and (i <= ParamCount) do
  begin
    result := LowerCase(aPrm) = LowerCase(ParamStr(i));
    inc(i);
  end;
end;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
class function TAppSysUtils.GetTheComputerName: string;
var
  compName: pChar;
  maxLen: DWORDLONG;
begin
  result := '';
  compName := nil;
  try
    try
      MaxLen := 256;
      GetMem(compName, maxLen);
      if GetComputerName(compName, MaxLen) then
        result := string(compName);
    finally
      FreeMem(compName);
    end;
  except
  end;
end;

/// 
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
class function TAppSysUtils.GetTheUserName: string;
var
  compName: pChar;
  maxLen: DWORDLONG;
begin
  result := '';
  compName := nil;
  try
    try
      maxLen := 256;
      GetMem(compName, MaxLen);
      if GetUserName(compName, MaxLen) then
        result := string(compName);
    finally
      FreeMem(compName);
    end;
  except
  end;
end;

/// 
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
class function TAppSysUtils.GetTheUserDomain: string;
var
  pBuf: PWkstaInfo102;
  res: LongInt;
begin
  result := '';
  try
    try
      res := NetWkstaGetInfo(nil, 102, @PBuf);
      if Res = 0 then
        Result := string(PBuf^.lanGroup);
    finally
      //FreeMem(pBuf);
    end;
  except
  end;
end;

/// 
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
class function TAppSysUtils.RamMonitor: TMemoryStatus;
Begin
  try
    FillChar(Result, SizeOf(result), 0);
    result.dwLength := SizeOf(result);
    GlobalMemoryStatus(result);
  except
    FillChar(Result, SizeOf(result), 0);
  end;
End;

///
/// parameters:
/// result:
/// Exceptions:
/// purpose:
/// tasks:
class function TAppSysUtils.RamMonitorEx: TMemoryStatusEx;
begin
  try
    FillChar(Result, SizeOf(result), 0);
    result.dwLength := SizeOf(result);
    Win32Check(GlobalMemoryStatusEx(result));   // todo: check on win64system!!!!
  except
    FillChar(Result, SizeOf(result), 0);
  end;
end;

class function  TAppSysUtils.EnumItemToString(aInfo: PTypeInfo; const aValue: integer): string;
begin
  result := GetEnumName(aInfo, aValue);
end;

class function TAppSysUtils.EmumToString(aInfo: PTypeInfo): string;
var
  i: integer;
  data: PTypeData;       // set's type data
begin
  if aInfo.Kind <> tkEnumeration then
    result := ''
  else
  begin
    data := GetTypeData(aInfo);
    result := '';
    for i := data.MinValue to data.MaxValue do
    begin
      if result <> '' then
        result := Result + ', ';
      result := result + GetEnumName(aInfo, i);
    end;
    result := Format('%s: (%s)', [aInfo.Name, result]);
  end;
end;

class function TAppSysUtils.SetToCommaString(aInfo: PTypeInfo; const aSetValue): string;
var
  i: Integer;
  data: PTypeData;        // set's type data
  enumInfo: PTypeInfo;	  // set's base type info
  enumData: PTypeData;    // set's base type data
begin
  if aInfo.Kind <> tkSet then
    result := ''
  else
  begin
    data := GetTypeData(aInfo);
    enumInfo := data^.CompType^;
    enumData := GetTypeData(enumInfo);

    result := '';
    for i := enumData.MinValue to enumData.MaxValue do
      if i in TIntegerSet(aSetValue) then
      begin
        if result <> '' then
          result := result + ',';
        result := result + GetEnumName(enumInfo, i);
      end;
    result := '[' + Result + ']';
  end;
end;

class function TAppSysUtils.GetIPAddress: String;
type
  pu_long = ^u_long;
var
  varTWSAData : TWSAData;
  varPHostEnt : PHostEnt;
  varTInAddr : TInAddr;
  namebuf : Array[0..255] of char;
begin
  if WSAStartup($101, varTWSAData) <> 0 Then
    result := 'No. IP Address'
  else
  begin
    gethostname(namebuf,sizeof(namebuf));
    varPHostEnt := gethostbyname(namebuf);
    varTInAddr.S_addr := u_long(pu_long(varPHostEnt^.h_addr_list^)^);
    result := inet_ntoa(varTInAddr);
  end;
  WSACleanup;
end;



end.
