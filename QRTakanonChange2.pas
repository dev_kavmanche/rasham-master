unit QRTakanonChange2;

{
History:
date        version  by   task     description
17/12/2019  1.7.7    VG   19075    fixed several bugs
24/12/2019  1.7.7    VG   19364    fixed runtime position and size changes
23/23/2020  1.8.4    sts  22057    refactoring 
}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, Qrctrls, ExtCtrls, DataObjects;

type
  TfmQrTakanonChange2 = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRImage5: TQRImage;
    QRLabel51: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRImage6: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel3: TQRLabel;
    lblCompName: TQRLabel;
    lblCompNum: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape27: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRBand8: TQRBand;
    QRShape30: TQRShape;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    edtCompName: TQRLabel;
    edtCompNum9: TQRLabel;
    edtCompNum8: TQRLabel;
    edtCompNum7: TQRLabel;
    edtCompNum6: TQRLabel;
    edtCompNum5: TQRLabel;
    edtCompNum4: TQRLabel;
    edtCompNum3: TQRLabel;
    edtCompNum2: TQRLabel;
    edtCompNum1: TQRLabel;
    edtAsefaDate: TQRLabel;
    edtPurpose1: TQRLabel;
    edtPurpose2: TQRLabel;
    edtPurpose3: TQRLabel;
    edtPurpose4: TQRLabel;
    edtPurpose2_1: TQRLabel;
    edtPurpose2_2: TQRLabel;
    edtPurpose2_3: TQRLabel;
    edtPurpose3_1: TQRLabel;
    edtPurpose3_2: TQRLabel;
    edtPurpose3_3: TQRLabel;
    edtPurpose4_1: TQRLabel;
    edtPurpose4_2: TQRLabel;
    edtPurpose4_3: TQRLabel;
    QuickRep2: TQuickRep;
    QRBand2: TQRBand;
    QRImage1: TQRImage;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRImage2: TQRImage;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QRShape51: TQRShape;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRBand3: TQRBand;
    QRShape52: TQRShape;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    edtResp1: TQRLabel;
    edtResp2: TQRLabel;
    edtResp2Text: TQRLabel;
    edtNoValueStocks: TQRLabel;
    edtValueStocks: TQRLabel;
    edtTotalCapital: TQRLabel;
    edtStockType1: TQRLabel;
    edtStockType2: TQRLabel;
    edtStockType3: TQRLabel;
    edtStockType4: TQRLabel;
    edt175_1_Yes: TQRLabel;
    edt175_2_Yes: TQRLabel;
    edt175_3_Yes: TQRLabel;
    edt175_1_No: TQRLabel;
    edt175_2_No: TQRLabel;
    edt175_3_No: TQRLabel;
    edt175_1_Text: TQRLabel;
    edt175_2_Text: TQRLabel;
    edt175_3_Text: TQRLabel;
    QuickRep3: TQuickRep;
    QRBand4: TQRBand;
    QRImage3: TQRImage;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRImage4: TQRImage;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRBand5: TQRBand;
    QRShape58: TQRShape;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRShape59: TQRShape;
    QRLabel146: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel156: TQRLabel;
    QRLabel157: TQRLabel;
    QRLabel158: TQRLabel;
    QRShape68: TQRShape;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    QRLabel161: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel163: TQRLabel;
    edtSignLimit1: TQRLabel;
    edtSignLimit2: TQRLabel;
    edtSignLimit3: TQRLabel;
    edtSignLimit2_Text: TQRLabel;
    edtSignLimit3_Text: TQRLabel;
    edtAsefaDate2: TQRLabel;
    edtMagishName: TQRLabel;
    edtMagishID: TQRLabel;
    edtHagashaDate: TQRLabel;
    edtMagishPhone: TQRLabel;
    edtMagishFax: TQRLabel;
    edtMagishMail: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    QRLabel167: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel169: TQRLabel;
    QRLabel170: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRCompositeReport1: TQRCompositeReport;
    lbl1: TQRLabel;
    lbl2: TQRLabel;
    lbl3: TQRLabel;
    lbl4: TQRLabel;
    lbl5: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel173: TQRLabel;
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    DefaultPenWidth: integer;
    FCompany: TCompany;

    procedure LoadData(ACompany: TCompany; dtNow: TDateTime);
    procedure PrintProc(DoPrint: boolean);
    procedure PrintProcEmpty(DoPrint: boolean);
    procedure PrintProcInner(DoPrint: boolean);
  public
    { Public declarations }
    procedure PrintExecute(FileName: string; DoPrint: boolean; ACompany: TCompany; dtNow: TDateTime);
    procedure PrintEmpty(const FileName: String);
  end;

var
  fmQrTakanonChange2: TfmQrTakanonChange2;

implementation

{$R *.DFM}
uses
  utils2, PrinterSetup,
  prTakanon;

procedure TfmQrTakanonChange2.FormCreate(Sender: TObject);
begin
  DefaultPenWidth := GetDefaultPenWidth(Self);
  FCompany := nil;
end;

procedure TfmQrTakanonChange2.PrintEmpty(const FileName: String);
begin
  LoadData(nil, Now);
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQrTakanonChange2.PrintExecute(FileName: string; DoPrint: boolean; ACompany: TCompany; dtNow: TDateTime);
begin
  FCompany := ACompany;
  LoadData(ACompany, dtNow);
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQrTakanonChange2.PrintProc(DoPrint: boolean);
begin
  PrintProcInner(DoPrint);

  // Moshe 23/10/2018
  Application.CreateForm(TfrmQrTakanon, frmQrTakanon);
  try
    frmQrTakanon.InnerPrintProc(FCompany, DoPrint);
  finally
    frmQrTakanon.Free;
  end;
end;

// Moshe 23/10/2018
procedure TfmQrTakanonChange2.PrintProcEmpty(DoPrint: boolean);
begin
  PrintProcInner(DoPrint);
end;

// Moshe 23/10/2018
procedure TfmQrTakanonChange2.PrintProcInner(DoPrint: boolean);
begin
  // Moshe 25/12/2017
  fmPrinterSetup.FormatQR(QuickRep1);
  fmPrinterSetup.FormatQR(QuickRep2);
  fmPrinterSetup.FormatQR(QuickRep3);
  fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
  if DoPrint then
    QRCompositeReport1.Print
  else
  begin
    QuickRep1.Preview;
    Application.ProcessMessages;

    QuickRep2.Preview;
    Application.ProcessMessages;

    QuickRep3.Preview;
    Application.ProcessMessages;
  end;
end;

procedure TfmQrTakanonChange2.LoadData(ACompany: TCompany; dtNow: TDateTime);
const
  MAX_LINE=120;
var
  i,j:integer;
  cmp:TComponent;
  //VG19075
  //zeut,s:string;
  zeut    : String;
  cBuffStr:String;
  //VG19075
  //v,code,start:integer;
  nTmpInt: integer;
  lstSplited: TStringList;
  code:    integer;
  start:   integer;
  totalCapital:double;
  lbl: TQRLabel;
begin
  //VG19364
  for i := 1 to 5 do
    (FindComponent('lbl'+IntToStr(i)) as TQRLabel).Caption := '';
  if ACompany=nil then
  begin
    for i := 0 to ComponentCount-1 do
    begin
     cmp := Components[i];
     if (UpperCase(Copy(cmp.Name,1,3)) = 'EDT') and (cmp is TQRLabel) then
       (cmp as TQRLabel).Caption := '';
    end;
    Exit;
  end;

  edtCompName.Caption := Trim(ACompany.Name);
  zeut := trim(ACompany.Zeut);
  val(zeut, nTmpInt, code);
  if (code <= 0) and (nTmpInt = 0) then
    zeut := '';
  start := 0;
  if (code <= 0) and (length(zeut) > 9) then
    start := length(zeut)-9;
  for i := 1 to 9 do
  begin
   cmp:=FindComponent('edtCompNum'+IntToStr(i));
   if cmp<>nil then
     if cmp is TQRLabel then
     begin
       if i + start > length(zeut)then
         (cmp as TQRLabel).Caption:=''
       else
         (cmp as TQRLabel).Caption:=Copy(zeut,i+start,1);
     end;
  end;
  edtAsefaDate.Caption := DateToStr(dtNow);//ACompany.ShowingDate);
  SetChecked2(edtPurpose1,(ACompany.CompanyInfo.Option1 and 1 > 0));
  SetChecked2(edtPurpose2,(ACompany.CompanyInfo.Option1 and 2 > 0));
  SetChecked2(edtPurpose3,(ACompany.CompanyInfo.Option1 and 4 > 0));
  SetChecked2(edtPurpose4,(ACompany.CompanyInfo.Option1 and 8 > 0));

  lstSplited := TStringList.Create;

  for i := 2 to 4 do
  begin
    cBuffStr := '';
    case i of
      2: if (ACompany.CompanyInfo.Option1 and 2 > 0) then
           cBuffStr := ACompany.CompanyInfo.Takanon2_2;
      3: if (ACompany.CompanyInfo.Option1 and 4 > 0) then
           cBuffStr := ACompany.CompanyInfo.Takanon2_3;
      4: if (ACompany.CompanyInfo.Option1 and 8 > 0) then
           cBuffStr := ACompany.CompanyInfo.Takanon2_4;
    end;
    if (cBuffStr = '') then
      Continue;
    cBuffStr := Trim(RichTextToPlainText(cBuffStr));
//VG19075 old code
//    for j := 1 to 3 do
//    begin
//      cmp := FindComponent('edtPurpose' + IntToStr(i) + '_' + IntToStr(j));
//      if cmp <> nil then
//        if cmp is TQRLabel then
//          (cmp as TQRLabel).Caption := Copy(s,MAX_LINE*(j-1),MAX_LINE);
//    end;
///////////////
//VG19075 new code
    splitPlainText(cBuffStr, lstSplited);
    nTmpInt := lstSplited.Count-1;
    for j := 0 to nTmpInt do
    begin
      cBuffStr := Format('edtPurpose%d_%d', [i, j+1]);
      cmp := FindComponent(cBuffStr);
      if not Assigned(cmp) then
        continue;

      if cmp is TQRLabel then
      begin
        lbl := cmp as TQRLabel;
        lbl.Caption := lstSplited[j];
      end;
    end;
  end;
  SetChecked2(edtResp1,(ACompany.CompanyInfo.Option3 = '1'));
  SetChecked2(edtResp2,(ACompany.CompanyInfo.Option3 = '3'));
  if (ACompany.CompanyInfo.Option3 = '3') then
    edtResp2Text.Caption := ACompany.CompanyInfo.OtherInfo;
  totalCapital := 0;
  ACompany.HonList.Filter := afActive;
  for i:= 0 to ACompany.HonList.Count-1 do
  begin
    totalCapital := totalCapital+THonItem(ACompany.HonList.Items[i]).Value *
      THonItem(ACompany.HonList.Items[i]).Rashum;
  end;
  SetChecked2(edtNoValueStocks,(totalCapital<=0));
  SetChecked2(edtValueStocks,(totalCapital>0));
  edtTotalCapital.Caption := Format('%0.2n', [(totalCapital)]);
  ACompany.HonList.Filter := afActive;
  for i := 0 to 3 do
  begin
    cmp := FindComponent('edtStockType' + IntToStr(i+1));
    if cmp <> nil then
    begin
      if (cmp is TQRLabel) then
      begin
        if ACompany.HonList.Count > i then
          (cmp as TQRLabel).Caption := ACompany.HonList.Items[i].Name
        else
          (cmp as TQRLabel).Caption := '';
      end;
    end;
  end;

  SetChecked2(edt175_1_Yes, (ACompany.CompanyInfo.SharesRestrictIndex = 0));
  SetChecked2(edt175_1_No, (ACompany.CompanyInfo.SharesRestrictIndex <> 0));
  edt175_1_Text.Caption := ACompany.CompanyInfo.SharesRestrictChapters;

  SetChecked2(edt175_2_Yes, (ACompany.CompanyInfo.DontOfferIndex = 0));
  SetChecked2(edt175_2_No, (ACompany.CompanyInfo.DontOfferIndex <> 0));
  edt175_2_Text.Caption := ACompany.CompanyInfo.DontOfferChapters;

  SetChecked2(edt175_3_Yes, (ACompany.CompanyInfo.ShareHoldersNoIndex = 0));
  SetChecked2(edt175_3_No, (ACompany.CompanyInfo.ShareHoldersNoIndex <> 0));
  edt175_3_Text.Caption := ACompany.CompanyInfo.ShareHoldersNoChapters;

  SetChecked2(edtSignLimit1, (ACompany.CompanyInfo.LimitedSignatoryIndex = 0));
  SetChecked2(edtSignLimit2, (ACompany.CompanyInfo.LimitedSignatoryIndex = 1));
  SetChecked2(edtSignLimit3, (ACompany.CompanyInfo.LimitedSignatoryIndex = 2));
  edtSignLimit2_Text.Caption := ACompany.CompanyInfo.LegalSectionsChapters;
  edtSignLimit3_Text.Caption := ACompany.CompanyInfo.SignatorySectionsChapters;

  edtAsefaDate2.Caption:=DateToStr(ACompany.ShowingDate);

  edtMagishName.Caption:=ACompany.Magish.Name;
  edtMagishID.Caption:=FormatInt(ACompany.Magish.Zeut);
  edtHagashaDate.Caption:=DateToStr(Now);
  edtMagishPhone.Caption:=ACompany.Magish.Phone;
  edtMagishFax.Caption:=ACompany.Magish.Fax;
  edtMagishMail.Caption:=ACompany.Magish.Mail;

  lstSplited.Free;

end; // TfmQrTakanonChange2.LoadData

procedure TfmQrTakanonChange2.QRCompositeReport1AddReports(Sender: TObject);
begin
  QRCompositeReport1.Reports.Add(QuickRep1);
  QRCompositeReport1.Reports.Add(QuickRep2);
  QRCompositeReport1.Reports.Add(QuickRep3);
end;

end.
