unit PrintFirstStockHolder;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, Qrctrls, ExtCtrls, DataObjects;

type
  TfmQRFirstStockHolder = class(TForm)
    qrStockHolderNonTaagid: TQuickRep;
    QRBand1: TQRBand;
    QRLabel28: TQRLabel;
    QRLabel40: TQRLabel;
    Label643: TQRLabel;
    QRBand3: TQRBand;
    l1: TQRLabel;
    QRLabel2: TQRLabel;
    l2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel78: TQRLabel;
    l3: TQRLabel;
    l4: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel80: TQRLabel;
    l5: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    l6: TQRLabel;
    label4342: TQRLabel;
    QRLabel79: TQRLabel;
    l7: TQRLabel;
    QRLabel88: TQRLabel;
    l8: TQRLabel;
    QRLabel89: TQRLabel;
    l9: TQRLabel;
    QRLabel90: TQRLabel;
    l10: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel10: TQRLabel;
    qrStockHolderTaagid: TQuickRep;
    QRBand2: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel25: TQRLabel;
    l13: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    l14: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel41: TQRLabel;
    l15: TQRLabel;
    QRLabel43: TQRLabel;
    l16: TQRLabel;
    QRLabel45: TQRLabel;
    l17: TQRLabel;
    QRLabel47: TQRLabel;
    l18: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRBand4: TQRBand;
    l11: TQRLabel;
    QRLabel52: TQRLabel;
    l12: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    qrPrivate: TQuickRep;
    QRBand5: TQRBand;
    QRLabel22: TQRLabel;
    QRLabel61: TQRLabel;
    edtPrivateLawyerName: TQRLabel;
    edtPrivateFullName: TQRLabel;
    QRLabel64: TQRLabel;
    edtPrivateCustomerZehut: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    edtPrivateLawyerDate: TQRLabel;
    QRLabel75: TQRLabel;
    edtPrivateLawyerLicence: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    edtPrivate: TQRLabel;
    QRShape153: TQRShape;
    QRLabel104: TQRLabel;
    edtPrivateLastName: TQRLabel;
    QRShape154: TQRShape;
    QRLabel105: TQRLabel;
    edtPrivateFirstName: TQRLabel;
    QRShape155: TQRShape;
    QRLabel106: TQRLabel;
    edtPrivateZehut: TQRLabel;
    QRShape156: TQRShape;
    QRLabel107: TQRLabel;
    QRShape157: TQRShape;
    QRLabel108: TQRLabel;
    edtPrivateDate: TQRLabel;
    QRShape158: TQRShape;
    QRLabel109: TQRLabel;
    QRLabel26: TQRLabel;
    QRShape1: TQRShape;
    QRLabel38: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel44: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    qrTaagid: TQuickRep;
    QRBand6: TQRBand;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    edtTaagidCompanyName1: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel62: TQRLabel;
    edtTaagidCompanyName2: TQRLabel;
    QRLabel63: TQRLabel;
    edtTaagid: TQRLabel;
    QRShape4: TQRShape;
    QRLabel66: TQRLabel;
    edtTaagidLastName: TQRLabel;
    QRShape5: TQRShape;
    QRLabel68: TQRLabel;
    edtTaagidFirstName: TQRLabel;
    QRShape6: TQRShape;
    QRLabel74: TQRLabel;
    edtTaagidZehut: TQRLabel;
    QRShape7: TQRShape;
    QRLabel77: TQRLabel;
    QRShape8: TQRShape;
    QRLabel92: TQRLabel;
    edtTaagidDate: TQRLabel;
    QRShape9: TQRShape;
    QRLabel94: TQRLabel;
    QRLabel65: TQRLabel;
    edtTaagidLawyerName: TQRLabel;
    edtTaagidFullName: TQRLabel;
    QRLabel76: TQRLabel;
    edtTaagidCustomerZehut: TQRLabel;
    QRLabel95: TQRLabel;
    edtTaagidLawyerLicence: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel110: TQRLabel;
    QRShape10: TQRShape;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    edtTaagidLawyerDate: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRLabel306: TQRLabel;
    QRLabel308: TQRLabel;
    QRLabel282: TQRLabel;
    QRLabel307: TQRLabel;
  private
    { Private declarations }
    FTaagid: Boolean;
    procedure PrintEmptyProc(DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintEmptyProc2(ATaagid,DoPrint:boolean);
    procedure PrintEmpty(const FileName: String; Taagid: Boolean);
    procedure PrintExecute(Company: TCompany; StockHolder: TStockHolder; DecisionDate: TDateTime; DoPrint: Boolean);
  end;

var
  fmQRFirstStockHolder: TfmQRFirstStockHolder;

implementation

{$R *.DFM}

uses Util, PrinterSetup;

procedure TfmQRFirstStockHolder.PrintEmpty(const FileName: String; Taagid: Boolean);
begin
  FTaagid:= Taagid;
  fmPrinterSetup.Execute(FileName, PrintEmptyProc);
end;

procedure TfmQRFirstStockHolder.PrintEmptyProc2(ATaagid,DoPrint:boolean);
begin
  FTaagid:=ATaagid;
  PrintEmptyProc(DoPrint);
end;

procedure TfmQRFirstStockHolder.PrintEmptyProc(DoPrint: Boolean);
var
  I: integer;
begin
  For I := 0 To Self.ComponentCount - 1 Do
  Begin
    if ((Components[I] is TQRLabel) and (UpperCase(Copy(Components[I].Name, 0,  3)) = 'EDT')) then
      (Components[i] as TQRLabel).Caption := '';
  End;
  if FTaagid Then
  begin
    fmPrinterSetup.FormatQR(qrTaagid);
    if DoPrint then
      qrTaagid.Print
    Else
      qrTaagid.Preview;
  end
  Else
  begin
    fmPrinterSetup.FormatQR(qrPrivate);
    If DoPrint then
      qrPrivate.Print
    Else
      qrPrivate.Preview;
  end;
  Application.ProcessMessages;
end;

procedure TfmQRFirstStockHolder.PrintExecute(Company: TCompany; StockHolder: TStockHolder; DecisionDate: TDateTime; DoPrint: Boolean);
var
  FirstName, LastName: string;
begin
  SplitName(StockHolder.Name, FirstName, LastName);
  If StockHolder.Taagid then
  begin
    l11.Caption := StockHolder.Name;
    l12.Caption := StockHolder.Name;
    edtTaagidCompanyName1.Caption := StockHolder.Name;
    edtTaagidCompanyName2.Caption := StockHolder.Name;
    edtTaagid.Caption := StockHolder.Name;
    edtTaagidFirstName.Caption := '';
    edtTaagidLastName.Caption := '';
    edtTaagidZehut.Caption := StockHolder.Zeut;
    edtTaagidDate.Caption := DateToStr(Date);
    l13.Caption := Company.CompanyInfo.LawName;
    l15.Caption := Company.CompanyInfo.LawName;
    edtTaagidLawyerName.Caption := Company.CompanyInfo.LawName;
    l16.Caption := Company.CompanyInfo.LawAddress;
    edtTaagidLawyerLicence.Caption := '';
    if Company.CompanyInfo.LawZeut<> '0' then
      l17.Caption := Company.CompanyInfo.LawZeut;
    if Company.CompanyInfo.LawLicence<> '0' then
    begin
      l18.Caption := Company.CompanyInfo.LawLicence;
      edtTaagidLawyerLicence.Caption := Company.CompanyInfo.LawLicence;
    end;
    edtTaagidFullName.Caption := '';
    edtTaagidCustomerZehut.Caption := '';
    edtTaagidLawyerDate.Caption := DateToStr(Date);
    fmPrinterSetup.FormatQR(qrStockHolderTaagid);
    fmPrinterSetup.FormatQR(qrTaagid);
    if DoPrint then
    begin
      //qrStockHolderTaagid.Print;
      qrTaagid.Print;
    end
    Else
    begin
      //qrStockHolderTaagid.Preview;
      qrTaagid.Preview;
      Application.ProcessMessages;
    end;
  end
  Else
  begin
    edtPrivate.Caption := StockHolder.Name;
    l1.Caption := StockHolder.Name;
    edtPrivateLastName.Caption := LastName;
    edtPrivateFirstName.Caption := FirstName;
    l2.Caption := StockHolder.Zeut;
    l3.Caption := Company.CompanyInfo.LawName;
    edtPrivateLawyerName.Caption := Company.CompanyInfo.LawName;
    l4.Caption := StockHolder.Name;
    edtPrivateFullName.Caption := StockHolder.Name;
    l5.Caption := StockHolder.Zeut;
    edtPrivateCustomerZehut.Caption := StockHolder.Zeut;
    edtPrivateZehut.Caption := StockHolder.Zeut;
    edtPrivateDate.Caption := DateToStr(Date);
    l7.Caption := Company.CompanyInfo.LawName;
    l8.Caption := Company.CompanyInfo.LawAddress;
    if Company.CompanyInfo.LawZeut<> '0' then
      l9.Caption := Company.CompanyInfo.LawZeut;
    if Company.CompanyInfo.LawLicence<> '0' then
    l10.Caption := Company.CompanyInfo.LawLicence;
    edtPrivateLawyerLicence.Caption := Company.CompanyInfo.LawLicence;
    edtPrivateLawyerDate.Caption := DateToStr(Date);

    //fmPrinterSetup.FormatQR(qrStockHolderNonTaagid);
    fmPrinterSetup.FormatQR(qrPrivate);
    If DoPrint then
    begin
      //qrStockHolderNonTaagid.Print;
      qrPrivate.Print;
    end
    Else
    begin
      //qrStockHolderNonTaagid.Preview;
      qrPrivate.Preview;
      Application.ProcessMessages;
    end;
  end;
end; // TfmQRFirstStockHolder.PrintExecute

end.
