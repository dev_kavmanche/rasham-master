unit Register;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, HLabel, ExtCtrls, HMemo;

type
  TfmRegister = class(TForm)
    btOk: TButton;
    btCancel: TButton;
    Bevel1: TBevel;
    Panel1: TPanel;
    Notebook: TNotebook;
    HebLabel2: THebLabel;
    edCompNum: TEdit;
    HebLabel5: THebLabel;
    HebLabel6: THebLabel;
    edRegKey: TEdit;
    HebLabel4: THebLabel;
    HebMemo1: THebMemo;
    HebMemo2: THebMemo;
    procedure edCompNumKeyPress(Sender: TObject; var Key: Char);
    procedure btCancelClick(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRegister: TfmRegister;

implementation

uses Main, HPSConsts;

{$R *.DFM}

procedure TfmRegister.edCompNumKeyPress(Sender: TObject; var Key: Char);
begin
  If Key = #13 then
    btOk.Click;
  if Key >= 'A' then
    Key:= #0;
end;

procedure TfmRegister.btCancelClick(Sender: TObject);
begin
  Close;
end;

function DeleteSpaces(const InStr: String): String;
var
  I: Integer;
begin
  Result:= '';
  For I:= 1 to length(InStr) do
    if InStr[I] in ['0'..'9'] then
      Result:= Result + InStr[I];
end;

procedure TfmRegister.btOkClick(Sender: TObject);
begin
  case Notebook.PageIndex of
    0: try
         If fmMain.SetRegistered(StrToInt(DeleteSpaces(edRegKey.Text))) then
           begin
             btCancel.Visible:= False;
             btOk.Caption:= '����';
             Notebook.PageIndex:= 2;
           end
         else
           raise Exception.Create('');
       except
         btOk.Caption:= '����';
         Notebook.PageIndex:=1;
       end;
    1: begin
         Notebook.PageIndex:= 0;
         btOk.Caption:= '�����';
         edRegKey.SelectAll;
         edRegKey.SetFocus;
       end;
    2: Close;
  end;
end;

procedure TfmRegister.FormShow(Sender: TObject);
begin
  HebMemo2.Visible:= fmMain.DemoMode;
  HebMemo1.Visible:= Not HebMemo2.Visible;
  Notebook.PageIndex:= 0;
  edCompNum.Text:= FloatToStrF(Abs(fmMain.GetVolumeSerNum), ffNumber, 30, 0);
end;

procedure TfmRegister.FormCreate(Sender: TObject);
begin
  HebMemo2.Lines.Clear;
  HebMemo2.Lines.Add('  ���� ���� !');
  HebMemo2.Lines.Add('');
  HebMemo2.Lines.Add('��� ���� �� ������, �� ����� ������ ������ ���:');
  HebMemo2.Lines.Add(HPSPhone+'���� ���� �� ���� ������. ���� ����');
  HebMemo2.Lines.Add('������ ����� �� ����� �� ���� ������ ����� �����');
  HebMemo2.Lines.Add('�� ������.');
  HebMemo2.Lines.Add('���� ������ �� ���� ������ ��� �� ����� �����.');
  HebMemo2.Lines.Add('');
  HebMemo2.Lines.Add('������ ��� - �������� ���� �� ���� ������ ��');
  HebMemo2.Lines.Add('���� ���� ���� ��� �����. ��� �� ���� �"� ����');
  HebMemo2.Lines.Add('�� ���� �����, ����� ���'' ������ ��� ���� ��');
  HebMemo2.Lines.Add('���� ������ ����� ������ ���� �� ������ ����');
  HebMemo2.Lines.Add('�� ���� ������ ������.');

  HebMemo1.Lines.Clear;
  HebMemo1.Lines.Add('  ���� ���� !');
  HebMemo1.Lines.Add('');
  HebMemo1.Lines.Add('���� ����� ����� ����� ���� ����� ����� �����');
  HebMemo1.Lines.Add('��� ����� ��. �� ����� ������ ������ ���:');
  HebMemo1.Lines.Add(HPSPhone+' ���� ���� �� ���� ������.');
  HebMemo1.Lines.Add('���� ����� �� ���� ������ ��� �� ����� �����.');
  HebMemo1.Lines.Add('');
  HebMemo1.Lines.Add('������ ��� - �������� ���� �� ���� ������ ��');
  HebMemo1.Lines.Add('���� ���� ���� ��� �����. ��� �� ���� �"� ����');
  HebMemo1.Lines.Add('�� ���� �����, ����� ���'' ������ ��� ���� ��');
  HebMemo1.Lines.Add('���� ������ ����� ������ ���� �� ������ ����');
  HebMemo1.Lines.Add('�� ���� ������ ������.');

end;

end.
