unit Internet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, HLabel,
  DialUp, OnlineIP, HTTPGet, HEdit, HMemo, HGroupBx, MSXML2_TLB, HChkBox, Registry;

type
  TTempAddress = record
    Street: string;
    Number: string;
    Mikud: string;
    City: string;
    At: string;
    POB: string;
  end;

  TShareInfo = record
    Name: string;
    Value: string;
    Sum: string;
    OutAmount: string;
  end;
  PShareInfo = ^TShareInfo;

  TTempPersonInfo = record
    Name: string;
    Zeut: string;
    Address: TTempAddress;
    MinuiDate: string;
    IsDirector: boolean;
    Shares: TList;
  end;
  PTempPersonInfo = ^TTempPersonInfo;

  TTempCompany = record
    Num: string;
    Name: string;
    RegDate: string;
    Responsibility: integer;
    Address: TTempAddress;
    Directors: TList;
    StockHolders: TList;
    Shares :TList;
  end;

  TfmInternet = class(TfmTemplate)
    HTTP: THTTPGet;
    iOnline: TOnlineIP;
    DialUp: TDialUp;
    btFind: TSpeedButton;
    HebGroupBox2: THebGroupBox;
    Memo: THebMemo;
    HebGroupBox1: THebGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ProgressBar: TProgressBar;
    HebGroupBox3: THebGroupBox;
    HebLabel2: THebLabel;
    edUser: THebEdit;
    HebLabel3: THebLabel;
    edPass: THebEdit;
    HebGroupBox4: THebGroupBox;
    HebLabel1: THebLabel;
    edNum: THebEdit;
    btCancel: TSpeedButton;
    pnDraft: TPanel;
    HebLabel4: THebLabel;
    procedure iOnlineChanged(Sender: TObject);
    procedure btFindClick(Sender: TObject);
    procedure HTTPProgress(Sender: TObject; TotalSize,ReadSoFar: Integer);
    procedure DialUpEntryGet(Sender: TObject; EntryName: array of Char);
    procedure DialUpError(Sender: TObject; ErrorCode: Integer;
      ErrorMessage: String);
    procedure DialUpNotConnected(Sender: TObject; ErrorCode: Integer;
      ErrorMessage: String);
    procedure DialUpAsyncEvent(Sender: TObject; State, Error: Integer;
      MessageText: String);
    procedure HTTPError(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure edUserKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNumKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    Connected: boolean;
    Canceled : boolean;
    SelfDial: boolean;
    TrialNumber: integer;
    function GetXMLErrorMessage(XMLDoc: DOMDocument): string;
    function LoadToScreen (Company :TTempCompany): boolean;
    function FindCompany :boolean;
    Function GetCompany :boolean;
    Function LookUpCompany(Zeut: string): Integer;
    Function TxtToResponsibility(Txt: string): integer;
    function FindAgain: boolean;
    procedure FreeCompany(Comp : TTempCompany);
    procedure RetrievalError(XMLDoc: DOMDocument);
    Function ChangeEmptyCity(S: string): String;
//    procedure SaveUserDetails;
    { Private declarations }
  public
    { Public declarations }
  end;

const
  XN_ERROR       = 'ERROR';
  XN_ROOT        = 'RASHAM_TABLES';
  XN_0_COMPNODE  = 'COMP_NAMES';
  XN_0_COMPNUM   = 'COMPNUMB';
  XN_0_COMPNAME  = 'COMPNAMEHEB';
  MAIN_REG       = 'Software\HpSoft\Rasham\';
  HEB_MAIN_REG       = 'Software\HpSoft\��� ����\';
var
  fmInternet: TfmInternet;

implementation

uses select, NewCompanyInfo,DataM, util, dialog, HPSConsts ;

{$R *.DFM}

procedure TfmInternet.iOnlineChanged(Sender: TObject);
begin
  inherited;
  Connected:= iOnline.Online;
end;

procedure TfmInternet.btFindClick(Sender: TObject);
begin
  inherited;
  TrialNumber:=0;
  if (edUser.Text='') or (edPass.Text = '') or (edNum.Text ='') then
  begin
    Application.MessageBox(PChar(
            '����� ������ �����.'+#10#13+'�� ��� �� �� �����.') ,
            '������ �����', MB_OK +
             MB_ICONINFORMATION + MB_RTLREADING + MB_RIGHT);
    exit;
  end;

  Canceled := false;
  Memo.Lines.Clear;
  fmSelect.Entries.Items.Clear;
  btFind.Enabled := false;
  btCancel.Enabled := true;
  SelfDial := false;
  screen.Cursor :=crHourGlass;
  if (Not Connected) then
  begin
    SelfDial := true;
    Memo.Lines.Add('�� ����� ������� �����');
    Memo.Lines.Add('���� ������� ��������');
    Memo.Lines.Add('-----------------------------------------------------------------------');
    DialUp.GetEntries;
    Memo.Lines.Add('-----------------------------------------------------------------------');
    if fmSelect.Entries.Items.Count<1 then
    begin
     Memo.Lines.Add('�����');
     Memo.Lines.Add('�� ����� ������� �������');
     Memo.Lines.Add('-----------------------------------------------------------------------');
     Memo.Lines.Add('������ �����');
     Memo.Lines.Add('-----------------------------------------------------------------------');
     btFind.Enabled := true;
     SpeedButton1.Enabled := true;
     Exit;
    end;

    if fmSelect.Entries.Items.Count>1 then
    begin
    fmSelect.ShowModal;
    DialUp.Entry:=fmSelect.Entries.Items[fmSelect.Entries.ItemIndex];
    end else
    begin
      DialUp.Entry:=fmSelect.Entries.Items[0];
    end;
    Memo.Lines.Add(DialUp.Entry+' ����� �������');
    DialUp.Dial;
    Application.ProcessMessages;
  end else
  begin
    Memo.Lines.Add('����� �������� ������ ���');

    if FindCompany then
    begin
      Memo.Lines.Add('����� ������� ������');
      WindowState:=wsMinimized;
      //add message
    end else
    begin
      Memo.Lines.Add('����� �����');
      ProgressBar.Position := 0;
    end;
    screen.Cursor :=crDefault;
    Memo.Lines.Add('-----------------------------------------------------------------------');
    if SelfDial then DialUp.HangUp;
    btCancel.Enabled :=false;
    btFind.Enabled :=True;
    ProgressBar.Position :=0;
  end;
end;
//=====================
procedure TfmInternet.HTTPProgress(Sender: TObject; TotalSize, ReadSoFar: Integer);
begin
 if HTTP.BinaryData then
  begin
   ProgressBar.Max := TotalSize;
   ProgressBar.Position := ReadSoFar;
  end;
end;
//=====================

procedure TfmInternet.DialUpEntryGet(Sender: TObject;
  EntryName: array of Char);
begin
  inherited;
  fmSelect.Entries.Items.Add(PChar(@EntryName[0]));
  Memo.Lines.Add(PChar(@EntryName[0]));
end;
//=====================
procedure TfmInternet.DialUpError(Sender: TObject; ErrorCode: Integer;
  ErrorMessage: String);
begin
  inherited;
  Memo.Lines.Add('Error '+IntToStr(ErrorCode)+' : '+ErrorMessage);
  Memo.Lines.Add('-----------------------------------------------------------------------');
  Memo.Lines.Add('������ �����');
  Memo.Lines.Add('-----------------------------------------------------------------------');
  btFind.Enabled := true;
  SpeedButton1.Enabled := true;
  btCancel.Enabled := false;
  Canceled := true;
  DialUp.HangUp;
end;
//=====================
procedure TfmInternet.DialUpNotConnected(Sender: TObject;
  ErrorCode: Integer; ErrorMessage: String);
begin
  inherited;
  Memo.Lines.Add(ErrorMessage);
  Memo.Lines.Strings[Memo.Lines.Count-1] := Memo.Lines.Strings[Memo.Lines.Count-1]+'.';
  if ErrorMessage='Connected' then FindCompany;
  if Error<>0 then
  begin
    Memo.Lines.Add('-----------------------------------------------------------------------');
    Memo.Lines.Add('������ �����');
    Memo.Lines.Add('-----------------------------------------------------------------------');
    btFind.Enabled := true;
    SpeedButton1.Enabled := true;
    btCancel.Enabled := false;
    Canceled := true;
    DialUp.HangUp;
  end;
end;
//=====================
procedure TfmInternet.DialUpAsyncEvent(Sender: TObject; State,
  Error: Integer; MessageText: String);
begin
  inherited;
  Memo.Lines.Strings[Memo.Lines.Count-1] := Memo.Lines.Strings[Memo.Lines.Count-1]+'.';
  if MessageText='Connected' then FindCompany; //not sure about this...
  if Error<>0 then
  begin
    Memo.Lines.Add('-----------------------------------------------------------------------');
    Memo.Lines.Add('������ �����');
    Memo.Lines.Add('-----------------------------------------------------------------------');
    btFind.Enabled := true;
    SpeedButton1.Enabled := true;
    btCancel.Enabled := false;
    Canceled := true;
    DialUp.HangUp;
  end;
end;
//=====================
procedure TfmInternet.HTTPError(Sender: TObject);
begin
  inherited;
 Memo.Lines.Add('����� ������ ����� ���������');
 btFind.Enabled := true;
 SpeedButton1.Enabled := true;
 btCancel.Enabled := false;
 Canceled := true;
 if SelfDial then
     DialUp.HangUp;
end;
//=====================
procedure TfmInternet.btCancelClick(Sender: TObject);
begin
  inherited;
  Canceled := true;
  Memo.Lines.Add('����� ������ ����� �"� ������');
  btFind.Enabled := true;
  SpeedButton1.Enabled := true;
  btCancel.Enabled := false;
  if SelfDial then DialUp.HangUp;
  ProgressBar.Position := 0;
  Screen.Cursor :=crDefault;
end;
//=====================
function TfmInternet.FindCompany: boolean;
var
  XMLDoc: DOMDocument;
  XMLSite: OLEVariant;
  CurNode :IXMLDOMNode;
  sCompName, sCompNum: string;
  MessageStr: string;
  LookUpResult: integer;
begin
  Result := false;
  MessageStr:=' ��� ����� ';
  if TrialNumber>0 then
  begin
    Memo.Lines.Add('-----------------------------------------------------------------------');
    Memo.Lines.Add('�������� ���  '+IntToStr(TrialNumber));
  end;
  Memo.Lines.Add('-----------------------------------------------------------------------');
  Memo.Lines.Add('���� �� ����� ����� ����� ������� ���');
  LookUpResult:=LookUpCompany ( edNum.Text);
  if (LookUpResult<> 0) then //Company already in Rasham somewhere
  begin
    if (LookUpResult=1) then //Company in database
      MessageStr:=MessageStr+'����� �������.'
    else //Company saved as draft
      MessageStr:=MessageStr+'������.';

    Memo.Lines.Add('�����'+MessageStr);
    Memo.Lines.Add('-----------------------------------------------------------------------');
    Application.MessageBox(PChar(
            '���� ��.  ' + edNum.Text +MessageStr+#13#10) ,
            '��� ������ ������', MB_OK +
             MB_ICONINFORMATION + MB_RTLREADING + MB_RIGHT);
    exit;
  end;
  Memo.Lines.Add('-----------------------------------------------------------------------');
  Memo.Lines.Add('���� �� ����� ����� ����� ��� ������....');

  XMLDoc:= CoDOMDocument.Create;
  try
    XMLDoc.async :=false;
    XMLSite:='http://lxsrv1.bizportal.co.il/web/RashamNew/RashamHPS.php?Comp='+edNum.Text+'&Type=0';
    XMLDoc.load(XMLSite);
    if XMLDoc.parseError.errorCode <> 0 then
    begin
      if (XMLDoc.parseError.errorCode =-2146697211) and (TrialNumber<3) then
        Result:=FindAgain
      else
        RetrievalError(XMLDoc);
      exit;
    end else
    begin
      CurNode := XMLDoc.documentElement.FirstChild;
       if CurNode.nodeName = XN_ERROR then
       begin
         if Pos('Invalid company', XMLDoc.documentElement.firstChild.text)<>0 then
           Application.MessageBox(PChar(
                  '      ���� ��.  ' + edNum.Text +#13#10+'���� ����� ����� ������� �� ��� ������.') ,
                  '���� �� �����', MB_OK +
                   MB_ICONINFORMATION + MB_RTLREADING + MB_RIGHT);
         Memo.Lines.Add('-----------------------------------------------------------------------');
         Memo.Lines.Add(XMLDoc.documentElement.firstChild.text );
      end else if CurNode.nodeName =XN_0_COMPNODE then
      begin
        Memo.Lines.Add('���� �����');
        CurNode:=CurNode.firstChild;
        if CurNode.nodeName = XN_0_COMPNUM then
          sCompNum :=CurNode.text ;
        CurNode:=CurNode.nextSibling;
        if CurNode.nodeName = XN_0_COMPNAME then
          sCompName:=CurNode.text ;

        if (Application.MessageBox(PChar(
            '���� ��.  ' + sCompNum +' �����.'+#13#10 +
            '�� ����� : ' + sCompName + #13#10 +
            '��� ������?') ,
            '���� �����', MB_YESNO +
             MB_ICONINFORMATION + MB_RTLREADING + MB_RIGHT) = idYes) then
          Begin
            fmDialog.ActiveControl:= fmDialog.Button2;
            fmDialog.Caption:= '�����';
            fmDialog.memo.lines.Clear;
            fmDialog.memo.lines.add('������ ���!');
            fmDialog.memo.lines.add('����� ����� ����� ������� ��� �� ����� ������.');
            fmDialog.memo.lines.add('������ ���� ��� ������ ����� ������� ��� ����.');
            fmDialog.memo.lines.add('');
            fmDialog.memo.lines.add('���� ������ ������ ��� �� ������ "��� �����".');
            fmDialog.memo.lines.add('������ �� ������ "��� �����" ��� ����/� �� ������.');
            fmDialog.Button1.Visible:= True;
            fmDialog.Button2.Visible:= True;
            fmDialog.Button3.Visible:= False;
            fmDialog.Button1.Caption:= '��� �����';
            fmDialog.Button2.Caption:= '�����';
            fmDialog.ShowModal;
            fmDialog.Button3.Visible:= True;
            fmDialog.Button1.Caption:= '��';
            fmDialog.Button2.Caption:= '��';
            fmDialog.Caption:= '����';

            if  fmDialog.DialogResult = drYes then
              Result:= GetCompany
            else
              Canceled :=true;
        end else
          Canceled :=true;
      end;
    end;
   finally
     XMLDoc:=nil;
     Memo.Lines.Add('-----------------------------------------------------------------------');
   end;

  if Canceled Then
    begin
      Memo.Lines.Add('-----------------------------------------------------------------------');
      Memo.Lines.Add('������ ������ �"� ������');
      Memo.Lines.Add('-----------------------------------------------------------------------');
      if SelfDial then DialUp.HangUp;
      ProgressBar.Position := 0;
      screen.Cursor :=crDefault;
      btCancel.Enabled :=false;
      btFind.Enabled :=true;
      Abort;
    end;
end;
//===============
function TfmInternet.GetCompany: boolean;
var
  XMLDoc: DOMDocument;
  XMLSite: OLEVariant;
  CurNode  :IXMLDOMNode;
  NodeList,NodeList2 :IXMLDOMNodeList;
  CompData: TTempCompany ;
  TempPersonInfo: PTempPersonInfo;
  TempShareInfo: PShareInfo;
  i,j : integer;
begin
  Result:=false;
  Memo.Lines.Add('-----------------------------------------------------------------------');
  Memo.Lines.Add('���� �� ����� ������...');
  ProgressBar.Position := 0;

  XMLDoc:= CoDOMDocument.Create;
  try
    ProgressBar.Position := 1;
    XMLDoc.async :=false;
    XMLSite:='http://lxsrv1.bizportal.co.il/web/RashamNew/RashamHPS.php?User='+edUser.text+ '&Pass=' +edPass.Text+'&Comp='+edNum.Text+'&Type=2';

    XMLDoc.load(XMLSite);
    if XMLDoc.parseError.errorCode <> 0 then
    begin
      RetrievalError(XMLDoc);
      exit;
    end else
    begin
      CurNode := XMLDoc.documentElement.FirstChild;
      if CurNode.nodeName = XN_ERROR then
      begin
        if Pos('Invalid user', XMLDoc.documentElement.firstChild.text)<>0 then
          Application.MessageBox(PChar(
                  '�� ����� �� ����� ���� ������.'+#10#13+'�� ���� ���� ����.') ,
                  '���� ����� ������', MB_OK +
                   MB_ICONINFORMATION + MB_RTLREADING + MB_RIGHT);
        Memo.Lines.Add('-----------------------------------------------------------------------');
        Memo.Lines.Add(XMLDoc.documentElement.firstChild.text );
        exit;
      end;

      ProgressBar.Position := 10;
      xmlDoc.setProperty('SelectionLanguage', 'XPath');
      CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/MS_TAAGID');
      if CurNode <> nil then
       CompData.Num :=edNum.Text;//CurNode.text;

     CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/SHEM_IVRI');
     if CurNode <> nil then
       CompData.Name :=CurNode.text;
     CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/TR_RISHUM');
     if CurNode <> nil then
       CompData.RegDate  := FutureToPast(CurNode.text);
     CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/SUG_MUGBELET_ID/TEUR');
     if CurNode <> nil then
       CompData.Responsibility  := TxtToResponsibility(CurNode.text);

     ProgressBar.Position := 20;
     CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/MS_KTOVET');
     if (CurNode <> nil) and (CurNode.childNodes.length >0) then
     begin
       CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/MS_KTOVET/RECHOV');
       if CurNode <> nil then
         CompData.Address.Street :=CurNode.text;
       CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/MS_KTOVET/MISPAR_BAIT');
       if CurNode <> nil then
         CompData.Address.Number :=CurNode.text;
       CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/MS_KTOVET/MIKUD');
       if CurNode <> nil then
         CompData.Address.Mikud  :=CurNode.text;
       CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/MS_KTOVET/YSHUV');
       if CurNode <> nil then
         CompData.Address.City :=ChangeEmptyCity(CurNode.text);
       CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/MS_KTOVET/ETZEL');
       if CurNode <> nil then
         CompData.Address.At  :=CurNode.text;
       CurNode := xmlDoc.SelectSingleNode('//RASHAM_TABLES/COTERET/MS_KTOVET/TADOAR');
       if CurNode <> nil then
         CompData.Address.POB :=CurNode.text;
     end;
     ProgressBar.Position := 30;
     NodeList := xmlDoc.selectNodes('//RASHAM_TABLES/DIRECTORS/DIRECTOR');
     if NodeList.Length>0 then
       CompData.Directors:=TList.Create
     else
       CompData.Directors:=nil;
       
     For i:= 0 to NodeList.Length-1 do
     begin
      New(TempPersonInfo);
      CompData.Directors.Add(TempPersonInfo);

      CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_ZIHUI');
      if CurNode <> nil then
        PTempPersonInfo(CompData.Directors.Items[i])^.Zeut := CurNode.text;

      CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET');
      if CurNode <> nil then
      begin
        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/TADOAR');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Directors.Items[i])^.Address.POB :=CurNode.text;

        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/ETZEL');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Directors.Items[i])^.Address.At :=CurNode.text;

        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/RECHOV');
        if CurNode <> nil then
            PTempPersonInfo(CompData.Directors.Items[i])^.Address.Street :=CurNode.text;

        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/MISPAR_BAIT');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Directors.Items[i])^.Address.Number :=CurNode.text;

        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/MIKUD');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Directors.Items[i])^.Address.Mikud :=CurNode.text;
        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/YSHUV');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Directors.Items[i])^.Address.City  :=ChangeEmptyCity(CurNode.text);
      end;

      CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/SHEM_IVRI');
      if CurNode <> nil then
        PTempPersonInfo(CompData.Directors.Items[i])^.Name  :=CurNode.text;

      CurNode:=NodeList.Item[i].SelectSingleNode('TR_MINUY');
      if CurNode <> nil then
        PTempPersonInfo(CompData.Directors.Items[i])^.MinuiDate  :=FutureToPast(CurNode.text);

      PTempPersonInfo(CompData.Directors.Items[i])^.IsDirector :=True;
    end;
    ProgressBar.Position := 40;

    NodeList := xmlDoc.selectNodes('//RASHAM_TABLES/OPTION_HOLDERS/OPTION_HOLDER');
    if NodeList.Length>0 then
      CompData.Stockholders:=TList.Create
    else
      CompData.Stockholders:=nil;
          
    For i:= 0 to NodeList.Length-1 do
    begin
      New(TempPersonInfo);
      CompData.Stockholders.Add(TempPersonInfo);

      CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_ZIHUI');
      if CurNode <> nil then
        PTempPersonInfo(CompData.Stockholders.Items[i])^.Zeut := CurNode.text;

      CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET');
      if CurNode <> nil then
      begin
        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/TADOAR');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Stockholders.Items[i])^.Address.POB :=CurNode.text;

        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/ETZEL');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Stockholders.Items[i])^.Address.At :=CurNode.text;

        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/RECHOV');
        if CurNode <> nil then
            PTempPersonInfo(CompData.Stockholders.Items[i])^.Address.Street :=CurNode.text;

        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/MISPAR_BAIT');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Stockholders.Items[i])^.Address.Number :=CurNode.text;

        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/MIKUD');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Stockholders.Items[i])^.Address.Mikud :=CurNode.text;
        CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/MS_KTOVET/YSHUV');
        if CurNode <> nil then
          PTempPersonInfo(CompData.Stockholders.Items[i])^.Address.City  :=ChangeEmptyCity(CurNode.text);
      end;

      CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MAAGAR_ID/SHEM_IVRI');
      if CurNode <> nil then
        PTempPersonInfo(CompData.Stockholders.Items[i])^.Name  :=CurNode.text;

      //SH shares
      NodeList2 := NodeList.Item[i].selectNodes('MS_BAAL_MENAYA_ID');
      if NodeList2.Length>0 then
        PTempPersonInfo(CompData.StockHolders[i])^.Shares :=TList.Create
      else
        PTempPersonInfo(CompData.StockHolders[i])^.Shares := nil;
        
      For j:= 0 to NodeList2.Length-1 do
      begin
        New(TempShareInfo);
        PTempPersonInfo(CompData.StockHolders[i])^.Shares.Add(TempShareInfo);

        CurNode:=NodeList2.Item[j].SelectSingleNode('TEUR');
        if CurNode <> nil then
          PShareInfo(PTempPersonInfo(CompData.StockHolders[i])^.Shares[j])^.Name   :=CurNode.text;

        CurNode:=NodeList2.Item[j].SelectSingleNode('ERECH_NAKUV');
        if CurNode <> nil then
          PShareInfo(PTempPersonInfo(CompData.StockHolders[i])^.Shares[j])^.Value   :=AddDecimals(ClearTokens(CurNode.text,','));

        CurNode:=NodeList2.Item[j].SelectSingleNode('KAMUT_MENAYOT');
        if CurNode <> nil then
          PShareInfo(PTempPersonInfo(CompData.StockHolders[i])^.Shares[j])^.Sum   :=AddDecimals(ClearTokens(CurNode.text,','));

      end;
      PTempPersonInfo(CompData.Stockholders.Items[i])^.IsDirector :=False;
    end;
  end;

    ProgressBar.Position := 50;
    //shares
    NodeList := xmlDoc.selectNodes('//RASHAM_TABLES/SHARES/SHARE');
    if NodeList.Length>0 then
      CompData.Shares :=TList.Create
    else
      CompData.Shares :=nil;
      
    For i:= 0 to NodeList.Length-1 do
    begin
      New(TempShareInfo);
      CompData.Shares.Add(TempShareInfo);

      CurNode:=NodeList.Item[i].SelectSingleNode('TEUR_MENAYA');
      if CurNode <> nil then
        PShareInfo(CompData.Shares[i])^.Name   :=CurNode.text;

      CurNode:=NodeList.Item[i].SelectSingleNode('ERECH_NAKUV');
      if CurNode <> nil then
        PShareInfo(CompData.Shares[i])^.Value    :=AddDecimals(ClearTokens(CurNode.text,','));

      CurNode:=NodeList.Item[i].SelectSingleNode('SUM_KAMUT_MENAYOT');
      if CurNode <> nil then
        PShareInfo(CompData.Shares[i])^.Sum    :=AddDecimals(ClearTokens(CurNode.text,','));

      CurNode:=NodeList.Item[i].SelectSingleNode('SUG_MENAYA_ID/OUT_AMOUNT');
      if CurNode <> nil then
        PShareInfo(CompData.Shares[i])^.OutAmount :=AddDecimals(ClearTokens(CurNode.text,','));
    end;
     ProgressBar.Position := 60;
     Memo.Lines.Add('����� �����.');
     Result:= LoadToScreen(CompData);

   finally
     XMLDoc:=nil;
     Memo.Lines.Add('-----------------------------------------------------------------------');
     FreeCompany(CompData);
   end;

  if Canceled Then
    begin
      Memo.Lines.Add('-----------------------------------------------------------------------');
      Memo.Lines.Add('������ ������ �"� ������');
      Memo.Lines.Add('-----------------------------------------------------------------------');
      if SelfDial then DialUp.HangUp;
      ProgressBar.Position := 0;
      Abort;
    end;
end;
//===============
Function TfmInternet.GetXMLErrorMessage(XMLDoc: DOMDocument): string;
begin
  Result:='���� XML ���� ������';
  with TStringList.Create do
  try
      Clear;
      Add( '-----------------------------------------------------------------------');
      Add( 'Time: '+ FormatDateTime( 'dd/mm/yyyy hh:nn:ss', Now ));
      Add( 'Document failed to load:' );
      Add( 'Error #: ' + IntToStr( XMLDoc.parseError.errorCode ) );
      Add( 'Reason: ' + XMLDoc.parseError.reason );
      Add( 'Line #: ' + IntToStr( XMLDoc.parseError.Line ) );
      Add( 'Line Position: ' + IntToStr( XMLDoc.parseError.linepos ) );
      Add( 'Position In File: ' + IntToStr( XMLDoc.parseError.filepos ) );
      Add( 'Source Text: ' + XMLDoc.parseError.srcText );
      Add( 'Document URL: ' + XMLDoc.parseError.url );
      Add( '-----------------------------------------------------------------------');
      Result := Text;
  finally
      Free;
  end;
end;
//===============
Function TfmInternet.LookUpCompany(Zeut: string): Integer;
begin
  with Data.taName do
  begin
    Filter:='([CompNum] = '''+Zeut+''')';
    Filtered:=true;
    If RecordCount =0 then
      Result:=0
    else
      Result:=1; //exists in Database
    Filtered := false;
  end;
  if Result = 0 then
    with Data.taFiles do
    begin
      Filter:='([Company] = '''+Zeut+''') and ([Draft] = 1)';
      Filtered:=true;
      If RecordCount =0 then
        Result:=0
      else
        Result:=2; //exists as draft
      Filtered := false;
    end;
end;
//===============
Function TfmInternet.TxtToResponsibility(Txt: string): integer ;
begin      // needs to be changed if ever data becomes more detailed (currently it is not)
  if Txt = Trim('������') then
    Result :=0
  else
    Result :=2;
end;
//================
function TfmInternet.LoadToScreen (Company: TTempCompany) :boolean;
var
  i,j,k: integer;
begin
  Result:=false;
  Memo.Lines.Add('���� ����� ����...');
  ProgressBar.Position := 70;

  try
//    if fmNewCompanyInfo=nil then
//    fmNewCompanyInfo:=nil ;
    Application.CreateForm(TfmNewCompanyInfo,fmNewCompanyInfo);
    fmNewCompanyInfo.SendToBack;
    //Company Details
    fmNewCompanyInfo.PageControl.ActivePage := fmNewCompanyInfo.PageControl.Pages[0];

    fmNewCompanyInfo.e11.Text := Company.Name;
    fmNewCompanyInfo.e12.Text := Company.Num;
    fmNewCompanyInfo.e12a.Text := Company.RegDate;

    fmNewCompanyInfo.e13.Text := Company.Address.Street;
    fmNewCompanyInfo.e14.Text := Company.Address.Number;
    fmNewCompanyInfo.e15.Text := Company.Address.City;
    fmNewCompanyInfo.e16.Text := Company.Address.Mikud;
    fmNewCompanyInfo.e17.Text := Company.Address.POB;
    fmNewCompanyInfo.e18.Text := Company.Address.At;

    ProgressBar.Position := 80;

    //Shares
    fmNewCompanyInfo.PageControl.ActivePage := fmNewCompanyInfo.PageControl.Pages[1];

    if Company.Shares<>nil then
      for i:=0 to Company.Shares.Count -1 do
      begin
        if i>0 then
          fmNewCompanyInfo.Grid2.RowCount :=fmNewCompanyInfo.Grid2.RowCount+1;

        fmNewCompanyInfo.Grid2.Cells[0,i]:= PShareInfo(Company.Shares[i])^.Name;
        fmNewCompanyInfo.Grid2.Cells[1,i]:= PShareInfo(Company.Shares[i])^.Value;
        fmNewCompanyInfo.Grid2.Cells[2,i]:= PShareInfo(Company.Shares[i])^.Sum ;
        fmNewCompanyInfo.Grid2.Cells[3,i]:= PShareInfo(Company.Shares[i])^.OutAmount ;
        for k:=4 to  fmNewCompanyInfo.Grid2.ColCount -1 do
          fmNewCompanyInfo.Grid2.Cells[k,i]:='0.00';
      end;

    //StockHolders
    fmNewCompanyInfo.PageControl.ActivePage := fmNewCompanyInfo.PageControl.Pages[2];

    if Company.StockHolders<> nil then
      for i:=0 to Company.StockHolders.Count -1 do
      begin
        if i>0 then
          fmNewCompanyInfo.Panel17Click(fmNewCompanyInfo.Panel17); //new StockHolder

        fmNewCompanyInfo.e32.Text := PTempPersonInfo(Company.StockHolders[i])^.Name;
        fmNewCompanyInfo.e33.Text := PTempPersonInfo(Company.StockHolders[i])^.Zeut;
        fmNewCompanyInfo.e34.Text := PTempPersonInfo(Company.StockHolders[i])^.Address.Street  ;
        fmNewCompanyInfo.e35.Text := PTempPersonInfo(Company.StockHolders[i])^.Address.Number ;
        fmNewCompanyInfo.e36.Text := PTempPersonInfo(Company.StockHolders[i])^.Address.Mikud ;
        fmNewCompanyInfo.e37.Text := PTempPersonInfo(Company.StockHolders[i])^.Address.City  ;

        if PTempPersonInfo(Company.StockHolders[i])^.Shares<>nil then
          for j:=0 to PTempPersonInfo(Company.StockHolders[i])^.Shares.Count -1  do
          begin
            if j>0 then
              fmNewCompanyInfo.Grid3.RowCount :=fmNewCompanyInfo.Grid2.RowCount+1;

            fmNewCompanyInfo.Grid3.Cells[0,j]:= PShareInfo(PTempPersonInfo(Company.StockHolders[i])^.Shares[j])^.Name;
            fmNewCompanyInfo.Grid3.Cells[1,j]:= PShareInfo(PTempPersonInfo(Company.StockHolders[i])^.Shares[j])^.Value;
            fmNewCompanyInfo.Grid3.Cells[2,j]:= PShareInfo(PTempPersonInfo(Company.StockHolders[i])^.Shares[j])^.Sum ;
            for k:=3 to  fmNewCompanyInfo.Grid3.ColCount -1 do
              fmNewCompanyInfo.Grid3.Cells[k,j]:='0.00';
          end;
      end;
    if fmNewCompanyInfo.e31.ItemIndex >0 then
    begin
      fmNewCompanyInfo.e31.ItemIndex := 0;
      fmNewCompanyInfo.e31Change(fmNewCompanyInfo.e31);
    end;

    ProgressBar.Position := 90;

    //Directors
    fmNewCompanyInfo.PageControl.ActivePage := fmNewCompanyInfo.PageControl.Pages[3];

    if Company.Directors<>nil then
      for i:=0 to Company.Directors.Count -1 do
      begin
        if i>0 then
          fmNewCompanyInfo.Panel28Click(fmNewCompanyInfo.Panel28); //new Director

        fmNewCompanyInfo.e45.Text := PTempPersonInfo(Company.Directors[i])^.Name;
        fmNewCompanyInfo.e46.Text := PTempPersonInfo(Company.Directors[i])^.Zeut;
        fmNewCompanyInfo.e47.Text := PTempPersonInfo(Company.Directors[i])^.Address.Street  ;
        fmNewCompanyInfo.e48.Text := PTempPersonInfo(Company.Directors[i])^.Address.Number ;
        fmNewCompanyInfo.e49.Text := PTempPersonInfo(Company.Directors[i])^.Address.Mikud ;
        fmNewCompanyInfo.e410.Text := PTempPersonInfo(Company.Directors[i])^.Address.City  ;
        fmNewCompanyInfo.e43.Text := PTempPersonInfo(Company.Directors[i])^.MinuiDate;
        fmNewCompanyInfo.e44.Text := '00/00/0000';//Data not in XML
      end;
    if fmNewCompanyInfo.e41.ItemIndex >0 then
    begin
      fmNewCompanyInfo.e41.ItemIndex := 0;
      fmNewCompanyInfo.e41Change(fmNewCompanyInfo.e41);
    end;

    //Additioanl Data
    fmNewCompanyInfo.PageControl.ActivePage := fmNewCompanyInfo.PageControl.Pages[5];

    case Company.Responsibility of
      1: fmNewCompanyInfo.e73.Checked :=true;
      2: fmNewCompanyInfo.e74.Checked :=true;
      3: fmNewCompanyInfo.e75.Checked :=true;
    else
    end;
      pnDraft.Visible:=True;
      fmNewCompanyInfo.PageControl.ActivePage := fmNewCompanyInfo.PageControl.Pages[0];
      pnDraft.Visible:=False;
      fmNewCompanyInfo.b1Click(fmNewCompanyInfo.b2); //save draft

  {    ShowMessage('��� ��!'+#10#13+'������ ����� ����� �� ����������'+
                  #10#13+'�� ������ ���� ��� ������'+#10#13+'��� ����� ����� ���� �����.');}
      Memo.Lines.Add('����� ���� ����� ������');
      ProgressBar.Position := 100;
      Result:=true;
  except
    on e:Exception do
    begin
      Memo.Lines.Add( '-----------------------------------------------------------------------');
      Memo.Lines.Add('����� ���� �� �����!');
      Memo.Lines.Add( e.Message);
      Memo.Lines.Add( '-----------------------------------------------------------------------');
    end;
  end;
end;
//==============================================
function TfmInternet.FindAgain: boolean; //in case of problem, try again 3 times.
var
  i :integer;
begin
  i:=0;
  Result:=false;
  While (i<3) and not Result do
  begin
    TrialNumber:=i;
    Result:= FindCompany;
    inc(i);
  end;
end;

//==============================================not needed now, but mybe for future use...
{procedure TfmInternet.SaveUserDetails;
var
  Registry : TRegistry;
  DataBuf: array [0..255] of Char;
  i, DataSize: integer;
  TempStr: string;
begin
  Registry := TRegistry.Create;
  Registry.OpenKey( MAIN_REG+'KavManhe', True );
//    if Registry.ValueExists( 'Login' ) then
//    begin
  TempStr:=edUser.Text+'X'+edPass.Text;
  DataSize:= length (TempStr);
  for i:= 0 to DataSize-1 do
    if TempStr[i+1] = 'X' then
      DataBuf[i]:=#15
    else
      DataBuf[i]:=TempStr[i+1];
  Registry.WriteBinaryData('Login',DataBuf,DataSize);
//      Registry.ReadBinaryData( 'HpsInfo', ExpiryData, SizeOf( ExpiryData ) );

  Registry.Free;
end;
}
//==============================================
procedure TfmInternet.FreeCompany(Comp : TTempCompany);
  //---------------
  procedure FreeList(L:  Tlist);
  var
    i: integer;
  begin
    for i:=0 to L.Count -1 do
    begin
      if L.Items[i]<> nil then
      begin
        Dispose(L.Items[i]);
        L.Items[i]:=nil;
      end;
    end;
  end;
  //---------------
var
  i: integer;
begin
  if Comp.Directors<>nil then
    FreeList( Comp.Directors);
  if Comp.StockHolders <>nil then
  begin
    for i:= 0 to Comp.StockHolders.Count-1 do
     if PTempPersonInfo(Comp.StockHolders[i])^.Shares <> nil then
       FreeList(PTempPersonInfo(Comp.StockHolders[i])^.Shares);
    FreeList( Comp.StockHolders);
  end;
  if Comp.Shares <>nil then
    FreeList( Comp.Shares);
end;
procedure TfmInternet.edUserKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN Then
  begin
    TWinControl(fmInternet.FindNextControl(TWinControl(Sender),true,true,false)).SetFocus;
    if (Sender = edNum) and btFind.enabled then
      btFindClick(btFind);
  end;
end;
//====================Tsahi 08/11/05: handling retrieval errors
procedure TfmInternet.RetrievalError(XMLDoc: DOMDocument);
begin
  Memo.Lines.Add('���� ������ ������ �����');
  LogError(GetXMLErrorMessage(XMLDoc));
  Application.MessageBox(PChar('���� ������ ������ �����'+#10#13
               +'��� ��� ������ �����'+#10#13
               +' ���'' '+HPSPhone)
               ,PChar('���� ������')
               ,MB_ICONINFORMATION + MB_RTLREADING + +MB_RIGHT)                   ;
  ProgressBar.Position := 0;
end;
//===================Tsahi 09/11/05  : If no city is given, it should be an empty field
Function TfmInternet.ChangeEmptyCity(S: string): String;
begin
 if '���� �� ����'=S then
   Result:=''
 else
   Result:=S;
end;
//===================Tsahi 09/11/05  : only numerals allowed
procedure TfmInternet.edNumKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (IsNumeric(Key) or IsControlChar(Key)) then
    Key:=#0;
end;

procedure TfmInternet.FormShow(Sender: TObject);
begin
  inherited;
  edUser.SetFocus;
end;

end.

