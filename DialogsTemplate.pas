{
MODIFICATION HISTORY
DATE         BY    TASK      VERSION     DESCRIPTION
16/12/2019   VG    19075     1.7.7       3 line limit for regulations editor; code refactoring
}

unit DialogsTemplate;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  HebForm, ExtCtrls, StdCtrls, HLabel, Mask, Buttons, ComCtrls, SdGrid,
  HComboBx, HEdit, DataObjects, HGrids, DBTables;

type
  TfmTemplate = class(TForm)
    ErrorTimer: TTimer;
    HebForm1: THebForm;
    Shape26: TShape;
    lCaption: THebLabel;
    Bevel1: TBevel;
    pnDashboard: TPanel;
    B1: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton3: TSpeedButton;
    B2: TSpeedButton;
    B3: TSpeedButton;
    pnNotebook: TPanel;
    PageControl: TPageControl;
    TabSheet1: TTabSheet;
    pnButtons: TPanel;
    pnB5: TPanel;
    pnB4: TPanel;
    pnB3: TPanel;
    pnB2: TPanel;
    pnB1: TPanel;
    pnB6: TPanel;
    pnB7: TPanel;
    btPrint: TSpeedButton;
    procedure ErrorTimerTimer(Sender: TObject);
    procedure B3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure pnB1Click(Sender: TObject);
    procedure CriticalEditChange(Sender: TObject);
    procedure ControlKeyPress(Sender: TObject; var Key: Char);
    procedure ControlKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DateKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure NonCriticalChange(Sender: TObject);
    procedure B1Click(Sender: TObject);
    procedure nGridDrawCell(Sender: TObject; Col, Row: Integer; aRect: TRect; State: TGridDrawState);
    procedure PanelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PanelMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btPrintClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FReadOnly: Boolean;
    FPageIndex: Integer;
    FLoading: Boolean;
    FDataChanged: Boolean;
    FExtendedCheck: Boolean;
    FFileName: String;
    FIsDraft: Boolean;
    FCurrentFileRecNum: Integer;
    procedure SetFileName(Const Value: String);
    procedure SetPageIndex(Value: Integer);
    procedure SetDataChanged(Value: Boolean);
  protected
    // Check errors Utilites
    FieldNames:TStringList;
    ErrorAt: TWinControl;
    CurrRow: integer;
    FFromDraft:boolean;
    function GetFieldName(cmp:TComponent):string;
    function CheckNumberField(Field: String): Boolean;
    function CheckErrors(Silent, Extended, Draft: Boolean): Boolean;
    function IsNumberOnly(Sender: TObject): Boolean;
    function IsFloatOnly(Sender: TObject): Boolean;
    function IsJumpingCombo(Sender: TObject): Boolean;
    procedure SetTabsCaptions;
    procedure NothingEnum(ObjectNum, NewNum: Integer);
    procedure EvPageIndexChange; virtual;
    procedure CheckGrid(Page: Integer; Silent, Extended: Boolean; Grid: TSdGrid);
    procedure CheckEdit(Silent: Boolean; Edit: TCustomEdit);
    procedure CheckZeut(Page: Integer; Silent: Boolean; Edit: TCustomEdit; NameFull: boolean);
    procedure CheckComboZeut(Page: Integer; Silent: Boolean; Edit: THebComboBox);
    procedure CheckDate(Silent: Boolean; Edit: TCustomEdit);
    procedure CheckComboBox(Silent: Boolean; Combo: THebComboBox);
    procedure ChangeNameFull(var NameEdit,ZeutEdit: TCustomEdit);
    // Events
    function EvSaveData(Draft: Boolean): Boolean; virtual; abstract;
    function EvLoadData: Boolean; virtual; abstract;
    function ExecuteTakanon(var Takanon: String; const DefaultTakanon, Caption: String): Boolean;
    procedure EvPrintData; virtual; abstract;
    procedure EvFillDefaultData; virtual;
    procedure EvDeleteEmptyRows; virtual;
    procedure EvSetCaption; virtual;
    procedure EvFocusNext(Sender: TObject); virtual; abstract;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); virtual;
    procedure EvTimerOver; virtual;
    procedure GoToCurrRecord; virtual; abstract;

    property ReadOnly: Boolean          read FReadOnly    write FReadOnly;
  public
    { Public declarations }
    procedure SetFileIsDraft(Value: Boolean; RecNum: Integer);

    property CurrentFileRecNum: Integer read FCurrentFileRecNum;
    property FileIsDraft: Boolean       read FIsDraft     write FIsDraft;
    property PageIndex: Integer         read FPageIndex   write SetPageIndex;
    property Loading: Boolean           read FLoading     write FLoading;
    property DataChanged: Boolean       read FDataChanged write SetDataChanged;
    property FileName: String           read FFileName    write SetFileName;
  end;

var
  fmTemplate: TfmTemplate;

implementation

{$R *.DFM}

uses cSTD, Util, dialog, dialog2, Main, About, Help, BookStock, DataM, TakanonEdit;

function TfmTemplate.GetFieldName(cmp:TComponent):string;
var
  i,p:integer;
  strName1,strName2:string;
begin
  Result:='';
  if FieldNames=nil then
    exit;
  if not Assigned(FieldNames) then
    exit;
  strName1:=uppercase(trim(cmp.Name));
  for i:=0 to FieldNames.Count-1 do
  begin
    p:=pos('=',FieldNames.Strings[i]);
    if p>0 then
      strName2:=Copy(FieldNames.Strings[i],p+1,length(FieldNames.Strings[i]))
    else
      strName2:=FieldNames.Strings[i];
    strName2:=uppercase(trim(strName2));
    if strName1=strName2 then
    begin
      if p>0 then
        Result:=uppercase(trim(Copy(FieldNames.Strings[i],1,p-1)))+':';
      exit;
    end;
  end;
end;

procedure TfmTemplate.NothingEnum(ObjectNum, NewNum: Integer);
begin
  // Nothing
end;

procedure TfmTemplate.EvTimerOver;
var
  I: Integer;
  ctrl: TControl;
begin
  ErrorTimer.Enabled:= False;
  ErrorTimer.Tag:= 0;
  For I := 0 To ComponentCount - 1 Do
    If Components[I] is TControl Then
    begin
      ctrl := (Components[I] as TControl);
      If (Pos('zeut',  ctrl.Hint) <> 0) or (Pos('number', ctrl.Hint) <> 0) or
         (Pos('date',  ctrl.Hint) <> 0) or (Pos('empty',  ctrl.Hint) <> 0) or
         (Pos('field', ctrl.Hint) <> 0) or (Pos('float',  ctrl.Hint) <> 0) then
        If ctrl is TEdit Then
          (ctrl as TEdit).Color:= clWindow
        Else If ctrl is THebEdit Then
          (ctrl as THebEdit).Color:= clWindow
        Else If ctrl is TMaskEdit Then
          (ctrl as TMaskEdit).Color:= clWindow
        Else If (ctrl is THebComboBox) Then
          (ctrl as THebComboBox).Color:= clWindow;
    end;
end;

function TfmTemplate.CheckNumberField(Field: String): Boolean;
begin
  If RemoveSpaces(Field, rmBoth) = '' Then
    Result:= True
  Else
    try
      If StrToFloat(Field) = 0 Then
        Result:= True
      Else
        Result:= False;
    except
      Result:= False;
    end;
end;

procedure TfmTemplate.EvDeleteEmptyRows;
begin
  // Do Nothing
end;

procedure TfmTemplate.EvFillDefaultData;
var
  I: Integer;
begin
  For I:= 0 To ComponentCount-1 Do
    If Components[I] is TControl Then
    begin
      If (Pos('date', (Components[I] as TControl).Hint)<>0) then
        If Components[I] is TCustomEdit Then
          (Components[I] as TCustomEdit).Text:= DateToLongStr(Now);
      If (Pos('number', (Components[I] as TControl).Hint)<>0) then
        If Components[I] is TCustomEdit Then
          (Components[I] as TCustomEdit).Text:= '0';
    end;
end;

procedure TfmTemplate.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);

   procedure ProcessControls(Control: TWinControl);
   var
     I: Integer;
   begin
     If Control.ControlCount > 0 Then
     begin
       For I:= 0 To Control.ControlCount-1 Do
         If Control.Controls[I] is TWinControl Then
           ProcessControls(Control.Controls[I] as TWinControl);
     end
     Else If (Control is TCustomEdit) Then
     begin
       If Extended and (Pos('date', Control.Hint)<>0) then
         CheckDate(Silent, Control as TCustomEdit);
       If Extended And (Pos('empty', Control.Hint)<>0) then
         CheckEdit(Silent, Control as TCustomEdit);
       If Extended and (Pos('zeut', Control.Hint)<>0)  then
       //Tsahi 21.5.06: added 'namefull' to indicate the  name related to the Zeut exists
         CheckZeut(Page, Silent, Control as TCustomEdit, (Pos('namefull', Control.Hint)<>0) );
     end;

     If (Control is THebComboBox) Then
     begin
       If Extended and (Pos('zeut', Control.Hint)<> 0) then
         CheckComboZeut(Page, Silent, Control as THebComboBox);
     end;
   end;

begin
  ProcessControls(PageControl.Pages[Page]);
end;

procedure TfmTemplate.EvSetCaption;
begin
  lCaption.Caption:= PageControl.ActivePage.Hint;
  If lCaption.Caption = '' Then
    lCaption.Caption:= PageControl.ActivePage.Caption;
end;

function TfmTemplate.IsNumberOnly(Sender: TObject): Boolean;
begin
  Result:= Pos('number',(Sender as TControl).Hint) <> 0;
end;

function TfmTemplate.IsFloatOnly(Sender: TObject): Boolean;
begin
  Result:= Pos('float',(Sender as TControl).Hint) <> 0;
end;

function TfmTemplate.IsJumpingCombo(Sender: TObject): Boolean;
begin
  Result:= Pos('combo',(Sender as TControl).Hint) <> 0;
end;

procedure TfmTemplate.SetPageIndex(Value: Integer);
var
  OldPageIndex:integer;
  ok:boolean;
begin
  if Value >= PageControl.PageCount then
    exit;
  OldPageIndex:=FPageIndex;
  FPageIndex:= Value;
  ok:=true;
  try
    PageControl.ActivePage:= PageControl.Pages[FPageIndex];
  except
    ok:=False;
  end;
  if ok then
    EvPageIndexChange
  else
    FPageIndex:=OldPageIndex;
end;

procedure TfmTemplate.ErrorTimerTimer(Sender: TObject);
begin
  EvTimerOver;
end;

procedure TfmTemplate.CheckGrid(Page: Integer; Silent, Extended: Boolean; Grid: TSdGrid);
var
  Col, Row: Integer;
  RealDate: Boolean;
begin
  For Row:= Grid.FixedRows To Grid.RowCount - 1 do
    For Col:= Grid.FixedCols To Grid.ColCount -1 do
    begin
      try
        if Pos(IntToStr(Col),Grid.TimeCells) <> 0 then
          StrToTime(Grid.Cells[Col, Row])
        Else
          if ((Pos(IntToStr(Col), Grid.SummCells) <> 0) or (Pos(IntToStr(Col), Grid.SummCCells) <> 0))
               And (RemoveSpaces(Grid.Cells[Col, Row], rmBoth) <> '') then
            StrToFloat(Grid.Cells[Col, Row]);
      except
        If Not Silent Then
        begin
          Grid.Row:= Row;
          Grid.Col:= Col;
        end;
        ErrorAt:= Grid;
        raise Exception.Create('���� �� ����');
      end;

      if Pos(IntToStr(Col), Grid.DateCells) <> 0 then
        If IsNotDate(Grid.Cells[Col, Row], RealDate) Then
        begin
          If Not Silent Then
          begin
            Grid.Row:= Row;
            Grid.Col:= Col;
          end;
          ErrorAt := Grid;
          If RealDate Then
            raise Exception.Create('������ ���� ����� ����� 1.1.1900 - 31.12.2999')
          else
            raise Exception.Create('����� �� ����');
        end;

      if Extended and
         (Pos(IntToStr(Col), Grid.ResultCell) <> 0) and
         (Trim(Grid.Cells[Col,Row]) <> '') and
         (Trim(Grid.Cells[Col,Row]) <> '0') and
         (not CheckID(Grid.Cells[Col, Row])) Then
      begin
        If Not Silent Then
        begin
          Grid.Row:= Row;
          Grid.Col:= Col;
        end;
        ErrorAt:= Grid;
        If (not Silent) and (not ErrorTimer.Enabled) Then
        begin
          If PageIndex<> Page Then
            PageIndex:= Page;
          If ErrorTimer.Tag= 0 Then
            try
              ErrorAt.SetFocus;
            except
            end;
          If Application.MessageBox('��� ��!'+#13+#10+'���� ����� ����/���� ���� ����.'+#13+#10+'��� ��� ��� �����?',
                   PChar(Caption), MB_YESNO + MB_IconHand + MB_RTLREADING + MB_RIGHT) <> IDYES then
            raise Exception.Create('zeut');
        end;
      end;
    end;
end;

procedure TfmTemplate.CheckDate(Silent: Boolean; Edit: TCustomEdit);
var
  RealDate: Boolean;
begin
  //==>Tsahi 10.7.06: fixes the problem with saving from Draft
  if (Edit.Name = 'e12a') and ((Edit.Visible = False) or (Edit.Parent.Visible = False)) then
    exit;
  //<==

  If IsNotDate(Edit.Text, RealDate) Then
  begin
    If Not Silent Then
      If Edit is THebEdit Then
        With Edit As THebEdit Do
          If Color = clWindow Then
            Color:= clBtnFace
          Else
            Color:= clWindow
      Else If Edit is TEdit Then
        With Edit As TEdit do
          If Color = clWindow Then
            Color := clBtnFace
          Else
            Color:= clWindow
      Else If Edit is TMaskEdit Then
        With Edit as TMaskEdit do
          If Color = clWindow Then
            Color := clBtnFace
          Else
            Color:= clWindow;

    ErrorAt := Edit;
    If RealDate Then
      raise Exception.Create('������ ���� ����� ����� 1.1.1900 - 31.12.2999')
    else
      raise Exception.Create('����� �� ����');
  end;
end;

procedure TfmTemplate.CheckComboZeut(Page: Integer; Silent: Boolean; Edit: THebComboBox);
begin
  If (Trim(Edit.Text) = '') or (Trim(Edit.Text) = '0') Then
    Exit;

  If not CheckID(Edit.Text) Then
  begin
    If Not Silent Then
      With Edit Do
        If Color = clWindow Then
          Color:= clBtnFace
        Else
          Color:= clWindow;

    ErrorAt:= Edit;
    If (not Silent) and (not ErrorTimer.Enabled) Then
    begin
      If PageIndex<> Page Then
        PageIndex:= Page;
      If ErrorTimer.Tag= 0 Then
        try
          ErrorAt.SetFocus;
        except
        end;
      If Application.MessageBox('��� ��!'+#13+#10+'���� ����� ����/���� ���� ����.'+#13+#10+'��� ��� ��� �����?',
               PChar(Caption), MB_YESNO + MB_IconHand + MB_RTLREADING + MB_RIGHT) <> IDYES then
        raise Exception.Create('zeut');
    end;
  end;
end;

procedure TfmTemplate.CheckZeut(Page: Integer; Silent: Boolean; Edit: TCustomEdit;  NameFull: boolean);
begin
  if NameFull and ((Trim(Edit.Text) = '') or (Trim(Edit.Text) = '0')) Then
  begin
    raise Exception.Create('���� ���� ���� ���� ����� 0.');
    Exit;
  end;

  If not CheckID(Edit.Text) Then
  begin
    If Not Silent Then
      If Edit is THebEdit Then
        With Edit As THebEdit Do
          If Color = clWindow Then
            Color:= clBtnFace
          Else
            Color:= clWindow
      Else If Edit is TEdit Then
        With Edit As TEdit do
          If Color = clWindow Then
            Color := clBtnFace
          Else
            Color:= clWindow
      Else If Edit is TMaskEdit Then
        With Edit as TMaskEdit do
          If Color = clWindow Then
            Color := clBtnFace
          Else
            Color:= clWindow;

    ErrorAt:= Edit;
    If (not Silent) and (not ErrorTimer.Enabled) Then
    begin
      If PageIndex<> Page Then
        PageIndex:= Page;
      If ErrorTimer.Tag= 0 Then
        try
          ErrorAt.SetFocus;
        except
        end;
      If Application.MessageBox('��� ��!'+#13+#10+'���� ����� ����/���� ���� ����.'+#13+#10+'��� ��� ��� �����?',
               PChar(Caption), MB_YESNO + MB_IconHand + MB_RTLREADING + MB_RIGHT) <> IDYES then
        raise Exception.Create('zeut');
    end;
  end;
end;

procedure TfmTemplate.CheckEdit(Silent: Boolean; Edit: TCustomEdit);
begin
  //==>Tsahi 10.7.06: fixes the problem with saving from Draft
  if (Edit.Name ='e413') and ((Edit.Visible = False)or (Edit.Parent.Visible = False)) then
    exit;
  //<==

  If RemoveSpaces(Edit.Text, rmBoth) = '' Then
  begin
    If Not Silent Then
      If Edit is THebEdit Then
        With Edit As THebEdit Do
          If Color = clWindow Then
            Color:= clBtnFace
          Else
            Color:= clWindow
      Else If Edit is TEdit Then
        With Edit As TEdit do
          If Color = clWindow Then
            Color := clBtnFace
          Else
            Color:= clWindow
      Else If Edit is TMaskEdit Then
        With Edit as TMaskEdit do
          If Color = clWindow Then
            Color := clBtnFace
          Else
            Color:= clWindow;

    ErrorAt:= Edit;
    raise Exception.Create(GetFieldName(Edit)+' '+'���� ���� �� ����');
  end;
end;

procedure TfmTemplate.CheckComboBox(Silent: Boolean; Combo: THebComboBox);
begin
  If Combo.Items.IndexOf(Combo.Text)= -1 Then
  begin
    If Not Silent Then
      If Combo.Color = clWindow Then
        Color:= clBtnFace
      Else
        Combo.Color:= clWindow;
    ErrorAt:= Combo;
    raise Exception.Create(GetFieldName(Combo) + ' ' + '���� ���� �� ����');
  end;
end;

function TfmTemplate.CheckErrors(Silent, Extended, Draft: Boolean): Boolean;
var
  i: Integer;
  ErrorStr: String;
begin
  Result:= False;
  for i := 0 to PageControl.PageCount - 1 do
  begin
    try
      EvCheckPage(i, Silent, Extended, Draft);
    except
      on E: Exception do
      begin
        ErrorStr := E.Message;
        Result := True;
        if not Silent then
        try
          if CurrRow <> -1 then
          begin
            try
              GoToCurrRecord;
              // Moshe -  stupid code - make comment
              // EvCheckPage(i, Silent, Extended, Draft);
            except
              // Moshe -  stupid code - make comment
              //on E: Exception do
              //ErrorStr := E.Message;
            end;
          end;
          if PageIndex <> i then
            PageIndex := i;
        except
        end;
        try
          if (ErrorAt <> nil) then
            ErrorAt.SetFocus;
        except
        end;
        Break;
      end;
    end;
  end; // for

  If Result And (Not Silent) and (Not ErrorTimer.Enabled) and (ErrorStr <> 'zeut') Then
    If Extended Then
      If Self is TfmBookStocks Then
        Application.MessageBox(PChar('�� ���� �����:'+#13#10 + ErrorStr + #13#10'�� ����.'), PChar(Caption), MB_OK + MB_IconHand + MB_RTLREADING + MB_RIGHT)
      else
        Application.MessageBox(PChar('�� ���� �����:'+#13#10 + ErrorStr + #13#10'�� ����.'), PChar(Caption), MB_OK + MB_IconHand + MB_RTLREADING + MB_RIGHT)
    else
      Application.MessageBox(PChar(ErrorStr), PChar(Caption), MB_OK + MB_IconHand + MB_RTLREADING + MB_RIGHT);

  ErrorTimer.Enabled := Result And (Not Silent);
  If ErrorTimer.Enabled then
    FExtendedCheck:= Extended;

  If not Result Then
    EvTimerOver;
end;

procedure TfmTemplate.SetDataChanged(Value: Boolean);
begin
  If ReadOnly Then
    Exit;
  FDataChanged:= Value;
end;

procedure TfmTemplate.B3Click(Sender: TObject);
begin
  if DataChanged then
  begin
    fmDialog.memo.lines.Clear;
    fmDialog.memo.lines.add('��� ��');
    fmDialog.memo.lines.add('����� ��� ����� ���� ������ ������.');
    fmDialog.memo.lines.add('��� ����� �� ������� ?');
    fmDialog.ShowModal;
    if fmDialog.DialogResult = drYES then
    begin
      if CheckErrors(False, True, False) then
        Exit
      else
        B1.Click;
    end;
    if fmDialog.DialogResult = drCancel then
      Exit;
  end;
  Loading := True;
  FFromDraft := True;
  if EvLoadData then
  begin
    PageIndex := 0;
    DataChanged := False;
  end;
  Loading := False;
end;

procedure TfmTemplate.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // Moshe 11/11/2018 - if can't save then don't care data changed
  if (not B1.Enabled) and (not B2.Enabled) then
    DataChanged := False;

  if DataChanged then
  begin
    fmDialog.memo.lines.Clear;
    fmDialog.memo.lines.add('��� ��');
    fmDialog.memo.lines.add('����� ��� ����� ���� ������ ������.');
    fmDialog.memo.lines.add('��� ����� �� ������� ?');
    fmDialog.ShowModal;
    if fmDialog.DialogResult = drYES then
    begin
      EvTimerOver;
      B1.Click;
      if ErrorTimer.Enabled then
      begin
        Action:= caNone;
        Exit;
      end;
    end;
    if fmDialog.DialogResult = drCANCEL then
    begin
      Action:= caNone;
      Exit;
    end;
  end;
  Action := caFree;
end;

procedure TfmTemplate.SpeedButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmTemplate.FormCreate(Sender: TObject);
begin
  FFromDraft := False;
  FieldNames := TStringList.Create;
  if Tag <> -1 then
    SetBounds(2, 2, Width, Height);
  PageIndex := 0;
  Loading := True;
  SetTabsCaptions;
  EvFillDefaultData;
  Loading := False;
end;

procedure TfmTemplate.nGridDrawCell(Sender: TObject; Col, Row: Integer;
  aRect: TRect; State: TGridDrawState);
begin
  (Sender as ThebStringGrid).Canvas.DrawFocusRect(Rect(0,0,0,0));

  if (gdSelected in State) and ((Sender as ThebStringGrid).Focused) then
  begin
   (Sender as ThebStringGrid).Canvas.Brush.Color := clBlack;
   (Sender as ThebStringGrid).Canvas.Font.Color := $00E1FFFF;
  end
  else
  begin
   (Sender as ThebStringGrid).Canvas.Brush.Color := $00E1FFFF;
   (Sender as ThebStringGrid).Canvas.Font.Color := clBlack;
  end;
  (Sender as ThebStringGrid).Canvas.FillRect(aRect);
  (Sender as ThebStringGrid).Canvas.TextOut(aRect.Right-((Sender as ThebStringGrid).Canvas.TextWidth((Sender as ThebStringGrid).Cells[Col,Row])+2),aRect.Top+0,(Sender as ThebStringGrid).Cells[Col,Row]);
end;

procedure TfmTemplate.SetTabsCaptions;
var
  I: Integer;
  Count: Integer;
begin
  // Coping Tabs Captions to Buttons
  Count:= 0;
  With pnButtons Do
    For I:= 0 To ControlCount-1 Do
      If (Controls[I] is TPanel) Then
        try
          if not PageControl.Pages[(Controls[I] as TPanel).Tag-1].Enabled then
            Abort;
          (Controls[I] as TPanel).Caption:= PageControl.Pages[(Controls[I] as TPanel).Tag-1].Caption;
          (Controls[I] as TPanel).Visible:= True;
          Inc(Count);
        Except
          (Controls[I] as TPanel).Caption:= '';
          (Controls[I] as TPanel).Visible:= False;
        End;

  // This one will center all of the buttons.
     // P.S - You really don't want to know how does it works!
     // You really tyhink that you are so clever ?!
     // You just made maintenence become so hard. Useless coding.
  With pnButtons Do
    For I:= 0 To ControlCount-1 Do
      If (Controls[I] is TPanel) And (Controls[I] as TPanel).Visible Then
        (Controls[I] as TPanel).Left:= (Width - Count * (Controls[I] as TPanel).Width) div 2 +
                       (Count - (Controls[I] as TPanel).Tag) * (Controls[I] as TPanel).Width;
end;

procedure TfmTemplate.pnB1Click(Sender: TObject);
begin
  if (Sender as TPanel).BevelInner = bvRaised then
    PageIndex:= (Sender as TPanel).Tag-1;
end;

procedure TfmTemplate.EvPageIndexChange;
var
  I: Integer;
begin
  With pnButtons Do
    If Visible Then
    begin
      For I := 0 To ControlCount - 1 Do
        If Controls[I] is TPanel Then
          If (Controls[I] as TPanel).Tag-1 = PageIndex Then
          begin
            (Controls[I] as TPanel).BevelInner := bvLowered;
            (Controls[I] as TPanel).BevelOuter := bvLowered;
            (Controls[I] as TPanel).Color := $00ACAC00;
          end
          Else
          begin
           (Controls[I] as TPanel).BevelInner := bvRaised;
           (Controls[I] as TPanel).BevelOuter := bvRaised;
           (Controls[I] as TPanel).Color:= clTeal;
          end;
    End;
  EvSetCaption;
end;

procedure TfmTemplate.CriticalEditChange(Sender: TObject);
begin
  if ReadOnly then
    Exit;

  if (Loading or (not Self.Visible)) then
    Exit;

  EvTimerOver;
  DataChanged := True;
end;

procedure TfmTemplate.ControlKeyPress(Sender: TObject; var Key: Char);
var
  Tmp       : String;
  I         : Integer;
begin
  If ReadOnly Then
  begin
    Key:= #0;
    Exit;
  end;

  If Key = #13 Then
    EvFocusNext(Sender);

  If IsNumberOnly(Sender) And (Not (Key in ['0','1','2','3','4','5','6','7','8','9',#8])) then
    Key:= #0;

  If IsFloatOnly(Sender) And (Not (Key in ['0','1','2','3','4','5','6','7','8','9',#8,'.'])) then
    Key:= #0;

  If (Sender is THebComboBox) And IsJumpingCombo(Sender) Then
  begin
    if (Sender as THebComboBox).SelLength>0 then
    begin
      tmp := (Sender as THebComboBox).Text;
      Delete(tmp,(Sender as THebComboBox).SelStart+1,(Sender as THebComboBox).SelLength);
      tmp := tmp + key;
    end
    else
      tmp:=(Sender as THebComboBox).Text+key;

    i:=0;
    While (Pos(tmp,(Sender as THebComboBox).Items.Strings[i])<>1) and (i<(Sender as THebComboBox).Items.Count) do
      inc(i);
    if i < (Sender as THebComboBox).Items.Count then
    begin
      (Sender as THebComboBox).ItemIndex := I;
      (Sender as THebComboBox).SelStart := Length(tmp);
      (Sender as THebComboBox).SelLength := Length((Sender as THebComboBox).Text)-Length(tmp);
      (Sender as THebComboBox).OnChange(Sender);
      key := #0;
      DATAChanged := True;
    end;
  end;
end;
//==> Tsahi. 2.6.05 Formerly done on KeyPress, it didn't capture some key events (Delete, for example)
procedure TfmTemplate.ControlKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  If ReadOnly Then
    Key:= 0;
end;
//<==

procedure TfmTemplate.DateKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  I: Integer;
  S: String;
begin
  S := (Sender as TMaskEdit).Text;
  for I := 1 to 10 do
    if S[I] = ' '  then
      S[I] := '0';
  (Sender as TMaskEdit).Text := S;
end;

procedure TfmTemplate.NonCriticalChange(Sender: TObject);
begin
  If Loading Then
    Exit;
  DATAchanged := True;
end;

procedure TfmTemplate.B1Click(Sender: TObject);
begin
  try
    CurrRow := -1;
    EvDeleteEmptyRows;
    If CheckErrors(False, Sender = B1, Sender = B2) Then
      Exit;
    if EvSaveData(Sender = B2) Then
      DataChanged := False;
  finally
    B1.Down := False;
    B2.Down := False;
  end;
end;

procedure TfmTemplate.SetFileName(const Value: String);
var
  TempStr: String;
begin
  TempStr:= Caption;
  If Pos(' - ',TempStr) <> 0 Then
    Delete(TempStr,Pos(' - ',TempStr),Length(TempStr));
  If Value <> '' Then
    Caption := TempStr + ' - ' + Value;
  FFileName := Value;
end;

procedure TfmTemplate.PanelMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (Sender as TPanel).BevelOuter := bvLowered;
end;

procedure TfmTemplate.PanelMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 (Sender as TPanel).BevelOuter := bvRaised;
end;

procedure TfmTemplate.GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (not((key=VK_UP)
      or (key=VK_DOWN)
      or (key=VK_LEFT)
      or (key=VK_RIGHT)
      or (key=VK_RETURN)
      or (key=VK_ESCAPE))) then
    DataChanged := True;
end;

procedure TfmTemplate.btPrintClick(Sender: TObject);
begin
  CurrRow := -1;
  EvDeleteEmptyRows;
  if CheckErrors(False, False, False) then
    Exit;
  EvPrintData;
end;

procedure TfmTemplate.SpeedButton3Click(Sender: TObject);
begin
  If SpeedButton3.Visible then
    ShowHelp(HelpContext);
end;

procedure TfmTemplate.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If Key = VK_F1 then
    SpeedButton3.Click;
end;

procedure TfmTemplate.SetFileIsDraft(Value: Boolean; RecNum: Integer);
begin
  FCurrentFileRecNum:= RecNum;
  FIsDraft:= Value;
end;

//================================Tsahi: 21/05/06 :This is the ChangeNameFull procedure itself
procedure TfmTemplate.ChangeNameFull(var NameEdit,ZeutEdit: TCustomEdit);
begin
  if (NameEdit.Text <>'') then
  begin
    if (pos('namefull',ZeutEdit.hint)= 0) then
      ZeutEdit.hint := ZeutEdit.Hint+';namefull';
  end
  else if (pos('namefull',ZeutEdit.hint)<>0) then
    ZeutEdit.hint := Copy(ZeutEdit.Hint,1,length(';namefull'));
end;

function TfmTemplate.ExecuteTakanon(var Takanon: String; const DefaultTakanon, Caption: String): Boolean;
begin
  if (Takanon = '') then
    Takanon := DefaultTakanon;
  Application.CreateForm(TfmTakanonEdit, fmTakanonEdit);
  //VG19075
  fmTakanonEdit.Initialize;
  fmTakanonEdit.MaxLines := C_DEFAULT_MAX_LINES; //defined in TakanonEdit
  Result := fmTakanonEdit.Execute(Takanon, '����� ����� - ����� ' + Caption);
  fmTakanonEdit.Free;
  fmTakanonEdit := nil;
end;

procedure TfmTemplate.FormDestroy(Sender: TObject);
begin
  FieldNames.Free;
end;

end.
