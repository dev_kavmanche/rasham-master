unit ExpiryExtend;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, HLabel, ExtCtrls, HMemo, Registry;

type
  TfrmExpiryExtend = class(TForm)
    HebMemo1: THebMemo;
    Bevel1: TBevel;
    HebLabel2: THebLabel;
    edCompNum: TEdit;
    edRegKey: TEdit;
    HebLabel4: THebLabel;
    btOk: TButton;
    btCancel: TButton;
    procedure btCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function CheckExtend( Key : Integer ) : Boolean;
    function NumStr( SrcStr : String ) : String;
  public
    { Public declarations }
  end;

var
  frmExpiryExtend: TfrmExpiryExtend;

implementation

uses Main, Util, HPSConsts;

{$R *.DFM}

//==============================================================================
procedure TfrmExpiryExtend.btCancelClick(Sender: TObject);
begin
    Close;
end;

//==============================================================================
procedure TfrmExpiryExtend.FormShow(Sender: TObject);
begin
    edCompNum.Text:= FloatToStrF(Abs(fmMain.GetVolumeSerNum), ffNumber, 30, 0);
end;

//==============================================================================
procedure TfrmExpiryExtend.btOkClick(Sender: TObject);
var
    ExtendCode : Integer;
begin
    ExtendCode := StrToIntDef( NumStr( edRegKey.Text ), 0 );
    fmMain.Expired := CheckExtend( ExtendCode );
    Close;
end;

//==============================================================================
function TfrmExpiryExtend.CheckExtend( Key : Integer ) : Boolean;
begin
  Result:= Abs(Abs(fmMain.GetVolumeSerNum) * CaptionToInt('��� ����') * Trunc(fmMain.GetVersion)) = Key;
end;

//==============================================================================
function TfrmExpiryExtend.NumStr( SrcStr : String ) : String;
var
    ResStr : String;
    i      : Integer;
begin
    ResStr := EmptyStr;
    for i := 1 to Length( SrcStr ) do
    begin
        if SrcStr[i] in ['0'..'9'] then
            ResStr := ResStr + SrcStr[i];
    end;
    Result := ResStr;
end;

procedure TfrmExpiryExtend.FormCreate(Sender: TObject);
begin
   HebMemo1.Lines.Clear;
   HebMemo1.Lines.Add('���� ���� ! ');
   HebMemo1.Lines.Add('');
   HebMemo1.Lines.Add('�� ���� ������ ������.');
   HebMemo1.Lines.Add('���� ������ �� ������ �� ������ ������ ');
   HebMemo1.Lines.Add('������');
   HebMemo1.Lines.Add('���''  '+HPSPhone);
end;

end.
