unit Util;
////////////////////////////////////////////////////
// History:
//   VG19075 12.12.2019 ver 1.7.7
//     code refactoring
////////////////////////////////////////////////////

interface
//VG19075
uses
  Windows, classes, forms, extctrls, Hmemo, HGrids, BDE, DBTables, db;

type
  TSplitCompanyName = record
    HebrewName,
    EnglishName: string;
  end;

  TSortType = (stString, stNumber, stDate, stObject);

  TUserOptions=class
  private
    FFileName: String;
    FData, FDefaultValues: TStringList;
    procedure Load;
    procedure Save;
    function GetStringParam(Index: Integer): String;
    function GetIntParam(Index: Integer): Integer;
    function GetBoolParam(Index: Integer): Boolean;
    procedure SetStringParam(Index: Integer; const Value: String);
    procedure SetIntParam(Index, Value: Integer);
    procedure SetBoolParam(Index: Integer; Value: Boolean);

  public
    constructor Create(const FileName: String);
    destructor Destroy; override;

    // If adding a property, you should also set a default value in Create constructor

    property RoeHeshbon: String index 0 read GetStringParam write SetStringParam;
    property RoeAddress: String index 1 read GetStringParam write SetStringParam;

    property PrintFullCaptions: Integer index 2 read GetIntParam write SetIntParam;
    property PrintDatesInDoh: Boolean index 3 read GetBoolParam write SetBoolParam;
    property IDCheck: Boolean index 4 read GetBoolParam write SetBoolParam;
    property LastBackUp: Integer index 5 read GetIntParam write SetIntParam;
    property BackUpEvery: Integer index 6 read GetIntParam write SetIntParam;
    property CheckLongNamesOnPrint: Boolean index 7 read GetBoolParam write SetBoolParam;
    //==>tsahi 11.9.08: added to comply with changes made by Rasham Ha-Havarot
    property RoeIdNum: String index 8 read GetStringParam write SetStringParam;
    property RoeLicense: String index 9 read GetStringParam write SetStringParam;
    //<== tsahi 11.9.08

  end;

  ChangeRec = packed record
    szName: DBINAME;
    iType: word;
    iSubType: word;
    iLength: word;
    iPrecision: byte;
  end;

  function Max(A,B: Integer): Integer;
  function CheckID(const ID: String): Boolean;
  procedure CreateHintWindow(ClientForm : tForm; var MemoNote : tHebMemo; var MemoShadow : tPanel; x,y,width,height: integer; TextLines : tStrings);
  function FormatNumber(StrNum : string; bPoint,aPoint : integer; Positive : boolean) : string;
  function IsNotDate(const inpStr : string; var RealDate: boolean):boolean;
  procedure SortGrid(Grid: ThebStringGrid; const SortStyle: String);
  //procedure SortGrid(Grid : THebStringGrid; MainColumn, SecColumn : integer; MainSortType, SecSortType: TSortType);
  function DateToLongStr(Date : TdateTime) : string;
  function MakeHebStr(const InStr: String; Numeric: Boolean): String; // This function will make the same visual view as at Hebrew components
  function DoubleIndexOf(Grid: THebStringGrid; Col1, Col2: String; StartRow: Integer): Integer;
  function FilterStr(Const S: String): String; // This function will solve Filter problems
  procedure WriteLog(const S: String; const F: String);
  procedure WriteChangeLog(const S: String);
  procedure WriteAnyLog(const S: String; const F: String; isChangeLog: boolean );
  function CheckBdeBadSettings : Boolean;
  function GetTempFileName(const Ext: string): String;
  function MyStrToFloat(S: String): Double;
  function MyExpandUNCFileName(const S: String): String;
  function DebugMode: Boolean;
  function CaptionToInt(Const Caption: String): Integer;
  { Added on 07/03/2005 by ES - new log --> }
  function GetComputerNetName: string;
  function GetUserFromWindows: string;
  { <-- }
  //==>Added Tsahi 19.7.05 : formatting data recievec from Kav-Manhe
  function ClearTokens(S, Token: string):String;
  Function AddDecimals(S: string): string;
  Function FutureToPast(DateStr: string):String;
  //<==
  function StrToDateDef(Str: string; Def:TDateTime): TDateTime;
  function InIntArray(Item: integer; Arr :Array of integer):boolean;
  function FormatForFilter(S: string): string; //makes all ' into '' in string for filter strings
  procedure LogError(Error: String);
  function RowEmpty(Row: TStrings): boolean;
  function IsNumeric(Key: char): boolean;
  function IsControlChar(Key: char): boolean;
{ Added on 06/05/2005 by ES - transactionlog ;) --> }
  //==>Added from Borland. Tsahi 14.6.06
  procedure AddField(Table: TTable; NewField: ChangeRec);
  procedure ChangeField(Table: TTable; Field: TField; Rec: ChangeRec);
  //<==
  //==>Added from SuperData. Tsahi 14.6.06
  procedure BackUpTable(const TableName: String);
  //<==
  procedure RebuildDB(AliasName, FileName: string);

  function SplitCompanyName(companyName: string): TSplitCompanyName;

  procedure SplitBySep(const S, Sep: string; var Left, Right: string);
  procedure SplitName(const Name: string; var FirstName, LastName: string);

  function ReplaceStr(const S, Search, Replace: string): string;
  function StringToFloat(const str: string): double;

  function Iif(Condition: boolean; aTrue, aFalse: Variant): Variant;

  function Pad0(const Str: string; Size: integer): string;

var
  MainPath,
  UserComp  : String;
{ <-- }

implementation

//VG19075
uses
  graphics, SysUtils, Main, FileCtrl, utils2, uRebuild, cSTD, Bidi;
//dialogs;

function DebugMode: Boolean;
begin
  Result:= DebugHook > 0; // Non Documented, See System.pas for details
end;

function MyExpandUNCFileName(const S: String): String;
begin
  If FileExists(ExtractFilePath(ParamStr(0))+'no.unc') then
    Result:= S
  else
    Result:= ExpandUNCFileName(S);
end;

function MyStrToFloat(S: String): Double;
var
  I: Integer;
begin
  try
    for I:=Length(S) downto 1 do
      if S[I] in [ThousandSeparator] then
        Delete(S,I,1);
    Result:= StrToFloat(S);
  except
    Result:= 0;
  end;
end;

//---------------------------------------------------------------------
function FilterStr(Const S: String): String;
var
  I: Integer;
begin
  Result:='';
  For I:= 1 to length(S) do
  begin
    Result:= Result + S[I];
    If S[I]='''' Then
      Result:= Result + S[I];
  end;
end;
//---------------------------------------------------------------------
(* SortStyle format:
   Col Style Direction;
   Col - number of column
   Style - Type of data in a column, see TSortType
   Direction - UP - from 'a' to 'z'; DOWN - from 'z' to 'a'
*)

type
  TSortOption = Class
    Col: Integer;
    UpDirection: Boolean;
    SortType: TSortType;
  end;

procedure SortGrid(Grid: ThebStringGrid; const SortStyle: String);
var
  SortOptions: TList;

  function ParseOptions(InStr: String): TList;

    function CutNextOption: String;
    begin
      Result:= Copy(InStr, 1, Pos(';', InStr)-1);
      if Result='' then
        Result:= InStr;

      Delete(InStr, 1, Length(Result)+1);
    end;

    function GetNum(S: String): Integer;
    var
      I: Integer;
    begin
      Result:= 0;
      for I:= 1 to Length(S) do
        if S[I] in ['0'..'9'] then
          Result:= Result*10 + Byte(S[I])-Byte('0')
        else
          Break;
    end;

  var  // ParseOptions
    Option: TSortOption;
    Str: String;
  begin
    Result:= TList.Create;
    repeat
      Str:= CutNextOption;
      Option:= TSortOption.Create;
      Option.Col:= GetNum(Str);
      Option.UpDirection:= Pos('UP', Str) <> 0;
      if Pos('STRING', Str)<>0 then
        Option.SortType:= stString
      else if Pos('NUMBER', Str)<>0 then
        Option.SortType:= stNumber
      else If Pos('DATE', Str)<>0 then
        Option.SortType:= stDate
      else If Pos('OBJECT', Str)<>0 then
        Option.SortType:= stObject
      else
        Raise Exception.Create('Unknown sort type');
      Result.Add(Option);
    until InStr='';
  end;

  function Compare(Row1, Row2: Integer): Boolean;

    function SubCompare(Option: TSortOption): Integer;

      function CompareDate: Integer;

        function MyStrToDate(Const S: String): TDateTime;
        begin
          try
            Result:= StrToDate(S);
          except
            Result:= 0;
          end;
        end;

      begin  // CompareDate
        If MyStrToDate(Grid.Cells[Option.Col, Row1]) > MyStrToDate(Grid.Cells[Option.Col, Row2]) Then
          Result:= 1
        Else If MyStrToDate(Grid.Cells[Option.Col, Row1]) = MyStrToDate(Grid.Cells[Option.Col, Row2]) Then
          Result:= 0
        Else
          Result:= -1;
      end;

      function CompareString: Integer;

        function FilterLine(Const InStr: String): String;
        var
          I: Integer;
        begin
          Result:='';
          For I := 1 To Length(InStr) do
            If Not (InStr[I] in ['(',')','<','>','[',']','{','}']) then
              Result := Result + InStr[I];
        end;

      begin  // CompareString
        If FilterLine(Grid.Cells[Option.Col, Row1]) > FilterLine(Grid.Cells[Option.Col, Row2]) Then
          Result:= 1
        Else If FilterLine(Grid.Cells[Option.Col, Row1]) = FilterLine(Grid.Cells[Option.Col, Row2]) Then
          Result:= 0
        Else
          Result:= -1;
      end;

      function CompareNumber: Integer;
      begin
        If MyStrToFloat(Grid.Cells[Option.Col, Row1]) > MyStrToFloat(Grid.Cells[Option.Col, Row2]) Then
          Result:= 1
        Else If MyStrToFloat(Grid.Cells[Option.Col, Row1]) = MyStrToFloat(Grid.Cells[Option.Col, Row2]) Then
          Result:= 0
        Else
          Result:= -1;
      end;

    function CompareObject: Integer;
    begin
      if Integer(Grid.Rows[Row1].Objects[Option.Col]) > Integer(Grid.Rows[Row2].Objects[Option.Col]) then
        Result:= 1
      else if Integer(Grid.Rows[Row1].Objects[Option.Col]) = Integer(Grid.Rows[Row2].Objects[Option.Col]) then
        Result:= 0
      else
        Result:= -1;
    end;

    begin  // SubCompare
      case Option.SortType of
        stDate: Result:= CompareDate;
        stNumber: Result:= CompareNumber;
        stString: Result:= CompareString;
        stObject: Result:= CompareObject;
      else
        Raise Exception.Create('Unknown Sort Type');
      end;
      if not Option.UpDirection then
        Result:= Result * -1;
    end;

  var     // Compare
    I: Integer;
    Res: Integer;
  begin
    Result:= False;
    I:= 0;
    while True do
    begin
      Res:= SubCompare(TSortOption(SortOptions[I]));
      Inc(I);
      If (Res <> 0) or (I>=SortOptions.Count) then
      begin
        Result:= Res = 1;
        Break;
      end;
    end;
  end;

  procedure QuickSort (lo,hi: integer);
 //  lo is the lower index, hi is the upper index
 //  of the region of array a that is to be sorted
  var //QuickSort
    TempRow: TStringList;
    iPivot,i,j: integer;
  begin
    TempRow:= TStringList.Create;
    i:=lo;
    j:=hi;
    iPivot:=lo;//(lo+hi) div 2;
   //  partition
    repeat
      while ((i <= hi) and Compare(iPivot, i)) do
        inc(i);

      while ((j >= 0) and Compare(j, iPivot)) do
        dec(j);

      If i<=j Then
      begin
        TempRow.Assign(Grid.Rows[J]);
        Grid.Rows[J].Assign(Grid.Rows[I]);
        Grid.Rows[I].Assign(TempRow);
        inc(i);
        dec(j);
      end;
    until (i>j);

   //  recursion
    if (lo<j) then
      QuickSort(lo, j);
    if (i<hi) then
      QuickSort(i, hi);
    TempRow.Free;
  end;

var  // SortGrid
  I: Integer;
//=== fixed on 20.4.2005 TF: Bubblesort was replaced with quicksort for speed
begin
  SortOptions:= ParseOptions(UpperCase(SortStyle));
  try
   quicksort(Grid.FixedRows,Grid.RowCount-1);
  finally
    for I:= 0 to SortOptions.Count-1 do
      TSortOption(SortOptions[I]).Free;
    SortOptions.Free;
  end;
end;

//---------------------------------------------------------------------
function DateToLongStr(Date : TdateTime) : string;
var
  Year,Month,Day : word;
//=====================

  function adZ(inp:string):string;
  begin
    if length(inp)=1 then
      Result:='0'+inp
    else
      Result:=inp;
  end;
//=====================
begin
  try
//    if Date <= 2 then
//      Result:='00/00/0000'
//    else
    begin
      DecodeDate(Date,Year,Month,Day);
      Result:=adZ(IntToStr(Day))+'/'+adZ(IntToStr(Month))+'/'+IntToStr(Year);
    end;
  except
    on EconvertError do
      Result:='Error';
  end;
end;
//---------------------------------------------------------------------

procedure CreateHintWindow(ClientForm : tForm; var MemoNote : tHebMemo; var MemoShadow : tPanel; x,y,width,height : integer; TextLines : tStrings);
begin
  MemoShadow       := tPanel.Create(ClientForm);
  MemoNote         := tHebMemo.Create(ClientForm);

  MemoNote.Width    := Width;
  MemoNote.Height   := Height;
  MemoShadow.Width  := Width;
  MemoShadow.Height := Height;

  MemoNote.Top      := y;
  MemoNote.Left     := x;

  MemoShadow.Top    := y+3;
  MemoShadow.Left   := x+3;

  MemoNote.Enabled  := false;
  MemoNote.Color    := $00E1FFFF;
  MemoShadow.Color  := clBlack;

  MemoShadow.BevelInner := bvNone;
  MemoShadow.BevelOuter := bvNone;

  MemoShadow.Parent := ClientForm;
  MemoNote.Parent   := ClientForm;

  MemoNote.Lines.Assign(TextLines);
  MemoNote.Ctl3D      := false;
end;
//---------------------------------------------------------------------
function FormatNumber(StrNum : string; bPoint,aPoint : integer; Positive : boolean) : string;
begin
  try
    if Positive then
      Str(Abs(StrToFloat(StrNum)):bPoint:aPoint,StrNum)
    else
      Str(StrToFloat(StrNum):bPoint:aPoint,StrNum);

    FormatNumber := RemoveSpaces(StrNum, rmBoth);
  except
    FormatNumber := '';
  end;
end;
//---------------------------------------------------------------------
function IsNotDate(const inpStr : string; var RealDate: boolean):boolean;
var
  Date: TDateTime;
begin
  RealDate:= False;
  try
    Date:= StrToDate(inpStr);
    RealDate:= True;
    Result:= (Date < EncodeDate(1900, 01, 01)) Or
             (Date >= EncodeDate(3000, 01, 01));
  except
    Result:= True;
  end;
end;
//---------------------------------------------------------------------
function DoubleIndexOf(Grid: THebStringGrid; Col1, Col2: String; StartRow: Integer): Integer;
var
  Row: Integer;
begin
  Result:= -1;
  For Row:= Grid.FixedRows To Grid.RowCount-1 Do
    with Grid.Rows[Row] Do
      If (Strings[StartRow] = Col1) And (Strings[StartRow+1] = Col2) Then
      begin
        Result:= Row;
        Exit;
      end;
end;

function MakeHebStr(const InStr: String; Numeric: Boolean): String;
var
  I, Ibegin, Iend: Integer;
  BeginStr, EndStr: String;
begin
  Result := '';
  If Numeric and (InStr = '0') Then
    Exit;

  Ibegin:= 1;
  BeginStr := '';
  while ((Length(InStr)+1)>Ibegin) and (InStr[Ibegin] < #$7F) Do
  begin
    BeginStr := BeginStr + InStr[Ibegin];
    Inc(Ibegin);
  end;
  EndStr:= '';
  Iend:=Length(InStr);
  While (Iend> Ibegin) and (InStr[Iend]<#$7F) Do
  begin
    EndStr:= InStr[Iend]+ EndStr;
    Dec(Iend);
  end;

  For I:= Ibegin to Iend Do
  begin
    Result := Result + InStr[I];
  end;
  Result := SetCharPlacement(EndStr)+ Result + SetCharPlacement(BeginStr);
end;

function CheckID(const ID: String): Boolean;
var
  Bikoret, Sum, I, J: Integer;
begin
  If not fmMain.NeedCheckID Then
  begin
    Result:= True;
    Exit;
  end;

  Result:= False;
  If Length(ID) <> 9 then
    Exit;

  try
    Sum:= 0;
    For I:= 1 To 8 do
    begin
      If (I mod 2) = 0 Then
      begin
        J:= StrToInt(ID[I]) * 2;
        If J>9 Then
          J:= J - 9;
      end
      else
        J:= StrToInt(ID[I]);
      Inc(Sum, J);
    end;

    Bikoret:= 10 - (sum mod 10);
    If Bikoret = 10 Then
      Bikoret:= 0;

    Result:= ID[9] = IntToStr(Bikoret)[1];
  except
  end;
end;

function Max(A,B: Integer): Integer;
begin
  If A>B Then
    Result:= A
  else
    Result:= B;
end;

function GetTempFileName(const Ext: String): String;
var
  I: Integer;
  MainPath: String;
begin
  I:= 0;
  MainPath:= ExtractFilePath(ParamStr(0));
  While FileExists(MainPath+'Temp'+IntToStr(I)+Ext) do
    Inc(I);

  Result := MainPath+'Temp'+IntToStr(I)+Ext;
end;

//==============================================================================
function CaptionToInt(Const Caption: String): Integer;
var
  I: Integer;
begin
  Result:= 1;
  For I:= 1 to Length(Caption) do
    If Ord(Caption[I])<> 0 Then
      Result:= Result * Ord(Caption[I]);
end;


//==============================================================================

// TUserOptions
constructor TUserOptions.Create(const FileName: String);
const           //0     1     2        3        4        5       6      7      8    9
  DefaultValues= #13 + #13+ '0'#13 + '1'#13 + '0'#13 + '0'#13 +'1'#13+'1'#13+#13 + #13;
begin
  FFileName:= FileName;
  FData:= TStringList.Create;
  FDefaultValues:= TStringList.Create;
  FDefaultValues.Text:= DefaultValues;
end;

destructor TUserOptions.Destroy;
begin
  FData.Free;
  FDefaultValues.Free;
end;

procedure TUserOptions.Save;
begin
  FData.SaveToFile(FFileName);
end;

procedure TUserOptions.SetStringParam(Index: Integer; const Value: String);
begin
  Load;
  FData[Index]:= Value;
  Save;
end;

function TUserOptions.GetStringParam(Index: Integer): String;
begin
  Load;
  Result:= FData[Index];
end;

function TUserOptions.GetIntParam(Index: Integer): Integer;
begin
  try
    Result:= StrToInt(GetStringParam(Index));
  except
    Result:= StrToInt(FDefaultValues[Index]);  // If Error occurs here -> check the Default Values in Constructor
  end;
end;

function CheckBdeBadSettings : Boolean;
var
  LocalShare : String;
  SessionParams : TStringList;
begin
  SessionParams := TStringList.Create;
  SessionParams.Clear;

  Session.GetConfigParams('\SYSTEM\INIT', EmptyStr, SessionParams);
  LocalShare := UpperCase(Trim(SessionParams.Values['LOCAL SHARE']));

  SessionParams.Free;

  Result := (LocalShare = 'FALSE');
end;

function TUserOptions.GetBoolParam(Index: Integer): Boolean;
begin
  Result:= GetIntParam(Index) <> 0;
end;

procedure TUserOptions.SetIntParam(Index, Value: Integer);
begin
  SetStringParam(Index, IntToStr(Value));
end;

procedure TUserOptions.SetBoolParam(Index: Integer; Value: Boolean);
var v:integer;
begin
 if Value then
   v:=1
 else
   v:=0;
 SetIntParam(Index,v);
end;

procedure TUserOptions.Load;
var
  I: Integer;
begin
  FData.Clear;
  try
    FData.LoadFromFile(FFileName);
  except
  end;

  For I:= FData.Count to FDefaultValues.Count-1 do  // If File has less rows than needed -> fill the default values
    FData.Add(FDefaultValues[I]);
end;


//write to some file
procedure WriteLog(const S: String; const F: String);
begin
  WriteAnyLog(S,F,false);
end;

//write to changelog
procedure WriteChangeLog(const S: String);
var
  fileName : string;
  logDir :string;
begin
  logDir :=  MainPath+'Logs\';
  fileName := logDir+GetComputerNetName()+FormatDateTime('yymm',Now)+'.log';
  if NOT DirectoryExists(logDir) then
  begin
    CreateDir( logDir);
  end;

  WriteAnyLog(S,fileName,true);
end;

procedure WriteAnyLog(const S: String; const F: String; isChangeLog: boolean );
var
  TF: Text;
  Msg : String; // Added on 06/03/2005 by ES - new log
begin
 // If (isChangeLog OR (UpperCase(ParamStr(1)) = 'LOG')) then  debug
   // begin
      { Added on 06/03/2005 by ES - new log --> }
  Msg := FormatDateTime( 'dd/mm/yyyy hh:nn:ss', Now ) + ' ' + S;
      { <-- }

  AssignFile(TF, {'C:\RashLog.txt'}F);
      { Added on 06/03/2005 by ES - new log --> }
  if FileExists( F ) then
    Append(TF)
  else
    Rewrite( TF );
      { <-- }
  Writeln(TF, FormatDateTime( 'dd/mm/yyyy hh:nn:ss', Now ) + ' ' + UserComp + ': ' + S);
  CloseFile(TF);
  //  end;
end;

{ Added on 07/03/2005 by ES - new log --> }
//==============================================================================
function GetComputerNetName: string;
var
  buffer: PChar;
  size: DWORD;
begin
  size := MAX_COMPUTERNAME_LENGTH + 1;
  GetMem( buffer, size );
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := 'Unspecified';
end;

//==============================================================================
function GetUserFromWindows: string;
Var
   UserName : string;
   UserNameLen : Dword;
Begin
   UserNameLen := 255;
   SetLength(userName, UserNameLen) ;
   If GetUserName(PChar(UserName), UserNameLen) Then
     Result := Copy(UserName,1,UserNameLen - 1)
   Else
     Result := 'Unknown';
End;
{ <-- }

//===============================Added Tsahi 19.7.05 : formatting data recievec from Kav-Manhe
Function ClearTokens(S, Token: string):String;
var
  ResStr: string;
  iPos: integer;
begin
  ResStr:='';
  repeat
    iPos:=Pos(Token,S);
    if iPos=0 then
      ResStr:=ResStr+S
    else
      ResStr:=ResStr+Copy(S,1,iPos-1);

    S:=Copy(S,iPos+1,Length(S));
  until iPos=0;
  Result:=ResStr;
end;
//===============================Added Tsahi 19.7.05 : formatting data recievec from Kav-Manhe
Function FutureToPast(DateStr: string):String;
var
  OriginalDate: TDateTime;
  CurYear, DateYear,Month,Day: word;
begin
  DecodeDate(Now,CurYear,Month,Day); //current date
  OriginalDate:=StrToDateDef(DateStr,0);
  if 0=OriginalDate then
    Result:='00/00/0000'
  else
  begin
    DecodeDate(OriginalDate,DateYear,Month,Day); //date in str

    if DateYear> CurYear then //date is in the future (conversion error)
    begin
      OriginalDate:=EncodeDate(DateYear-100,Month,Day);//back to the 20th century!
    end;

    Result:=FormatDateTime('dd/mm/yyyy',OriginalDate);
  end;
end;
//============================Added Tsahi 20.7.05 : formatting data recievec from Kav-Manhe
Function AddDecimals(S: string): string;
begin
  if S='' then
    Result:='0.00'
  else if Pos('.',S) = 0 then
    Result:=S+'.00'
  else
    Result:=S;
end;
//========================================================Tsahi. 06/11/05
function StrToDateDef(Str: string; Def:TDateTime): TDateTime;
begin
  try
    Result:=StrToDate(Str);
  except
    Result:=Def;
  end;
end;
//============================
function InIntArray(Item: integer; Arr :Array of integer):boolean;
var
  i: integer;
  iSize: integer;
begin
  Result:=false;
  iSize:=High(Arr);
  for i:= low(Arr) to iSize do
    if Arr[i] = Item then
    begin
      Result:=True;
      break;
    end;
end;
//============================
function FormatForFilter(S: string): string; //makes all ' into '' in string for filter strings
var
  iPos: integer;
  ResStr: string;
  TempStr: string;
begin
  TempStr:= S;
  ResStr:='';
  Repeat
    iPos:= pos('''',TempStr);
    if iPos<>0 then
    begin
      ResStr:=ResStr+Copy(TempStr,1,iPos)+'''';
      TempStr:=Copy(TempStr,iPos+1,Length(TempStr));
    end;
  until 0 = iPos;
  ResStr:=ResStr+TempStr;
  Result:=ResStr;
end;
//============================08/11/05 Tsahi: Logging Errors (without LOG in parameters)
procedure LogError(Error: String);
var
  LogFile: Text;
  FileName: string;
begin
  FileName:=ExtractFilePAth(ParamStr(0))+'XMLError.Log';

  AssignFile(LogFile, FileName);
  if FileExists( FileName ) then
    Append(LogFile)
  else
    Rewrite( LogFile );
  Writeln(LogFile, Error);
  CloseFile(LogFile);
end;
//============================//Tsahi 09/11/05 Checks if a row in a string grid is empty
function RowEmpty(Row: TStrings): boolean;
var
  i:integer;
begin
  Result:=True;
  for i:=0 to Row.Count -1 do
    if Row[i]<>'' then
    begin
      Result:=False;
      break;
    end;
end;
//=============================Tsahi 09/11/05
function IsNumeric(Key: char): boolean;
begin
  result := (Key in ['0'..'9']);
end;
//===========================Tsahi 10/11/05 : arrows, enter etc..
function IsControlChar(Key: char): boolean;
begin
  Result := ((Key<#32) or (Key=#127));
end;
//===========================
// From http://www.borland.com/devsupport/bde/bdeapiex/dbidorestructure.html
procedure AddField(Table: TTable; NewField: ChangeRec);
var
  Props: CURProps;
  hDb: hDBIDb;
  TableDesc: CRTblDesc;
  pFlds: pFLDDesc;
  pOp: pCROpType;
  B: byte;
begin
  // Make sure the table is open exclusively so we can get the db handle...
{  if Table.Active = False then
    raise EDatabaseError.Create('Table must be opened to restructure');
  if Table.Exclusive = False then
    raise EDatabaseError.Create('Table must be opened exclusively to restructure');}
  If (Table.Active and Not Table.Exclusive) Then
    Table.Close;
  If (Not Table.Exclusive) Then
    Table.Exclusive := True;
  If (Not Table.Active) Then
    Table.Open;

  // Get the table properties to determine table type...
  Check(DbiSetProp(hDBIObj(Table.Handle), curxltMODE, integer(xltNONE)));
  Check(DbiGetCursorProps(Table.Handle, Props));
  pFlds := AllocMem((Table.FieldCount + 1) * sizeof(FLDDesc));
  FillChar(pFlds^, (Table.FieldCount + 1) * sizeof(FLDDesc), 0);
  Check(DbiGetFieldDescs(Table.handle, pFlds));

  for B := 1 to Table.FieldCount do
  begin
    pFlds^.iFldNum := B;
    Inc(pFlds, 1);
  end;
  try
    StrCopy(pFlds^.szName, NewField.szName);
    pFlds^.iFldType := NewField.iType;
    pFlds^.iSubType := NewField.iSubType;
    pFlds^.iUnits1  := NewField.iLength;
    pFlds^.iUnits2  := NewField.iPrecision;
    pFlds^.iFldNum  := Table.FieldCount + 1;
  finally
    Dec(pFlds, Table.FieldCount);
  end;

  pOp := AllocMem((Table.FieldCount + 1) * sizeof(CROpType));
  Inc(pOp, Table.FieldCount);
  pOp^ := crADD;
  Dec(pOp, Table.FieldCount);

  // Blank out the structure...
  FillChar(TableDesc, sizeof(TableDesc), 0);
  //  Get the database handle from the table's cursor handle...
  Check(DbiGetObjFromObj(hDBIObj(Table.Handle), objDATABASE, hDBIObj(hDb)));
  // Put the table name in the table descriptor...
  StrPCopy(TableDesc.szTblName, Table.TableName);
  // Put the table type in the table descriptor...
  StrPCopy(TableDesc.szTblType, Props.szTableType);
  // Close the table so the restructure can complete...
  TableDesc.iFldCount := Table.FieldCount + 1;
  Tabledesc.pfldDesc := pFlds;
  TableDesc.pecrFldOp := pOp;
  Table.Close;
  // Call DbiDoRestructure...
  try
    Check(DbiDoRestructure(hDb, 1, @TableDesc, nil, nil, nil, FALSE));
  finally
    FreeMem(pFlds);
    FreeMem(pOp);
    Table.Exclusive:= False;
    Table.Open;
  end;
end;

// From   http://www.borland.com/devsupport/bde/bdeapiex/dbidorestructure.html
procedure ChangeField(Table: TTable; Field: TField; Rec: ChangeRec);
var
  Props: CURProps;
  hDb: hDBIDb;
  TableDesc: CRTblDesc;
  pFields: pFLDDesc;
  pOp: pCROpType;
  B: byte;

begin
  // Make sure the table is open exclusively so we can get the db handle...
  if Table.Active = False then
    raise EDatabaseError.Create('Table must be opened to restructure');
  if Table.Exclusive = False then
    raise EDatabaseError.Create('Table must be opened exclusively to restructure');

  Check(DbiSetProp(hDBIObj(Table.Handle), curxltMODE, integer(xltNONE)));
  // Get the table properties to determine table type...
  Check(DbiGetCursorProps(Table.Handle, Props));
  // Make sure the table is either Paradox or dBASE...
  if (Props.szTableType <> szPARADOX) and (Props.szTableType <> szDBASE) then
    raise EDatabaseError.Create('Field altering can only occur on Paradox' +
                ' or dBASE tables');
  // Allocate memory for the field descriptor...
  pFields := AllocMem(Table.FieldCount * sizeof(FLDDesc));
  // Allocate memory for the operation descriptor...
  pOp := AllocMem(Table.FieldCount * sizeof(CROpType));
  try
    // Set the pointer to the index in the operation descriptor to put
    // crMODIFY (This means a modification to the record is going to happen)...
    Inc(pOp, Field.Index);
    pOp^ := crMODIFY;
    Dec(pOp, Field.Index);
    // Fill the field descriptor with the existing field information...
    Check(DbiGetFieldDescs(Table.Handle, pFields));
    // Set the pointer to the index in the field descriptor to make the
    // midifications to the field
    Inc(pFields, Field.Index);

    // If the szName portion of the ChangeRec has something in it, change it...
    if StrLen(Rec.szName) > 0 then
      pFields^.szName := Rec.szName;
    // If the iType portion of the ChangeRec has something in it, change it...
    if Rec.iType > 0 then
      pFields^.iFldType := Rec.iType;
    // If the iSubType portion of the ChangeRec has something in it, change it...
    if Rec.iSubType > 0 then
      pFields^.iSubType := Rec.iSubType;
    // If the iLength portion of the ChangeRec has something in it, change it...
    if Rec.iLength > 0 then
      pFields^.iUnits1 := Rec.iLength;
    // If the iPrecision portion of the ChangeRec has something in it, change it...
    if Rec.iPrecision > 0 then
      pFields^.iUnits2 := Rec.iPrecision;
    Dec(pFields, Field.Index);

    for B := 1 to Table.FieldCount do begin
      pFields^.iFldNum := B;
      Inc(pFields, 1);
    end;
    Dec(pFields, Table.FieldCount);

    // Blank out the structure...
    FillChar(TableDesc, sizeof(TableDesc), 0);
    //  Get the database handle from the table's cursor handle...
    Check(DbiGetObjFromObj(hDBIObj(Table.Handle), objDATABASE, hDBIObj(hDb)));
    // Put the table name in the table descriptor...
    StrPCopy(TableDesc.szTblName, Table.TableName);
    // Put the table type in the table descriptor...
    StrPCopy(TableDesc.szTblType, Props.szTableType);
    // The following three lines are necessary when doing any field restructure
    // operations on a table...

    // Set the field count for the table
    TableDesc.iFldCount := Table.FieldCount;
    // Link the operation descriptor to the table descriptor...
    TableDesc.pecrFldOp := pOp;
    // Link the field descriptor to the table descriptor...
    TableDesc.pFldDesc := pFields;
    // Close the table so the restructure can complete...
    Table.Close;
    // Call DbiDoRestructure...
    Check(DbiDoRestructure(hDb, 1, @TableDesc, nil, nil, nil, FALSE));
    Table.Exclusive:= False;
    Table.Open;
  finally
    if pFields <> nil then
      FreeMem(pFields);
    if pOp <> nil then
      FreeMem(pOp);
  end;
end;
//===Added Tsahi 14.6.06 . Copied from SuperData================================
procedure BackUpTable(const TableName: String);
var
  SearchRec: TSearchRec;
  Error: Integer;
  AliasPath: string;
  AliasProp: TStringList;
begin
  AliasProp:= TStringList.Create;
  Session.GetAliasParams('Rasham', AliasProp);
  AliasPath:= AliasProp.Values['Path'];
  If AliasPath[Length(AliasPath)] <> '\' then
    AliasPath:= AliasPath +'\';
  AliasProp.Free;

  Error:= FindFirst(AliasPath+Copy(TableName,1, Length(TableName)-2)+'*', faAnyFile, SearchRec);
  while Error=0 do
  begin
    Windows.CopyFile(PChar(AliasPath+SearchRec.Name), PChar(ExtractFilePath(ParamStr(0))+'Backups\'+SearchRec.Name), False);
    Error:= FindNext(SearchRec);
  end;
  FindClose(SearchRec);
end;
//==============================================================================
procedure RebuildDB(AliasName, FileName: string);
var
  DBFixer :TDBFixer;
  i : integer;
begin
  DBFixer := TDBFixer.Create;
  DBFixer.AliasName := AliasName;
  DBFixer.Fix;
  for i:=0 to DBFixer.MessageList.Count-1 do
    WriteLog(DBFixer.MessageList[i],FileName);
  DBFixer.Free;
end;
//==============================================================================
function SplitCompanyName(companyName: string): TSplitCompanyName;
var
  isFound: boolean;
  index, stringLength: integer;
  currCharValue, lowerAValue ,lowerZValue, upperAValue ,upperZValue: integer;

begin
  lowerAValue := Ord('a');
  lowerZValue := Ord('z');
  upperAValue := Ord('A');
  upperZValue := Ord('Z');

  stringLength := Length(companyName);
  isFound := false;
  index := 0;

  while ((not isFound) AND (index < stringLength)) DO
  begin
    currCharValue := Ord(companyName[index]);
    isFound := (((currCharValue >= lowerAValue) AND (currCharValue <= lowerZValue )) OR  ((currCharValue >= upperAValue) AND (currCharValue <= upperZValue )));
    index := index+1 ;
  end;

  if (isFound = true) then
  begin
    result.HebrewName := Copy(companyName, 0 , index-2);
    result.EnglishName := Copy(companyName, index-1 , stringLength - index+2 );
  end
  else
  begin
    result.HebrewName := companyName;
  end;
end;

procedure SplitBySep(const S, Sep: string; var Left, Right: string);
var
  l: integer;
begin
  Left := S;
  Right := '';
  l := Pos(Sep, S);
  if (l > 0) then
  begin
    Left := Copy(S, 1, l-1);
    Right := Copy(S, l+1, Length(S));
  end;
end;

procedure SplitName(const Name: string; var FirstName, LastName: string);
begin
  SplitBySep(Name, ' ', FirstName, LastName);
end;

function ReplaceStr(const S, Search, Replace: string): string;
var
  i: Integer;
  Source: string;
begin
  Source := S;
  Result := '';
  repeat
    i := Pos(Search, Source);
    if (i > 0) then
    begin
      Result := Result + Copy(Source, 1, i - 1) + Replace;
      Source := Copy(Source, i + Length(Search), MaxInt);
    end
    else
      Result := Result + Source;
  until I <= 0;
end;

function StringToFloat(const str: string): double;
var
  s: string;
begin
  try
    s := ReplaceStr(str, ',', '');
    Result := StrToFloat(s);
  except
    Result:=0;
  end;
end;

function Iif(Condition: boolean; aTrue, aFalse: Variant): Variant;
begin
  if (Condition) then
    Result := aTrue
  else
    Result := aFalse;
end;

function Pad0(const Str: string; Size: integer): string;
var
  l, i: integer;
  s: string;
begin
  Result := '';
  if (Str = '') then
    Exit;
  l := Length(Str);
  if (l > Size) then
    Result := Str
  else
  begin
    s := '0000000000' + Str;
    i := Size - l + 1;
    Result := Copy(s, i, Size);
  end;
end;
//==============================================================================
var
  TF: Text;

initialization

  If UpperCase(ParamStr(1)) = 'LOG' then
  begin
    AssignFile(TF, GetLogFileName);//'C:\RashLog.txt');
    Rewrite(TF);
    CloseFile(TF);
  end;

end.
