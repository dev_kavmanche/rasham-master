unit RoeHeshbon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, HLabel,
  HEdit, DataM;

type
  TfmRoeHeshbon = class(TfmTemplate)
    Shape84: TShape;
    HebLabel42: THebLabel;
    Shape85: TShape;
    e11: THebEdit;
    Shape1: TShape;
    HebLabel1: THebLabel;
    Shape2: TShape;
    e12: THebEdit;
    HebLabel41: THebLabel;
    Shape59: TShape;
    Shape123: TShape;
    txtIdNum: TEdit;
    txtLicense: THebEdit;
    Shape129: TShape;
    HebLabel71: THebLabel;
    HebLabel74: THebLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    function EvSaveData(Draft: Boolean): Boolean; override;
    procedure EvFocusNext(Sender: TObject); override;
  public
    { Public declarations }
  end;

var
  fmRoeHeshbon: TfmRoeHeshbon;

implementation

uses dialog2, Main;

{$R *.DFM}

procedure TfmRoeHeshbon.EvFocusNext(Sender: TObject);
begin
    //==>tsahi 11.9.08: changed due to changes made by Rasham Ha-Havarot
  if Sender=e11 Then
  begin
     txtIdNum.SetFocus;
  end
  else if Sender=txtIdNum Then
  begin
     e12.SetFocus;
  end
  else if Sender=e12 Then
  begin
     txtLicense.SetFocus;
  end
  else if Sender= txtLicense Then
  begin
     e11.SetFocus;
  end;
  //<==tsahi 11.9.08
end;

function TfmRoeHeshbon.EvSaveData(Draft: Boolean): Boolean;
begin
  try
    fmMain.UserOptions.RoeHeshbon:= e11.Text;
    fmMain.UserOptions.RoeAddress:= e12.Text;
    //==>tsahi 11.9.08: added to comply with changes made by Rasham Ha-Havarot
    fmMain.UserOptions.RoeIdNum := txtIdNum.Text;
    fmMain.UserOptions.RoeLicense := txtLicense.Text;
    //<==tsahi 11.9.08
    fmDialog2.Show;
    Result:= True;
  finally
  end;
end;

procedure TfmRoeHeshbon.FormCreate(Sender: TObject);
begin
  inherited;
  e11.Text:= fmMain.UserOptions.RoeHeshbon;
  e12.Text:= fmMain.UserOptions.RoeAddress;
  //==>tsahi 11.9.08: added to comply with changes made by Rasham Ha-Havarot
  txtIdNum.Text := fmMain.UserOptions.RoeIdNum;
  txtLicense.Text := fmMain.UserOptions.RoeLicense;
  //<==tsahi 11.9.08
end;

end.
