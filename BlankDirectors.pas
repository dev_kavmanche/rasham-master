unit BlankDirectors;

interface                                                                                                     

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, StdCtrls, HRadio, HGrids, SdGrid, HComboBx, HEdit,
  HebForm, DataObjects, ExtCtrls, ComCtrls, Buttons, Mask, HLabel, HListBox;

type
  TfmDirectors = class(TfmBlankTemplate)
    Shape55: TShape;
    Shape45: TShape;
    Shape5: TShape;
    Shape6: TShape;
    Shape62: TShape;
    Shape51: TShape;
    Shape52: TShape;
    Shape53: TShape;
    HebLabel28: THebLabel;
    HebLabel31: THebLabel;
    Shape63: TShape;
    HebLabel3: THebLabel;
    Shape7: TShape;
    HebLabel4: THebLabel;
    Shape8: TShape;
    HebLabel27: THebLabel;
    Shape60: TShape;
    HebLabel23: THebLabel;
    Shape43: TShape;
    Shape44: TShape;
    Shape46: TShape;
    HebLabel24: THebLabel;
    Shape47: TShape;
    Shape48: TShape;
    HebLabel25: THebLabel;
    Shape49: TShape;
    Shape50: TShape;
    HebLabel26: THebLabel;
    Shape57: TShape;
    HebLabel30: THebLabel;
    Shape56: TShape;
    HebLabel29: THebLabel;
    Shape54: TShape;
    Shape21: TShape;
    Shape23: TShape;
    HebLabel52: THebLabel;
    pnDir: TPanel;
    Shape59: TShape;
    Shape78: TShape;
    Shape75: TShape;
    Shape72: TShape;
    Shape70: TShape;
    Shape69: TShape;
    Shape80: TShape;
    HebLabel40: THebLabel;
    HebLabel35: THebLabel;
    HebLabel34: THebLabel;
    Shape67: TShape;
    Shape68: TShape;
    Shape79: TShape;
    HebLabel37: THebLabel;
    Shape71: TShape;
    HebLabel36: THebLabel;
    Shape73: TShape;
    HebLabel38: THebLabel;
    HebLabel39: THebLabel;
    Shape77: TShape;
    Shape76: TShape;
    Shape74: TShape;
    HebLabel41: THebLabel;
    e412: THebEdit;
    e414: THebEdit;
    e417: THebComboBox;
    e416: TEdit;
    e413: TEdit;
    e418: THebComboBox;
    e415: THebEdit;
    e42: THebComboBox;
    e43: TMaskEdit;
    e44: TMaskEdit;
    Panel19: TPanel;
    Panel28: TPanel;
    e45: THebEdit;
    e46: TEdit;
    e49: TEdit;
    e47: THebEdit;
    e410: THebComboBox;
    e411: THebComboBox;
    TempGrid4: TSdGrid;
    e48: THebEdit;
    e46b: TEdit;
    TabSheet2: TTabSheet;
    Bevel2: TBevel;
    Grid: TSdGrid;
    Panel11: TPanel;
    Panel26: TPanel;
    nGrid: THebStringGrid;
    gb21: TPanel;
    gb22: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel1: TPanel;
    HebLabel7: THebLabel;
    HebLabel6: THebLabel;
    rb22: THebRadioButton;
    rb21: THebRadioButton;
    e41: THebComboBox;
    btSearch: TButton;
    Shape9: TShape;
    procedure gb21Click(Sender: TObject);
    procedure gb22Click(Sender: TObject);
    procedure e41Change(Sender: TObject);
    procedure e42Change(Sender: TObject);
    procedure Panel28Click(Sender: TObject);
    procedure Panel19Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GridEnter(Sender: TObject);
    procedure GridSelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure GridKeyPress(Sender: TObject; var Key: Char);
    procedure nGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure nGridDblClick(Sender: TObject);
    procedure GridLineReset(Sender: TObject; Row: Integer);
    procedure btSearchClick(Sender: TObject);
    procedure e45Change(Sender: TObject);
    procedure e412Change(Sender: TObject);
  private
    { Private declarations }
    procedure Load1Enum(Director: TDirector);
    procedure Load2Enum(ObjectNum, NewNum: Integer);
    procedure SaveOldDirectors(FileItem: TFileItem; Directors: TDirectors);
  protected
    procedure EvPrintData; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    function Save4(Index: Integer): Boolean;
    procedure Load4(Index: Integer);
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvFillDefaultData; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    procedure EvDeleteEmptyRows; override;
    function EvCollectData(var Directors: TDirectors): TDirectors;
    procedure EvDataLoaded; override;
    function EvLoadData: Boolean; override;
    procedure EvPageIndexChange; override;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmDirectors: TfmDirectors;

implementation

uses cStd, dialog, DataM, SaveDialog, LoadDialog, dialog2, PrintDirectors,
  PeopleSearch, Util;

{$R *.DFM}

procedure TfmDirectors.EvPageIndexChange;
begin
  Inherited;
  try
    if PageIndex = 0 then
      e45.SetFocus
    Else
      Grid.SetFocus;
  except End;
end;

procedure TfmDirectors.DeleteFile(FileItem: TFileItem);
var
  Directors: TDirectors;
begin
  If Not FileItem.FileInfo.Draft Then
    Raise Exception.Create('This file is not Draft at TfmDirectors.DeleteFile');
  If (FileItem.FileInfo.Action <> faDirector) Then
    raise Exception.Create('Invalid file type at TfmDirectors.DeleteFile');
  Directors:= TDirectors.LoadFromFile(Company, FileItem);
  Directors.FileDelete(FileItem);
  Directors.Free;
  FileItem.DeleteFile;
end;

procedure TfmDirectors.EvFillDefaultData;
begin
  Grid.ColHebrew[3]:= False;
  Inherited;
  DATA.taCity.First;
  While Not DATA.taCity.EOF do
    begin
      e410.Items.Add(DATA.taCity.Fields[0].Text);
      e417.Items.Add(DATA.taCity.Fields[0].Text);
      DATA.taCity.Next;
    end;

  DATA.taCountry.First;
  While Not DATA.taCountry.EOF do
    begin
      e411.Items.Add(DATA.taCountry.Fields[0].Text);
      e418.Items.Add(DATA.taCountry.Fields[0].Text);
      DATA.taCountry.Next;
    end;
end;

procedure TfmDirectors.EvDeleteEmptyRows;
var
  I: Integer;
begin
  With Grid Do
    For I:= RowCount-1 DownTo 0 Do
      If RemoveSpaces(Cells[0,I], rmBoth)='' Then
        Grid.LineDeleteByIndex(I);
end;

procedure TfmDirectors.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);

  procedure SpecialCheckID(const ID: String; ComboBox: THebComboBox; ItemIndex: Integer; Edit: TEdit);
  begin
    If not CheckID(ID) and
       (not Silent) and (not ErrorTimer.Enabled) Then
      begin
        If PageIndex<> Page Then
          PageIndex:= Page;
        ComboBox.ItemIndex:= ItemIndex;
        ComboBox.OnChange(ComboBox);
        If ErrorTimer.Tag= 0 Then
          try
            Edit.SetFocus;
          except end;

        If Edit.Color = clWindow Then
          Edit.Color:= clBtnFace
        Else
          Edit.Color:= clWindow;

        If Application.MessageBox('��� ��!'+#13+#10+'���� ����� ����/���� ���� ����.'+#13+#10+'��� ��� ��� �����?',
                                  PChar(Caption), MB_YESNO + MB_IconHand + MB_RTLREADING + MB_RIGHT) <> IDYES then
          begin
            ErrorAt:= Edit;
            raise Exception.Create('zeut');
          end;
      end;
  end;

var
  Directors: TDirectors;
  I: Integer;
begin
  Inherited;
  If Page = 1 Then
    CheckGrid(Page, Silent, Extended, Grid);

  If Not Extended Then
    Exit;

  Case Page of
    0: begin
         Save4(StrToInt(e41.Text));
         Directors:= TDirectors.CreateNew;
         try
           Company.Directors.Filter:= afActive;
           With TempGrid4 Do
             For I:= 0 To RowCount-1 Do
               If (RemoveSpaces(Cells[0,I],rmBoth) <> '') Then
                 If (Directors.FindByZeut(Cells[5,I]) = nil) And
                    (Company.Directors.FindByZeut(Cells[5,I])= nil) Then
                   begin
                     SpecialCheckID(Cells[5,I], e41, e41.Items.IndexOf(Cells[0,I]), e46);
                     If Cells[1,I] = '1' Then
                       SpecialCheckID(Cells[13,I], e41, e41.Items.IndexOf(Cells[0,I]), e413);
                     Directors.Add( TDirector.CreateNew(Date, Cells[4,I], Cells[5,I], StrToDate(Cells[2,I]), StrToDate(Cells[3,I]),
                        TDirectorAddress.CreateNew(Cells[7,I], Cells[8,I], Cells[9,I], Cells[10,I], Cells[11,I], Cells[6,I]), Nil, True));
                     If StrToDate(Cells[2,I]) > StrToDate(Cells[3,I]) Then
                       begin
                         If Not Silent Then
                           begin
                             e41.ItemIndex:= e41.Items.IndexOf(Cells[0,I]);
                             e41Change(e41);
                             With e44 Do
                               If Color = clWindow Then
                                 Color:= clBtnFace
                               Else
                                 Color:= clWindow
                           end;
                         ErrorAt:= e44;
                         raise Exception.Create('����� ����� ����� ��� ������ ����');
                       end;
                   end
                 Else
                   begin
                     If Not Silent Then
                       begin
                         e41.ItemIndex:= StrToInt(Cells[0,I]);
                         e41Change(e41);
                         With e46 Do
                           If Color = clWindow Then
                             Color:= clBtnFace
                           Else
                             Color:= clWindow
                       end;
                     ErrorAt:= e46;
                     raise Exception.Create('���� ���� �� ��� ���� �������� ���');
                   end;
         finally
           Directors.Free;
         end;
       end;
    1: begin
         For I:= 0 To Grid.RowCount-1 Do
          If (nGrid.Cols[1].IndexOf(Grid.Cells[0,I])=-1) And
              (Not ((Grid.RowCount=1) And (RemoveSpaces(Grid.Cells[0,I],rmBoth)=''))) Then
            begin
              If Not Silent Then
                begin
                  Grid.Row:= I;
                  Grid.Col:= 0;
                end;
              ErrorAt:= Grid;
              raise Exception.Create('�� ���� �� �������� ����� �������');
            end;
       end;
  end;
end;

procedure TfmDirectors.EvFocusNext(Sender: TObject);
begin
  Inherited;
  If Sender = e14 Then
    If PageIndex=0 Then
      e41.SetFocus
    Else
      Grid.SetFocus;
  if Sender = e41   then e42.SetFocus;
  if Sender = e42   then e45.SetFocus;
  if Sender = e45   then e46.SetFocus;
  if Sender = e46   then e46b.SetFocus;
  if Sender = e46b  then e43.SetFocus;
  if Sender = e43   then e44.SetFocus;
  if Sender = e44   then e47.SetFocus;
  if Sender = e47   then e48.SetFocus;
  if Sender = e48   then e49.SetFocus;
  if Sender = e49   then e410.SetFocus;
  if Sender = e410  then e411.SetFocus;
  if Sender = e411  then
  if pnDir.Visible  then e412.SetFocus
                    else begin
                     fmDialog.memo.lines.Clear;
                     fmDialog.memo.lines.add('��� ������ ����� ������� ���� ?');
                     fmDialog.ShowModal;
                     if fmDialog.DialogResult = drYES then Panel28Click(nil)
                                                      else pnB1Click(pnB2);
                    end;

  if Sender = e412  then e413.SetFocus;
  if Sender = e413  then e414.SetFocus;
  if Sender = e414  then e415.SetFocus;
  if Sender = e415  then e416.SetFocus;
  if Sender = e416  then e417.SetFocus;
  if Sender = e417  then e418.SetFocus;
  if Sender = e418  then begin
                    fmDialog.memo.lines.Clear;
                    fmDialog.memo.lines.add('��� ������ ����� ������� ���� ?');
                    fmDialog.ShowModal;
                    if fmDialog.DialogResult = drYES then Panel28Click(nil)
                                                     else pnB1Click(pnB2);
                    end;
end;

procedure TfmDirectors.EvDataLoaded;
var
  I: Integer;
begin
  inherited;
  If Not DoNotClear Then
    begin
      e41.Items.Clear;
      e41.Items.Add('1');
      e41.ItemIndex:=0;
      e42.ItemIndex:=0;
      e411.Text := '�����';
      e418.Text := '�����';
      Grid.RowCount:=1;
      Grid.ResetLine(0);
      Grid.Repaint;
    end;
    
  nGrid.RowCount:= 1;
  nGrid.Rows[0].Clear;
  Company.Directors.Filter:= afActive;
  For I:= 0 To Company.Directors.Count-1 do
    begin
      nGrid.Cells[0, nGrid.RowCount-1]:= IntToStr(nGrid.RowCount);
      nGrid.Cells[1, nGrid.RowCount-1]:= (Company.Directors.Items[I] as TDirector).Name;
      nGrid.Cells[2, nGrid.RowCount-1]:= (Company.Directors.Items[I] as TDirector).Zeut;
      If (Company.Directors.Items[I] as TDirector).Taagid <> nil then
        nGrid.Cells[3, nGrid.RowCount-1]:= (Company.Directors.Items[I] as TDirector).Taagid.Name
      Else
        nGrid.Cells[3, nGrid.RowCount-1]:= '';
      nGrid.RowCount:= nGrid.RowCount+1;
    end;
  If nGrid.RowCount>1 Then
    nGrid.RowCount:= nGrid.RowCount-1;
end;

function TfmDirectors.Save4(Index: Integer): Boolean;
var
  GIndex   : integer;
begin
  If (RemoveSpaces(e45.Text,rmBoth)='') And (e46.Text='0') Then
    begin
      Result:= False;
      Exit;
    end;

  GIndex := TempGrid4.Cols[0].IndexOf(IntToStr(Index));
  if GIndex = -1 then
     begin
       TempGrid4.RowCount := TempGrid4.RowCount+1;
       GIndex := TempGrid4.RowCount-1;
     end;

  TempGrid4.Cells[0,GIndex]  := IntToStr(Index);
  TempGrid4.Cells[1,GIndex]  := IntToStr(e42.ItemIndex);
  TempGrid4.Cells[2,GIndex]  := e43.Text;
  TempGrid4.Cells[3,GIndex]  := e44.Text;
  TempGrid4.Cells[4,GIndex]  := e45.Text;
  TempGrid4.Cells[5,GIndex]  := e46.Text;
  TempGrid4.Cells[6,GIndex]  := e46b.Text;
  TempGrid4.Cells[7,GIndex]  := e47.Text;
  TempGrid4.Cells[8,GIndex]  := e48.Text;
  TempGrid4.Cells[9,GIndex]  := e49.Text;
  TempGrid4.Cells[10,GIndex] := e410.Text;
  TempGrid4.Cells[11,GIndex] := e411.Text;
  TempGrid4.Cells[12,GIndex] := e412.Text;
  TempGrid4.Cells[13,GIndex] := e413.Text;
  TempGrid4.Cells[14,GIndex] := e414.Text;
  TempGrid4.Cells[15,GIndex] := e415.Text;
  TempGrid4.Cells[16,GIndex] := e416.Text;
  TempGrid4.Cells[17,GIndex] := e417.Text;
  TempGrid4.Cells[18,GIndex] := e418.Text;
  Result:= True;
end;

procedure TfmDirectors.Load4(Index: Integer);
var
  GIndex : integer;
begin
  GIndex := TempGrid4.Cols[0].IndexOf(IntToStr(Index));

  If GIndex = -1 then
    begin
      e42.ItemIndex := 0;
      e42Change(e42);
      e43.Text := e13.Text;
      e44.Text := e13.Text;

      e45.Text := '';    e46.Text := '0';   e46b.Text := '';
      e47.Text := '';    e48.Text := '';    e49.Text := '0';
      e410.Text := '';
      e411.Text := '�����';

      e412.Text := '';   e413.Text := '0';   e414.Text := '';
      e415.Text := '';   e416.Text := '0';   e417.Text := '';
      e418.Text := '�����';
    end
  else
    begin
      e42.ItemIndex := StrToInt(TempGrid4.Cells[1,GIndex]);
      e42Change(e42);
      e43.Text      := TempGrid4.Cells[2,GIndex];
      e44.Text      := TempGrid4.Cells[3,GIndex];
      e45.Text      := TempGrid4.Cells[4,GIndex];
      e46.Text      := TempGrid4.Cells[5,GIndex];
      e46b.Text     := TempGrid4.Cells[6,GIndex];
      e47.Text      := TempGrid4.Cells[7,GIndex];
      e48.Text      := TempGrid4.Cells[8,GIndex];
      e49.Text      := TempGrid4.Cells[9,GIndex];
      e410.Text     := TempGrid4.Cells[10,GIndex];
      e411.Text     := TempGrid4.Cells[11,GIndex];
      e412.Text     := TempGrid4.Cells[12,GIndex];
      e413.Text     := TempGrid4.Cells[13,GIndex];
      e414.Text     := TempGrid4.Cells[14,GIndex];
      e415.Text     := TempGrid4.Cells[15,GIndex];
      e416.Text     := TempGrid4.Cells[16,GIndex];
      e417.Text     := TempGrid4.Cells[17,GIndex];
      e418.Text     := TempGrid4.Cells[18,GIndex];
    end;
end;

procedure TfmDirectors.gb21Click(Sender: TObject);
begin
  inherited;
  Grid.LineDelete;
end;

procedure TfmDirectors.gb22Click(Sender: TObject);
begin
  inherited;
  Grid.LineInsert;
end;

procedure TfmDirectors.e41Change(Sender: TObject);
var
  Old_DATAChanged: Boolean;
begin
  inherited;

  if Loading then
    exit;

  OLD_DATAChanged:= DATAChanged;
  Loading:= True;
  Save4(e41.Tag);
  Load4(StrToInt(e41.Text));
  E41.Tag := StrToInt(e41.Text);
  e42Change(nil);
  Loading:= False;
  DATAChanged:= OLD_DATAChanged;
end;

procedure TfmDirectors.e42Change(Sender: TObject);
begin
  inherited;
  if e42.ItemIndex = 0 then
    pnDIr.Visible := false
  else
    pnDIr.Visible := true;
  if Not Loading then
    DATAchanged := true;
end;

procedure TfmDirectors.Panel28Click(Sender: TObject);
begin
  inherited;
  If Save4(StrToInt(e41.Text)) then
    begin
      e41.Items.Add(IntToStr(e41.Items.Count+1));
      e41.ItemIndex := e41.Items.Count-1;
      e41Change(nil);
      Loading:= True;
      e46.Text:= '0';
      Loading:= False;
    end;
  e45.SetFocus;
end;

procedure TfmDirectors.Panel19Click(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  CriticalEditChange(e41);
  Loading:= True;
  if e41.Items.Count = 1 then
    begin
      e42.ItemIndex := 0;
      e43.Text := DateToLongStr(Now);
      e44.Text := DateToLongStr(Now);
      e45.Text := '';    e46.Text := '';
      e47.Text := '';    e48.Text := '';    e49.Text := '';
      e410.Text := '';
      e411.Text := '�����';

      e412.Text := '';   e413.Text := '';   e414.Text := '';
      e415.Text := '';   e416.Text := '';   e417.Text := '';
      e418.Text := '�����';
   end;

  While (TempGrid4.Cols[0].IndexOf(e41.Text)<>-1) do
    TempGrid4.LineDeleteByIndex(TempGrid4.Cols[0].IndexOf(e41.Text));

  for i:=0 to TempGrid4.RowCount-1 do
    try
      if (StrToInt(TempGrid4.Cells[0,i]) > StrToInt(e41.Text)) then
        TempGrid4.Cells[0,i] := IntToStr(StrToInt(TempGrid4.Cells[0,i])-1);
    except end;

  if (e41.ItemIndex = e41.Items.Count-1) and (e41.Text<>'1') then
    e41.ItemIndex := e41.ItemIndex-1;

  e41.Tag := StrToInt(e41.Text);
  Load4(StrToInt(e41.Text));

  If e41.Items.Count>1 then
    e41.Items.Delete(e41.Items.Count-1);
  Loading:= False;
  DATAChanged:= True;
end;

procedure TfmDirectors.FormCreate(Sender: TObject);
begin
  ShowStyle:= afActive;
  FileAction:= faDirector;
  inherited;
end;

procedure TfmDirectors.GridEnter(Sender: TObject);
var
  CanSelect: Boolean;
begin
  inherited;
  if (Grid.Row = 0) and (Grid.Col = 0) then
    GridSelectCell(Grid,0,0,CanSelect);
end;

procedure TfmDirectors.GridSelectCell(Sender: TObject; Col, Row: Integer;
  var CanSelect: Boolean);
var
  Found: Integer;
  I: Integer;
begin
  inherited;
  if (Grid.Pcol=0) then
    begin
      Found:=-1;
        for i:=0 to nGrid.RowCount-1 do
          if (Grid.Cells[0,Grid.PRow]=nGrid.Cells[0,i]) And (RemoveSpaces(nGrid.Cells[0,i],rmBoth)<>'') then
            Found:=i;
      if Found<>-1 then
        begin
          Grid.Cells[0,Grid.PRow]:=nGrid.Cells[1,Found];
          Grid.Cells[1,Grid.PRow]:=nGrid.Cells[3,Found];
          Grid.Cells[2,Grid.PRow]:=nGrid.Cells[2,Found];
        end;
    end;

  nGrid.Visible:= Col=0;
end;

procedure TfmDirectors.GridKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (key=#13) Then
    If ((Sender as TSDGrid).EditorMode=false) then
      (Sender as TSDGrid).EditorMode:=True
    Else
      nGrid.Visible:= False;
end;

procedure TfmDirectors.nGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (key = VK_RETURN) then
    nGridDblClick(Sender);

  if (key = VK_ESCAPE) then
    begin
      Grid.Col := 1;
      Grid.SetFocus;
    end;
end;

procedure TfmDirectors.nGridDblClick(Sender: TObject);
var
  VKKEY: Word;
begin
  inherited;
  VKKEY:= VK_NONAME;
  Grid.Cells[0,Grid.Row] := nGrid.Cells[1,nGrid.Row];
  Grid.Cells[1,Grid.Row] := nGrid.Cells[3,nGrid.Row];
  Grid.Cells[2,Grid.Row] := nGrid.Cells[2,nGrid.Row];
  Grid.Col := 3;
  Grid.SetFocus;
  GridKeyDown(Grid, VKKEY, []);
end;

function TfmDirectors.EvCollectData(var Directors: TDirectors): TDirectors;

  procedure CreateOldDirector(Row: Integer);
  var
    TempDir: TDirector;
  begin
    TempDir:= (Company.Directors.FindByZeut(Grid.Cells[2,Row]) as TDirector);
    TempDir.DeActivate(StrToDate(Grid.Cells[3,Row]));
    Directors.Add(TempDir);
  end;

  function CreateNewDirector(I: Integer): TDirector;
  var
    Taagid: TTaagid;
  begin
    With TempGrid4 do
      begin
        If Cells[1,I] = '1' Then
          Taagid:= TTaagid.CreateNew(Cells[14,I], Cells[15,I], Cells[16,I], Cells[17,I], Cells[18,I], Cells[12,I], Cells[13,I])
        Else
          Taagid:= Nil;

         Result:= TDirector.CreateNew(DecisionDate, Cells[4,I], Cells[5,I], StrToDate(Cells[2,I]), StrToDate(Cells[3,I]),
            TDirectorAddress.CreateNew(Cells[7,I], Cells[8,I], Cells[9,I], Cells[10,I], Cells[11,I], Cells[6,I]),
            Taagid, True);
      end;
    Directors.Add(Result);
  end;

var
  I: Integer;
begin
  Result:= TDirectors.CreateNew;
  Directors:= TDirectors.CreateNew;
  Company.Directors.Filter:= afActive;
  For I:= 0 To TempGrid4.RowCount-1 Do
    if RemoveSpaces(TempGrid4.Cells[0,I], rmBoth)<>'' then
      Result.Add(CreateNewDirector(I));
  Company.Directors.Filter:= afAll;
  For I:= 0 To Grid.RowCount-1 Do
    If RemoveSpaces(Grid.Cells[0,I],rmBoth)<>'' Then
      CreateOldDirector(I);
end;

procedure TfmDirectors.SaveOldDirectors(FileItem: TFileItem; Directors: TDirectors);
var
  I: Integer;
begin
  Directors.Filter:= afNonActive;
  For I:= 0 to Directors.Count-1 do
    With Directors.Items[I] as TDirector do
      FileItem.SaveChange(ocDirector, Trunc(LastChanged), acDelete, RecNum);
end;

function TfmDirectors.EvSaveData(Draft: Boolean): Boolean;
var
  FileItem: TFileItem;
  FileInfo: TFileInfo;
  DraftDir, NonDraftDir: TDirectors;
  FilesList: TFilesList;
begin
  Result:= False;
  try
    Save4(StrToInt(e41.Text));
    DraftDir:= EvCollectData(NonDraftDir);
    FilesList:= TFilesList.Create(Company.RecNum, faDirector, Draft);
    FileInfo:= TFileInfo.Create;
    FileInfo.Action:= faDirector;
    FileInfo.Draft:= Draft;
    FileInfo.DecisionDate:= DecisionDate;
    FileInfo.NotifyDate:= NotifyDate;
    FileInfo.CompanyNum:= Company.RecNum;
    FileItem:= fmSaveDialog.Execute(Self, FilesList, FileInfo);
    If FileItem<>nil Then
      begin
        FileItem.FileInfo.Draft:= True;
        DraftDir.ExSaveData(FileItem, Company);
        SaveOldDirectors(FileItem, NonDraftDir);
        If rb22.Checked then
          FileItem.SaveChange(ocStock, -2, acSignName, 1);
        If Not Draft Then
          NonDraftDir.SaveData(Company, FileItem.FileInfo.RecNum);
        FileName:= FileItem.FileInfo.FileName;
        FileItem.Free;
        fmDialog2.Show;
        Result:= True;
      end
    Else
      FileInfo.Free;
    DraftDir.Free;
    NonDraftDir.Deconstruct;
    FilesList.Free;
  finally
  end;
end;

procedure TfmDirectors.Load1Enum(Director: TDirector);
begin
  With Director do
    begin
      TempGrid4.RowCount:= TempGrid4.RowCount+1;
      TempGrid4.Rows[TempGrid4.RowCount-1].Clear;
      TempGrid4.Cells[0,TempGrid4.RowCount-1]:= IntToStr(TempGrid4.RowCount-1);
      e41.Items.Add(TempGrid4.Cells[0,TempGrid4.RowCount-1]);
      If Taagid<> nil Then
        TempGrid4.Cells[1,TempGrid4.RowCount-1]:= '1'
      Else
        TempGrid4.Cells[1,TempGrid4.RowCount-1]:= '0';
      TempGrid4.Cells[2,TempGrid4.RowCount-1]:= DateToLongStr(Date1);
      TempGrid4.Cells[3,TempGrid4.RowCount-1]:= DateToLongStr(Date2);
      TempGrid4.Cells[4,TempGrid4.RowCount-1]:= Name;
      TempGrid4.Cells[5,TempGrid4.RowCount-1]:= Zeut;
      TempGrid4.Cells[6,TempGrid4.RowCount-1]:= Address.Phone;
      TempGrid4.Cells[7,TempGrid4.RowCount-1]:= Address.Street;
      TempGrid4.Cells[8,TempGrid4.RowCount-1]:= Address.Number;
      TempGrid4.Cells[9,TempGrid4.RowCount-1]:= Address.Index;
      TempGrid4.Cells[10,TempGrid4.RowCount-1]:= Address.City;
      TempGrid4.Cells[11,TempGrid4.RowCount-1]:= Address.Country;
      If Taagid<>nil Then
        begin
          TempGrid4.Cells[12,TempGrid4.RowCount-1]:= Taagid.Name;
          TempGrid4.Cells[13,TempGrid4.RowCount-1]:= Taagid.Zeut;
          TempGrid4.Cells[14,TempGrid4.RowCount-1]:= Taagid.Street;
          TempGrid4.Cells[15,TempGrid4.RowCount-1]:= Taagid.Number;
          TempGrid4.Cells[16,TempGrid4.RowCount-1]:= Taagid.Index;
          TempGrid4.Cells[17,TempGrid4.RowCount-1]:= Taagid.City;
          TempGrid4.Cells[18,TempGrid4.RowCount-1]:= Taagid.Country;
        end;
    end;
end;

procedure TfmDirectors.Load2Enum(ObjectNum, NewNum: Integer);
var
  Found: Boolean;
  I: Integer;
begin
  Found:= False;
  Company.Directors.Filter:= afAll;
  For I:= 0 to Company.Directors.Count-1 do
    With (Company.Directors.Items[I] as TDirector) do
      If RecNum = NewNum Then
        begin
          Grid.Cells[0, Grid.RowCount-1]:= Name;
          If Taagid= Nil Then
            Grid.Cells[1, Grid.RowCount-1]:= ''
          Else
            Grid.Cells[1, Grid.RowCount-1]:= Taagid.Name;
          Grid.Cells[2, Grid.RowCount-1]:= Zeut;
          Grid.Cells[3, Grid.RowCount-1]:= DateToLongStr(ObjectNum);
          Grid.RowCount:= Grid.RowCount+1;
          Found:= True;
        end;
  If not Found Then
    Raise Exception.Create('Record not found at TfmDirectors.Load2Enum');
end;

function TfmDirectors.EvLoadData: Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  Directors: TDirectors;
  I: Integer;
begin
  Result:= False;
  FileItem:= fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faDirector);
  If FileItem = Nil Then
    Exit;
  DecisionDate:= (FileItem.FileInfo.DecisionDate);
  NotifyDate:= (FileItem.FileInfo.NotifyDate);
  TempGrid4.RowCount:=1;
  TempGrid4.Rows[0].Clear;
  Grid.RowCount:= 1;
  Grid.ResetLine(0);
  Directors:= TDirectors.LoadFromFile(Company, FileItem);
  e41.Items.Clear;
  For I:= Directors.Count-1 DownTo 0 Do
    Load1Enum(Directors.Items[I] as TDirector);
  Directors.Free;
  FileItem.EnumChanges(ocDirector, -1, acDelete, Load2Enum);
  If Grid.RowCount>1 Then
    Grid.RowCount:= Grid.RowCount-1;
  If FileItem.EnumChanges(ocStock, -2, acSignName, NothingEnum)=1 Then
    rb22.Checked:= True
  Else
    rb21.Checked:= True;
  If e41.Items.Count = 0 then
    e41.Items.Add('1');
  e41.ItemIndex:= 0;
  Load4(1);
  FileItem.Free;
  FilesList.Free;
  Result:= True;
end;

procedure TfmDirectors.GridLineReset(Sender: TObject; Row: Integer);
begin
  inherited;
  Grid.Cells[3,Row]:= e13.Text;
end;

procedure TfmDirectors.EvPrintData;
var
  TaagidOnly,
  Active, NonActive: TDirectors;

  procedure CollectActive;
  var
    I: Integer;
    Taagid: TTaagid;
    TempDirector: TDirector;
  begin
    With TempGrid4 do
      for I:= 0 to RowCount-1 do
        If RemoveSpaces(Cells[0,I], rmBoth)<>'' Then
          begin
            If Cells[1,I] = '1' Then
              Taagid:= TTaagid.CreateNew(Cells[14,I], Cells[15,I], Cells[16,I], Cells[17,I], Cells[18,I], Cells[12,I], Cells[13,I])
            Else
              Taagid:= Nil;

             TempDirector:= TDirector.CreateNew(DecisionDate, Cells[4,I], Cells[5,I], StrToDate(Cells[2,I]), StrToDate(Cells[3,I]),
                TDirectorAddress.CreateNew(Cells[7,I], Cells[8,I], Cells[9,I], Cells[10,I], Cells[11,I], Cells[6,I]),
                Taagid, True);

             If Taagid<> nil then
               TaagidOnly.Add(TempDirector);
             Active.Add(TempDirector);
          end;
  end;

  procedure CollectNonActive;
  var
    Row: Integer;
    TempDir: TDirector;
  begin
    Company.Directors.Filter:= afAll;
    For Row:= 0 to Grid.RowCount-1 do
      if RemoveSpaces(Grid.Cells[0,Row], rmBoth)<>'' then
        begin
          TempDir:= (Company.Directors.FindByZeut(Grid.Cells[2,Row]) as TDirector);
          TempDir.DeActivate(StrToDate(Grid.Cells[3,Row]));
          NonActive.Add(TempDir);
        end;
  end;

begin
  Application.CreateForm(TfmQRDirectors, fmQRDirectors);
  TaagidOnly:= TDirectors.CreateNew;
  Active:= TDirectors.CreateNew;
  NonActive:= TDirectors.CreateNew;
  try
    Save4(StrToInt(e41.Text));
    CollectActive;
    CollectNonActive;
    NonActive.Filter:= afNonActive;
    fmQRDirectors.PrintExecute(FileName, Company, TaagidOnly, Active, NonActive, rb21.Checked, NotifyDate);
  finally
    fmQRDirectors.Free;
    TaagidOnly.Deconstruct;
    Active.Free;
    NonActive.Deconstruct;
  end;
end;

procedure TfmDirectors.btSearchClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= TCompany.Create(Company.RecNum, Company.ShowingDate, False);
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
    begin
      e45.SetFocus;
      Exit;
    end;

  Loading:= True;
  If FResult is TStockHolder Then
    With FResult as TStockHolder do
      begin
        if Taagid Then
          begin
            e42.ItemIndex:= 1;
            e42Change(e42);
            e412.Text:= '';
            e413.Text:= '';
            e414.Text:= '';
            e415.Text:= '';
            e416.Text:= '';
            e417.Text:= '';
            e418.Text:= '�����';
          end
        Else
          begin
            e42.ItemIndex:= 0;
            e42Change(e42);
          end;
        e45.Text:= Name;
        e46.Text:= Zeut;
        e46b.Text:= '';
        e43.Text:= DateToLongStr(Now);
        e44.Text:= DateToLongStr(Now);
        e47.Text:= Address.Street;
        e48.Text:= Address.Number;
        e49.Text:= Address.Index;
        e410.Text:= Address.City;
        e411.Text:= Address.Country;
      end;

  if FResult is TDirector Then
    With FResult as TDirector do
      begin
        if Taagid<> nil Then
          begin
            e42.ItemIndex:= 1;
            e42Change(e42);
            e412.Text:= Taagid.Name;
            e413.Text:= Taagid.Zeut;
            e414.Text:= Taagid.Street;
            e415.Text:= Taagid.Number;
            e416.Text:= Taagid.Index;
            e417.Text:= Taagid.City;
            e418.Text:= Taagid.Country;
          end
        Else
          begin
            e42.ItemIndex:= 0;
            e42Change(e42);
          end;
        e45.Text:= Name;
        e46.Text:= Zeut;
        e46b.Text:= Address.Phone;
        e43.Text:= DateToLongStr(Now);
        e44.Text:= DateToLongStr(Now);
        e47.Text:= Address.Street;
        e48.Text:= Address.Number;
        e49.Text:= Address.Index;
        e410.Text:= Address.City;
        e411.Text:= Address.Country;
      end;
  TempCompany.Free;
  Loading:= False;
  DataChanged:= True;
  e45.SetFocus;
end;

procedure TfmDirectors.e45Change(Sender: TObject);
begin
  inherited;
  ChangeNameFull(TCustomEdit(e45),TCustomEdit(e46));
  CriticalEditChange(Sender);
end;
//================================
procedure TfmDirectors.e412Change(Sender: TObject);
begin
  inherited;
  ChangeNameFull(TCustomEdit(e412),TCustomEdit(e413));
  NonCriticalChange(Sender);
end;
//================================
end.
