unit PrintCompanyInfo;
{
History:
date        version    by   task     description
21/12/2020  1.8.4      sts  22057    refactoriing;
}


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, ExtCtrls, Qrctrls, Db, DBTables, DataObjects;

type
  TfmQRCompanyInfo = class(TForm)
    qrCompanyInfo: TQuickRep;
    QRSubDetail1: TQRSubDetail;
    QRDBText8: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    GroupHeaderBand1: TQRBand;
    QRLabel23: TQRLabel;
    QRLabel29: TQRLabel;
    QRShape1: TQRShape;
    QRLabel24: TQRLabel;
    QRShape2: TQRShape;
    QRLabel25: TQRLabel;
    QRShape3: TQRShape;
    QRLabel26: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape6: TQRShape;
    QRLabel30: TQRLabel;
    QRShape7: TQRShape;
    QRLabel31: TQRLabel;
    QRShape8: TQRShape;
    QRLabel32: TQRLabel;
    QRShape9: TQRShape;
    QRBand1: TQRBand;
    QRLabel3: TQRLabel;
    l1: TQRLabel;
    QRLabel4: TQRLabel;
    l2: TQRLabel;
    QRLabel5: TQRLabel;
    l4: TQRLabel;
    l5: TQRLabel;
    QRLabel8: TQRLabel;
    l6: TQRLabel;
    QRLabel10: TQRLabel;
    l9: TQRLabel;
    QRLabel12: TQRLabel;
    l8: TQRLabel;
    QRLabel14: TQRLabel;
    l10: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    l11: TQRLabel;
    l7: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    l3: TQRLabel;
    QRLabel58: TQRLabel;
    QRSubDetail2: TQRSubDetail;
    GroupHeaderBand2: TQRBand;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRShape10: TQRShape;
    QRLabel35: TQRLabel;
    QRShape11: TQRShape;
    QRLabel36: TQRLabel;
    QRShape12: TQRShape;
    QRLabel37: TQRLabel;
    QRShape13: TQRShape;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    sdStockHolders: TQRSubDetail;
    GroupHeaderBand4: TQRBand;
    QRLabel61: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel48: TQRLabel;
    QRShape14: TQRShape;
    QRLabel49: TQRLabel;
    QRShape15: TQRShape;
    QRLabel50: TQRLabel;
    QRShape16: TQRShape;
    QRLabel51: TQRLabel;
    QRShape17: TQRShape;
    GroupHeaderBand3: TQRBand;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel82: TQRLabel;
    QRSubDetail4: TQRSubDetail;
    sdDirectors: TQRSubDetail;
    QRLabel84: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel99: TQRLabel;
    QRSubDetail3: TQRSubDetail;
    GroupHeaderBand6: TQRBand;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    l12: TQRLabel;
    QRLabel101: TQRLabel;
    l13: TQRLabel;
    QRLabel103: TQRLabel;
    l14: TQRLabel;
    QRSubDetail6: TQRSubDetail;
    GroupHeaderBand7: TQRBand;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRShape18: TQRShape;
    QRLabel107: TQRLabel;
    QRShape19: TQRShape;
    QRLabel108: TQRLabel;
    QRShape20: TQRShape;
    QRLabel109: TQRLabel;
    QRShape21: TQRShape;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRSubDetail7: TQRSubDetail;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    l15: TQRLabel;
    QRLabel113: TQRLabel;
    l16: TQRLabel;
    QRLabel115: TQRLabel;
    l17: TQRLabel;
    QRLabel117: TQRLabel;
    l18: TQRLabel;
    cb1: TQRImage;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    cb2: TQRImage;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    cb3: TQRImage;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    cb4: TQRImage;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    imV: TImage;
    QRLabel128: TQRLabel;
    cb5: TQRImage;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    cb6: TQRImage;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText33: TQRDBText;
    QRDBText34: TQRDBText;
    QRDBText35: TQRDBText;
    QRDBText36: TQRDBText;
    QRDBText37: TQRDBText;
    QRDBText38: TQRDBText;
    QRDBText39: TQRDBText;
    QRDBText40: TQRDBText;
    QRDBText41: TQRDBText;
    QRDBText45: TQRDBText;
    QRDBText43: TQRDBText;
    QRDBText44: TQRDBText;
    QRDBText42: TQRDBText;
    QRDBText46: TQRDBText;
    Table1: TTable;
    Table2: TTable;
    Table3: TTable;
    Table4: TTable;
    Table5: TTable;
    Table6: TTable;
    Table7: TTable;
    QRLabel83: TQRLabel;
    QRSubDetail5: TQRSubDetail;
    QRDBText15: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText16: TQRDBText;
    GroupFooterBand1: TQRBand;
    QRShape23: TQRShape;
    l19: TQRLabel;
    QRBand2: TQRBand;
    title: TQRLabel;
    QRShape24: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel11: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    string2: TQRLabel;
    string1: TQRLabel;
    QRShape25: TQRShape;
    l1a: TQRLabel;
    procedure sdStockHoldersAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure sdDirectorsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    { Private declarations }
    procedure PrintProc(DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintExecute(const FileName: String; Company: TCompany);
  end;

var
  fmQRCompanyInfo: TfmQRCompanyInfo;

implementation

{$R *.DFM}

uses
  PrinterSetup, Util, DataM, Main, HPSConsts;

procedure TfmQRCompanyInfo.PrintProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(qrCompanyInfo);
  If DoPrint then
    qrCompanyInfo.Print
  Else
    begin
      qrCompanyInfo.Preview;
      Application.ProcessMessages;
    end;
end;

procedure TfmQRCompanyInfo.PrintExecute(const FileName: String; Company: TCompany);
var
  I, J: Integer;
begin
  String1.Caption:= fmMain.UserOptions.RoeHeshbon;
  string2.Caption:= fmMain.UserOptions.RoeAddress;
  With Table1 Do
  begin
    FieldDefs.Clear;
    FieldDefs.Add('Name', ftString, 20, False);
    FieldDefs.Add('Value', ftString,20,False);
    FieldDefs.Add('Rashum',ftString,20,False);
    FieldDefs.Add('Mokza', ftString, 20, False);
    FieldDefs.Add('Cash', ftString, 20, False);
    FieldDefs.Add('NonCash', ftString, 20, False);
    FieldDefs.Add('Part', ftString, 20, False);
    FieldDefs.Add('PartValue', ftString, 20, False);
    CreateTable;
    Open;
    For I:= 0 To Company.HonList.Count-1 do
      With Company.HonList.Items[I] as THonItem do
      begin
        Append;
        FieldByName('Name').AsString:= MakeHebStr(Name, False);
        FieldByName('Value').AsString:= FloatToStrF(Value, ffNumber, 20, 4);
        FieldByName('Rashum').AsString:= FloatToStrF(Rashum, ffNumber, 20, 2);
        FieldByName('Mokza').AsString:= FloatToStrF(Mokza, ffNumber, 20, 2);
        FieldByName('Cash').AsString:= FloatToStrF(Cash, ffNumber, 20, 2);
        FieldByName('NonCash').AsString:= FloatToStrF(NonCash, ffNumber, 20, 2);
        FieldByName('Part').AsString:= FloatToStrF(Part, ffNumber, 20, 2);
        FieldByName('PartValue').AsString:= FloatToStrF(PartValue, ffNumber, 20, 2);
        Post;
      end;
  end;

  With Table2 Do
  begin
    FieldDefs.Clear;
    FieldDefs.Add('Name', ftString, 20, False);
    FieldDefs.Add('Value', ftString,20,False);
    FieldDefs.Add('ShtarNum',ftString,20,False);
    FieldDefs.Add('ShtarCount', ftString, 20, False);
    CreateTable;
    Open;
    For I := 0 To Company.MuhazList.Count-1 do
      With Company.MuhazList.Items[I] as TMuhazItem do
      begin
        Append;
        FieldByName('Name').AsString:= MakeHebStr(Name, False);
        FieldByName('Value').AsString:= FloatToStrF(Value, ffNumber, 20, 4);
        FieldByName('ShtarNum').AsString:= MakeHebStr(Zeut, True);
        FieldByName('ShtarCount').AsString:= FloatToStrF(ShtarValue, ffNumber, 20, 2);
        Post;
      end;
  end;

  With Table3 Do
  begin
    FieldDefs.Clear;
    FieldDefs.Add('RecNum', ftAutoInc, 0, True);
    FieldDefs.Add('Name', ftString, 40, False);
    FieldDefs.Add('Zeut', ftString,10,False);
    FieldDefs.Add('Taagid',ftString,5,False);
    FieldDefs.Add('Street', ftString, 40, False);
    FieldDefs.Add('Number', ftString, 10, False);
    FieldDefs.Add('Index', ftString, 10, False);
    FieldDefs.Add('City', ftString, 30, False);
    FieldDefs.Add('Country', ftString, 30, False);
    CreateTable;
    Open;

    Table4.FieldDefs.Clear;
    Table4.FieldDefs.Add('RecNum', ftInteger, 0 , True);
    Table4.FieldDefs.Add('Name', ftString, 20, False);
    Table4.FieldDefs.Add('Value', ftString,20,False);
    Table4.FieldDefs.Add('Count',ftString, 20,False);
    Table4.FieldDefs.Add('Paid', ftString, 20, False);
    Table4.CreateTable;
    Table4.Open;

    For I:= 0 To Company.StockHolders.Count-1 do
      With Company.StockHolders.Items[I] as TStockHolder do
      begin
        Append;
        FieldByName('Name').AsString:= MakeHebStr(Name, False);
        FieldByName('Zeut').AsString:= MakeHebStr(Zeut, True);
        if Taagid Then
          FieldByName('Taagid').AsString:= '��'
        Else
          FieldByName('Taagid').AsString:= '��';
        FieldByName('Street').AsString:= MakeHebStr(Address.Street, False);
        FieldByName('Number').AsString:= MakeHebStr(Address.Number, True);
        FieldByName('Index').AsString:= MakeHebStr(Address.Index, True);
        FieldByName('City').AsString:= MakeHebStr(Address.City, False);
        FieldByName('Country').AsString:= MakeHebStr(Address.Country, False);
        Post;

        With Table4 Do
          For J := 0 To Stocks.Count - 1 do
          begin
            Append;
            FieldByName('RecNum').AsInteger:= Table3.FieldByName('RecNum').AsInteger;
            With Stocks.Items[J] as TStock do
            begin
              FieldByName('Name').AsString:= Name;
              FieldByName('Value').AsString:= FloatToStrF(Value, ffNumber, 20, 4);
              FieldByName('Count').AsString:= FloatToStrF(Count, ffNumber, 20, 2);
              FieldByName('Paid').AsString:= FloatToStrF(Paid, ffNumber, 20, 2);
            end;
            Post;
          end;
      end;
  end;

  With Table5 Do
  begin
    FieldDefs.Clear;
    FieldDefs.Add('RecNum', ftAutoInc, 0, True);
    FieldDefs.Add('Name', ftString, 40, False);
    FieldDefs.Add('Zeut', ftString, 10, False);
    FieldDefs.Add('Taagid', ftString,5,False);
    FieldDefs.Add('Tel',ftString,20,False);
    FieldDefs.Add('Date1', ftString, 10, False);
    FieldDefs.Add('Date2', ftString, 10, False);
    FieldDefs.Add('Street', ftString, 40, False);
    FieldDefs.Add('Number', ftString, 10, False);
    FieldDefs.Add('Index', ftString, 10, False);
    FieldDefs.Add('City', ftString, 30, False);
    FieldDefs.Add('Country', ftString, 30, False);
    CreateTable;
    Open;

    With Table6 Do
    begin
      FieldDefs.Clear;
      FieldDefs.Add('RecNum', ftInteger, 0, True);
      FieldDefs.Add('Name', ftString, 40, False);
      FieldDefs.Add('Zeut', ftString,10,False);
      FieldDefs.Add('Street', ftString, 40, False);
      FieldDefs.Add('Number', ftString, 10, False);
      FieldDefs.Add('Index', ftString, 10, False);
      FieldDefs.Add('City', ftString, 30, False);
      FieldDefs.Add('Country', ftString, 30, False);
      CreateTable;
      Open;
    end;

    For I:= 0 To Company.Directors.Count-1 do
      With Company.Directors.Items[I] as TDirector do
        begin
          Append;
          FieldByName('Name').AsString:= MakeHebStr(Name, False);
          FieldByName('Zeut').AsString:= MakeHebStr(Zeut, True);
          if Taagid <> nil Then
            FieldByName('Taagid').AsString:= '��'
          Else
            FieldByName('Taagid').AsString:= '��';
          FieldByName('Tel').AsString:= MakeHebStr(Address.Phone, True);
          FieldByName('Date1').AsString:= DateToLongStr(Date1);
          FieldByName('Date2').AsString:= DateToLongStr(Date2);
          FieldByName('Street').AsString:= MakeHebStr(Address.Street, False);
          FieldByName('Number').AsString:= MakeHebStr(Address.Number, True);
          FieldByName('Index').AsString:= MakeHebStr(Address.Index, True);
          FieldByName('City').AsString:= MakeHebStr(Address.City, False);
          FieldByName('Country').AsString:= MakeHebStr(Address.Country, False);
          Post;

          if Taagid<> nil Then
          begin
            Table6.Append;
            Table6.FieldByName('RecNum').AsInteger := Table5.FieldByName('RecNum').AsInteger;
            With Taagid do
            begin
              Table6.FieldByName('Name').AsString    := MakeHebStr(Name, False);
              Table6.FieldByName('Zeut').AsString    := MakeHebStr(Zeut, True);
              Table6.FieldByName('Street').AsString  := MakeHebStr(Street, False);
              Table6.FieldByName('Number').AsString  := MakeHebStr(Number, True);
              Table6.FieldByName('Index').AsString   := MakeHebStr(Index, True);
              Table6.FieldByName('City').AsString    := MakeHebStr(City, False);
              Table6.FieldByName('Country').AsString := MakeHebStr(Country, False);
            end;
            Table6.Post;
          end;
        end;
  end;

  With Table7 Do
  begin
    FieldDefs.Clear;
    FieldDefs.Add('Name', ftString, 40, False);
    FieldDefs.Add('Zeut', ftString, 10,False);
    FieldDefs.Add('Address', ftString, 80,False);
    FieldDefs.Add('Tel', ftString, 20, False);
    CreateTable;
    Open;
    Company.Managers.Filter := afActive;
    For I := 0 To Company.Managers.Count - 1 do
      With Company.Managers.Items[I] as TManager do
      begin
        Append;
        FieldByName('Name').AsString:= MakeHebStr(Name, False);
        FieldByName('Zeut').AsString:= MakeHebStr(Zeut, True);
        FieldByName('Address').AsString:= MakeHebStr(Address, True);
        FieldByName('Tel').AsString:= MakeHebStr(Phone, True);
        Post;
      end;
  end;

  If (Company.CompanyInfo.Option1 and 1) > 0 then
    cb1.Picture.Assign(imV.Picture);
  If (Company.CompanyInfo.Option1 and 2) > 0 then
    cb2.Picture.Assign(imV.Picture);
  If (Company.CompanyInfo.Option1 and 4) > 0 then
    cb3.Picture.Assign(imV.Picture);
  If (Company.CompanyInfo.Option1 and 8) > 0 then
    cb4.Picture.Assign(imV.Picture);

  If Company.CompanyInfo.Option3 = '1' then
    cb5.Picture.Assign(imV.Picture)
  else
    if Company.CompanyInfo.Option3 = '3' then  // sts17674 prev: = '2'; new code = '3'
      cb6.Picture.Assign(imV.Picture);

  l1.Caption:= Company.Name;
  l1a.Caption:= Company.Name;
  l2.Caption:= Company.Zeut;
  l3.Caption:= DateToLongStr(Company.CompanyInfo.RegistrationDate);
  l4.Caption:= Company.Address.Street;
  l5.Caption:= Company.Address.Number;
  l6.Caption:= Company.Address.Index;
  l7.Caption:= Company.Address.City;
  l8.Caption:= Company.Address.Ezel;
  l9.Caption:= Company.Address.POB;
  l10.Caption:= Company.Address.Phone;
  l11.Caption:= Company.Address.Fax;
  l12.Caption:= Company.CompanyInfo.SigName;
  l13.Caption:= Company.CompanyInfo.SigZeut;
  l14.Caption:= Company.CompanyInfo.SigTafkid;
  l15.Caption:= Company.CompanyInfo.LawName;
  l16.Caption:= Company.CompanyInfo.LawZeut;
  l17.Caption:= Company.CompanyInfo.LawAddress;
  l18.Caption:= Company.CompanyInfo.LawLicence;

  if Company.CompanyInfo.Option3 = '3' then   // sts17674 - > do not print orderinfo if not appropriate option
    l19.Caption:= Company.CompanyInfo.OtherInfo
  else
    l19.Caption:= '';

    //==>Tsahi 26.7.2006: Change of phone number
  QRLabel9.Caption := '��. '+HPSPhone;
  QRLabel11.Caption := '���.  '+HPSFax;
  QRLabel7.Caption := '';
  QRLabel6.Caption := HPSName;
  //<==

  fmPrinterSetup.Execute(FileName, PrintProc);
  Table1.Close;
  Table2.Close;
  Table3.Close;
  Table4.Close;
  Table5.Close;
  Table6.Close;
  Table7.Close;
end;

procedure TfmQRCompanyInfo.sdStockHoldersAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  Table4.Filter := '([RecNum] =''' + Table3.FieldByName('RecNum').AsString + ''')';
  Table4.Filtered := True;
end;

procedure TfmQRCompanyInfo.sdDirectorsAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  QRShape25.Pen.Color:= clBlack;
  Table6.Filter := '([RecNum] =''' + Table5.FieldByName('RecNum').AsString + ''')';
  Table6.Filtered := True;
end;

end.
