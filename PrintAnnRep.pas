{ QuickReport mailing labels form }

unit PrintAnnRep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, quickrpt, DB, DBTables, ExtCtrls, DataObjects, memdset;

type
  TQRAnnualReport = class(TQuickRep)
    QRBand1: TQRBand;
    QRImage4: TQRImage;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRImage3: TQRImage;
    QRLabel6: TQRLabel;
    lblDate: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    lblMailB: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel28: TQRLabel;
    edtCompanyName: TQRLabel;
    edtCompanyAddress: TQRLabel;
    edtCompanyPhone: TQRLabel;
    edtCompanyMail: TQRLabel;
    edtCompanyNumber: TQRLabel;
    edtReportDate: TQRLabel;
    edtAsefaDate: TQRLabel;
    rectHon: TQRShape;
    vlHonA: TQRShape;
    hlHon: TQRShape;
    lblSakhHon: TQRLabel;
    edtSakhHon: TQRDBText;
    lblStockNameA: TQRLabel;
    lblStockNameB: TQRLabel;
    edtStockName: TQRDBText;
    lblStockType: TQRLabel;
    edtStockType: TQRDBText;
    lblStocksNum: TQRLabel;
    edtStocksNum: TQRDBText;
    lblmukzot: TQRLabel;
    edtMukzot: TQRDBText;
    lblStockValue: TQRLabel;
    edtStockValue: TQRDBText;
    vlHonB: TQRShape;
    taMemHon: TMemDataSet;
    taMemHonSakhHon: TCurrencyField;
    taMemHonStockName: TStringField;
    taMemHonStockType: TStringField;
    taMemHonStocksNum: TFloatField;
    taMemHonMukzot: TFloatField;
    taMemHonStockValue: TCurrencyField;
    QRBand2: TQRBand;
    taMemStocks: TMemDataSet;
    taMemStocksSumNotPaid: TCurrencyField;
    taMemStocksAddress: TStringField;
    taMemStocksStocksNum: TFloatField;
    taMemStocksid: TStringField;
    taMemStocksStocksType: TStringField;
    taMemStocksStockHolderName: TStringField;
    QRBand3: TQRBand;
    lblStocksTitle:TQRLabel;
    rectStocks: TQRShape;
    vlStocksA: TQRShape;
    vlStocksB: TQRShape;
    lblStockHolderName: TQRLabel;
    edtStockHolderName: TQRDBText;
    lblStockTypeA: TQRLabel;
    edtStockTypeA: TQRDBText;
    lblID: TQRLabel;
    edtID: TQRDBText;
    lblStocksNumA: TQRLabel;
    edtStocksNumA: TQRDBText;
    lblAddressA: TQRLabel;
    lblAddressB: TQRLabel;
    edtAddressA: TQRDBText;
    lblSumNotPaid: TQRLabel;
    edtSumNotPaid: TQRDBText;
    QRSubDetail1:TQRSubDetail;
    QRSubDetail2:TQRSubDetail;
    QRSubDetail3: TQRSubDetail;
    QRShape7: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRSubDetail4: TQRSubDetail;
    QRLabel26: TQRLabel;
    QRLabel31: TQRLabel;
    QRMemo1: TQRMemo;
    QRBand4: TQRBand;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRSubDetail5: TQRSubDetail;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    edtMisparMenayot: TQRDBText;
    edtMisparShtar: TQRDBText;
    edtSakhMenayotLamokaz: TQRLabel;
    taMemMokaz: TMemDataSet;
    taMemMokazShtarValue: TFloatField;
    taMemMokazValue: TFloatField;
    QRBand5: TQRBand;
    QRLabel36: TQRLabel;
    QRSubDetail6: TQRSubDetail;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    taMemDirectors: TMemDataSet;
    edtDirectorName: TQRDBText;
    edtDirectorID: TQRDBText;
    edtDirectorDate: TQRDBText;
    edtDirectorAddress: TQRDBText;
    taMemDirectorsAddress: TStringField;
    taMemDirectorsStartDate: TDateTimeField;
    taMemDirectorsName: TStringField;
    taMemDirectorsZeut: TStringField;
    procedure QRBand1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRSubDetail1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBand3AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRSubDetail2AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail3AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRSubDetail3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail4AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRSubDetail5AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRSubDetail6AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    { Private declarations }
    PrintedOnPage:integer;
    detail3printed,detail4printed:boolean;
    Company:TCompany;
    procedure LoadData;
    procedure CreateMemTables;
  public
    { Public declarations }
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintExecute(const FileName: string; ACompany: TCompany);
  end;

var
  QRAnnualReport: TQRAnnualReport;

implementation

uses PrinterSetup;

{$R *.DFM}

procedure TQRAnnualReport.CreateMemTables;
var i,j:integer;
begin
  taMemHon.Close;
  taMemHon.EmptyTable;
  taMemHon.Open;
  for i:=0 to Company.HonList.Count-1 do
    begin
      taMemHon.Append;
      taMemHon.FieldByName('SakhHon').AsFloat:=THonItem(Company.HonList.Items[i]).Rashum*THonItem(Company.HonList.Items[i]).Value;
      taMemHon.FieldByName('StockName').AsString:=Company.HonList.Items[i].Zeut;
      taMemHon.FieldByName('StockType').AsString:=Company.HonList.Items[i].Name;
      taMemHon.FieldByName('StocksNum').AsFloat:=THonItem(Company.HonList.Items[i]).Rashum;
      taMemHon.FieldByName('Mukzot').AsFloat:=THonItem(Company.HonList.Items[i]).Mokza;
      taMemHon.FieldByName('StockValue').AsFloat:= THonItem(Company.HonList.Items[i]).Value;
      taMemHon.Post;
    end;
  taMemStocks.Close;
  taMemStocks.EmptyTable;
  taMemStocks.Open;
  for i:=0 to Company.StockHolders.Count-1 do
    begin
      for j:=0 to TStockHolder(Company.StockHolders.Items[i]).Stocks.Count-1 do
        begin
          taMemStocks.Append;
          taMemStocks.FieldByName('StockHolderName').AsString:=Company.StockHolders.Items[i].Name;
          taMemStocks.FieldByName('id').AsString:=Company.StockHolders.Items[i].Zeut;
          taMemStocks.FieldByName('Address').AsString:=TStockHolder(Company.StockHolders.Items[i]).Address.Street+' '+
                      TStockHolder(Company.StockHolders.Items[i]).Address.Number+' '+
                      TStockHolder(Company.StockHolders.Items[i]).Address.City+' '+
                      TStockHolder(Company.StockHolders.Items[i]).Address.Index+' '+
                      TStockHolder(Company.StockHolders.Items[i]).Address.Country;
          taMemStocks.FieldByName('StocksType').AsString:=
             TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Name;
          taMemStocks.FieldByName('StocksNum').AsFloat:=
             TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count;
          taMemStocks.FieldByName('SumNotPaid').AsFloat:=
             TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Value-
                                      TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Paid;
          taMemStocks.Post;
        end;
    end;
   taMemMokaz.Close;
   taMemMokaz.EmptyTable;
   taMemMokaz.Open;
   for i:=0 to Company.MuhazList.Count-1 do
     begin
       taMemMokaz.Append;
       taMemMokaz.FieldByName('Value').AsFloat:=TMuhazItem(Company.MuhazList.Items[i]).Value;
       taMemMokaz.FieldByName('ShtarValue').AsFloat:=TMuhazItem(Company.MuhazList.Items[i]).ShtarValue;
       taMemMokaz.Post;
     end;
   taMemDirectors.Close;
   taMemDirectors.EmptyTable;
   taMemDirectors.Open;
   for i:=0 to Company.Directors.Count-1 do
     begin
       if (TDirector(Company.Directors.Items[i]).Date2<=EncodeDate(1899,12,31))or(TDirector(Company.Directors.Items[i]).Date2>Now)then
         begin
           taMemDirectors.Append;
           taMemDirectors.FieldByName('Zeut').AsString:=Company.Directors.Items[i].Zeut;
           taMemDirectors.FieldByName('Name').AsString:=Company.Directors.Items[i].Name;
           taMemDirectors.FieldByName('StartDate').AsDateTime:=TDirector(Company.Directors.Items[i]).Date1;
           taMemDirectors.FieldByName('Address').AsString:=TDirector(Company.Directors.Items[i]).Address.City+' '+
                                                           TDirector(Company.Directors.Items[i]).Address.Street+' '+
                                                           TDirector(Company.Directors.Items[i]).Address.Number+' '+
                                                           TDirector(Company.Directors.Items[i]).Address.Index;
           taMemDirectors.Post;
         end;
     end;
end;


procedure TQRAnnualReport.LoadData;
var
  i:integer;
  sum:double;
begin
  lblDate.Caption:=DateToStr(Now);
  if Company=nil then exit;
  edtCompanyName.Caption:=Company.Name;
  edtCompanyNumber.Caption:=Company.Zeut;
  edtCompanyAddress.Caption:=Company.CompanyInfo.LawAddress;
  edtCompanyPhone.Caption:=Company.Address.Phone;
  edtCompanyMail.Caption:=Company.CompanyInfo.Mail;
  edtReportDate.Caption:=DateToStr(Now);
  edtAsefaDate.Caption:=DateToStr(Company.ShowingDate);
  sum:=0;
  for i:=0 to Company.MuhazList.Count-1 do
    sum:=sum+TMuhazItem(Company.MuhazList.Items[i]).Value;
  edtSakhMenayotLamokaz.Caption:=FloatToStr(sum);
end;

procedure TQRAnnualReport.PrintProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(Self);
      If DoPrint then
      begin
        Self.Print;
      end
      Else
      begin
        Self.Preview;
        Application.ProcessMessages;
      end;
end;


procedure TQRAnnualReport.PrintExecute(const FileName: string; ACompany: TCompany);
begin
   Company:=ACompany;
   CreateMemTables;
   LoadData;
   fmPrinterSetup.Execute(FileName, PrintProc);
end;


procedure TQRAnnualReport.QRBand1AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if BandPrinted then PrintedOnPage:=PrintedOnPage+QRBand1.Height;
end;

procedure TQRAnnualReport.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintedOnPage:=0;
  detail3printed:=False;
  detail4printed:=False;
end;

procedure TQRAnnualReport.QRBand2AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if BandPrinted then
    PrintedOnPage:=PrintedOnPage+QRBand2.Height;
end;

procedure TQRAnnualReport.QRSubDetail1AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if BandPrinted then
    PrintedOnPage:=PrintedOnPage+QRSubDetail1.Height;
end;

procedure TQRAnnualReport.QRBand3AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if BandPrinted then
    PrintedOnPage:=PrintedOnPage+QRBand3.Height;
end;

procedure TQRAnnualReport.QRSubDetail2AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if BandPrinted then
    PrintedOnPage:=PrintedOnPage+QRSubDetail2.Height;
end;

procedure TQRAnnualReport.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if PrintedOnPage+QRSubDetail1.Height+QRBand2.Height>self.Page.Length then
    begin
      self.NewPage;
      PrintedOnPage:=0;
    end;
end;

procedure TQRAnnualReport.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if PrintedOnPage+QRSubDetail2.Height>self.Page.Length then
    begin
      self.NewPage;
      PrintedOnPage:=0;
    end;
end;

procedure TQRAnnualReport.QRSubDetail3AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if not BandPrinted then exit;
  detail3printed:=True;
  PrintedOnPage:=PrintedOnPage+QRSubDetail3.Height;
end;

procedure TQRAnnualReport.QRSubDetail3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand:=not detail3printed;
end;

procedure TQRAnnualReport.QRSubDetail4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
    PrintBand:=(not detail4printed)and(Company.MuhazList.Count>0);
end;

procedure TQRAnnualReport.QRSubDetail4AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if not BandPrinted then exit;
  detail4printed:=True;
  PrintedOnPage:=PrintedOnPage+QRSubDetail4.Height;
end;

procedure TQRAnnualReport.QRSubDetail5AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if edtSakhMenayotLamokaz.Enabled then edtSakhMenayotLamokaz.Enabled:=False;
  if BandPrinted then
    PrintedOnPage:=PrintedOnPage+QRSubDetail5.Height;
end;

procedure TQRAnnualReport.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand:=(Company.MuhazList.Count>0);
end;

procedure TQRAnnualReport.QRSubDetail5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand:=(Company.MuhazList.Count>0);
  if not PrintBand then exit;
  if PrintedOnPage+QRSubDetail5.Height<=Page.Length then exit;
  PrintedOnPage:=0;
  self.NewPage;
end;

procedure TQRAnnualReport.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand:=(taMemDirectors.RecordCount>0);
  if not PrintBand then exit;
  if PrintedOnPage+QRBand5.Height+QRSubDetail6.Height<=Page.Length then exit;
  PrintedOnPage:=0;
  self.NewPage;
end;

procedure TQRAnnualReport.QRSubDetail6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand:=(taMemDirectors.RecordCount>0);
  if not PrintBand then exit;
  if PrintedOnPage+QRSubDetail6.Height<=Page.Length then exit;
  PrintedOnPage:=0;
  self.NewPage;
end;

procedure TQRAnnualReport.QRBand5AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if BandPrinted then PrintedOnPage:=PrintedOnPage+QRBand5.Height;
end;

procedure TQRAnnualReport.QRSubDetail6AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if BandPrinted then PrintedOnPage:=PrintedOnPage+QRSubDetail6.Height;
end;

end.




