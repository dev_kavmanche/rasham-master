unit PrintCompanies;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls, Db, DBTables;

type
  TfmQRCompanies = class(TForm)
    qrCompanies: TQuickRep;
    taCompanies: TTable;
    QRBand1: TQRBand;
    lZeut: TQRLabel;
    lName: TQRLabel;
    QRBand2: TQRBand;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    title: TQRLabel;
    QRShape24: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel11: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    string2: TQRLabel;
    string1: TQRLabel;
    procedure QRDBText10Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    procedure PrintProc(DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintExecute(const Label1, Label2: String);
  end;

var
  fmQRCompanies: TfmQRCompanies;

implementation

{$R *.DFM}

uses Util, PrinterSetup, DataM, Main, HPSConsts;

procedure TfmQRCompanies.PrintExecute(const Label1, Label2: String);
begin
  String1.Caption:= fmMain.UserOptions.RoeHeshbon;
  string2.Caption:= fmMain.UserOptions.RoeAddress;
  lName.Caption:= Label1;
  lZeut.Caption:= Label2;

  //==>Tsahi 26.7.2006: Change of phone number
  QRLabel9.Caption := '��. '+HPSPhone;
  QRLabel11.Caption := '���.  '+HPSFax;
  QRLabel7.Caption := '';
  QRLabel6.Caption := HPSName;
  //<==

  fmPrinterSetup.Execute('����� ������', PrintProc);
end;

procedure TfmQRCompanies.PrintProc(DoPrint: Boolean);
begin
  taCompanies.Active:= True;
  fmPrinterSetup.FormatQR(qrCompanies);
  If DoPrint then
    qrCompanies.Print
  Else
    begin
      qrCompanies.Preview;
      Application.ProcessMessages;
    end;
end;

procedure TfmQRCompanies.QRDBText10Print(sender: TObject;
  var Value: String);
begin
 Value:=' '+Value+' ';
end;

end.
