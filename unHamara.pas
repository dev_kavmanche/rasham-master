unit unHamara;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HGrids, SdGrid, HRadio, HGroupBx, HComboBx, DataObjects, Util;

type
  TSaveInfo = Record
    FDraftHon: THonList;
    FDraftStockHolders: TStockHolders;
    FDraftMuhaz: TMuhazList;
    FNonDraftHonNew, FNonDraftHonOld: THonList;
    FNonDraftStocksAndHolders: TList;
    FNonDraftMuhazOld, FNonDraftMuhazNew: TMuhazList;
  end;

  TfmHamara = class(TfmBlankTemplate)
    TabSheet2: TTabSheet;
    Shape10: TShape;
    HebLabel6: THebLabel;
    Grid3: TSdGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    btReset: TButton;
    Shape8: TShape;
    HebLabel20: THebLabel;
    HebLabel21: THebLabel;
    HebLabel22: THebLabel;
    HebLabel23: THebLabel;
    edSourceCount: TEdit;
    edDestCount: TEdit;
    cbDestName: THebComboBox;
    cbDestValue: THebComboBox;
    cbSourceName: THebComboBox;
    HebLabel4: THebLabel;
    HebLabel5: THebLabel;
    HebLabel7: THebLabel;
    rbAll: THebRadioButton;
    rbChoose: THebRadioButton;
    cbSourceValue: THebComboBox;
    HebLabel8: THebLabel;
    edCount: TEdit;
    Shape5: TShape;
    Shape6: TShape;
    Shape7: TShape;
    HebLabel3: THebLabel;
    Panel4: TPanel;
    HebLabel9: THebLabel;
    edReset: TEdit;
    HebLabel10: THebLabel;
    procedure btResetClick(Sender: TObject);
    procedure Grid3SelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure rbChooseClick(Sender: TObject);
    procedure cbSourceNameChange(Sender: TObject);
    procedure cbDestNameChange(Sender: TObject);
    procedure edCountKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }

    // EvCollectData
    procedure SetAmount(Amount: Double);
    function EvCollectData(Draft: Boolean): TSaveInfo;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    procedure EvDataLoaded; override;
    procedure EvFillGrid3(Amount: Double);
    procedure EvFocusNext(Sender: TObject); override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    function EvLoadData: Boolean; override;
    procedure EvFillSugList;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmHamara: TfmHamara;

implementation

uses DataM, SaveDialog, dialog2, LoadDialog;

{$R *.DFM}

type
  TStocksAndHolder = class
    StockHolder: TStockHolder;
    Stocks: TStocks;
  end;

const
  cLoMukza = '��� �� �����';
  cMuhaz = '��� ���"� ����';

procedure TfmHamara.DeleteFile(FileItem: TFileItem);
var
  SaveInfo: TSaveInfo;
begin
  with SaveInfo do
    begin
      If Not FileItem.FileInfo.Draft Then
        Raise Exception.Create('This file is not Draft at TfmHamara.DeleteFile');
      If (FileItem.FileInfo.Action <> faHamara) Then
        raise Exception.Create('Invalid file type at TfmHamara.DeleteFile');
      FDraftStockHolders:= TStockHolders.LoadFromFile(Company, FileItem);
      FDraftStockHolders.FileDelete(FileItem);
      FDraftStockHolders.Free;
      FDraftHon:= THonList.LoadFromFile(Company, FileItem);
      FDraftHon.FileDelete(FileItem);
      FDraftHon.Free;
      FDraftMuhaz:= TMuhazList.LoadFromFile(Company, FileItem);
      FDraftMuhaz.FileDelete(FileItem);
      FDraftMuhaz.Free;
      FileItem.DeleteFile;
    end;
end;

function TfmHamara.EvSaveData(Draft: Boolean): Boolean;
var
  FileItem: TFileItem;
  FileInfo: TFileInfo;
  FilesList: TFilesList;
  I,J: Integer;
begin
  Result:= False;
  try
  with EvCollectData(Draft) do
    begin
      try
        FilesList:= TFilesList.Create(Company.RecNum, faHamara, Draft);
        FileInfo:= TFileInfo.Create;
        FileInfo.Action:= faHamara;
        FileInfo.Draft:= Draft;
        FileInfo.DecisionDate:= DecisionDate;
        FileInfo.NotifyDate:= NotifyDate;
        FileInfo.CompanyNum:= Company.RecNum;
        FileItem:= fmSaveDialog.Execute(Self, FilesList, FileInfo);
        If FileItem<> Nil then
          begin
            FileName:= FileItem.FileInfo.FileName;
            FileItem.FileInfo.Draft:= True;
            FDraftHon.ExSaveData(FileItem, Company);
            FDraftMuhaz.ExSaveData(FileItem, Company);
            FDraftStockHolders.ExSaveData(FileItem, Company);
            if not Draft Then
              begin
                FNonDraftHonNew.SaveData(Company, FileItem.FileInfo.RecNum);
                FNonDraftHonOld.SaveData(Company, FileItem.FileInfo.RecNum);
                FNonDraftMuhazNew.SaveData(Company, FileItem.FileInfo.RecNum);
                FNonDraftMuhazOld.SaveData(Company, FileItem.FileInfo.RecNum);
                for I:= 0 to FNonDraftStocksAndHolders.Count-1 do
                  with TStocksAndHolder(FNonDraftStocksAndHolders[I]) do
                    begin
                      Stocks.SaveData(StockHolder, FileItem.FileInfo.RecNum);
                    end;
              end;
            FileItem.Free;
            fmDialog2.Show;
            Result:= True;
          end
        Else
          FileInfo.Free;
      finally
        FDraftHon.Free;
        FDraftStockHolders.Free;
        FDraftMuhaz.Free;
        if Not Draft then
          begin
            FNonDraftHonNew.Free;
            FNonDraftHonOld.Deconstruct;
            FNonDraftMuhazOld.Deconstruct;
            FNonDraftMuhazNew.Free;
            for I:= 0 to FNonDraftStocksAndHolders.Count-1 do
              with TStocksAndHolder(FNonDraftStocksAndHolders[I]) do
                begin
                  for J:= 0 to Stocks.Count-1 do
                    if Stocks.Items[J].Active then
                      Stocks.Items[J].Free;
                  Stocks.Deconstruct;
                end;
            FNonDraftStocksAndHolders.Free;
          end;
          FilesList.Free;
      end;
    end;
  finally
  end;
end;

function TfmHamara.EvLoadData: Boolean;

  procedure AddToGrid3(const Name, Zeut: String; Value: Double);
  var
    Row: Integer;
    I: Integer;
  begin
    Row:= -1;
    for I:= 0 to Grid3.RowCount-1 do
      if (Grid3.Cells[0,I] = Name) and (Grid3.Cells[1,I] = Zeut) then
        begin
          Row:= I;
          Break;
        end;
    if Row=-1 then
      begin
        Row:= Grid3.RowCount;
        Grid3.RowCount:= Row+1;
        Grid3.Cells[0,Row]:= Name;
        Grid3.Cells[1,Row]:= Zeut;
        Grid3.Cells[2,Row]:= '0.00';
      end;
    Grid3.Cells[3,Row]:= FormatNumber(FloatToStr(Value),15,2,True);
  end;

var
  FileItem: TFileItem;
  I: Integer;
  LoMukza: Double;
  FilesList: TFilesList;
  SaveInfo: TSaveInfo;
begin
  Result:= False;
  FileItem:= fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faHamara);
  if FileItem = nil then
    Exit;
  with SaveInfo do
    begin
      DecisionDate:= (FileItem.FileInfo.DecisionDate);
      NotifyDate:= (FileItem.FileInfo.NotifyDate);
      FDraftHon:= THonList.LoadFromFile(Company, FileItem);
      FDraftMuhaz:= TMuhazList.LoadFromFile(Company, FileItem);
      FDraftStockHolders:= TStockHolders.LoadFromFile(Company, FileItem);
      FDraftHon.Filter:= afAll;
      try
        LoMukza:= 0;
        for I:= 0 to FDraftHon.Count-1 do
          with FDraftHon.Items[I] as THonItem do
            begin
              if Rashum=2 then
                begin
                  cbDestName.Text:= Name;
                  cbDestValue.Text:= FormatNumber(FloatToStr(Value), 15,4,True);
                end
              else
                begin
                  cbSourceName.ItemIndex:= cbSourceName.Items.IndexOf(Name);
                  if cbSourceName.ItemIndex=-1 then
                    cbSourceName.ItemIndex:= cbSourceName.Items.Add(Name);
                  cbSourceValue.ItemIndex:= cbSourceValue.Items.IndexOf(FormatNumber(FloatToStr(Value), 15,4,True));
                  if cbSourceValue.ItemIndex=-1 then
                    cbSourceValue.ItemIndex:= cbSourceValue.Items.Add(FormatNumber(FloatToStr(Value), 15,4,True));
                  LoMukza:= Mokza;
                  edSourceCount.Text:= FormatNumber(FloatToStr(Cash), 15,4,True);
                  edDestCount.Text:= FormatNumber(FloatToStr(NonCash), 15,4,True);
                  if Part=-1 then
                    begin
                      rbChoose.Checked:= True;
                      edCount.Text:= '100';
                    end
                  else
                    begin
                      rbAll.Checked:= True;
                      edCount.Text:= FloatToStr(Part);
                    end;
                  rbChooseClick(rbChoose);
                end;
            end;

        if rbChoose.Checked then
          begin
            EvFillGrid3(0);
            AddToGrid3(cLoMukza,'', LoMukza);
            for I:=0 to FDraftStockHolders.Count-1 do
              with FDraftStockHolders.Items[I] as TStockHolder do
                AddToGrid3(Name, Zeut, (Stocks.Items[0] as TStock).Count);

            for I:= 0 to FDraftMuhaz.Count-1 do
              AddToGrid3(cMuhaz,(FDraftMuhaz.Items[I] as TMuhazItem).Zeut, 1);

          end
        else
          EvFillGrid3(MyStrToFloat(edCount.Text));
      finally
        FDraftHon.Free;
        FDraftMuhaz.Free;
        FDraftStockHolders.Free;
      end;
    end;
  PageIndex:=0;
  FileItem.Free;
  FilesList.Free;
  Result:= True;
end;

function TfmHamara.EvCollectData(Draft: Boolean): TSaveInfo;
var
  SugName: String;
  SugValue: Double;
  HonItemSourceOld, HonItemSourceNew, HonItemDestOld: THonItem;
  TempPart: Double;
  Hamara: Double;
  LoMukza: Double;
  Stocks: TStocks;
  CountAll: Double;
  ChangeAmount: Double;
  MuhazZeut: String;
  I: Integer;
  MuhazItem: TMuhazItem;
  StockHolder: TStockHolder;
  StockSourceOld, StockSourceNew, StockDestOld: TStock;
  StocksAndHolder: TStocksAndHolder;
begin
  with Result do
    begin
      if rbAll.Checked then
        EvFillGrid3(MyStrToFloat(edCount.Text));
      SugName:= cbSourceName.Text;
      SugValue:= MyStrToFloat(cbSourceValue.Text);
      HonItemSourceOld:= Company.HonList.FindHonItem(SugName, SugValue);
      Hamara:= MyStrToFloat(edDestCount.Text) / MyStrToFloat(edSourceCount.Text);
      FDraftHon:= THonList.CreateNew;
      FDraftMuhaz:= TMuhazList.CreateNew;
      FDraftStockHolders:= TStockHolders.CreateNew;
      if not Draft then
        begin
          FNonDraftHonNew:= THonList.CreateNew;
          FNonDraftHonOld:= THonList.CreateNew;
          FNonDraftStocksAndHolders:= TList.Create;
          FNonDraftMuhazNew:= TMuhazList.CreateNew;
          FNonDraftMuhazOld:= TMuhazList.CreateNew;
        end;
      LoMukza:= 0;
      CountAll:= 0;
      for I:= 0 to Grid3.RowCount-1 do
        begin
          ChangeAmount:=MyStrToFloat(Grid3.Cells[3,I]);
          if ChangeAmount >0 then
            begin
              if Grid3.Cells[0,I]=cLoMukza then
                begin
                  LoMukza:= ChangeAmount;
                  CountAll:= CountAll+ChangeAmount;
                end
              else
                if Grid3.Cells[0,I]=cMuhaz then
                  begin
                    MuhazZeut:= Grid3.Cells[1,I];
                    FDraftMuhaz.Add(TMuhazItem.CreateNew(DecisionDate, SugName, MuhazZeut, SugValue, 1, True));
                    if not Draft then
                      begin
                        MuhazItem:= Company.MuhazList.FindByZeut(MuhazZeut) as TMuhazItem;
                        MuhazItem.DeActivate(DecisionDate);
                        CountAll:= CountAll+MuhazItem.ShtarValue;
                        FNonDraftMuhazOld.Add(MuhazItem);
                        FNonDraftMuhazNew.Add(TMuhazItem.CreateNew(DecisionDate, cbDestName.Text, MuhazZeut, MyStrToFloat(cbDestValue.Text), MuhazItem.ShtarValue * Hamara , True));
                      end;
                  end
                else
                  begin
                    CountAll:= CountAll+ChangeAmount;
                    Stocks:= TStocks.CreateNew;
                    Stocks.Add(TStock.CreateNew(DecisionDate, SugName, SugValue, MyStrToFloat(Grid3.Cells[3,I]), 0, True));
                    FDraftStockHolders.Add(TStockHolder.CreateNew(DecisionDate, Grid3.Cells[0,I], Grid3.Cells[1,I], False, TStockHolderAddress.CreateNew('','','','',''), Stocks, True));
                    if not Draft then
                      begin
                        StockHolder:= Company.StockHolders.FindByZeut(Grid3.Cells[1,I]) as TStockHolder;
                        StocksAndHolder:= TStocksAndHolder.Create;
                        StocksAndHolder.Stocks:= TStocks.CreateNew;
                        StocksAndHolder.StockHolder:= StockHolder;
                        StockSourceOld:= StockHolder.Stocks.FindStock(SugName, SugValue);
                        StockSourceOld.DeActivate(DecisionDate);
                        StocksAndHolder.Stocks.Add(StockSourceOld);
                        if StockSourceOld.Count > ChangeAmount then
                          StockSourceNew:= TStock.CreateNew(DecisionDate, StockSourceOld.Name, StockSourceOld.Value, StockSourceOld.Count-ChangeAmount, StockSourceOld.Paid, True)
                        else
                          StockSourceNew:= nil;

                        StockDestOld:= StockHolder.Stocks.FindStock(cbDestName.Text, MyStrToFloat(cbDestValue.Text));
                        if Assigned(StockDestOld) then
                          if StockDestOld = StockSourceOld then
                            if Assigned(StockSourceNew) then // not changing 100% of stocks
                              begin
                                StocksAndHolder.Stocks.Add(TStock.CreateNew(DecisionDate, StockDestOld.Name, StockDestOld.Value, StockSourceNew.Count + ChangeAmount*Hamara, StockDestOld.Paid, True));
                                StockSourceNew:= nil;
                              end
                            else
                              StocksAndHolder.Stocks.Add(TStock.CreateNew(DecisionDate, StockDestOld.Name, StockDestOld.Value, ChangeAmount*Hamara, StockDestOld.Paid, True))
                          else
                            begin
                              StockDestOld.DeActivate(DecisionDate);
                              StocksAndHolder.Stocks.Add(StockDestOld);
                              StocksAndHolder.Stocks.Add(TStock.CreateNew(DecisionDate, StockDestOld.Name, StockDestOld.Value, StockDestOld.Count+ChangeAmount*Hamara, StockDestOld.Paid, True));
                            end
                        else
                          begin
                            StocksAndHolder.Stocks.Add(TStock.CreateNew(DecisionDate, cbDestName.Text, MyStrToFloat(cbDestValue.Text), ChangeAmount*Hamara, 0, True));
                          end;

                        if Assigned(StockSourceNew) then
                          StocksAndHolder.Stocks.Add(StockSourceNew);
                        FNonDraftStocksAndHolders.Add(StocksAndHolder);
                      end;
                  end;
            end;
        end;
      if rbAll.Checked then
        TempPart:= MyStrToFloat(edCount.Text)
      else
        TempPart:=-1;
      FDraftHon.Add(THonItem.CreateNew(DecisionDate, SugName, SugValue, 1, LoMukza, MyStrToFloat(edSourceCount.Text), MyStrToFloat(edDestCount.Text), TempPart, 0, True));
      FDraftHon.Add(THonItem.CreateNew(DecisionDate, cbDestName.Text, MyStrToFloat(cbDestValue.Text), 2, 0,0,0,0,0, True));
      if not Draft then
        begin
          HonItemSourceOld.DeActivate(DecisionDate);
          FNonDraftHonOld.Add(HonItemSourceOld);
          if HonItemSourceOld.Rashum> CountAll then
            HonItemSourceNew:= THonItem.CreateNew(DecisionDate, HonItemSourceOld.Name, HonItemSourceOld.Value, HonItemSourceOld.Rashum-CountAll,
                        HonItemSourceOld.Mokza-CountAll+LoMukza, HonItemSourceOld.Cash, HonItemSourceOld.NonCash, HonItemSourceOld.Part, HonItemSourceOld.PartValue, True)
          else
            HonItemSourceNew:= nil;
          HonItemDestOld:= Company.HonList.FindHonItem(cbDestName.Text, MyStrToFloat(cbDestValue.Text));
          if Assigned(HonItemDestOld) then
            if HonItemDestOld = HonItemSourceOld then   // If Dest and Source are same
              if Assigned(HonItemSourceNew) then  // Not changing 100%
                begin
                  FNonDraftHonNew.Add(THonItem.CreateNew(DecisionDate, HonItemSourceNew.Name, HonItemSourceNew.Value, HonItemSourceNew.Rashum+CountAll*Hamara, HonItemSourceNew.Mokza+ (CountAll-LoMukza)*Hamara, HonItemSourceNew.Cash, HonItemSourceNew.NonCash, HonItemSourceNew.Part, HonItemSourceNew.PartValue, True));
                  HonItemSourceNew:= nil;
                end
              else
                FNonDraftHonNew.Add(THonItem.CreateNew(DecisionDate, HonItemSourceOld.Name, HonItemSourceOld.Value, CountAll*Hamara, (CountAll-LoMukza)*Hamara, HonItemSourceOld.Cash, HonItemSourceOld.NonCash, HonItemSourceOld.Part, HonItemSourceOld.PartValue, True))
            else
              begin
                HonItemDestOld.DeActivate(DecisionDate);
                FNonDraftHonOld.Add(HonItemDestOld);
                FNonDraftHonNew.Add(THonItem.CreateNew(DecisionDate, HonItemDestOld.Name, HonItemDestOld.Value, HonItemDestOld.Rashum+CountAll*Hamara, HonItemDestOld.Mokza+ (CountAll-LoMukza)*Hamara, HonItemDestOld.Cash, HonItemDestOld.NonCash, HonItemDestOld.Part, HonItemDestOld.PartValue, True));
              end
          else
            begin
              FNonDraftHonNew.Add(THonItem.CreateNew(DecisionDate, cbDestName.Text, MyStrToFloat(cbDestValue.Text), CountAll*Hamara, (CountAll-LoMukza)*Hamara, 0, 0, 0, 0, True));
            end;

          if Assigned(HonItemSourceNew) then
            FNonDraftHonNew.Add(HonItemSourceNew);
        end;
    end;
end;

procedure TfmHamara.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
  I: Integer;
  SugName: String;
  AllZero: Boolean;
  SugValue: Double;
  HonItem: THonItem;
  Stockholder: TStockHolder;
  Stock: TStock;
  MuhazItem: TMuhazItem;
begin
  if rbAll.Checked and (Page=1) then
    Exit;

  inherited;
  SugName:= cbSourceName.Text;
  SugValue:= MyStrToFloat(cbSourceValue.Text);
  Company.HonList.Filter:= afActive;
  HonItem:= Company.HonList.FindHonItem(SugName, SugValue);
  Company.StockHolders.Filter:= afActive;
  Company.MuhazList.Filter:= afActive;
  case Page of
    0: begin
         if not Assigned(HonItem) then
           begin
             If Not Silent Then
               With cbSourceName do
                 If Color = clWindow Then
                   Color := clBtnFace
                 Else
                   Color:= clWindow;
             ErrorAt:= cbSourceName;
             raise Exception.Create('��� ���� �� �� ���� �����');
           end;

         if cbDestName.Text = '' then
           begin
             If Not Silent Then
               With cbDestName do
                 If Color = clWindow Then
                   Color := clBtnFace
                 Else
                   Color:= clWindow;
             ErrorAt:= cbDestName;
             raise Exception.Create('��� ��� ���');
           end;

         if (cbDestName.Text=SugName) and (MyStrToFloat(cbDestValue.Text)=SugValue) then
           begin
             if not Silent then
               with cbDestName do
                 if Color = clWindow then
                   Color:= clBtnFace
                 else
                   Color:= clWindow;
             ErrorAt:= cbDestName;
             raise Exception.Create('�� ���� ����� ����� �����.');
           end;

         if (not Silent) and (not ErrorTimer.Enabled) and
            (Company.HonList.FindHonItem(cbDestName.Text, MyStrToFloat(cbDestValue.Text)) = nil) then
           begin
             PageIndex:= 0;
             if Application.MessageBox(PChar('��� ����� ���� "'+cbDestName.Text+'" ��� ���� "'+cbDestValue.Text+'"'#13#10+
                                       '�� ���� �����, ��� ����� ����� ������ �������� ��� ����� ��.'+#13#10+
                                       '��� ������?'),
                                       PChar(Caption), MB_YESNO + MB_IconHand + MB_RTLREADING + MB_RIGHT) <> IDYES then
               begin
                 ErrorAt:= cbDestName;
                 raise Exception.Create('zeut');
               end
           end;

         if MyStrToFloat(edSourceCount.Text)<= 0 then
           begin
             if not Silent then
               with edSourceCount do
                 if Color = clWindow then
                   Color:= clBtnFace
                 else
                   Color:= clWindow;
             ErrorAt:= edSourceCount;
             raise Exception.Create('��� ���� �� ����');
           end;

         if MyStrToFloat(edDestCount.Text)<= 0 then
           begin
             if not Silent then
               with edDestCount do
                 if Color = clWindow then
                   Color:= clBtnFace
                 else
                   Color:= clWindow;
             ErrorAt:= edDestCount;
             raise Exception.Create('��� ���� �� ����');
           end;

         if rbAll.Checked and (MyStrToFloat(edCount.Text)>100) then
           begin
             if not Silent then
               with edCount do
                 if Color = clWindow then
                   Color:= clBtnFace
                 else
                   Color:= clWindow;
             ErrorAt:= edCount;
             raise Exception.Create('�� ���� ����� ���� �-100% �������.');
           end;
       end;
    1: begin
         if rbAll.Checked then
           Exit;
         CheckGrid(Page, Silent, Extended, Grid3);
         AllZero:=True;
         for I:= 0 to Grid3.RowCount-1 do
           if (Trim(Grid3.Cells[0, I])<>'') and (MyStrToFloat(Grid3.Cells[3,I]) <> 0) then
             begin
               AllZero:= False;
               if MyStrToFloat(Grid3.Cells[3,I])<0 then
                 begin
                   If not Silent then
                     begin
                       Grid3.Row:= I;
                       Grid3.Col:= 3;
                     end;
                   ErrorAt:= Grid3;
                   raise Exception.Create('���� ������ ����� ���� ����� ����� ������.');
                 end
               else
                 if Grid3.Cells[0,I] = cLoMukza then
                   begin
                     if (HonItem=nil) or ((HonItem.Mokza + MyStrToFloat(Grid3.Cells[3,I])) > HonItem.Rashum) then
                       begin
                         If Not Silent Then
                           begin
                             Grid3.Row:= I;
                             Grid3.Col:= 3;
                           end;
                         ErrorAt:= Grid3;
                         raise Exception.Create('����� "'+cLoMukza+'" ����� ����� ���� ����� ����� ����� ���"� ������');
  //                       raise Exception.Create('��� �� ����� ��� ����� ��');
                       end;
                   end
                 else
                   if Grid3.Cells[0,I] = cMuhaz then
                     begin
                       MuhazItem:= Company.MuhazList.FindByZeut(Grid3.Cells[1,I]) as TMuhazItem;
                       if not Assigned(MuhazItem) then
                         begin
                           If Not Silent Then
                             begin
                               Grid3.Row:= I;
                               Grid3.Col:= 0;
                             end;
                           ErrorAt:= Grid3;
                           raise Exception.Create('��� �� �� ����');
                         end;
                     end
                   else
                     begin
                       StockHolder:= Company.StockHolders.FindByZeut(Grid3.Cells[1,I]) as TStockHolder;
                       if Assigned(StockHolder) then
                         begin
                           StockHolder.Stocks.Filter:= afActive;
                           Stock:= Stockholder.Stocks.FindStock(SugName, SugValue);
                           if Assigned(Stock) then
                             begin
                               if Stock.Count < MyStrToFloat(Grid3.Cells[3,I]) then
                                 begin
                                   If Not Silent Then
                                     begin
                                       Grid3.Row:= I;
                                       Grid3.Col:= 3;
                                     end;
                                   ErrorAt:= Grid3;
                                   raise Exception.Create('���� ����� '+ StockHolder.Name+' �� �� '+FloatToStr(Stock.Count)+ ' �����.');
                                 end;
                             end
                           else
                             begin
                               If Not Silent Then
                                 begin
                                   Grid3.Row:= I;
                                   Grid3.Col:= 3;
                                 end;
                               ErrorAt:= Grid3;
                               raise Exception.Create('���� ����� '+StockHolder.Name+' ��� ����� ���� '+cbSourceName.Text+' '+cbSourceValue.Text);
                             end;
                         end
                       else
                         begin
                           If Not Silent Then
                             begin
                               Grid3.Row:= I;
                               Grid3.Col:= 0;
                             end;
                           ErrorAt:= Grid3;
                           raise Exception.Create('����� ��� ��� ����� ��� '+ Grid3.Cells[0,I]);
                         end;
                     end;  // StockHolders Check
             end; // If Grid3 Row is not empty
         // This should be outside "for"
         if AllZero then
           begin
             if Not Silent then
               begin
                 Grid3.Row:=0;
                 Grid3.Col:=3;
               end;
             ErrorAt:= Grid3;
             raise Exception.Create('����� ����� ����� ����� ����� ����');
           end;
       end;  // 1: begin
  end;   // Of Case PAGE
end;

procedure TfmHamara.EvFillSugList;
{var
  I: Integer;}
begin
  cbDestName.Items.Clear;
  {Company.HonList.Filter:= afActive;
  for I:= 0 to Company.HonList.Count-1 do
    if cbSug.Items.IndexOf(Company.HonList.Items[I].Name) = -1 then
      cbSug.Items.Add(Company.HonList.Items[I].Name);
  cbSug.Items.Add('------------');     }
  Data.taStocks.First;
  while not Data.taStocks.EOF do
    begin
      if cbDestName.Items.IndexOf(Data.taStocks.Fields[0].Text) = -1 then
        cbDestName.Items.Add(Data.taStocks.Fields[0].Text);
      Data.taStocks.Next;
    end;
end;

procedure TfmHamara.EvDataLoaded;
var
  I: Integer;
  S: String;
begin
  inherited;
  Company.HonList.Filter:= afActive;
  S:= cbSourceName.Text;
  cbSourceName.Items.Clear;
  for I:= 0 to Company.HonList.Count-1 do
    if cbSourceName.Items.IndexOf(Company.HonList.Items[I].Name) = -1 then
      cbSourceName.Items.Add(Company.HonList.Items[I].Name);
  cbSourceName.ItemIndex:= cbSourceName.Items.IndexOf(S);
  // cbSourceNameChange(cbSourceName);
  if not DoNotClear then
    begin
      if (cbSourceName.ItemIndex=-1) and (cbSourceName.Items.Count>0) then
        cbSourceName.ItemIndex:= 0;
      cbSourceNameChange(cbSourceName);
      EvFillSugList;
      if (cbDestName.Items.Count>0) then
        begin
          cbDestName.ItemIndex:=0;
          cbDestNameChange(cbDestName);
        end;
      rbAll.Checked:= True;
      rbChooseClick(rbAll);
      edSourceCount.Text:= '1';
      edDestCount.Text:= '1';
      edCount.Text:= '100';
      edReset.Text:= '100';
      EvFillGrid3(0);
    end;
end;

procedure TfmHamara.EvFillGrid3(Amount: Double);
var
  I: Integer;
  Stock: TStock;
  SelName: String;
  SelValue: Double;
  HonItem: THonItem;
  MuhazItem: TMuhazItem;
begin
  Grid3.RowCount:= 1;
  Grid3.ResetLine(0);
  Company.StockHolders.Filter:= afActive;
  if (Company.HonList.Count<1) or (cbSourceName.ItemIndex=-1) then
    begin
      Grid3.Options:= Grid3.Options-[goEditing];
    end
  else
    begin
      SelName:= cbSourceName.Text;
      SelValue:= MyStrToFloat(cbSourceValue.Text);
      HonItem:= Company.HonList.FindHonItem(SelName, SelValue);
      if Assigned(HonItem) and (HonItem.Rashum> HonItem.Mokza) then
        begin
          Grid3.Cells[0,Grid3.RowCount-1]:= cLoMukza;
          Grid3.Cells[1,Grid3.RowCount-1]:= '';
          Grid3.Cells[2,Grid3.RowCount-1]:= FormatNumber(FloatToStr(HonItem.Rashum-HonItem.Mokza),15,2,True);
          Grid3.Cells[3,Grid3.RowCount-1]:= '0.00';
          Grid3.RowCount:= Grid3.RowCount+1;
        end;

      Company.MuhazList.Filter:= afActive;
      for I:= 0 to Company.MuhazList.Count-1 do
        begin
          MuhazItem:= Company.MuhazList.Items[I] as TMuhazItem;
          If (MuhazItem.Name = SelName) and (MuhazItem.Value = SelValue) then
            begin
              Grid3.Cells[0,Grid3.RowCount-1]:= cMuhaz;
              Grid3.Cells[1,Grid3.RowCount-1]:= MuhazItem.Zeut;
              Grid3.Cells[2,Grid3.RowCount-1]:= '1';
              Grid3.Cells[3,Grid3.RowCount-1]:= '0.00';
              Grid3.RowCount:= Grid3.RowCount+1;
            end;
        end;

      for I:= 0 to Company.StockHolders.Count-1 do
        with Company.StockHolders.Items[I] as TStockHolder do
          begin
            Stocks.Filter:= afActive;
            Stock:= Stocks.FindStock(SelName, SelValue);
            if Assigned(Stock) then
              begin
                Grid3.Cells[0,Grid3.RowCount-1]:= Company.StockHolders.Items[I].Name;
                Grid3.Cells[1,Grid3.RowCount-1]:= Company.StockHolders.Items[I].Zeut;
                Grid3.Cells[2,Grid3.RowCount-1]:= FormatNumber(FloatToStr(Stock.Count),15,2,True);
                Grid3.Cells[3,Grid3.RowCount-1]:= '0.00';
                Grid3.RowCount:= Grid3.RowCount+1;
              end;
          end;
    end;
  if Grid3.RowCount > 1 then
    Grid3.RowCount:= Grid3.RowCount-1;
  SetAmount(Amount);
end;

procedure TfmHamara.EvFocusNext(Sender: TObject);
begin
  inherited;
  case PageIndex of
    0: begin
         if Sender = e14 then edSourceCount.SetFocus;
         if Sender = edSourceCount then cbSourceName.SetFocus;
         if Sender = cbSourceName then cbSourceValue.SetFocus;
         if Sender = cbSourceValue then edDestCount.SetFocus;
         if Sender = edDestCount then cbDestName.SetFocus;
         if Sender = cbDestName then cbDestValue.SetFocus;
         if Sender = cbDestValue then rbAll.SetFocus;
         if Sender = rbAll then edCount.SetFocus;
         if Sender = edCount then rbChoose.SetFocus;
         if (Sender = rbChoose) and rbChoose.Checked then
           begin
             PageIndex:= 1;
             Grid3.SetFocus;
           end;
       end;
    1: begin
         If Sender=e14 then grid3.SetFocus;
       end;
  end;
end;

procedure TfmHamara.SetAmount(Amount: Double);
var
  I: Integer;
begin
  Amount:= Amount/100; // Convert Percents
  for I:= 0 to Grid3.RowCount-1 do
    if Grid3.Cells[0,I] = cMuhaz then
      if Amount=0 then
        Grid3.Cells[3,I]:= '0'
      else
        Grid3.Cells[3,I]:= '1'
    else
      Grid3.Cells[3,I]:= FormatNumber(FloatToStr(MyStrToFloat(Grid3.Cells[2,I])*Amount), 15,2,True);
end;

procedure TfmHamara.btResetClick(Sender: TObject);
begin
  inherited;
  SetAmount(MyStrToFloat(edReset.Text));
end;

procedure TfmHamara.Grid3SelectCell(Sender: TObject; Col, Row: Integer;
  var CanSelect: Boolean);
begin
  inherited;
  Company.StockHolders.Filter:= afActive;
  Grid3.Options:= Grid3.Options-[goEditing];
  if (Company.HonList.Count>0) and (cbSourceName.ItemIndex<>-1) and (Col = 3) then
    Grid3.Options:= Grid3.Options+[goEditing];
end;

procedure TfmHamara.FormCreate(Sender: TObject);
begin
  FileAction:= faHamara;
  ShowStyle:= afActive;
  inherited;

end;

procedure TfmHamara.rbChooseClick(Sender: TObject);
begin
  inherited;
  CriticalEditChange(Sender);
  if rbChoose.Checked then
    begin
      ClientHeight:= 411;
      pnDashboard.Top:= 360;
      pnButtons.Visible:= True;
    end
  else
    begin
      pnButtons.Visible:= False;
      pnDashboard.Top:= pnButtons.Top;
      ClientHeight:= 371;
    end;
end;

procedure TfmHamara.cbSourceNameChange(Sender: TObject);

  procedure EvFillValues;
  var
    I: Integer;                   
  begin
    Company.HonList.Filter:= afActive;
    cbSourceValue.Items.Clear;
    For I:= 0 to Company.HonList.Count-1 do
      if Company.HonList.Items[I].Name = cbSourceName.Text then
        cbSourceValue.Items.Add(FormatNumber(FloatToStr((Company.HonList.Items[I] as THonItem).Value), 15, 4, True));
  end;

var
  OldText: String;
begin
  inherited;
  CriticalEditChange(Sender);
  if Sender= cbSourceName then
    begin
      OldText:= cbSourceValue.Text;
      EvFillValues;
      cbSourceValue.ItemIndex:= cbSourceValue.Items.IndexOf(OldText);
      if (cbSourceValue.ItemIndex=-1) and (cbSourceName.Items.Count>0) then
        cbSourceValue.ItemIndex:=0;
    end;
  EvFillGrid3(MyStrToFloat(edCount.Text));
end;

procedure TfmHamara.cbDestNameChange(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  CriticalEditChange(Sender);
  cbDestValue.Items.Clear;
  for I:= 0 to Company.HonList.Count-1 do
    with Company.HonList.Items[I] as THonItem do
      if Name= cbDestName.Text then
        cbDestValue.Items.Add(FormatNumber(FloatToStr(Value),15,4,True));
end;

procedure TfmHamara.edCountKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  ControlKeyPress(Sender, Key);
  if Key <> #0 then
    rbAll.Checked:= True;
end;

end.
