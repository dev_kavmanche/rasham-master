unit UpdateTables; //Create 24.7.05 for table changes on updates

interface
  uses
   classes, db, dbtables, bde,SysUtils,Windows;

 type
  ChangeRec = packed record
    szName: DBINAME;
    iType: word;
    iSubType: word;
    iLength: word;
    iPrecision: byte;
  end;
  procedure VersionUpdate( Owner: TComponent);
  procedure UpdateForVersion40( Owner: TComponent);
  procedure UpdateForVersion90( Owner: TComponent);
  procedure AddField(Table: TTable; NewField: ChangeRec);
  procedure ChangeField(Table : TTable; FieldName : String; Rec : ChangeRec);


 implementation
  uses DataM;
//=================================================================
procedure VersionUpdate( Owner: TComponent);
begin
  //Add updates for other versions here, by order of versions
  UpdateForVersion40 (Owner);
  UpdateForVersion90 (Owner);
end;

//=================================================================
procedure UpdateForVersion40( Owner: TComponent); //table update for version 4.0
const
  NEW_FLDSIZE = 10;
var
  Rec      : ChangeRec;
  ATable   : TTable;
  ZeutField : TField;
  TableList: TStringList;
  i: integer;
begin
  ATable := TTable.Create( Owner );
  TableList:= TStringList.Create ;
  TableList.Add(Data.taDirectors.TableName  );
  TableList.Add(Data.taStockHolders.TableName);
  TableList.Add(Data.taManager.TableName );
  TableList.Add(Data.taSigName.TableName );

  try
    for i := 0 to TableList.Count -1 do
    begin
      ATable.Close;
      ATable.DatabaseName := 'RASHAM';
      ATable.TableName := TableList.Strings[i];
      ATable.Open;
      if ATable.TableName<> 'SigName.db' then
         ZeutField := ATable.FieldByName( 'Zeut' )
      else
         ZeutField := ATable.FieldByName( 'SigZeut' ) ;

      if ZeutField.Size < NEW_FLDSIZE then
      begin
        FillChar( Rec, SizeOf( Rec ), 0 );
        Rec.iLength := NEW_FLDSIZE;
        ChangeField( ATable, ZeutField.FieldName, Rec );
      end;
    end;
  finally
    ATable.Close;
    ATable.Free;
  end;
end;

//=================================================================
procedure UpdateForVersion90( Owner: TComponent); //table update for version 4.0
const
  NEW_FLDSIZE = 80;
  FIELD_NAME = 'EngName';
var
  Rec      : ChangeRec;
  ATable   : TTable;
begin


  ATable := TTable.Create( Owner );
 try
      ATable.Close;
      ATable.DatabaseName := 'RASHAM';
      ATable.TableName := 'signame.db';
      ATable.Open;

     if ATable.FindField(FIELD_NAME) = nil then
      begin
        ZeroMemory(@Rec, SizeOf(Rec));
        Rec.szName := FIELD_NAME;
        Rec.iType  :=  fldZSTRING;
        Rec.iLength := NEW_FLDSIZE;
        AddField( ATable,Rec );
      end;
  finally
    ATable.Close;
    ATable.Free;
  end;
end;

//==========================================Added Tsahi 24.7.04 // for chaning tables (copied from Supderdata code)
// From http://www.borland.com/devsupport/bde/bdeapiex/dbidorestructure.html
procedure AddField(Table: TTable; NewField: ChangeRec);
var
  Props: CURProps;
  hDb: hDBIDb;
  TableDesc: CRTblDesc;
  pFlds: pFLDDesc;
  pOp: pCROpType;
  B: byte;

begin
  // Make sure the table is open exclusively so we can get the db handle...
  {if Table.Active = False then
    raise EDatabaseError.Create('Table must be opened to restructure');
  if Table.Exclusive = False then
    raise EDatabaseError.Create('Table must be opened exclusively to restructure');}
  If (Table.Active and Not Table.Exclusive) Then Table.Close;
  If (Not Table.Exclusive) Then Table.Exclusive := True;
  If (Not Table.Active) Then Table.Open;

  // Get the table properties to determine table type...
  Check(DbiSetProp(hDBIObj(Table.Handle), curxltMODE, integer(xltNONE)));
  Check(DbiGetCursorProps(Table.Handle, Props));
  pFlds := AllocMem((Table.FieldCount + 1) * sizeof(FLDDesc));
  FillChar(pFlds^, (Table.FieldCount + 1) * sizeof(FLDDesc), 0);
  Check(DbiGetFieldDescs(Table.handle, pFlds));

  for B := 1 to Table.FieldCount do begin
    pFlds^.iFldNum := B;
    Inc(pFlds, 1);
  end;
  try
    StrCopy(pFlds^.szName, NewField.szName);
    pFlds^.iFldType := NewField.iType;
    pFlds^.iSubType := NewField.iSubType;
    pFlds^.iUnits1  := NewField.iLength;
    pFlds^.iUnits2  := NewField.iPrecision;
    pFlds^.iFldNum  := Table.FieldCount + 1;
  finally
    Dec(pFlds, Table.FieldCount);
  end;

  pOp := AllocMem((Table.FieldCount + 1) * sizeof(CROpType));
  Inc(pOp, Table.FieldCount);
  pOp^ := crADD;
  Dec(pOp, Table.FieldCount);

  // Blank out the structure...
  FillChar(TableDesc, sizeof(TableDesc), 0);
  //  Get the database handle from the table's cursor handle...
  Check(DbiGetObjFromObj(hDBIObj(Table.Handle), objDATABASE, hDBIObj(hDb)));
  // Put the table name in the table descriptor...
  StrPCopy(TableDesc.szTblName, Table.TableName);
  // Put the table type in the table descriptor...
  StrPCopy(TableDesc.szTblType, Props.szTableType);
  // Close the table so the restructure can complete...
  TableDesc.iFldCount := Table.FieldCount + 1;
  Tabledesc.pfldDesc := pFlds;
  TableDesc.pecrFldOp := pOp;
  Table.Close;
  // Call DbiDoRestructure...
  try
    Check(DbiDoRestructure(hDb, 1, @TableDesc, nil, nil, nil, FALSE));
  finally
    FreeMem(pFlds);
    FreeMem(pOp);
    Table.Open;
  end;
end;

//==========================================Added Tsahi 24.7.04 // for chaning tables (copied from Supderdata code)
// Procedure to modify Paradox table field properties
// (based on example from the Borland BDE Reference help)
procedure ChangeField(Table : TTable; FieldName : String; Rec : ChangeRec);
var
  Props: CURProps;
  hDb: hDBIDb;
  Res : DBIResult;
  TableDesc: CRTblDesc;
  pFields: pFLDDesc;
  pOp: pCROpType;
  B: byte;
  Field : TField;
begin
  // Make sure table is opened exclusively
  If (Table.Active and Not Table.Exclusive) Then Table.Close;
  If (Not Table.Exclusive) Then Table.Exclusive := True;
  If (Not Table.Active) Then Table.Open;

  Field := Table.FieldByName(FieldName);

  // Get table properties (needed to get table type)
  Check(DbiSetProp(hDBIObj(Table.Handle), curxltMODE, integer(xltNONE)));
  Check(DbiGetCursorProps(Table.Handle, Props));

  // Allocate memory for pointers
  pFields := AllocMem(Table.FieldCount * sizeof(FLDDesc));
  pOp := AllocMem(Table.FieldCount * sizeof(CROpType));

  try
    // Set the pointer to the index in the operation descriptor
    // to crMODIFY (to modify field properties)
    Inc(pOp, Field.Index);
    pOp^ := crMODIFY;
    Dec(pOp, Field.Index);

    // Get existing field properties
    Check(DbiGetFieldDescs(Table.Handle, pFields));
    Inc(pFields, Field.Index);

    // Check for field changes specified by caller
    if StrLen(Rec.szName) > 0 then pFields^.szName := Rec.szName;
    if Rec.iType > 0 then pFields^.iFldType := Rec.iType;
    if Rec.iSubType > 0 then pFields^.iSubType := Rec.iSubType;
    if Rec.iLength > 0 then pFields^.iUnits1 := Rec.iLength;
    if Rec.iPrecision > 0 then pFields^.iUnits2 := Rec.iPrecision;

    Dec(pFields, Field.Index);

    for B := 1 to Table.FieldCount do begin
      pFields^.iFldNum := B;
      Inc(pFields, 1);
    end;
    Dec(pFields, Table.FieldCount);

    // Clear the structure
    FillChar(TableDesc, sizeof(TableDesc), 0);

    // Get database handle from the table cursor's handle
    Check(DbiGetObjFromObj(hDBIObj(Table.Handle), objDATABASE, hDBIObj(hDb)));

    // Set table name and type into the descriptor
    StrPCopy(TableDesc.szTblName, Table.TableName);
    StrPCopy(TableDesc.szTblType, Props.szTableType);

    // Set field count for this table
    TableDesc.iFldCount := Table.FieldCount;

    // Link the operation descriptor to the table descriptor
    TableDesc.pecrFldOp := pOp;

    // Link the field descriptor to the table descriptor
    TableDesc.pFldDesc := pFields;

    Table.Close;

    // Do the actual table restructuring
    Res := DbiDoRestructure(hDb, 1, @TableDesc, nil, nil, nil, FALSE);
    Check(Res);
  finally
    if pFields <> nil then FreeMem(pFields);
    if pOp <> nil then FreeMem(pOp);

    Table.Exclusive := False;
    Table.Open;
  end;
end;
//=================================================================

end.
