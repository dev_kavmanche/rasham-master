unit FixProblems;

interface
uses
  classes, SysUtils, inifiles, Dialogs, Forms,Controls;

type
  TFileRec= record
    RecNum: integer;
    RecName: string;
    Company: integer;
    Action: string;
    DecisionDate: TDateTime;
    NotifyDate: TDateTime;
  end;

  procedure FixDB(CheckIni: boolean; SendingForm: TForm); //Tsahi 07/11/05 : Use this procedure to fix problems in DB as needed
  procedure FixRetroactiveReport (SendingForm : TForm); //Tsahi 07/11/05 :See proc for details
  function CheckPatches:integer; //Tsahi 07/11/05 : Checks which fixes still need applying
  procedure WritePatchesToIni (Key: string); //Tsahi 07/11/05 : Write Changes to ini
  function LoadRec: TFileRec;
  function CheckAndReplace(CheckRec: TFileRec): boolean;
  procedure CorrectDecisionDate(DecisionDate: TDateTime; RecNum: integer);
  procedure CorrectChanges(DecisionDate: TDateTime; RecNum: integer);

  procedure FixManagersProblem (Sender: TForm);

implementation
uses
  DataM, DataObjects, Util, DBTables, FixManagers;

Const
//binary masks for fixes
  FIX_ALL         = 3; //Change this when adding fixes to include all masks
  FIX_RETROACTIVE = 1;  //FixRetroactiveReport
  FIX_MANAGERS    = 2;  //FixAllManagers

var
  fTable: TTable;
//========================
procedure FixDB(CheckIni: boolean; SendingForm: TForm); //Tsahi 07/11/05 : Use this procedure to fix problems in DB as needed
var
  FixesNeeded: integer;
begin
  Screen.Cursor := crHourGlass;
  if CheckIni then
    FixesNeeded:=CheckPatches
  else
    FixesNeeded:=FIX_ALL;


  if ((FixesNeeded and  FIX_RETROACTIVE)= FIX_RETROACTIVE) then
    FixRetroactiveReport (SendingForm);
  if ((FixesNeeded and  FIX_MANAGERS)= FIX_MANAGERS) then
    FixManagersProblem(SendingForm);

  Screen.Cursor := crDefault;
end;
//========================
//Tsahi 07/11/05
//Fixes the problem of directors and stockholders
//saved with a wrong Decision Date,
//leading to them apearing with the wrong details in retroactive reports
procedure FixRetroactiveReport;
var
  qFiles :TQuery;
  TempRec : TFileRec;
  CheckedRecs: TStringList;
  Current, i : integer;
dbg: integer;
  Checked: integer;
begin
  CheckedRecs:=TStringList.Create;
  fTable:=TTable.Create(nil);
  fTable.DatabaseName :=data.taFiles.DatabaseName;
  fTable.TableName  :=data.taFiles.TableName;
  qFiles:=TQuery.Create(nil);
  qFiles.DatabaseName :=data.taFiles.DatabaseName;
  qFiles.SQL.Add ('SELECT Distinct RecName');
  qFiles.SQL.Add (' FROM Files.db');
  qFiles.ExecSQL;
  qFiles.Open;
  qFiles.First;
  While Not qFiles.EOF do
    with fTable do
    begin
      If SendingForm <> nil then
        SendingForm.Refresh;

      Filter:='(RecName = '''+ FormatForFilter(qFiles.FieldByName('RecName').AsString)+''') '+
              'and ( draft = 0) and ((Action = '''+FileActionToChar(faDirectorInfo)+
              ''') or (Action = '''+FileActionToChar(faStockHolderInfo)+''') OR (action = '''+FileActionToChar(faSignName)+''') OR (Action = '''+FileActionToChar(faManager)+'''))';
      Filtered := True;
      CheckedRecs.Clear;
      Current := 1;
      Open;
      First;
      While not EOF do
      begin
        if (CheckedRecs.IndexOf(IntTostr(Current))=-1)  then
        begin
          TempRec:=LoadRec;
          for i:= Current to RecordCount-1 do
          begin
            Next;
            if  (CheckedRecs.IndexOf(IntToStr(i))=-1)  then
              if CheckAndReplace(TempRec) then
                CheckedRecs.Add(IntToStr(i));
            end;
          FindKey([TempRec.RecNum]);
        end;
        Next;
        inc(Current);
      end;
      Filtered :=false;
      qFiles.Next;
      Close;
    end;
  qFiles.Close;
  qFiles.Free;
  CheckedRecs.Free;
  fTable.free;
  WritePatchesToIni('DecisionDateFix');
end;
//========================
function CheckPatches:integer; //Tsahi 07/11/05 : Checks which fixes still need applying
var
  f: TiniFile;
  FileName: string;
begin
  Result := 0;
  try
    FileName:=ExtractFilePath(ParamStr(0))+'RashamFP.ini';
    F:=TIniFile.Create(FileName);
    if StrToInt(F.ReadString('Done','DecisionDateFix','0')) = 0 then
      Result:= (Result OR FIX_RETROACTIVE);
    if StrToInt(F.ReadString('Done','ManagersFix','0')) = 0 then  //Tsahi 31.8.06 manager fixes
       Result:= (Result OR FIX_MANAGERS);
    F.Free;
  except;
  end;
end;
//========================
procedure WritePatchesToIni (Key: string);
var
  F: TIniFile;
begin
  try
    F:=TIniFile.Create(ExtractFilePath(ParamStr(0))+'RashamFP.ini');
    F.WriteString('Done',Key,'1');
    F.Free;
  except;
  end;
end;
//========================
function LoadRec: TFileRec;
var
  ResRec: TFileRec;
begin
  With fTable do
  begin
    ResRec.RecNum          :=FieldByName('RecNum').AsInteger;
    ResRec.RecName         :=FieldByName('RecName').AsString;
    ResRec.Company         :=FieldByName('Company').AsInteger;
    ResRec.Action          :=FieldByName('Action').AsString;
    ResRec.DecisionDate   :=FieldByName('DecisionDate').AsDateTime;
    ResRec.NotifyDate      :=FieldByName('NotifyDate').AsDateTime;
  end;
  Result:= ResRec;
end;
//========================
function CheckAndReplace(CheckRec: TFileRec): boolean;
var
  CurrDate: TDateTime;
  DateToUse: TDateTime;
  RecToUse: integer;
  OtherRecNum: integer;
begin
  result:=false;
  With fTable do
  begin
    if CheckRec.Company = FieldByName('Company').AsInteger  then
    begin
      CurrDate:= FieldByName('DecisionDate').AsDateTime;
      if (CheckRec.DecisionDate<>CurrDate) then
      begin
        OtherRecNum:=FieldByName('RecNum').AsInteger;
        If CheckRec.DecisionDate = CheckRec.NotifyDate then //The Checked Record is wrong
        begin
          DateToUse:= CurrDate;
          RecToUse := CheckRec.RecNum;
        end
        else if ((FieldByName('NotifyDate').AsDateTime = CurrDate)and (CurrDate>2)) then //The other record is wrong
        begin
          DateToUse:= CheckRec.DecisionDate;
          RecToUse := OtherRecNum;
        end;

        CorrectDecisionDate(DateToUse ,RecToUse );
        CorrectChanges(DateToUse,CheckRec.RecNum);
        CorrectChanges(DateToUse,OtherRecNum);
      end;
    end;
  end;
end;
//========================
procedure CorrectDecisionDate(DecisionDate: TDateTime; RecNum: integer);
var
  CurrRec: integer;
begin
  With fTable do
  begin
    CurrRec:=FieldByName('RecNum').AsInteger;
    if CurrRec<>RecNum then
      FindKey([RecNum]);
    Data.taAutoInc.Open;
    Edit;
    FieldByName('DecisionDate').AsDateTime:=DecisionDate;
    Post;
    Data.taAutoInc.Close;
    FindKey([CurrRec]);
  end;
end;
//========================
procedure CorrectChanges(DecisionDate: TDateTime; RecNum: integer);
var
  cTable :TTable;
begin
  cTable:=TTable.Create(nil);
  With cTable do
  begin
    DatabaseName := Data.taChanges.DatabaseName;
    TableName := Data.taChanges.TableName;
    Filter:='ResInt1 = '+IntToStr(RecNum);
    Filtered := True;
    Open;
    First;
    While not EOF do
    begin
      if FieldByName('Date').AsDateTime <> DecisionDate then
      begin
        Edit;
        FieldByName('Date').AsDateTime := DecisionDate;
        Post;
      end;
      Next;
    end;
    Close;
    free;
  end;
end;
//========================
procedure FixManagersProblem(Sender: TForm);
begin
  FixAllManagers (Sender);
  WritePatchesToIni('ManagersFix');
end;



end.
