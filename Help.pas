unit Help;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, Htmlview, HebForm, ExtCtrls, ComCtrls, Buttons,
  StdCtrls, HLabel;

type
  TfmHelp = class(TfmTemplate)
    HTML: THTMLViewer;
    HTMLprint: THTMLViewer;
    DgPrintSetup: TPrinterSetupDialog;
    imLogo: TImage;
    procedure GoHome(Sender: TObject);
    procedure B3Click(Sender: TObject);
    procedure HTMLprintPrintHeader(Sender: TObject; Canvas: TCanvas;
      NumPage, W, H: Integer; var StopPrinting: Boolean);
  private
    { Private declarations }
    FPrintAll: Boolean;
    FPath: String;
    PageCount: Integer;
    function GetPath: String;
    property HelpPath: String read GetPath;
  protected
    procedure EvPrintData; override;
  public
    { Public declarations }
  end;

  procedure ShowHelp(HelpContext: Integer);

var
  fmHelp: TfmHelp;

implementation

{$R *.DFM}

uses Registry, Main, Util, dialog2,HPSConsts;

procedure ShowHelp(HelpContext: Integer);
begin
  Application.CreateForm(TfmHelp, fmHelp);
  If (HelpContext<>0) And FileExists(fmHelp.HelpPath+IntToStr(HelpContext)+'.htm') then
    fmHelp.HTML.LoadFromFile(fmHelp.HelpPath+IntToStr(HelpContext)+'.htm')
  Else
    fmHelp.HTML.LoadFromFile(fmHelp.HelpPath+'3.htm');
  fmHelp.FPrintAll:= HelpContext < 0;  // From Main Menu
  fmHelp.ShowModal;
  fmHelp.Free;
end;

function TfmHelp.GetPath: String;
begin
  If FPath= '' then
    FPath:= fmMain.GetHelpPath;

  Result:= FPath;
end;

procedure TfmHelp.GoHome(Sender: TObject);
begin
  HTML.LoadFromFile(HelpPath + '3.htm');
end;

procedure TfmHelp.EvPrintData;
var
  Count: Integer;
  OldCaption: String;
begin
  PageCount:= 0;
  fmDialog2.Tag:= -1;
  OldCaption:= fmDialog2.Panel1.Caption;
  fmDialog2.Panel1.Caption:='����� ���';
  fmDialog2.Show;
  if FPrintAll Then
    begin
      Count:= 1;
      while FileExists(HelpPath + IntToStr(Count)+'.htm') do
        begin
          HTMLprint.LoadFromFile(HelpPath+IntToStr(Count)+'.htm');
          HTMLprint.Print(0,99999);
          Inc(Count);
        end;
    end
  Else
    HTML.Print(0,99999);
  fmDialog2.Close;
  fmDialog2.Panel1.Caption:= OldCaption;
  fmDialog2.Tag:=0;
end;

procedure TfmHelp.B3Click(Sender: TObject);
begin
  DgPrintSetup.Execute;
end;

procedure TfmHelp.HTMLprintPrintHeader(Sender: TObject; Canvas: TCanvas;
  NumPage, W, H: Integer; var StopPrinting: Boolean);
begin
  Inc(PageCount);
  Canvas.Font.Name  := 'Times New Roman';
  Canvas.Font.Size  := 28;
  Canvas.Font.Style := [fsItalic,fsUnderline];
  Canvas.TextOut(10,5,'��� ����');

  Canvas.Font.Name  := 'David';
  Canvas.Font.Size  := 10;
  Canvas.Font.Style := [];
  Canvas.TextOut(25,46,'������ ������ ������');
  Canvas.TextOut(30,60,'���� ��� ������');
  Canvas.TextOut(33,72,'��. '+HPSPhone);
  Canvas.TextOut(30,84,HPSFax+' ���');
  Canvas.Font.Size  := 10;
  Canvas.TextOut((W Div 2)-(Canvas.TextWidth(IntToStr(NumPage)) Div 2),10,IntToStr(PageCount));
  Canvas.TextOut((W Div 2)-(Canvas.TextWidth(DateToLongStr(Now)) Div 2),45,DateToLongStr(Now));
  Canvas.MoveTo(0,110);
  Canvas.LineTo(W,110);
  Canvas.Draw(600,10,imLogo.Picture.Bitmap);
  Canvas.Font.Name:= 'David';
end;

end.
