unit Splash;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, jpeg, ComCtrls;

type
  TfmSplash = class(TForm)
    Panel1: TPanel;
    Image: TImage;
    Progress: TProgressBar;
    procedure FormPaint(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
     ProgressCounter : integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSplash: TfmSplash;

implementation

{$R *.DFM}

procedure TfmSplash.FormPaint(Sender: TObject);
begin
  inherited;

  if ProgressCounter =50 then
  begin
    ProgressCounter := 0;
    if Progress.Position <= 100 then
      Progress.Position := Progress.Position+1;
  end else
    inc(ProgressCounter);
end;

procedure TfmSplash.FormCreate(Sender: TObject);
begin
  ProgressCounter := 0;
end;

procedure TfmSplash.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Progress.Position := 100;
end;

end.
