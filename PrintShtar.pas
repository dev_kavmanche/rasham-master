unit PrintShtar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, DataObjects, Db, memdset;

type
  TfmQRShtar = class(TForm)
    qrShtar: TQuickRep;
    QRBand12: TQRBand;
    QRLabel59: TQRLabel;
    QRBand10: TQRBand;
    QRLabel7: TQRLabel;
    l1: TQRLabel;
    l2: TQRLabel;
    QRLabel2: TQRLabel;
    l3: TQRLabel;
    QRLabel4: TQRLabel;
    ql4: TQRLabel;
    l4: TQRLabel;
    QRLabel1: TQRLabel;
    l5: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    l6: TQRLabel;
    QRLabel10: TQRLabel;
    ql7: TQRLabel;
    l7: TQRLabel;
    l8: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel18: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText2: TQRDBText;
    SummaryBand1: TQRBand;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    taMemory: TMemDataSet;
  private
    { Private declarations }
    //function Replace(const ReplacedStr, InsertStr, MainStr: String): String;
    function FormatAddress(StockHolder: TStockHolder): String;
  public
    { Public declarations }
    procedure PrintExecute(const CompanyName, CompanyNum: String; FirstStockHolder, SecondStockHolder: TStockHolder; Stocks: TStocks; DoPrint: Boolean);
  end;

var
  fmQRShtar: TfmQRShtar;

implementation

{$R *.DFM}

uses Util, Printers, PrinterSetup;

{const
  StockStr = ', %1 ����� %2, ���� %3 �"� ��� ���� �"�';}

function TfmQRShtar.FormatAddress(StockHolder: TStockHolder): String;
begin
  With StockHolder.Address do
    Result:= Street+' '+Number+' '+ City+' '+Index+' '+Country;
end;

{function TfmQRShtar.Replace(const ReplacedStr, InsertStr, MainStr: String): String;
var
  I: Integer;
begin
  Result:= MainStr;
  I := Pos(ReplacedStr, Result);
  While I <> 0 do
    begin
      Delete(Result, I, Length(ReplacedStr));
      Insert(InsertStr, Result, I);
      I := Pos(Result, ReplacedStr);
    end;
end;}

procedure TfmQRShtar.PrintExecute(const CompanyName, CompanyNum: String; FirstStockHolder, SecondStockHolder: TStockHolder; Stocks: TStocks; DoPrint: Boolean);
var
  I: Integer;
begin
  taMemory.Close;
  taMemory.EmptyTable;
  taMemory.Open;
  For I:= 0 to Stocks.Count-1 do
    With Stocks.Items[I] As TStock do
      taMemory.AppendRecord([FloatToStrF(Count, ffNumber, 15, 2), Name, FloatToStrF(Value, ffNumber, 15, 4)]);

  l1.Caption:= CompanyName;
  l2.Caption:= CompanyNum;
  l3.Caption:= FirstStockHolder.Name;
  If FirstStockHolder.Taagid Then
    ql4.Caption:= '.�.�'
  else
    ql4.Caption:= '.�.�';
  l4.Caption:= FirstStockHolder.Zeut;
  l5.Caption:= FormatAddress(FirstStockHolder);
  l6.Caption:= SecondStockHolder.Name;
  If SecondStockHolder.Taagid Then
    ql7.Caption:= '.�.�'
  else
    ql7.Caption:= '.�.�';
  l7.Caption:= SecondStockHolder.Zeut;
  l8.Caption:= FormatAddress(SecondStockHolder);

  fmPrinterSetup.FormatQR(qrShtar);
  if DoPrint Then
    qrShtar.Print
  Else
    begin
      qrShtar.Preview;
      Application.ProcessMessages;
    end;
end;


end.
