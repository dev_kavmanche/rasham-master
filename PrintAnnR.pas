unit PrintAnnR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, HebForm, Qrctrls, Db, memdset,DataObjects, StdCtrls;

type
  TAnnualReport = record
    ReportDate:TDateTime;
    LastReport:TDateTime;
    DirectorsNotChanged:boolean;
    DirectorsChanged:boolean;
    DirectorionApproved:boolean;
    ReportsPresented:boolean;
    ReportsSent:boolean;
    ReportsAreNotMust:boolean;
    IsBookKeeper:boolean;
    BookKeeperName:string;
    BookKeeperAddress:string;
    NoBookKeeper:boolean;
    DaysWithoutBookKeeper:integer;
    NonActiveCompany:boolean;
    MemuneName:string;
    MemuneID:string;
    MemuneRole:string;
    MusmakhName:string;
    MusmakhID:string;
    MusmakhRole:string;
    SentDate:TDateTime;
    DateInReport:boolean;
    MemaleName:string;
    MemaleID:string;
    MemaleRole:string;
    AdvMeametName:string;
    AdvMatzhir2:string;
    AdvMatzhirID:string;
    AdvMeametFullName:string;
    AdvMeametAddress:string;
    AdvMeametID:string;
    AdvMeametLicence:string;
    BkMeametName:string;
    bkMatzhir2:string;
    BkMatzhirID:string;
    BkMeametFullName:string;
    BkMeametAddress:string;
    BkMeametID:string;
    AccountantID:string;
    BkMeametLicence:string;
  end;

  TfrmPrintAnnualReport = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRImage4: TQRImage;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRImage3: TQRImage;
    QRLabel6: TQRLabel;
    lblDate: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    lblMailB: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel28: TQRLabel;
    edtCompanyName: TQRLabel;
    edtCompanyAddress: TQRLabel;
    edtCompanyPhone: TQRLabel;
    edtCompanyMail: TQRLabel;
    edtCompanyNumber: TQRLabel;
    edtReportDate: TQRLabel;
    edtAsefaDate: TQRLabel;
    lblHonTitle: TQRLabel;
    rectHon: TQRShape;
    hlHon: TQRShape;
    vlHonA: TQRShape;
    lblSakhHon: TQRLabel;
    edtSakhHon: TQRLabel;
    lblStockNameA: TQRLabel;
    lblStockNameB: TQRLabel;
    edtStockName: TQRDBText;
    lblStockType: TQRLabel;
    edtStockType: TQRDBText;
    lblStocksNum: TQRLabel;
    edtStocksNum: TQRDBText;
    lblmukzot: TQRLabel;
    edtMukzot: TQRDBText;
    lblStockValue: TQRLabel;
    edtStockValue: TQRDBText;
    vlHonB: TQRShape;
    taMemHon: TMemDataSet;
    taMemHonStockName: TStringField;
    taMemHonStockType: TStringField;
    taMemHonStocksNum: TFloatField;
    taMemHonMukzot: TFloatField;
    taMemHonStockValue: TCurrencyField;
    QRBand3: TQRBand;
    lblStocksTitle: TQRLabel;
    rectStocks: TQRShape;
    vlStocksA: TQRShape;
    vlStocksB: TQRShape;
    lblStockHolderName: TQRLabel;
    edtStockHolderName: TQRDBText;
    lblStockTypeA: TQRLabel;
    edtStockTypeA: TQRDBText;
    lblID: TQRLabel;
    edtID: TQRDBText;
    lblStocksNumA: TQRLabel;
    edtStocksNumA: TQRDBText;
    lblAddressA: TQRLabel;
    lblAddressB: TQRLabel;
    edtAddressA: TQRDBText;
    lblSumNotPaid: TQRLabel;
    edtSumNotPaid: TQRDBText;
    QRShape7: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel24: TQRLabel;
    taMemMokaz: TMemDataSet;
    taMemMokazShtarValue: TFloatField;
    QuickRep2: TQuickRep;
    QRBand5: TQRBand;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape12: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRSubDetail5: TQRSubDetail;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    edtMisparMenayot: TQRDBText;
    edtMisparShtar: TQRDBText;
    edtSakhMenayotLamokaz: TQRLabel;
    taMemDirectors: TMemDataSet;
    taMemDirectorsAddress: TStringField;
    taMemDirectorsStartDate: TDateTimeField;
    taMemDirectorsName: TStringField;
    taMemDirectorsZeut: TStringField;
    QRBand6: TQRBand;
    QRLabel36: TQRLabel;
    QRSubDetail6: TQRSubDetail;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    edtDirectorName: TQRDBText;
    edtDirectorID: TQRDBText;
    edtDirectorDate: TQRDBText;
    edtDirectorAddress: TQRDBText;
    QRSubDetail7: TQRSubDetail;
    QRBand7: TQRBand;
    QRLabel26: TQRLabel;
    taMemNonDirectors: TMemDataSet;
    taMemNonDirectorsName: TStringField;
    taMemNonDirectorsEndDate: TDateTimeField;
    taMemNonDirectorsZeut: TStringField;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRLabel31: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    edtNonDirectorName: TQRDBText;
    edtNonDirectorID: TQRDBText;
    edtEndDate: TQRDBText;
    QRBand8: TQRBand;
    QRImage1: TQRImage;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRImage2: TQRImage;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRMemo1: TQRMemo;
    taMemManagers: TMemDataSet;
    taMemManagersPhone: TStringField;
    taMemManagersAddress: TStringField;
    taMemManagersZeut: TStringField;
    taMemManagersName: TStringField;
    taMemMokazZeut: TStringField;
    QRCompositeReport1: TQRCompositeReport;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel27: TQRLabel;
    QRMemo4: TQRMemo;
    QRLabel30: TQRLabel;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    lbl3: TQRLabel;
    QRLabel146: TQRLabel;
    taMemStocks: TMemDataSet;
    taMemStocksSumNotPaid: TCurrencyField;
    taMemStocksAddress: TStringField;
    taMemStocksStocksNum: TFloatField;
    taMemStocksid: TStringField;
    taMemStocksStocksType: TStringField;
    taMemStocksStockHolderName: TStringField;
    QuickRep4: TQuickRep;
    QRBand12: TQRBand;
    QRImage7: TQRImage;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRImage8: TQRImage;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    edtAdvMeametname: TQRLabel;
    QRLabel123: TQRLabel;
    edtAdvMatzhir2: TQRLabel;
    QRLabel125: TQRLabel;
    edtAdvMatzhirID: TQRLabel;
    QRLabel127: TQRLabel;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    edtAdvMeametFullName: TQRLabel;
    edtAdvMeametAddress: TQRLabel;
    edtAdvMeametID: TQRLabel;
    edtAdvMeametLicence: TQRLabel;
    QRLabel132: TQRLabel;
    QRShape49: TQRShape;
    QRLabel112: TQRLabel;
    edtBkMeametName: TQRLabel;
    QRLabel134: TQRLabel;
    edtbkMatzhir2: TQRLabel;
    QRLabel135: TQRLabel;
    edtBkMatzhirID: TQRLabel;
    QRLabel138: TQRLabel;
    QRShape50: TQRShape;
    QRShape51: TQRShape;
    QRShape52: TQRShape;
    QRShape53: TQRShape;
    QRShape54: TQRShape;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    edtBkMeametFullName: TQRLabel;
    edtBkMeametAddress: TQRLabel;
    edtBkMeametID: TQRLabel;
    edtBkMeametLicence: TQRLabel;
    QRLabel143: TQRLabel;
    QRShape55: TQRShape;
    QRLabel177: TQRLabel;
    QRLabel178: TQRLabel;
    QRLabel179: TQRLabel;
    QRLabel180: TQRLabel;
    QRLabel181: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel182: TQRLabel;
    QRLabel183: TQRLabel;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    QRLabel186: TQRLabel;
    QRLabel187: TQRLabel;
    QRLabel188: TQRLabel;
    QRLabel189: TQRLabel;
    QRLabel190: TQRLabel;
    QRLabel191: TQRLabel;
    QRLabel192: TQRLabel;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabel195: TQRLabel;
    QRLabel196: TQRLabel;
    QRLabel266: TQRLabel;
    QRLabel267: TQRLabel;
    QRLabel268: TQRLabel;
    QRLabel269: TQRLabel;
    QRShape56: TQRShape;
    QRShape11: TQRShape;
    QRShape57: TQRShape;
    QRLabel17: TQRLabel;
    QuickRep3: TQuickRep;
    QRBand9: TQRBand;
    QRImage5: TQRImage;
    QRLabel51: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRImage6: TQRImage;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    lblNotChanged: TQRLabel;
    QRLabel62: TQRLabel;
    lblChanged: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    lblDirectorionApproved: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    lblReportsPresented: TQRLabel;
    lblReportsSent: TQRLabel;
    lblReportsAreNotMust: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel73: TQRLabel;
    lblIsBookKeeper: TQRLabel;
    lblNoBookkeeper: TQRLabel;
    lblInactiveCompany: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    lblBookKeepername: TQRLabel;
    QRLabel81: TQRLabel;
    lblBookKeeperAddress: TQRLabel;
    QRLabel83: TQRLabel;
    lblDaysWithoutBookKeeper: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel156: TQRLabel;
    QRLabel157: TQRLabel;
    QRLabel158: TQRLabel;
    QRLabel159: TQRLabel;
    QRLabel161: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel163: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    QRLabel167: TQRLabel;
    QRLabel168: TQRLabel;
    QRBand10: TQRBand;
    QRLabel74: TQRLabel;
    QRMemo2: TQRMemo;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel80: TQRLabel;
    QRSubDetail3: TQRSubDetail;
    QRShape30: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    edtManagerName: TQRDBText;
    edtManagerID: TQRDBText;
    edtManagerAddress: TQRDBText;
    edtManagerPhone: TQRDBText;
    QRLabel160: TQRLabel;
    QRBand11: TQRBand;
    QRShape26: TQRShape;
    QRShape31: TQRShape;
    QRShape37: TQRShape;
    QRLabel82: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel87: TQRLabel;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel91: TQRLabel;
    edtMemuneName: TQRDBText;
    edtMemuneID: TQRLabel;
    edtMemuneRole: TQRLabel;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    edtMusmakhName: TQRLabel;
    edtMusmakhID: TQRLabel;
    edtMusmakhRole: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel100: TQRLabel;
    lblMatzhir: TQRLabel;
    QRLabel99: TQRLabel;
    lblSentDate: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRShape40: TQRShape;
    QRShape41: TQRShape;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    edtMemaleName: TQRLabel;
    edtMemaleID: TQRLabel;
    edtMemaleRole: TQRLabel;
    QRLabel107: TQRLabel;
    QRShape43: TQRShape;
    QRLabel169: TQRLabel;
    QRLabel170: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel92: TQRLabel;
    QRShape42: TQRShape;
    QRLabel173: TQRLabel;
    QRLabel174: TQRLabel;
    QRLabel175: TQRLabel;
    QRLabel176: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel29: TQRLabel;
    procedure QRSubDetail5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail5AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRSubDetail1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail8AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRSubDetail9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail9AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure QRSubDetail2AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRSubDetail2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    honPrinted:boolean;
    detail5printed,detail8printed,detail9printed,lbl3printed:boolean;
    Company:TCompany;
    AnnualReport:TAnnualReport;
    procedure LoadData(Empty:boolean);
    procedure CreateMemTables(Empty:boolean);
    procedure PrintEmptyProc(DoPrint: Boolean);
    procedure PrintProc(DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintEmpty(const FileName: String);

    procedure PrintExecute(const FileName: string; ACompany: TCompany;AAnnualReport:TAnnualReport);

  end;

var
  frmPrintAnnualReport: TfrmPrintAnnualReport;

implementation

uses PrinterSetup,utils2;


{$R *.DFM}
procedure TfrmPrintAnnualReport.PrintEmpty(const FileName: String);
begin
  CreateMemTables(True);
  LoadData(True);
  fmPrinterSetup.Execute(FileName, PrintEmptyProc);
end;

procedure TfrmPrintAnnualReport.LoadData(Empty:boolean);
var
  i:integer;
  sum:double;
begin
  detail5printed:=False;
  detail8printed:=False;
  detail9printed:=False;
  lbl3printed:=False;
  honPrinted:=False;
  if Empty then
    begin
      lblDate.Caption:=EMPTY_DATE;
      SetChecked(lblNotChanged,False,0);
      SetChecked(lblChanged,False,0);
      SetChecked(lblDirectorionApproved,False,0);
      SetChecked(lblReportsPresented,False,0);
      SetChecked(lblReportsSent,False,0);
      SetChecked(lblReportsAreNotMust,False,0);
      SetChecked(lblIsBookKeeper,False,0);
      lblBookKeeperName.Caption:=EmptyPlace(20);
      lblBookKeeperAddress.Caption:=EmptyPlace(20);
      SetChecked(lblNoBookKeeper,False,0);
      lblDaysWithoutBookKeeper.Caption:=EmptyPlace(3);
      edtReportDate.Caption:=EMPTY_DATE;
      SetChecked(lblInActiveCompany,False,0);
      edtMemuneName.Caption:=EmptyPlace(20);
      edtMemuneID.Caption:=EmptyPlace(9);
      edtMemuneRole.Caption:=EmptyPlace(20);
      edtMusmakhName.Caption:=EmptyPlace(20);
      edtMusmakhID.Caption:=EmptyPlace(9);
      edtMusmakhRole.Caption:=EmptyPlace(20);
      edtAdvMatzhirID.Caption:=EmptyPlace(9);
      edtAdvMeametFullName.Caption:=EmptyPlace(20);
      edtAdvMeametAddress.Caption:=EmptyPlace(20);
      edtAdvMeametID.Caption:=EmptyPlace(9);
      edtAdvMeametLicence.Caption:=EmptyPlace(10);
      edtBkMeametName.Caption:=EmptyPlace(20);
      edtBkMatzhirID.Caption:=EmptyPlace(9);
      edtBkMeametFullName.Caption:=EmptyPlace(20);
      edtBkMeametAddress.Caption:=EmptyPlace(20);
      edtBkMeametID.Caption:=EmptyPlace(9);
      edtBkMeametLicence.Caption:=EmptyPlace(10);
      lblMatzhir.Caption:=EmptyPlace(20);
      lblSentDate.Caption:=EMPTY_DATE;
      edtMemaleName.Caption:=EmptyPlace(20);
      edtMemaleID.Caption:=EmptyPlace(9);
      edtMemaleRole.Caption:=EmptyPlace(20);
      edtAdvMeametName.Caption:=EmptyPlace(20);
      edtAdvMatzhir2.Caption:=EmptyPlace(20);
      edtbkMatzhir2.Caption:=EmptyPlace(20);
      edtCompanyName.Caption:=EmptyPlace(20);
      edtCompanyNumber.Caption:=EmptyPlace(10);
      edtCompanyAddress.Caption:=EmptyPlace(20);
      edtCompanyPhone.Caption:=EMPTY_PHONE;
      edtCompanyMail.Caption:=EMPTY_MAIL;
      edtAsefaDate.Caption:=EMPTY_DATE;
      edtSakhMenayotLamokaz.Caption:=EmptyPlace(15);
    end
  else
    begin
     lblDate.Caption:=DateToStr(AnnualReport.ReportDate);
     SetChecked(lblNotChanged,AnnualReport.DirectorsNotCHanged,0);
     SetChecked(lblChanged,AnnualReport.DirectorsChanged,0);
     SetChecked(lblDirectorionApproved,AnnualReport.DirectorionApproved,0);
     SetChecked(lblReportsPresented,AnnualReport.ReportsPresented,0);
     SetChecked(lblReportsSent,AnnualReport.ReportsSent,0);
     SetChecked(lblReportsAreNotMust,AnnualReport.ReportsAreNotMust,0);
     SetChecked(lblIsBookKeeper,AnnualReport.IsBookKeeper,0);

     if AnnualReport.IsBookKeeper then
       begin
         lblBookKeeperName.Caption:=AnnualReport.BookKeeperName;
         lblBookKeeperAddress.Caption:=AnnualReport.BookKeeperAddress;
       end
    else
      begin
        lblBookKeeperName.Caption:='';
        lblBookKeeperAddress.Caption:='';
      end;
    SetChecked(lblNoBookKeeper,AnnualReport.NoBookKeeper,0);
    if AnnualReport.NoBookKeeper then
      lblDaysWithoutBookKeeper.Caption:=IntToStr(AnnualReport.DaysWithoutBookKeeper)
    else
      lblDaysWithoutBookKeeper.Caption:='';
    SetChecked(lblInActiveCompany,AnnualReport.NonActiveCompany,0);
    edtReportDate.Caption:=DateToStr(AnnualReport.ReportDate);
    edtMemuneName.Caption:=AnnualReport.MemuneName;
    edtMemuneID.Caption:=AnnualReport.MemuneID;
    edtMemuneRole.Caption:=AnnualReport.MemuneRole;
    edtMusmakhName.Caption:=AnnualReport.MusmakhName;
    edtMusmakhID.Caption:=AnnualReport.MusmakhID;
    edtMusmakhRole.Caption:=AnnualReport.MusmakhRole;
    edtAdvMatzhirID.Caption:=AnnualReport.AdvMatzhirID;
    edtAdvMeametFullName.Caption:=AnnualReport.AdvMeametFullName;
    edtAdvMeametAddress.Caption:=AnnualReport.AdvMeametAddress;
    edtAdvMeametID.Caption:=AnnualReport.AdvMeametID;
    edtAdvMeametLicence.Caption:=AnnualReport.AdvMeametLicence;
    edtBkMeametName.Caption:=AnnualReport.BkMeametName;
    edtBkMatzhirID.Caption:=AnnualReport.BkMatzhirID;
    edtBkMeametFullName.Caption:=AnnualReport.BkMeametFullName;
    edtBkMeametAddress.Caption:=AnnualReport.BkMeametAddress;
    edtBkMeametID.Caption:=AnnualReport.BkMeametID;
    edtBkMeametLicence.Caption:=AnnualReport.BkMeametLicence;
  // SetChecked(lblHatzhara,AnnualReport.DateInReport);
    if AnnualReport.DateInReport then
      begin
        lblMatzhir.Caption:=edtMusmakhName.Caption;
        if AnnualReport.SentDate>EncodeDate(1899,12,13) then
          lblSentDate.Caption:=DateToStr(AnnualReport.SentDate)
        else
          lblSentDate.Caption:=space(10);
      end
    else
      begin
        lblMatzhir.Caption:=space(20);
        lblSentDate.Caption:=space(10);
      end;
   edtMemaleName.Caption:=AnnualReport.MemaleName;
   edtMemaleID.Caption:=AnnualReport.MemaleID;
   edtMemaleRole.Caption:=AnnualReport.MemaleRole;
   edtAdvMeametName.Caption:=AnnualReport.AdvMeametName;
   edtAdvMatzhir2.Caption:=AnnualReport.AdvMatzhir2;
   edtbkMatzhir2.Caption:=AnnualReport.bkMatzhir2;

   if Company=nil then exit;

    edtCompanyName.Caption:=Company.Name;
    edtCompanyNumber.Caption:=Company.Zeut;
    edtCompanyAddress.Caption:=Company.CompanyInfo.LawAddress;
    edtCompanyPhone.Caption:=Company.Address.Phone;
    edtCompanyMail.Caption:=Company.CompanyInfo.Mail;
    edtAsefaDate.Caption:=DateToStr(Company.ShowingDate);
    sum:=0;
    for i:=0 to Company.MuhazList.Count-1 do
      sum:=sum+TMuhazItem(Company.MuhazList.Items[i]).Value;
    edtSakhMenayotLamokaz.Caption:=FloatToStr(sum);
  end;
end;

procedure TfrmPrintAnnualReport.CreateMemTables(Empty:boolean);
var
  i,j,v,code:integer;
  hon:double;
begin
  hon:=0.0;
  StartTable(taMemHon);
  if not Empty then
    begin
      for i:=0 to Company.HonList.Count-1 do
        begin
          hon:=hon+THonItem(Company.HonList.items[i]).Rashum*THonItem(Company.HonList.Items[i]).Value;
          taMemHon.Append;
          taMemHon.FieldByName('StockName').AsString:=trim(Company.HonList.Items[i].Name)+' '+FloatToStr(THonItem(Company.HonList.Items[i]).Value) ;
          taMemHon.FieldByName('StockType').AsString:=Company.HonList.Items[i].Zeut;
          taMemHon.FieldByName('StocksNum').AsFloat:=THonItem(Company.HonList.Items[i]).Rashum;
          taMemHon.FieldByName('Mukzot').AsFloat:=THonItem(Company.HonList.Items[i]).Mokza;
          taMemHon.FieldByName('StockValue').AsFloat:= THonItem(Company.HonList.Items[i]).Value;
          taMemHon.Post;
        end;
    end;
  if taMemHon.RecordCount=0 then
    begin
      taMemHon.Append;
      if Empty then
        begin
          taMemHon.FieldByName('StockName').AsString:=EmptyPlace(10);
          taMemHon.FieldByName('StockType').AsString:=EmptyPlace(10);
        end
      else
        begin
          taMemHon.FieldByName('StockName').AsString:='';
          taMemHon.FieldByName('StockType').AsString:='';
        end;
      taMemHon.Post;
    end;
  if Empty then
    edtSakhHon.Caption:=EMPTY_MONEY
  else
    edtSakhHon.Caption:=FloatToStr(hon);
  StartTable(taMemStocks);
  if not Empty then
    begin
      for i:=0 to Company.StockHolders.Count-1 do
        begin
          for j:=0 to TStockHolder(Company.StockHolders.Items[i]).Stocks.Count-1 do
            begin
              taMemStocks.Append;
              taMemStocks.FieldByName('StockHolderName').AsString:=Company.StockHolders.Items[i].Name;
              taMemStocks.FieldByName('id').AsString:=Company.StockHolders.Items[i].Zeut;
              taMemStocks.FieldByName('Address').AsString:=TStockHolder(Company.StockHolders.Items[i]).Address.Street+' '+
                      TStockHolder(Company.StockHolders.Items[i]).Address.Number+' '+
                      TStockHolder(Company.StockHolders.Items[i]).Address.City+' '+
                      TStockHolder(Company.StockHolders.Items[i]).Address.Index+' '+
                      TStockHolder(Company.StockHolders.Items[i]).Address.Country;
              taMemStocks.FieldByName('StocksType').AsString:=
                TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Name;
              taMemStocks.FieldByName('StocksNum').AsFloat:=
                TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count;
              taMemStocks.FieldByName('SumNotPaid').AsFloat:=TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Paid;
              taMemStocks.Post;
            end;
        end;
    end;
   if taMemStocks.RecordCount=0 then
     begin
       taMemStocks.Append;
       if Empty then
         begin
           taMemStocks.FieldByName('StockHolderName').AsString:=EmptyPlace(20);
           taMemStocks.FieldByName('id').AsString:=EmptyPlace(9);
           taMemStocks.FieldByName('Address').AsString:=EmptyPlace(20);
           taMemStocks.FieldByName('StocksType').AsString:=EmptyPlace(10);
         end
       else
         begin
           taMemStocks.FieldByName('StockHolderName').AsString:='';
           taMemStocks.FieldByName('id').AsString:='';
           taMemStocks.FieldByName('Address').AsString:='';
           taMemStocks.FieldByName('StocksType').AsString:='';
         end;
       taMemStocks.Post;
     end;
   StartTable(taMemMokaz);
   if not Empty then
     begin
       for i:=0 to Company.MuhazList.Count-1 do
         begin
           taMemMokaz.Append;
           taMemMokaz.FieldByName('Zeut').AsString:=Company.MuhazList.Items[i].Zeut;
           taMemMokaz.FieldByName('ShtarValue').AsFloat:=TMuhazItem(Company.MuhazList.Items[i]).ShtarValue;
           taMemMokaz.Post;
         end;
     end;
   if taMemMokaz.RecordCount=0 then
     begin
       taMemMokaz.Append;
       if Empty then
         taMemMokaz.FieldByName('Zeut').AsString:=EmptyPlace(9)
       else
         taMemMokaz.FieldByName('Zeut').AsString:='';
       taMemMokaz.Post;
     end;
   StartTable(taMemDirectors);
   if not Empty then
     begin
       Company.Directors.Filter:=afActive;
       for i:=0 to Company.Directors.Count-1 do
         begin
           taMemDirectors.Append;
           taMemDirectors.FieldByName('Zeut').AsString:=Company.Directors.Items[i].Zeut;
           taMemDirectors.FieldByName('Name').AsString:=Company.Directors.Items[i].Name;
           taMemDirectors.FieldByName('StartDate').AsDateTime:=TDirector(Company.Directors.Items[i]).Date1;
           taMemDirectors.FieldByName('Address').AsString:=TDirector(Company.Directors.Items[i]).Address.City+' '+
                                                           TDirector(Company.Directors.Items[i]).Address.Street+' '+
                                                           TDirector(Company.Directors.Items[i]).Address.Number+' '+
                                                           TDirector(Company.Directors.Items[i]).Address.Index;
           taMemDirectors.Post;
        end;
     end;
   if taMemDirectors.RecordCount=0 then
     begin
       taMemDirectors.Append;
       if Empty then
         begin
           taMemDirectors.FieldByName('Zeut').AsString:=EmptyPlace(9);
           taMemDirectors.FieldByName('Name').AsString:=EmptyPlace(20);
           taMemDirectors.FieldByName('Address').AsString:=EmptyPlace(20);
         end
       else
         begin
           taMemDirectors.FieldByName('Zeut').AsString:='';
           taMemDirectors.FieldByName('Name').AsString:='';
           taMemDirectors.FieldByName('Address').AsString:='';
         end;
       taMemDirectors.Post;
     end;
   StartTable(taMemNonDirectors);
   if not Empty then
     begin
       Company.Directors.Filter:=afNonActive;
       for i:=0 to Company.Directors.Count-1 do
         begin
           if Company.Directors.Items[i].LastChanged>=AnnualReport.LastReport then
             begin
               taMemNonDirectors.Append;
               taMemNonDirectors.FieldByName('Zeut').AsString:=Company.Directors.Items[i].Zeut;
               taMemNonDirectors.FieldByName('Name').AsString:=Company.Directors.Items[i].Name;
               taMemNonDirectors.FieldByName('EndDate').AsDateTime:=Company.Directors.Items[i].LastChanged;
               taMemNonDirectors.Post;
             end;
        end;
     end;
   if taMemNonDirectors.RecordCount=0 then
     begin
       taMemNonDirectors.Append;
       if Empty then
         begin
           taMemNonDirectors.FieldByName('Zeut').AsString:=EmptyPlace(9);
           taMemNonDirectors.FieldByName('Name').AsString:=EmptyPlace(20);
         end
       else
         begin
           taMemNonDirectors.FieldByName('Zeut').AsString:='';
           taMemNonDirectors.FieldByName('Name').AsString:='';
         end;
       taMemNonDirectors.Post;
     end;
   StartTable(taMemManagers);
   if not Empty then
     begin
       for i:=0 to Company.Managers.Count-1 do
         begin
           val(TManager(Company.Managers.Items[i]).Zeut,v,code);
           if code<>0 then v:=-1;
           if (v>0)or(trim(TManager(Company.Managers.Items[i]).Name)<>'')then
             begin
               taMemManagers.Append;
               taMemManagers.FieldByName('Name').AsString:=TManager(Company.Managers.Items[i]).Name;
               taMemManagers.FieldByName('Zeut').AsString:=TManager(Company.Managers.Items[i]).Zeut;
               taMemManagers.FieldByName('Address').AsString:=TManager(Company.Managers.Items[i]).Address;
               taMemManagers.FieldByName('Phone').AsString:=TManager(Company.Managers.Items[i]).Phone;
               taMemManagers.Post;
             end;
          end;
     end;
  if taMemManagers.RecordCount=0 then
     begin
       taMemManagers.Append;
       if Empty then
         begin
           taMemManagers.FieldByName('Name').AsString:=EmptyPlace(20);
           taMemManagers.FieldByName('Zeut').AsString:=EmptyPLace(9);
           taMemManagers.FieldByName('Address').AsString:=EmptyPlace(20);
           taMemManagers.FieldByName('Phone').AsString:=EMPTY_PHONE;
         end
       else
         begin
           taMemManagers.FieldByName('Name').AsString:='';
           taMemManagers.FieldByName('Zeut').AsString:='';
           taMemManagers.FieldByName('Address').AsString:='';
           taMemManagers.FieldByName('Phone').AsString:='';
         end;
       taMemManagers.Post;
     end;
end;

procedure TfrmPrintAnnualReport.PrintEmptyProc(DoPrint: Boolean);
begin
  PrintProc(DoPrint);
end;

procedure TfrmPrintAnnualReport.PrintProc(DoPrint: Boolean);
var
  i:integer;
begin
   for i:=1 to 4 do
     fmPrinterSetup.FormatQR(FindComponent('QuickRep'+IntToStr(i)) as TQuickRep);
   fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
   if DoPrint then
       QRCompositeReport1.Print
   else
     QRCompositeReport1.Preview;
 //  Application.ProcessMessages;
end;

procedure TfrmPrintAnnualReport.PrintExecute(const FileName: string; ACompany: TCompany;AAnnualReport:TAnnualReport);
begin
   Company:=ACompany;
   AnnualReport:=AAnnualReport;
   CreateMemTables(False);
   LoadData(False);
   fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfrmPrintAnnualReport.QRSubDetail5BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
    edtSakhMenayotLaMokaz.Enabled:=not detail5printed;
end;

procedure TfrmPrintAnnualReport.QRSubDetail5AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  if BandPrinted then detail5printed:=true;
end;

procedure TfrmPrintAnnualReport.QRSubDetail1AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  if BandPrinted then honPrinted:=true;
end;

procedure TfrmPrintAnnualReport.QRSubDetail1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  lblSakhHon.Enabled:=not honPrinted;
  edtSakhHon.Enabled:=not honPrinted;
end;

procedure TfrmPrintAnnualReport.QRSubDetail8BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  PrintBand:=not detail8printed;
end;

procedure TfrmPrintAnnualReport.QRSubDetail8AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  if BandPrinted then detail8printed:=True;
end;

procedure TfrmPrintAnnualReport.QRSubDetail9BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  PrintBand:=not Detail9printed;
end;

procedure TfrmPrintAnnualReport.QRSubDetail9AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  if BandPrinted then detail9printed:=False;
end;

procedure TfrmPrintAnnualReport.QRCompositeReport1AddReports(
  Sender: TObject);
var
  i:integer;
begin
  for i:=1 to 4 do
    QRCompositeReport1.Reports.Add(FindComponent('QuickRep'+IntToStr(i)) as TQuickRep);
end;

procedure TfrmPrintAnnualReport.QRSubDetail2AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
   if BandPrinted then lbl3printed:=true;
end;

procedure TfrmPrintAnnualReport.QRSubDetail2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  if lbl3printed then lbl3.Enabled:=false;
end;

end.
