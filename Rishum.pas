{
MODIFICATION HISTORY
DATE         BY    TASK      VERSION     DESCRIPTION
12/12/2019   VG    19075     1.7.7       moved btTakanon7 to page 8 and made it visible
}

unit Rishum;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CompanyInfoTemplate, HebForm, ExtCtrls, StdCtrls, Mask, HGrids, SdGrid,
  HComboBx, HEdit, ComCtrls, Buttons, HLabel, DataObjects, HGroupBx,
  HRadGrup, HRadio, HChkBox;

type

  TfmRishum = class(TfmCompanyInfoTemplate)
    TabSheet7: TTabSheet;
    Shape82: TShape;
    Shape90: TShape;
    Shape92: TShape;
    Shape93: TShape;
    Shape94: TShape;
    HebLabel54: THebLabel;
    Shape97: TShape;
    HebLabel55: THebLabel;
    Shape98: TShape;
    HebLabel57: THebLabel;
    Shape100: TShape;
    HebLabel58: THebLabel;
    Shape101: TShape;
    Shape102: TShape;
    Shape103: TShape;
    HebLabel59: THebLabel;
    Shape104: TShape;
    Shape105: TShape;
    HebLabel60: THebLabel;
    Shape106: TShape;
    Shape107: TShape;
    Shape108: TShape;
    Shape109: TShape;
    HebLabel63: THebLabel;
    Shape110: TShape;
    Shape111: TShape;
    Shape112: TShape;
    HebLabel64: THebLabel;
    e62: THebComboBox;
    e64: TMaskEdit;
    e61: THebEdit;
    e63: TEdit;
    e68: TEdit;
    e66: THebEdit;
    e69: THebComboBox;
    e610: THebComboBox;
    e67: THebEdit;
    e65: TEdit;
    HebLabel61: THebLabel;
    HebLabel62: THebLabel;
    btSearch: TButton;
    Shape133: TShape;
    Shape134: TShape;
    HebLabel1: THebLabel;
    btTakanon5: TButton;
    btTakanon2: TButton;
    btTakanon4: TButton;
    HebLabel48: THebLabel;
    edtCompanyName2: THebEdit;
    edtCompanyName3: THebEdit;
    Shape135: TShape;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    HebLabel49: THebLabel;
    edtMail: TEdit;
    Shape136: TShape;
    btnTakanon2_1: TSpeedButton;
    btnTakanon2_2: TSpeedButton;
    btnTakanon2_3: TSpeedButton;
    btnTakanon2_4: TSpeedButton;
    pnB8: TPanel;
    TabSheet8: TTabSheet;
    Shape137: TShape;
    HebLabel50: THebLabel;
    grbLimitedSignatory: THebRadioGroup;
    edtLegalSections: THebEdit;
    edtSignatorySections: THebEdit;
    Shape140: TShape;
    HebLabel75: THebLabel;
    edMagishEmail: TEdit;
    grbSharesRestrictions: THebGroupBox;
    HebLabel51: THebLabel;
    edtSharesRestrictionsChapters: THebEdit;
    rbSharesRestrictionsYes: THebRadioButton;
    rbSharesRestrictionsNo: THebRadioButton;
    grbDontOffer: THebGroupBox;
    HebLabel76: THebLabel;
    edtDontOfferChapters: THebEdit;
    rbDontOfferYes: THebRadioButton;
    rbDontOfferNo: THebRadioButton;
    grpShareHoldersNumber: THebGroupBox;
    HebLabel77: THebLabel;
    edtShareHoldersNumberChapters: THebEdit;
    rbShareHoldersNumberYes: THebRadioButton;
    rbShareHoldersNumberNo: THebRadioButton;
    HebCheckBox1: THebLabel;
    HebCheckBox2: THebLabel;
    HebCheckBox3: THebLabel;
    btTakanon7: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btSearchClick(Sender: TObject);
    procedure btTakanonClick(Sender: TObject);
    procedure btnTakanon2_1Click(Sender: TObject);
    procedure e71Click(Sender: TObject);
    procedure btnTakanon2_2Click(Sender: TObject);
    procedure e72Click(Sender: TObject);
    procedure btnTakanon2_3Click(Sender: TObject);
    procedure btnTakanon2_4Click(Sender: TObject);
    procedure e72bClick(Sender: TObject);
    procedure e72aClick(Sender: TObject);
  //  procedure b1Click(Sender: TObject);
  private
    { Private declarations }
    function CheckSinglePerson(Company: TCompany): boolean ;
    function CheckStockRequirements(Company: TCompany): boolean ;
    function ExecuteTakanon(var Takanon: String; const DefaultTakanon, Caption: String): Boolean;
   // function CheckControls(Draft:boolean): boolean;
  protected
    procedure EvPrintData; override;
    procedure EvFillData(Company: TCompany); override;
    function EvLoadData: Boolean; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvPageIndexChange; override;
    function EvSaveData(Draft: Boolean): boolean; override;
    function EvCollectData: TCompany; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
  public
    { Public declarations }
  end;

var
  fmRishum: TfmRishum;

const
  allowedSingleHonValues: array[0..2,0..1] of Integer =   ((100,1000),(1000,1000),(1000,100000));

implementation

uses
  RisCompany, dialog2, RishumPrintingSelection, RishumRegularRequest, PeopleSearch,
  TakanonEdit, Util;
//VG19075
//, PrintRishum, DataM, HDialogs, prTakanon;

{$R *.DFM}

procedure TfmRishum.EvFillData(Company: TCompany);
begin
  Inherited;
  If Company.Magish <> nil Then
    With Company.Magish Do
    begin
      e61.Text:= Name;
      If Taagid then
        e62.ItemIndex:= 1
      Else
        e62.ItemIndex:= 0;
      e63.Text:= Zeut;
      e65.Text:= Phone;
      e66.Text:= Street;
      e67.Text:= Number;
      e68.Text:= Index;
      e69.Text:= City;
      e610.Text:= Country;
      edMagishEMail.Text:=Mail;
    end;
  // Moshe 31/10/2016 Task 12538
  edtCompanyName2.Text               := Company.CompanyInfo.CompanyName2;
  edtCompanyName3.Text               := Company.CompanyInfo.CompanyName3;
  edtMail.Text                       := Company.CompanyInfo.Mail;
  rbSharesRestrictionsYes.Checked    := (Company.CompanyInfo.SharesRestrictIndex = 0);
  rbDontOfferYes.Checked             := (Company.CompanyInfo.DontOfferIndex = 0);
  rbShareHoldersNumberYes.Checked    := (Company.CompanyInfo.ShareHoldersNoIndex = 0);
  edtSharesRestrictionsChapters.Text := Company.CompanyInfo.SharesRestrictChapters;
  edtDontOfferChapters.Text          := Company.CompanyInfo.DontOfferChapters;
  edtShareHoldersNumberChapters.Text := Company.CompanyInfo.ShareHoldersNoChapters;
  grbLimitedSignatory.ItemIndex      := Company.CompanyInfo.LimitedSignatoryIndex;
  edtLegalSections.Text              := Company.CompanyInfo.LegalSectionsChapters;
  edtSignatorySections.Text          := Company.CompanyInfo.SignatorySectionsChapters;
  rbSharesRestrictionsNo.Checked     := (not rbSharesRestrictionsYes.Checked);
  rbDontOfferNo.Checked              := (not rbDontOfferYes.Checked);
  rbShareHoldersNumberNo.Checked     := (not rbShareHoldersNumberYes.Checked);
end; // TfmRishum.EvFillData

function TfmRishum.EvLoadData: Boolean;
var
  FileItem: TFileItem;
begin
  Result := False;
  FileItem := fmRisCompany.Execute(Self);
  if (FileItem = nil) then
    Exit;
  Company.Free;
  Company := TCompany.LoadFromFile(nil, FileItem);
  EvFillData(Company);
  e64.Text := DateToLongStr(FileItem.FileInfo.DecisionDate);
  Result := True;
  FileName := FileItem.FileInfo.FileName;
  FileIsDraft := FileItem.FileInfo.Draft;
  FileItem.Free;
end;

function TfmRishum.EvSaveData(Draft: Boolean): Boolean;
var
  FileInfo: TFileInfo;
  FileItem: TFileItem;
begin
  //VG19075
  Result := True;
  try
    e12a.Text := FormatDateTime('dd/mm/yyyy', Now);
    FFilesList := TFilesList.Create(-1, faRishum, Draft);

    if (Assigned(Company)) then Company.Free;
    Company := EvCollectData;
    FileInfo := TFileInfo.Create;
    FileInfo.FileName := e11.Text;
    FileInfo.DecisionDate := StrToDate(e64.Text);
    FileInfo.NotifyDate := StrToDate(e64.Text);
    FileInfo.Draft := Draft;
    FileInfo.Action := faRishum;
    FileInfo.CompanyNum := -1;
    FFilesList.Add(FileInfo);
    FileItem := TFileItem.Create(FFilesList, FileInfo);
    Company.ExSaveData(FileItem, nil);

    FFilesList.Free;

    FileName := FileItem.FileInfo.FileName;
    FileIsDraft := Draft;
    FileItem.Free;
    fmDialog2.Show;
  //VG19075
  //  Result := True;
  except
  end;
end;

procedure TfmRishum.EvPageIndexChange;
begin
  Inherited;
  try
    If PageIndex=6 Then
      e61.SetFocus;
  except
  end;
end;

procedure TfmRishum.EvFocusNext(Sender: TObject);
begin
  Inherited EvFocusNext(Sender);
  if Sender = e79 then btTakanon2.SetFocus;
  if Sender = e61 then e62.SetFocus;
  if Sender = e62 then e63.SetFocus;
  if Sender = e63 then e64.SetFocus;
  if Sender = e64 then e65.SetFocus;
  if Sender = e65 then e66.SetFocus;
  if Sender = e66 then e67.SetFocus;
  if Sender = e67 then e68.SetFocus;
  if Sender = e68 then e69.SetFocus;
  if Sender = e69 then e610.SetFocus;
end;

procedure TfmRishum.FormCreate(Sender: TObject);
const
  MagishText:string='����';
var
  lLeft: integer;
begin
  Loading:= True;
  inherited;
  FieldNames.Add(HebLabel49.Caption+'='+edtMail.Name);//company name
  FieldNames.Add(HebLabel57.Caption+'='+e61.Name); //magish name
  FieldNames.Add(MagishText+' '+HebLabel54.Caption+'='+e62.Name);//magish taagid?
  FieldNames.Add(MagishText+':'+HebLabel58.Caption+'='+e63.Name);//magish zehut/taagid
  FieldNames.Add(HebLabel55.Caption+'='+e64.Name);//submission date
  FieldNames.Add(MagishText+':'+HebLabel64.Caption+'='+e65.Name);//submitter phone
  FieldNames.Add(MagishText+':'+HebLabel61.Caption+'='+e66.Name);//submitter street
  FieldNames.Add(MagishText+':'+HebLabel60.Caption+'='+e67.Name);//submitter house
  FieldNames.Add(MagishText+':'+HebLabel59.Caption+'='+e68.Name);//submitter ZIP
  FieldNames.Add(MagishText+':'+HebLabel62.Caption+'='+e69.Name);//submitter city
  FieldNames.Add(MagishText+':'+HebLabel63.Caption+'='+e610.Name);//submitter country
  FieldNames.Add(MagishText+':'+HebLabel75.Caption+'='+edMagishEmail.Name);

  Application.HintHidePause:=7000;

  e62.ItemIndex:=0;
  e610.Items.AddStrings(e38.Items);
  e69.Items.AddStrings(e37.Items);
  e610.Text:= '�����';

  lLeft := pnB5.Left;
  pnB5.Left := pnB6.Left;
  pnB6.Left := pnB7.Left;
  pnB7.Left := pnB8.Left;
  pnB8.Left := lLeft;
  pnB8.Caption := '���� 175';

  Loading:= False;
  PageControl.Enabled:=True;
  if not PageControl.Enabled then
    ShowMessage('PageControl disabled')
  else if not PageControl.Visible then
    ShowMessage('PageControl unvisible');
end;

function TfmRishum.EvCollectData: TCompany;
var
  i, j: integer;
  HonList: THonList;
  MuhazList: TMuhazList;
  StockHolders: TStockHolders;
  Stocks: TStocks;
  Directors: TDirectors;
  Managers: TManagers;
  Option1: Byte;
  Option3: char;
  CompanyInfo: TCompanyInfo;
  Magish: TMagish;
  ASubmissionData: TSubmissionData;
  Taagid: TTaagid;
begin
  WriteChangeLog( #9 + 'Collect data start');
  //==> Tsahi 06/11/05. added so drafts can be saved with 00/00/0000 dates
  Date:= StrToDateDef(e12a.Text,0);
  //==>
  // Page 2 - Saving HonList
  WriteChangeLog( #9 + 'Saving HonList');
  HonList:= THonList.CreateNew;
  For I:= 0 To Grid2.RowCount-1 Do
    With Grid2.Rows[I] Do
      HonList.Add( THonItem.CreateNew(Date, Strings[0], MyStrToFloat(Strings[1]),
        MyStrToFloat(Strings[2]), MyStrToFloat(Strings[3]), MyStrToFloat(Strings[4]),
        MyStrToFloat(Strings[5]), MyStrToFloat(Strings[6]), MyStrToFloat(Strings[7]), True));

  // Saving Muhaz List
  WriteChangeLog( #9 + 'Saving Muhaz List');
  MuhazList:= TMuhazList.CreateNew;
  For I:= 0 to Grid2a.RowCount-1 do
    with Grid2a.Rows[I] do
      If Trim(Strings[0]) <> '' then
        MuhazList.Add( TMuhazItem.CreateNew(Date, Strings[0], Strings[2], MyStrToFloat(Strings[1]), MyStrToFloat(Strings[3]), True));

  // Page 3 - Saving StockHolders
  WriteChangeLog( #9 + 'Saving StockHolders');
  StockHolders := TStockHolders.CreateNew;
  Save3(StrToInt(e31.Text));
  For I:= 0 To TempGrid31.RowCount-1 Do
    If Trim(TempGrid31.Cells[0,I]) <> '' then
    begin
      // Phase 1 - Collecting Stocks
      Stocks:= TStocks.CreateNew;
      With TempGrid32 Do
        For J:= 0 To RowCount-1 Do
          If Cells[0,J] = TempGrid31.Cells[0,I] Then
            Stocks.Add( TStock.CreateNew(Date, Cells[1,J], MyStrToFloat(Cells[2,J]), MyStrToFloat(Cells[3,J]), MyStrToFloat(Cells[4,J]), True));

      // Phase 2 - Adding Person Info
      With TempGrid31 do
        StockHolders.Add(TStockHolder.CreateNew(Date, Cells[1,I], Cells[2,I], Cells[8,I] = '1',
          TStockHolderAddress.CreateNew(Cells[3,I], Cells[4,I], Cells[5,I], Cells[6,I], Cells[7,I]),
          Stocks, True));
    end;
  // Page 4  - Saving Directors
  WriteChangeLog( #9 + 'Saving Directors');
  Directors:= TDirectors.CreateNew;
  Save4(StrToInt(e41.Text));
  With TempGrid4 do
    For I:= 0 To RowCount-1 Do
      If Trim(Cells[0,I]) <> '' Then
      begin
        // Check - If Taagid - Then Load it's data
        If Cells[1,I] = '1' Then
          Taagid:= TTaagid.CreateNew(Cells[14,I], Cells[15,I], Cells[16,I], Cells[17,I], Cells[18,I], Cells[12,I], Cells[13,I])
        Else
          Taagid:= Nil;
        //==> Tsahi 06/11/05. added StrToDateDef so drafts can be saved with 00/00/0000 dates
        Directors.Add( TDirector.CreateNew(Date, Cells[4,I], Cells[5,I], StrToDateDef(Cells[2,I],0), StrToDateDef(Cells[3,I],0),
          TDirectorAddress.CreateNew(Cells[7,I], Cells[8,I], Cells[9,I], Cells[10,I], Cells[11,I], Cells[6,I]),
          Taagid, True));
        //<==
      end;
  // Page 5 - Saving Managers
  WriteChangeLog( #9 + 'Saving Managers');
  Managers := TManagers.CreateNew;
  With Grid5 do
    For I:= 0 To RowCount-1 Do
      Managers.Add(TManager.CreateNew(Date, Cells[0,I], Cells[1,I], Cells[2,I], Cells[3,I], Cells[4,I]));

  // Page 6 - Saving Options
  WriteChangeLog( #9 + 'Saving Options');
  Option1:= 0;

  if e71.Checked then
    Option1:= Option1 or 1;

  if e72.Checked then
    Option1:= Option1 or 2;

  if e72a.Checked then
    Option1:= Option1 or 4;

  If e72b.Checked then
    Option1:= Option1 or 8;

  if e73.Checked then
    Option3 := '1'
  Else if e75.Checked then
    Option3 := '3'
  else
    Option3 := '2';

  // Moshe 01/11/2016 Task 12538
  // Create CompanyInfo
  CompanyInfo := TCompanyInfo.CreateNew(Date);
  CompanyInfo.Option1          := Option1;
  CompanyInfo.Option3          := Option3;
  CompanyInfo.OtherInfo        := e75a.Text;
  CompanyInfo.SigName          := e51.Text;
  CompanyInfo.SigZeut          := e52.Text;
  CompanyInfo.SigTafkid        := e53.Text;
  CompanyInfo.LawName          := e76.Text;
  CompanyInfo.LawAddress       := e78.Text;
  CompanyInfo.LawZeut          := e77.Text;
  CompanyInfo.LawLicence       := e79.Text;
  CompanyInfo.Takanon2_1       := FTakanon2_1;
  CompanyInfo.Takanon2_2       := FTakanon2_2;
  CompanyInfo.Takanon2_3       := FTakanon2_3;
  CompanyInfo.Takanon2_4       := FTakanon2_4;
  CompanyInfo.Takanon4         := FTakanon4;
  CompanyInfo.Takanon5         := FTakanon5;
  CompanyInfo.Takanon7         := FTakanon7;
  CompanyInfo.EnglishName      := edEnglishName.Text;
  CompanyInfo.RegistrationDate := Date;
  CompanyInfo.CompanyName2     := edtCompanyName2.Text;
  CompanyInfo.CompanyName3     := edtCompanyName3.Text;
  CompanyInfo.Mail             := edtMail.Text;

  CompanyInfo.SharesRestrictIndex := iif(rbSharesRestrictionsYes.Checked, 0, 1);
  if (rbSharesRestrictionsYes.Checked) then
    CompanyInfo.SharesRestrictChapters := edtSharesRestrictionsChapters.Text;

  CompanyInfo.DontOfferIndex := iif(rbDontOfferYes.Checked, 0, 1);
  if (rbDontOfferYes.Checked) then
    CompanyInfo.DontOfferChapters := edtDontOfferChapters.Text;

  CompanyInfo.ShareHoldersNoIndex := iif(rbShareHoldersNumberYes.Checked, 0, 1);
  if (rbShareHoldersNumberYes.Checked) then
    CompanyInfo.ShareHoldersNoChapters := edtShareHoldersNumberChapters.Text;

  CompanyInfo.LimitedSignatoryIndex := grbLimitedSignatory.ItemIndex;
  CompanyInfo.LegalSectionsChapters := edtLegalSections.Text;
  CompanyInfo.SignatorySectionsChapters := edtSignatorySections.Text;
  CompanyInfo.ConfirmedCopies := '0';
  CompanyInfo.MoreCopies := '0';

  ASubmissionData := TSubmissionData.CreateNew(
      StrToDate(e64.Text),
      e61.Text,
      '',
      e65.Text,
      edMagishEMail.Text,
      edtCompanyName2.Text,
      '',
      edtCompanyName3.Text,
      '','','',
      False,
      edtMail.Text,
      '',
      False,
      0,
      0,
      0,
      e18.Text,
      True);

   Magish := TMagish.CreateNew(e61.Text, e63.Text, e66.Text, e67.Text, e68.Text, e69.Text, e610.Text, e65.Text,
      edMagishEMail.Text, '', (e62.ItemIndex = 1));

  // Final - Saving all data!
  WriteChangeLog(#9 + 'Final - Saving all data');

  Result:= TCompany.CreateNew(
      Date,
      e11.Text,
      e12.Text,
      TCompanyAddress.CreateNew(e13.Text, e14.Text, e16.Text, e15.Text, '�����', e17.Text, e18.Text, e19.Text, e110.Text),
      Directors,
      StockHolders,
      HonList,
      MuhazList,
      Managers,
      CompanyInfo,
      ASubmissionData,
      Magish);
  WriteChangeLog(#9 + 'Collect data end');
end; // TfmRishum.EvCollectData

procedure TfmRishum.EvPrintData;
var
  Company: TCompany;
begin
  CurrRow := -1;
  EvDeleteEmptyRows;
  Company := EvCollectData;
  if DataChanged then
  begin
    if CheckErrors(False, not FFromDraft, FFromDraft) then
      Exit;
    if not EvSaveData(FFromDraft) then
      Exit;
    DataChanged := False;
  end;

  if (CheckSinglePerson(Company) and CheckStockRequirements(Company)) then
  begin
    try
      Application.CreateForm(TfmRishumPrintingSelection, fmRishumPrintingSelection);
      fmRishumPrintingSelection.Open(FileName, Company, StrToDate(e64.Text));
    finally
      fmRishumPrintingSelection.Free;
    end;
  end
  else
  begin
    try
      Application.CreateForm(TfmRishumRegularRequest, fmRishumRegularRequest);
      fmRishumRegularRequest.Open(FileName, Company, StrToDate(e64.Text));
    finally
      fmRishumRegularRequest.Free;
    end;
  end;

  Self.SubmissionData := Company.SubmissionData;
end;

procedure TfmRishum.btSearchClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= EvCollectData;
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  If FResult = nil Then
  begin
    e61.SetFocus;
    TempCompany.Free;
    Exit;
  end;

  Loading:= True;
  If FResult is TStockHolder Then
    With FResult as TStockHolder do
    begin
      if Taagid Then
        e62.ItemIndex:= 1
      Else
        e62.ItemIndex:= 0;
      e61.Text:= Name;
      e63.Text:= Zeut;
      e65.Text:= '';
      e66.Text:= Address.Street;
      e67.Text:= Address.Number;
      e68.Text:= Address.Index;
      e69.Text:= Address.City;
      e610.Text:= Address.Country;
    end;

  if FResult is TDirector Then
    With FResult as TDirector do
    begin
      if Taagid<> nil Then
        e62.ItemIndex:= 1
      Else
        e62.ItemIndex:= 0;
      e61.Text:= Name;
      e63.Text:= Zeut;
      e65.Text:= Address.Phone;
      e66.Text:= Address.Street;
      e67.Text:= Address.Number;
      e68.Text:= Address.Index;
      e69.Text:= Address.City;
      e610.Text:= Address.Country;
    end;
  TempCompany.Free;
  Loading:= False;
  DataChanged:= True;
  e61.SetFocus;
end;

function TfmRishum.ExecuteTakanon(var Takanon: String; const DefaultTakanon, Caption: String): Boolean;
begin
  if (Takanon = '') then
    Takanon := DefaultTakanon;
  Application.CreateForm(TfmTakanonEdit, fmTakanonEdit);
  //VG19075
  fmTakanonEdit.Initialize;
  fmTakanonEdit.MaxLines := C_DEFAULT_MAX_LINES; //defined in TakanonEdit

  Result := fmTakanonEdit.Execute(Takanon, '����� ����� - ����� ' + Caption);
  fmTakanonEdit.Free;
  fmTakanonEdit := nil;
end;

procedure TfmRishum.btTakanonClick(Sender: TObject);
begin
  inherited;                          // FTakanon2 declared in CompanyInfoTemplate.pas
  case (Sender as TButton).Tag of
    2: DataChanged := ExecuteTakanon(FTakanon2, DefaultTakanon2, (Sender as TButton).Caption);
    4: DataChanged := ExecuteTakanon(FTakanon4, DefaultTakanon4, (Sender as TButton).Caption);
    5: DataChanged := ExecuteTakanon(FTakanon5, DefaultTakanon5, (Sender as TButton).Caption);
    7: DataChanged := ExecuteTakanon(FTakanon7, DefaultTakanon7, (Sender as TButton).Caption);
  end;
end;

function TfmRishum.CheckSinglePerson(Company: TCompany): boolean;
var
  singleZeut, currentZeut : string;
begin
  result := false;

  if ((Company.Directors.Count = 1) AND
      (Company.StockHolders.Count = 1) ) then
  begin
    singleZeut := Company.Directors.Items[0].Zeut;

    currentZeut := Company.StockHolders.Items[0].Zeut;
    if(singleZeut = currentZeut ) then
    begin
      singleZeut := currentZeut;
      result := true;
    end
    else
    begin
      result := false;
    end;

    currentZeut := Company.Magish.Zeut;
    if( (result) and (singleZeut = currentZeut )) then
    begin
      singleZeut := currentZeut;
      result := true;
    end
    else
    begin
      result := false;
    end;
  end;
end;

function TfmRishum.CheckStockRequirements(Company: TCompany): boolean ;
var
 i: integer;
 item: THonItem;
begin
  result := false;
  if (Company.HonList.Count = 1) then
  begin
    item := THonItem (Company.HonList.Items[0]);

    i := 0;
    while(not result) and (i <= 2 ) do
    begin
      // if hon rashum and hon mukze are correct, go on
      if (item.Rashum = allowedSingleHonValues[i,1]) and
         (item.Mokza = allowedSingleHonValues[i,0]) and
         (item.Value = 1) then //only stocks with 1 shekel value
      begin
        result := true;
      end;
      i := i + 1;
    end;
  end;
end;

procedure TfmRishum.btnTakanon2_1Click(Sender: TObject);
begin
  if (e71.Checked) then
  begin
    DataChanged := True;
    ExecuteTakanon(FTakanon2_1, DefaultTakanon2, btnTakanon2_1.Caption);
  end;
end;

procedure TfmRishum.e71Click(Sender: TObject);
begin
  inherited;
 // if (not Loading) then
   // btnTakanon2_1Click(Sender);
end;

procedure TfmRishum.btnTakanon2_2Click(Sender: TObject);
begin
  if (e72.Checked) then
  begin
    DataChanged := True;
    ExecuteTakanon(FTakanon2_2, DefaultTakanon2, btnTakanon2_2.Caption);
  end;
end;

procedure TfmRishum.e72Click(Sender: TObject);
begin
  inherited;
  if (not Loading) then
    btnTakanon2_2Click(Sender);
end;

procedure TfmRishum.btnTakanon2_3Click(Sender: TObject);
begin
  if (e72a.Checked) then
  begin
    DataChanged := True;
    ExecuteTakanon(FTakanon2_3, DefaultTakanon2, btnTakanon2_3.Caption);
  end;
end;

procedure TfmRishum.e72aClick(Sender: TObject);
begin
  inherited;
  if (not Loading) then
    btnTakanon2_3Click(Sender);
end;

procedure TfmRishum.btnTakanon2_4Click(Sender: TObject);
begin
  if (e72b.Checked) then
  begin
    DataChanged := True;
    ExecuteTakanon(FTakanon2_4, DefaultTakanon2, btnTakanon2_4.Caption);
  end;
end;

procedure TfmRishum.e72bClick(Sender: TObject);
begin
  inherited;
  if (not Loading) then
    btnTakanon2_4Click(Sender);
end;

procedure TfmRishum.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
const
  C_Must_Choose      = '���� ����� ����� ������';
  C_Must_Fill        = '���� ���� �� ����';
  C_Must_Be_Positive = '����� ����� ����� ����';
begin
  ErrorAt := nil;
  if Draft then
    Exit;
  inherited EvCheckPage(Page, Silent, Extended, Draft);
  if ErrorAt <> nil then
    Exit;
  case Page of
    5:begin
        if (not (e71.Checked or e72.Checked or e72a.Checked or e72b.Checked)) then
        begin
          pnB1Click(pnB6);
          ErrorAt:=e71;
          raise Exception.Create(HebLabel65.Caption+': '+#10+#13+C_Must_Fill);
          Exit;
        end;
      end;
    6:begin
        if grbLimitedSignatory.ItemIndex<0 then
        begin
          pnB1Click(pnB7);
          ErrorAt:=grbLimitedSignatory;
          raise Exception.Create(grbLimitedSignatory.Caption+': '+#10+#13+C_Must_Choose);
          Exit;
        end;
        if Trim(e65.Text)='' then
        begin
          pnB1Click(pnB7);
          ErrorAt:=e65;
          raise Exception.Create(HebLabel64.Caption+': '+#10+#13+C_Must_Fill);
          Exit;
        end;
        if Trim(edMagishEmail.Text)='' then
        begin
          pnB1Click(pnB7);
          ErrorAt := edMagishEmail;
          raise Exception.Create(HebLabel75.Caption + ': ' + #10+#13 + C_Must_Fill);
          Exit;
        end;
      end;
    7:begin
        if (not (rbSharesRestrictionsYes.Checked or rbSharesRestrictionsNo.Checked)) then
        begin
          pnB1Click(pnB8);
          ErrorAt := grbSharesRestrictions;
          raise Exception.Create(grbSharesRestrictions.Caption + ':' + #10+#13 + C_Must_Choose);
          Exit;
        end;
        if (not (rbDontOfferYes.Checked or rbDontOfferNo.Checked)) then
        begin
          pnB1Click(pnB8);
          ErrorAt := grbDontOffer;
          raise Exception.Create(grbDontOffer.Caption + ':' + #10+#13 + C_Must_Choose);
          Exit;
        end;
        if (not (rbShareHoldersNumberYes.Checked or rbShareHoldersNumberNo.Checked)) then
        begin
          pnB1Click(pnB8);
          ErrorAt := grpShareHoldersNumber;
          raise Exception.Create(grpShareHoldersNumber.Caption + ':' + #10+#13 + C_Must_Choose);
          Exit;
        end;
      end;
  end;
end;

end.

