unit RishumPrintingSelection;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, HRadio, HGroupBx, Buttons, ExtCtrls, HebForm, DataObjects;

type
  TfmRishumPrintingSelection = class(TForm)
    HebForm1: THebForm;
    pnlButtons: TPanel;
    bnCancel: TBitBtn;
    bnPrint: TBitBtn;
    HebGroupBox1: THebGroupBox;
    radioRegularPrinting: THebRadioButton;
    radioQuickPrinting: THebRadioButton;
    procedure bnCancelClick(Sender: TObject);
    procedure bnPrintClick(Sender: TObject);
  private
    { Private declarations }
    FileName: string;
    Company :TCompany;
    DecisionDate: TDateTime ;
  public
    { Public declarations }
    procedure Open(LoadingFileName: string; LoadingCompany :TCompany; LoadingDecisionDate: TDateTime);
  end;

var
  fmRishumPrintingSelection: TfmRishumPrintingSelection;

implementation

uses RishumRegularRequest, RishumQuickRequest;

{$R *.DFM}

procedure TfmRishumPrintingSelection.bnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfmRishumPrintingSelection.bnPrintClick(Sender: TObject);
begin
if (Self.radioRegularPrinting.Checked) then
   begin
      Application.CreateForm(TfmRishumRegularRequest,fmRishumRegularRequest);
      fmRishumRegularRequest.Open(FileName, Company, DecisionDate);
   end else
   begin
      Application.CreateForm(TfmRishumQuickRequest,fmRishumQuickRequest);
      fmRishumQuickRequest.Open(FileName, Company);
      fmRishumQuickRequest.Destroy;
   end;
   Close; //on printing complete, close this screen
end;

procedure TfmRishumPrintingSelection.Open(LoadingFileName: string; LoadingCompany :TCompany; LoadingDecisionDate: TDateTime);
begin
    Self.FileName := LoadingFileName;
    Self.Company :=    LoadingCompany  ;
    Self.DecisionDate  :=    LoadingDecisionDate   ;

    ShowModal;
end;

end.
