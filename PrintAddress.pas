unit PrintAddress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls, DataObjects;

type
  TfmQRAddress = class(TForm)
    qrAddress: TQuickRep;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel8: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    l1: TQRLabel;
    l2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRLabel15: TQRLabel;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    l3: TQRLabel;
    QRLabel30: TQRLabel;
    l9: TQRLabel;
    QRLabel32: TQRLabel;
    l10: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    l11: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel62: TQRLabel;
    l12: TQRLabel;
    l13: TQRLabel;
    QRLabel65: TQRLabel;
    l4: TQRLabel;
    l5: TQRLabel;
    l6: TQRLabel;
    l7: TQRLabel;
    l8: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape15: TQRShape;
    QRLabel9: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel40: TQRLabel;
    QuickRep1: TQuickRep;
    QRBand3: TQRBand;
    QRImage3: TQRImage;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRImage4: TQRImage;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRLabel47: TQRLabel;
    edtCompanyName: TQRLabel;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRLabel49: TQRLabel;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    edtCompanyZeut: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRShape23: TQRShape;
    QRLabel57: TQRLabel;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape29: TQRShape;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    edtStreet: TQRLabel;
    edtNumber: TQRLabel;
    edtCity: TQRLabel;
    edtZIP: TQRLabel;
    edtEzel: TQRLabel;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRShape34: TQRShape;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel71: TQRLabel;
    edtPOB: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRShape22: TQRShape;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    edtMail: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel91: TQRLabel;
    QRShape28: TQRShape;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    edtSignTafkid: TQRLabel;
    edtSignZeut: TQRLabel;
    edtSignName: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    edtDate: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRShape38: TQRShape;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRShape39: TQRShape;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRBand4: TQRBand;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRShape40: TQRShape;
    QRLabel173: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure PrintEmpty(const FileName: String);
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintExecute(const FileName: String; Company: TCompany; Address: TCompanyAddress; NotifyDate: TDateTime; NeedPrompt: Boolean);
  end;

var
  fmQRAddress: TfmQRAddress;

implementation

uses Util, PrinterSetup;

{$R *.DFM}

procedure TfmQRAddress.PrintEmpty(const FileName: String);
begin
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQRAddress.PrintExecute(const FileName: String; Company: TCompany; Address: TCompanyAddress; NotifyDate: TDateTime; NeedPrompt: Boolean);
begin
  edtCompanyName.Caption  := Company.Name;
  If NeedPrompt then
    edtCompanyZeut.Caption:= Company.Zeut;
  edtStreet.Caption       := Address.Street;
  edtNumber.Caption       := Address.Number;
  edtCity.Caption         := Address.City;
  edtZIP.Caption          := Address.Index;
  edtPOB.Caption          := Address.POB;
  edtEzel.Caption         := Address.Ezel;
  edtMail.Caption         := Company.CompanyInfo.Mail;
  edtSignName.Caption     := Company.CompanyInfo.SigName;
  edtSignZeut.Caption     := Company.CompanyInfo.SigZeut;
  edtSignTafkid.Caption   := Company.CompanyInfo.SigTafkid;
  edtDate.Caption         := DateToLongStr(NotifyDate);

  if NeedPrompt then
  begin
    Address.Free;
    fmPrinterSetup.Execute(FileName, PrintProc);
  end;
end;

procedure TfmQRAddress.PrintProc(DoPrint: boolean);
begin
  fmPrinterSetup.FormatQR(QuickRep1);
  if DoPrint then
    QuickRep1.Print
  else
    QuickRep1.Preview;
end;

end.
