{
MODIFICATION HISTORY
DATE         BY    TASK      VERSION     DESCRIPTION
}
unit Companies;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  LoadSaveTemplate, HebForm, StdCtrls, HEdit, ExtCtrls, HGrids, SdGrid,
  HLabel, DataObjects, Db, DBTables;

type
  TfmCompanies = class(TfmLoadSaveTemplate)
    btPrint: TButton;
    taTemp: TTable;
    procedure sdNamesSelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure btOkClick(Sender: TObject);
    procedure edNameKeyPress(Sender: TObject; var Key: Char);
    procedure btPrintClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    DoNotUpdate: Boolean;
    CompaniesList: TCompanies;
    Company: TCompany;
    procedure FillData;
  public
    { Public declarations }
    function Execute(Const ACaption: String; ShowStyle: TActivityFilter): TCompany;
  end;

var
  fmCompanies: TfmCompanies;

implementation

uses
  Util, dialog, PrintCompanies;

{$R *.DFM}

function TfmCompanies.Execute(Const ACaption: String; ShowStyle: TActivityFilter): TCompany;
begin
  Caption := ACaption;
  edName.Text := '';
  edNumber.Text := '';
  If Assigned(CompaniesList) Then
    CompaniesList.Free;
  CompaniesList := TCompanies.Create(EncodeDate(3000, 1, 1), False, False);
  CompaniesList.Filter := ShowStyle;
  Company := Nil;
  FillData;
  ShowModal;
  Result := Company;
end;

procedure TfmCompanies.FillData;
var
  I: Integer;
begin
  sdNames.ResetLine(0);
  sdNames.RowCount := CompaniesList.Count;
  For I := 0 To CompaniesList.Count - 1 Do
    begin
     sdNames.Cells[0, I] := CompaniesList.Names[I];
     if CompaniesList.Numbers[I] = '-1' then
       sdNames.Cells[1, I] := '����'
     Else
       sdNames.Cells[1, I] := CompaniesList.Numbers[I];
    end;
  SortGrid(sdNames, '0,STRING,UP');
  sdNames.Repaint;
end;

procedure TfmCompanies.sdNamesSelectCell(Sender: TObject; Col,
  Row: Integer; var CanSelect: Boolean);
begin
  If DoNotUpdate Then
    Exit;
  inherited;
  edName.Text := sdNames.Cells[0,Row];
  edNumber.Text := sdNames.Cells[1,Row];
end;

procedure TfmCompanies.btOkClick(Sender: TObject);
var
  i, Line: Integer;
begin
  inherited;

  Line := -1;
  if edName.Text <> '' Then
  begin
    for i := 0 to CompaniesList.Count-1 do
    begin
      if CompaniesList.Names[i] = edName.Text then
        Line := i;
    end;
  end
  else if (edNumber.Text <> '') and (edNumber.Text <> '����') then
  begin
    for i := 0 to CompaniesList.Count-1 do
    begin
      if CompaniesList.Numbers[i] = edNumber.Text then
        Line := i;
    end;
  end;

  if Line = -1 then
  begin
    fmDialog.ActiveControl := fmDialog.Button3;
    fmDialog.Caption := '�����';
    fmDialog.memo.lines.Clear;
    fmDialog.memo.lines.Add('��� ��');
    fmDialog.memo.lines.Add('���� �� �� ����� ����� �������');
    fmDialog.Button1.Visible := False;
    fmDialog.Button2.Visible := False;
    fmDialog.Button3.Caption := '�����';
    fmDialog.ShowModal;
    fmDialog.Button3.Caption := '�����';
    fmDialog.Button1.Visible := True;
    fmDialog.Button2.Visible := True;
    fmDialog.Caption := '����';
  end
  else
  begin
    Company := CompaniesList.Items[Line];
    Close;
  end;
end; // TfmCompanies.btOkClick

procedure TfmCompanies.edNameKeyPress(Sender: TObject; var Key: Char);
type
  TLegals = set of char;
Const
  Legals : TLegals = ['0','1','2','3','4','5','6','7','8','9','/',#8];
Var
  Tmp: String;
  I: Integer;
  Col: Integer;
begin
  inherited;
  If (Key = #13) Then
    If (Sender = edName) Then
    begin
      edNumber.SetFocus;
      Key := #0;
    end
    Else
    begin
      btOk.Click;
    end;

  If (Sender = edNumber) And (Not (Key in Legals)) then
    Key := #0;

  If (Sender As TCustomEdit).SelLength>0 then
  begin
     Tmp := (Sender As TCustomEdit).Text;
     Delete(Tmp, (Sender As TCustomEdit).SelStart+1, (Sender As TCustomEdit).SelLength);
     Tmp := Tmp + Key;
  end
  else
    Tmp :=(Sender As TCustomEdit).Text + Key;

  If Sender = edNumber then
    Col := 1
  Else
    Col := 0;

  I := 0;
  While (Pos(Tmp, sdNames.Cells[Col,I]) <> 1) and (I < sdNames.RowCount) Do
    Inc(I);
  if I < sdNames.RowCount then
  begin
    edName.Text := sdNames.Cells[0, I];
    edNumber.Text := sdNames.Cells[1, I];
    (Sender As TCustomEdit).SelStart := Length(Tmp);
    (Sender As TCustomEdit).SelLength := Length((Sender As TCustomEdit).Text) - Length(Tmp);
    DoNotUpdate := True;
    try
      sdNames.Row := I;
    finally
      DoNotUpdate := False;
    end;
    Key := #0;
  end
  Else If Sender = edName then
    edNumber.Text := ''
  Else
    edName.Text := '';
end;

procedure TfmCompanies.btPrintClick(Sender: TObject);
var
  I,J: Integer;
begin
  inherited;
  with taTemp.FieldDefs do
  begin
    Clear;
    Add('Name', ftString, 80, False);
    Add('Zeut', ftString, 15, False);
  end;
  taTemp.CreateTable;
  taTemp.Open;
  For I := 0 To sdNames.RowCount - 1 do
  begin
    taTemp.Append;
    For J:= 0 To sdNames.ColCount - 1 do
      taTemp.Fields[J].AsString := MakeHebStr(sdNames.Cells[J,I], False);
    taTemp.Post;
  end;
  taTemp.Close;
  Application.CreateForm(TfmQRCompanies, fmQRCompanies);
  try
    fmQRCompanies.PrintExecute(HebLabel19.Caption, HebLabel1.Caption);
  finally
    fmQRCompanies.Free;
  end;
end;

procedure TfmCompanies.FormShow(Sender: TObject);
begin
  inherited;
  edName.SetFocus;
end;

end.
