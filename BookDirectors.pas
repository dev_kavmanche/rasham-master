unit BookDirectors;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BookStock, HebForm, ExtCtrls, HGrids, SdGrid, ComCtrls, Buttons,
  StdCtrls, Mask, HLabel, DataObjects, Db, DBTables;

type
  TfmBookDirectors = class(TfmBookStocks)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function FillEnum1(Line: Integer; ChangeDate: TDateTime; Param: Pointer): Boolean;
    function FillEnum2(Line: Integer; ChangeDate: TDateTime; Param: Pointer): Boolean;
  protected
    procedure EvPrintData; override;
    procedure EvFillData1; override;
    procedure EvFillData2; override;
    procedure EvFilldefaultData; override;
  public
    { Public declarations }
  end;

var
  fmBookDirectors: TfmBookDirectors;

implementation

uses Util, cSTD, PrintDirectorBook1, PrintDirectorBook2;
{$R *.DFM}

procedure TfmBookDirectors.EvFilldefaultData;
begin                                                                    
  Inherited;
  Grid2.Rows[0].CommaText:= '"�����","�����","��","��'' ����","����","��'' ���","�����","�����","�����","�. �����","�. ����� �����"';
  Grid3.Rows[0].CommaText:= '"�����","�����","��","�� �����","��'' ����","����","��'' ���","�����","�����","�����"';
end;

function TfmBookDirectors.FillEnum1(Line: Integer; ChangeDate: TDateTime; Param: Pointer): Boolean;
var
  I: Integer;
begin
  Result:= True;
  For I:= 0 to Company.Directors.Count-1 do
    With Company.Directors.Items[I] as TDirector do
      If Line = RecNum Then
//        If Taagid=nil Then
          begin
            If (ChangeDate>=NotifyDate) and (ChangeDate<=DecisionDate) then
              begin
                SortList.Add(FloatToStr(ChangeDate));
                Grid2.Cells[0,Grid2.RowCount-1]:= DateToLongStr(ChangeDate);
                Grid2.Cells[1,Grid2.RowCount-1]:= '�����';
                Grid2.Cells[2,Grid2.RowCount-1]:= Name;
                Grid2.Cells[3,Grid2.RowCount-1]:= Zeut;
                Grid2.Cells[4,Grid2.RowCount-1]:= Address.Street;
                Grid2.Cells[5,Grid2.RowCount-1]:= Address.Number;
                Grid2.Cells[6,Grid2.RowCount-1]:= Address.Index;
                Grid2.Cells[7,Grid2.RowCount-1]:= Address.City;
                Grid2.Cells[8,Grid2.RowCount-1]:= Address.Country;
                Grid2.Cells[9,Grid2.RowCount-1]:= DateToLongStr(Date1);
                Grid2.Cells[10,Grid2.RowCount-1]:= DateToLongStr(Date2);
                Grid2.RowCount:= Grid2.RowCount+1;
              end;
            If Not Active Then
              If (LastChanged>=NotifyDate) and (LastChanged<=DecisionDate) then
                begin
                  SortList.Add(FloatToStr(LastChanged));
                  Grid2.Cells[0,Grid2.RowCount-1]:= DateToLongStr(LastChanged);
                  Grid2.Cells[1,Grid2.RowCount-1]:= '�����';
                  Grid2.Cells[2,Grid2.RowCount-1]:= Name;
                  Grid2.Cells[3,Grid2.RowCount-1]:= Zeut;
                  Grid2.Cells[4,Grid2.RowCount-1]:= Address.Street;
                  Grid2.Cells[5,Grid2.RowCount-1]:= Address.Number;
                  Grid2.Cells[6,Grid2.RowCount-1]:= Address.Index;
                  Grid2.Cells[7,Grid2.RowCount-1]:= Address.City;
                  Grid2.Cells[8,Grid2.RowCount-1]:= Address.Country;
                  Grid2.Cells[9,Grid2.RowCount-1]:= DateToLongStr(Date1);
                  Grid2.Cells[10,Grid2.RowCount-1]:= DateToLongStr(Date2);
                  Grid2.RowCount:= Grid2.RowCount+1;
                end;
          end;
end;

procedure TfmBookDirectors.EvFillData1;
var
  Changes: TChanges;
begin
  Company.Directors.Filter:= afAll;
  Changes:= TChanges.Create(Company);
  try
    Changes.EnumChanges(ocCompany, Company.RecNum, acNewDirector,FillEnum1,nil);
  finally
    Changes.Free;
  end;
end;

function TfmBookDirectors.FillEnum2(Line: Integer; ChangeDate: TDateTime; Param: Pointer): Boolean;
var
  I: Integer;
begin
  Result:= True;
  For I:= 0 to Company.Directors.Count-1 do
    With Company.Directors.Items[I] as TDirector do
      If Line = RecNum Then
        If Taagid<>Nil Then
          begin
            If (ChangeDate>=NotifyDate) and (ChangeDate<=DecisionDate) then
              begin
                SortList.Add(FloatToStr(ChangeDate));
                Grid3.Cells[0,Grid3.RowCount-1]:= DateToLongStr(ChangeDate);
                Grid3.Cells[1,Grid3.RowCount-1]:= '�����';
                Grid3.Cells[2,Grid3.RowCount-1]:= Taagid.Name;
                Grid3.Cells[3,Grid3.RowCount-1]:= Name;
                Grid3.Cells[4,Grid3.RowCount-1]:= Taagid.Zeut;
                Grid3.Cells[5,Grid3.RowCount-1]:= Taagid.Street;
                Grid3.Cells[6,Grid3.RowCount-1]:= Taagid.Number;
                Grid3.Cells[7,Grid3.RowCount-1]:= Taagid.Index;
                Grid3.Cells[8,Grid3.RowCount-1]:= Taagid.City;
                Grid3.Cells[9,Grid3.RowCount-1]:= Taagid.Country;
                Grid3.RowCount:= Grid3.RowCount+1;
              end;
            If Not Active Then
              If (LastChanged>=NotifyDate) and (LastChanged<=DecisionDate) then
                begin
                  SortList.Add(FloatToStr(LastChanged));
                  Grid3.Cells[0,Grid3.RowCount-1]:= DateToLongStr(LastChanged);
                  Grid3.Cells[1,Grid3.RowCount-1]:= '�����';
                  Grid3.Cells[2,Grid3.RowCount-1]:= Taagid.Name;
                  Grid3.Cells[3,Grid3.RowCount-1]:= Name;
                  Grid3.Cells[4,Grid3.RowCount-1]:= Taagid.Zeut;
                  Grid3.Cells[5,Grid3.RowCount-1]:= Taagid.Street;
                  Grid3.Cells[6,Grid3.RowCount-1]:= Taagid.Number;
                  Grid3.Cells[7,Grid3.RowCount-1]:= Taagid.Index;
                  Grid3.Cells[8,Grid3.RowCount-1]:= Taagid.City;
                  Grid3.Cells[9,Grid3.RowCount-1]:= Taagid.Country;
                  Grid3.RowCount:= Grid3.RowCount+1;
                end;
          end;
end;

procedure TfmBookDirectors.EvFillData2;
var
  Changes: TChanges;
begin
  Company.Directors.Filter:= afAll;
  Changes:= TChanges.Create(Company);
  try
    Changes.EnumChanges(ocCompany, Company.RecNum, acNewDirector,FillEnum2,nil);
  finally
    Changes.Free;
  end;
end;

procedure TfmBookDirectors.EvPrintData;
var
  I,J: Integer;
begin
  case PageIndex of
    1: begin
         with taTemp.FieldDefs do
           begin
             Clear;
             Add('Date', ftString, 10, False);
             Add('Action', ftString, 15, False);
             Add('Name', ftString, 40, False);
             Add('Zeut', ftString, 9, False);
             Add('Street', ftString, 40, False);
             Add('Number', ftString, 8, False);
             Add('Index', ftString, 8, False);
             Add('City', ftString, 30, False);
             Add('Country', ftString, 30, False);
             Add('Date1', ftString, 10, False);
             Add('Date2', ftString, 10, False);
           end;
         taTemp.CreateTable;
         taTemp.Open;
         For I:= 1 To Grid2.RowCount-1 do
           begin
             taTemp.Append;
             For J:= 0 To Grid2.ColCount-1 do
               taTemp.Fields[J].AsString:= MakeHebStr(Grid2.Cells[J,I], False);
             taTemp.Post;
           end;
         taTemp.Close;
         Application.CreateForm(TfmQRDirBook1, fmQRDirBook1);
         try
           fmQRDirBook1.PrintExecute(Caption, Company, NotifyDate, DecisionDate);
         finally
           fmQRDirBook1.Free;
         end;
       end;
    2: begin
         with taTemp.FieldDefs do
           begin
             Clear;
             Add('Date', ftString, 10, False);
             Add('Action', ftString, 15, False);
             Add('Name', ftString, 40, False);
             Add('Taagid', ftString, 40, False);
             Add('Zeut', ftString, 9, False);
             Add('Street', ftString, 40, False);
             Add('Number', ftString, 8, False);
             Add('Index', ftString, 8, False);
             Add('City', ftString, 30, False);
             Add('Country', ftString, 30, False);
           end;
         taTemp.CreateTable;
         taTemp.Open;
         For I:= 1 To Grid3.RowCount-1 do
           begin
             taTemp.Append;
             For J:= 0 To Grid3.ColCount-1 do
               taTemp.Fields[J].AsString:= MakeHebStr(Grid3.Cells[J,I], False);
             taTemp.Post;
           end;
         taTemp.Close;
         Application.CreateForm(TfmQRDirBook2, fmQRDirBook2);
         try
           fmQRDirBook2.PrintExecute(Caption, Company, NotifyDate, DecisionDate);
         finally
           fmQRDirBook2.Free;
         end;
       end;
  end;
end;

procedure TfmBookDirectors.FormCreate(Sender: TObject);
begin
  ShowStyle:= afActive;
  inherited;
end;

end.
