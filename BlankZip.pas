unit BlankZip;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HEdit, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls,
  Mask, HLabel, DataObjects, DBTables;


type
  TfmBlankZip= class(TfmBlankTemplate)
    Shape51: TShape;
    HebLabel27: THebLabel;
    Shape60: TShape;
    e12: TEdit;
    objectTypeCombo: TComboBox;
    Label1: TLabel;
    objectSelectionCombo: TComboBox;
    Shape5: TShape;
    procedure FormCreate(Sender: TObject);
    procedure objectTypeComboChange(Sender: TObject);
    procedure objectSelectionComboChange(Sender: TObject);
    procedure e12Change(Sender: TObject);

//    procedure btPrintClick(Sender: TObject);
  private
    { Private declarations }
    originalZip :string;

//    procedure EvPrintData; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    function EvLoadData: Boolean; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvDataLoaded; override;

    function GetZip(Company: TCompany; p_objectType: TObjectClass; p_objectNum: integer):string;
    procedure SetZipText;
    function LoadObejctSelectionCombo: boolean;
    procedure ToggleCombos(enable: boolean);
    procedure  ToggleTextType (isNumeric: boolean);
    function GetIndexByObjectType(p_objectType: TObjectClass): integer;
    function GetIndexByObjectNum(p_objectNum: integer): integer;

    procedure SetZipEnabled(isEnabled : boolean);
  public
    { Public declarations }
    ObjectType: TObjectClass;
    ObjectNum: integer;
    //constructor Create(AOWner:TComponent; p_objectType: TObjectClass; p_objectNum: integer);
    procedure DeleteFile(FileItem: TFileItem); override;
    class procedure EvUndoFile(FileItem: TFileItem);
  end;

const
  doSelectCompany = false;
var
  fmBlankZip: TfmBlankZip;

implementation

uses  dialog2, LoadDialog, PrintName, DataM, SaveDialog, Util;

{$R *.DFM}
{//adapted
constructor TfmBlankZip.Create(AOWner:TComponent; p_objectType: TObjectClass; p_objectNum: integer);
begin
     inherited Create(AOwner);

     ObjectType := p_objectType;
     ObjectNum := p_objectNum;
end;}

//adapted
function TfmBlankZip.EvLoadData: Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  TempCompany: TCompany;
begin
  Result:= False;
  FileItem:= fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faZipCode);
  If FileItem = Nil Then
    Exit;

  self.ObjectType := FileItem.FileInfo.AdditionalObjectClass;
  self.ObjectNum := FileItem.FileInfo.AdditionalObjectNum;
  NotifyDate:= (FileItem.FileInfo.NotifyDate);
  DecisionDate:= (FileItem.FileInfo.DecisionDate);  //this activates the evDataLoaded procedure


  self.objectTypeCombo.itemIndex := GetIndexByObjectType(FileItem.FileInfo.AdditionalObjectClass);
  objectTypeComboChange( self.objectTypeCombo);
  if (self.ObjectType <> ocCompany) then
  begin
    self.objectSelectionCombo.itemIndex := GetIndexByObjectNum(FileItem.FileInfo.AdditionalObjectNum);
    objectSelectionComboChange(self.objectSelectionCombo);
  end;

  If FileItem.FileInfo.Draft then
  begin
     e12.Text:= FileItem.FileInfo.AdditionalData
  end Else
    begin
      TempCompany:= TCompany.Create(Company.RecNum, DecisionDate, False);
      e12.Text:= GetZip(TempCompany, FileItem.FileInfo.AdditionalObjectClass, FileItem.FileInfo.AdditionalObjectNum); //?
      TempCompany.Free;
    end;
  originalZip := e12.Text;
  FileItem.Free;
  FilesList.Free;
  Result:= True;
end;

//adapted
function TfmBlankZip.GetZip(Company: TCompany; p_objectType: TObjectClass; p_objectNum: integer) :string;
var
  director: TDirector;
  stockHolder: TStockHolder;
  manager :TManager;
begin
     case p_objectType of
          ocCompany:
                    begin
                      if (Company.Address <> nil) then
                      begin
                        Result:= Company.Address.Index;
                      end else
                      begin
                        Raise Exception.Create ('�� ���� ����� ����� ������ ����');
                      end;
                    end;
          ocDirector,
          ocTaagid:
                     begin
                      director := Company.Directors.FindByRecNum(p_objectNum) as TDirector;
                      if (director <> nil) then
                      begin
                        if (p_objectType =ocTaagid) then
                           if director.Taagid <> nil then
                              Result:= director.Taagid.Index
                           else
                              Raise Exception.Create ('�� ���� ����� ����� ������ �����')
                        else if (director.Address <> nil) then
                           Result:= director.Address.Index
                        else
                           Raise Exception.Create ('�� ���� ����� ����� ������ �������');
                      end else
                      begin
                        Raise Exception.Create ('�� ���� ����� ����� ������ ������� �� �����');
                      end;
                    end;
          ocStockHolder:
                    begin
                      stockHolder := Company.StockHolders.FindByRecNum(p_objectNum) as TStockHolder;
                      if (stockHolder <> nil) and (stockHolder.Address <> nil) then
                      begin
                        Result:= stockHolder.Address.Index;
                      end else
                      begin
                        Raise Exception.Create ('�� ���� ����� ����� ������ ��� �����');
                      end;
                    end;
          ocManager:
                    begin
                      manager := Company.Managers.FindByRecNum(p_objectNum) as TManager;
                      Result:= manager.Address;
                      if (manager <> nil) then
                      begin
                        Result:= manager.Address;
                      end else
                      begin
                        Raise Exception.Create ('�� ���� ����� ����� ������ ����');
                      end;
                    end;
     else
         Result:= '';
     end;
end;

//adapted
procedure TfmBlankZip.DeleteFile(FileItem: TFileItem);
begin
  If Not FileItem.FileInfo.Draft Then
    Raise Exception.Create('This file is not Draft at TfmBlankZip.DeleteFile');
  If (FileItem.FileInfo.Action <> faZipCode) Then
    raise Exception.Create('Invalid file type at TfmBlankZip.DeleteFile');
  FileItem.DeleteFile;
end;
//adapted//?
class procedure TfmBlankZip.EvUndoFile(FileItem: TFileItem);
//var
//  Company: TCompany;
begin
//  Company:= TCompany.Create(FileItem.FileInfo.CompanyNum, FileItem.FileInfo.DecisionDate+1, False);
//  Company.SetNumber(Company.Zeut, FileItem.FileInfo.FileName);
//  Company.Free;
  FileItem.DeleteFile;
end;
//adapted
procedure TfmBlankZip.EvFocusNext(Sender: TObject);
begin
//  Inherited;
  If Sender = e13 then e12.SetFocus;
end;
//adapted
procedure TfmBlankZip.EvDataLoaded;
begin
  Inherited;
  if(  objectTypeCombo.ItemIndex = -1) then //never loaded before
  begin
    objectTypeCombo.ItemIndex := 0;
    self.ObjectType := ocCompany;
    self.ObjectNum := Company.RecNum;
    If Not DoNotClear Then
       SetZipText();
  end;
end;
//adapted
function TfmBlankZip.EvSaveData(Draft: Boolean): Boolean;
var
  FilesList{, TempFileList}: TFilesList;
  FileInfo: TFileInfo;
  FileItem{, OldFile}: TFileItem;
 // I: Integer;
begin
  Result := True;
  if DecisionDate > Date() THEN
  begin
    Application.MessageBox(PChar('�� ���� ���� ����� ������ �����.'),
                           '����� ������',
                           MB_OK + MB_ICONERROR + MB_RTLREADING + MB_RIGHT);
  end
  else
  begin
   try
     FilesList:= TFilesList.Create(Company.RecNum, faZipCode, Draft);
     FileInfo:= TFileInfo.Create;
     FileInfo.Action:= faZipCode;
     FileInfo.Draft:= Draft;

     FileInfo.AdditionalObjectClass := ObjectType;
     FileInfo.AdditionalObjectNum := ObjectNum;
     FileInfo.AdditionalData:= e12.Text;

 //    FileInfo.FileName := ObjectToName(ObjectType)+'_'+inttostr(ObjectNum)+'_'+e12.Text;
 {    If Draft Then
       FileInfo.AdditionalData:= e12.Text
     Else
       FileInfo.AdditionalData:= GetZip(Company, ObjectType, ObjectNum);    //?
  }
     FileInfo.DecisionDate:= DecisionDate;
     FileInfo.NotifyDate:= Now;
     FileInfo.CompanyNum:= Company.RecNum;
     FileItem:= fmSaveDialog.Execute(Self, FilesList, FileInfo);

     // Moshe 08/08/18
     if ((not Draft) and (objectType = ocCompany)) then
     //if (Draft and (objectType = ocCompany)) then
     begin
      Company.Address.Index := e12.Text;
      Company.Address.SaveData(Company, Company.LastChanged, Draft, FileInfo.RecNum);
     end;

     if (FileItem <> nil) then
     begin
       fmdialog2.Show;
       FileItem.Free;
       FilesList.Free;
      // Result:= True;
     end;
   finally
   end;
  end;
end;
//adapted
procedure TfmBlankZip.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
   History: THistory;
begin
  Inherited;

  If Not Extended Then
    Exit;

  History:= THistory.Create;
  try
    If (Not History.IsZipActionLast(Company.RecNum, self.ObjectType, self.ObjectNum, DecisionDate)) then
      begin
        if not Silent then
          with e13 do
            if Color = clWindow then
              Color:= clBtnFace
            Else
              Color:= clWindow;
        ErrorAt:= e13;
        raise Exception.Create('����� ������ ����� ���� '+ DateToLongStr(DecisionDate));
      end;
  finally
    History.Free;
  end;

  if (GetZip(Company, ObjectType, ObjectNum) = e12.Text) then
    begin
      if not Silent Then
        With e12 do
          If Color = clWindow Then
            Color := clBtnFace
          Else
            Color:= clWindow;

      ErrorAt:= e12;
      raise Exception.Create('������ ���� ��� ������ ����');
    end;
end;
//adapted
procedure TfmBlankZip.FormCreate(Sender: TObject);
begin
  FileAction:= faZipCode;
  ShowStyle:= afActive;
  inherited;
end;


procedure TfmBlankZip.SetZipText();
begin
  ToggleTextType (ObjectType <> ocManager);
  originalZip := GetZip(Company,ObjectType, ObjectNum);
  e12.Text:=originalZip;
end;

{procedure TfmBlankZip.EvPrintData;
begin
  Application.CreateForm(TfmQRName, fmQRName);
  fmQRName.PrintExecute(lName.Caption, lNumber.Caption, e12.Text);
  fmQRName.Free;
end;}


{procedure TfmBlankZip.btPrintClick(Sender: TObject);
begin
  if e13.Focused Then   // Needed to update lName.Caption
    e12.SetFocus;
  inherited;
end;}

procedure TfmBlankZip.objectTypeComboChange(Sender: TObject);
var
   messageObject: string;
begin
  inherited;
  case objectTypeCombo.ItemIndex of
       0: self.ObjectType :=ocCompany;
       1: self.ObjectType :=ocStockHolder;
       2: self.ObjectType :=ocDirector;
       3: self.ObjectType :=ocTaagid;
       4: self.ObjectType :=ocManager;
  end;

  SetZipEnabled(true);
  if (self.ObjectType <> ocCompany) then
  begin
     if LoadObejctSelectionCombo() then
     begin
         self.objectSelectionCombo.Visible := true ;
         objectSelectionComboChange(sender);
         //SetZipText;
     end else
     begin
         self.objectSelectionCombo.Visible := false ;
         e12.Text := '';

         if ( self.ObjectType = ocTaagid) OR (self.ObjectType = ocManager) then
         begin
            //SetZipEnabled(false);
            if self.ObjectType = ocTaagid then
               messageObject :='����� ���� ����� ��������'
            else
               messageObject :='���� �����';

            Application.MessageBox( PChar( '�� ���� '+messageObject),
                                    PChar('���� ���'), MB_OK +
                                        MB_ICONWARNING + MB_RTLREADING + MB_RIGHT );

            objectTypeCombo.ItemIndex := -1;
            EvDataLoaded();
         end;
     end;
  end else
  begin
    self.objectSelectionCombo.Visible := false;
    self.ObjectNum := Company.RecNum;
//    objectSelectionComboChange(sender);
    //SetZipText;
  end;

end;

procedure TfmBlankZip.objectSelectionComboChange(Sender: TObject);
var
   itemObject: TListedClass;
begin
  itemObject:=  objectSelectionCombo.Items.Objects[objectSelectionCombo.ItemIndex] as TListedClass;

  self.ObjectNum := itemObject.RecNum  ;
  SetZipText;
end;

function TfmBlankZip.LoadObejctSelectionCombo: boolean;
var
   objects: TListObject;
   i: integer;
begin
   Result:=True;
   objects := nil;

   case self.ObjectType of
        ocDirector:
        begin
           objects := Company.Directors;
        end;
        ocTaagid:
        begin
          objects :=TDirectors.CreateNew;
          for i := 0 to  Company.Directors.Count-1 do
          begin
             if (TDirector(Company.Directors.Items[i]).Taagid <> nil) then
                objects.Add(Company.Directors.Items[i]);

          end;
        end;
        ocStockHolder:  objects := Company.StockHolders;
        ocManager:  //objects :=Company.Managers;
        begin
          objects := TManagers.CreateNew;
          for i := 0 to  Company.Managers.Count-1 do
          begin
             if (TManager(Company.Managers.Items[i]).Name <> '') then
                objects.Add(Company.Managers.Items[i]);
          end;
        end;
   end;
   objects.Filter  := afActive;

   if (objects <> nil ) then
   begin
     objectSelectionCombo.Clear;
     if (objects.Count > 0) then
     begin
       for i := 0 to objects.Count-1 do
       begin
           objectSelectionCombo.Items.AddObject(objects.Items[i].name, objects.Items[i]);
       end;
       objectSelectionCombo.ItemIndex := 0;
     //  Result := true;
     end else
       Result := false;
   end;
end;

procedure TfmBlankZip.e12Change(Sender: TObject);
begin
  inherited;
  ToggleCombos((originalZip = '') OR (originalZip = e12.Text));
end;

procedure TfmBlankZip.ToggleCombos(enable: boolean);
begin
     self.objectTypeCombo.Enabled := enable;
     self.objectSelectionCombo.Enabled := enable;
end;

procedure TfmBlankZip.ToggleTextType (isNumeric: boolean);
begin
     if isNumeric then
     begin
        e12.Hint := 'number';
        e12.MaxLength := 7;
     end else
     begin
        e12.Hint := 'empty';
        e12.MaxLength := 60;
     end;
end;


function TfmBlankZip.GetIndexByObjectType(p_objectType: TObjectClass): integer;
begin
  Result := 0;
  case p_objectType of
    ocCompany:      Result := 0;
    ocStockHolder:  Result := 1;
    ocDirector:     Result := 2;
    ocTaagid:       Result := 3;
    ocManager:      Result := 4;
  end;
end;

function TfmBlankZip.GetIndexByObjectNum(p_objectNum: integer): integer;
var
   i: integer;
   itemObject: TListedClass;
begin
     Result := -1;
     for i:= 0 to self.objectSelectionCombo.Items.Count-1 do
     begin
        itemObject:=  objectSelectionCombo.Items.Objects[i] as TListedClass;
        if (itemObject.RecNum =  p_objectNum) then
        begin
             Result := i;
             break;
        end;
     end;
end;

procedure TfmBlankZip.SetZipEnabled(isEnabled : boolean);
begin
   e12.Enabled := isEnabled;
   if (isEnabled) then
      e12.Color := clWhite
   else
      e12.Color := clGray;

//   b1.Enabled := isEnabled;
//   b2.Enabled := isEnabled;
end;

end.

