unit BlankEngName;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  HebForm, ExtCtrls, HEdit, ComCtrls, Buttons, StdCtrls, Mask, HLabel, BlankTemplate,DataObjects;

type
  TfmBlankEngName = class(TfmBlankTemplate)
    HebLabel27: THebLabel;
    Shape51: TShape;
    e12: THebEdit;
    Shape60: TShape;
    procedure FormCreate(Sender: TObject);
    //procedure b1Click(Sender: TObject);
  private
    { Private declarations }
    function EvSaveData(Draft: Boolean): Boolean; override;
    procedure EvDataLoaded; override;
  public
    { Public declarations }
  end;

var
  fmBlankEngName: TfmBlankEngName;

implementation

uses dialog2, DataM, LoadDialog;
{$R *.DFM}

function TfmBlankEngName.EvSaveData(Draft: Boolean): Boolean;
begin
  try
    Company.CompanyInfo.SetEnglishName(self.e12.Text, Company, DecisionDate);

    fmdialog2.Show;
    Result:= True;
  finally
  end;
end;


procedure TfmBlankEngName.EvDataLoaded;
begin
  Inherited;
  If Not DoNotClear Then
    e12.Text:= Company.CompanyInfo.EnglishName;
end;

procedure TfmBlankEngName.FormCreate(Sender: TObject);
begin
  FileAction:= faEngName;
  ShowStyle:= afActive;
  inherited;
end;

end.
