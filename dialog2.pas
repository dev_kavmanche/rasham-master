unit dialog2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, HMemo, ExtCtrls;

type
  TfmDialog2 = class(TForm)
    Timer1: TTimer;
    Panel2: TPanel;
    Panel1: TPanel;
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDialog2: TfmDialog2;

implementation

{$R *.DFM}

procedure TfmDialog2.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:= False;
  close;
  Panel1.Caption:= '����� ������� ������';
end;

procedure TfmDialog2.FormShow(Sender: TObject);
begin
  if Tag=0 then
    Timer1.Enabled:= True;
end;

end.
