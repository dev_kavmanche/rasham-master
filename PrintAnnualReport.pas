unit PrintAnnualReport;

{
History:
DATE        VERSION   BY   TASK     DESCRIPTION
17/12/2019  1.7.7     VG   19075    code refactoring; fixed several bugs
24/12/2019  1.7.7     VG   19364    added thousand separators,  fixed TQRAnnualReport.PrintProc
27/02/2020  1.7.8     sts  19736    fixed bug with date printing
25/03/2020  1.8.0     sts  19925    annual report redesign
15/06/2020  1.8.1     sts  20654    fix bug with def printer for rep5
18/06/2020  1.8.3     sts  21232    fix bug with decimal point
20/12/2020  1.8.4     sts  22057    fix bug with float rounded to integer; refactoring
02/11/2021  1.8.9     sts  24218    fix caption in report 
}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, Printers, QuickRpt, ExtCtrls, DataObjects, Db, memdset;

type

  TAnnualReportData = class
  public
    DirectorsChanged, DirectorsNotChanged: boolean;
    MusmakhName, MusmakhZeut, MusmakhRole: string;
    ReportsApproved: boolean;
    ReportsDisplayed, DoesNotHaveAnnualAssembly, DoesNotHaveToMakeReports: boolean;
    ThereIsAnAccountant, AccountantNameIs, NoAccountant, NonActive: boolean;
    AccountantName, AccountantAddress: string;
    DaysWithoutAccountant: integer;
    FManagerName, FManagerID, FManagerAddress, FManagerPhone: string;
    MemuneName, MemuneID, MemuneRole: string;
    ReportBrought, ReportSent: boolean;
    ReportSentAt: TDateTime;
    MatzhirName, MatzhirID, MatzhirRole: string;
    AdvName, AdvSignName, AdvSignID: string;
    AdvMeametName, AdvMeametAddress, AdvMeametID, AdvMeametLicence: string;
    AccName, AccSignName, AccSignID: string;
    AccMeametName, AccMeametAddress, AccMeametID, AccMeametLicence: string;
    NotifyDate: TDateTime;
    DecisionDate: TDateTime;
    LastDoh: TDateTime;

    function GetManagerCount: integer;
    function GetManagerName(index: integer): string;
    function GetManagerID(index: integer): string;
    function GetManagerAddress(index: integer): string;
    function GetManagerPhone(index: integer): string;

    property ManagerCount: integer read GetManagerCount;
    property ManagerName[Index: integer]: string read GetManagerName;
    property ManagerID[Index: integer]: string read GetManagerID;
    property ManagerAddress[Index: integer]: string read GetManagerAddress;
    property ManagerPhone[Index: integer]: string read GetManagerPhone;
  end;

  TQRAnnualReport = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRImage13: TQRImage;
    QRImage8: TQRImage;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape6: TQRShape;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape7: TQRShape;
    sp2: TQRShape;
    sp1: TQRShape;
    lblTotalCapital: TQRLabel;
    lblStockName1: TQRLabel;
    lblStockNameA1: TQRLabel;
    lblStockType1: TQRLabel;
    lblRashum1: TQRLabel;
    lblAssigned1: TQRLabel;
    lblStockValue1: TQRLabel;
    QRLabel29: TQRLabel;
    QRShape11: TQRShape;
    sp6: TQRShape;
    sp5: TQRShape;
    sp7: TQRShape;
    sp8: TQRShape;
    lblStockHolderName1: TQRLabel;
    lblStockHolderID1: TQRLabel;
    lblStockHolderAddress1: TQRLabel;
    lblStockHolderAddressA1: TQRLabel;
    lblStockTypeA1: TQRLabel;
    lblStocksNum1: TQRLabel;
    lblSumNotPaid1: TQRLabel;
    edtCompanyName: TQRLabel;
    edtCompNum: TQRLabel;
    edtCompanyAddress: TQRLabel;
    edtCompanyPhone: TQRLabel;
    edtCompanyEmail: TQRLabel;
    edtREportIsUpToDateAt: TQRLabel;
    edtAnnualAssemblyDate: TQRLabel;
    edtTotalCapital: TQRLabel;
    edtStockName1: TQRLabel;
    edtStockTypeA1: TQRLabel;
    edtRashum1: TQRLabel;
    edtAssignedStocksNumber1: TQRLabel;
    edtTotalStockValue1: TQRLabel;
    edtStockHolderName1: TQRLabel;
    edtStockType1: TQRLabel;
    edtStockHolderID1: TQRLabel;
    edtStocksNum1: TQRLabel;
    edtStockHolderAddress1: TQRLabel;
    edtSumNotPaid1: TQRLabel;
    lblStockHolderName2: TQRLabel;
    edtStockHolderName2: TQRLabel;
    lblStockTypeA2: TQRLabel;
    edtStockType2: TQRLabel;
    lblStockHolderID2: TQRLabel;
    edtStockHolderID2: TQRLabel;
    lblStocksNum2: TQRLabel;
    edtStocksNum2: TQRLabel;
    lblStockHolderAddress2: TQRLabel;
    lblStockHolderAddressA2: TQRLabel;
    edtStockHolderAddress2: TQRLabel;
    lblSumNotPaid2: TQRLabel;
    edtSumNotPaid2: TQRLabel;
    lblStockHolderName3: TQRLabel;
    edtStockHolderName3: TQRLabel;
    lblStockTypeA3: TQRLabel;
    edtStockType3: TQRLabel;
    lblStockHolderID3: TQRLabel;
    edtStockHolderID3: TQRLabel;
    lblStocksNum3: TQRLabel;
    edtStocksNum3: TQRLabel;
    lblStockHolderAddress3: TQRLabel;
    lblStockHolderAddressA3: TQRLabel;
    edtStockHolderAddress3: TQRLabel;
    lblSumNotPaid3: TQRLabel;
    edtSumNotPaid3: TQRLabel;
    QRShape16: TQRShape;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRCompositeReport1: TQRCompositeReport;
    QuickRep4: TQuickRep;
    QRBand4: TQRBand;
    QRImage5: TQRImage;
    QRLabel161: TQRLabel;
    QRLabel189: TQRLabel;
    QRLabel190: TQRLabel;
    QRLabel191: TQRLabel;
    QRImage6: TQRImage;
    QRLabel194: TQRLabel;
    QRLabel195: TQRLabel;
    QRShape78: TQRShape;
    edtAdvName: TQRLabel;
    QRLabel196: TQRLabel;
    QRShape79: TQRShape;
    edtSignName: TQRLabel;
    QRLabel197: TQRLabel;
    QRShape80: TQRShape;
    edtSignID: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel199: TQRLabel;
    QRShape81: TQRShape;
    QRShape82: TQRShape;
    QRLabel200: TQRLabel;
    edtAdvMeametName: TQRLabel;
    QRShape83: TQRShape;
    QRLabel201: TQRLabel;
    edtAdvMeametAddress: TQRLabel;
    QRShape84: TQRShape;
    QRLabel202: TQRLabel;
    QRLabel203: TQRLabel;
    edtAdvMeametID: TQRLabel;
    QRLabel204: TQRLabel;
    edtAdvMeametLicence: TQRLabel;
    QRShape86: TQRShape;
    QRLabel205: TQRLabel;
    QRShape87: TQRShape;
    QRLabel206: TQRLabel;
    edtAccName: TQRLabel;
    QRShape88: TQRShape;
    QRLabel207: TQRLabel;
    edtSignName2: TQRLabel;
    QRShape89: TQRShape;
    QRLabel208: TQRLabel;
    edtSignID2: TQRLabel;
    QRShape90: TQRShape;
    QRLabel209: TQRLabel;
    QRShape91: TQRShape;
    QRLabel210: TQRLabel;
    QRShape92: TQRShape;
    QRLabel211: TQRLabel;
    QRLabel212: TQRLabel;
    QRShape93: TQRShape;
    QRShape94: TQRShape;
    QRLabel213: TQRLabel;
    QRShape95: TQRShape;
    edtAccMeametName: TQRLabel;
    edtAccMeametAddress: TQRLabel;
    edtAccMeametID: TQRLabel;
    edtAccMeametLicence: TQRLabel;
    QRLabel214: TQRLabel;
    QRShape96: TQRShape;
    QRShape97: TQRShape;
    QRLabel215: TQRLabel;
    QRLabel216: TQRLabel;
    QRShape98: TQRShape;
    QRLabel217: TQRLabel;
    QRLabel218: TQRLabel;
    QRShape99: TQRShape;
    QRLabel219: TQRLabel;
    QRLabel220: TQRLabel;
    QRShape100: TQRShape;
    QRLabel221: TQRLabel;
    QRLabel222: TQRLabel;
    QRShape101: TQRShape;
    QRLabel223: TQRLabel;
    QuickRep2: TQuickRep;
    QRBand2: TQRBand;
    QRImage1: TQRImage;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRImage2: TQRImage;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    edtTOtalMuhaz: TQRLabel;
    edtStocksNumInShtar1: TQRLabel;
    edtShtarNum1: TQRLabel;
    edtStocksNumInShtar2: TQRLabel;
    edtShtarNum2: TQRLabel;
    QRLabel75: TQRLabel;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    lblDirectorName1: TQRLabel;
    QRShape30: TQRShape;
    lblDirectorZeut1: TQRLabel;
    QRShape31: TQRShape;
    lblDirectorStartDate1: TQRLabel;
    edtDirectorName1: TQRLabel;
    edtDirectorZeut1: TQRLabel;
    edtDirectorStartDate1: TQRLabel;
    lblDirectorAddress1: TQRLabel;
    edtDirectorAddress1: TQRLabel;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    lblDirectorName2: TQRLabel;
    lblDirectorZeut2: TQRLabel;
    lblDirectorStartDate2: TQRLabel;
    edtDirectorName2: TQRLabel;
    edtDirectorZeut2: TQRLabel;
    edtDirectorStartDate2: TQRLabel;
    lblDirectorAddress2: TQRLabel;
    edtDirectorAddress2: TQRLabel;
    lblDirectorName3: TQRLabel;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    lblDirectorZeut3: TQRLabel;
    lblDirectorStartDate3: TQRLabel;
    edtDirectorName3: TQRLabel;
    edtDirectorZeut3: TQRLabel;
    edtDirectorStartDate3: TQRLabel;
    lblDirectorAddress3: TQRLabel;
    edtDirectorAddress3: TQRLabel;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    lblDirectorName4: TQRLabel;
    lblDirectorZeut4: TQRLabel;
    lblDirectorStartDate4: TQRLabel;
    edtDirectorName4: TQRLabel;
    edtDirectorZeut4: TQRLabel;
    edtDirectorStartDate4: TQRLabel;
    lblDirectorAddress4: TQRLabel;
    edtDirectorAddress4: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRShape39: TQRShape;
    QRLabel103: TQRLabel;
    QRShape40: TQRShape;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    edtNoDirectorName1: TQRLabel;
    edtNoDirectorZeut1: TQRLabel;
    edtNoDirectorEndDate1: TQRLabel;
    QRShape41: TQRShape;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    edtNoDirectorName2: TQRLabel;
    edtNoDirectorZeut2: TQRLabel;
    edtNoDirectorEndDate2: TQRLabel;
    QRShape42: TQRShape;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    edtNoDirectorName3: TQRLabel;
    edtNoDirectorZeut3: TQRLabel;
    edtNoDirectorEndDate3: TQRLabel;
    QRShape43: TQRShape;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    edtNoDirectorName4: TQRLabel;
    edtNoDirectorZeut4: TQRLabel;
    edtNoDirectorEndDate4: TQRLabel;
    QRLabel118: TQRLabel;
    QRShape44: TQRShape;
    edtDirectorNotChanged: TQRLabel;
    QRLabel119: TQRLabel;
    QRShape45: TQRShape;
    edtDirectorsChanged: TQRLabel;
    QRLabel120: TQRLabel;
    QuickRep3: TQuickRep;
    QRBand3: TQRBand;
    QRImage3: TQRImage;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel124: TQRLabel;
    QRImage4: TQRImage;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRShape46: TQRShape;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    edtMusmakhName: TQRLabel;
    edtMusmakhZeut: TQRLabel;
    edtMusmakhRole: TQRLabel;
    QRLabel134: TQRLabel;
    QRShape50: TQRShape;
    edtDirectorionApproved: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRShape51: TQRShape;
    edtReportsDisplayed: TQRLabel;
    QRLabel144: TQRLabel;
    QRShape52: TQRShape;
    edtDoesNotHaveAnnualAssembly: TQRLabel;
    QRLabel145: TQRLabel;
    QRShape53: TQRShape;
    edtDoesNotHaveToMakeReports: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRShape54: TQRShape;
    edtThereIsAnAccountant: TQRLabel;
    QRLabel156: TQRLabel;
    QRShape55: TQRShape;
    edtAccountantNameIs: TQRLabel;
    QRLabel157: TQRLabel;
    QRShape56: TQRShape;
    edtAccountantName: TQRLabel;
    QRLabel158: TQRLabel;
    QRShape57: TQRShape;
    edtAccountantAddress: TQRLabel;
    QRShape58: TQRShape;
    edtNoAccountant: TQRLabel;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    QRShape59: TQRShape;
    edtDaysWithoutAccountant: TQRLabel;
    QRLabel162: TQRLabel;
    QRShape60: TQRShape;
    edtNonActive: TQRLabel;
    QRLabel163: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    QRShape61: TQRShape;
    QRLabel167: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel169: TQRLabel;
    QRLabel170: TQRLabel;
    QRShape62: TQRShape;
    QRShape63: TQRShape;
    QRShape64: TQRShape;
    QRShape65: TQRShape;
    edtManagerName: TQRLabel;
    edtManagerID: TQRLabel;
    edtManagerAddress: TQRLabel;
    edtManagerPhone: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRLabel173: TQRLabel;
    QRLabel174: TQRLabel;
    QRShape66: TQRShape;
    QRLabel175: TQRLabel;
    QRShape67: TQRShape;
    QRLabel176: TQRLabel;
    QRShape68: TQRShape;
    QRLabel177: TQRLabel;
    QRShape70: TQRShape;
    edtMemuneName: TQRLabel;
    edtMemuneID: TQRLabel;
    edtMemuneRole: TQRLabel;
    QRLabel179: TQRLabel;
    QRLabel180: TQRLabel;
    QRLabel181: TQRLabel;
    QRLabel182: TQRLabel;
    QRShape73: TQRShape;
    QRLabel183: TQRLabel;
    edtDateBroughtReports: TQRLabel;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    QRShape69: TQRShape;
    QRLabel178: TQRLabel;
    QRShape74: TQRShape;
    QRLabel186: TQRLabel;
    QRShape75: TQRShape;
    QRLabel187: TQRLabel;
    QRShape76: TQRShape;
    edtMatzhirName: TQRLabel;
    edtMatzhirID: TQRLabel;
    edtMatzhirRole: TQRLabel;
    QRLabel188: TQRLabel;
    QRShape77: TQRShape;
    taMemPastDirector: TMemDataSet;
    taMemManager: TMemDataSet;
    taMemMuhaz: TMemDataSet;
    taMemDirector: TMemDataSet;
    taMemHon: TMemDataSet;
    taMemStocks: TMemDataSet;
    QRLabel298: TQRLabel;
    QRLabel300: TQRLabel;
    edtStockName2: TQRLabel;
    edtStockTypeA2: TQRLabel;
    edtRashum2: TQRLabel;
    edtAssignedStocksNumber2: TQRLabel;
    edtTotalStockValue2: TQRLabel;
    edtStockName3: TQRLabel;
    edtStockTypeA3: TQRLabel;
    edtRashum3: TQRLabel;
    edtAssignedStocksNumber3: TQRLabel;
    edtTotalStockValue3: TQRLabel;
    lblAppendix1: TQRLabel;
    lblAppendix4: TQRLabel;
    QuickRep6: TQuickRep;
    QRSubDetail4: TQRSubDetail;
    QRShape157: TQRShape;
    QRDBText17: TQRDBText;
    QRShape158: TQRShape;
    QRShape159: TQRShape;
    QRShape160: TQRShape;
    QRShape162: TQRShape;
    QRShape163: TQRShape;
    QRShape164: TQRShape;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRBand8: TQRBand;
    QRShape120: TQRShape;
    QRShape121: TQRShape;
    QRLabel264: TQRLabel;
    QRShape123: TQRShape;
    QRLabel265: TQRLabel;
    QRShape124: TQRShape;
    QRLabel267: TQRLabel;
    QRLabel268: TQRLabel;
    QRShape126: TQRShape;
    QRLabel271: TQRLabel;
    QRShape127: TQRShape;
    QRShape129: TQRShape;
    QRLabel272: TQRLabel;
    QRShape130: TQRShape;
    QRLabel273: TQRLabel;
    QRLabel274: TQRLabel;
    QRLabel275: TQRLabel;
    QRShape131: TQRShape;
    QRShape132: TQRShape;
    QRLabel276: TQRLabel;
    QRLabel277: TQRLabel;
    QRShape133: TQRShape;
    QRLabel278: TQRLabel;
    QRShape134: TQRShape;
    QRLabel279: TQRLabel;
    QRBand5: TQRBand;
    QRLabel224: TQRLabel;
    QRLabel225: TQRLabel;
    edtCompanyName21: TQRLabel;
    QRLabel226: TQRLabel;
    edtCompNum21: TQRLabel;
    QRLabel227: TQRLabel;
    edtCaption1: TQRLabel;
    GroupHeaderBand21: TQRBand;
    QRShape106: TQRShape;
    QRShape107: TQRShape;
    QRLabel236: TQRLabel;
    QRShape108: TQRShape;
    QRLabel237: TQRLabel;
    QRShape109: TQRShape;
    QRLabel239: TQRLabel;
    QRLabel240: TQRLabel;
    QRShape110: TQRShape;
    QRLabel243: TQRLabel;
    QRShape111: TQRShape;
    QRShape112: TQRShape;
    QRLabel244: TQRLabel;
    QRShape113: TQRShape;
    QRLabel246: TQRLabel;
    QRLabel248: TQRLabel;
    QRLabel251: TQRLabel;
    QRShape114: TQRShape;
    QRShape115: TQRShape;
    QRShape116: TQRShape;
    QRLabel252: TQRLabel;
    QRLabel253: TQRLabel;
    QRLabel254: TQRLabel;
    QRLabel255: TQRLabel;
    QRLabel256: TQRLabel;
    QRLabel257: TQRLabel;
    QRLabel258: TQRLabel;
    QRSubDetail2: TQRSubDetail;
    QRShape135: TQRShape;
    QRDBText5: TQRDBText;
    QRShape136: TQRShape;
    QRShape137: TQRShape;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRShape138: TQRShape;
    QRDBText6: TQRDBText;
    QRShape140: TQRShape;
    QRShape144: TQRShape;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRShape147: TQRShape;
    QRShape154: TQRShape;
    GroupHeaderBand11: TQRBand;
    QRLabel228: TQRLabel;
    QRShape102: TQRShape;
    QRLabel229: TQRLabel;
    QRLabel230: TQRLabel;
    QRShape103: TQRShape;
    QRShape104: TQRShape;
    QRShape105: TQRShape;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel233: TQRLabel;
    QRLabel234: TQRLabel;
    QRLabel235: TQRLabel;
    QRShape161: TQRShape;
    QRLabel245: TQRLabel;
    QRLabel247: TQRLabel;
    QRLabel249: TQRLabel;
    edtAppendixTotalHon1: TQRLabel;
    QRLabel259: TQRLabel;
    QRLabel250: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    QRShape117: TQRShape;
    QRDBText10: TQRDBText;
    QRShape122: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRShape125: TQRShape;
    QRShape128: TQRShape;
    QRDBText2: TQRDBText;
    QRBand6: TQRBand;
    QuickRep5: TQuickRep;
    QRBand7: TQRBand;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    edtCaption: TQRLabel;
    QRLabel25: TQRLabel;
    edtCompanyName2: TQRLabel;
    QRLabel26: TQRLabel;
    edtCompNum2: TQRLabel;
    ghbHon: TQRBand;
    QRShape8: TQRShape;
    QRLabel27: TQRLabel;
    edtAppendixTotalHon: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape9: TQRShape;
    subHon: TQRSubDetail;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape10: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRShape14: TQRShape;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRShape15: TQRShape;
    QRShape71: TQRShape;
    QRDBText26: TQRDBText;
    QRShape72: TQRShape;
    QRLabel35: TQRLabel;
    ghbHolders: TQRBand;
    QRLabel22: TQRLabel;
    QRShape85: TQRShape;
    QRLabel36: TQRLabel;
    QRShape118: TQRShape;
    QRShape119: TQRShape;
    QRLabel50: TQRLabel;
    QRShape139: TQRShape;
    QRShape141: TQRShape;
    QRLabel51: TQRLabel;
    QRShape142: TQRShape;
    QRLabel52: TQRLabel;
    QRShape143: TQRShape;
    QRLabel53: TQRLabel;
    QRShape145: TQRShape;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRShape146: TQRShape;
    QRLabel56: TQRLabel;
    QRShape148: TQRShape;
    QRLabel57: TQRLabel;
    QRShape149: TQRShape;
    QRLabel58: TQRLabel;
    subHolders: TQRSubDetail;
    QRShape150: TQRShape;
    QRDBText27: TQRDBText;
    QRShape151: TQRShape;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRShape152: TQRShape;
    QRShape153: TQRShape;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRShape155: TQRShape;
    QRShape156: TQRShape;
    QRShape165: TQRShape;
    QRDBText33: TQRDBText;
    QRDBText34: TQRDBText;
    QRDBText35: TQRDBText;
    QRShape166: TQRShape;
    ghbDirectors: TQRBand;
    QRLabel59: TQRLabel;
    QRShape167: TQRShape;
    QRShape168: TQRShape;
    QRShape169: TQRShape;
    QRLabel60: TQRLabel;
    QRShape170: TQRShape;
    QRShape171: TQRShape;
    QRLabel63: TQRLabel;
    QRShape172: TQRShape;
    QRShape173: TQRShape;
    QRLabel64: TQRLabel;
    QRShape174: TQRShape;
    QRLabel65: TQRLabel;
    QRShape175: TQRShape;
    QRLabel238: TQRLabel;
    QRShape176: TQRShape;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    QRShape177: TQRShape;
    QRShape178: TQRShape;
    QRLabel260: TQRLabel;
    QRShape179: TQRShape;
    subDirectors: TQRSubDetail;
    QRShape180: TQRShape;
    QRShape181: TQRShape;
    QRShape182: TQRShape;
    QRShape183: TQRShape;
    QRShape184: TQRShape;
    QRShape185: TQRShape;
    QRShape186: TQRShape;
    QRDBText36: TQRDBText;
    QRDBText37: TQRDBText;
    QRDBText38: TQRDBText;
    QRDBText39: TQRDBText;
    QRDBText40: TQRDBText;
    QRDBText41: TQRDBText;
    QRDBText42: TQRDBText;
    QRShape187: TQRShape;
    lblAppendix5: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRShape188: TQRShape;
    QRShape189: TQRShape;
    QRShape190: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel44: TQRLabel;
    edtTotalCapitalCS: TQRLabel;
    edtRashumNm1: TQRLabel;
    edtRashumNm2: TQRLabel;
    edtRashumNm3: TQRLabel;
    edtTotalStockValueCS1: TQRLabel;
    edtTotalStockValueCS2: TQRLabel;
    edtTotalStockValueCS3: TQRLabel;
    QRShape38: TQRShape;
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRDBTextPrint(sender: TObject; var Value: String);
    procedure subHoldersBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure subHonBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    { Private declarations }
    ShowAppendix, BigHon, BigMuhaz, BigDirectors, BigNoDirectors, BigStocks, BigManagers: boolean;
    //VG19364:
    //before create the report we will remember the original font size for QR DB controls and will use it when we need
    FOriginalFontSize: Integer;
    procedure LoadData(Company: TCompany; AnnualRepData: TAnnualReportData);
  public
    { Public declarations }
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintExecute(const FileName: string; Company: TCompany; AnnualReportData: TAnnualReportData);
    procedure PrintEmpty(const FileName: String);
  end;

var
  QRAnnualReport: TQRAnnualReport;

implementation

uses
  PrinterSetup, utils2, Main;

function TAnnualReportData.GetManagerCount:integer;
begin
  Result := GetCommaTextCount(FManagerName);
end;

function TAnnualReportData.GetManagerName(index:integer):string;
begin
  Result := GetCommaText(FManagerName,index);
end;

function TAnnualReportData.GetManagerID(index:integer):string;
begin
  Result := GetCommaText(FManagerID,index);
end;

function TAnnualReportData.GetManagerAddress(index:integer):string;
begin
  Result := GetCommaText(FManagerAddress,index);
end;

function TAnnualReportData.GetManagerPhone(index:integer):string;
begin
  Result := GetCommaText(FManagerPhone,index);
end;

{$R *.DFM}

procedure TQRAnnualReport.LoadData(Company: TCompany; AnnualRepData: TAnnualReportData);
var
  //VG19075
  i, j, g, nStocks: integer;
  //nStocks1, nStocks2, nStocks3: integer;
  totalCapital, totalMuhaz: double;
  strName, strAddress, strType, strCount, strPaid: string;
  strZeut, strShtarValue: string;
  strDirName, strDirZeut, strDirStartDate, strDirAddress, strDirEndDate: string;
  DirCount1, DirCount2, p1, p2: integer;
  strStockName, strSaidStockValue, strStockType, strRashum, strTotalStockValue, strAssignedStocksNumber: string;
  StockHolder: TStockHolder;
  Stock: TStock;
  curSign: string;   //sts19925
  qrShape: TQrShape;
begin
  ShowAppendix := False;
  BigHon := False;
  BigMuhaz := False;
  BigDirectors := False;
  BigNoDirectors := False;
  BigStocks := False;
  BigManagers := False;

  if (Company = nil) or (AnnualRepData = nil) then
  begin
    for i := 0 to ComponentCount - 1 do
    begin
      if (Components[i] is TQRLabel)and(uppercase(Copy(Components[i].Name, 1, 3)) = 'EDT') then
        (Components[i] as TQRLabel).Caption := '';
    end;
    Exit;
  end;

  SetChecked(edtDirectorNotChanged, AnnualRepData.DirectorsChanged, 0);
  SetChecked(edtDirectorsChanged, AnnualRepData.DirectorsNotChanged, 0);
  edtMusmakhName.Caption := AnnualRepData.MusmakhName;
  edtMusmakhZeut.Caption := FormatInt(AnnualRepData.MusmakhZeut);
  edtMusmakhRole.Caption := AnnualRepData.MusmakhRole;
  SetChecked(edtDirectorionApproved, AnnualRepData.ReportsApproved, 0);
  SetChecked(edtReportsDisplayed, AnnualRepData.ReportsDisplayed, 0);
  SetChecked(edtDoesNotHaveAnnualAssembly, AnnualRepData.DoesNotHaveAnnualAssembly, 0);
  SetChecked(edtDoesNotHaveToMakeReports, AnnualRepData.DoesNotHaveToMakeReports, 0);
  SetChecked(edtThereIsAnAccountant, AnnualRepData.ThereIsAnAccountant, 0);
  SetChecked(edtAccountantNameIs, AnnualRepData.AccountantNameIs, 0);
  SetChecked(edtNoAccountant, AnnualRepData.NoAccountant, 0);
  SetChecked(edtNonActive, AnnualRepData.NonActive, 0);
  edtAccountantName.Caption := AnnualRepData.AccountantName;
  edtAccountantAddress.Caption := AnnualRepData.AccountantAddress;
  if AnnualRepData.DaysWithoutAccountant > 0 then
    edtDaysWithoutAccountant.Caption := IntToStr(AnnualRepData.DaysWithoutAccountant)
  else
    edtDaysWithoutAccountant.Caption := '';

  BigManagers := (AnnualRepData.ManagerCount > 1);

  edtManagerName.Caption := AnnualRepData.ManagerName[0];
  edtManagerID.Caption := FormatInt(AnnualRepData.ManagerID[0]);
  edtManagerAddress.Caption := AnnualRepData.ManagerAddress[0];
  edtManagerPhone.Caption := FormatInt(AnnualRepData.ManagerPhone[0]);
  edtMemuneName.Caption := AnnualRepData.MemuneName;
  edtMemuneID.Caption := FormatInt(AnnualRepData.MemuneID);
  edtMemuneRole.Caption := AnnualRepData.MemuneRole;
  edtMatzhirName.Caption := AnnualRepData.MatzhirName;
  edtMatzhirID.Caption := FormatInt(AnnualRepData.MatzhirID);
  edtMatzhirRole.Caption := AnnualRepData.MatzhirRole;
  edtAdvName.Caption := AnnualRepData.AdvName;
  edtSignName.Caption := AnnualRepData.AdvSignName;
  edtSignID.Caption := FormatInt(AnnualRepData.AdvSignID);
  edtAdvMeametName.Caption := AnnualRepData.AdvMeametName;
  edtAdvMeametAddress.Caption := AnnualRepData.AdvMeametAddress;
  edtAdvMeametID.Caption := FormatInt(AnnualRepData.AdvMeametID);
  edtAdvMeametLicence.Caption := FormatInt(AnnualRepData.AdvMeametLicence);
  edtAccName.Caption := AnnualRepData.AccName;
  edtSignName2.Caption := AnnualRepData.AccSignName;
  edtSignID2.Caption := FormatInt(AnnualRepData.AccSignID);
  edtAccMeametName.Caption := AnnualRepData.AccMeametName;
  edtAccMeametAddress.Caption := AnnualRepData.AccMeametAddress;
  edtAccMeametID.Caption := FormatInt(AnnualRepData.AccMeametID);
  edtAccMeametLicence.Caption := FormatInt(AnnualRepData.AccMeametLicence);
  // Show date
  if (fmMain.UserOptions.PrintDatesInDoh) then
  begin
    edtREportIsUpToDateAt.Caption := DateToStr(AnnualRepData.DecisionDate);
    edtAnnualAssemblyDate.Caption := DateToStr(AnnualRepData.NotifyDate);
    edtDateBroughtReports.Caption := DateToStr(AnnualRepData.ReportSentAt);
  end
  else
  begin
    edtREportIsUpToDateAt.Caption := '';
    edtAnnualAssemblyDate.Caption := '';
    edtDateBroughtReports.Caption := '';
  end;

  edtCompanyName.Caption := Company.Name;
  edtCompNum.Caption := FormatInt(Company.Zeut);
  edtCompanyAddress.Caption := FormatAddress(Company.Address.Street, Company.Address.Number,
                       Company.Address.City, Company.Address.Index, Company.Address.Country);
  edtCompanyPhone.Caption := FormatInt(Company.Address.Phone);
  edtCompanyEmail.Caption := Company.CompanyInfo.Mail;
  Company.HonList.Filter := afActive;

  BigHon := (Company.HonList.Count > 3);

  totalCapital := 0;
  for i := 0 to Company.HonList.Count - 1 do
  begin
    with Company.HonList.Items[i] as THonItem do
      totalCapital := totalCapital + Value * Rashum;
  end;
  //VG19364
  edtTotalCapitalCS.caption := '';
  edtTotalCapital.Caption := FormatDoubleForQRControl(totalCapital, 4);  //sts21232
  if trim(edtTotalCapital.Caption) <> '' then // sts19925
    edtTotalCapitalCS.caption := '�"�';

  for i := 0 to 2 do
  begin
    if (i < Company.HonList.Count) and (not BigHon) then
    begin
      strStockName := Company.HonList.Items[i].Name;
      strSaidStockValue := FloatToStr(THonItem(Company.HonList.Items[i]).Value);
      strStockType := Company.HonList.Items[i].Zeut;
      //VG19364
      strRashum := FormatDoubleForQRControl(THonItem(Company.HonList.Items[i]).Rashum, 2);  //sts22057
      strTotalStockValue := FloatToStr(THonItem(Company.HonList.Items[i]).Value);
      //VG19364
      strAssignedStocksNumber := FormatDoubleForQRControl(THonItem(Company.HonList.Items[i]).Mokza, 2); //sts22057
      curSign := '�"�';   //sts19925
    end
    else
    begin
      strStockName := '';
      strSaidStockValue := '';
      strStockType := '';
      strRashum := '';
      strTotalStockValue := '';
      strAssignedStocksNumber := '';
      curSign := '';
    end;
    SetCaption(Self, 'edtStockName' + IntToStr(i + 1), format('%s  %s  %s', [strStockName, strSaidStockValue, curSign])); //sts19925
    SetCaption(Self, 'edtStockTypeA' + IntToStr(i + 1), strStockName);              // sts19925
    SetCaption(Self, 'edtRashum' + IntToStr(i + 1), strRashum);
    SetCaption(Self, 'edtRashumNm' + IntToStr(i + 1), strStockName);                // sts19925
    SetCaption(Self, 'edtTotalStockValue' + IntToStr(i + 1), strTotalStockValue);
    SetCaption(Self, 'edtTotalStockValueCS' + IntToStr(i + 1), curSign);            // sts19925
    SetCaption(Self, 'edtAssignedStocksNumber' +  IntToStr(i + 1), strAssignedStocksNumber);
  end;

  strName := '';
  strZeut := '';
  strAddress := '';
  strType := '';
  strCount := '';
  strPaid := '';
//VG19075
//  nStocks1 := 0;
//  nStocks2 := 0;
//  nStocks3 := 0;

  for i := 1 to 3 do
  begin
    SetCaption(Self, 'edtStockHolderName' +     IntToStr(i), '');
    SetCaption(Self, 'edtStockHolderID' +       IntToStr(i), '');
    SetCaption(Self, 'edtStockHolderAddress' +  IntToStr(i), '');
    SetCaption(Self, 'edtStockType' +           IntToStr(i), '');
    SetCaption(Self, 'edtStocksNum' +           IntToStr(i), '');
    SetCaption(Self, 'edtSumNotPaid' +          IntToStr(i), '');
  end;

  Company.StockHolders.Filter := afActive;
  nStocks := 0;
  for i := 0 to Company.StockHolders.Count-1 do
  begin
    nStocks := nStocks + TStockHolder(Company.StockHolders.Items[i]).Stocks.Count;
  end;
  BigStocks := (Company.StockHolders.Count > 3) or (nStocks > 3);
  if (not BigStocks) then
  begin
    g := 1;
    for i := 0 to Company.StockHolders.Count-1 do
    begin
      StockHolder := TStockHolder(Company.StockHolders.Items[i]);
      strName := Company.StockHolders.Items[i].Name;
      strZeut := FormatInt(Company.StockHolders.Items[i].Zeut);
      strAddress := FormatAddress(StockHolder.Address.Street, StockHolder.Address.Number, StockHolder.Address.City,
                                  StockHolder.Address.Index, StockHolder.Address.Country);
      StockHolder.Stocks.Filter := afActive;

      for j := 0 to StockHolder.Stocks.Count - 1 do
      begin
        Stock     := TStock(StockHolder.Stocks.Items[j]);
        strType   := Stock.Name;
        strCount  := FloatToStr(Stock.Count);
        strPaid   := FloatToStr(Stock.Paid);

        SetCaption(Self, 'edtStockHolderName' +     IntToStr(g), strName);
        SetCaption(Self, 'edtStockHolderID' +       IntToStr(g), strZeut);
        SetCaption(Self, 'edtStockHolderAddress' +  IntToStr(g), strAddress);
        SetCaption(Self, 'edtStockType' +           IntToStr(g), strType);
        SetCaption(Self, 'edtStocksNum' +           IntToStr(g), strCount);
        SetCaption(Self, 'edtSumNotPaid' +          IntToStr(g), strPaid);
        Inc(g);
      end;
    end;
  end
  else
  begin
    for i := 1 to 3 do
    begin
      SetCaption(Self, 'lblStockHolderName' +     IntToStr(i), '');
      SetCaption(Self, 'lblStockTypeA' +          IntToStr(i), '');
      SetCaption(Self, 'lblStockHolderID' +       IntToStr(i), '');
      SetCaption(Self, 'lblStocksNum' +           IntToStr(i), '');
      SetCaption(Self, 'lblStockHolderAddress' +  IntToStr(i), '');
      SetCaption(Self, 'lblStockHolderAddressA' + IntToStr(i), '');
      SetCaption(Self, 'lblSumNotPaid' +          IntToStr(i), '');
    end;
  end;

  totalMuhaz := 0;
  Company.MuhazList.Filter := afActive;
  BigMuhaz := (Company.MuhazList.Count>2);
  for i := 0 to Company.MuhazList.Count-1 do
  begin
    totalMuhaz := totalMuhaz+TMuhazItem(Company.MuhazList.Items[i]).ShtarValue;
  end;
  edtTOtalMuhaz.Caption := FloatToStr(totalMuhaz);
  for i := 0 to 1 do
  begin
    if i < Company.MuhazList.Count then
    begin
      strZeut := Company.MuhazList.Items[i].Zeut;
      strShtarValue := FloatToStr(TMuhazItem(Company.MuhazList.Items[i]).ShtarValue);
    end
    else
    begin
      strZeut := '';
      strShtarValue := '';
    end;
    SetCaption(Self,'edtStocksNumInShtar' + IntToStr(i + 1),strShtarValue);
    SetCaption(Self,'edtShtarNum' + IntToStr(i + 1),strZeut);
  end;

  Company.Directors.Filter := afActive;
  DirCount1 := Company.Directors.Count;
  BigDirectors := (DirCount1 > 4);
  for i := 0 to 3 do
  begin
    if ((i < Company.Directors.Count) and (not BigDirectors)) then
    begin
      strDirName := Company.Directors.Items[i].Name;
      strDirZeut := FormatInt(Company.Directors.Items[i].Zeut);
      strDirStartDate := FormatDateTime('dd/mm/yyyy',TDirector(Company.Directors.Items[i]).Date1);
      strDirAddress := FormatAddress(TDirector(Company.Directors.Items[i]).Address.Street,
                                     TDirector(Company.Directors.Items[i]).Address.Number,
                                     TDirector(Company.Directors.Items[i]).Address.City,
                                     TDirector(Company.Directors.Items[i]).Address.Index,
                                     TDirector(Company.Directors.Items[i]).Address.Country);
    end
    else
    begin
      strDirName := '';
      strDirZeut := '';
      strDirStartDate := '';
      strDirAddress := '';
    end;
    SetCaption(Self, 'edtDirectorName' +      IntToStr(i + 1), strDirName);
    SetCaption(Self, 'edtDirectorZeut' +      IntToStr(i + 1), strDirZeut);
    SetCaption(Self, 'edtDirectorStartDate' + IntToStr(i + 1), strDirStartDate);
    SetCaption(Self, 'edtDirectorAddress' +   IntToStr(i + 1), strDirAddress);

    if (BigDirectors) then
    begin
      SetCaption(Self, 'lblDirectorName' +      IntToStr(i + 1), '');
      SetCaption(Self, 'lblDirectorZeut' +      IntToStr(i + 1), '');
      SetCaption(Self, 'lblDirectorStartDate' + IntToStr(i + 1), '');
      SetCaption(Self, 'lblDirectorAddress' +   IntToStr(i + 1), '');
    end;
  end;

  Company.Directors.Filter := afNonActive;
  taMemPastDirector.Open;
  for i := 0 to Company.Directors.Count - 1 do
  begin
    if (Company.Directors.Items[i].LastChanged >= AnnualRepData.LastDoh) then
    begin
      taMemPastDirector.Append;
      taMemPastDirector.FieldByName('Name').AsString := Company.Directors.Items[i].Name;
      taMemPastDirector.FieldByName('Zeut').AsString := FormatInt(Company.Directors.Items[i].Zeut);
      taMemPastDirector.FieldByName('Date').AsDateTime := TDirector(Company.Directors.Items[i]).LastChanged;
      taMemPastDirector.Post;
    end;
  end;

  DirCount2 := taMemPastDirector.RecordCount; // Company.Directors.Count;
  BigNoDirectors := (DirCount2 > 4);
  taMemPastDirector.First;
  for i := 0 to 3 do
  begin
    if (i < taMemPastDirector.RecordCount) then
    begin
      strDirName := taMemPastDirector.FieldByName('Name').AsString;
      strDirZeut := taMemPastDirector.FieldByName('Zeut').AsString;
      strDirEndDate := FormatDateTime('dd/mm/yyyy', taMemPastDirector.FieldByName('Date').AsDateTime);
      taMemPastDirector.Next;
    end
    else
    begin
      strDirName := '';
      strDirZeut := '';
      strDirEndDate := '';
    end;
    SetCaption(Self, 'edtNoDirectorName' +    IntToStr(i + 1), strDirName);
    SetCaption(Self, 'edtNoDirectorZeut' +    IntToStr(i + 1), strDirZeut);
    SetCaption(Self, 'edtNoDirectorEndDate' + IntToStr(i + 1), strDirEndDate);
  end;
  taMemPastDirector.Close;

  lblAppendix1.Enabled := BigHon;
  lblAppendix5.Enabled := BigDirectors;
  lblAppendix4.Enabled := BigStocks;

  ShowAppendix := (BigHon or BigStocks or BigDirectors or BigNoDirectors or BigMuhaz or BigManagers);

  if ShowAppendix then
  begin
    StartTable(taMemHon);
    StartTable(taMemStocks);
    StartTable(taMemDirector);
    StartTable(taMemPastDirector);
    StartTable(taMemMuhaz);
    StartTable(taMemManager);
  end;

  edtCaption.Caption := '';
  if ShowAppendix then
  begin
    if BigHon then
      edtCaption.Caption := '��� �����';
    Company.HonList.Filter := afActive;
    for i := 0 to Company.HonList.Count-1 do
    begin
      taMemHon.Append;
      taMemHon.FieldByName('Name').AsString := Company.HonList.Items[i].Name;
      taMemHon.FieldByName('Type').AsString := Company.HonList.Items[i].Zeut;
      taMemHon.FieldByName('Value').AsFloat := THonItem(Company.HonList.Items[i]).Value;
      taMemHon.FieldByName('Rashum').AsFloat :=  THonItem(Company.HonList.Items[i]).Rashum;
      taMemHon.FieldByName('Mokza').AsFloat := THonItem(Company.HonList.Items[i]).Mokza;
      taMemHon.Post;
    end;
  end;
  if ShowAppendix then
  begin
    if BigStocks then
    begin
      if (edtCaption.Caption <> '') then
        edtCaption.Caption := edtCaption.Caption + ', ';
      edtCaption.Caption := edtCaption.Caption + '���� �����';
    end;
    Company.StockHolders.Filter := afActive;
    for i := 0 to Company.StockHolders.Count-1 do
    begin
      TStockHolder(Company.StockHolders.Items[i]).Stocks.Filter := afActive;
      for j := 0 to TStockHolder(Company.StockHolders.Items[i]).Stocks.Count-1 do
      begin
        taMemStocks.Append;
        taMemStocks.FieldByName('Name').AsString := Company.StockHolders.Items[i].Name;
        taMemStocks.FieldByName('Zeut').AsString := FormatInt(Company.StockHolders.Items[i].Zeut);
        taMemStocks.FieldByName('City').AsString := TStockHolder(Company.StockHolders.Items[i]).Address.City;
        taMemStocks.FieldByName('Street').AsString := TStockHolder(Company.StockHolders.Items[i]).Address.Street;
        taMemStocks.FieldByName('Number').AsString := FormatInt(TStockHolder(Company.StockHolders.Items[i]).Address.Number);
        taMemStocks.FieldByName('Index').AsString := FormatInt(TStockHolder(Company.StockHolders.Items[i]).Address.Index);
        taMemStocks.FieldByName('StockName').AsString := TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j].Name;
        taMemStocks.FieldByName('StockCount').AsFloat := TSTock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count;
        taMemStocks.FieldByName('StockPaid').AsFloat := TSTock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Paid;
        taMemStocks.Post;
      end;
    end;
  end;

  if ShowAppendix then
  begin
    if BigDirectors then
    begin
      if (edtCaption.Caption <> '') then
        edtCaption.Caption := edtCaption.Caption + ', ';
      edtCaption.Caption := edtCaption.Caption + '��������� ������';
    end;
    Company.Directors.Filter := afActive;
    for i := 0 to Company.Directors.Count-1 do
    begin
      taMemDirector.Append;
      taMemDirector.FieldByName('Name').AsString    := Company.Directors.Items[i].Name;
      taMemDirector.FieldByName('Zeut').AsString    := FormatInt(Company.Directors.Items[i].Zeut);
      taMemDirector.FieldByName('City').AsString    := TDirector(Company.Directors.Items[i]).Address.City;
      taMemDirector.FieldByName('Street').AsString  := TDirector(Company.Directors.Items[i]).Address.Street;
      taMemDirector.FieldByName('Number').AsString  := FormatInt(TDirector(Company.Directors.Items[i]).Address.Number);
      taMemDirector.FieldByName('Index').AsString   := FormatInt(TDirector(Company.Directors.Items[i]).Address.Index);
      taMemDirector.FieldByName('Date1').AsDateTime := TDirector(Company.Directors.Items[i]).Date1;
      taMemDirector.Post;
    end;
  end;

  if ShowAppendix then
  begin
    Company.MuhazList.Filter := afActive;
    for i := 0 to Company.MuhazList.Count-1 do
    begin
      taMemMuhaz.Append;
      taMemMuhaz.FieldByName('ShtarValue').AsFloat := TMuhazItem(Company.MuhazList.Items[i]).ShtarValue;
      taMemMuhaz.FieldByName('Zeut').AsString := FormatInt(Company.MuhazList.Items[i].Zeut);
      taMemMuhaz.Post;
    end;
  end;

  if ShowAppendix then
  begin
    for i := 0 to AnnualRepData.ManagerCount - 1 do
    begin
      taMemManager.Append;
      taMemManager.FieldByName('Name').AsString := AnnualRepData.ManagerName[i];
      taMemManager.FieldByName('Zeut').AsString := FormatInt(AnnualRepData.ManagerID[i]);
      taMemManager.FieldByName('Address').AsString := AnnualRepData.ManagerAddress[i];
      taMemManager.FieldByName('Telephone').AsString := FormatInt(AnnualRepData.ManagerPhone[i]);
      taMemManager.Post;
    end;
  end;

  if ShowAppendix then
  begin
    edtCompanyName2.Caption := Company.Name;
    edtCompNum2.Caption := Company.Zeut;
    //VG19364
    edtAppendixTotalHon.Caption := FormatDoubleForQRControl(totalCapital, 2); //sts22057
  end;

  p1 := 0;
  p2 := 0;
  if BigHon then
    p1 := 1
  else if BigStocks then
    p1 := 5;
  if BigStocks then
   p2 := 8
  else if BigHon then
    p2 := 4;
  if (p1 > 0) and (p2 > 0) then
  begin
    for i := p1 to p2 do
    begin
      qrShape := (FindCOmponent('sp' + IntToStr(i)) as TQRShape);  //sts19925
      if qrShape <> nil then
        qrShape.Visible := False;
    end;
  end;

  //VG19364
  FOriginalFontSize := QRDBText34.Font.Size;
end; // TQRAnnualReport.LoadData

procedure TQRAnnualReport.PrintProc(DoPrint: Boolean);
begin
  // Moshe 26/12/2017
  fmPrinterSetup.FormatQR(QuickRep1);
  fmPrinterSetup.FormatQR(QuickRep2);
  fmPrinterSetup.FormatQR(QuickRep3);
  fmPrinterSetup.FormatQR(QuickRep4);
  fmPrinterSetup.BindReportToPrinter(QuickRep5);  //sts20654

  fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
  if (DoPrint) then
  try
    QRCompositeReport1.Print;
    Application.ProcessMessages;
  except
    on e: exception do
      MessageDlg(e.Message, mtError, [mbOk],0);
  end
  else
  begin
    QRCompositeReport1.Preview;
  end;
  if (ShowAppendix) then
    try
    if (DoPrint) then
      QuickRep5.Print
    else
      QuickRep5.Preview;
    Application.ProcessMessages;
  except
    on e: exception do
      MessageDlg(e.Message, mtError, [mbOk],0);
  end;
end; // TQRAnnualReport.PrintProc

procedure TQRAnnualReport.PrintExecute(const FileName: string; Company: TCompany; AnnualReportData: TAnnualReportData);
begin
  LoadData(Company, AnnualReportData);
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TQRAnnualReport.PrintEmpty(const FileName: String);
begin
  LoadData(nil, nil);
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TQRAnnualReport.QRCompositeReport1AddReports(Sender: TObject);
begin
  QRCompositeReport1.Reports.Clear;
  QRCompositeReport1.Reports.Add(QuickRep1);
  QRCompositeReport1.Reports.Add(QuickRep2);
  QRCompositeReport1.Reports.Add(QuickRep3);
  QRCompositeReport1.Reports.Add(QuickRep4);
end;

procedure TQRAnnualReport.FormCreate(Sender: TObject);
begin
  ShowAppendix := False;
  BigHon := False;
  BigMuhaz := False;
  BigDirectors := False;
  BigNoDirectors := False;
  BigStocks := False;
  BigManagers := False;
end;

procedure TQRAnnualReport.QRBand2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QuickRep2.NewPage;
end;

procedure TQRAnnualReport.QRBand3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QuickRep3.NewPage;
end;

procedure TQRAnnualReport.QRBand4BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QuickRep4.NewPage;
end;

procedure TQRAnnualReport.QRBand5BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QuickRep5.NewPage;
end;

// VG19364
procedure TQRAnnualReport.QRDBTextPrint(sender: TObject; var Value: String);
begin
  Value := FormatDoubleFromQRDBText(sender as TQRDBText, 2);   //sts22057
end;

procedure TQRAnnualReport.subHoldersBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  DefineAppropriateFontSize4QRDBText(QRDBText34, FOriginalFontSize);
end;

procedure TQRAnnualReport.subHonBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  DefineAppropriateFontSize4QRDBText(QRDBText26, FOriginalFontSize);
  DefineAppropriateFontSize4QRDBText(QRDBText24, FOriginalFontSize);
end;

//sts19925  - arrange controls before printing
procedure TQRAnnualReport.QRBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  procedure SetLeftOfLbl(lblLeft, lblRight: TQrlabel; limit: integer);
  begin
    lblLeft.left := lblRight.left - 10 - lblLeft.width;
    if lblLeft.left < limit then
      lblLeft.left := limit;
  end;
begin
  edtTotalCapital.left := QRShape7.Width - edtTotalCapital.width - 10;  // sts20654
  SetLeftOfLbl(edtTotalCapitalCS, edtTotalCapital, 390);
  SetLeftOfLbl(edtRashumNm1, edtRashum1, 444);
  SetLeftOfLbl(edtRashumNm2, edtRashum2, 444);
  SetLeftOfLbl(edtRashumNm3, edtRashum3, 444);
  SetLeftOfLbl(edtTotalStockValueCS1, edtTotalStockValue1, 12);
  SetLeftOfLbl(edtTotalStockValueCS2, edtTotalStockValue2, 12);
  SetLeftOfLbl(edtTotalStockValueCS3, edtTotalStockValue3, 12);
end;

end.
