unit HonTemplate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HGrids, SdGrid, DataObjects;

type
  TfmHonTemplate = class(TfmBlankTemplate)
    Shape59: TShape;
    Bevel2: TBevel;
    Grid: TSdGrid;
    Panel11: TPanel;
    nGrid: THebStringGrid;
    gb21: TPanel;
    gb22: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel26: TPanel;
    HebLabel3: THebLabel;
    procedure GridEnter(Sender: TObject);
    procedure nGridDblClick(Sender: TObject);
    procedure nGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure GridKeyPress(Sender: TObject; var Key: Char);
    procedure gb21Click(Sender: TObject);
    procedure gb22Click(Sender: TObject);
    procedure GridExit(Sender: TObject);
    procedure GridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    HonList: THonList;
    FFilesList: TFilesList;

    function CollectData(var HonList: THonList): THonList;
    procedure LoadEnum(ObjectNum, NewNum: Integer);
    function CheckIncHon: boolean;
    procedure FillValue;
  protected
    IncAction: Boolean;

    procedure EvPrintData; override;
    function EvLoadData: Boolean; override;
    function EvSaveData(Draft: Boolean): Boolean; override;
    procedure EvDeleteEmptyRows; override;
    procedure EvDataLoaded; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmHonTemplate: TfmHonTemplate;

implementation

uses DataM, Util, cSTD, SaveDialog, LoadDialog, dialog2, PrintIncHon,
  PrintDecHon;

{$R *.DFM}

procedure TfmHonTemplate.DeleteFile(FileItem: TFileItem);
var
  HonList: THonList;
begin
  If Not FileItem.FileInfo.Draft Then
    Raise Exception.Create('This file is not Draft at TfmHonTemplate.DeleteFile');
  If Not (FileItem.FileInfo.Action in [faIncrease, faDecrease]) Then
    raise Exception.Create('Invalid file type at TfmHonTemplate.DeleteFile');
  HonList := THonList.LoadFromFile(Company, FileItem);
  HonList.FileDelete(FileItem);
  HonList.Free;
  FileItem.DeleteFile;
end;

procedure TfmHonTemplate.LoadEnum(ObjectNum, NewNum: Integer);
var
  HonItem: THonItem;
begin
  HonItem:= THonItem.Create(NewNum, Company, DecisionDate);
  Grid.RowCount:= Grid.RowCount+1;
  Grid.Cells[0, Grid.RowCount-1] := HonItem.Name;
  Grid.Cells[1, Grid.RowCount-1] := FormatNumber(FloatTostr(HonItem.Rashum), 15, 2, True);
  Grid.Cells[2, Grid.RowCount-1] := FormatNumber(FloatToStr(HonItem.Value),15, 4, True);
  Grid.Cells[3, Grid.RowCount-1] := FloatToStrF(HonItem.Rashum*HonItem.Value, ffNumber, 15, 4);
  HonItem.Free;
end;

function TfmHonTemplate.EvLoadData: Boolean;
var
  FileItem: TFileItem;
  Action: TFileAction;
begin
  Result := False;
  If IncAction Then
    Action:= faIncrease
  Else
    Action:= faDecrease;
  FileItem := fmLoadDialog.Execute(Self, FFilesList, Company.RecNum, Action);
  If FileItem = Nil Then
    Exit;
  FileName := FileItem.FileInfo.FileName;
  Grid.RowCount := 1;
  DecisionDate := (FileItem.FileInfo.DecisionDate);
  NotifyDate := (FileItem.FileInfo.NotifyDate);
  FileItem.EnumChanges(ocCompany, Company.RecNum, acNewHon, LoadEnum);
  Grid.LineDeleteByIndex(0);
  Grid.Repaint;
  Result := True;
end;

function TfmHonTemplate.CollectData(var HonList: THonList): THonList;

  function CreateNewHonItem(Const Name: String; Value, Count: Double): THonItem;
  var
    Item: THonItem;
  begin
    Result := THonItem.CreateNew(DecisionDate, Name, Value, Count, 0,0,0,0,0, True);
    Item := Company.HonList.FindHonItem(Name, Value);
    if (Item <> nil) then
    begin
      Item.DeActivate(DecisionDate);
      HonList.Add(Item);
      if (IncAction) then
        HonList.Add(THonItem.CreateNew(DecisionDate, Name, Value, Item.Rashum + Count, Item.Mokza,
           Item.Cash, Item.NonCash, Item.Part, Item.PartValue, True))
      else if (Item.Rashum - Count > 0) then
        HonList.Add(THonItem.CreateNew(DecisionDate, Name, Value, Item.Rashum - Count, Item.Mokza,
          Item.Cash, Item.NonCash, Item.Part, Item.PartValue, True));
    end
    else
      HonList.Add(Result);
  end;

var
  i: Integer;
begin
  Result := THonList.CreateNew;
  HonList := THonList.CreateNew;
  Company.HonList.Filter := afActive;
  for i := 0 to Grid.RowCount-1 do
    Result.Add(CreateNewHonItem(Grid.Cells[0,i], StrToFloat(Grid.Cells[2,i]), StrToFloat(Grid.Cells[1,i])));
end;

function TfmHonTemplate.EvSaveData(Draft: Boolean): Boolean;
var
  FileItem: TFileItem;
  FileInfo: TFileInfo;
  Action: TFileAction;
  TempHon: THonList;
begin
  Result := False;

  // Moshe 21/12/2017
  if (FileAction = faIncrease) then
    if (not CheckIncHon) then
      Exit;

  try
    HonList := CollectData(TempHon);
    If IncAction Then
      Action := faIncrease
    Else
      Action := faDecrease;
    FFilesList := TFilesList.Create(Company.RecNum, Action, Draft);
    FileInfo := TFileInfo.Create;
    FileInfo.Action := Action;
    FileInfo.Draft := Draft;
    FileInfo.DecisionDate := DecisionDate;
    FileInfo.NotifyDate := NotifyDate;
    FileInfo.CompanyNum := Company.RecNum;
    FileItem := fmSaveDialog.Execute(Self, FFilesList, FileInfo);
    If FileItem <> nil Then
      begin
        FileItem.FileInfo.Draft := True;
        HonList.ExSaveData(FileItem, Company);
        If Not Draft Then
          TempHon.SaveData(Company, FileItem.FileInfo.RecNum);
        FileName := FileItem.FileInfo.FileName;
        FileItem.Free;
        fmDialog2.Show;
        Result := True;
      end
    else
      FileInfo.Free;
    HonList.Free;
    TempHon.Deconstruct;
    FFilesList.Free;
  finally
  end;
end;

procedure TfmHonTemplate.EvDeleteEmptyRows;
var
  Row: Integer;
begin
  For Row:= Grid.RowCount -1 DownTo Grid.FixedRows Do
    If (RemoveSpaces(Grid.Cells[0, Row], rmBoth) = '') And
       CheckNumberField(Grid.Cells[1, Row]) And
       CheckNumberField(Grid.Cells[2, Row]) Then
      Grid.LineDeleteByIndex(Row);
end;

procedure TfmHonTemplate.EvDataLoaded;
begin
  Inherited;
  if not DoNotClear Then
  begin
    Grid.RowCount := 1;
    Grid.ResetLine(0);
    Grid.Repaint;
  end;
end;
 
procedure TfmHonTemplate.EvFocusNext(Sender: TObject);
begin
  Inherited;
  If Sender = e14 Then
    Grid.SetFocus;
end;

procedure TfmHonTemplate.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
  j, i: Integer;
begin
  inherited;
  CheckGrid(Page, Silent, Extended, Grid);
  for i := 0 to Grid.RowCount-1 do
  begin
    for j := I+1 to Grid.RowCount-1 do
    begin
      if (Grid.Cells[0,i] = Grid.Cells[0,j]) and
         (Grid.Cells[2,i] = Grid.Cells[2,j]) then
      begin
        if not Silent then
        begin
          Grid.Row:= J;
          Grid.Col:= 0;
        end;
        ErrorAt:= Grid;
        raise Exception.Create('�� ���� ����� ���� ���� ���� ���� ���');
      end;
    end;

    if Extended then
      if nGrid.Cols[1].IndexOf(Grid.Cells[0,I]) = -1 then
      begin
        if not Silent then
        begin
          Grid.Row := I;
          Grid.Col := 0;
        end;
        ErrorAt:= Grid;
        raise Exception.Create('���� �� �����');
      end;
  end;
end;

procedure TfmHonTemplate.GridEnter(Sender: TObject);
var
  CanSelect: Boolean;
begin
  inherited;
  if (Grid.Row = 0) and (Grid.Col = 0) then
    GridSelectCell(Grid, 0, 0, CanSelect);
end;

procedure TfmHonTemplate.nGridDblClick(Sender: TObject);
var
  VKKEY: Word;
begin
  inherited;
  VKKEY := VK_NONAME;
  Grid.Cells[0,Grid.Row] := nGrid.Cells[1,nGrid.Row];
  if (nGrid.ColCount = 3) then
    Grid.Cells[2,Grid.Row] := nGrid.Cells[2,nGrid.Row];
  Grid.Col := 1;
  Grid.SetFocus;
  GridKeyDown(Grid, VKKEY, []);
end;

procedure TfmHonTemplate.nGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  i: integer;
begin
  if (Key = VK_DOWN) then
  begin
    for i := 0 to Grid.RowCount-1 do
    begin
      if (Grid.Cells[0, i] = '') then
      begin
        Key := 0;
        Exit;
      end;
    end;
  end;

  inherited;
  if (key = VK_RETURN) then
    nGridDblClick(Sender);

  if (key = VK_ESCAPE) then
    Grid.Col := 1;
  Grid.SetFocus;
end;

procedure TfmHonTemplate.GridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
var
  Found, I: Integer;
  value, rashum: double;
begin
  inherited;

  if Grid.PCol = 1 then
    Grid.Cells[Grid.Pcol,Grid.Prow] := FormatNumber(Grid.Cells[Grid.Pcol,Grid.Prow],10,2,True);
  if (Grid.PCol = 2) or (Grid.PCol = 3) then
    Grid.Cells[Grid.Pcol,Grid.Prow] := FormatNumber(Grid.Cells[Grid.Pcol,Grid.Prow],10,4,True);

  if (Grid.Pcol=0) then
    begin
      Found:=-1;
        for i:=0 to nGrid.RowCount-1 do
          if (Grid.Cells[0,Grid.PRow]=nGrid.Cells[0,i]) and (RemoveSpaces(nGrid.Cells[0,I], rmBoth)<>'') then
            Found:=i;
      if Found<>-1 then
        begin
          Grid.Cells[0,Grid.PRow]:=nGrid.Cells[1,Found];
          If nGrid.ColCount = 3 then
            Grid.Cells[2,Grid.PRow]:=nGrid.Cells[2,Found];
        end;
    end;

  With Grid.Rows[Grid.PRow] Do
  try
    value := StringToFloat(Strings[1]);
    rashum := StringToFloat(Strings[2]);
    //Strings[3] := FloatToStrF(StrToFloat(Strings[1]) * StrToFloat(Strings[2]), ffNumber, 15, 4);
    Strings[3] := FloatToStrF(value * rashum, ffNumber, 15, 4);
  except
    Strings[3] := '';
  end;

  nGrid.Visible := (Col = 0);
end;

procedure TfmHonTemplate.FormCreate(Sender: TObject);
begin
  ShowStyle:= afActive;
  inherited;
end;

procedure TfmHonTemplate.GridKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (Key = #13) then
    if ((Sender as TSDGrid).EditorMode = False) then
      (Sender as TSDGrid).EditorMode := True
    else
      nGrid.Visible := False;
end;

procedure TfmHonTemplate.gb21Click(Sender: TObject);
begin
  inherited;
  Grid.LineDelete;
end;

procedure TfmHonTemplate.gb22Click(Sender: TObject);
begin
  inherited;
  Grid.LineInsert;
end;

procedure TfmHonTemplate.EvPrintData;
var
  TempForm: TfmQRIncHon;
  TempHon, HonList: THonList;
begin
  If IncAction then
    Application.CreateForm(TfmQRIncHon, TempForm)
  Else
    Application.CreateForm(TfmQRDecHon, TempForm);

  HonList:= CollectData(TempHon);
  try
    TempForm.PrintExecute(FileName, Company, HonList, DecisionDate, NotifyDate);
  finally
    TempForm.Free;
    HonList.Free;
    TempHon.Deconstruct;
  end;
end;

// Moshe 21/12/2017
function TfmHonTemplate.CheckIncHon: boolean;
var
  i, j: integer;
  HonItem: THonItem;
  Name: string;
  Value: double;
begin
  Result := False;
  try
  for i := 0 to Grid.RowCount-1 do
  begin
    Name := Grid.Cells[0,i];
    Value := StrToFloat(Grid.Cells[2,i]);
    Result := False;
    for j := 0 to Company.HonList.Count-1 do
    begin
      HonItem := Company.HonList.Items[j] as THonItem;
      if ((HonItem.Name = Name) and (HonItem.Value = Value)) then
      begin
        Result := True;
        Break;
      end
    end;
    if (not Result) then
      Break;
  end;
  if (not Result) then
    Application.MessageBox(PChar('���� ���� �����'),'����� �������' ,MB_OK);
  except on E: Exception do
    WriteChangeLog( 'TfmHonTemplate.CheckIncHon: ' + E.Message);
  end;
end; // TfmHonTemplate.CheckIncHon

procedure TfmHonTemplate.GridExit(Sender: TObject);
var
  Value, Rashum: double;
begin
  inherited;
  With Grid.Rows[Grid.PRow] Do
  try
    Value := StringToFloat(Strings[1]);
    Rashum := StringToFloat(Strings[2]);
    Strings[3] := FloatToStrF(Value * Rashum, ffNumber, 15, 4);
  except
    Strings[3] := '';
  end;
end;

// Moshe 14/10/2018
procedure TfmHonTemplate.FillValue;
var
  i: integer;
  Value, Rashum: double;
begin
  for i := 0 to Grid.RowCount-1 do
  begin
    try
    Rashum := StringToFloat(Grid.Cells[1, i]);
    Value  := StringToFloat(Grid.Cells[2, i]);
    Grid.Cells[3, i] := FloatToStrF(Rashum * Value, ffNumber, 15, 4);
    except end;
  end;
end;

procedure TfmHonTemplate.GridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  FillValue;
end;

end.

