{
HISTORY:
DATE        VERSION   BY   TASK      DESCRIPTION
17/12/2019  1.7.7     VG   19075     code refactoring; fixed several bugs
24/12/2019  1.7.7     VG   19364     added thousand separators
20/12/2020  1.8.4     sts  22057     fix bug with float round to integer; refactoring
21/02/2021  1.8.5     sts  22432     fixed bug with share value   
}
unit prTakanon;


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, ExtCtrls, Qrctrls, Db, memdset, DataObjects;

type

  TfrmQrTakanon = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRImage4: TQRImage;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    lblDate: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    edtCompanyName: TQRLabel;
    taMemPurposes: TMemDataSet;
    QRDBText1: TQRDBText;
    QRBand2: TQRBand;
    QRLabel8: TQRLabel;
    QRShape1: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape6: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    taMemHon: TMemDataSet;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    taMemResponsibility: TMemDataSet;
    taMem175: TMemDataSet;
    taMemSignaturePermit: TMemDataSet;
    taMemStockHolders: TMemDataSet;
    QRCompositeReport1: TQRCompositeReport;
    QuickRep4: TQuickRep;
    taMemSignature: TMemDataSet;
    QRBand10: TQRBand;
    QRBand11: TQRBand;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    edtAdvMeametName: TQRLabel;
    lblAdvMeametID: TQRLabel;
    edtAdvMeametID: TQRLabel;
    lblAdvMeametLicence: TQRLabel;
    edtAdvMeametLicence: TQRLabel;
    QRLabel38: TQRLabel;
    edtAdvMeametAddress: TQRLabel;
    lblApprove: TQRLabel;
    QRMemo1: TQRMemo;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    edtDate: TQRLabel;
    QRLabel45: TQRLabel;
    QRShape33: TQRShape;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel56: TQRLabel;
    edtNumStocks: TQRLabel;
    cbNoValueStocks2: TQRLabel;
    QRLabel58: TQRLabel;
    cbValueStocks2: TQRLabel;
    QRLabel60: TQRLabel;
    edtTotalStocksValue: TQRLabel;
    edtLefiHaluka: TQRLabel;
    edtShah: TQRLabel;
    QRLabel63: TQRLabel;
    QRShape3: TQRShape;
    QRShape8: TQRShape;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRShape7: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRShape40: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    taMemStockHolders22: TMemDataSet;
    QuickRep2: TQuickRep;
    QRBand3: TQRBand;
    QRSubDetail9: TQRSubDetail;
    spmv4: TQRShape;
    spmv3: TQRShape;
    spmv2: TQRShape;
    spmv1: TQRShape;
    spmv5: TQRShape;
    spmv6: TQRShape;
    dbtmv4: TQRDBText;
    dbtmv6: TQRDBText;
    dbtmv3: TQRDBText;
    dbtmv5: TQRDBText;
    dbtmv2: TQRDBText;
    dbtmv1: TQRDBText;
    QRLabel234: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel91: TQRLabel;
    cbNoValueStocks: TQRLabel;
    QRLabel93: TQRLabel;
    cbValueStocks: TQRLabel;
    QRLabel95: TQRLabel;
    edtStocksHoldSum: TQRLabel;
    QuickRep3: TQuickRep;
    QRBand5: TQRBand;
    QRLabel24: TQRLabel;
    QRLabel50: TQRLabel;
    QRDBText14: TQRDBText;
    QRBand6: TQRBand;
    QRLabel26: TQRLabel;
    QRLabel51: TQRLabel;
    QRDBText15: TQRDBText;
    QRBand7: TQRBand;
    QRLabel27: TQRLabel;
    QRLabel52: TQRLabel;
    QRDBText18: TQRDBText;
    QRBand8: TQRBand;
    QRLabel28: TQRLabel;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel53: TQRLabel;
    QRShape23: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText23: TQRDBText;
    QRLabel40: TQRLabel;
    QRDBText24: TQRDBText;
    QRLabel41: TQRLabel;
    QRDBText25: TQRDBText;
    QRLabel42: TQRLabel;
    lblNIS: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape18: TQRShape;
    QRLabel20: TQRLabel;
    QRShape17: TQRShape;
    QRShape19: TQRShape;
    QRShape12: TQRShape;
    QRDBText8: TQRDBText;
    QRLabel71: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRShape44: TQRShape;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel72: TQRLabel;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText36: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText33: TQRDBText;
    QRDBText34: TQRDBText;
    QRDBText35: TQRDBText;
    QRShape56: TQRShape;
    QRShape51: TQRShape;
    QRShape52: TQRShape;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRShape53: TQRShape;
    QRLabel85: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel260: TQRLabel;
    QRLabel258: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel89: TQRLabel;
    QRShape57: TQRShape;
    QRShape42: TQRShape;
    QRShape55: TQRShape;
    QRLabel87: TQRLabel;
    QRShape45: TQRShape;
    QRShape47: TQRShape;
    QRShape43: TQRShape;
    QRShape46: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    QRShape50: TQRShape;
    QRShape58: TQRShape;
    QRShape54: TQRShape;
    QRLabel61: TQRLabel;
    QRShape41: TQRShape;
    QRLabel33: TQRLabel;
    QRLabel54: TQRLabel;
    edtTakanon7: TQRMemo;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    edt175_1_Yes: TQRLabel;
    edt175_2_Yes: TQRLabel;
    edt175_3_Yes: TQRLabel;
    edt175_1_No: TQRLabel;
    edt175_2_No: TQRLabel;
    edt175_3_No: TQRLabel;
    edt175_1_Text: TQRLabel;
    edt175_2_Text: TQRLabel;
    edt175_3_Text: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRLabel170: TQRLabel;
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure QRDBText15Print(sender: TObject; var Value: String);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand13AfterPrint(Sender: TQRCustomBand; BandPrinted: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRDBTextPrint(sender: TObject; var Value: String);
    procedure QRSubDetail2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText26Print(sender: TObject; var Value: String);
    procedure QRDBText2Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    prevID:integer;
    page1printed,IsEmpty:boolean;
    StockHoldersComponents:TStringList;

    //VG19364:
    //before create the report we will remember the original font size for QR DB controls
    //and will use it when we need
    FOriginalFontSize: Integer;

    procedure CreateMemTables(Company:TCompany);
    procedure StartEmptyTables;
    procedure LoadData(Company:TCompany);
    procedure AppendPlainText2Purpose(cTxt: String);
  public
    { Public declarations }
    procedure PrintExecute(Filename:string;DoPrint:boolean;Company:TCompany);
    procedure PrintEmpty(const Filename: String);
    procedure PrintProc(DoPrint:boolean);
    procedure InnerPrintProc(ACompany:TCompany;DoPrint:boolean);
  end;

var
  frmQrTakanon: TfrmQrTakanon;

implementation

uses
  PrinterSetup, Util, utils2;

{$R *.DFM}

procedure TfrmQrTakanon.InnerPrintProc(ACompany:TCompany;DoPrint:boolean);
begin
  LoadData(ACompany);
  PrintProc(DoPrint);
end;

procedure TfrmQrTakanon.PrintEmpty(const Filename: String);
begin
  LoadData(Nil);
  fmPrinterSetup.Execute(Filename, PrintProc);
end;

procedure TfrmQrTakanon.StartEmptyTables;
begin
  StartTable(taMemPurposes);
  StartTable(taMemHon);
  StartTable(taMemStockHolders);
  StartTable(taMemSignature);
  StartTable(taMemStockHolders22);
  StartTable(taMemResponsibility);
  StartTable(taMem175);
  StartTable(taMemSignaturePermit);
end;

//VG19075
procedure TfrmQrTakanon.AppendPlainText2Purpose(cTxt: String);
var
  lstSplited: TStringList;
  cBuffStr: String;
  cSpaceStr: String;
  i: Integer;
  nLen: Integer;
begin
  lstSplited := TStringList.Create;
  try
    cBuffStr := Trim(cTxt);
    if (Length(cBuffStr) > 0) and (cBuffStr <> DefaultTakanon2) then
    begin
      splitPlainText(cBuffStr, lstSplited);
      nLen := lstSplited.Count - 1;
      cSpaceStr := Space(4);
      for i := 0 to nLen do
      begin
        AppendStr2(taMemPurposes, lstSplited[i], cSpaceStr);
      end;
    end;
  finally
    lstSplited.Free;
  end;
end;

procedure TfrmQrTakanon.CreateMemTables(Company:TCompany);
var
  i, j, id, code: integer;
  totalCount, totalValue, allocated: double;
  address: string;
  //VG19075
  tv, cnt: double;
  dblTmp: double;
  FirstName, LastName, str: string;
  NumericID: boolean;

begin
  StartEmptyTables;
  if (Company = nil) then
  begin
    // Moshe 07/01/2017
    for i := 1 to 8 do
    begin
      taMemHon.Append;
      taMemHon.Post;
    end;

    for i := 1 to 3 do
    begin
      taMemStockHolders22.Append;
      taMemStockHolders22.Post;
    end;

    taMemStockHolders.Append;
    taMemStockHolders.FieldByName('Name').AsString := EmptyPlace(15);
    taMemStockHolders.FieldByName('Zeut').AsString := EmptyPlace(9);
    taMemStockHolders.FieldByName('Address').AsString := EmptyPlace(15);
    taMemStockHolders.Post;

    taMemSignature.Append;
    taMemSignature.FieldByName('Number').AsInteger := 1;
    taMemSignature.FieldByName('Name').AsString := space(15);
    taMemSignature.FieldByName('Zeut').AsString := space(9);
    taMemSignature.Post;

    Exit;
  end;

  edtTakanon7.Lines.Text:=RichTextToPlainText(Company.CompanyInfo.Takanon7);

  if (Company.CompanyInfo.Option1 and 1) > 0 Then
  begin
    str := '.';
    str := str + '��� ���� 32 (1) ���� - ����� ��� ����� ����';
    AppendStr2(taMemPurposes,str,'(1)');
    str := Trim(Company.CompanyInfo.Takanon2_1);
    if (str <> '') and (str <> DefaultTakanon2)then
      AppendStr2(taMemPurposes, str, '');
  end;

  if (Company.CompanyInfo.Option1 and 2) > 0 Then
  begin
    str := ':';
    str := str + '��� ���� 32 (2) ���� - ����� ��� ����� ����, ���� �������� �������� ����';
    AppendStr2(taMemPurposes,str,'(2)');
    //VG19075
    AppendPlainText2Purpose(Company.CompanyInfo.Takanon2_2);
  end;

  if (Company.CompanyInfo.Option1 and 4) > 0 Then
  begin
    str := ':';
    str := str + '��� ���� 32 (3) ���� - ����� �������� �������� ����';
    AppendStr2(taMemPurposes,str, '(3)');
    //VG19075
    AppendPlainText2Purpose(Company.CompanyInfo.Takanon2_3);
  end;

  if (Company.CompanyInfo.Option1 and 8) > 0 Then
  begin
    str := ':';
    str := str + '����� ����� ������ ��������, �������� ����, ����, ��� ���� ����� ��� ����� �� ���� ������';
    AppendStr2(taMemPurposes,str,'(4)');
    //VG19075
    AppendPlainText2Purpose(Company.CompanyInfo.Takanon2_4);
  end;

  totalCount := 0.0;
  totalValue := 0.0;

  Company.HonList.Filter:=afActive;
  for i:=0 to Company.HonList.Count-1 do
  begin
    dblTmp := THonItem(Company.HonList.Items[i]).Value;
    cnt := THonItem(Company.HonList.Items[i]).Rashum;
    tv := dblTmp * cnt;
    totalCount := totalCount+cnt;
    totalValue := totalValue+tv;
    taMemHon.Append;
    taMemHon.FieldByName('Name').AsString := Trim(Company.HonList.Items[i].Name);
    taMemHon.FieldByName('Zeut').AsString := Trim(Company.HonList.Items[i].Zeut);
    taMemHon.FieldByName('ValueSh').AsInteger := trunc(dblTmp);
    taMemHon.FieldByName('ValueAg').AsInteger := GetAgorot(dblTmp);
    taMemHon.FieldByName('Rashum').AsFloat := cnt;
    taMemHon.FieldByName('TotalValueSh').AsInteger := trunc(tv);
    taMemHon.FieldByName('TotalValueAg').AsInteger := GetAgorot(tv);
    taMemHon.Post;
  end;
  //VG19364
  edtNumStocks.Caption := FormatDoubleForQRControl(totalCount, 2);  //sts22057
  edtTotalStocksValue.Caption := FormatDoubleForQRControl(totalValue, 2);
  SetChecked(cbNoValueStocks2,(totalValue <= 0), 3);
  SetChecked(cbValueStocks2,(totalValue > 0), 3);

  Company.StockHolders.Filter := afActive;
  allocated := 0;
  for i := 0 to Company.StockHolders.Count - 1 do
  begin
    val(Trim(Company.StockHolders.Items[i].Zeut), id, code);
    if code <> 0 then
      id := 0;
    NumericID := (code = 0);

    // Moshe 22/05/2019
    if (TStockHolder(Company.StockHolders.Items[i]).Taagid) then
    begin
      LastName := '';
      FirstName := Company.StockHolders.Items[i].Name;
    end
    else
      SplitName(Company.StockHolders.Items[i].Name, FirstName, LastName);

    FirstName := Trim(FirstName);
    LastName := Trim(LastName);
    address := Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Country);
    if (Trim(TStockHolder(Company.StockHolders.Items[i]).Address.City) <> '') and (Trim(address) <> '') then
      address := address + ' ';
    address := address + Trim(TStockHolder(Company.StockHolders.Items[i]).Address.City);
    if (Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Street) <> '') and (Trim(address) <> '')then
      address := address + ' ';
    address := address + Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Street);
    if (Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Number) <> '') and (Trim(address) <> '')
       and(Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Number) <> '0') then
      address := address + ' ';
    if Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Number) <> '0'  then
      address := address+Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Number);
    if (Trim(TSTockHolder(Company.StockHolders.Items[i]).Address.Index) <> '') and (Trim(address) <> '')
      and (Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Index) <> '0') then
      address := address + ' ';
    if Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Index) <> '0'  then
      address := address + Trim(TStockHolder(Company.stockHolders.Items[i]).Address.Index);

    taMemStockHolders.Append;
    taMemStockHolders.FieldByName('Name').AsString := Trim(Company.StockHolders.Items[i].Name);
    if NumericID then
      taMemStockHolders.FieldByName('Zeut').AsString := FormatFloat('000000000', id)
    else
       taMemStockHolders.FieldByName('Zeut').AsString := MySubstr(Trim(Company.StockHolders.Items[i].Zeut), 9);
    taMemStockHolders.FieldByName('Address').AsString := address;
    if TStockHolder(Company.StockHolders.Items[i]).Taagid then
      taMemStockHolders.FieldByName('Taagid').AsString := '��'
    else
      taMemStockHolders.FieldByName('Taagid').AsString := '��';
    taMemStockHolders.Post;

    taMemSignature.Append;
    taMemSignature.FieldByName('Number').AsInteger := i + 1;
    taMemSignature.FieldByName('Name').AsString := Trim(Company.StockHolders.Items[i].Name);
    if NumericID then
      taMemSignature.FieldByName('Zeut').AsString := FormatFloat('000000000', id)
    else
      taMemSignature.FieldByName('Zeut').AsString := MySubstr(Trim(Company.StockHolders.Items[i].Zeut), 9);
    taMemSignature.Post;
    for j := 0 to TStockHolder(Company.StockHolders.Items[i]).Stocks.Count - 1 do
    begin
      taMemStockHolders22.Append;
      taMemStockHolders22.FieldByName('Zeut').AsInteger := id;
      if NumericID then
      begin
       taMemStockHolders22.FieldByName('CheckDigit').AsString := IntToStr(id mod 10);
       taMemStockHolders22.FieldByName('Soder').AsString := FormatFloat('00000000',id div 10);
      end
      else
      begin
       taMemStockHolders22.FieldByName('CheckDigit').AsString := '';
       taMemStockHolders22.FieldByName('Soder').AsString := MySubStr(Trim(Company.StockHolders.Items[i].Zeut), 8);
      end;
      taMemStockHolders22.FieldByName('FirstName').AsString := FirstName;
      taMemStockHolders22.FieldByName('LastName').AsString := LastName;
      taMemStockHolders22.FieldByName('Country').AsString := Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Country);
      taMemStockHolders22.FieldByName('City').AsString := Trim(TStockHolder(Company.StockHolders.Items[i]).Address.City);
      taMemStockHolders22.FieldByName('Street').AsString := Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Street);
      if Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Number) = '0' then
       taMemStockHolders22.FieldByName('Number').AsString := ''
      else
       taMemStockHolders22.FieldByName('Number').AsString := Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Number);
      if Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Index) = '0' then
        taMemStockHolders22.FieldByName('Index').AsString := ''
      else
        taMemStockHolders22.FieldByName('Index').AsString := Trim(TStockHolder(Company.StockHolders.Items[i]).Address.Index);
      taMemStockHolders22.FieldByName('StockName').AsString := Trim(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j].Name);
      taMemStockHolders22.FieldByName('Count').AsFloat := TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count;
      taMemStockHolders22.FieldByName('ValueSh').AsInteger := trunc(TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Value);
      taMemStockHolders22.FieldByName('ValueAg').AsInteger := GetAgorot(TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Value);
      taMemStockHolders22.FieldByName('PaidSh').AsInteger := trunc(TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Paid);
      taMemStockHolders22.FieldByName('PaidAg').AsInteger := GetAgorot(TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Paid);
      taMemStockHolders22.Post;

      allocated  :=  allocated + TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Value
                     * TStock(TStockHolder(Company.StockHolders.Items[i]).Stocks.Items[j]).Count;
    end;
  end;
  edtStocksHoldSum.Caption := MyStr2Float(allocated, 2);
  SetChecked(cbNoValueStocks,(allocated <= 0), 3);
  SetChecked(cbValueStocks,(allocated > 0), 3);
  if Company.CompanyInfo.Option3 = '1' Then
    taMemResponsibility.AppendRecord(['.������ ������'])
  else if Company.CompanyInfo.Option3 = '2' then
    taMemResponsibility.AppendRecord(['(���� ������ ������. ����� ��� ���� ������� ������ (��"�, ��"�, ������ ������'])
  else
    AppendStr(taMemResponsibility,Company.CompanyInfo.OtherInfo);

  taMem175.AppendRecord(['@������ �� ����� �����']);
  case Company.CompanyInfo.SharesRestrictIndex of
    0:taMem175.AppendRecord([':����� ����� ����� �� ����� ������ ����� �����, ����� �������']);
    1:taMem175.AppendRecord(['����� ����� ���� ����� �� ����� ������ ����� �����']);
  end;
  str := Trim(Company.CompanyInfo.SharesRestrictChapters);
  if str<>'' then
    AppendStr(taMem175,str);

  taMem175.AppendRecord(['@����� �� ���� ������ �� ����� �� ����� ���']);
  case Company.CompanyInfo.DontOfferIndex of
    0:taMem175.AppendRecord([':����� ����� ���� ���� ������ �� ����� �� ����� ��� ����� �������']);
    1:taMem175.AppendRecord(['����� ����� ���� ���� ���� ������ �� ����� �� ����� ���']);
  end;
  str := Trim(Company.CompanyInfo.DontOfferChapters);
  if str<>'' then
    AppendStr(taMem175,str);

  taMem175.AppendRecord(['@����� �� ���� ���� ������ �����']);
  case Company.CompanyInfo.ShareHoldersNoIndex of
    0:taMem175.AppendRecord([':����� ����� ����� �� ���� ���� ������ ����� �� ������ �������']);
    1:taMem175.AppendRecord(['����� ����� ���� ����� �� ���� ���� ������ ����� �� ������, ���� ����� �����']);
  end;
  str := Trim(Company.CompanyInfo.ShareHoldersNoChapters);
  if str<>'' then
    AppendStr(taMem175,str);

  case Company.CompanyInfo.LimitedSignatoryIndex of
    0:taMemSignaturePermit.AppendRecord(['.�� ������ ������ ������ �� ������ �������']);
    1:begin
        taMemSignaturePermit.AppendRecord(['������ ������ ������ �� ������ ������� ����� �������']);
        str := Trim(Company.CompanyInfo.LegalSectionsChapters);
        if str<>'' then
          AppendStr(taMemSignaturePermit,str);
      end;
    2:begin
        taMemSignaturePermit.AppendRecord(['������ ������� ������ ���� ������� ����� �������']);
        str := Trim(Company.CompanyInfo.SignatorySectionsChapters);
        if str<>'' then
          AppendStr(taMemSignaturePermit,str);
      end;
  end;
end; // TfrmQrTakanon.CreateMemTables

procedure TfrmQrTakanon.LoadData(Company:TCompany);
var
  date: TDateTime;
  LawZeut, LawLicence: string;
  i: integer;
begin
  PrevID := 0;
  IsEmpty := (Company = nil);
  page1printed := False;
  CreateMemTables(Company);

  if (Company = nil) then
  begin
    for i := 0 to ComponentCount-1 do
    begin
      if (Components[i] is TQRLabel) and (pos('QRLABEL', UpperCase(Components[i].Name)) <> 1) then
        (Components[i] as TQRLabel).Caption := ''
      else if (Components[i] is TQRMemo) then
        (Components[i] as TQRMemo).Lines.Clear;
    end;
    cbNoValueStocks.Caption := Space(10);
    cbValueStocks.Caption := Space(10);
    edtStocksHoldSum.Caption := EmptyPlace(15);
    lblNIS.Caption := EmptyPlace(4);
  end
  else
  begin
    date := Now;
    lblDate.Caption := MyDate2Str(date);
    edtCompanyName.Caption := Trim(Company.Name);
    edtAdvMeametName.Caption := Trim(Company.CompanyInfo.LawName);
    LawZeut := Trim(Company.CompanyInfo.LawZeut);
    if LawZeut='0' then
      LawZeut := '';
    edtAdvMeametID.Caption := LawZeut;
    LawLicence := Trim(Company.CompanyInfo.LawLicence);
    if (LawLicence='0') then
      Lawlicence := '';
    edtAdvMeametLicence.Caption := LawLicence;
    edtAdvMeametAddress.Caption := Trim(Company.CompanyInfo.LawAddress);
    edtDate.Caption := MyDate2Str(date);
  end;
  edtShah.Left := edtTotalStocksValue.Left - edtShah.Width - 5;
  edtLefiHaluka.Left := edtShah.Left - edtLefiHaluka.Width - 5;
  lblNIS.Left := edtStocksHoldSum.Left - lblNIS.Width - 5;

  // Moshe 16/09/2018 - 175 section
  edt175_1_Yes.Caption := ' ';
  edt175_1_No.Caption := ' ';
  edt175_1_Text.Caption := ' ';
  edt175_2_Yes.Caption := ' ';
  edt175_2_No.Caption := ' ';
  edt175_2_Text.Caption := ' ';
  edt175_3_Yes.Caption := ' ';
  edt175_3_No.Caption := ' ';
  edt175_3_Text.Caption := ' ';

  // Moshe 20/01/2019
  if (Company <> nil) then
  begin
    if (Company.CompanyInfo.SharesRestrictIndex = 0) then
    begin
      edt175_1_Yes.Caption := 'X';
      edt175_1_Text.Caption := Company.CompanyInfo.SharesRestrictChapters;
    end
    else if (Company.CompanyInfo.SharesRestrictIndex = 1) then
      edt175_1_No.Caption := 'X';

    if (Company.CompanyInfo.DontOfferIndex = 0) then
    begin
      edt175_2_Yes.Caption := 'X';
      edt175_2_Text.Caption := Company.CompanyInfo.DontOfferChapters;
    end
    else if (Company.CompanyInfo.DontOfferIndex = 1) then
      edt175_2_No.Caption := 'X';

    if (Company.CompanyInfo.ShareHoldersNoIndex = 0) then
    begin
      edt175_3_Yes.Caption := 'X';
      edt175_3_Text.Caption := Company.CompanyInfo.ShareHoldersNoChapters;
    end
    else if (Company.CompanyInfo.ShareHoldersNoIndex = 1) then
      edt175_3_No.Caption := 'X';
  end;
  //VG19364
  FOriginalFontSize := QRDBText27.Font.Size;
end; // TfrmQrTakanon.LoadData

procedure TfrmQrTakanon.PrintProc(DoPrint:boolean);
begin
  fmPrinterSetup.FormatQR(QuickRep1);
  fmPrinterSetup.FormatQR(QuickRep2);
  fmPrinterSetup.FormatQR(QuickRep3);
  fmPrinterSetup.FormatQR(QuickRep4);
  fmPrinterSetup.FormatCompositeQR(QRCompositeReport1);
  if DoPrint then
    QRCompositeReport1.Print
  else
  begin
    QuickRep1.Preview;
    Application.ProcessMessages;

    QuickRep2.Preview;
    Application.ProcessMessages;

    QuickRep3.Preview;
    Application.ProcessMessages;

    QuickRep4.Preview;
    Application.ProcessMessages;
  end;
end;

procedure TfrmQrTakanon.PrintExecute(Filename:string;DoPrint:boolean;Company:TCompany);
begin
  LoadData(Company);
  fmPrinterSetup.Execute(Filename, PrintProc);
end;

procedure TfrmQrTakanon.QRCompositeReport1AddReports(Sender: TObject);
begin
  QRCompositeReport1.Reports.Add(QuickRep1);
  QRCompositeReport1.Reports.Add(QuickRep2);
  QRCompositeReport1.Reports.Add(QuickRep3);
  QRCompositeReport1.Reports.Add(QuickRep4);
end;

procedure TfrmQrTakanon.QRDBText15Print(sender: TObject; var Value: String);
begin
  (Sender as TQRDBText).Font.Style:=GetTitleFontStyle(Value);
end;

procedure TfrmQrTakanon.QRBand4BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  PrintBand:=page1printed;
end;

procedure TfrmQrTakanon.QRBand13AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if not page1printed then
    page1printed := True;
end;

procedure TfrmQrTakanon.FormCreate(Sender: TObject);
var
  i:integer;
begin
  StockHoldersComponents := TStringList.Create;
  for i := 0 to QRSubDetail9.ControlCount - 1 do
    StockHoldersComponents.Add(QRSubDetail9.Controls[i].Name);
end;

procedure TfrmQrTakanon.FormDestroy(Sender: TObject);
begin
  StockHoldersComponents.Free;
end;

procedure TfrmQrTakanon.QRBand3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QuickRep2.NewPage;
end;

procedure TfrmQrTakanon.QRSubDetail2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  DefineAppropriateFontSize4QRDBText(QRDBText6, FOriginalFontSize);
  DefineAppropriateFontSize4QRDBText(QRDBText27, FOriginalFontSize);
end;

procedure TfrmQrTakanon.QRSubDetail9BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  DefineAppropriateFontSize4QRDBText(dbtmv1, FOriginalFontSize);
  DefineAppropriateFontSize4QRDBText(dbtmv3, FOriginalFontSize);
end;

//VG19364
procedure TfrmQrTakanon.QRDBTextPrint(sender: TObject; var Value: String);
begin
  Value := FormatDoubleFromQRDBText(sender as TQRDBText, 0); //sts22057   // decimal no limitation
end;

procedure TfrmQrTakanon.QRDBText26Print(sender: TObject; var Value: String);
begin
  value := FormatFloat('00', StrToIntDef(Value, 0));  //sts22057          // decimal limitet to 2 digits
end;

procedure TfrmQrTakanon.QRDBText2Print(sender: TObject; var Value: String);
begin
  Value := FormatDoubleFromQRDBText(sender as TQRDBText, 2); //sts22057   // float no limitation 2 digits presition
end;

end.
