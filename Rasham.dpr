{
History:
DATE        VERSION   BY   TASK      DESCRIPTION
2020/02/27  1.7.8     sts  19736     fixed bug with wrong net folder in bdeAdmin
}

program Rasham;

uses
  SysUtils,
  Forms,
  windows,
  Main in 'Main.pas' {fmMain},
  DataObjects in 'DataObjects.pas',
  DataM in 'DataM.pas' {Data: TDataModule},
  LoadSaveTemplate in 'LoadSaveTemplate.pas' {fmLoadSaveTemplate},
  Companies in 'Companies.pas' {fmCompanies},
  dialog2 in 'dialog2.pas' {fmDialog2},
  dialog in 'dialog.pas' {fmDialog},
  uMenu in 'uMenu.pas' {fmMenu},
  DialogsTemplate in 'DialogsTemplate.pas' {fmTemplate},
  BlankTemplate in 'BlankTemplate.pas' {fmBlankTemplate},
  Util in 'Util.pas',
  CompanyInfoTemplate in 'CompanyInfoTemplate.pas' {fmCompanyInfoTemplate},
  NewCompanyInfo in 'NewCompanyInfo.pas' {fmNewCompanyInfo},
  EditCompanyInfo in 'EditCompanyInfo.pas' {fmEditCompanyInfo},
  Rishum in 'Rishum.pas' {fmRishum},
  RisCompany in 'RisCompany.pas' {fmRisCompany},
  HonTemplate in 'HonTemplate.pas' {fmHonTemplate},
  IncHon in 'IncHon.pas' {fmIncHon},
  DecHon in 'DecHon.pas' {fmDecHon},
  DraftCompany in 'DraftCompany.pas' {fmDraftCompany},
  LoadDialog in 'LoadDialog.pas' {fmLoadDialog},
  SaveDialog in 'SaveDialog.pas' {fmSaveDialog},
  BlankDirectors in 'BlankDirectors.pas' {fmDirectors},
  BlankStocks in 'BlankStocks.pas' {fmStocks},
  BlankHaavara in 'BlankHaavara.pas' {fmHaavara},
  ShowCompany in 'ShowCompany.pas' {fmShowCompany},
  Number in 'Number.pas' {fmNumber},
  Doh in 'Doh.pas' {fmDoh},
  DohFull in 'DohFull.pas' {fmDohFull},
  BookStock in 'BookStock.pas' {fmBookStocks},
  BookDirectors in 'BookDirectors.pas' {fmBookDirectors},
  BlankAddress in 'BlankAddress.pas' {fmBlankAddress},
  About in 'About.pas' {AboutBox},
  BlankUndo in 'BlankUndo.pas' {fmUndoBlank},
  BlankName in 'BlankName.pas' {fmBlankName},
  StockHolderAddress in 'StockHolderAddress.pas' {fmStockAddress},
  BlankDirectorAddress in 'BlankDirectorAddress.pas' {fmDirectorAddress},
  SugStock in 'SugStock.pas' {fmSugStock},
  SignName in 'SignName.pas' {fmSignName},
  Lawer in 'Lawer.pas' {fmLawer},
  PrintRishum in 'PrintRishum.pas' {fmQRRishum},
  PrintFirstDirector in 'PrintFirstDirector.pas' {fmQRFirstDirector},
  PrintIncHon in 'PrintIncHon.pas' {fmQRIncHon},
  PrintDecHon in 'PrintDecHon.pas' {fmQRDecHon},
  PrintAddress in 'PrintAddress.pas' {fmQRAddress},
  PrintHaavara in 'PrintHaavara.pas' {fmQRHaavara},
  PrinterSetup in 'PrinterSetup.pas' {fmPrinterSetup},
  PrinterSetupQR in 'PrinterSetupQR.pas' {QRPrinterSetup: TQuickRep},
  PrintHakzaa in 'PrintHakzaa.pas' {fmQRHakzaa},
  PrintDoh in 'PrintDoh.pas' {fmQRDoh},
  PrintDirectors in 'PrintDirectors.pas' {fmQRDirectors},
  PrintDirectorBook1 in 'PrintDirectorBook1.pas' {fmQRDirBook1},
  PrintDirectorBook2 in 'PrintDirectorBook2.pas' {fmQRDirBook2},
  PrintStockBook1 in 'PrintStockBook1.pas' {fmQRStockBook1},
  BackUp in 'BackUp.pas' {fmBackUp},
  Register in 'Register.pas' {fmRegister},
  Help in 'Help.pas' {fmHelp},
  Splash in 'Splash.pas' {fmSplash},
  PrintFirstStockHolder in 'PrintFirstStockHolder.pas' {fmQRFirstStockHolder},
  PrintName in 'PrintName.pas' {fmQRName},
  PrintTakanon in 'PrintTakanon.pas' {fmQRTakanon},
  CompaniesDisabler in 'CompaniesDisabler.pas' {fmCompaniesDisabler},
  PeopleSearch in 'PeopleSearch.pas' {fmPeopleSearch},
  ChangeInfo in 'ChangeInfo.pas' {fmChangeInfo},
  RoeHeshbon in 'RoeHeshbon.pas' {fmRoeHeshbon},
  PrintSetup in 'PrintSetup.pas' {fmPrintSetup},
  PrintCompanyInfo in 'PrintCompanyInfo.pas' {fmQRCompanyInfo},
  PrintShtar in 'PrintShtar.pas' {fmQRShtar},
  unMerge in 'unMerge.pas' {fmMergeModule},
  unMergeData in 'unMergeData.pas' {MergeData: TDataModule},
  PrintCompanies in 'PrintCompanies.pas' {fmQRCompanies},
  PrintUndo in 'PrintUndo.pas' {fmQRUndo},
  TakanonEdit in 'TakanonEdit.pas' {fmTakanonEdit},
  unManager in 'unManager.pas' {fmManager},
  unHamara in 'unHamara.pas' {fmHamara},
  dbTables,
  Classes,
  FileCtrl,
  dialogs,
  ExpiryExtend in 'ExpiryExtend.pas' {frmExpiryExtend},
  select in 'select.pas' {fmSelect},
  MultipleDirectorsChoice in 'MultipleDirectorsChoice.pas' {fmMultipleDirectorsChoice},
  UpdateTables in 'UpdateTables.pas',
  FixProblems in 'FixProblems.pas',
  HPSConsts in '..\General\HPSConsts.pas',
  FixManagers in 'FixManagers.pas',
  uRebuild in '..\General\PDX Rebuild\uRebuild.pas',
  PrintRishumRegSubmit in 'PrintRishumRegSubmit.pas' {QRPrintRishumRegSubmit: TQuickRep},
  RishumRegularRequest in 'RishumRegularRequest.pas' {fmRishumRegularRequest},
  PrintRishumQuickSubmit in 'PrintRishumQuickSubmit.pas' {QRPrintRishumQuickSubmit: TQuickRep},
  RishumQuickRequest in 'RishumQuickRequest.pas' {fmRishumQuickRequest},
  RishumPrintingSelection in 'RishumPrintingSelection.pas' {fmRishumPrintingSelection},
  BlankEngName in 'BlankEngName.pas' {fmBlankEngName},
  MSXML2_TLB in '..\..\Program Files\Borland\Delphi 3\Imports\MSXML2_TLB.pas',
  RishumNew in 'RishumNew.pas' {frmQrRishumNewComp: TQuickRep},
  prTakanon in 'prTakanon.pas' {frmQrTakanon},
  PrintSignName in 'PrintSignName.pas' {fmQrSignName},
  utils2 in 'utils2.pas',
  TakanonChange in 'TakanonChange.pas' {fmTakanonChange},
  BlankZip in 'BlankZip.pas' {fmBlankZip},
  listdg in 'listdg.pas' {fmListDlg},
  QRTakanonChange in 'qrTakanonChange.pas' {fmQrTakanonChange},
  TakanonRep in 'TakanonRep.pas' {fmTakanonRep},
  PrintAnnualReport in 'PrintAnnualReport.pas' {QRAnnualReport},
  QRTakanonChange2 in 'QRTakanonChange2.pas' {fmQrTakanonChange2},
  PrintEmptyTakanon in 'PrintEmptyTakanon.pas' {EmptyTakanonForm},
  urRshmOneHinstCheckAndLog in 'urRshmOneHinstCheckAndLog.pas',
  uAppSysUtils in 'uAppSysUtils.pas';

{$R *.RES}

var
  useSplash: Boolean;
  aliasProp: TStringList;
  aliasPath: string;
  path: string;
  logFileName: string;
  fn : string;
  i: integer;
begin
  logFileName := GetLogFileName;
  if FileExists(LogFileName) then
    WriteLog('==== START ======',LogFileName);
  WriteLog('Application.Initialize', LogFileName);
  Application.Initialize;

  //*************** NetDir + Delete LCK
  WriteLog('ExpandUNCFileName', LogFileName);
  Path:= MyExpandUNCFileName(ExtractFilePath(ParamStr(0)) + 'Net\');
  WriteLog('Path = "' + Path + '"', LogFileName);
  WriteLog('ForceDirectories: ' + path, LogFileName);
  ForceDirectories(Path);
  try
    fn := Path + 'Pdoxusrs.net';
    WriteLog('If FileExists(' + fn + ')', LogFileName);
    If FileExists(fn) Then
    begin
      WriteLog('DeleteFile ' + fn, LogFileName);
      SysUtils.DeleteFile(fn);
      If FileExists(fn) Then
       WriteLog('DeleteFile ' + fn, LogFileName + ' failed, file still exist');
    end;
  except
    WriteLog('Delete raised exception', LogFileName);
  end;

  try
    WriteLog('Set NetDir: ' + path, LogFileName);
    Session.NetFileDir:= Path;
  except
    on e: exception do
      WriteLog('Set NetDir error: ' + e.message, LogFileName);
  end;

  AliasProp:= TStringList.Create;
  try
    try
      Session.Active:= True;
      Session.GetAliasParams('Rasham', AliasProp);
      AliasPath:= AliasProp.Values['Path'];

      // validate db folder existing in bde admin session data
      i := LastDelimiter('\', AliasPath);
      if i = length(AliasPath) then
        Delete(AliasPath, i, 1);
      if Not DirectoryExists(AliasPath) then
      begin
        fn := 'wrong directory in BDE Administrator:' + AliasPath;
        WriteLog(fn, LogFileName);
        ShowMessage(fn);
        Application.Terminate;
      end;

      If AliasPath[Length(AliasPath)] <> '\' then
        AliasPath := AliasPath + '\';
    finally
      AliasProp.Free;
    end;
  except
    on e: exception do
      WriteLog(e.message, LogFileName);
  end;

  WriteLog('Delete LCK files, Path:' + AliasPath, LogFileName);
  try
    If FileExists(AliasPath + 'Paradox.lck') Then
      If DeleteFile(PChar(AliasPath + 'Paradox.lck')) Then
        WriteLog('Delete ' + AliasPath + ' Paradox.lck OK ', LogFileName)
      Else
        WriteLog('Delete ' + AliasPath + ' Paradox.lck Failed', LogFileName)
    else
      WriteLog(AliasPath + '\ Paradox.lck not found', LogFileName);

    If FileExists(AliasPath + 'Pdoxusrs.lck') Then
      If DeleteFile(PChar(AliasPath + 'Pdoxusrs.lck')) then
        WriteLog('Delete ' + AliasPath + ' Pdoxusrs.lck OK', LogFileName)
      else
        WriteLog('Delete ' + AliasPath + ' Pdoxusrs.lck Failed', LogFileName)
    else
      WriteLog(AliasPath + ' Pdoxusrs.lck not found', LogFileName);
  except
    WriteLog('Delete ' + AliasPath + ' LCK Files Failed', LogFileName);
  end;

  TSICheckAndLog.Instance.LogTo(AliasPath);
  //*************** End of NetDir
  UseSplash := Not ((ParamCount > 0) And (UpperCase(ParamStr(1)) = 'NF'));
  if UseSplash Then
  begin
    WriteLog('Create Splash', LogFileName);
    fmSplash := TfmSplash.Create(nil);
  end
  else
    WriteLog('Don''t load splash screen', LogFileName);
  try
    If UseSplash Then
    begin
      WriteLog('Show Splash', LogFileName);
      fmSplash.Show;
      fmSplash.Update;
    end;

    WriteLog('Set Title', LogFileName);                         Application.Title := '��� ����';
    WriteLog('Create Data module', LogFileName);                Application.CreateForm(TData, Data);
    WriteLog('Create fmMain', LogFileName);                     Application.CreateForm(TfmMain, fmMain);
    WriteLog('Apply DB fixes', LogFileName);
    if UseSplash then
      FixDB(True, fmSplash) //Tsahi 07/11/05 : Use this procedure to fix problems in DB as needed
    else
      FixDB(True, nil);     //Tsahi 07/11/05 : Use this procedure to fix problems in DB as needed
    WriteLog('Create fmMenu', LogFileName);                     Application.CreateForm(TfmMenu, fmMenu);
    WriteLog('Create fmCompanies', LogFileName);                Application.CreateForm(TfmCompanies, fmCompanies);
    WriteLog('Create fmDialog2', LogFileName);                  Application.CreateForm(TfmDialog2, fmDialog2);
    WriteLog('Create fmDialog', LogFileName);                   Application.CreateForm(TfmDialog, fmDialog);
    WriteLog('Create fmRisCompany', LogFileName);               Application.CreateForm(TfmRisCompany, fmRisCompany);
    WriteLog('Create fmDraftCompany', LogFileName);             Application.CreateForm(TfmDraftCompany, fmDraftCompany);
    WriteLog('Create fmLoadDialog', LogFileName);               Application.CreateForm(TfmLoadDialog, fmLoadDialog);
    WriteLog('Create fmSaveDialog',LogFileName);                Application.CreateForm(TfmSaveDialog, fmSaveDialog);
    WriteLog('Create fmShowCompany', LogFileName);              Application.CreateForm(TfmShowCompany, fmShowCompany);
    WriteLog('Create QRPrinterSetup', LogFileName);             Application.CreateForm(TQRPrinterSetup, QRPrinterSetup);
    WriteLog('Create fmPrinterSetup', LogFileName);             Application.CreateForm(TfmPrinterSetup, fmPrinterSetup);
    WriteLog('Create fmSelect', LogFileName);                   Application.CreateForm(TfmSelect, fmSelect);
    { WriteLog('Create fmMultipleDirectors', 'C:\RashLog.txt'); Application.CreateForm(TfmMultipleDirectorsChoice, fmMultipleDirectorsChoice);}
  finally
    If UseSplash Then
    begin
      WriteLog('Release Splash', LogFileName);
      fmSplash.Free;
    end;
  end;
  WriteLog('Application Run', LogFileName);
  try
    Application.Run;
  except
    on e: Exception do
    begin
      WriteLog(e.Message, LogFileName);//'C:\RashErrLog.txt');
      WriteLog(e.ClassName, LogFileName);// 'C:\RashErrLog.txt');
    end;
  end;
end.
