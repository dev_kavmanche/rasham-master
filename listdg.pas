unit listdg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, HListBox, HebForm, HLabel;

type
  TfmListDlg = class(TForm)
    btnClose: TBitBtn;
    Bevel1: TBevel;
    HebForm1: THebForm;
    lbItems: THebListBox;
    lblMessage: THebLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Execute(const ACaption,ANoMessage:string;sl:TStringList);
  end;

var
  fmListDlg: TfmListDlg;

implementation

{$R *.DFM}
procedure TfmListDlg.Execute(const ACaption,ANoMessage:string;sl:TStringList);
var
  i:integer;
  empty:boolean;
begin
  Caption:=ACaption;
  if sl=nil then
    empty:=True
  else
    empty:=(sl.Count<=0);
  lbItems.Visible:=not empty;
  lblMessage.Visible:=empty;
  if empty then
    lblMessage.Caption:=ANoMessage
  else
    begin
      lbItems.Clear;
      for i:=0 to sl.Count-1 do
        lbItems.Items.Add(sl.Strings[i]);
    end;
  ShowModal;  
end;

end.
