unit PrintDirectorBook1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, Db, DBTables, DataObjects;

type
  TfmQRDirBook1 = class(TForm)
    qrDirectors: TQuickRep;
    QRBand3: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    taDirectors: TTable;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    l1: TQRLabel;
    l2: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    l3: TQRLabel;
    l4: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    procedure QRDBText9Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    FCompany: TCompany;
    FNotifyDate, FDecisionDate: TDateTime;
    procedure PrintProc(DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintExecute(const FileName: String; Company: TCompany; NotifyDate, DecisionDate: TDateTime);
  end;

var
  fmQRDirBook1: TfmQRDirBook1;

implementation

{$R *.DFM}

uses Util, PrinterSetup;

procedure TfmQRDirBook1.PrintExecute(const FileName: String; Company: TCompany; NotifyDate, DecisionDate: TDateTime);
begin
  FCompany:= Company;
  FNotifyDate:= NotifyDate;
  FDecisionDate:= DecisionDate;
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQRDirBook1.PrintProc(DoPrint: Boolean);
begin
  l1.Caption:= FCompany.Name;
  l2.Caption:= FCompany.Zeut;
  l3.Caption:= DateToLongStr(FNotifyDate);
  l4.Caption:= DateToLongStr(FDecisionDate);
  taDirectors.Active:= True;
  fmPrinterSetup.FormatQR(qrDirectors);
  If DoPrint then
    qrDirectors.Print
  Else
    begin
      qrDirectors.Preview;
      Application.ProcessMessages;
    end;
end;

procedure TfmQRDirBook1.QRDBText9Print(sender: TObject; var Value: String);
begin
  if Trim(Value)='0' then
    Value:= '';
end;

end.
