unit PrintIncHon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls, Db, DBTables, DataObjects;

type
  TfmQRIncHon = class(TForm)
    qrHonTemplate: TQuickRep;
    QRBand1: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel45: TQRLabel;
    QRShape1: TQRShape;
    QRBand2: TQRBand;
    QRLabel7: TQRLabel;
    QRShape3: TQRShape;
    QRLabel8: TQRLabel;
    QRShape5: TQRShape;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape9: TQRShape;
    QRLabel14: TQRLabel;
    QRShape14: TQRShape;
    QRLabel18: TQRLabel;
    QRShape7: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    l1: TQRLabel;
    l2: TQRLabel;
    l3: TQRLabel;
    QRShape8: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape16: TQRShape;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRLabel26: TQRLabel;
    QRLabel25: TQRLabel;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape21: TQRShape;
    QRShape23: TQRShape;
    taHon: TTable;
    QRDBText1: TQRDBText;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRShape31: TQRShape;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand3: TQRBand;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    l4: TQRLabel;
    QRLabel32: TQRLabel;
    l5: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    l6: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel62: TQRLabel;
    l7: TQRLabel;
    l8: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel31: TQRLabel;
    QRImage4: TQRImage;
    QRLabel35: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRImage3: TQRImage;
    QRLabel43: TQRLabel;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    QRShape35: TQRShape;
    QRShape2: TQRShape;
    QRShape4: TQRShape;
    QRShape6: TQRShape;
    QRShape13: TQRShape;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel173: TQRLabel;
    QRShape15: TQRShape;
    QRShape22: TQRShape;
  private
    { Private declarations }

    procedure PrintProc(DoPrint: Boolean);
    procedure StartTable;
  public
    { Public declarations }
    procedure PrintExecute(const FileName: String; Company: TCompany; HonList: THonList; DecisionDate, NotifyDate: TDateTime);
    procedure PrintEmpty(const FileName: String);
  end;

var
  fmQRIncHon: TfmQRIncHon;

implementation

uses Util, PrinterSetup;

{$R *.DFM}

procedure TfmQRIncHon.PrintEmpty(const FileName: String);
var
  i: integer;
begin
  StartTable;
  for i := 1 to 8 do
  begin
    taHon.Append;
    taHon.Post;
  end;

  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQRIncHon.PrintExecute(const FileName: String; Company: TCompany; HonList: THonList; DecisionDate, NotifyDate: TDateTime);
var
  I: Integer;
begin
  StartTable;
  with taHon do
  begin
    for I := 0 to HonList.Count-1 do
      with HonList.Items[I] as THonItem do
      begin
        Append;
        FieldByName('Name').AsString := Name;
        FieldByName('Count').AsFloat := Rashum;
        FieldByName('Value').AsInteger := Trunc(Value);
        FieldByName('Value100').AsInteger := Trunc(Frac(Value)*100);
        FieldByName('AllValue').AsInteger := Trunc(Value*Rashum);
        FieldByName('AllValue100').AsInteger := Trunc(Frac(Value*Rashum)*100);
        Post;
      end;
  end;

  l1.Caption := Company.Name;
  l2.Caption := Company.Zeut;
  l3.Caption := DateToLongStr(DecisionDate);
  l4.Caption := Company.CompanyInfo.SigName;
  l5.Caption := Company.CompanyInfo.SigZeut;
  l6.Caption := Company.CompanyInfo.SigTafkid;
  l7.Caption := DateToLongStr(NotifyDate);

  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQRIncHon.StartTable;
begin
  taHon.FieldDefs.Clear;
  taHon.FieldDefs.Add('Name', ftString, 40, False);
  taHon.FieldDefs.Add('Count',ftFloat,0,False);
  taHon.FieldDefs.Add('Value100', ftInteger,0,False);
  taHon.FieldDefs.Add('Value', ftInteger,0,False);
  taHon.FieldDefs.Add('AllValue100', ftInteger,0,False);
  taHon.FieldDefs.Add('AllValue', ftInteger,0,False);
  taHon.CreateTable;
  taHon.Open;
end;

procedure TfmQRIncHon.PrintProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(qrHonTemplate);
  if DoPrint then
    qrHonTemplate.Print
  else
    qrHonTemplate.Preview;
end;

end.
