unit PrintStockBook1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Qrctrls, QuickRpt, ExtCtrls, DataObjects;

type
  TfmQRStockBook1 = class(TForm)
    qrStocks: TQuickRep;
    QRBand3: TQRBand;
    QRLabel9: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    l1: TQRLabel;
    l2: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    l3: TQRLabel;
    l4: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel15: TQRLabel;
    QRBand1: TQRBand;
    QRDBText9: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText10: TQRDBText;
    taStocks: TTable;
    QRLabel6: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel16: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel17: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel18: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    procedure QRDBText9Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    fCompany: TCompany;
    fNotifyDate, FDecisionDate: TDateTime;
    fRepData: TTable;
    procedure PrintProc(DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintExecute(aData: TTable; const aFileName: String; aCompany: TCompany; aNotifyDate, aDecisionDate: TDateTime);
  end;

var
  fmQRStockBook1: TfmQRStockBook1;

implementation

{$R *.DFM}

uses Util, PrinterSetup;

procedure TfmQRStockBook1.PrintExecute(aData: TTable; const aFileName: String; aCompany: TCompany; aNotifyDate, aDecisionDate: TDateTime);
var
  i: integer;
begin
  if assigned(aData) then
  begin
    fCompany := aCompany;
    fNotifyDate := aNotifyDate;
    fDecisionDate := aDecisionDate;
    fRepData := aData;
    for i := 0  to QRBand1.ControlCount - 1  do
      if QRBand1.Controls[i] is TQRDBText then
        (QRBand1.Controls[i] as TQRDBText).DataSet := aData;
    fmPrinterSetup.Execute(aFileName, PrintProc);
  end;
end;

procedure TfmQRStockBook1.PrintProc(DoPrint: Boolean);
begin
  l1.Caption := FCompany.Name;
  l2.Caption := FCompany.Zeut;
  l3.Caption := DateToLongStr(FDecisionDate);
  l4.Caption := DateToLongStr(FNotifyDate);
  qrStocks.DataSet := fRepData;
  qrStocks.DataSet.Active := True;
  fmPrinterSetup.FormatQR(qrStocks);
  If DoPrint then
    qrStocks.Print
  Else
  begin
    qrStocks.Preview;
    Application.ProcessMessages;
  end;
end;

procedure TfmQRStockBook1.QRDBText9Print(sender: TObject; var Value: String);
begin
  if Trim(Value) = '0' then
    Value := '';
end;

end.
