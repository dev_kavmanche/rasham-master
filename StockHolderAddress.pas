unit StockHolderAddress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HEdit, StdCtrls, HComboBx, HebForm, ExtCtrls, ComCtrls,
  Buttons, Mask, HLabel, DataObjects, HChkBox;

type
  TfmStockAddress = class(TfmBlankTemplate)
    Shape38: TShape;
    Shape39: TShape;
    Shape33: TShape;
    Shape42: TShape;
    Shape31: TShape;
    Shape40: TShape;
    Shape34: TShape;
    HebLabel17: THebLabel;
    Shape41: TShape;
    HebLabel21: THebLabel;
    Shape27: TShape;
    Shape35: TShape;
    Shape5: TShape;
    Shape6: TShape;
    HebLabel3: THebLabel;
    HebLabel18: THebLabel;
    Shape28: TShape;
    HebLabel15: THebLabel;
    HebLabel20: THebLabel;
    Shape32: TShape;
    HebLabel16: THebLabel;
    e36: TEdit;
    e38: THebComboBox;
    e37: THebComboBox;
    e34: THebEdit;
    e35: THebEdit;
    e32: THebComboBox;
    e33: THebComboBox;
    HebLabel41: THebLabel;
    Shape59: TShape;
    Shape7: TShape;
    Shape8: TShape;
    e39: THebEdit;
    cbChangeName: THebCheckBox;
    Shape9: TShape;
    Shape10: TShape;
    edZeut: THebEdit;
    cbZeut: THebCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure CriticalEditChange(Sender: TObject);
    procedure cbChangeNameClick(Sender: TObject);
    procedure cbZeutClick(Sender: TObject);
  private
    { Private declarations }
    CurrentZeut: string;
    ZeutChange: boolean;

    procedure FillReadOnlyFields;
    procedure DeleteEnum(ObjectNum, NewNum: Integer);
    procedure LoadEnum(ObjectNum, NewNum: Integer);

  protected
    function EvSaveData(Draft: Boolean): Boolean; override;
    function EvLoadData: Boolean; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvFillDefaultData; override;
    procedure EvDataLoaded; override;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmStockAddress: TfmStockAddress;

implementation

{$R *.DFM}
uses DataM, Util, cSTD, SaveDialog, LoadDialog, dialog2, dialog,
  MultipleDirectorsChoice;

var
  TempFileItem: TFileItem;

procedure TfmStockAddress.LoadEnum(ObjectNum, NewNum: Integer);
var
  StockHolder: TSTockHolder;
  StockAddress: TStockHolderAddress;
  i: Integer;
begin
  for i := 0 To Company.StockHolders.Count-1 do
  begin
    if Company.StockHolders.Items[I].RecNum = ObjectNum Then
    begin
      StockHolder := Company.StockHolders.Items[I] as TStockHolder;
      e33.ItemIndex := e33.Items.IndexOf(StockHolder.Zeut);
      e32.ItemIndex := e33.ItemIndex;
      StockAddress := TStockHolderAddress.Create(NewNum);
      e34.Text := StockAddress.Street;
      e35.Text := StockAddress.Number;
      e36.Text := StockAddress.Index;
      e37.Text := StockAddress.City;
      e38.Text := StockAddress.Country;
      e39.Text := e32.Text;
      edZeut.Text := e33.Text;
      cbZeut.Checked := False;
      cbChangeName.Checked := False;
      cbChangeNameClick(Self);
      StockAddress.Free;
    end;
  end;
end;

function TfmStockAddress.EvLoadData: Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  TmpStr: String;
begin
  Result := False;
  FileItem := fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faStockHolderInfo);
  if FileItem = nil then
    Exit;
  DecisionDate := (FileItem.FileInfo.DecisionDate);
  NotifyDate := (FileItem.FileInfo.NotifyDate);
  TempFileItem := FileItem;
  Company.StockHolders.Filter := afAll;
  FileItem.EnumChanges(ocStockHolder, -1, acAddress, LoadEnum);
  if FileItem.LoadNameChange(TmpStr) then
  begin
    cbChangeName.Checked := True;
    cbChangeNameClick(Self);
    e39.Text := TmpStr;
  end;
  FileItem.Free;
  FilesList.Free;
end;

function TfmStockAddress.EvSaveData(Draft: Boolean): Boolean;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  FileInfo: TFileInfo;
  StockHolder: TStockHolder;
  Address: TStockHolderAddress;
  Changes: TChanges;
  RecNum: Integer;
  i: integer;
  SelectedStockHolders: TMultipleSelectRec;
  LastChange : boolean;
  SaveDirectors,SaveManagers: TDialogResult;
begin
  Result := False;
  Address := TStockHolderAddress.CreateNew(e34.Text, e35.Text, e36.Text, e37.Text, e38.Text);
  FilesList := TFilesList.Create(Company.RecNum, faStockHolderInfo, Draft);
  FileInfo := TFileInfo.Create;
  FileInfo.Action := faStockHolderInfo;
  FileInfo.Draft := Draft;
  FileInfo.DecisionDate := DecisionDate;
  FileInfo.NotifyDate := NotifyDate;
  FileInfo.CompanyNum := Company.RecNum;

  try
    if (cbZeut.Checked) and ((Trim(edZeut.Text) = '') or (Trim(edZeut.Text) = '0')) then
    begin
      Application.MessageBox(PChar('�� ���� ����� ���� ���� ��� �� "0" .' + #10+#13 + '��� ��� ���.             '), '���� �������' ,MB_OK);
      Exit;
    end;

    FileItem := fmSaveDialog.Execute(Self, FilesList, FileInfo);
    if FileItem <> nil then
    begin
      StockHolder := Company.StockHolders.FindByZeut(e33.Text) as TStockHolder;

      FileItem.FileInfo.Draft:= True;
      Address.ExSaveData(FileItem, StockHolder, DecisionDate);

      if cbChangeName.Checked then
      begin
        RecNum := FileItem.SaveNameChange(e39.Text, acNewName, ocStockHolder);
        if (not Draft) and (Trim(e39.Text) <> '') and (e39.Text <> StockHolder.Name) then
        begin
          Changes:= TChanges.Create(Company);
          try
            Changes.SaveChanges(ocStockHolder, StockHolder.RecNum, acNewName, RecNum, DecisionDate, FileItem.FileInfo.RecNum);
          finally
            Changes.Free;
          end;
        end;
      end;

      if cbZeut.Checked then
      begin
        RecNum := FileItem.SaveNameChange(edZeut.Text, acNewZeut, ocStockHolder);
        if (not Draft) and (Trim(edZeut.Text) <>' ') and (edZeut.Text <> StockHolder.Zeut) then
        begin
          Changes:= TChanges.Create(Company);
          try
            Changes.SaveChanges(ocStockHolder, StockHolder.RecNum, acNewZeut, RecNum, DecisionDate, FileItem.FileInfo.RecNum);
          finally
            Changes.Free;
          end;
        end;
      end;

      if not Draft then
      begin
        // Moshe 14/10/2018
        //Application.CreateForm(TfmMultipleDirectorsChoice, fmMultipleDirectorsChoice);
        fmMultipleDirectorsChoice := TfmMultipleDirectorsChoice.Create(Self);
        fmMultipleDirectorsChoice.ShowFull(StockHolder.Zeut, '', StockHolder.RecNum, diStockHolder, Self.DecisionDate);
        if (fmMultipleDirectorsChoice.ModalResult = mrAbort) then
          Exit;

        //==>Tsahi: 21.6.05 : changed so as to ask two questions of user (multiple+stockholder) whenever appropriate
        if fmMultipleDirectorsChoice.ResultList.PersonCount(diDirector) > 0 then
        begin
          fmDialog.memo.Clear;
          fmDialog.memo.Lines.Add('');
          fmDialog.memo.Lines.Add('��� ����� �� ��� �� ������� �����.');
          fmDialog.memo.Lines.Add('��� ����� �� ����� �� ��������?');
          fmDialog.Button3.Visible := False;
          fmDialog.ShowModal;
          fmDialog.Button3.Visible := True;
        end;
        SaveDirectors := fmDialog.DialogResult;

        //==>Tsahi: 30.6.05 : changed so as to ask third questions of user (managers) whenever appropriate
        if (fmMultipleDirectorsChoice.ResultList.PersonCount(diManager) > 0) or (fmMultipleDirectorsChoice.ResultList.PersonCount(diSignName) > 0) then
        begin
          fmDialog.memo.Clear;
          fmDialog.memo.Lines.Add('');
          fmDialog.memo.Lines.Add('��� ����� �� ��� �� ���"� �� ���� ���� �����.');
          fmDialog.memo.Lines.Add('��� ����� �� ����� �� ����"�/���� ����?');
          fmDialog.Button3.Visible := False;
          fmDialog.ShowModal;
          fmDialog.Button3.Visible := True;
        end;
        SaveManagers := fmDialog.DialogResult;

        for i := 0 to fmMultipleDirectorsChoice.ResultList.Count-1 do
        begin
          if SaveDirectors = drNo then
              PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.DirectorNum := 0;
          if SaveManagers = drNo then
          begin
            PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.ManagerNum := 0;
            PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^.SignNameNum := 0;
          end;
        end;

        if ((fmMultipleDirectorsChoice.ModalResult <> mrCancel) or (fmDialog.DialogResult = drYes)) and
           (fmMultipleDirectorsChoice.ResultList.Count >= 1) then
        begin
          Screen.Cursor := crHourGlass;
          LastChange := False;
          for i := 0 to fmMultipleDirectorsChoice.ResultList.Count-1 do
          begin
            if i = fmMultipleDirectorsChoice.ResultList.Count-1 then
              LastChange := True;
            SelectedStockHolders := (PMultipleSelectRec(fmMultipleDirectorsChoice.ResultList.Items[i])^);
            StockHolder.SaveDataToAll(SelectedStockHolders, Address, e39.Text, edZeut.Text,
              cbChangeName.Checked, cbZeut.Checked, LastChange, True, FileInfo);
            ManagerChange(StockHolder.Zeut, cbChangeName.Checked, cbZeut.Checked, e39.Text, edZeut.Text, FileItem);
          end;
        end;
        Address.SaveData(StockHolder, DecisionDate, False, FileItem.FileInfo.RecNum);
        CurrentZeut := StockHolder.Zeut;
        ZeutChange := True;
      end;

      fmDialog2.Show;
      Result := True;
    end
  finally
    Screen.Cursor := crDefault;
    FilesList.Free;
    FileItem.Free;
    Address.Free;

    if fmMultipleDirectorsChoice <> nil then
    begin
      fmMultipleDirectorsChoice.Free;
      fmMultipleDirectorsChoice := nil;
    end;
  end;
end; // TfmStockAddress.EvSaveData

procedure TfmStockAddress.DeleteFile(FileItem: TFileItem);
begin
  TempFileItem := FileItem;
  if not FileItem.FileInfo.Draft then
    Raise Exception.Create('This file is not Draft at TfmStockAddress.DeleteFile');
  if (FileItem.FileInfo.Action <> faStockHolderInfo) then
    raise Exception.Create('Invalid file type at TfmStockAddress.DeleteFile');
  Company.StockHolders.Filter := afAll;
  FileItem.EnumChanges(ocStockHolder, -1, acAddress, DeleteEnum);
  FileItem.DeleteFile;
end;

procedure TfmStockAddress.DeleteEnum(ObjectNum, NewNum: Integer);
var
  I: Integer;
  StockHolderAddress: TStockHolderAddress;
begin
  For I:= 0 To Company.StockHolders.Count-1 do
    If Company.StockHolders.Items[I].RecNum = ObjectNum Then
      begin
        StockHolderAddress:= TStockHolderAddress.Create(NewNum);
        StockHolderAddress.FileDelete(Company.StockHolders.Items[I] as TStockHolder, TempFileItem);
        StockHolderAddress.Free;
      end;
end;

procedure TfmStockAddress.FillReadOnlyFields;
var
  StockHolder: TStockHolder;
begin
  Company.StockHolders.Filter:= afActive;
  StockHolder:= (Company.StockHolders.FindByZeut(e33.Text) as TStockHolder);
  cbChangeName.Checked:= False;
  cbChangeNameClick(Self);
  try
    e34.Text:= StockHolder.Address.Street;
    e35.Text:= StockHolder.Address.Number;
    e36.Text:= StockHolder.Address.Index;
    e37.Text:= StockHolder.Address.City;
    e38.Text:= StockHolder.Address.Country;
    e39.Text:= StockHolder.Name;
    edZeut.Text := STockHolder.Zeut;
    DataChanged:= False;
  except
  end;
end;

procedure TfmStockAddress.EvFocusNext(Sender: TObject);
begin
//  Inherited;
  if (Sender = e13) then e33.SetFocus;
  if (Sender = e33) then e32.SetFocus;
  if (Sender = e32) then e34.SetFocus;
  if (Sender = e34) then e35.SetFocus;
  if (Sender = e35) then e36.SetFocus;
  if (Sender = e36) then e37.SetFocus;
  if (Sender = e37) then e38.SetFocus;
  if (Sender = e38) then
    if (e39.Enabled) then e39.SetFocus
    else                  EvFocusNext(e39);
  if (Sender = e39) then
    if (edZeut.Enabled) then edZeut.SetFocus
    else                     e33.SetFocus;
end;

procedure TfmStockAddress.EvDataLoaded;
var
  I: Integer;
  Old33Text: String;
begin
  inherited;
  if ZeutChange then
    Old33Text := CurrentZeut
  else
    Old33Text := e33.Text;
  ZeutChange := False;

  e32.Items.Clear;
  e33.Items.Clear;
  If Not DoNotClear Then
    begin
      e32.Text:= '';
      e33.Text:= '';
    end;
  Company.StockHolders.Filter:= afActive;
  For I:= 0 To Company.StockHolders.Count-1 do
    begin
      e33.Items.Add(Company.StockHolders.Items[I].Zeut);
      e32.Items.Add(Company.StockHolders.Items[I].Name);
    end;
  If DoNotClear Then
    begin
      e33.ItemIndex:= e33.Items.IndexOf(Old33Text);
      e32.ItemIndex:= e33.ItemIndex;
    end;
  // FillReadOnlyFields;
end;

procedure TfmStockAddress.EvFillDefaultData;
begin
  inherited;
  DATA.taCity.First;
  While Not DATA.taCity.EOF do
    begin
      e37.Items.Add(DATA.taCity.Fields[0].Text);
      DATA.taCity.Next;
    end;

  DATA.taCountry.First;
  While Not DATA.taCountry.EOF do
    begin
      e38.Items.Add(DATA.taCountry.Fields[0].Text);
      DATA.taCountry.Next;
    end;
end;

procedure TfmStockAddress.FormCreate(Sender: TObject);
begin
  FileAction := faStockHolderInfo;
  ShowStyle := afActive;
  inherited;
end;

procedure TfmStockAddress.CriticalEditChange(Sender: TObject);
var
  OldValue: Boolean;
begin
  OldValue := Loading;
  inherited;
  Loading := True;
  if Sender = e33 then
  begin
    e32.ItemIndex := e33.ItemIndex;
    FillReadOnlyFields;
  end;
  if Sender = e32 then
  begin
    e33.ItemIndex := e32.ItemIndex;
    FillReadOnlyFields;
  end;
  Loading := OldValue;
end;

procedure TfmStockAddress.cbChangeNameClick(Sender: TObject);
begin
  e39.Enabled := cbChangeName.Checked;
end;

procedure TfmStockAddress.cbZeutClick(Sender: TObject);
begin
  inherited;
  edZeut.Enabled := cbZeut.Checked;
end;

end.
