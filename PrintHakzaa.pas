unit PrintHakzaa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, DataObjects;

type
  TfmQRHakzaa = class(TForm)
    qrHakzaa1Old: TQuickRep;
    QRBand1: TQRBand;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape3: TQRShape;
    QRLabel8: TQRLabel;
    l1: TQRLabel;
    l3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape19: TQRShape;
    QRLabel27: TQRLabel;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRLabel28: TQRLabel;
    QRShape23: TQRShape;
    QRLabel29: TQRLabel;
    QRLabel40: TQRLabel;
    l2: TQRLabel;
    QRBand2: TQRBand;
    QRShape4: TQRShape;
    QRShape2: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel9: TQRLabel;
    QRShape7: TQRShape;
    QRLabel11: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape10: TQRShape;
    QRLabel14: TQRLabel;
    QRShape11: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape12: TQRShape;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape18: TQRShape;
    QRShape20: TQRShape;
    QRLabel45: TQRLabel;
    QRShape24: TQRShape;
    QRLabel23: TQRLabel;
    QRShape25: TQRShape;
    QRLabel47: TQRLabel;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRShape26: TQRShape;
    QRLabel24: TQRLabel;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRLabel25: TQRLabel;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRLabel26: TQRLabel;
    QRLabel39: TQRLabel;
    QRShape32: TQRShape;
    QRLabel42: TQRLabel;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape38: TQRShape;
    QRLabel44: TQRLabel;
    QRShape39: TQRShape;
    QRShape40: TQRShape;
    QRLabel41: TQRLabel;
    QRShape41: TQRShape;
    QRLabel43: TQRLabel;
    QRShape42: TQRShape;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
    QRShape48: TQRShape;
    QRLabel50: TQRLabel;
    QRShape49: TQRShape;
    QRShape50: TQRShape;
    QRShape51: TQRShape;
    QRShape53: TQRShape;
    QRLabel51: TQRLabel;
    QRShape54: TQRShape;
    QRShape55: TQRShape;
    QRShape56: TQRShape;
    QRShape58: TQRShape;
    QRShape59: TQRShape;
    QRShape60: TQRShape;
    QRShape61: TQRShape;
    QRShape43: TQRShape;
    QRLabel49: TQRLabel;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel55: TQRLabel;
    l49: TQRLabel;
    l50: TQRLabel;
    l51: TQRLabel;
    l52: TQRLabel;
    l53: TQRLabel;
    l54: TQRLabel;
    l55: TQRLabel;
    l56: TQRLabel;
    l57: TQRLabel;
    l28: TQRLabel;
    l29: TQRLabel;
    l30: TQRLabel;
    l31: TQRLabel;
    l34: TQRLabel;
    l35: TQRLabel;
    l36: TQRLabel;
    l37: TQRLabel;
    l38: TQRLabel;
    l39: TQRLabel;
    l40: TQRLabel;
    l41: TQRLabel;
    l42: TQRLabel;
    l43: TQRLabel;
    l44: TQRLabel;
    l45: TQRLabel;
    l46: TQRLabel;
    l47: TQRLabel;
    l48: TQRLabel;
    l4: TQRLabel;
    l5: TQRLabel;
    l6: TQRLabel;
    l32: TQRLabel;
    l33: TQRLabel;
    l10: TQRLabel;
    l11: TQRLabel;
    l12: TQRLabel;
    l13: TQRLabel;
    l14: TQRLabel;
    l15: TQRLabel;
    l16: TQRLabel;
    l17: TQRLabel;
    l18: TQRLabel;
    l19: TQRLabel;
    l20: TQRLabel;
    l21: TQRLabel;
    l22: TQRLabel;
    l23: TQRLabel;
    l24: TQRLabel;
    l25: TQRLabel;
    l26: TQRLabel;
    l27: TQRLabel;
    imV: TImage;
    QRShape47: TQRShape;
    QRShape52: TQRShape;
    QRShape57: TQRShape;
    QRShape62: TQRShape;
    l7: TQRLabel;
    l8: TQRLabel;
    l9: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel79: TQRLabel;
    qrHakzaa2: TQuickRep;
    QRBand11: TQRBand;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel233: TQRLabel;
    lLawyer: TQRLabel;
    lFullName: TQRLabel;
    QRLabel236: TQRLabel;
    lIDNum: TQRLabel;
    QRLabel238: TQRLabel;
    QRLabel239: TQRLabel;
    QRLabel240: TQRLabel;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    lLawyer2: TQRLabel;
    QRLabel248: TQRLabel;
    lLawyerLicense: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel246: TQRLabel;
    lLawyerAddress: TQRLabel;
    lLawyerID: TQRLabel;
    DetailBand1: TQRBand;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel66: TQRLabel;
    l58: TQRLabel;
    l59: TQRLabel;
    l60: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    l61: TQRLabel;
    l62: TQRLabel;
    l63: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    l64: TQRLabel;
    QRLabel32: TQRLabel;
    l65: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    l66: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel62: TQRLabel;
    l67: TQRLabel;
    l68: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel63: TQRLabel;
    cb1: TQRImage;
    QRLabel85: TQRLabel;
    QRShape64: TQRShape;
    QRShape65: TQRShape;
    lPhone: TQRLabel;
    QRLabel83: TQRLabel;
    QRShape13: TQRShape;
    QRShape63: TQRShape;
    lAddress2: TQRLabel;
    lAddress1: TQRLabel;
    qrHakzaa1: TQuickRep;
    QRBand3: TQRBand;
    QRLabel84: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRImage2: TQRImage;
    QRImage1: TQRImage;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRShape66: TQRShape;
    QRShape67: TQRShape;
    QRLabel88: TQRLabel;
    edtCompanyName: TQRLabel;
    QRShape68: TQRShape;
    QRShape69: TQRShape;
    QRLabel89: TQRLabel;
    QRShape70: TQRShape;
    QRShape71: TQRShape;
    QRLabel90: TQRLabel;
    QRShape72: TQRShape;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    edtCompanyDate: TQRLabel;
    QRShape73: TQRShape;
    QRLabel93: TQRLabel;
    QRShape74: TQRShape;
    edtCompanyZeut: TQRLabel;
    QRShape75: TQRShape;
    QRShape76: TQRShape;
    edtCompanyPhone: TQRLabel;
    QRLabel98: TQRLabel;
    QRShape77: TQRShape;
    QRShape78: TQRShape;
    edtCompanyCity: TQRLabel;
    QRLabel100: TQRLabel;
    edtCompanyStreet: TQRLabel;
    QRShape79: TQRShape;
    QRShape81: TQRShape;
    QRLabel94: TQRLabel;
    QRLabel99: TQRLabel;
    QRShape82: TQRShape;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRShape84: TQRShape;
    QRLabel103: TQRLabel;
    QRShape85: TQRShape;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    QRShape86: TQRShape;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRShape87: TQRShape;
    QRShape88: TQRShape;
    QRShape89: TQRShape;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRShape90: TQRShape;
    QRShape91: TQRShape;
    edtHonName1: TQRLabel;
    edtHonName2: TQRLabel;
    edtHonName3: TQRLabel;
    edtHonValueSh1: TQRLabel;
    edtHonValueSh2: TQRLabel;
    edtHonValueSh3: TQRLabel;
    edtHonMokza1: TQRLabel;
    edtHonMokza2: TQRLabel;
    edtHonMokza3: TQRLabel;
    edtHonCash1: TQRLabel;
    edtHonCash2: TQRLabel;
    edtHonCash3: TQRLabel;
    edtHonNone1: TQRLabel;
    edtHonNone2: TQRLabel;
    edtHonNone3: TQRLabel;
    edtHonPart1: TQRLabel;
    edtHonPart2: TQRLabel;
    edtHonPart3: TQRLabel;
    edtHonErechSh1: TQRLabel;
    edtHonErechSh2: TQRLabel;
    edtHonErechSh3: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRShape92: TQRShape;
    QRLabel136: TQRLabel;
    QRShape80: TQRShape;
    QRShape83: TQRShape;
    QRShape93: TQRShape;
    edtHonValueAg1: TQRLabel;
    edtHonValueAg2: TQRLabel;
    edtHonValueAg3: TQRLabel;
    QRShape94: TQRShape;
    QRLabel140: TQRLabel;
    edtHonErechAg1: TQRLabel;
    edtHonErechAg2: TQRLabel;
    edtHonErechAg3: TQRLabel;
    QRShape95: TQRShape;
    QRLabel113: TQRLabel;
    QRShape97: TQRShape;
    QRLabel114: TQRLabel;
    QRShape98: TQRShape;
    QRLabel115: TQRLabel;
    QRShape101: TQRShape;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRShape105: TQRShape;
    QRLabel120: TQRLabel;
    QRShape106: TQRShape;
    QRShape107: TQRShape;
    QRShape109: TQRShape;
    QRLabel121: TQRLabel;
    QRShape110: TQRShape;
    QRShape111: TQRShape;
    QRLabel122: TQRLabel;
    QRShape112: TQRShape;
    QRLabel123: TQRLabel;
    QRShape113: TQRShape;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    edtHolderFirst1: TQRLabel;
    edtHolderZeut1: TQRLabel;
    edtHolderCountry1: TQRLabel;
    edtHolderStreet1: TQRLabel;
    edtHolderNumber1: TQRLabel;
    edtHolderZip1: TQRLabel;
    QRShape96: TQRShape;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    edtHolderLast1: TQRLabel;
    edtHolderZip2: TQRLabel;
    edtHolderNumber2: TQRLabel;
    edtHolderStreet2: TQRLabel;
    edtHolderCity2: TQRLabel;
    edtHolderCountry2: TQRLabel;
    edtHolderZeut2: TQRLabel;
    edtHolderFirst2: TQRLabel;
    edtHolderLast2: TQRLabel;
    QRShape99: TQRShape;
    edtHolderCountry3: TQRLabel;
    edtHolderCity3: TQRLabel;
    edtHolderStreet3: TQRLabel;
    edtHolderNumber3: TQRLabel;
    edtHolderZip3: TQRLabel;
    edtHolderZeut3: TQRLabel;
    edtHolderFirst3: TQRLabel;
    edtHolderLast3: TQRLabel;
    QRShape100: TQRShape;
    edtHolderCountry4: TQRLabel;
    edtHolderCity4: TQRLabel;
    edtHolderStreet4: TQRLabel;
    edtHolderNumber4: TQRLabel;
    edtHolderZip4: TQRLabel;
    edtHolderZeut4: TQRLabel;
    edtHolderFirst4: TQRLabel;
    edtHolderLast4: TQRLabel;
    QRShape102: TQRShape;
    edtHolderCountry5: TQRLabel;
    edtHolderCity5: TQRLabel;
    edtHolderStreet5: TQRLabel;
    edtHolderNumber5: TQRLabel;
    edtHolderZip5: TQRLabel;
    edtHolderZeut5: TQRLabel;
    edtHolderFirst5: TQRLabel;
    edtHolderLast5: TQRLabel;
    edtHolderCity1: TQRLabel;
    QRShape103: TQRShape;
    QRLabel126: TQRLabel;
    QRShape104: TQRShape;
    QRShape108: TQRShape;
    QRShape114: TQRShape;
    QRShape115: TQRShape;
    QRLabel127: TQRLabel;
    QRShape116: TQRShape;
    QRShape117: TQRShape;
    QRShape118: TQRShape;
    QRShape119: TQRShape;
    QRShape120: TQRShape;
    QRShape121: TQRShape;
    QRShape122: TQRShape;
    QRShape123: TQRShape;
    QRLabel128: TQRLabel;
    QRShape124: TQRShape;
    QRShape125: TQRShape;
    QRShape126: TQRShape;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    edtStocksValueSh1: TQRLabel;
    edtStocksValueSh2: TQRLabel;
    edtStocksValueSh3: TQRLabel;
    edtStocksCount1: TQRLabel;
    edtStocksCount2: TQRLabel;
    edtStocksCount3: TQRLabel;
    edtStocksPaidSh1: TQRLabel;
    edtStocksPaidSh2: TQRLabel;
    edtStocksPaidSh3: TQRLabel;
    QRLabel146: TQRLabel;
    QRShape127: TQRShape;
    QRShape128: TQRShape;
    QRShape129: TQRShape;
    QRShape130: TQRShape;
    edtStocksValueAg1: TQRLabel;
    edtStocksValueAg2: TQRLabel;
    QRShape131: TQRShape;
    QRShape132: TQRShape;
    edtStocksValueAg3: TQRLabel;
    edtStocksValueSh4: TQRLabel;
    edtStocksValueAg4: TQRLabel;
    edtStocksValueSh5: TQRLabel;
    edtStocksValueAg5: TQRLabel;
    QRLabel156: TQRLabel;
    QRShape133: TQRShape;
    QRShape134: TQRShape;
    edtStocksCount4: TQRLabel;
    edtStocksCount5: TQRLabel;
    QRShape135: TQRShape;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    QRShape136: TQRShape;
    QRShape137: TQRShape;
    edtStocksPaidSh4: TQRLabel;
    edtStocksPaidSh5: TQRLabel;
    edtStocksPaidAg5: TQRLabel;
    edtStocksPaidAg1: TQRLabel;
    edtStocksPaidAg2: TQRLabel;
    edtStocksPaidAg3: TQRLabel;
    edtStocksPaidAg4: TQRLabel;
    QRShape138: TQRShape;
    QRLabel133: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRShape139: TQRShape;
    QRLabel137: TQRLabel;
    QRBand4: TQRBand;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRShape140: TQRShape;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRImage3: TQRImage;
    QRImage4: TQRImage;
    QRBand5: TQRBand;
    QRLabel153: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRShape141: TQRShape;
    QRLabel157: TQRLabel;
    QRLabel158: TQRLabel;
    QRShape142: TQRShape;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel38: TQRLabel;
  private
    { Private declarations }
    FHonList: THonList;
    FStockHolders: TStockHolders;
    FCompany: TCompany;
    FDecisionDate, FNotifyDate: TDateTime;
    FDoPrint: Boolean;
    LastHon,
    LastStockHolder,
    LastStock: Integer;
    PrintRep: TQuickRep;
    procedure PrintNextPage1;
    procedure PrintNextPage2;
    procedure PrintProc(DoPrint: Boolean);
    procedure PrintEmptyProc(DoPrint: Boolean);
    procedure PrintGenericProc (DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintEmpty(const FileName: String);
    procedure PrintExecute(const FileName: String; Company: TCompany; HonList: THonList; StockHolders: TStockHolders; DecisionDate, NotifyDate: TDateTime);
  end;

var
  fmQRHakzaa: TfmQRHakzaa;

implementation

uses Util, PrinterSetup;

{$R *.DFM}

procedure TfmQRHakzaa.PrintEmptyProc( DoPrint: Boolean);
begin
  PrintRep := qrHakzaa1;
  PrintGenericProc(DoPrint);
  PrintRep := qrHakzaa2;
  PrintGenericProc(DoPrint);
end;

procedure TfmQRHakzaa.PrintEmpty(const FileName: String);
begin
  fmPrinterSetup.Execute(FileName, PrintEmptyProc);
end;

procedure TfmQRHakzaa.PrintExecute(const FileName: String; Company: TCompany; HonList: THonList; StockHolders: TStockHolders; DecisionDate, NotifyDate: TDateTime);
begin
  FCompany := Company;
  FHonList := HonList;
  FStockHolders := StockHolders;
  FNotifyDate := NotifyDate;
  FDecisionDate := DecisionDate;
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

procedure TfmQRHakzaa.PrintProc(DoPrint: Boolean);
begin
  FDoPrint := DoPrint;
  LastHon := 0;
  LastStockHolder := 0;
  LastStock := 0;
  PrintNextPage1;
  PrintNextPage2;
end;

procedure TfmQRHakzaa.PrintNextPage1;
var
  i: integer;
  HonItem: THonItem;
  VS, VSh, VAg: string;
  ES, ESh, EAg: string;
  Holder: TStockHolder;
  FirstName, LastName: string;
  Stock: TStock;
begin
  if ((LastHon >= FHonList.Count) and (LastStockHolder>= FStockHolders.Count)) then
    Exit;

  edtCompanyName.Caption  := FCompany.Name;
  edtCompanyDate.Caption  := DateToLongStr(FDecisionDate);
  edtCompanyZeut.Caption  := FCompany.Zeut;
  edtCompanyStreet.Caption:= FCompany.Address.Street + ' ' + FCompany.Address.Number;
  edtCompanyCity.Caption  := FCompany.Address.City + ' ' + FCompany.Address.Index;
  edtCompanyPhone.Caption := FCompany.Address.Phone;

  // Clear Hon controls
  for i := 0 to Self.ControlCount-1 do
  begin
    if ((Self.Controls[i].ClassName = 'TQRLabel') and
        (UpperCase(Copy(Self.Controls[i].Name, 1, 6)) = 'EDTHON')) then
    TQRLabel(Self.Controls[i]).Caption := '';
  end;

  // Fill HonList
  // Line 1
  if (LastHon < FHonList.Count) then
  try
    HonItem := FHonList.Items[LastHon] as THonItem;
    VS := FloatToStrF(HonItem.Value, ffNumber, 15, 4);
    SplitBySep(VS, '.', VSh, VAg);
    ES := FloatToStrF(HonItem.PartValue, ffNumber, 15, 2);
    SplitBySep(VS, '.', ESh, EAg);

    edtHonName1.Caption     := HonItem.Name;
    edtHonValueSh1.Caption  := VSh;
    edtHonValueAg1.Caption  := VAg;
    edtHonMokza1.Caption    := FloatToStrF(HonItem.Mokza, ffNumber, 15,2);
    edtHonCash1.Caption     := FloatToStrF(HonItem.Cash, ffNumber, 15,2);
    edtHonNone1.Caption     := FloatToStrF(HonItem.NonCash, ffNumber, 15,2);
    edtHonPart1.Caption     := FloatToStrF(HonItem.Part, ffNumber, 15, 2);
    edtHonErechSh1.Caption  := ESh;
    edtHonErechAg1.Caption  := EAg;

    Inc(LastHon);
  except
  end;

  // Line 2
  if (LastHon < FHonList.Count) then
  try
    HonItem := FHonList.Items[LastHon] as THonItem;
    VS := FloatToStrF(HonItem.Value, ffNumber, 15, 2);
    SplitBySep(VS, '.', VSh, VAg);
    ES := FloatToStrF(HonItem.PartValue, ffNumber, 15, 2);
    SplitBySep(VS, '.', ESh, EAg);

    edtHonName2.Caption     := HonItem.Name;
    edtHonValueSh2.Caption  := VSh;
    edtHonValueAg2.Caption  := VAg;
    edtHonMokza2.Caption    := FloatToStrF(HonItem.Mokza, ffNumber, 15,2);
    edtHonCash2.Caption     := FloatToStrF(HonItem.Cash, ffNumber, 15,2);
    edtHonNone2.Caption     := FloatToStrF(HonItem.NonCash, ffNumber, 15,2);
    edtHonPart2.Caption     := FloatToStrF(HonItem.Part, ffNumber, 15, 2);
    edtHonErechSh2.Caption  := ESh;
    edtHonErechAg2.Caption  := EAg;

    Inc(LastHon);
  except
  end;

  // Line 3
  if (LastHon < FHonList.Count) then
  try
    HonItem := FHonList.Items[LastHon] as THonItem;
    VS := FloatToStrF(HonItem.Value, ffNumber, 15, 2);
    SplitBySep(VS, '.', VSh, VAg);
    ES := FloatToStrF(HonItem.PartValue, ffNumber, 15, 2);
    SplitBySep(VS, '.', ESh, EAg);

    edtHonName3.Caption     := HonItem.Name;
    edtHonValueSh3.Caption  := VSh;
    edtHonValueAg3.Caption  := VAg;
    edtHonMokza3.Caption    := FloatToStrF(HonItem.Mokza, ffNumber, 15,2);
    edtHonCash3.Caption     := FloatToStrF(HonItem.Cash, ffNumber, 15,2);
    edtHonNone3.Caption     := FloatToStrF(HonItem.NonCash, ffNumber, 15,2);
    edtHonPart3.Caption     := FloatToStrF(HonItem.Part, ffNumber, 15, 2);
    edtHonErechSh3.Caption  := ESh;
    edtHonErechAg3.Caption  := EAg;
  except
  end;
  Inc(LastHon);

  // Clear Holder controls
  for i := 0 to Self.ControlCount-1 do
  begin
    if ((Self.Controls[i].ClassName = 'TQRLabel') and
        (UpperCase(Copy(Self.Controls[i].Name, 1, 9)) = 'EDTHOLDER')) then
      TQRLabel(Self.Controls[i]).Caption := '';
  end;

  // Clear Stocks controls
  for i := 0 to Self.ControlCount-1 do
  begin
    if ((Self.Controls[i].ClassName = 'TQRLabel') and
        (UpperCase(Copy(Self.Controls[i].Name, 1, 9)) = 'EDTSTOCKS')) then
      TQRLabel(Self.Controls[i]).Caption := '';
  end;

  // Fill StockHolders
  // Line 1
  if (LastStockHolder < FStockHolders.Count) then
  try
    Holder := FStockHolders.Items[LastStockHolder] as TStockHolder;
    SplitName(Holder.Name, FirstName, LastName);

    edtHolderFirst1.Caption   := FirstName;
    edtHolderLast1.Caption    := LastName;
    edtHolderZeut1.Caption    := Holder.Zeut;
    edtHolderCountry1.Caption := Holder.Address.Country;
    edtHolderCity1.Caption    := Holder.Address.City;
    edtHolderStreet1.Caption  := Holder.Address.Street;
    edtHolderNumber1.Caption  := Holder.Address.Number;
    edtHolderZip1.Caption     := Holder.Address.Index;

    if (LastStock < Holder.Stocks.Count) then
    begin
      Stock := Holder.Stocks.Items[LastStock] as TStock;
      VS := FloatToStrF(Stock.Value, ffNumber, 15, 4);
      SplitBySep(VS, '.', VSh, VAg);
      ES := FloatToStrF(Stock.Paid, ffNumber, 15, 4);
      SplitBySep(VS, '.', ESh, EAg);

      edtStocksValueSh1.Caption := VSh;
      edtStocksValueAg1.Caption := VAg;
      edtStocksCount1.Caption   := FloatToStrF(Stock.Count, ffNumber, 15, 2);
      edtStocksPaidSh1.Caption  := ESh;
      edtStocksPaidAg1.Caption  := EAg;

      Inc(LastStock);
      if (LastStock >= Holder.Stocks.Count) then
      begin
        LastStock := 0;
        Inc(LastStockHolder);
      end;
    end;
    if (Holder.Stocks.Count = 0) then
      Inc(LastStockHolder);
  except
  end;

  // Line 2
  if (LastStockHolder < FStockHolders.Count) then
  try
    Holder := FStockHolders.Items[LastStockHolder] as TStockHolder;
    SplitName(Holder.Name, FirstName, LastName);

    edtHolderFirst2.Caption   := FirstName;
    edtHolderLast2.Caption    := LastName;
    edtHolderZeut2.Caption    := Holder.Zeut;
    edtHolderCountry2.Caption := Holder.Address.Country;
    edtHolderCity2.Caption    := Holder.Address.City;
    edtHolderStreet2.Caption  := Holder.Address.Street;
    edtHolderNumber2.Caption  := Holder.Address.Number;
    edtHolderZip2.Caption     := Holder.Address.Index;

    if (LastStock < Holder.Stocks.Count) then
    begin
      Stock := Holder.Stocks.Items[LastStock] as TStock;
      VS := FloatToStrF(Stock.Value, ffNumber, 15, 4);
      SplitBySep(VS, '.', VSh, VAg);
      ES := FloatToStrF(Stock.Paid, ffNumber, 15, 4);
      SplitBySep(VS, '.', ESh, EAg);

      edtStocksValueSh2.Caption := VSh;
      edtStocksValueAg2.Caption := VAg;
      edtStocksCount2.Caption   := FloatToStrF(Stock.Count, ffNumber, 15, 2);
      edtStocksPaidSh2.Caption  := ESh;
      edtStocksPaidAg2.Caption  := EAg;

      Inc(LastStock);
      if (LastStock >= Holder.Stocks.Count) then
      begin
        LastStock := 0;
        Inc(LastStockHolder);
      end;
    end;
  except
  end;

  // Line 3
  if (LastStockHolder < FStockHolders.Count) then
  try
    Holder := FStockHolders.Items[LastStockHolder] as TStockHolder;
    SplitName(Holder.Name, FirstName, LastName);

    edtHolderFirst3.Caption   := FirstName;
    edtHolderLast3.Caption    := LastName;
    edtHolderZeut3.Caption    := Holder.Zeut;
    edtHolderCountry3.Caption := Holder.Address.Country;
    edtHolderCity3.Caption    := Holder.Address.City;
    edtHolderStreet3.Caption  := Holder.Address.Street;
    edtHolderNumber3.Caption  := Holder.Address.Number;
    edtHolderZip3.Caption     := Holder.Address.Index;

    if (LastStock < Holder.Stocks.Count) then
    begin
      Stock := Holder.Stocks.Items[LastStock] as TStock;
      VS := FloatToStrF(Stock.Value, ffNumber, 15, 4);
      SplitBySep(VS, '.', VSh, VAg);
      ES := FloatToStrF(Stock.Paid, ffNumber, 15, 4);
      SplitBySep(VS, '.', ESh, EAg);

      edtStocksValueSh3.Caption := VSh;
      edtStocksValueAg3.Caption := VAg;
      edtStocksCount3.Caption   := FloatToStrF(Stock.Count, ffNumber, 15, 2);
      edtStocksPaidSh3.Caption  := ESh;
      edtStocksPaidAg3.Caption  := EAg;

      Inc(LastStock);
      if (LastStock >= Holder.Stocks.Count) then
      begin
        LastStock := 0;
        Inc(LastStockHolder);
      end;
    end;
  except
  end;

  // Line 4
  if (LastStockHolder < FStockHolders.Count) then
  try
    Holder := FStockHolders.Items[LastStockHolder] as TStockHolder;
    SplitName(Holder.Name, FirstName, LastName);

    edtHolderFirst4.Caption   := FirstName;
    edtHolderLast4.Caption    := LastName;
    edtHolderZeut4.Caption    := Holder.Zeut;
    edtHolderCountry4.Caption := Holder.Address.Country;
    edtHolderCity4.Caption    := Holder.Address.City;
    edtHolderStreet4.Caption  := Holder.Address.Street;
    edtHolderNumber4.Caption  := Holder.Address.Number;
    edtHolderZip4.Caption     := Holder.Address.Index;

    if (LastStock < Holder.Stocks.Count) then
    begin
      Stock := Holder.Stocks.Items[LastStock] as TStock;
      VS := FloatToStrF(Stock.Value, ffNumber, 15, 4);
      SplitBySep(VS, '.', VSh, VAg);
      ES := FloatToStrF(Stock.Paid, ffNumber, 15, 4);
      SplitBySep(VS, '.', ESh, EAg);

      edtStocksValueSh4.Caption := VSh;
      edtStocksValueAg4.Caption := VAg;
      edtStocksCount4.Caption   := FloatToStrF(Stock.Count, ffNumber, 15, 2);
      edtStocksPaidSh4.Caption  := ESh;
      edtStocksPaidAg4.Caption  := EAg;

      Inc(LastStock);
      if (LastStock >= Holder.Stocks.Count) then
      begin
        LastStock := 0;
        Inc(LastStockHolder);
      end;
    end;
  except
  end;

  // Line 2
  if (LastStockHolder < FStockHolders.Count) then
  try
    Holder := FStockHolders.Items[LastStockHolder] as TStockHolder;
    SplitName(Holder.Name, FirstName, LastName);

    edtHolderFirst5.Caption   := FirstName;
    edtHolderLast5.Caption    := LastName;
    edtHolderZeut5.Caption    := Holder.Zeut;
    edtHolderCountry5.Caption := Holder.Address.Country;
    edtHolderCity5.Caption    := Holder.Address.City;
    edtHolderStreet5.Caption  := Holder.Address.Street;
    edtHolderNumber5.Caption  := Holder.Address.Number;
    edtHolderZip5.Caption     := Holder.Address.Index;

    if (LastStock < Holder.Stocks.Count) then
    begin
      Stock := Holder.Stocks.Items[LastStock] as TStock;
      VS := FloatToStrF(Stock.Value, ffNumber, 15, 4);
      SplitBySep(VS, '.', VSh, VAg);
      ES := FloatToStrF(Stock.Paid, ffNumber, 15, 4);
      SplitBySep(VS, '.', ESh, EAg);

      edtStocksValueSh5.Caption := VSh;
      edtStocksValueAg5.Caption := VAg;
      edtStocksCount5.Caption   := FloatToStrF(Stock.Count, ffNumber, 15, 2);
      edtStocksPaidSh5.Caption  := ESh;
      edtStocksPaidAg5.Caption  := EAg;

      Inc(LastStock);
      if (LastStock >= Holder.Stocks.Count) then
      begin
        LastStock := 0;
        Inc(LastStockHolder);
      end;
    end;
  except
  end;

  PrintRep := qrHakzaa1;
  PrintGenericProc(FDoPrint);

  PrintNextPage1;
end; // TfmQRHakzaa.PrintNextPage1

procedure TfmQRHakzaa.PrintNextPage2;
begin
  //==>14.6.05 Tsahi: moved to this page
  l64.Caption:= FCompany.CompanyInfo.SigName;
  l65.Caption:= FCompany.CompanyInfo.SigZeut;
  l66.Caption:= FCompany.CompanyInfo.SigTafkid;
  l67.Caption:= DateToLongStr(FNotifyDate);
  //<==
  //==>14.6.05 Tsahi
  lLawyer.Caption:= FCompany.CompanyInfo.LawName;
  lLawyer2.Caption:= lLawyer.Caption;
  lFullName.Caption:= FCompany.CompanyInfo.SigName;
  lIDNum.Caption:= FCompany.CompanyInfo.SigZeut ;
  lLawyerAddress.Caption:= FCompany.CompanyInfo.LawAddress ;
  lLawyerID.Caption:= FCompany.CompanyInfo.LawZeut ;
  lLawyerLicense.Caption:= FCompany.CompanyInfo.LawLicence ;

  if lLawyerID.Caption='0' then
    lLawyerID.Caption:='';
  if lIDNum.Caption='0' then
    lIDNum.Caption:='';
  if lLawyerLicense.Caption='0' then
    lLawyerLicense.Caption:='';
  //<==
  PrintRep := qrHakzaa2;
  PrintGenericProc(FDoPrint);
end;
//==============================================================================
procedure TfmQRHakzaa.PrintGenericProc (DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(PrintRep);//qrHakzaa);
  if DoPrint then
    PrintRep.Print //qrHakzaa.Print
  Else
  begin
    PrintRep.Preview;// qrHakzaa.Preview;
    Application.ProcessMessages;
  end;
end;
//==============================================================================
end.
