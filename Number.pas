unit Number;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HEdit, StdCtrls, HebForm, ExtCtrls, ComCtrls, Buttons,
  HLabel, DataObjects, Mask, HComboBx;

type
  TfmNumber = class(TfmTemplate)
    Shape51: TShape;
    Shape43: TShape;
    Shape45: TShape;
    HebLabel23: THebLabel;
    Shape60: TShape;
    HebLabel27: THebLabel;
    HebLabel1: THebLabel;
    Shape1: TShape;
    HebLabel2: THebLabel;
    HebLabel46: THebLabel;
    Shape2: TShape;
    lName: THebLabel;
    e13: TEdit;
    btBrowse: TButton;
    Shape5a: TShape;
    Shape6a: TShape;
    HebLabel4a: THebLabel;
    e12a: TMaskEdit;
    ShapeEngName51: TShape;
    ShapeFinalEng60: TShape;
    HebLabelFinalEng27: THebLabel;
    ShapeEng1: TShape;
    lblEngNameLabel: THebLabel;
    ShapeEng2: TShape;
    lEngName: THebLabel;
    edEngName: THebEdit;
    e12: THebComboBox;
    procedure btBrowseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    Company: TCompany;
    procedure EvFillData(Company: TCompany);
    procedure EvFocusNext(Sender: TObject); override;
    function EvSaveData(Draft: Boolean): boolean; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
  public
    { Public declarations }
    class procedure EvUndoFile(FileItem: TFileItem);
  end;

var
  fmNumber: TfmNumber;

implementation

uses Companies, dialog, dialog2, Util, ChangeInfo, DataM;

{$R *.DFM}

procedure TfmNumber.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
var
  Companies: TCompanies;
  I: Integer;
  History: THistory;
begin
  Inherited;
  If not (Self is TfmChangeInfo) then
    begin
      History:= THistory.Create;
      try
        If Not History.IsActionLast(Company.RecNum, faNumber, Now) then
          begin
            If not Silent Then
                If e12.Color = clWindow Then
                begin
                  e12.Color := clBtnFace;
                  edEngName.Color := clBtnFace;
                end Else
                begin
                  e12.Color:= clWindow;
                  edEngName.Color := clWindow;
                end;
            ErrorAt:= e12;
            raise Exception.Create('����� ������ ����� ����� ��');
          end;
      finally
        History.Free;
      end;
    end;
    
  Companies:= TCompanies.Create(Now, False, False);
  Companies.Filter:= afActive;
  try
    For I:= 0 To Companies.Count-1 do
      begin
        If Companies.Names[I] = e12.Text then
          begin
            If not Silent Then
              with e12 do
                If Color = clWindow Then
                  Color := clBtnFace
                Else
                  Color:= clWindow;
            ErrorAt:= e12;
            raise Exception.Create('�� ����� ��� ���� ����� ����');
          end;

        If e13.Enabled and (Companies.Numbers[I] = e13.Text) then
          begin
            If not Silent Then
              with e13 do
                If Color = clWindow Then
                  Color := clBtnFace
                Else
                  Color:= clWindow;
            ErrorAt:= e13;
            raise Exception.Create('���� ����� ��� ���� ����� ����');
          end;
      end;
  finally
    Companies.Free;
  end;
end;

function TfmNumber.EvSaveData(Draft: Boolean): Boolean;
var
  FilesList: TFilesList;
  FileInfo: TFileInfo;
  RegDate: TDateTime;

begin
  try
    RegDate := StrToDate(e12a.Text);
    FilesList:= TFilesList.Create(Company.RecNum, faNumber, False);
    FileInfo:= TFileInfo.Create;
    FileInfo.Action:= faNumber;
    FileInfo.Draft:= False;
    FileInfo.FileName:= Company.Name;
    FileInfo.DecisionDate:= RegDate;
    FileInfo.NotifyDate:= Now;
    FileInfo.CompanyNum:= Company.RecNum;
    FilesList.Add(FileInfo);
    Company.SetNumber(e13.Text, e12.Text);
    if (edEngName.Text <> '') then
       Company.CompanyInfo.SetEnglishName(edEngName.Text,Company, RegDate);

    //SetRegistrationDate must come last (so it effects all the other entries too)
    Company.SetRegistrationDate(RegDate, nil);

    fmdialog2.Show;
    Result:= True;
  finally
    try
    finally
      FilesList.Free;
    end;
  end;
end;

class procedure TfmNumber.EvUndoFile(FileItem: TFileItem);
var
  Company: TCompany;
begin
  Company:= TCompany.Create(FileItem.FileInfo.CompanyNum, FileItem.FileInfo.DecisionDate+1, False);
  Company.SetNumber('����', FileItem.FileInfo.FileName);
  FileItem.DeleteFile;
  Company.Free;
end;

procedure TfmNumber.EvFocusNext(Sender: TObject);
begin
  If Sender=e12a Then e12.SetFocus;
  If Sender=e12 Then
    if e13.Enabled Then
      e13.SetFocus;
  If Sender=e13 Then
    if e12a.Visible Then
      e12a.SetFocus
    else
      e12.SetFocus;
end;

procedure TfmNumber.EvFillData(Company: TCompany);
begin
  lName.Caption:= Company.Name;
  lEngName.Caption:= Company.CompanyInfo.EnglishName;
  e12a.Text:= DateToLongStr(Now);
  e12.Items.Add(Company.Name);
  e12.Items.Add(Company.CompanyInfo.CompanyName2);
  e12.Items.Add(Company.CompanyInfo.CompanyName3);
  e12.Text:= Company.Name;
  e12.SelectAll;

  edEngName.Text := Company.CompanyInfo.EnglishName;
end;

procedure TfmNumber.btBrowseClick(Sender: TObject);
var
  TempCompany: TCompany;
begin
  inherited;
  If DATAChanged Then
    begin
      fmDialog.memo.lines.Clear;
      fmDialog.memo.lines.add('��� ��');
      fmDialog.memo.lines.add('����� ��� ����� ���� ������ ������.');
      fmDialog.memo.lines.add('��� ����� �� ������� ?');
      fmDialog.ShowModal;
      if fmDialog.DialogResult = drYES then
        begin
          If CheckErrors(False,True, False) Then
            Exit
          Else
            b1.Click;
        end;
      If fmDialog.DialogResult = drCancel Then
        Exit;
    end;
  TempCompany:= fmCompanies.Execute(Caption, afNonActive);
  If TempCompany <> Nil Then
    begin
      If Company<> nil Then
        Company.Free;
      Company:= TempCompany;
      EvFillData(Company);
    end;
end;

procedure TfmNumber.FormCreate(Sender: TObject);
begin
  inherited;
  If Tag<0 Then
    Exit;
  btBrowse.Click;
  If Company=nil then
    Close;
end;

end.
