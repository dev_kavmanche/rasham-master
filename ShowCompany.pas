unit ShowCompany;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  HebForm, ExtCtrls, StdCtrls, Mask, HGrids, SdGrid,
  HComboBx, HEdit, ComCtrls, Buttons, HLabel, EditCompanyInfo,
  CompanyInfoTemplate, DataObjects;

type
  TfmShowCompany = class(TfmEditCompanyInfo)
    procedure B3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Execute(ShowDate: TDateTime; CompanyNum: Integer);
  end;

var
  fmShowCompany: TfmShowCompany;

implementation

{$R *.DFM}

uses Util, BlankZip;

procedure TfmShowCompany.Execute(ShowDate: TDateTime; CompanyNum: Integer);
begin
  FileName := '������� ������ ���� ' + DateToLongStr(ShowDate);
  ReadOnly := True;
  Date := ShowDate;
  if CompanyNum = -1 then
  begin
    btBrowse.Click;
    if Company = nil then
      Close
    else
      Show;
  end
  else
  begin
    Company := TCompany.Create(CompanyNum, ShowDate, False);
    EvFillData(Company);
    Show;
  end;
end;

procedure TfmShowCompany.B3Click(Sender: TObject);
begin
  btBrowse.Click;
end;

procedure TfmShowCompany.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Company<>nil then
  begin
    Company.Free;
    Company:=nil;
  end;
  Action:=caHide;
end;

end.
