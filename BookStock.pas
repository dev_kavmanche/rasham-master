unit BookStock;

////////////////////////////////////////////////////
// History:
// VG19364 23.12.2019 ver 1.7.7
//   code refactoring
////////////////////////////////////////////////////

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HGrids, SdGrid, DataObjects, Db, DBTables;

type
  TfmBookStocks = class(TfmBlankTemplate)
    btnHolders: TButton;
    btnStocks: TButton;
    HebLabel3: THebLabel;
    TabSheet2: TTabSheet;
    Grid2: TSdGrid;
    TabSheet3: TTabSheet;
    Grid3: TSdGrid;
    taTemp: TTable;
    procedure btnStocksClick(Sender: TObject);
    procedure e13KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnHoldersClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GridFixedClick(Sender: TObject; Col, Row: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure btBrowseClick(Sender: TObject);

  private
    { Private declarations }
    EnumStocks: TStocks;
    procedure FillStartData;
    procedure AddStock(Stock, Original: TStock; Parent: TStockHolder; Date: TDateTime; IsHakzaa: Boolean; SerNum: Integer);
    procedure AddMuhaz(MuhazItem: TMuhazItem; Date: TDateTime; SerNum: Integer; const ActionStr: String);
    procedure HaavaraEnum(ObjectNum, NewNum: Integer);
    procedure ProcessHaavara(FileItem: TFileItem);
    procedure ProcessHamara(FileItem: TFileItem);
    procedure ProcessHakzaa(FileItem: TFileItem);
    function EnumHamara(ObjectType: TObjectClass; Action: TAction; ObjectNum, NewNum: Integer; FileItem: TFileItem): Boolean;
  protected
    { Protected declarations }
    SortList: TStringList;
    TempCompany: TCompany;
    procedure EvPrintData; override;
    procedure EvFormatGrid(Grid: TSdGrid);
    procedure EvFillData1; virtual;
    procedure EvFillData2; virtual; abstract;
    procedure EvDataLoaded; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvFilldefaultData; override;
    function NotChanged(StockNum: integer): boolean;
    function CheckIfRishum(FileNum: integer): boolean;
  public
    { Public declarations }
  end;

var
  fmBookStocks: TfmBookStocks;

implementation

uses Main, Util, cSTD, PrintStockBook1, DataM;

var
  TempStocks: TStocks;
  TempFileItem: TFileItem;
  TempChanges: TChanges;

const
  cHakzaa = '�����';
  cHaavara = '�����';
  cHamara = '����';

{$R *.DFM}

procedure TfmBookStocks.EvFormatGrid(Grid: TSdGrid);
var
  Col,Row: Integer;
  MaxSize: Integer;
begin
  for Col := 0 To Grid.ColCount-1 do
  begin
    MaxSize := 0;
    for Row := 0 To Grid.RowCount-1 do
      If MaxSize < (Grid.Canvas.TextWidth(Grid.Cells[Col,Row]) + 5) then
        MaxSize := (Grid.Canvas.TextWidth(Grid.Cells[Col,Row]) + 5);
    Grid.ColWidths[Col] := MaxSize;
  end;
end;

procedure TfmBookStocks.EvFilldefaultData;
begin
  inherited;
  Grid2.Cells[0,0] := '�����';
  Grid2.Cells[1,0] := '�����';
  Grid2.Cells[2,0] := '��';
  Grid2.Cells[3,0] := '��'' ����';
  Grid2.Cells[4,0] := '����';
  Grid2.Cells[5,0] := '���';
  Grid2.Cells[6,0] := '�����';
  Grid2.Cells[7,0] := '�����';
  Grid2.Cells[8,0] := '�����';
  Grid2.Cells[9,0] := '��� ������';
  Grid2.Cells[10,0] := '��� ����';
  Grid2.Cells[11,0] := '���� ������';
  Grid2.Cells[12,0] := '��"� ���� ������';
  Grid2.Cells[13,0] := '���� ���� ���� �"� ������ ���� ������';
  Grid2.Cells[14,0] := '��"� ���� ���� ����';
end;

procedure TfmBookStocks.EvFocusNext(Sender: TObject);
begin
  if Sender = e14 then e13.SetFocus;
  if Sender = e13 Then btnHolders.SetFocus;
end;

procedure TfmBookStocks.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);
begin
  inherited;
  if DecisionDate<NotifyDate then
  begin
    if not Silent then
      with e13 do
        if Color = clWindow then
          Color := clBtnFace
        else
          Color := clWindow;
    ErrorAt := e13;
    raise Exception.Create('���� �������� ���� ����');
  end;
end;

procedure TfmBookStocks.EvDataLoaded;
begin
  Inherited;
  If not DoNotClear Then
    NotifyDate:= EncodeDate(1900, 01, 01);
  PageIndex:= 0;
end;

procedure TfmBookStocks.btnStocksClick(Sender: TObject);
begin
  inherited;
  If CheckErrors(False, True, FFromDraft) then
    Exit;
  SortList.Clear;
  SortList.Add('��"�');
  Grid3.RowCount := 2;
  Grid3.ResetLine(1);
  EvFillData2;
  If Grid3.RowCount > 2 Then
    Grid3.RowCount := Grid3.RowCount - 1;
  PageIndex := 2;
  EvFormatGrid(Grid3);
  GridFixedClick(Grid3, 0, 0);
end;

procedure TfmBookStocks.e13KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  PageIndex:= 0;
end;

procedure TfmBookStocks.btnHoldersClick(Sender: TObject);
begin
  inherited;
  if CheckErrors(False, True, FFromDraft) then
    Exit;
  SortList.Clear;
  SortList.Add('��"�');
  Grid2.RowCount := 2;
  Grid2.ResetLine(1);
  EvFillData1;
  if Grid2.RowCount > 2 then
    Grid2.RowCount := Grid2.RowCount - 1;
  PageIndex := 1;
  EvFormatGrid(Grid2);
  GridFixedClick(Grid2, 0, 0);
end;

procedure TfmBookStocks.AddStock(Stock, Original: TStock; Parent: TStockHolder; Date: TDateTime; IsHakzaa: Boolean; SerNum: Integer);
begin
  With Stock do
    begin
      Grid2.Cells[0,Grid2.RowCount-1]:= DateToLongStr(Date);
      If Active Then
        Grid2.Cells[1,Grid2.RowCount-1]:= cHakzaa
      Else
        Grid2.Cells[1,Grid2.RowCount-1]:= cHaavara;
      Grid2.Cells[9,Grid2.RowCount-1]:= Name;
      Grid2.Cells[10,Grid2.RowCount-1]:= FloatToStrF(Value, ffNumber, 15, 4);
      Grid2.Cells[11,Grid2.RowCount-1]:= FloatToStrF(Count, ffNumber, 15, 2);
      if IsHakzaa Then
      begin
        Grid2.Cells[1,Grid2.RowCount-1]:= cHakzaa;
        Grid2.Cells[13,Grid2.RowCount-1]:= FloatToStrF(0, ffNumber, 15, 2);
      end Else
        Grid2.Cells[13,Grid2.RowCount-1]:= FloatToStrF(Paid, ffNumber, 15, 2);
    end;
  If Original<> nil then
    begin
      Grid2.Cells[12,Grid2.RowCount-1]:= FloatToStrF(Original.Count, ffNumber, 15,2);
      Grid2.Cells[14,Grid2.RowCount-1]:= FloatToStrF(Original.Paid, ffNumber, 15,2);
    end
  Else
    begin
      Grid2.Cells[12,Grid2.RowCount-1]:= '0.00';
      Grid2.Cells[14,Grid2.RowCount-1]:= '0.00';
    end;

  SortList.Add(IntToStr(SerNum));
  With Parent do
    begin
      Grid2.Cells[2,Grid2.RowCount-1]:= Name;
      Grid2.Cells[3,Grid2.RowCount-1]:= Zeut;
      Grid2.Cells[4,Grid2.RowCount-1]:= Address.Street;
      Grid2.Cells[5,Grid2.RowCount-1]:= Address.Number;
      Grid2.Cells[6,Grid2.RowCount-1]:= Address.Index;
      Grid2.Cells[7,Grid2.RowCount-1]:= Address.City;
      Grid2.Cells[8,Grid2.RowCount-1]:= Address.Country;
      Grid2.RowCount:= Grid2.RowCount+1;
    end;
end;

procedure TfmBookStocks.AddMuhaz(MuhazItem: TMuhazItem; Date: TDateTime; SerNum: Integer; const ActionStr: String);
begin
  SortList.Add(IntToStr(SerNum));
  With MuhazItem do
    begin
      Grid2.ResetLine(Grid2.RowCount-1);
      Grid2.Cells[0,Grid2.RowCount-1]:= DateToLongStr(Date);
      Grid2.Cells[1,Grid2.RowCount-1]:= ActionStr;
      Grid2.Cells[2,Grid2.RowCount-1]:= '����� ����"�';
      Grid2.Cells[3,Grid2.RowCount-1]:= ':��'' ����';
      Grid2.Cells[4,Grid2.RowCount-1]:= Zeut;
      Grid2.Cells[9,Grid2.RowCount-1]:= Name;
      Grid2.Cells[10,Grid2.RowCount-1]:= FloatToStrF(Value, ffNumber, 15, 4);
      Grid2.Cells[11,Grid2.RowCount-1]:= FloatToStrF(ShtarValue, ffNumber, 15, 2);
      Grid2.Cells[12,Grid2.RowCount-1]:= '';
      Grid2.Cells[13,Grid2.RowCount-1]:= '';
      Grid2.Cells[14,Grid2.RowCount-1]:= '';
      Grid2.RowCount:= Grid2.RowCount+1;
    end;
end;

function TfmBookStocks.NotChanged(StockNum: integer): boolean; //Tsahi 14/11/05 checks if this stock line is an origianl Hakzaa
var
   fileNumber: integer;
begin
  Result :=false;
  try
    With TTable.Create(nil) do
    begin
      DatabaseName:=Data.taChanges.DataBaseName;
      TableName   :=Data.taChanges.TableName;
      Filtered:=false;
      Filter:='([ObjectType] ='''+ObjToChar(ocStockHolder)+''') AND ([Action] = '''+ActionToChar(acNewStock)+''') AND ([NewNum] = '+IntToSTr(StockNum)+' ) ';//AND ([ResInt1] = -1)';
      Filtered :=true;
      Open;
      if RecordCount <>0 then
      begin
           first;
           fileNumber := FieldByName('ResInt1').AsInteger;
           if ((fileNumber = -1) // company was created normally and this is the first hakzaa (no fileNumber was assigned)
            OR (CheckIfRishum(fileNumber)))
            then // company was created by Rishum. There is a file number but this action is the first hakzaa
           begin
             result:=true;
           end; //else, this action should not be displayed
      end;
      free;
    end;
  except
  end;
end;

function TfmBookStocks.CheckIfRishum(FileNum: integer) : boolean; //tsahi 28/04//2010 check if a certain file is refering a company that was created using rishum
var
   compNum: integer;
begin
  Result :=false;
  try
    With TTable.Create(nil) do
    begin
      DatabaseName:=Data.taFiles.DataBaseName;
      TableName   :=Data.taFiles.TableName;
      Filtered:=false;
      Filter:='([RecNum] ='+IntToSTr(FileNum)+' )';
      Filtered :=true;
      Open;
      if RecordCount <>0 then
      begin
           first;
           compNum := FieldByName('Company').AsInteger;
           if (compNum = -1) then //no company number => created in rishum
           begin
                result := true;
           end;
      end;
      free;
    end;
  except
  end;
end;

procedure TfmBookStocks.FillStartData;
var
  TempCompany: TCompany;
  I,J: Integer;
begin
  TempCompany:= TCompany.Create(Company.RecNum, Company.CompanyInfo.RegistrationDate + 1, False);
  try
    TempCompany.StockHolders.Filter := afAll;
    For I := 0 To TempCompany.StockHolders.Count - 1 do
      with TempCompany.StockHolders.Items[I] as TStockHolder Do
      begin
        Stocks.Filter := afAll;
        For J := 0 To Stocks.Count - 1 do
          if NotChanged(TStock(Stocks.Items[J]).RecNum) then
            AddStock(Stocks.Items[J] as TStock,
                     Stocks.Items[J] as TStock,
                     TempCompany.StockHolders.Items[I] as TStockHolder,
                     Company.CompanyInfo.RegistrationDate,
                     true,
                     -1);
      end;
    TempCompany.MuhazList.Filter:= afAll;
    For I := 0 to TempCompany.MuhazList.Count - 1 do
      AddMuhaz(TempCompany.MuhazList.Items[I] as TMuhazItem, Company.CompanyInfo.RegistrationDate, -1, cHakzaa);
  finally
    TempCompany.Free;
  end;
end;

procedure TfmBookStocks.EvFillData1;
var
  FilesList: TFilesList;
  FileItem: TFileItem;
  i: Integer;
begin
  if NotifyDate <= Company.CompanyInfo.RegistrationDate then
    FillStartData;

  FilesList := TFilesList.Create(Company.RecNum, faHakzaa, False);
  try
    for i := 0 to FilesList.Count - 1 do
    begin
      FileItem:= FilesList.Items[i];
        try
          if (FileItem.FileInfo.DecisionDate <= DecisionDate) and (FileItem.FileInfo.DecisionDate >= NotifyDate) then
            ProcessHakzaa(FileItem);
        finally
          FileItem.Free;
        end;
    end;
  finally
    FilesList.Free;
  end;

  TempChanges := TChanges.Create(Company);
  FilesList := TFilesList.Create(Company.RecNum, faHaavara, False);
  try
    for i := 0 to FilesList.Count-1 do
    begin
      FileItem:= FilesList.Items[i];
      try
        if (FileItem.FileInfo.DecisionDate <= DecisionDate) and (FileItem.FileInfo.DecisionDate >= NotifyDate) then
          ProcessHaavara(FileItem);
      finally
        FileItem.Free;
      end;
    end;
  finally
    FilesList.Free;
    TempChanges.Free;
  end;

  FilesList := TFilesList.Create(Company.RecNum, faHamara, False);
  try
    for i := 0 to FilesList.Count-1 do
    begin
      FileItem := FilesList.Items[i];
      try
        if (FileItem.FileInfo.DecisionDate <= DecisionDate) and (FileItem.FileInfo.DecisionDate >= NotifyDate) then
          ProcessHamara(FileItem);
      finally
        FileItem.Free;
      end;
    end;
  finally
    FilesList.Free;
  end;
end;

function TfmBookStocks.EnumHamara(ObjectType: TObjectClass; Action: TAction; ObjectNum, NewNum: Integer; FileItem: TFileItem): Boolean;

  procedure AddHamaraMuhaz(RecNum: Integer);
  var
    MuhazItem: TMuhazItem;
  begin
    MuhazItem:=TMuhazItem.Create(RecNum, Company, FileItem.FileInfo.DecisionDate);
    try
      AddMuhaz(MuhazItem, FileItem.FileInfo.DecisionDate, FileItem.FileInfo.RecNum, cHamara);
    finally
      MuhazItem.Free;
    end;
  end;

  procedure AddHamaraStock(RecNum, ParentNum: Integer; Active: Boolean);

    function FindStockHolder: TStockHolder;
    var
      I, J: Integer;
      StockHolder: TStockHolder;
    begin
      Result:= nil;
      TempCompany.StockHolders.Filter:= afAll;
      try
        for I := 0 to TempCompany.StockHolders.Count - 1 do
          if Active then
          begin
            if TempCompany.StockHolders.Items[I].RecNum = ParentNum then
            begin
              Result := TempCompany.StockHolders.Items[I] as TStockHolder;
              Abort;
            end;
          end
          else
          begin
            StockHolder := TempCompany.StockHolders.Items[I] as TStockHolder;
            StockHolder.Stocks.Filter := afAll;
            for J := 0 to StockHolder.Stocks.Count - 1 do
              if StockHolder.Stocks.Items[J].RecNum = RecNum then
              begin
                Result := StockHolder;
                Abort;
              end;
          end;
        except
        end;
      if not Assigned(Result) then
        raise Exception.Create('Stock Holder number: ' + IntToStr(RecNum) +
                               ' not found for File #' + IntToStr(FileItem.FileInfo.RecNum));
    end;

  var
    Stock: TStock;
  begin
    Stock:= TStock.Create(RecNum, FindStockHolder, FileItem.FileInfo.DecisionDate);
    if not Active then
      Stock.DeActivate(FileItem.FileInfo.DecisionDate);
    EnumStocks.Add(Stock);
  end;

begin
  if (ObjectType = ocCompany) and (Action = acNewMuhaz) then
    AddHamaraMuhaz(NewNum);
  if (ObjectType = ocStockHolder) and (Action = acNewStock) then
    AddHamaraStock(NewNum, ObjectNum, True);
  if (ObjectType = ocStock) and (Action = acDelete) then
    AddHamaraStock(ObjectNum, ObjectNum, False);
  Result:= True;
end;

procedure TfmBookStocks.ProcessHamara(FileItem: TFileItem);
var
  Changes: TChanges;
  HonList: THonList;
  SourceHon, DestHon: THonItem;
  I: Integer;

  procedure FillHamaraStocks;

    procedure ProcessStockHolder(StockHolder: TStockHolder);

      procedure AddHamaraStock(OldStock, NewStock: TStock);
      begin
        If (OldStock=nil) and (NewStock=nil) then
          if (SourceHon.Name = DestHon.Name) and (SourceHon.Value=DestHon.Value) then
            Exit
          else
            raise Exception.Create('Stock not found for StockHolder '+IntToStr(StockHolder.RecNum)+' at file #'+IntToStr(FileItem.FileInfo.RecNum));
        Grid2.Cells[0,Grid2.RowCount-1]:= DateToLongStr(FileItem.FileInfo.DecisionDate);
        Grid2.Cells[1,Grid2.RowCount-1]:= cHamara;
        with StockHolder do
          begin
            Grid2.Cells[2,Grid2.RowCount-1]:= Name;
            Grid2.Cells[3,Grid2.RowCount-1]:= Zeut;
            Grid2.Cells[4,Grid2.RowCount-1]:= Address.Street;
            Grid2.Cells[5,Grid2.RowCount-1]:= Address.Number;
            Grid2.Cells[6,Grid2.RowCount-1]:= Address.Index;
            Grid2.Cells[7,Grid2.RowCount-1]:= Address.City;
            Grid2.Cells[8,Grid2.RowCount-1]:= Address.Country;
          end;
        if Assigned(NewStock) then
          with NewStock do
            begin
              Grid2.Cells[9,Grid2.RowCount-1]:= Name;
              Grid2.Cells[10,Grid2.RowCount-1]:= FloatToStrF(Value, ffNumber, 15, 4);
              if Assigned(OldStock) then
                Grid2.Cells[11,Grid2.RowCount-1]:= FloatToStrF(Count-OldStock.Count, ffNumber, 15, 2)
              else
                Grid2.Cells[11,Grid2.RowCount-1]:= FloatToStrF(Count, ffNumber, 15, 2);
              Grid2.Cells[12,Grid2.RowCount-1]:= FloatToStrF(Count, ffNumber, 15,2);
              Grid2.Cells[13,Grid2.RowCount-1]:= FloatToStrF(0, ffNumber, 15, 2);
              Grid2.Cells[14,Grid2.RowCount-1]:= FloatToStrF(Paid, ffNumber, 15,2);
            end
        else
          with OldStock do
            begin
              Grid2.Cells[9,Grid2.RowCount-1]:= Name;
              Grid2.Cells[10,Grid2.RowCount-1]:= FloatToStrF(Value, ffNumber, 15, 4);
              Grid2.Cells[11,Grid2.RowCount-1]:= FloatToStrF(-Count, ffNumber, 15, 2);
              Grid2.Cells[12,Grid2.RowCount-1]:= FloatToStrF(0, ffNumber, 15,2);
              Grid2.Cells[13,Grid2.RowCount-1]:= FloatToStrF(0, ffNumber, 15, 2);
              Grid2.Cells[14,Grid2.RowCount-1]:= FloatToStrF(0, ffNumber, 15,2);
            end;
        Grid2.RowCount:= Grid2.RowCount+1;
      end;

    var
      I: Integer;
      Stock, OldSourceStock, NewSourceStock, OldDestStock, NewDestStock: TStock;
    begin
      OldSourceStock:= nil;
      NewSourceStock:= nil;
      OldDestStock:= nil;
      NewDestStock:= nil;
      For I:= 0 to EnumStocks.Count-1 do
        begin
          Stock:= EnumStocks.Items[I] as TStock;
          if Stock.Parent = StockHolder then
            begin
              if (Stock.Name=SourceHon.Name) and (Stock.Value = SourceHon.Value) then
                if Stock.Active then
                  NewSourceStock:= Stock
                else
                  OldSourceStock:= Stock
              else
                if Stock.Active then
                  NewDestStock:= Stock
                else
                  OldDestStock:= Stock;
            end;
        end;
      AddHamaraStock(OldSourceStock, NewSourceStock);
      AddHamaraStock(OldDestStock, NewDestStock);
    end;

  var
    I: Integer;
    StockHolders: TStockHolders;
    StockHolder: TStockHolder;
  begin
    StockHolders:= TStockHolders.CreateNew;
    try
      for I:= 0 to EnumStocks.Count-1 do
        begin
          StockHolder:= (EnumStocks.Items[I] as TStock).Parent as TStockHolder;
          if StockHolders.FindByZeut(StockHolder.Zeut) = nil then
            begin
              ProcessStockHolder(StockHolder);
              StockHolders.Add(StockHolder);
            end;
        end;
    finally
      StockHolders.Deconstruct;
    end;
  end;

begin
  TempCompany:= TCompany.Create(Company.RecNum, FileItem.FileInfo.DecisionDate, False);
  EnumStocks:= TStocks.CreateNew;
  EnumStocks.Filter:= afAll;
  Changes:= TChanges.Create(Company);
  SourceHon:= nil;
  DestHon:= nil;
  HonList:= THonList.LoadFromFile(Company, FileItem);
  try
    for I:= 0 to HonList.Count-1 do
      if (HonList.Items[I] as THonItem).Rashum = 1 then
        SourceHon:= HonList.Items[I] as THonItem
      else
        If (HonList.Items[I] as THonItem).Rashum = 2 then
          DestHon:= HonList.Items[I] as THonItem;

    if (SourceHon=nil) or (DestHon=nil) then
      Raise Exception.Create('Error loading Hamara file number: '+IntToStr(FileItem.FileInfo.RecNum));

    Changes.EnumFileItem(FileItem, EnumHamara);
    FillHamaraStocks;
  finally
    TempCompany.Free;
    HonList.Free;
    Changes.Free;
    EnumStocks.Free;
  end;
end;

procedure TfmBookStocks.ProcessHakzaa(FileItem: TFileItem);
var
  StockHolders: TStockHolders;
  I,J: Integer;
  Changes: TChanges;

  function GetStocks(Const Zeut: String): TStocks;
  var
    I: Integer;
  begin
    Result := nil;
    Company.StockHolders.Filter := afAll;
    For I := 0 To Company.StockHolders.Count - 1 do
      If (Company.StockHolders.Items[I].Zeut = Zeut) And (Company.StockHolders.Items[I].LastChanged <= FileItem.FileInfo.DecisionDate) Then
      begin
        Result := (Company.StockHolders.Items[I] as TStockHolder).Stocks;
        Exit;
      end;
  end;

begin
  Changes:= TChanges.Create(Company);
  StockHolders:= TStockHolders.LoadFromFile(Company, FileItem);
  try
    For I:= 0 to StockHolders.Count - 1 do
      with StockHolders.Items[I] as TStockHolder do
        For J:= 0 to Stocks.Count - 1 do
          AddStock(Stocks.Items[J] as TStock,
                   Changes.GetStockInfo(GetStocks(StockHolders.Items[I].Zeut), FileItem, Stocks.Items[J] as TStock),
                   StockHolders.Items[I] as TStockHolder,
                   FileItem.FileInfo.DecisionDate,
                   True,
                   FileItem.FileInfo.RecNum);
  finally
    StockHolders.Free;
    Changes.Free;
  end;
end;

procedure TfmBookStocks.HaavaraEnum(ObjectNum, NewNum: Integer);
var
  I,J: Integer;
begin
  Company.StockHolders.Filter:= afAll;
  For I:= 0 To Company.StockHolders.Count-1 do
    if Company.StockHolders.Items[I].RecNum = NewNum then
      For J:= 0 to TempStocks.Count-1 do
        AddStock(TempStocks.Items[J] as TStock, TempChanges.GetStockInfo((Company.StockHolders.Items[I] as TStockHolder).Stocks, TempFileItem, TempStocks.Items[J] as TStock),
                 Company.StockHolders.Items[I] as TStockHolder, TempFileItem.FileInfo.DecisionDate, False, TempFileItem.FileInfo.RecNum);
end;

procedure TfmBookStocks.ProcessHaavara(FileItem: TFileItem);

   function FindStockHolder(Input: TStockHolder): TStockHolder;
   var
     I: Integer;
   begin
     Result:= Input;
     For I:= 0 To Company.StockHolders.Count-1 do
       If (Company.StockHolders.Items[I].Zeut = Input.Zeut) And (Company.StockHolders.Items[I].RecNum <= Input.RecNum+1) Then
       begin
         Result:= Company.StockHolders.Items[I] as TStockHolder;
         Exit;
       end;
   end;

var
  I,J: Integer;
  StockHolders: TStockHolders;
begin
  StockHolders:= TStockHolders.LoadFromFile(Company, FileItem);
  TempStocks:= TStocks.CreateNew;
  TempFileItem:= FileItem;
  Company.StockHolders.Filter:= afAll;
  For I:= 0 to StockHolders.Count-1 do
    With StockHolders.Items[I] as TStockHolder do
      For J:= 0 to Stocks.Count-1 do
      begin
        AddStock(Stocks.Items[J] as TStock,
                 TempChanges.GetStockInfo(FindStockHolder(StockHolders.Items[I] as TStockHolder).Stocks,FileItem,Stocks.Items[J] as TStock),
           FindStockHolder(StockHolders.Items[I] as TStockHolder), FileItem.FileInfo.DecisionDate, False, FileItem.FileInfo.RecNum);
        With Stocks.Items[J] as TStock do
          TempStocks.Add(TStock.CreateNew(Now, Name, Value, Count, Paid, False));
      end;
  TempStocks.Filter:= afNonActive;
  FileItem.EnumChanges(ocStockHolder, -1, acDelete, HaavaraEnum);
  StockHolders.Free;
  TempStocks.Free;
end;

procedure TfmBookStocks.EvPrintData;
var
  I,J: Integer;
begin
  case PageIndex of
    1: begin
         Application.CreateForm(TfmQRStockBook1, fmQRStockBook1);
         try
           taTemp.FieldDefs.Clear;
           taTemp.FieldDefs.Add('Date', ftString, 10, False);
           taTemp.FieldDefs.Add('Action', ftString, 15, False);
           taTemp.FieldDefs.Add('Name', ftString, 40, False);
           taTemp.FieldDefs.Add('Zeut', ftString, 9, False);
           taTemp.FieldDefs.Add('Street', ftString, 40, False);
           taTemp.FieldDefs.Add('Number', ftString, 8, False);
           taTemp.FieldDefs.Add('Index', ftString, 8, False);
           taTemp.FieldDefs.Add('City', ftString, 30, False);
           taTemp.FieldDefs.Add('Country',ftString,30, False);
           taTemp.FieldDefs.Add('StockName', ftString, 40, False);
           taTemp.FieldDefs.Add('StockValue', ftString, 20, False);
           taTemp.FieldDefs.Add('StockCount', ftString, 20, False);
           taTemp.FieldDefs.Add('StockCountAll', ftString, 20, False);
           taTemp.FieldDefs.Add('StockPaid', ftString, 20, False);
           taTemp.FieldDefs.Add('StockPaidAll', ftString, 20, False);
           taTemp.CreateTable;
           taTemp.Open;
           for I := 1 To Grid2.RowCount - 1 do
           begin
             taTemp.Append;
             For J := 0 To Grid2.ColCount - 1 do
               taTemp.Fields[J].AsString := MakeHebStr(Grid2.Cells[J,I], False);
             taTemp.Post;
           end;
           fmQRStockBook1.PrintExecute(taTemp, Caption, Company, DecisionDate, NotifyDate);
         finally
           taTemp.Close;
           fmQRStockBook1.Free;
         end;
       end;
  end;
end;

procedure TfmBookStocks.FormCreate(Sender: TObject);
begin
  SortList := TStringList.Create;
  ShowStyle := afActive;
  FileAction := faNone;
  inherited;

  // Moshe 27/05/2019
  if (Company <> nil) then
    btnHoldersClick(Sender);
end;

procedure TfmBookStocks.GridFixedClick(Sender: TObject; Col, Row: Integer);
var
  //SortType: TSortType;
  SortType: String;
begin
  if Pos(','+IntToStr(Col)+',', (Sender as TSdGrid).DateCells) <> 0 then
    //SortType:= stDate
    SortType:= 'DATE'
  Else If Pos(','+IntToStr(Col)+',',(Sender as TSdGrid).SummCells) <> 0 then
    //SortType:= stNumber
    SortType:= 'NUMBER'
  Else
    //SortType:= stString;
    SortType:= 'STRING';

  With Sender as THebStringGrid do
  begin
    ColCount:= ColCount+1;
    Cols[ColCount-1].Clear;
    Cols[ColCount-1].AddStrings(SortList);
  end;

  //SortGrid(Sender as THebStringGrid, Col, (Sender as THebStringGrid).ColCount-1, SortType, stNumber);
  SortGrid(Sender as THebStringGrid,
           IntToStr(Col)+','+SortType+',UP;'+IntToStr((Sender as THebStringGrid).ColCount-1)+',NUMBER,UP');

  With Sender as THebStringGrid do
  begin
    SortList.Clear;
    SortList.AddStrings(Cols[ColCount-1]);
    ColCount:= ColCount-1;
    Repaint;
  end;
end;

procedure TfmBookStocks.FormShow(Sender: TObject);
begin
  inherited;
  e14.Text := '01/01/1900';
  e14.SetFocus;
end;

procedure TfmBookStocks.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DataChanged:= False;
  inherited;
end;

procedure TfmBookStocks.FormDestroy(Sender: TObject);
begin
  inherited;
  SortList.Free;
end;

procedure TfmBookStocks.btBrowseClick(Sender: TObject);
begin
  DataChanged := False;
  inherited;

  // Moshe 27/05/2019
  if (Company <> nil) then
    btnHoldersClick(Sender);
end;

end.
