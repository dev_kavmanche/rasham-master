unit PrinterSetup;

{
History:
date        version     by   task       description
26/12/2019  1.7.7       VG   19364      code refactoring
15/06/2020  1.8.1       sts  20654      fix bug with def printer for rep5
15/06/2020  1.8.2       sts  20687      fixed bug with ambiguous with .. do
22/12/2020  1.8.4       sts  22057      refactoring
}


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, HebForm, QuickRpt, QRPrntr, HComboBx, HLabel, HEdit, Printers;

type
  TPrintProc = procedure (DoPrint: Boolean) of Object;

  TfmPrinterSetup = class(TForm)
    btSetup: TButton;
    btPrint: TButton;
    btPreview: TButton;
    Bevel1: TBevel;
    HebForm1: THebForm;
    edFileName: THebEdit;
    HebLabel1: THebLabel;
    HebLabel2: THebLabel;
    cbPrinter: THebComboBox;
    btCancel: TButton;
    procedure btSetupClick(Sender: TObject);
    procedure btPreviewClick(Sender: TObject);
    procedure btPrintClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbPrinterChange(Sender: TObject);
  private
    { Private declarations }
    FPrintFullCaption: Boolean;
    FPrintProc: TPrintProc;
    {DEBUG-->}
    FName,
    PrnAction,
    SelectedPrinter : String;
    procedure SaveLog;
    {<--}
  public
    { Public declarations }
    PrintDateInDoh: Boolean;
    procedure Execute(const Filename: String; PrintProc: TPrintProc);
    procedure FormatQR(aQR: TQuickRep);
    procedure FormatCompositeQR(QR: TQRCompositeReport);
    procedure BindReportToPrinter(aQR: TQuickRep);
  end;

var
  fmPrinterSetup: TfmPrinterSetup;

implementation

uses PrinterSetupQR, Main, DataM, QRCTRLS, Util, HDialogs;

{$R *.DFM}

procedure TfmPrinterSetup.FormatCompositeQR(QR: TQRCompositeReport);
var
  QRPrinter: TQRPrinter;
begin
  With QRPrinterSetup.PrinterSettings do
  begin
    QR.PrinterSettings.PrinterIndex:= PrinterIndex;
    QR.PrinterSettings.Copies:= Copies;
    QR.PrinterSettings.Duplex:= Duplex;
    QR.PrinterSettings.FirstPage:= FirstPage;
    QR.PrinterSettings.LastPage:= LastPage;
  end;
  QRPrinter:= TQRPrinter.Create;
  try
    QRPrinter.PrinterIndex:= Printer.PrinterIndex;
    QR.PrinterSettings.PaperSize:= QRPrinter.PaperSize;
  finally
    QRPrinter.Free;
  end;
end;

procedure TfmPrinterSetup.FormatQR(aQR: TQuickRep);
var
  I,J: Integer;
begin
  For I := 0 To aQR.BandList.Count-1 do
    with TQRBand(aQR.BandList.Items[I]) do
      For J := 0 to ControlCount-1 do
      begin
        if (Not FPrintFullCaption) and (Controls[J].Tag in [10..20]) Then
        begin
          If Controls[J] is TQRLabel Then
          begin
            (Controls[J] as TQRLabel).Color := clWhite;
            (Controls[J] as TQRLabel).Font.Color := clBlack;
          end
          else If Controls[J] is TQRShape Then
            (Controls[J] as TQRShape).Brush.Color := clWhite;
        end;

        if (Controls[J].Tag) in [20..30] Then
        begin
          If Controls[J] is TQRLabel Then
            (Controls[J] as TQRLabel).Caption := MakeHebStr((Controls[J] as TQRLabel).Caption, Tag = 22);  //sts20687
        end;
      end;
  BindReportToPrinter(aQr); //sts20654
end;

/// TfmPrinterSetup.BindReportToPrinter
/// bind report and selected printer
/// task: sts20654
procedure TfmPrinterSetup.BindReportToPrinter(aQR: TQuickRep);
var
  qrPrinter: TQRPrinter;
begin
  if aQR <> nil then
  begin
    With QRPrinterSetup.PrinterSettings do
    begin
      aQR.PrinterSettings.PrinterIndex:= PrinterIndex;
      aQR.PrinterSettings.Copies:= Copies;
      aQR.PrinterSettings.Duplex:= Duplex;
      aQR.PrinterSettings.FirstPage:= FirstPage;
      aQR.PrinterSettings.LastPage:= LastPage;
    end;
    qrPrinter := TQRPrinter.Create;
    try
      qrPrinter.PrinterIndex:= Printer.PrinterIndex;
      aQR.Page.PaperSize:= qrPrinter.PaperSize;
    finally
      qrPrinter.Free;
    end;
  end;
end;

procedure TfmPrinterSetup.Execute(const Filename: String; PrintProc: TPrintProc);
begin
  edFileName.Text := Filename;
  FPrintProc := PrintProc;
  {DEBUG-->}
  FName := Filename;
  {<--}
  ShowModal;
end;

procedure TfmPrinterSetup.btSetupClick(Sender: TObject);
begin
  QRPrinterSetup.PrinterSetup;
  cbPrinter.ItemIndex:= QRPrinterSetup.PrinterSettings.PrinterIndex;
  If cbPrinter.ItemIndex=-1 then
    cbPrinter.ItemIndex:= 0;
  cbPrinterChange(Self);
end;

procedure TfmPrinterSetup.btPreviewClick(Sender: TObject);
{DEBUG-->}
var
  i : Integer;
{<--}
begin
  {DEBUG-->}
  PrnAction := 'Preview';
  SelectedPrinter := cbPrinter.Text;
  for i := 1 to ParamCount do
    if UpperCase( ParamStr( i ) ) = 'PLOG' then
    begin
      SaveLog;
      Break;
    end;
  {<--}
  FPrintProc(False);
end;

procedure TfmPrinterSetup.btPrintClick(Sender: TObject);
{DEBUG-->}
var
  i : Integer;
{<--}
begin
  {DEBUG-->}
  PrnAction := 'Print';
  SelectedPrinter := cbPrinter.Text;
  for i := 1 to ParamCount do
    if UpperCase( ParamStr( i ) ) = 'PLOG' then
    begin
      SaveLog;
      Break;
    end;
  {<--}
  FPrintProc(True);
  Close;
end;

procedure TfmPrinterSetup.FormCreate(Sender: TObject);
var
  I: Integer;
  FileData: TStringList;
begin
  cbPrinter.Clear;
  For I := 0 to Printer.Printers.Count-1 do
    cbPrinter.Items.Add(Printer.Printers[I]);

  FileData:= TStringList.Create;
  try
    FileData.LoadFromFile('Printer.set');
    cbPrinter.ItemIndex:= cbPrinter.Items.IndexOf(FileData.Strings[0]);
    if cbPrinter.ItemIndex=-1 then
      cbPrinter.ItemIndex:= 0;
  except
    cbPrinter.ItemIndex:= cbPrinter.Items.IndexOf(Printer.Printers.Strings[Printer.PrinterIndex]);
  end;

  FileData.Clear;
  try
    FileData.LoadFromFile(Data.taName.Database.Directory+'Account.dat');
    FPrintFullCaption:= FileData.Strings[2] <> '1';
    PrintDateInDoh:= FileData.Strings[3] = '1';
  except
    FPrintFullCaption:= True;
    PrintDateInDoh:= True;
  end;
  FileData.Free;

  cbPrinterChange(cbPrinter);
end;

procedure TfmPrinterSetup.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  FileData: TStringList;
begin
  FileData:= TStringList.Create;
  try
    FileData.Add(cbPrinter.Text);
    FileData.SaveToFile('Printer.set');
  except
    HebShowMessage('�� ���� ����� ����� ������');
  end;
  FileData.Free;
end;

procedure TfmPrinterSetup.cbPrinterChange(Sender: TObject);
begin
  try
    QRPrinterSetup.PrinterSettings.PrinterIndex:= cbPrinter.ItemIndex;
    Printer.PrinterIndex:= cbPrinter.ItemIndex;
  except
  end;
end;

{DEBUG-->}
//==============================================================================
procedure TfmPrinterSetup.SaveLog;
var
  FContents : TStringList;
  LogFile : String;
  i : Integer;
begin
  LogFile := ExtractFilePath( Application.ExeName ) + 'print.log';
  FContents := TStringList.Create;
  try
    FContents.Clear;

    if FileExists( LogFile ) then
      FContents.LoadFromFile( LogFile );
    FContents.Add( '=====================================================' );
    FContents.Add( 'Print on ' + FormatDateTime( 'dd/mm/yyyy hh:nn:ss', Now ) );
    //FContents.Add( 'Printing by UserName' );
    FContents.Add( 'Printing: ' + FName );
    FContents.Add( 'Action: ' + PrnAction );
    FContents.Add( #9 + 'Printer list: ' );
    for i := 0 to ( Printer.Printers.Count - 1 ) do
      FContents.Add( #9#9 + Printer.Printers[i] );

    FContents.Add( #9 + 'User selected printer: ' + SelectedPrinter );
    FContents.Add( #9 + 'Windows selected printer: ' + Printer.Printers[Printer.PrinterIndex] );
    FContents.Add( #9 + 'QuickReport selected printer index: ' + IntToStr( QRPrinterSetup.PrinterSettings.PrinterIndex ) );

    FContents.SaveToFile( LogFile );
  finally
    FContents.Free;
  end;
end;
{<--}

end.
