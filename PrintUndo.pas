unit PrintUndo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, Db, DBTables, DataObjects;

type
  TfmQRUndo = class(TForm)
    taTemp: TTable;
    qrUndo: TQuickRep;
    QRBand2: TQRBand;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText1: TQRDBText;
    QRBand1: TQRBand;
    title: TQRLabel;
    QRShape24: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel11: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    string2: TQRLabel;
    string1: TQRLabel;
    l1: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel12: TQRLabel;
    l2: TQRLabel;
    lZeut: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    procedure QRDBText1Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    procedure PrintProc(DoPrint: Boolean);
  public
    { Public declarations }
    procedure PrintExecute(Company: TCompany);
  end;

var
  fmQRUndo: TfmQRUndo;

implementation

{$R *.DFM}

uses Util, PrinterSetup, DataM, Main, HPSConsts;

procedure TfmQRUndo.PrintExecute(Company: TCompany);
begin
  String1.Caption:= fmMain.UserOptions.RoeHeshbon;
  string2.Caption:= fmMain.UserOptions.RoeAddress;
  l1.Caption:= Company.Name;
  l2.Caption:= Company.Zeut;

  //==>Tsahi 26.7.2006: Change of phone number
  QRLabel9.Caption := '��. '+HPSPhone;
  QRLabel11.Caption := '���.  '+HPSFax;
  QRLabel7.Caption := '';
  QRLabel6.Caption := HPSName;
  //<==
  fmPrinterSetup.Execute('����� ������', PrintProc);
end;

procedure TfmQRUndo.PrintProc(DoPrint: Boolean);
begin
  taTemp.Active:= True;
  fmPrinterSetup.FormatQR(qrUndo);
  If DoPrint then
    qrUndo.Print
  Else
    begin
      qrUndo.Preview;
      Application.ProcessMessages;
    end;
end;

procedure TfmQRUndo.QRDBText1Print(sender: TObject; var Value: String);
begin
  Value:= ' '+Value+' ';
end;

end.
