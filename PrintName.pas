unit PrintName;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, Qrctrls, ExtCtrls, DataObjects;

type
  TfmQRName = class(TForm)
    qrNameOld: TQuickRep;
    QRBand2: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel25: TQRLabel;
    Label3243: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel37: TQRLabel;
    l4: TQRLabel;
    QRLabel54: TQRLabel;
    QRBand4: TQRBand;
    l1: TQRLabel;
    l2: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    l3: TQRLabel;
    qrName: TQuickRep;
    QRBand1: TQRBand;
    QRImage8: TQRImage;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    QRImage13: TQRImage;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    edtCompanyName: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel8: TQRLabel;
    edtCompanyID: TQRLabel;
    QRLabel9: TQRLabel;
    edtDecisionDate: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    edtCompanyName1: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    edtCompanyName2: TQRLabel;
    QRLabel104: TQRLabel;
    edtCompanyName3: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    edtDecisionDate2: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRShape5: TQRShape;
    QRLabel28: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    edtRegulationNo: TQRLabel;
    edtRegulationYes: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape8: TQRShape;
    edtSigName: TQRLabel;
    QRLabel34: TQRLabel;
    QRShape9: TQRShape;
    edtSigZehut: TQRLabel;
    QRShape10: TQRShape;
    QRLabel35: TQRLabel;
    QRShape11: TQRShape;
    QRLabel36: TQRLabel;
    edtSigDate: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    edtMagishName: TQRLabel;
    QRShape12: TQRShape;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRShape13: TQRShape;
    edtMagishPhone: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRBand3: TQRBand;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRShape27: TQRShape;
    QRLabel52: TQRLabel;
    QRLabel173: TQRLabel;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
  private
    { Private declarations }
    procedure PrintProc(DoPrint: Boolean);
  public
    { Public declarations }
    //--> Change Company Name functionality improvement == Added on 06/11/2001 by Elijah Shlieserman
    //REM by ES>> procedure PrintExecute(Company: TCompany; Const NewName: String);
    procedure PrintExecute(
                            CompanyName,
                            CompanyZeut,
                            DecisionDate,
                            CompanyName1,
                            CompanyName2,
                            CompanyName3,
                            No,
                            Yes,
                            SigName,
                            SigZehut,
                            MagishName,
                            MagishPhone
                            : String);
    procedure PrintEmpty(Const FileName: String);
  end;

var
  fmQRName: TfmQRName;

implementation

uses PrinterSetup, Util, Main;

{$R *.DFM}

procedure TfmQRName.PrintProc(DoPrint: Boolean);
begin
  fmPrinterSetup.FormatQR(qrName);
  If DoPrint then
    qrName.Print
  Else
  begin
    qrName.Preview;
    Application.ProcessMessages;
  end;
end;

procedure TfmQRName.PrintExecute(
                            CompanyName,
                            CompanyZeut,
                            DecisionDate,
                            CompanyName1,
                            CompanyName2,
                            CompanyName3,
                            No,
                            Yes,
                            SigName,
                            SigZehut,
                            MagishName,
                            MagishPhone
                            : String);
begin
  // Moshe 19/12/16
  edtCompanyName.Caption := CompanyName;
  edtCompanyID.Caption := CompanyZeut;
  edtDecisionDate.Caption := DecisionDate;
  edtCompanyName1.Caption := CompanyName1;
  edtCompanyName2.Caption := CompanyName2;
  edtCompanyName3.Caption := CompanyName3;
  edtDecisionDate2.Caption := edtDecisionDate.Caption;
  edtRegulationNo.Caption := No;
  edtRegulationYes.Caption := Yes;
  edtSigName.Caption := SigName;
  edtSigZehut.Caption := SigZehut;
  edtSigDate.Caption := DateToStr(Now);
  edtMagishName.Caption := MagishName;
  edtMagishPhone.Caption := MagishPhone;

  fmPrinterSetup.Execute(CompanyName, PrintProc);
end;

procedure TfmQRName.PrintEmpty(const FileName: String);
var i:integer;
begin
  for i := 0 to ComponentCount-1 do
    if (UpperCase(Copy(Components[i].Name,1,3)) = 'EDT') and (Components[i] is TQRLabel) then
      (Components[i] as TQRLabel).Caption := '';
  fmPrinterSetup.Execute(FileName, PrintProc);
end;

end.
