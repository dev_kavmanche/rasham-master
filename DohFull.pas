unit DohFull;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogsTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, HLabel,
  HGrids, SdGrid, DataObjects;

type
  TfmDohFull = class(TfmTemplate)
    Grid1: TSdGrid;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel26: TPanel;
    TabSheet2: TTabSheet;
    Grid2: TSdGrid;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel2: TPanel;
    Panel8: TPanel;
    HebLabel1: THebLabel;
    HebLabel2: THebLabel;
    Panel12: TPanel;
    HebLabel3: THebLabel;
    HebLabel4: THebLabel;
    Panel15: TPanel;
    HebLabel5: THebLabel;
    Panel16: TPanel;
    HebLabel6: THebLabel;
    HebLabel7: THebLabel;
    TabSheet3: TTabSheet;
    Grid3: TSdGrid;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    Panel25: TPanel;
    Panel27: TPanel;
    TabSheet4: TTabSheet;
    Grid4: TSdGrid;
    Panel31: TPanel;
    Panel32: TPanel;
    Panel33: TPanel;
    Grid2a: TSdGrid;
    Panel3: TPanel;
    Panel1: TPanel;
    Panel17: TPanel;
    Panel28: TPanel;
    Shape8234: TShape;
    HebLabel1354: THebLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ShowDoh(Company: TCompany; LastDoh: TDateTime);
  end;

var
  fmDohFull: TfmDohFull;

implementation

{$R *.DFM}

uses Util, Doh;

procedure TfmDohFull.ShowDoh(Company: TCompany; LastDoh: TDateTime);
var
  I,J: Integer;
begin
  // Phase 1
  FileName:= '���� '+DateToLongStr(Company.ShowingDate-1);
  Company.HonList.Filter:= afActive;
  Grid1.RowCount := Company.HonList.Count;
  Grid1.ResetLine(0);
  For I:= 0 To Company.HonList.Count-1 do
    With Company.HonList.Items[I] as THonItem Do
      begin
        Grid1.Cells[0,I]:= Name;
        Grid1.Cells[1,I]:= FloatToStrF(Value, ffNumber, 15, 4);
        Grid1.Cells[2,I]:= FloatToStrF(Rashum, ffNumber, 15, 2);
        Grid1.Cells[3,I]:= FloatToStrF(Mokza, ffNumber, 15, 2);
      end;
  Grid1.Repaint;

  Company.MuhazList.Filter:= afActive;
  Grid2a.RowCount:= Company.MuhazList.Count;
  Grid2a.ResetLine(0);
  For I:= 0 To Company.MuhazList.Count-1 do
    With Company.MuhazList.Items[I] as TMuhazItem do
      begin
        Grid2a.Cells[0,I]:= Name;
        Grid2a.Cells[1,I]:= FloatToStrF(Value, ffNumber, 15, 4);
        Grid2a.Cells[2,I]:= Zeut;
        Grid2a.Cells[3,I]:= FloatToStrF(ShtarValue, ffNumber, 15, 2);
      end;
  Grid2a.Repaint;

  // Phase 2
  Grid2.RowCount := 1;
  Grid2.ResetLine(0);

  Company.StockHolders.Filter:= afActive;
  For I := 0 To Company.StockHolders.Count-1 do
    with Company.StockHolders.Items[I] as TStockHolder do
      begin
        Stocks.Filter:= afActive;
        For J:= 0 To Stocks.Count-1 do
          begin
            Grid2.Cells[0, Grid2.RowCount-1]:= Name;
            Grid2.Cells[1, Grid2.RowCount-1]:= Zeut;
            Grid2.Cells[2, Grid2.RowCount-1]:= Address.City;
            Grid2.Cells[3, Grid2.RowCount-1]:= Address.Street;
            Grid2.Cells[4, Grid2.RowCount-1]:= Address.Number;
            Grid2.Cells[5, Grid2.RowCount-1]:= Address.Index;
            with Stocks.Items[J] as TStock do
              begin
                Grid2.Cells[6, Grid2.RowCount-1]:= Name;
                Grid2.Cells[7, Grid2.RowCount-1]:= FormatNumber(FloatToStr(Value),15,4,True);
                Grid2.Cells[8, Grid2.RowCount-1]:= FormatNumber(FloatToStr(Count),15,2,True);
                Grid2.Cells[9, Grid2.RowCount-1]:= FormatNumber(FloatToStr(Paid),15,2,True);
              end;
            Grid2.RowCount:= Grid2.RowCount+1;
          end;
      end;
  If Grid2.RowCount>1 Then
    Grid2.RowCount:= Grid2.RowCount-1;
  Grid2.Repaint;

  // Phase 3
  Company.Directors.Filter:= afActive;
  Grid3.RowCount:= Company.Directors.Count;
  Grid3.ResetLine(0);
  For I:= 0 To Company.Directors.Count-1 do
    With Company.Directors.Items[I] as TDirector do
      begin
        Grid3.Cells[0,I]:= Name;
        Grid3.Cells[1,I]:= Zeut;
        Grid3.Cells[2,I]:= Address.Country;
        Grid3.Cells[3,I]:= Address.City;
        Grid3.Cells[4,I]:= Address.Street;
        Grid3.Cells[5,I]:= Address.Number;
        Grid3.Cells[6,I]:= Address.Index;
        Grid3.Cells[7,I]:= DateToLongStr(Date1);
      end;
  Grid3.Repaint;

  // Pahse 4
  Grid4.RowCount:=1;
  Grid4.ResetLine(0);
  Company.Directors.Filter:= afNonActive;
  For I:= 0 To Company.Directors.Count-1 do
    With Company.Directors.Items[I] as TDirector Do
      If LastChanged > LastDoh Then
        begin
          Grid4.Cells[0, Grid4.RowCount-1]:= Name;
          Grid4.Cells[1, Grid4.RowCount-1]:= Zeut;
          Grid4.Cells[2, Grid4.RowCount-1]:= DateToLongStr(LastChanged);
          Grid4.RowCount:= Grid4.RowCount+1;
        end;
  If Grid4.RowCount>1 Then
    Grid4.RowCount:= Grid4.RowCount-1;
  WindowState:= wsNormal;
end;

procedure TfmDohFull.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  fmDoh.WindowState:= wsNormal;
end;

end.
