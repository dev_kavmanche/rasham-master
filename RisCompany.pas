{
MODIFICATION HISTORY
DATE         BY    TASK      VERSION     DESCRIPTION
}
unit RisCompany;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Companies, HebForm, StdCtrls, HGrids, SdGrid, HEdit, ExtCtrls, HLabel,
  DataObjects, CompanyInfoTemplate, Db, DBTables;

type
  TfmRisCompany = class(TfmCompanies)
    procedure btOkClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
  private
    { Private declarations }
  protected
    DraftMode: Boolean;
    FSender: TfmCompanyInfoTemplate;
    FileItem: TFileItem;
    FilesList: TFilesList;
    procedure FillData; virtual;
  public
    { Public declarations }
    function Execute(Sender: TfmCompanyInfoTemplate): TFileItem;
  end;

var
  fmRisCompany: TfmRisCompany;

implementation

uses
  dialog, DataM, Util;

{$R *.DFM}

function TfmRisCompany.Execute(Sender: TfmCompanyInfoTemplate): TFileItem;
begin
  FileItem:= Nil;
  FSender:= Sender;
  with fmDialog Do
  begin
    memo.lines.Clear;
    memo.lines.add('��� ����� ������ ����');
    Button1.Caption:= '������';
    Button2.Caption:= '�����';
    ShowModal;
    Button1.Caption:= '��';
    Button2.Caption:= '��';
    if DialogResult = drYES then
    begin
      DraftMode:= False;
      Self.Caption:= '���� ����� - ����';
      FillData;
      Self.ShowModal;
    end
    Else If DialogResult = drNo Then
    begin
      DraftMode:= True;
      btDelete.Visible:= True;
      Self.Caption:= '���� ����� - �����';
      FillData;
      Self.ShowModal;
    end
    Else
    begin
      Result := Nil;
      Exit;
    end;
  end;
  Result:= FileItem;
  FilesList.Free;
end;

procedure TfmRisCompany.FillData;
var
  I: Integer;
begin
  sdNames.Rows[0].Clear;
  FilesList:= TFilesList.Create(-1, faRishum, DraftMode);
  sdNames.RowCount:= FilesList.Count;
  For I := 0 To FilesList.Count - 1 Do
  begin
    sdNames.Cells[0,I] := FilesList.FileInfo[I].FileName;
    sdNames.Cells[1,I] := DateToLongStr(FilesList.FileInfo[I].DecisionDate);
  end;
  SortGrid(sdNames,'0,STRING,UP');
  sdNames.Repaint;
end;

procedure TfmRisCompany.btOkClick(Sender: TObject);
var
  I,Line: Integer;
begin
  Line:= -1;
  If edName.Text <> '' Then
  begin
    For I := 0 to FilesList.Count - 1 do
      If FilesList.FileInfo[I].FileName = edName.Text Then
        Line:= I;
  end;

  If Line = -1 Then
  begin
    fmDialog.ActiveControl:= fmDialog.Button3;
    fmDialog.Caption:= '�����';
    fmDialog.memo.lines.Clear;
    fmDialog.memo.lines.add('��� ��');
    fmDialog.memo.lines.add('���� �� �� ����� ����� �������');
    fmDialog.Button1.Visible:= false;
    fmDialog.Button2.Visible:= False;
    fmDialog.Button3.Caption:= '�����';
    fmDialog.ShowModal;
    fmDialog.Button3.Caption:= '�����';
    fmDialog.Button1.Visible:= True;
    fmDialog.Button2.Visible:= True;
    fmDialog.Caption:= '����';
  end
  Else
  begin
    try
      FileItem:= FilesList.Items[Line];
    except
      FileItem:= nil;
    end;
    If Sender = btOk Then
      Close;
  end;
end;

procedure TfmRisCompany.btDeleteClick(Sender: TObject);
begin
  btOkClick(Sender);
  If FileItem = nil Then
    Exit;
  fmDialog.ActiveControl:= fmDialog.Button1;
  fmDialog.memo.lines.Clear;
  fmDialog.memo.lines.add('��� ����� ����');
  fmDialog.memo.Lines.Add(FileItem.FileInfo.FileName);
  fmDialog.Button3.Visible:= false;
  fmDialog.ShowModal;
  fmDialog.Button3.Visible:= True;
  if fmDialog.DialogResult = drNo Then
  begin
    FileItem.Free;
    FileItem:= nil;
    Exit;
  end;
  Screen.Cursor :=crHourGlass;
  try
    if (FSender <> nil) then
      FSender.DeleteFile(FileItem);
    FileItem.Free;
    FileItem:= Nil;
    FilesList.Free;
    FillData;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
