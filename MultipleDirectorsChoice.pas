{
MODIFICATION HISTORY
DATE         BY    TASK      VERSION     DESCRIPTION
08/08/2021   sts   23127     1.8.6       bug fixes 
}
unit MultipleDirectorsChoice;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, HLabel, checklst, HChkListBox, HebForm, DataObjects;

type

  TfmMultipleDirectorsChoice = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    lbTitle: THebLabel;
    HebLabel2: THebLabel;
    cbbCompanies: THebCheckListBox;
    btAll: TButton;
    btOK: TButton;
    btCancel: TButton;
    HebForm1: THebForm;
    procedure btCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbbCompaniesClickCheck(Sender: TObject);
    procedure btAllClick(Sender: TObject);
    procedure btOKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbbCompaniesMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FullList: TMultiDirList;
    FResultList: TMultiDirList;
    Zeut: string;
    FileName: string;
    ChangeDate: TDateTime;
    CurrPersonNum: integer;
    TableName: string;
    PersonType: TPersonType;
    AllChecked: boolean;

    procedure FillTable;
    function GetCount: integer;
  public
    { Public declarations }
    procedure ShowFull(FZeut, FFileName: string; FCurrPersonNum: integer;  FPersonType: TPersonType; PChangeDate: TDateTime);
    property ResultList: TMultiDirList read FResultList;
    property Count: integer read GetCount;
  end;

var
  fmMultipleDirectorsChoice: TfmMultipleDirectorsChoice;

implementation

{$R *.DFM}

//========================
procedure TfmMultipleDirectorsChoice.btCancelClick(Sender: TObject);
begin
  Close;
end;

//========================
procedure TfmMultipleDirectorsChoice.ShowFull(FZeut,FFileName: string; FCurrPersonNum: integer;  FPersonType: TPersonType; PChangeDate: TDateTime);
begin
  cbbCompanies.Clear;
  AllChecked := false;
  Zeut := FZeut;
  FileName := FFileName;
  CurrPersonNum := FCurrPersonNum;
  PersonType := FPersonType;
  ChangeDate := PChangeDate;

  if PersonType = diDirector then
    lbTitle.Caption := '�������' + lbTitle.Caption
  else if PersonType = diStockHolder then
    lbTitle.Caption := '��� �����' + lbTitle.Caption;

  Screen.Cursor := crHourGlass;
  try
    FillTable;
  finally
    Screen.Cursor := crDefault;
  end;

  if (FullList.Count > 1) then
  begin
    ShowModal;
    if (ModalResult = mrCancel) then
      ModalResult := mrAbort;
  end
  else
  begin
    ModalResult := mrCancel;
    FResultList := TMultiDirList.Create;
    if FullList.Count = 1 then
      FResultList.Add(FullList.Items[0]);
  end;
end;

//========================
procedure TfmMultipleDirectorsChoice.FillTable;
var
  i: integer;
begin
  FullList.CreateAndFill(Zeut, FileName, ChangeDate);
  for i := 0 to FullList.Count - 1 do
  begin
    cbbCompanies.Items.Add((PMultipleSelectRec(FullList.Items[i])^).CompanyName);
    if FileName = '' then // not undo form
    begin
      if (
        ((PersonType = diDirector)    and (PMultipleSelectRec(FullList.Items[i])^.DirectorNum = CurrPersonNum)) or
        ((PersonType = diStockHolder) and (PMultipleSelectRec(FullList.Items[i])^.StockHolderNum = CurrPersonNum))
      ) then
        cbbCompanies.Checked[i] := True;
    end
    // undo form
    else if (PMultipleSelectRec(FullList.Items[i])^.CompanyNum = CurrPersonNum) then
      cbbCompanies.Checked[i] := True;
  end;
end;

//========================
procedure TfmMultipleDirectorsChoice.FormCreate(Sender: TObject);
begin
  FullList := TMultiDirList.Create;
end;

//=======================
procedure TfmMultipleDirectorsChoice.cbbCompaniesClickCheck(Sender: TObject);
begin
  if FileName = '' then //not undo form
    if ((PersonType = diDirector) and (PMultipleSelectRec(FullList.Items[cbbCompanies.ItemIndex])^.DirectorNum = CurrPersonNum)
       or (PersonType = diStockHolder) and (PMultipleSelectRec(FullList.Items[cbbCompanies.ItemIndex])^.StockHolderNum = CurrPersonNum)) then
      cbbCompanies.Checked[cbbCompanies.ItemIndex]:= true
  else else //undo form
    if (PMultipleSelectRec(FullList.Items[cbbCompanies.ItemIndex])^.CompanyNum  = CurrPersonNum) then
      cbbCompanies.Checked[cbbCompanies.ItemIndex]:= true;
end;

//=======================
procedure TfmMultipleDirectorsChoice.btAllClick(Sender: TObject);
var
  i: integer;
begin
  AllChecked:= not AllChecked;
  for i:=0 to cbbCompanies.Items.Count - 1 do
    if FileName ='' then //not undo form
      if ((PersonType = diDirector) and (PMultipleSelectRec(FullList.Items[i])^.DirectorNum <> CurrPersonNum)
         or (PersonType = diStockHolder) and (PMultipleSelectRec(FullList.Items[i])^.StockHolderNum <> CurrPersonNum)) then
        cbbCompanies.Checked[i]:= AllChecked
    else else //undo form
      if (PMultipleSelectRec(FullList.Items[i])^.CompanyNum  <> CurrPersonNum) then
        cbbCompanies.Checked[i]:= AllChecked;

  if AllChecked then
    btAll.Caption := '��� �� �� ����� �������'
  else
    btAll.Caption := '��� ���';
end;

//=======================
procedure TfmMultipleDirectorsChoice.btOKClick(Sender: TObject);
var
 i: integer;
begin
  FResultList:=TMultiDirList.Create;
  for i := 0 to FullList.Count -1 do
  begin
    if cbbCompanies.Checked[i] then
      FResultList.Add(FullList.Items[i]);
  end;
  ModalResult:=mrOk;
end;

procedure TfmMultipleDirectorsChoice.FormDestroy(Sender: TObject);
begin
  if FullList <> nil then
    FullList.FreeList;
  if FResultList <> nil then
    FResultList.Free;
end;

procedure TfmMultipleDirectorsChoice.cbbCompaniesMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (X< cbbCompanies.Width *0.94) and (cbbCompanies.ItemIndex>-1) then
  begin
    cbbCompanies.Checked[cbbCompanies.ItemIndex] := not (cbbCompanies.Checked[cbbCompanies.ItemIndex]);
    cbbCompaniesClickCheck(Sender);
  end;
end;

function TfmMultipleDirectorsChoice.GetCount: integer;
begin
  Result := FullList.Count;
end;

end.

