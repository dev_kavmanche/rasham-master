unit BlankStocks;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BlankTemplate, HebForm, ExtCtrls, ComCtrls, Buttons, StdCtrls, Mask,
  HLabel, HGrids, SdGrid, HComboBx, HEdit, DataObjects;

type
  TStockList = class
    Stocks: TStocks;
    Parent: TStockHolder;
  end;

  TfmStocks = class(TfmBlankTemplate)
    TabSheet2: TTabSheet;
    Bevel2: TBevel;
    Grid2: TSdGrid;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    HebLabel11: THebLabel;
    HebLabel12: THebLabel;
    Panel11: TPanel;
    Panel26: TPanel;
    gb21: TPanel;
    gb22: TPanel;
    nGrid2: THebStringGrid;
    Shape38: TShape;
    Shape30: TShape;
    Shape39: TShape;
    Shape33: TShape;
    Shape42: TShape;
    Shape31: TShape;
    Shape40: TShape;
    Shape34: TShape;
    HebLabel17: THebLabel;
    Shape41: TShape;
    HebLabel21: THebLabel;
    Shape29: TShape;
    Shape27: TShape;
    Shape35: TShape;
    Shape5: TShape;
    Shape6: TShape;
    HebLabel3: THebLabel;
    HebLabel18: THebLabel;
    Shape28: TShape;
    HebLabel15: THebLabel;
    HebLabel22: THebLabel;
    Bevel3: TBevel;
    HebLabel20: THebLabel;
    Shape32: TShape;
    HebLabel16: THebLabel;
    Grid3: TSdGrid;
    e36: TEdit;
    e38: THebComboBox;
    e37: THebComboBox;
    e31: THebComboBox;
    e34: THebEdit;
    Panel17: TPanel;
    Panel16: TPanel;
    Panel15: TPanel;
    Panel14: TPanel;
    Panel13: TPanel;
    Panel27: TPanel;
    nGrid3: THebStringGrid;
    gb31: TPanel;
    gb32: TPanel;
    e35: THebEdit;
    TempGrid31: TSdGrid;
    TempGrid32: TSdGrid;
    cbName: THebComboBox;
    cbZeut: THebComboBox;
    HebLabel4: THebLabel;
    Shape7: TShape;
    e31a: THebComboBox;
    Shape22: TShape;
    Shape8: TShape;
    btSearch: TButton;
    procedure e31Change(Sender: TObject);
    procedure Panel16Click(Sender: TObject);
    procedure Panel17Click(Sender: TObject);
    procedure gb31Click(Sender: TObject);
    procedure gb32Click(Sender: TObject);
    procedure gb21Click(Sender: TObject);
    procedure gb22Click(Sender: TObject);
    procedure nGrid2DblClick(Sender: TObject);
    procedure nGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Grid2Enter(Sender: TObject);
    procedure Grid3KeyPress(Sender: TObject; var Key: Char);
    procedure Grid2SelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure btSearchClick(Sender: TObject);
    procedure cbZeutChange(Sender: TObject);
    procedure cbNameChange(Sender: TObject);
  private
    { Private declarations }
    FReadOnly: Boolean;
    procedure SetReadOnly(Value: Boolean);
    property ReadOnly: Boolean read FReadOnly write SetReadOnly;
  protected
    { Protected declarations }
    function EvSaveData(Draft: Boolean): Boolean; override;
    function EvLoadData: Boolean; override;
    procedure EvDeleteEmptyRows; override;
    procedure EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean); override;
    procedure EvFillDefaultData; override;
    procedure EvDataLoaded; override;
    procedure EvFocusNext(Sender: TObject); override;
    procedure EvPageIndexChange; override;
    function EvCollectStock(Var NonDraft: TStockHolders; Var NonDraftStockListTemp, NonDraftStocks: TList): TStockHolders;
    function EvCollectHon(var NonDraftTemp, NonDraftNew: THonList): THonList;
    procedure Load3(Index: Integer);
    function Save3(Index: Integer): Boolean;
    procedure EvPrintData; override;
  public
    { Public declarations }
    procedure DeleteFile(FileItem: TFileItem); override;
  end;

var
  fmStocks: TfmStocks;

implementation

uses DataM, Util, cSTD, SaveDialog, dialog2, LoadDialog, PrintHakzaa,
  PeopleSearch;

{$R *.DFM}

procedure TfmStocks.DeleteFile(FileItem: TFileItem);
var
  StockHolders: TStockHolders;
  HonList: THonList;
begin
  If Not FileItem.FileInfo.Draft Then
    Raise Exception.Create('This file is not Draft at TfmStocks.DeleteFile');
  If (FileItem.FileInfo.Action <> faHakzaa) Then
    raise Exception.Create('Invalid file type at TfmStocks.DeleteFile');

  StockHolders:= TStockHolders.LoadFromFile(Company, FileItem);
  StockHolders.FileDelete(FileItem);
  StockHolders.Free;
  HonList:= THonList.LoadFromFile(Company, FileItem);
  HonList.FileDelete(FileItem);
  HonList.Free;
  FileItem.DeleteFile;
end;

function TfmStocks.EvCollectHon(var NonDraftTemp, NonDraftNew: THonList): THonList;

  function CreateNewHonItem(Const Name: String; Value, Count, Cash, NonCash, Part, PartValue: Double): THonItem;
  var
    TempItem: THonItem;
  begin
    Result:= THonItem.CreateNew(DecisionDate, Name, Value,0,Count,Cash,NonCash,Part,PartValue, True);
    TempItem:= Company.HonList.FindHonItem(Name, Value);
    TempItem.DeActivate(DecisionDate);
    NonDraftTemp.Add(TempItem);
    NonDraftNew.Add(THonItem.CreateNew(DecisionDate, Name, Value, TempItem.Rashum, TempItem.Mokza + Count,
       TempItem.Cash+ Cash, TempItem.NonCash+NonCash, TempItem.Part+Part, TempItem.PartValue+PartValue, True));
  end;

var
  I: Integer;
begin
  Result:= THonList.CreateNew;
  NonDraftTemp:= THonList.CreateNew;
  NonDraftNew:= THonList.CreateNew;
  Company.HonList.Filter:= afActive;
  For I:= 0 To Grid2.RowCount-1 Do
    If Trim(Grid2.Cells[0,I]) <> '' Then
      Result.Add(CreateNewHonItem(Grid2.Cells[0,I], StrToFloat(Grid2.Cells[1,I]),
       StrToFloat(Grid2.Cells[2,I]), StrToFloat(Grid2.Cells[3,I]), StrToFloat(Grid2.Cells[4,I]),
       StrToFloat(Grid2.Cells[5,I]), StrToFloat(Grid2.Cells[6,I])));
end;

function TfmStocks.EvCollectStock(Var NonDraft: TStockHolders; var NonDraftStockListTemp, NonDraftStocks: TList): TStockHolders;
var
  I: Integer;

  function CreateNewStockHolder: TStockHolder;
  var
    Stocks,
    TempStocks: TStocks;
    J: Integer;
    TempHolder: TStockHolder;
    TempStock: TStock;
    TempStockList: TStockList;
    TempStockListTemp: TStockList;
  begin
    // Phase #1 Creating Draft StockHolder
    Stocks:= TStocks.CreateNew;
    With TempGrid32 Do
      For J:= 0 To RowCount-1 Do
        If Cells[0,J] = TempGrid31.Cells[0,I] Then
          Stocks.Add( TStock.CreateNew(DecisionDate, Cells[1,J], StrToFloat(Cells[2,J]), StrToFloat(Cells[3,J]), StrToFloat(Cells[4,J]), True));

    With TempGrid31 do
      Result:= TStockHolder.CreateNew(DecisionDate, Cells[1,I], Cells[2,I], Cells[8,I] = '1',
         TStockHolderAddress.CreateNew(Cells[3,I], Cells[4,I], Cells[5,I], Cells[6,I], Cells[7,I]),
         Stocks, True);

    // Phase 2
    TempHolder:= (Company.StockHolders.FindByZeut(Result.Zeut) as TStockHolder);
    if TempHolder = nil Then
    begin
      // Changed at 22/8/2000 because of "PAID" property
      // Old line ->   NonDraft.Add(Result)
      TempStocks:= TStocks.CreateNew;
      For J:= 0 To Stocks.Count-1 do
        with Stocks.Items[J] as TStock do
          TempStocks.Add( TStock.CreateNew(LastChanged, Name, Value, Count, 0, True));
      with Result do
        NonDraft.Add( TStockHolder.CreateNew(LastChanged, Name, Zeut, Taagid,
            TStockHolderAddress.CreateNew(Address.Street, Address.Number, Address.Index, Address.City, Address.Country), TempStocks, True));
      // -------- END ------------
    end
    else
    begin
      TempStockList := TStockList.Create;
      TempStockList.Parent:= TempHolder;
      TempStockList.Stocks:= TStocks.CreateNew;
      TempStockList.Stocks.Filter:= afAll;
      TempStockListTemp := TStockList.Create;
      TempStockListTemp.Parent:= TempHolder;
      TempStockListTemp.Stocks:= TStocks.CreateNew;
      TempStockListTemp.Stocks.Filter:= afAll;
      For J := 0 to Result.Stocks.Count - 1 do
      begin
        TempStock:= TempHolder.Stocks.FindStock((Result.Stocks.Items[J] as TStock).Name, (Result.Stocks.Items[J] as TStock).Value);
        If TempStock=nil then
        begin
          // Old Line -> TempStockListTemp.Stocks.Add(Result.Stocks.Items[J])
          With Result.Stocks.Items[J] as TStock do
            If Count>0 Then
              TempStockList.Stocks.Add( TStock.CreateNew(LastChanged, Name, Value, Count, 0, True));
          // -------------------  END ---------------------
        end
        else
        begin
          TempStock.DeActivate(DecisionDate);
          TempStockListTemp.Stocks.Add(TempStock);
          With Result.Stocks.Items[J] as TStock do
            If (Count+TempStock.Count)>0 Then
              TempStockList.Stocks.Add( TStock.CreateNew(DecisionDate, Name, Value,
                        Count + TempStock.Count, TempStock.Paid, True));
        end;
      end;
      If TempStockList.Stocks.Count = 0 Then
        begin
          TempStockList.Stocks.Free;
          TempStockList.Free;
        end
      Else
        NonDraftStocks.Add(TempStockList);

      If TempStockListTemp.Stocks.Count = 0 Then
        begin
          TempStockListTemp.Stocks.Free;
          TempStockListTemp.Free;
        end
      Else
        NonDraftStockListTemp.Add(TempStockListTemp);
    end;
  end;

begin
  Result:= TStockHolders.CreateNew;
  NonDraft:= TStockHolders.CreateNew;
  NonDraftStocks:= TList.Create;
  NonDraftStockListTemp:= TList.Create;
  For I:= 0 to TempGrid31.RowCount-1 do
    If RemoveSpaces(TempGrid31.Cells[0,I],rmBoth)<>'' Then
      Result.Add(CreateNewStockHolder);
end;

function TfmStocks.EvSaveData(Draft: Boolean): Boolean;

  procedure DisposeStockList(StockList: TList);
  var
    I,J: Integer;
  begin
    For I:= 0 To StockList.Count-1 Do
      With TStockList(StockList.Items[I]) Do
        begin
          For J:= 0 to Stocks.Count-1 Do
            If Stocks.Items[J].Active Then
              Stocks.Items[J].Free;
          Stocks.Deconstruct;
        end;
    StockList.Free;
  end;

var
  FileItem: TFileItem;
  FileInfo: TFileInfo;
  I: Integer;
  DraftHonList, NonDraftHonListTemp, NonDraftHonListNew: THonList;
  DraftStockHolders, NonDraftStockHolder: TStockHolders;
  NonDraftStockList, NonDraftStockListTemp: TList;
  FilesList: TFilesList;
begin
  Result:= False;
  try
    Save3(StrToInt(e31.Text));
    DraftHonList:=EvCollectHon(NonDraftHonListTemp, NonDraftHonListNew);
    DraftStockHolders:= EvCollectStock(NonDraftStockHolder, NonDraftStockListTemp, NonDraftStockList);
    FilesList:= TFilesList.Create(Company.RecNum, faHakzaa, Draft);
    FileInfo:= TFileInfo.Create;
    FileInfo.Action:= faHakzaa;
    FileInfo.Draft:= Draft;
    FileInfo.DecisionDate:= DecisionDate;
    FileInfo.NotifyDate:= NotifyDate;
    FileInfo.CompanyNum:= Company.RecNum;
    Try
      FileItem:= fmSaveDialog.Execute(Self, FilesList, FileInfo);
      If FileItem<> Nil then
        begin
          FileName:= FileItem.FileInfo.FileName;
          FileItem.FileInfo.Draft:= True;
          DraftHonList.ExSaveData(FileItem, Company);
          DraftStockHolders.ExSaveData(FileItem, Company);
          If not Draft Then
            begin
              NonDraftHonListTemp.SaveData(Company, FileItem.FileInfo.RecNum);
              NonDraftHonListNew.SaveData(Company, FileItem.FileInfo.RecNum);
              NonDraftStockHolder.SaveData(Company, FileItem.FileInfo.RecNum);
              For I:= 0 to NonDraftStockListTemp.Count-1 do
                With TStockList(NonDraftStockListTemp.Items[I]) do
                  Stocks.SaveData(Parent, FileItem.FileInfo.RecNum);
              For I:= 0 To NonDraftStockList.Count-1 Do
                With TStockList(NonDraftStockList.Items[I]) do
                  Stocks.SaveData(Parent, FileItem.FileInfo.RecNum);
            end;
          FileItem.Free;
          fmDialog2.Show;
          Result:= True;
        end
      Else
        FileInfo.Free;
    finally
      FilesList.Free;
      DisposeStockList(NonDraftStockList);
      For I:= 0 To NonDraftStockListTemp.Count-1 do
        With TStockList(NonDraftStockListTemp.Items[I]) do
          Stocks.Deconstruct;
      NonDraftStockListTemp.Free;
      DraftStockHolders.Free;
      NonDraftStockHolder.Free; // Changed Deconstruct;
      NonDraftHonListNew.Free;
      NonDraftHonListTemp.Deconstruct;
      DraftHonList.Free;
    end;
  finally
  end;
end;

function TfmStocks.EvLoadData: Boolean;

  procedure LoadHon(HonList: THonList);
  var
    I: Integer;
  begin
    Grid2.RowCount:= HonList.Count;
    For I:= 0 to HonList.Count-1 Do
      With HonList.Items[I] as THonItem do
        begin
          Grid2.Cells[0,I]:= Name;
          Grid2.Cells[1,I]:= FormatNumber(FloatToStr(Value),15,4,False);
          Grid2.Cells[2,I]:= FormatNumber(FloatToStr(Mokza),15,2,False);
          Grid2.Cells[3,I]:= FormatNumber(FloatToStr(Cash),15,2,False);
          Grid2.Cells[4,I]:= FormatNumber(FloatToStr(NonCash),15,2,False);
          Grid2.Cells[5,I]:= FormatNumber(FloatToStr(Part),15,2,False);
          Grid2.Cells[6,I]:= FormatNumber(FloatToStr(PartValue),15,2,False);
        end;
    HonList.Free;
  end;

  procedure LoadStockHolders(StockHolders: TStockHolders);
  var
    J, I: Integer;
  begin
    // TempGrid31.RowCount:= StockHolders.Count;
    For I:= 0 To StockHolders.Count-1 do
      With TStockHolder(StockHolders.Items[I]) Do
        begin
          // Address info
          TempGrid31.RowCount:= TempGrid31.RowCount+1;
          TempGrid31.Cells[0,TempGrid31.RowCount-1]:= IntToStr(e31.Items.Count+1);
          e31.Items.Add(TempGrid31.Cells[0,TempGrid31.RowCount-1]);
          TempGrid31.Cells[1,TempGrid31.RowCount-1]:= Name;
          TempGrid31.Cells[2,TempGrid31.RowCount-1]:= Zeut;
          TempGrid31.Cells[3,TempGrid31.RowCount-1]:= Address.Street;
          TempGrid31.Cells[4,TempGrid31.RowCount-1]:= Address.Number;
          TempGrid31.Cells[5,TempGrid31.RowCount-1]:= Address.Index;
          TempGrid31.Cells[6,TempGrid31.RowCount-1]:= Address.City;
          TempGrid31.Cells[7,TempGrid31.RowCount-1]:= Address.Country;
          If Taagid then
            TempGrid31.Cells[8,TempGrid31.RowCount-1]:= '1'
          Else
            TempGrid31.Cells[8,TempGrid31.RowCount-1]:= '0';
            
          // Stocks
          Stocks.Filter:= afActive;
          For J:= Stocks.Count-1 DownTo 0 do
            With Stocks.Items[J] as TStock Do
              begin
                 TempGrid32.RowCount:= TempGrid32.RowCount+1;
                 TempGrid32.Cells[0,TempGrid32.RowCount-1]:= TempGrid31.Cells[0,TempGrid31.RowCount-1];
                 TempGrid32.Cells[1,TempGrid32.RowCount-1]:= Name;
                 TempGrid32.Cells[2,TempGrid32.RowCount-1]:= FormatNumber(FloatToStr(Value),15,4,False);
                 TempGrid32.Cells[3,TempGrid32.RowCount-1]:= FormatNumber(FloatToStr(Count),15,2,False);
                 TempGrid32.Cells[4,TempGrid32.RowCount-1]:= FormatNumber(FloatToStr(Paid),15,4,False);
              end;
        end;
    StockHolders.Free;
  end;
  
var
  FileItem: TFileItem;
  FilesList: TFilesList;
begin
  Result:= False;
  FileItem:= fmLoadDialog.Execute(Self, FilesList, Company.RecNum, faHakzaa);
  If FileItem = Nil Then
    Exit;
  DecisionDate:= (FileItem.FileInfo.DecisionDate);
  NotifyDate:= (FileItem.FileInfo.NotifyDate);
  Grid2.RowCount:= 1;
  Grid2.ResetLine(0);
  Grid3.RowCount:= 1;
  Grid3.ResetLine(0);
  nGrid3.RowCount:=1;
  nGrid3.Rows[0].Clear;
  TempGrid31.RowCount:=1;
  TempGrid31.ResetLine(0);
  TempGrid32.RowCount:=1;
  TempGrid32.ResetLine(0);
  e31.Items.Clear;
  e31.Tag:=1;
  LoadHon( THonList.LoadFromFile(Company, FileItem));
  LoadStockHolders( TStockHolders.LoadFromFile(Company, FileItem));
  e31.ItemIndex:=0;
  Load3(1);
  If e31.Items.Count=0 then
    e31.Items.add('1');
  FileItem.Free;
  FilesList.Free;
end;

procedure TfmStocks.EvDeleteEmptyRows;
var
  I: Integer;
begin
  // Grid2
  With Grid2 Do
    For I:= RowCount-1 DownTo 0 Do
      If (RemoveSpaces(Cells[0,I], rmBoth) = '') And CheckNumberField(Cells[1,I]) And CheckNumberField(Cells[2,I]) And
          CheckNumberField(Cells[3,I]) And CheckNumberField(Cells[4,I]) And CheckNumberField(Cells[5,I]) And
          CheckNumberField(Cells[6,I]) Then
        LineDeleteByIndex(I);

  // Grid3
  With Grid3 Do
    For I:= RowCount-1 DownTo 0 Do
      If (RemoveSpaces(Cells[0,I], rmBoth) = '') And CheckNumberField(Cells[1,I]) And CheckNumberField(Cells[2,I]) And
          CheckNumberField(Cells[3,I]) Then
        LineDeleteByIndex(I);
end;

procedure TfmStocks.SetReadOnly(Value: Boolean);
begin
  FReadOnly:= Value;
  e34.ReadOnly:= Value;
  e35.ReadOnly:= Value;
  e36.ReadOnly:= Value;
  e37.Enabled:= Not Value;
  e38.Enabled:= Not Value;
end;

procedure TfmStocks.EvPageIndexChange;
var
  I: Integer;
begin
  Inherited;
  try
    case PageIndex of
      0: Grid2.SetFocus;
      1: begin
           cbZeut.SetFocus;
           for i:=0 to nGrid3.RowCount-1 do
             nGrid3.Rows[i].Clear;
           nGrid3.RowCount := 1;

           for I:=0 to Grid2.RowCount-1 do
             if DoubleIndexOf(nGrid3, Grid2.Cells[0,I], Grid2.Cells[1,I], 1)=-1 then
               begin
                 nGrid3.Cells[0,nGrid3.RowCount-1] := IntToStr(nGrid3.RowCount);
                 nGrid3.Cells[1,nGrid3.RowCount-1] := Grid2.Cells[0,i];
                 nGrid3.Cells[2,nGrid3.RowCount-1] := Grid2.Cells[1,I];
                 nGrid3.RowCount := nGrid3.RowCount+1;
               end;
           if nGrid3.RowCount >1 then
             nGrid3.RowCount := nGrid3.RowCount-1;
         end;
    end;
  except end;
end;

procedure TfmStocks.EvFocusNext(Sender: TObject);
begin
  inherited;
  if Sender = e14 then
    if PageIndex = 0 then
      Grid2.SetFocus
    else
      cbZeut.SetFocus;
  if Sender = e31     then e31a.SetFocus;
  if Sender = e31a    then cbZeut.SetFocus;
  if Sender = cbZeut  then cbName.SetFocus;
  if Sender = cbName  then e34.SetFocus;
  if Sender = e34     then e35.SetFocus;
  if Sender = e35     then e36.SetFocus;
  if Sender = e36     then
    if ReadOnly       then
      Grid3.SetFocus
    else
      e37.SetFocus;
  if Sender = e37     then e38.SetFocus;
  if Sender = e38     then Grid3.SetFocus;
end;

procedure TfmStocks.EvDataLoaded;
var
  i: Integer;
begin
  Inherited;
  If Not DoNotClear Then
    begin
      e38.Text := '�����';
      e31.ItemIndex := 0;
      e31a.ItemIndex:= 0;
      e31.Tag:= 1;
      Grid3.RowCount:=1;
      Grid3.ResetLine(0);
      Grid2.RowCount:=1;
      Grid2.ResetLine(0);
      Grid3.Repaint;
      Grid2.Repaint;
    end;
  Company.HonList.Filter:= afActive;
  nGrid2.RowCount:= 1;
  nGrid2.Rows[0].Clear;
  For I:= 0 To Company.HonList.Count-1 do
    With Company.HonList.Items[I] as THonItem do
      begin
        nGrid2.Cells[0, nGrid2.RowCount-1]:= IntToStr(nGrid2.RowCount);
        nGrid2.Cells[1, nGrid2.RowCount-1]:= Name;
        nGrid2.Cells[2, nGrid2.RowCount-1]:= FormatNumber(FloatToStr(Value),15,4,False);
        nGrid2.RowCount:= nGrid2.RowCount+1;
      end;
  If nGrid2.RowCount > 1 Then
    nGrid2.RowCount := nGrid2.RowCount-1;
  cbName.Items.Clear;
  cbZeut.Items.Clear;
  Company.StockHolders.Filter := afActive;
  for i := 0 To Company.StockHolders.Count-1 do
  begin
    cbZeut.Items.Add(Company.StockHolders.Items[i].Zeut);
    cbName.Items.Add(Company.StockHolders.Items[i].Name);
  end;
end;

procedure TfmStocks.EvFillDefaultData;
begin
  Inherited;
  DATA.taCity.First;
  While Not DATA.taCity.EOF do
    begin
      e37.Items.Add(DATA.taCity.Fields[0].Text);
      DATA.taCity.Next;
    end;

  DATA.taCountry.First;
  While Not DATA.taCountry.EOF do
    begin
      e38.Items.Add(DATA.taCountry.Fields[0].Text);
      DATA.taCountry.Next;
    end;
end;

procedure TfmStocks.e31Change(Sender: TObject);
var
  Old_DATAChanged: Boolean;
begin
  inherited;
  If Loading then
    Exit;

  OLD_DATAChanged:= DATAChanged;
  Loading:= True;
  Save3(e31.Tag);
  Load3(StrToInt(e31.Text));
  E31.Tag := StrToInt(e31.Text);
  Loading:= False;
  DATAChanged:= OLD_DATAChanged;
  ReadOnly:= False;
end;

procedure TfmStocks.Load3(Index: Integer);
var
  I,GIndex : integer;
begin
  GIndex := TempGrid31.Cols[0].IndexOf(IntToStr(Index));

  if GIndex = -1 then
    begin
      cbName.Text := '';
      cbZeut.Text := '0';
      e34.Text := '';
      e35.Text := '';
      e36.Text := '0';
      e37.Text := '';
      e38.Text := '�����';
      e31a.ItemIndex := 0;
      Grid3.RowCount := 1;
      Grid3.ResetLine(0);
    end
  else
    begin
      cbName.Text := TempGrid31.Cells[1,GIndex];
      cbZeut.Text := TempGrid31.Cells[2,GIndex];
      e34.Text    := TempGrid31.Cells[3,GIndex];
      e35.Text    := TempGrid31.Cells[4,GIndex];
      e36.Text    := TempGrid31.Cells[5,GIndex];
      e37.Text    := TempGrid31.Cells[6,GIndex];
      e38.Text    := TempGrid31.Cells[7,GIndex];
      e31a.ItemIndex:= StrToInt(TempGrid31.Cells[8,GIndex]);
      Grid3.RowCount := 1;
      Grid3.ResetLine(0);

      for i:=0 to TempGrid32.RowCount-1 do
        if TempGrid32.Cells[0,i] = IntToStr(Index) then
          begin
            Grid3.Cells[0,Grid3.RowCount-1] := TempGrid32.Cells[1,i];
            Grid3.Cells[1,Grid3.RowCount-1] := TempGrid32.Cells[2,i];
            Grid3.Cells[2,Grid3.RowCount-1] := TempGrid32.Cells[3,i];
            Grid3.Cells[3,Grid3.RowCount-1] := TempGrid32.Cells[4,i];

            Grid3.RowCount       := Grid3.RowCount+1;
          end;
      if Grid3.RowCount > 1 then
        Grid3.RowCount := Grid3.RowCount-1;
    end;

  Grid3.Repaint;
end;

function TfmStocks.Save3(Index: Integer): Boolean;
var
  i,GIndex   : integer;
begin
  if (RemoveSpaces(cbName.Text,rmBoth) = '') and (cbZeut.Text = '0') then
  begin
    Result := False;
    Exit;
  end;

  GIndex := TempGrid31.Cols[0].IndexOf(IntToStr(Index));
  if GIndex = -1 then
    begin
      TempGrid31.RowCount := TempGrid31.RowCount+1;
      GIndex := TempGrid31.RowCount-1;
    end;

  TempGrid31.Cells[0,GIndex] := IntToStr(Index);
  TempGrid31.Cells[1,GIndex] := cbName.Text;
  TempGrid31.Cells[2,GIndex] := cbZeut.Text;
  TempGrid31.Cells[3,GIndex] := e34.Text;
  TempGrid31.Cells[4,GIndex] := e35.Text;
  TempGrid31.Cells[5,GIndex] := e36.Text;
  TempGrid31.Cells[6,GIndex] := e37.Text;
  TempGrid31.Cells[7,GIndex] := e38.Text;
  TempGrid31.Cells[8,GIndex] := IntToStr(e31a.ItemIndex);

  While (TempGrid32.Cols[0].IndexOf(IntToStr(Index))<>-1) do
    TempGrid32.LineDeleteByIndex(TempGrid32.Cols[0].IndexOf(IntToStr(Index)));

  TempGrid32.RowCount:= TempGrid32.RowCount + 1;

  for i := 0 to Grid3.RowCount-1 do
  begin
    if RemoveSpaces(Grid3.Cells[0,i],rmBoth)<>'' then
    begin
      TempGrid32.Cells[0,TempGrid32.RowCount-1] := IntToStr(Index);
      TempGrid32.Cells[1,TempGrid32.RowCount-1] := Grid3.Cells[0,i];
      TempGrid32.Cells[2,TempGrid32.RowCount-1] := Grid3.Cells[1,i];
      TempGrid32.Cells[3,TempGrid32.RowCount-1] := Grid3.Cells[2,i];
      TempGrid32.Cells[4,TempGrid32.RowCount-1] := Grid3.Cells[3,i];
      TempGrid32.RowCount := TempGrid32.RowCount+1;
    end;
  end;
  TempGrid32.RowCount:= TempGrid32.RowCount -1;
  Result:= True;
end;

procedure TfmStocks.Panel16Click(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  EvTimerOver;
  Loading:= True;
  if e31.Items.Count = 1 then
    begin
      cbName.Text := '';
      cbZeut.Text := '';
      e34.Text := '';
      e35.Text := '';
      e36.Text := '';
      e37.Text := '';
      e38.Text := '�����';
      Grid3.RowCount := 1;
      Grid3.ResetLine(0);
    end;

  While (TempGrid32.Cols[0].IndexOf(e31.Text)<>-1) do
    TempGrid32.LineDeleteByIndex(TempGrid32.Cols[0].IndexOf(e31.Text));
  While (TempGrid31.Cols[0].IndexOf(e31.Text)<>-1) do
    TempGrid31.LineDeleteByIndex(TempGrid31.Cols[0].IndexOf(e31.Text));

  for i:=0 to TempGrid32.RowCount-1 do
    try
      if (StrToInt(TempGrid32.Cells[0,i]) > StrToInt(e31.Text)) then
        TempGrid32.Cells[0,i] := IntToStr(StrToInt(TempGrid32.Cells[0,i])-1);
    except end;

  for i:=0 to TempGrid31.RowCount-1 do
    try
      if (StrToInt(TempGrid31.Cells[0,i]) > StrToInt(e31.Text)) then
        TempGrid31.Cells[0,i] := IntToStr(StrToInt(TempGrid31.Cells[0,i])-1);
    except end;

  if (e31.ItemIndex = e31.Items.Count-1) and (e31.Text<>'1') then
    e31.ItemIndex := e31.ItemIndex-1;

  e31.Tag := StrToInt(e31.Text);
  Load3(StrToInt(e31.Text));

  if e31.Items.Count>1 then
    e31.Items.Delete(e31.Items.Count-1);
  Loading:= False;
  ReadOnly:= False;
  DATAChanged:= True;
end;

procedure TfmStocks.Panel17Click(Sender: TObject);
begin
  inherited;
  if Save3(StrToInt(e31.Text)) then
  begin
    e31.Items.Add(IntToStr(e31.Items.Count+1));
    e31.ItemIndex := e31.Items.Count-1;
    e31Change(nil);
    cbZeut.Text := '0';
    ReadOnly := False;
  end;
  cbZeut.SetFocus;
end;

procedure TfmStocks.gb31Click(Sender: TObject);
begin
  inherited;
  Grid3.LineDelete;
end;

procedure TfmStocks.gb32Click(Sender: TObject);
begin
  inherited;
  Grid3.LineInsert;
end;

procedure TfmStocks.gb21Click(Sender: TObject);
begin
  inherited;
  Grid2.LineDelete;
end;

procedure TfmStocks.gb22Click(Sender: TObject);
begin
  inherited;
  Grid2.LineInsert;
end;

procedure TfmStocks.nGrid2DblClick(Sender: TObject);
var
  VKKEY: Word;
begin
  Inherited;
  VKKEY:= VK_NONAME;
  if Sender = nGrid2 then
    begin
      Grid2.Cells[0,Grid2.Row] := nGrid2.Cells[1,nGrid2.Row];
      Grid2.Cells[1,Grid2.Row] := nGrid2.Cells[2,nGrid2.Row];
      Grid2.Col := 1;
      Grid2.SetFocus;
      GridKeyDown(Grid2, VKKEY, []);
    end
  else
    if Sender = nGrid3 then
      begin
        Grid3.Cells[0,Grid3.Row] := nGrid3.Cells[1,nGrid3.Row];
        Grid3.Cells[1,Grid3.Row] := nGrid3.Cells[2,nGrid3.Row];
        Grid3.Col := 2;
        Grid3.SetFocus;
        GridKeyDown(Grid3, VKKEY, []);
      end;
end;

procedure TfmStocks.nGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (key = VK_RETURN) then
    nGrid2DblClick(Sender);

  if (key = VK_ESCAPE) then
    if Sender = nGrid2 then
      begin
        Grid2.Col := 1;
        Grid2.SetFocus;
      end
    else
      if Sender = nGrid3 then
        begin
          Grid3.Col := 2;
          Grid3.SetFocus;
        end;
end;

procedure TfmStocks.Grid2Enter(Sender: TObject);
var
  CanSelect : boolean;
begin
  inherited;
  if Sender = Grid2 then
    if (Grid2.Row = 0) and (Grid2.Col = 0) then
      Grid2SelectCell(Grid2,0,0,CanSelect);

  if Sender = Grid3 then
    if (Grid3.Row = 0) and (Grid3.Col = 0) then
      Grid2SelectCell(Grid3,0,0,CanSelect);
end;

procedure TfmStocks.Grid3KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (key=#13) Then
    If ((Sender as TSDGrid).EditorMode=false) then
      (Sender as TSDGrid).EditorMode:=True
    Else
      begin
        nGrid2.Visible:= False;
        nGrid3.Visible:= False;
      end;
end;

procedure TfmStocks.Grid2SelectCell(Sender: TObject; Col, Row: Integer;
  var CanSelect: Boolean);
var
  i,Found        : integer;
begin
  inherited;
  // GRID 2
  if Sender=Grid2 then
    begin
      if  Grid2.PCol = 1 then
        Grid2.Cells[Grid2.Pcol,Grid2.Prow] := FormatNumber(Grid2.Cells[Grid2.Pcol,Grid2.Prow],10,4,False);
      if (Grid2.PCol = 2) or (Grid2.PCol = 3) or (Grid2.PCol = 4)
          or (Grid2.PCol = 5) or (Grid2.PCol = 6) or (Grid2.PCol = 7) then
        Grid2.Cells[Grid2.Pcol,Grid2.Prow] := FormatNumber(Grid2.Cells[Grid2.Pcol,Grid2.Prow],10,2,False);

      if (grid2.Pcol=0) then
        begin
          Found:=-1;
            for i:=0 to nGrid2.RowCount-1 do
              if (Grid2.Cells[0,Grid2.PRow]=nGrid2.Cells[0,i]) and (RemoveSpaces(nGrid2.Cells[0,I], rmBoth)<>'') then
                Found:=i;
          if Found<>-1 then
            begin
              Grid2.Cells[0,Grid2.PRow]:=nGrid2.Cells[1,Found];
              Grid2.Cells[1,Grid2.PRow]:=nGrid2.Cells[2,Found];
            end;
        end;

      nGrid2.Visible:= Col=0;
    end;

  // GRID 3
  if Sender=Grid3 then
    begin
      if  (Grid3.PCol = 1) or (Grid3.PCol = 3) then
        Grid3.Cells[Grid3.Pcol,Grid3.Prow] := FormatNumber(Grid3.Cells[Grid3.Pcol,Grid3.Prow],10,4,False);
      if (Grid3.PCol = 2)  then
        Grid3.Cells[Grid3.Pcol,Grid3.Prow] := FormatNumber(Grid3.Cells[Grid3.Pcol,Grid3.Prow],10,2,False);

      if (grid3.Pcol=0) then
        begin
          Found:=-1;
          for i:=0 to nGrid3.RowCount-1 do
            if (Grid3.Cells[0,Grid3.PRow]=nGrid3.Cells[0,i]) and (RemoveSpaces(nGrid3.Cells[0,I], rmBoth)<>'') then
              Found:=i;
          if Found<>-1 then
            begin
              Grid3.Cells[0,Grid3.PRow]:=nGrid3.Cells[1,Found];
              Grid3.Cells[1,Grid3.PRow]:=nGrid3.Cells[2,Found];
            end;
        end;

      nGrid3.Visible:= Col=0;
    end;
end;

procedure TfmStocks.EvCheckPage(Page: Integer; Silent, Extended, Draft: Boolean);

  procedure SpecialCheckID(const ID: String; ComboBox: THebComboBox; ItemIndex: Integer; Edit: THebComboBox);
  begin
    If (not CheckID(ID)) and
       (not Silent) and (not ErrorTimer.Enabled) Then
      begin
        If PageIndex<> Page Then
          PageIndex:= Page;
        ComboBox.ItemIndex:= ItemIndex;
        ComboBox.OnChange(ComboBox);
        If ErrorTimer.Tag= 0 Then
          try
            Edit.SetFocus;
          except end;

        If Edit.Color = clWindow Then
          Edit.Color:= clBtnFace
        Else
          Edit.Color:= clWindow;

        If Application.MessageBox('��� ��!'+#13+#10+'���� ����� ����/���� ���� ����.'+#13+#10+'��� ��� ��� �����?',
                                  PChar(Caption), MB_YESNO + MB_IconHand + MB_RTLREADING + MB_RIGHT) <> IDYES then
          begin
            ErrorAt:= Edit;
            raise Exception.Create('zeut');
          end;
      end;
  end;

var
  HonList: THonList;
  HonItem: THonItem;
  StockHolders: TStockHolders;
  Stocks: TStocks;
  I,D,J: Integer;
  TempStock: TStock;
  TempStockHolder,
  LastStockHolder: TStockHolder;
  UsedStocks: Double;
begin
  Inherited;

  case Page of
    0: CheckGrid(Page, Silent, Extended, Grid2);
    1: CheckGrid(Page, Silent, Extended, Grid3);
  end;

  If Not Extended then
    Exit;

  case Page of
    0: begin
         HonList:= THonList.CreateNew;
         HonList.Filter:= afActive;
         Company.HonList.Filter:= afActive;
         try
           With Grid2 Do
             For I:= 0 To RowCount-1 Do
               If HonList.FindHonItem(Cells[0,I], StrToFloat(Cells[1,I])) = nil Then
                 begin
                   HonItem:= Company.HonList.FindHonItem(Cells[0,I], StrToFloat(Cells[1,I]));
                   If HonItem = Nil Then
                     begin
                       If Not Silent Then
                         begin
                           Row:= I;
                           Col:= 0;
                         end;
                       ErrorAt:= Grid2;
                       raise Exception.Create('���� �� �����');
                     end
                   Else
                     If HonItem.Rashum < (HonItem.Mokza + StrToFloat(Cells[2,I])) then
                       begin
                         If Not Silent Then
                           begin
                             Row:= I;
                             Col:= 2;
                           end;
                         ErrorAt:= Grid2;
                         raise Exception.Create('��� ������ ������ ���� ���� ����� ���� ���� �����');
                       end
                     Else
                       If (HonItem.Mokza + StrToFloat(Cells[2,I]))<0 Then
                         begin
                           If Not Silent Then
                             begin
                               Row:= I;
                               Col:= 2;
                             end;
                           ErrorAt:= Grid2;
                           raise Exception.Create('��� ������ ������ ���� ���� ����� ��� ����');
                         end
                       Else
                         HonList.Add(THonItem.CreateNew(DecisionDate, Cells[0,I], StrToFloat(Cells[1,I]),0,0,0,0,0,0,True));
                 end
               Else
                 begin
                   If Not Silent Then
                     begin
                       Row:= I;
                       Col:= 0;
                     end;
                   ErrorAt:= Grid2;
                   raise Exception.Create('�� ���� ����� ���� ���� ���� ���� ���');
                 end;
         finally
           HonList.Free;
         end;
       end;
    1: begin
         Save3(StrToInt(e31.Text));
         StockHolders:= TStockHolders.CreateNew;
         StockHolders.Filter:= afActive;
         try
           With TempGrid31 Do
             For I:= 0 To RowCount -1 Do
               If RemoveSpaces(Cells[0,I], rmBoth)<> '' Then
                 If StockHolders.FindByZeut(Cells[2,I]) = Nil Then
                 begin
                   SpecialCheckID(Cells[2,I], e31, e31.Items.IndexOf(Cells[0,I]), cbZeut);
                   Stocks:= TStocks.CreateNew;
                   With TempGrid32 Do
                     For J:= 0 To RowCount-1 Do
                       If Cells[0,J] = TempGrid31.Cells[0,I] Then
                         If Stocks.FindStock(Cells[1,J], StrToFloat(Cells[2,J])) = Nil Then
                           If DoubleIndexOf(Grid2, Cells[1,J], Cells[2,J], 0) <> -1 then
                           begin
                             TempStockHolder:= (Company.StockHolders.FindByZeut(Cells[2,I]) as TStockHolder);
                             If TempStockHolder<>nil Then
                             begin
                               TempStock:= TempStockHolder.Stocks.FindStock(Cells[1,J], StrToFloat(Cells[2,J]));
                               If (TempStock<> nil) and ((TempStock.Count+StrToFloat(Cells[3,J])) < 0) Then
                               begin
                                 If Not Silent Then
                                 begin
                                   e31.ItemIndex:= e31.Items.IndexOf(Cells[0,J]);
                                   e31Change(e31);
                                   Grid3.Row:= DoubleIndexOf(Grid3, Cells[1,J], Cells[2,J], 0);
                                   Grid3.Col:= 2;
                                 end;
                                 ErrorAt:= Grid3;
                                 raise Exception.Create('���� ������ �� ���� ����� ��� ����');
                               end;
                             end;
                             Stocks.Add( TStock.CreateNew(Date, Cells[1,J], StrToFloat(Cells[2,J]), StrToFloat(Cells[3,J]), StrToFloat(Cells[4,J]), True))
                           end
                           Else
                             begin
                               If Not Silent Then
                                 begin
                                   e31.ItemIndex:= e31.Items.IndexOf(Cells[0,J]);
                                   e31Change(e31);
                                   Grid3.Row:= DoubleIndexOf(Grid3, Cells[1,J], Cells[2,J], 0);
                                   Grid3.Col:= 0;
                                 end;
                               ErrorAt:= Grid3;
                               raise Exception.Create('���� �� �����');
                             end
                         Else
                           begin
                             If Not Silent Then
                               begin
                                 e31.ItemIndex:= e31.Items.IndexOf(Cells[0,J]);
                                 e31Change(e31);
                                 Grid3.Row:= DoubleIndexOf(Grid3, Cells[1,J], Cells[2,J], 0);
                                 Grid3.Col:= 0;
                               end;
                             ErrorAt:= Grid3;
                             raise Exception.Create('�� ���� ����� ���� ���� ���� ���� ���');
                           end;

                   StockHolders.Add( TStockHolder.CreateNew(Date, Cells[1,I], Cells[2,I], Cells[8,I] = '1',
                      TStockHolderAddress.CreateNew(Cells[3,I], Cells[4,I], Cells[5,I], Cells[6,I], Cells[7,I]),
                      Stocks, True));
                 end
                 else
                 begin
                   If Not Silent Then
                   begin
                     e31.ItemIndex:= e31.Items.IndexOf(Cells[0,I]);
                     e31Change(e31);
                     if (cbZeut.Color = clWindow) then
                      cbZeut.Color := clBtnFace
                     else
                      cbZeut.Color := clWindow;
                   end;
                   ErrorAt := cbZeut;
                   raise Exception.Create('���� ���� �� ��� ���� ���� ����� ���');
                 end;

             HonList:= THonList.CreateNew;
             try
               With Grid2 Do
                 For I:= 0 To RowCount-1 Do
                   With Rows[I] Do
                     HonList.Add(THonItem.CreateNew(DecisionDate, Strings[0], StrToFloat(Strings[1]),0,StrToFloat(Strings[2]),0,0,0,0, True));

               For I:= 0 To HonList.Count-1 Do
                 begin
                   UsedStocks:= 0;

                   LastStockHolder:= Nil;
                   for D:= 0 to StockHolders.Count-1 Do
                     with (StockHolders.Items[D] as TStockHolder) do
                       for J:= 0 To Stocks.Count-1 Do
                         if (Stocks.Items[J].Name = HonList.Items[I].Name) And (TStock(Stocks.Items[J]).Value = THonItem(HonList.Items[I]).Value) Then
                           begin
                             UsedStocks:= UsedStocks+ TStock(Stocks.Items[J]).Count;
                             LastStockHolder:= (StockHolders.Items[D] as TStockHolder);
                           end;

                   If UsedStocks > THonItem(HonList.Items[I]).Mokza Then
                     begin
                       If Not Silent Then
                         begin
                           e31.ItemIndex:= e31.Items.IndexOf(TempGrid31.Cells[0, TempGrid31.Cols[2].IndexOf(LastStockHolder.Zeut)]);
                           e31Change(e31);
                           Grid3.Row:= DoubleIndexOf(Grid3, HonList.Items[I].Name, FormatNumber(FloatToStr(THonItem(HonList.Items[I]).Value),15,4,False),0);
                           Grid3.Col:= 2;
                         end;
                       ErrorAt:= Grid3;
                       raise Exception.Create('��"� ������ ������� ��� ������� ������ ����� ������');
                     end
                   Else
                     If UsedStocks < THonItem(HonList.Items[I]).Mokza Then
                     begin
                       If Not Silent Then
                         try
                           e31.ItemIndex:= e31.Items.IndexOf(TempGrid31.Cells[0, TempGrid31.Cols[2].IndexOf(LastStockHolder.Zeut)]);
                           e31Change(e31);
                           Grid3.Row:= DoubleIndexOf(Grid3, HonList.Items[I].Name, FormatNumber(FloatToStr(THonItem(HonList.Items[I]).Value),15,4,False),0);
                           Grid3.Col:= 2;
                         except end;
                       ErrorAt:= Grid3;
                       raise Exception.Create('��"� ������ ������� ���� ������� ������ ����� ������');
                     end;
                 end;
             finally
               HonList.Free;
             end;
         finally
           StockHolders.Free;
         end;
       end;
  end;
end;

procedure TfmStocks.FormCreate(Sender: TObject);
begin
  FileAction:= faHakzaa;
  ShowStyle:= afActive;
  inherited;
end;

procedure TfmStocks.EvPrintData;
var
  I,J: Integer;
  HonList: THonList;
  StockHolders: TStockHolders;
  Stocks: TStocks;
begin
  Save3(StrToInt(e31.Text));
  Application.CreateForm(TfmQRHakzaa, fmQRHakzaa);
  HonList:= THonList.CreateNew;
  StockHolders:= TStockHolders.CreateNew;
  try
    For I:= 0 to Grid2.RowCount-1 do
      HonList.Add( THonItem.CreateNew(DecisionDate, Grid2.Cells[0,I], StrToFloat(Grid2.Cells[1,I]), 0, StrToFloat(Grid2.Cells[2,I]),
           StrToFloat(Grid2.Cells[3,I]), StrToFloat(Grid2.Cells[4,I]), StrToFloat(Grid2.Cells[5,I]), StrToFloat(Grid2.Cells[6,I]), True));

    For I:= 0 to TempGrid31.RowCount-1 do
      If RemoveSpaces(TempGrid31.Cells[0,I],rmBoth)<>'' Then
      begin
        Stocks:= TStocks.CreateNew;
        With TempGrid32 do
          For J:= 0 To RowCount - 1 Do
            If Cells[0,J] = TempGrid31.Cells[0,I] Then
              Stocks.Add( TStock.CreateNew(DecisionDate, Cells[1,J], StrToFloat(Cells[2,J]), StrToFloat(Cells[3,J]), StrToFloat(Cells[4,J]), True));
        With TempGrid31 do
          StockHolders.Add(TStockHolder.CreateNew(DecisionDate, Cells[1,I], Cells[2,I], Cells[8,I] = '1',
             TStockHolderAddress.CreateNew(Cells[3,I], Cells[4,I], Cells[5,I], Cells[6,I], Cells[7,I]),
             Stocks, True));
      end;

    fmQRHakzaa.PrintExecute(FileName, Company, HonList, StockHolders, DecisionDate, NotifyDate);
  finally
    fmQRHakzaa.Free;
    HonList.Free;
    StockHolders.Free;
  end;
end;

procedure TfmStocks.btSearchClick(Sender: TObject);
var
  FResult: TListedClass;
  TempCompany: TCompany;
begin
  Application.CreateForm(TfmPeopleSearch, fmPeopleSearch);
  TempCompany:= TCompany.Create(Company.RecNum, Company.ShowingDate, False);
  FResult := fmPeopleSearch.Execute(TempCompany);
  fmPeopleSearch.Free;
  if FResult = nil then
  begin
    cbName.SetFocus;
    Exit;
  end;

  if cbZeut.Items.IndexOf(FResult.Zeut) <> -1 Then
  begin
    cbZeut.ItemIndex := cbZeut.Items.IndexOf(FResult.Zeut);
    cbZeutChange(nil);
  end
  else
  begin
    Loading:= True;
    ReadOnly:= False;
    if FResult is TStockHolder then
      with FResult as TStockHolder do
      begin
        if Taagid then
          e31a.ItemIndex := 1
        else
          e31a.ItemIndex := 0;
        cbName.Text := Name;
        cbZeut.Text := Zeut;
        e34.Text := Address.Street;
        e35.Text := Address.Number;
        e36.Text := Address.Index;
        e37.Text := Address.City;
        e38.Text := Address.Country;
      end;

    if FResult is TDirector Then
      with FResult as TDirector do
      begin
        if Taagid <> nil then
          e31a.ItemIndex := 1
        else
          e31a.ItemIndex := 0;
        cbName.Text := Name;
        cbZeut.Text := Zeut;
        e34.Text := Address.Street;
        e35.Text := Address.Number;
        e36.Text := Address.Index;
        e37.Text := Address.City;
        e38.Text := Address.Country;
      end;
  end;
  TempCompany.Free;
  Loading := False;
  DataChanged := True;
  cbName.SetFocus;
end;

procedure TfmStocks.cbZeutChange(Sender: TObject);
var
  StockHolder: TStockHolder;
begin
  inherited;
  // Moshe 27/05/2019
  Company.StockHolders.Filter := afActive;
  StockHolder := (Company.StockHolders.FindByZeut(cbZeut.Text) as TStockHolder);

  if (StockHolder <> nil) then
  begin
    cbName.Text := StockHolder.Name;
    e34.Text := StockHolder.Address.Street;
    e35.Text := StockHolder.Address.Number;
    e36.Text := StockHolder.Address.Index;
    e37.Text := StockHolder.Address.City;
    e38.Text := StockHolder.Address.Country;
    ReadOnly := True;
  end
  else
  begin
    e34.Text := '';
    e35.Text := '';
    e36.Text := '';
    e37.Text := '';
    e38.Text := '�����';
    ReadOnly := False;
  end;
end;

procedure TfmStocks.cbNameChange(Sender: TObject);
var
  StockHolder: TStockHolder;
begin
  inherited;

  // Moshe 27/05/2019
  Company.StockHolders.Filter := afActive;
  StockHolder := (Company.StockHolders.FindByName(cbName.Text) as TStockHolder);
  if (StockHolder <> nil) then
  begin
    cbZeut.Text := StockHolder.Zeut;
    e34.Text := StockHolder.Address.Street;
    e35.Text := StockHolder.Address.Number;
    e36.Text := StockHolder.Address.Index;
    e37.Text := StockHolder.Address.City;
    e38.Text := StockHolder.Address.Country;
    ReadOnly := True;
  end
  else
  begin
    e34.Text := '';
    e35.Text := '';
    e36.Text := '';
    e37.Text := '';
    e38.Text := '�����';
    ReadOnly := False;
  end;
end;

end.
